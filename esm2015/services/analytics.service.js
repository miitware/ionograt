import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class AnalyticsService {
    constructor() { }
    getUserId(rut) {
        if (!this.userId) {
            this.userId = btoa(rut);
        }
        return this.userId;
    }
    /**
     * @param categoryEvent Hace referencia a la sección del sitio web en donde el usuario está interactuando.
     * @param etiqueta Texto del elemento que se esta registrando.
     * @param tipo Tipo de evento a registrar. Valor pro defecto: 'click'.
     */
    eventRegister(categoryEvent, labelEvent, typeEvent) {
        if (!typeEvent) {
            typeEvent = 'click';
        }
        const data = {
            event: 'evento-interactivo',
            'evento-interactivo-categoria': categoryEvent,
            'evento-interactivo-accion': typeEvent,
            'evento-interactivo-etiqueta': labelEvent
        };
        window.dataLayer.push(data);
    }
    /**
     * @param categoryEvent Hace referencia a la sección del sitio web en donde el usuario está interactuando.
     * @param pageEvent Direccion o identificador de la pagina actual.
     * @param userRut Rut del usuario logeado.
     */
    pageLoadRegister(categoryEvent, pageEvent, userRut) {
        const userId = this.getUserId(userRut);
        const data = {
            event: 'pagina-virtual',
            pageEvent,
            pagename: categoryEvent,
            userID: userId,
        };
        window.dataLayer.push(data);
    }
}
/** @nocollapse */ AnalyticsService.ɵfac = function AnalyticsService_Factory(t) { return new (t || AnalyticsService)(); };
/** @nocollapse */ AnalyticsService.ɵprov = /** @pureOrBreakMyCode */ i0.ɵɵdefineInjectable({ token: AnalyticsService, factory: AnalyticsService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AnalyticsService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5hbHl0aWNzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiQzovVXNlcnMvVXN1YXJpby9EZXNrdG9wL2xpYnJlcmlhbmd1bGFyLTI5LTEwLTIxL2NvcGlhLWFuZ3VsYXItbGliLXVwZC0xMi1zaW4tcnV0L3Byb2plY3RzL2lzYXByZS1kaWdpdGFsL3NyYy8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2FuYWx5dGljcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBSzNDLE1BQU0sT0FBTyxnQkFBZ0I7SUFJM0IsZ0JBQWdCLENBQUM7SUFFVCxTQUFTLENBQUMsR0FBVztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN6QjtRQUNELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLGFBQWEsQ0FBQyxhQUFxQixFQUFFLFVBQWtCLEVBQUUsU0FBa0I7UUFDaEYsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNkLFNBQVMsR0FBRyxPQUFPLENBQUM7U0FDckI7UUFDRCxNQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxvQkFBb0I7WUFDM0IsOEJBQThCLEVBQUUsYUFBYTtZQUM3QywyQkFBMkIsRUFBRSxTQUFTO1lBQ3RDLDZCQUE2QixFQUFFLFVBQVU7U0FDMUMsQ0FBQztRQUNELE1BQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksZ0JBQWdCLENBQUMsYUFBcUIsRUFBRSxTQUFpQixFQUFFLE9BQWU7UUFDL0UsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN2QyxNQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxnQkFBZ0I7WUFDdkIsU0FBUztZQUNULFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLE1BQU0sRUFBRSxNQUFNO1NBQ2YsQ0FBQztRQUNELE1BQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7O21HQTdDVSxnQkFBZ0I7cUdBQWhCLGdCQUFnQixXQUFoQixnQkFBZ0IsbUJBRmYsTUFBTTt1RkFFUCxnQkFBZ0I7Y0FINUIsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQW5hbHl0aWNzU2VydmljZSB7XHJcblxyXG4gIHByaXZhdGUgdXNlcklkOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHByaXZhdGUgZ2V0VXNlcklkKHJ1dDogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXRoaXMudXNlcklkKSB7XHJcbiAgICAgIHRoaXMudXNlcklkID0gYnRvYShydXQpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXMudXNlcklkO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQHBhcmFtIGNhdGVnb3J5RXZlbnQgSGFjZSByZWZlcmVuY2lhIGEgbGEgc2VjY2nDs24gZGVsIHNpdGlvIHdlYiBlbiBkb25kZSBlbCB1c3VhcmlvIGVzdMOhIGludGVyYWN0dWFuZG8uXHJcbiAgICogQHBhcmFtIGV0aXF1ZXRhIFRleHRvIGRlbCBlbGVtZW50byBxdWUgc2UgZXN0YSByZWdpc3RyYW5kby5cclxuICAgKiBAcGFyYW0gdGlwbyBUaXBvIGRlIGV2ZW50byBhIHJlZ2lzdHJhci4gVmFsb3IgcHJvIGRlZmVjdG86ICdjbGljaycuXHJcbiAgICovXHJcbiAgcHVibGljIGV2ZW50UmVnaXN0ZXIoY2F0ZWdvcnlFdmVudDogc3RyaW5nLCBsYWJlbEV2ZW50OiBzdHJpbmcsIHR5cGVFdmVudD86IHN0cmluZywgKSB7XHJcbiAgICBpZiAoIXR5cGVFdmVudCkge1xyXG4gICAgICB0eXBlRXZlbnQgPSAnY2xpY2snO1xyXG4gICAgfVxyXG4gICAgY29uc3QgZGF0YSA9IHtcclxuICAgICAgZXZlbnQ6ICdldmVudG8taW50ZXJhY3Rpdm8nLFxyXG4gICAgICAnZXZlbnRvLWludGVyYWN0aXZvLWNhdGVnb3JpYSc6IGNhdGVnb3J5RXZlbnQsXHJcbiAgICAgICdldmVudG8taW50ZXJhY3Rpdm8tYWNjaW9uJzogdHlwZUV2ZW50LFxyXG4gICAgICAnZXZlbnRvLWludGVyYWN0aXZvLWV0aXF1ZXRhJzogbGFiZWxFdmVudFxyXG4gICAgfTtcclxuICAgICh3aW5kb3cgYXMgYW55KS5kYXRhTGF5ZXIucHVzaChkYXRhKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBwYXJhbSBjYXRlZ29yeUV2ZW50IEhhY2UgcmVmZXJlbmNpYSBhIGxhIHNlY2Npw7NuIGRlbCBzaXRpbyB3ZWIgZW4gZG9uZGUgZWwgdXN1YXJpbyBlc3TDoSBpbnRlcmFjdHVhbmRvLlxyXG4gICAqIEBwYXJhbSBwYWdlRXZlbnQgRGlyZWNjaW9uIG8gaWRlbnRpZmljYWRvciBkZSBsYSBwYWdpbmEgYWN0dWFsLlxyXG4gICAqIEBwYXJhbSB1c2VyUnV0IFJ1dCBkZWwgdXN1YXJpbyBsb2dlYWRvLlxyXG4gICAqL1xyXG4gIHB1YmxpYyBwYWdlTG9hZFJlZ2lzdGVyKGNhdGVnb3J5RXZlbnQ6IHN0cmluZywgcGFnZUV2ZW50OiBzdHJpbmcsIHVzZXJSdXQ6IHN0cmluZykge1xyXG4gICAgY29uc3QgdXNlcklkID0gdGhpcy5nZXRVc2VySWQodXNlclJ1dCk7XHJcbiAgICBjb25zdCBkYXRhID0ge1xyXG4gICAgICBldmVudDogJ3BhZ2luYS12aXJ0dWFsJyxcclxuICAgICAgcGFnZUV2ZW50LFxyXG4gICAgICBwYWdlbmFtZTogY2F0ZWdvcnlFdmVudCxcclxuICAgICAgdXNlcklEOiB1c2VySWQsXHJcbiAgICB9O1xyXG4gICAgKHdpbmRvdyBhcyBhbnkpLmRhdGFMYXllci5wdXNoKGRhdGEpO1xyXG4gIH1cclxufVxyXG4iXX0=