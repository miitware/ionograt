import { Directive, HostListener, Input } from '@angular/core';
import * as i0 from "@angular/core";
export class TooltipDirective {
    constructor(_el, _renderer) {
        this._el = _el;
        this._renderer = _renderer;
        this.offset = 6;
        this.maxCharacters = 200;
    }
    onMouseEnter() {
        if (!this.tooltip) {
            this.show();
        }
    }
    onMouseLeave() {
        if (this.tooltip) {
            this.hide();
        }
    }
    show() {
        this.create();
        this.setPosition();
        this._renderer.addClass(this.tooltip, 'show');
    }
    hide() {
        this._renderer.removeClass(this.tooltip, 'show');
        window.setTimeout(() => {
            this._renderer.removeChild(document.body, this.tooltip);
            this.tooltip = null;
        }, 0);
    }
    create() {
        this.tooltip = this._renderer.createElement('div');
        const inner = this._renderer.createElement('div');
        this.arrow = this._renderer.createElement('div');
        this._renderer.addClass(this.tooltip, 'tooltip');
        this._renderer.addClass(inner, 'tooltip-inner');
        this._renderer.addClass(this.arrow, 'arrow');
        this._renderer.appendChild(inner, this._renderer.createText((this.tooltipTex.length > this.maxCharacters) ? (this.tooltipTex.slice(0, this.maxCharacters)) + '...' : this.tooltipTex));
        this._renderer.appendChild(this.tooltip, this.arrow);
        this._renderer.appendChild(this.tooltip, inner);
        this._renderer.addClass(this.tooltip, `bs-tooltip-${this.placement}`);
        this._renderer.appendChild(document.body, this.tooltip);
    }
    setPosition() {
        const viwportSize = window.outerWidth;
        const hostPos = this._el.nativeElement.getBoundingClientRect();
        const tooltipPos = this.tooltip.getBoundingClientRect();
        // console.log(tooltipPos);
        const tooltipWidth = tooltipPos.width;
        // console.log(tooltipPos.width);
        const tooltipHeight = tooltipPos.height;
        const scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        let top, left;
        if (this.placement === 'top') {
            top = hostPos.top - tooltipPos.height - this.offset;
            left = (hostPos.left + (hostPos.width - tooltipPos.width) / 2) >= 0 ? hostPos.left + (hostPos.width - tooltipPos.width) / 2 : 15;
            let arrowPositionX = (tooltipWidth / 2) - 6;
            let tootipPositionX = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
            this._renderer.setStyle(this.arrow, 'left', `${tootipPositionX < 0 ? arrowPositionX - Math.abs(tootipPositionX) - 15 : arrowPositionX}px`);
            if (hostPos.top - tooltipPos.height - this.offset <= 0) {
                top = hostPos.bottom + this.offset;
                left = (hostPos.left + (hostPos.width - tooltipPos.width) / 2) >= 0 ? hostPos.left + (hostPos.width - tooltipPos.width) / 2 : 15;
                let arrowPositionX = (tooltipWidth / 2) - 6;
                let tootipPositionX = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-right');
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-left');
                this._renderer.addClass(this.tooltip, 'bs-tooltip-bottom');
                this._renderer.setStyle(this.arrow, 'top', 0);
                this._renderer.setStyle(this.arrow, 'left', `${tootipPositionX < 0 ? arrowPositionX - Math.abs(tootipPositionX) - 15 : arrowPositionX}px`);
            }
            if ((left * 100) / viwportSize > 70) {
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-right');
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-left');
                left = (hostPos.left + 20) - tooltipPos.width;
                this._renderer.setStyle(this.arrow, 'top', `${tooltipHeight - 6}px`);
                this._renderer.setStyle(this.arrow, 'left', `${tooltipWidth - 20}px`);
            }
        }
        if (this.placement === 'bottom') {
            top = hostPos.bottom + this.offset;
            left = (hostPos.left + (hostPos.width - tooltipPos.width) / 2) >= 0 ? hostPos.left + (hostPos.width - tooltipPos.width) / 2 : 15;
            let arrowPositionX = (tooltipWidth / 2) - 6;
            let tootipPositionX = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
            this._renderer.setStyle(this.arrow, 'left', `${tootipPositionX < 0 ? arrowPositionX - Math.abs(tootipPositionX) - 15 : arrowPositionX}px`);
        }
        if (this.placement === 'left') {
            top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
            left = hostPos.left - tooltipPos.width - this.offset <= 0 ? hostPos.right + this.offset : hostPos.left - tooltipPos.width - this.offset;
            if (hostPos.left - tooltipPos.width - this.offset <= 0) {
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-left');
                this._renderer.addClass(this.tooltip, 'bs-tooltip-right');
            }
            this._renderer.setStyle(this.arrow, 'top', `${(tooltipHeight / 2) - 6}px`);
        }
        if (this.placement === 'right') {
            top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
            left = hostPos.right + this.offset;
            if (viwportSize <= 768 && viwportSize > 480 && hostPos.x > (viwportSize / 3)) {
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-right');
                this._renderer.addClass(this.tooltip, 'bs-tooltip-left');
                left = hostPos.left - tooltipPos.width - this.offset;
                this._renderer.setStyle(this.arrow, 'top', `${(tooltipHeight / 2) - 6}px`);
            }
            else {
                this._renderer.setStyle(this.arrow, 'top', `${(tooltipHeight / 2) - 6}px`);
            }
            if (viwportSize <= 480 && hostPos.x > (viwportSize / 3) || (left * 100) / viwportSize > 90) {
                top = hostPos.bottom + this.offset;
                left = (hostPos.left + (hostPos.width - tooltipPos.width) / 2) >= 0 ? hostPos.left + (hostPos.width - tooltipPos.width) / 2 : 15;
                let arrowPositionX = (tooltipWidth / 2) - 6;
                let tootipPositionX = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-right');
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-left');
                this._renderer.addClass(this.tooltip, 'bs-tooltip-bottom');
                this._renderer.setStyle(this.arrow, 'top', 0);
                this._renderer.setStyle(this.arrow, 'left', `${tootipPositionX < 0 ? arrowPositionX - Math.abs(tootipPositionX) - 15 : arrowPositionX}px`);
            }
        }
        this._renderer.setStyle(this.tooltip, 'top', `${top + scrollPos}px`);
        this._renderer.setStyle(this.tooltip, 'left', `${left}px`);
    }
    ngOnDestroy() {
        if (this.tooltip) {
            this.hide();
        }
    }
}
/** @nocollapse */ TooltipDirective.ɵfac = function TooltipDirective_Factory(t) { return new (t || TooltipDirective)(i0.ɵɵdirectiveInject(i0.ElementRef), i0.ɵɵdirectiveInject(i0.Renderer2)); };
/** @nocollapse */ TooltipDirective.ɵdir = /** @pureOrBreakMyCode */ i0.ɵɵdefineDirective({ type: TooltipDirective, selectors: [["", "id-tooltip", ""]], hostBindings: function TooltipDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("mouseenter", function TooltipDirective_mouseenter_HostBindingHandler() { return ctx.onMouseEnter(); })("mouseleave", function TooltipDirective_mouseleave_HostBindingHandler() { return ctx.onMouseLeave(); });
    } }, inputs: { tooltipTex: ["text", "tooltipTex"], placement: "placement" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(TooltipDirective, [{
        type: Directive,
        args: [{
                selector: '[id-tooltip]'
            }]
    }], function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }]; }, { tooltipTex: [{
            type: Input,
            args: ['text']
        }], placement: [{
            type: Input
        }], onMouseEnter: [{
            type: HostListener,
            args: ['mouseenter']
        }], onMouseLeave: [{
            type: HostListener,
            args: ['mouseleave']
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiQzovVXNlcnMvVXN1YXJpby9EZXNrdG9wL2xpYnJlcmlhbmd1bGFyLTI5LTEwLTIxL2NvcGlhLWFuZ3VsYXItbGliLXVwZC0xMi1zaW4tcnV0L3Byb2plY3RzL2lzYXByZS1kaWdpdGFsL3NyYy8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvdG9vbHRpcC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQWEsS0FBSyxFQUF5QixNQUFNLGVBQWUsQ0FBQzs7QUFLakcsTUFBTSxPQUFPLGdCQUFnQjtJQU8zQixZQUNVLEdBQWUsRUFDZixTQUFvQjtRQURwQixRQUFHLEdBQUgsR0FBRyxDQUFZO1FBQ2YsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUo5QixXQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ1gsa0JBQWEsR0FBRyxHQUFHLENBQUM7SUFJaEIsQ0FBQztJQUV1QixZQUFZO1FBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQUU7SUFDckMsQ0FBQztJQUUyQixZQUFZO1FBQ3RDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUFFO0lBQ3BDLENBQUM7SUFFRCxJQUFJO1FBQ0YsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUNELElBQUk7UUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ2pELE1BQU0sQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3hELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuRCxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQ3RMLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFFLENBQUM7UUFFbEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxjQUFjLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxXQUFXO1FBQ1QsTUFBTSxXQUFXLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUN0QyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQy9ELE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUN4RCwyQkFBMkI7UUFDM0IsTUFBTSxZQUFZLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQztRQUN0QyxpQ0FBaUM7UUFDakMsTUFBTSxhQUFhLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQztRQUN4QyxNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQztRQUMzRyxJQUFJLEdBQVcsRUFBRSxJQUFZLENBQUM7UUFFOUIsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBRTtZQUM1QixHQUFHLEdBQUcsT0FBTyxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDcEQsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ2pJLElBQUksY0FBYyxHQUFHLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxJQUFJLGVBQWUsR0FBRyxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEdBQUcsZUFBZSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxDQUFDO1lBQzNJLElBQUksT0FBTyxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0RCxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUNuQyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ2pJLElBQUksY0FBYyxHQUFHLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxlQUFlLEdBQUcsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO2dCQUM3RCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQzVELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztnQkFDM0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEdBQUcsZUFBZSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxDQUFDO2FBQzVJO1lBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsR0FBRyxXQUFXLEdBQUcsRUFBRSxFQUFFO2dCQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLGtCQUFrQixDQUFDLENBQUM7Z0JBQzdELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDO2dCQUM5QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLGFBQWEsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNyRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxHQUFHLFlBQVksR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ3ZFO1NBQ0Y7UUFFRCxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssUUFBUSxFQUFFO1lBQy9CLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDbkMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ2pJLElBQUksY0FBYyxHQUFHLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxJQUFJLGVBQWUsR0FBRyxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEdBQUcsZUFBZSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxDQUFDO1NBQzVJO1FBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLE1BQU0sRUFBRTtZQUM3QixHQUFHLEdBQUcsT0FBTyxDQUFDLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM3RCxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ3hJLElBQUssT0FBTyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFHO2dCQUN4RCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQzVELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzthQUMzRDtZQUNELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM1RTtRQUVELElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxPQUFPLEVBQUU7WUFDOUIsR0FBRyxHQUFHLE9BQU8sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDN0QsSUFBSSxHQUFHLE9BQU8sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNuQyxJQUFJLFdBQVcsSUFBSSxHQUFHLElBQUksV0FBVyxHQUFHLEdBQUcsSUFBSSxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUM1RSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLGtCQUFrQixDQUFDLENBQUM7Z0JBQzdELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztnQkFDekQsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUNyRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDNUU7aUJBQUk7Z0JBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzVFO1lBRUQsSUFBSSxXQUFXLElBQUksR0FBRyxJQUFJLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLEdBQUcsV0FBVyxHQUFHLEVBQUUsRUFBQztnQkFDekYsR0FBRyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDbkMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNqSSxJQUFJLGNBQWMsR0FBRyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzVDLElBQUksZUFBZSxHQUFHLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzVFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLG1CQUFtQixDQUFDLENBQUM7Z0JBQzNELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM5QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxHQUFHLGVBQWUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsY0FBYyxJQUFJLENBQUMsQ0FBQzthQUM1STtTQUNGO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxHQUFHLEdBQUcsU0FBUyxJQUFJLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FBRTtJQUNwQyxDQUFDOzttR0FySVUsZ0JBQWdCO2tHQUFoQixnQkFBZ0I7dUdBQWhCLGtCQUFjLHNGQUFkLGtCQUFjOzt1RkFBZCxnQkFBZ0I7Y0FINUIsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2FBQ3pCO3FGQUVnQixVQUFVO2tCQUF4QixLQUFLO21CQUFDLE1BQU07WUFHSixTQUFTO2tCQUFqQixLQUFLO1lBUXNCLFlBQVk7a0JBQXZDLFlBQVk7bUJBQUMsWUFBWTtZQUlFLFlBQVk7a0JBQXZDLFlBQVk7bUJBQUMsWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBSZW5kZXJlcjIsIElucHV0LCBFbGVtZW50UmVmLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gIHNlbGVjdG9yOiAnW2lkLXRvb2x0aXBdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9vbHRpcERpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XHJcbiAgQElucHV0KCd0ZXh0JykgdG9vbHRpcFRleDogc3RyaW5nO1xyXG4gIHRvb2x0aXA6IEhUTUxFbGVtZW50O1xyXG4gIGFycm93OiBIVE1MRWxlbWVudDtcclxuICBASW5wdXQoKSBwbGFjZW1lbnQ6IHN0cmluZztcclxuICBvZmZzZXQgPSA2O1xyXG4gIG1heENoYXJhY3RlcnMgPSAyMDA7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9lbDogRWxlbWVudFJlZixcclxuICAgIHByaXZhdGUgX3JlbmRlcmVyOiBSZW5kZXJlcjJcclxuICApIHsgfVxyXG5cclxuICBASG9zdExpc3RlbmVyKCdtb3VzZWVudGVyJykgb25Nb3VzZUVudGVyKCkge1xyXG4gICAgaWYgKCF0aGlzLnRvb2x0aXApIHsgdGhpcy5zaG93KCk7IH1cclxuICB9XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ21vdXNlbGVhdmUnKSBvbk1vdXNlTGVhdmUoKSB7XHJcbiAgICBpZiAodGhpcy50b29sdGlwKSB7IHRoaXMuaGlkZSgpOyB9XHJcbiAgfVxyXG5cclxuICBzaG93KCkge1xyXG4gICAgdGhpcy5jcmVhdGUoKTtcclxuICAgIHRoaXMuc2V0UG9zaXRpb24oKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMudG9vbHRpcCwgJ3Nob3cnKTtcclxuICB9XHJcbiAgaGlkZSgpIHtcclxuICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMudG9vbHRpcCwgJ3Nob3cnKTtcclxuICAgIHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlQ2hpbGQoZG9jdW1lbnQuYm9keSwgdGhpcy50b29sdGlwKTtcclxuICAgICAgdGhpcy50b29sdGlwID0gbnVsbDtcclxuICAgIH0sIDApO1xyXG4gIH1cclxuXHJcbiAgY3JlYXRlKCkge1xyXG4gICAgdGhpcy50b29sdGlwID0gdGhpcy5fcmVuZGVyZXIuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICBjb25zdCBpbm5lciA9IHRoaXMuX3JlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgdGhpcy5hcnJvdyA9IHRoaXMuX3JlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy50b29sdGlwLCAndG9vbHRpcCcpO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3MoaW5uZXIsICd0b29sdGlwLWlubmVyJyk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLmFycm93LCAnYXJyb3cnKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFwcGVuZENoaWxkKGlubmVyLCB0aGlzLl9yZW5kZXJlci5jcmVhdGVUZXh0KCh0aGlzLnRvb2x0aXBUZXgubGVuZ3RoID4gdGhpcy5tYXhDaGFyYWN0ZXJzKSA/ICh0aGlzLnRvb2x0aXBUZXguc2xpY2UoMCwgdGhpcy5tYXhDaGFyYWN0ZXJzKSkgKyAnLi4uJzogdGhpcy50b29sdGlwVGV4KSk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5hcHBlbmRDaGlsZCh0aGlzLnRvb2x0aXAsIHRoaXMuYXJyb3cpO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuYXBwZW5kQ2hpbGQoIHRoaXMudG9vbHRpcCwgaW5uZXIgKTtcclxuXHJcbiAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLnRvb2x0aXAsIGBicy10b29sdGlwLSR7dGhpcy5wbGFjZW1lbnR9YCk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5hcHBlbmRDaGlsZChkb2N1bWVudC5ib2R5LCB0aGlzLnRvb2x0aXApO1xyXG4gIH1cclxuXHJcbiAgc2V0UG9zaXRpb24oKSB7XHJcbiAgICBjb25zdCB2aXdwb3J0U2l6ZSA9IHdpbmRvdy5vdXRlcldpZHRoO1xyXG4gICAgY29uc3QgaG9zdFBvcyA9IHRoaXMuX2VsLm5hdGl2ZUVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICBjb25zdCB0b29sdGlwUG9zID0gdGhpcy50b29sdGlwLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgLy8gY29uc29sZS5sb2codG9vbHRpcFBvcyk7XHJcbiAgICBjb25zdCB0b29sdGlwV2lkdGggPSB0b29sdGlwUG9zLndpZHRoO1xyXG4gICAgLy8gY29uc29sZS5sb2codG9vbHRpcFBvcy53aWR0aCk7XHJcbiAgICBjb25zdCB0b29sdGlwSGVpZ2h0ID0gdG9vbHRpcFBvcy5oZWlnaHQ7XHJcbiAgICBjb25zdCBzY3JvbGxQb3MgPSB3aW5kb3cucGFnZVlPZmZzZXQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCB8fCBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcCB8fCAwO1xyXG4gICAgbGV0IHRvcDogbnVtYmVyLCBsZWZ0OiBudW1iZXI7XHJcblxyXG4gICAgaWYgKHRoaXMucGxhY2VtZW50ID09PSAndG9wJykge1xyXG4gICAgICB0b3AgPSBob3N0UG9zLnRvcCAtIHRvb2x0aXBQb3MuaGVpZ2h0IC0gdGhpcy5vZmZzZXQ7XHJcbiAgICAgIGxlZnQgPSAoaG9zdFBvcy5sZWZ0ICsgKGhvc3RQb3Mud2lkdGggLSB0b29sdGlwUG9zLndpZHRoKSAvIDIpID49IDAgPyBob3N0UG9zLmxlZnQgKyAoaG9zdFBvcy53aWR0aCAtIHRvb2x0aXBQb3Mud2lkdGgpIC8gMiA6IDE1O1xyXG4gICAgICBsZXQgYXJyb3dQb3NpdGlvblggPSAodG9vbHRpcFdpZHRoIC8gMikgLSA2O1xyXG4gICAgICBsZXQgdG9vdGlwUG9zaXRpb25YID0gaG9zdFBvcy5sZWZ0ICsgKGhvc3RQb3Mud2lkdGggLSB0b29sdGlwUG9zLndpZHRoKSAvIDI7XHJcbiAgICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKHRoaXMuYXJyb3csICdsZWZ0JywgYCR7dG9vdGlwUG9zaXRpb25YIDwgMCA/IGFycm93UG9zaXRpb25YIC0gTWF0aC5hYnModG9vdGlwUG9zaXRpb25YKSAtIDE1IDogYXJyb3dQb3NpdGlvblh9cHhgKTtcclxuICAgICAgaWYgKGhvc3RQb3MudG9wIC0gdG9vbHRpcFBvcy5oZWlnaHQgLSB0aGlzLm9mZnNldCA8PSAwKSB7XHJcbiAgICAgICAgdG9wID0gaG9zdFBvcy5ib3R0b20gKyB0aGlzLm9mZnNldDtcclxuICAgICAgICBsZWZ0ID0gKGhvc3RQb3MubGVmdCArIChob3N0UG9zLndpZHRoIC0gdG9vbHRpcFBvcy53aWR0aCkgLyAyKSA+PSAwID8gaG9zdFBvcy5sZWZ0ICsgKGhvc3RQb3Mud2lkdGggLSB0b29sdGlwUG9zLndpZHRoKSAvIDIgOiAxNTtcclxuICAgICAgICBsZXQgYXJyb3dQb3NpdGlvblggPSAodG9vbHRpcFdpZHRoIC8gMikgLSA2O1xyXG4gICAgICAgIGxldCB0b290aXBQb3NpdGlvblggPSBob3N0UG9zLmxlZnQgKyAoaG9zdFBvcy53aWR0aCAtIHRvb2x0aXBQb3Mud2lkdGgpIC8gMjtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLnRvb2x0aXAsICdicy10b29sdGlwLXJpZ2h0Jyk7XHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy50b29sdGlwLCAnYnMtdG9vbHRpcC1sZWZ0Jyk7XHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy50b29sdGlwLCAnYnMtdG9vbHRpcC1ib3R0b20nKTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmFycm93LCAndG9wJywgMCk7XHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy5hcnJvdywgJ2xlZnQnLCBgJHt0b290aXBQb3NpdGlvblggPCAwID8gYXJyb3dQb3NpdGlvblggLSBNYXRoLmFicyh0b290aXBQb3NpdGlvblgpIC0gMTUgOiBhcnJvd1Bvc2l0aW9uWH1weGApO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICgobGVmdCAqIDEwMCkgLyB2aXdwb3J0U2l6ZSA+IDcwKSB7XHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy50b29sdGlwLCAnYnMtdG9vbHRpcC1yaWdodCcpO1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMudG9vbHRpcCwgJ2JzLXRvb2x0aXAtbGVmdCcpO1xyXG4gICAgICAgIGxlZnQgPSAoaG9zdFBvcy5sZWZ0ICsgMjApIC0gdG9vbHRpcFBvcy53aWR0aDtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmFycm93LCAndG9wJywgYCR7dG9vbHRpcEhlaWdodCAtIDZ9cHhgKTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmFycm93LCAnbGVmdCcsIGAke3Rvb2x0aXBXaWR0aCAtIDIwfXB4YCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5wbGFjZW1lbnQgPT09ICdib3R0b20nKSB7XHJcbiAgICAgIHRvcCA9IGhvc3RQb3MuYm90dG9tICsgdGhpcy5vZmZzZXQ7XHJcbiAgICAgIGxlZnQgPSAoaG9zdFBvcy5sZWZ0ICsgKGhvc3RQb3Mud2lkdGggLSB0b29sdGlwUG9zLndpZHRoKSAvIDIpID49IDAgPyBob3N0UG9zLmxlZnQgKyAoaG9zdFBvcy53aWR0aCAtIHRvb2x0aXBQb3Mud2lkdGgpIC8gMiA6IDE1O1xyXG4gICAgICBsZXQgYXJyb3dQb3NpdGlvblggPSAodG9vbHRpcFdpZHRoIC8gMikgLSA2O1xyXG4gICAgICBsZXQgdG9vdGlwUG9zaXRpb25YID0gaG9zdFBvcy5sZWZ0ICsgKGhvc3RQb3Mud2lkdGggLSB0b29sdGlwUG9zLndpZHRoKSAvIDI7XHJcbiAgICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKHRoaXMuYXJyb3csICdsZWZ0JywgYCR7dG9vdGlwUG9zaXRpb25YIDwgMCA/IGFycm93UG9zaXRpb25YIC0gTWF0aC5hYnModG9vdGlwUG9zaXRpb25YKSAtIDE1IDogYXJyb3dQb3NpdGlvblh9cHhgKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5wbGFjZW1lbnQgPT09ICdsZWZ0Jykge1xyXG4gICAgICB0b3AgPSBob3N0UG9zLnRvcCArIChob3N0UG9zLmhlaWdodCAtIHRvb2x0aXBQb3MuaGVpZ2h0KSAvIDI7XHJcbiAgICAgIGxlZnQgPSBob3N0UG9zLmxlZnQgLSB0b29sdGlwUG9zLndpZHRoIC0gdGhpcy5vZmZzZXQgPD0gMCA/IGhvc3RQb3MucmlnaHQgKyB0aGlzLm9mZnNldCA6IGhvc3RQb3MubGVmdCAtIHRvb2x0aXBQb3Mud2lkdGggLSB0aGlzLm9mZnNldDtcclxuICAgICAgaWYgKCBob3N0UG9zLmxlZnQgLSB0b29sdGlwUG9zLndpZHRoIC0gdGhpcy5vZmZzZXQgPD0gMCApIHtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLnRvb2x0aXAsICdicy10b29sdGlwLWxlZnQnKTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLnRvb2x0aXAsICdicy10b29sdGlwLXJpZ2h0Jyk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy5hcnJvdywgJ3RvcCcsIGAkeyh0b29sdGlwSGVpZ2h0IC8gMikgLSA2fXB4YCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMucGxhY2VtZW50ID09PSAncmlnaHQnKSB7XHJcbiAgICAgIHRvcCA9IGhvc3RQb3MudG9wICsgKGhvc3RQb3MuaGVpZ2h0IC0gdG9vbHRpcFBvcy5oZWlnaHQpIC8gMjtcclxuICAgICAgbGVmdCA9IGhvc3RQb3MucmlnaHQgKyB0aGlzLm9mZnNldDtcclxuICAgICAgaWYgKHZpd3BvcnRTaXplIDw9IDc2OCAmJiB2aXdwb3J0U2l6ZSA+IDQ4MCAmJiBob3N0UG9zLnggPiAodml3cG9ydFNpemUgLyAzKSkge1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMudG9vbHRpcCwgJ2JzLXRvb2x0aXAtcmlnaHQnKTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLnRvb2x0aXAsICdicy10b29sdGlwLWxlZnQnKTtcclxuICAgICAgICBsZWZ0ID0gaG9zdFBvcy5sZWZ0IC0gdG9vbHRpcFBvcy53aWR0aCAtIHRoaXMub2Zmc2V0O1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKHRoaXMuYXJyb3csICd0b3AnLCBgJHsodG9vbHRpcEhlaWdodCAvIDIpIC0gNn1weGApO1xyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmFycm93LCAndG9wJywgYCR7KHRvb2x0aXBIZWlnaHQgLyAyKSAtIDZ9cHhgKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHZpd3BvcnRTaXplIDw9IDQ4MCAmJiBob3N0UG9zLnggPiAodml3cG9ydFNpemUgLyAzKSB8fCAobGVmdCAqIDEwMCkgLyB2aXdwb3J0U2l6ZSA+IDkwKXtcclxuICAgICAgICB0b3AgPSBob3N0UG9zLmJvdHRvbSArIHRoaXMub2Zmc2V0O1xyXG4gICAgICAgIGxlZnQgPSAoaG9zdFBvcy5sZWZ0ICsgKGhvc3RQb3Mud2lkdGggLSB0b29sdGlwUG9zLndpZHRoKSAvIDIpID49IDAgPyBob3N0UG9zLmxlZnQgKyAoaG9zdFBvcy53aWR0aCAtIHRvb2x0aXBQb3Mud2lkdGgpIC8gMiA6IDE1O1xyXG4gICAgICAgIGxldCBhcnJvd1Bvc2l0aW9uWCA9ICh0b29sdGlwV2lkdGggLyAyKSAtIDY7XHJcbiAgICAgICAgbGV0IHRvb3RpcFBvc2l0aW9uWCA9IGhvc3RQb3MubGVmdCArIChob3N0UG9zLndpZHRoIC0gdG9vbHRpcFBvcy53aWR0aCkgLyAyO1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMudG9vbHRpcCwgJ2JzLXRvb2x0aXAtcmlnaHQnKTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLnRvb2x0aXAsICdicy10b29sdGlwLWxlZnQnKTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLnRvb2x0aXAsICdicy10b29sdGlwLWJvdHRvbScpO1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKHRoaXMuYXJyb3csICd0b3AnLCAwKTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmFycm93LCAnbGVmdCcsIGAke3Rvb3RpcFBvc2l0aW9uWCA8IDAgPyBhcnJvd1Bvc2l0aW9uWCAtIE1hdGguYWJzKHRvb3RpcFBvc2l0aW9uWCkgLSAxNSA6IGFycm93UG9zaXRpb25YfXB4YCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKHRoaXMudG9vbHRpcCwgJ3RvcCcsIGAke3RvcCArIHNjcm9sbFBvc31weGApO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy50b29sdGlwLCAnbGVmdCcsIGAke2xlZnR9cHhgKTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMudG9vbHRpcCkgeyB0aGlzLmhpZGUoKTsgfVxyXG4gIH1cclxufVxyXG4iXX0=