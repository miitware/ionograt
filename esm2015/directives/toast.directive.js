import { Directive, Input, HostListener, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export class ToastDirective {
    constructor(
    // private _el: ElementRef,
    _renderer) {
        this._renderer = _renderer;
        this.maxCharacters = 100;
        this.timeoutToast = undefined;
        this.idToast = new EventEmitter();
    }
    onClick(event, targetElement) {
        this.show();
        this.timer();
    }
    create() {
        this.toast = this._renderer.createElement('div');
        this._renderer.addClass(this.toast, 'alert');
        this._renderer.addClass(this.toast, 'alert-toast');
        this._renderer.addClass(this.toast, 'alert-dismissible');
        this._renderer.addClass(this.toast, `alert-${this.type}`);
        this._renderer.addClass(this.toast, 'fade');
        this._renderer.addClass(this.toast, 'show');
        const span = this._renderer.createElement('h6');
        this._renderer.appendChild(span, this._renderer.createText((this.toastText.length > this.maxCharacters) ? (this.toastText.slice(0, this.maxCharacters)) : this.toastText));
        this._renderer.appendChild(this.toast, span);
        this._renderer.setAttribute(this.toast, 'role', 'alert');
        const btn = this._renderer.createElement('button');
        this._renderer.addClass(btn, 'close');
        this._renderer.setAttribute(btn, 'type', 'button');
        this._renderer.listen(btn, 'click', () => {
            this._renderer.removeClass(this.toast, 'show');
            this._renderer.removeChild(document.body, this.toast);
            this.toast = null;
        });
        const icon = this._renderer.createElement('i');
        this._renderer.addClass(icon, 'material-icons');
        this._renderer.appendChild(icon, this._renderer.createText('close'));
        this._renderer.appendChild(btn, icon);
        this._renderer.appendChild(this.toast, btn);
        this._renderer.appendChild(document.body, this.toast);
    }
    show() {
        // console.log(document.querySelectorAll('.alert-toast'));
        // if (document.querySelectorAll('.alert-toast')) {
        //   document.querySelectorAll('.alert-toast').forEach(e => e.remove());
        // }
        if (this.toast != undefined) {
            this.hide();
        }
        this.create();
    }
    hide() {
        if (this.toast != undefined) {
            this._renderer.removeClass(this.toast, 'show');
            this._renderer.removeChild(document.body, this.toast);
            this.toast = undefined;
        }
    }
    timer() {
        window.clearTimeout(this.timeoutToast);
        this.timeoutToast = window.setTimeout(() => {
            this.hide();
        }, 5000);
    }
}
/** @nocollapse */ ToastDirective.ɵfac = function ToastDirective_Factory(t) { return new (t || ToastDirective)(i0.ɵɵdirectiveInject(i0.Renderer2)); };
/** @nocollapse */ ToastDirective.ɵdir = /** @pureOrBreakMyCode */ i0.ɵɵdefineDirective({ type: ToastDirective, selectors: [["", "id-toast", ""]], hostBindings: function ToastDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("click", function ToastDirective_click_HostBindingHandler($event) { return ctx.onClick($event, $event.target); });
    } }, inputs: { toastText: ["text", "toastText"], type: "type" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ToastDirective, [{
        type: Directive,
        args: [{
                selector: '[id-toast]'
            }]
    }], function () { return [{ type: i0.Renderer2 }]; }, { toastText: [{
            type: Input,
            args: ['text']
        }], type: [{
            type: Input
        }], onClick: [{
            type: HostListener,
            args: ['click', ['$event', '$event.target']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IkM6L1VzZXJzL1VzdWFyaW8vRGVza3RvcC9saWJyZXJpYW5ndWxhci0yOS0xMC0yMS9jb3BpYS1hbmd1bGFyLWxpYi11cGQtMTItc2luLXJ1dC9wcm9qZWN0cy9pc2FwcmUtZGlnaXRhbC9zcmMvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL3RvYXN0LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFhLE1BQU0sZUFBZSxDQUFDOztBQUt4RixNQUFNLE9BQU8sY0FBYztJQU96QjtJQUNFLDJCQUEyQjtJQUNuQixTQUFvQjtRQUFwQixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBTDlCLGtCQUFhLEdBQUcsR0FBRyxDQUFDO1FBQ3BCLGlCQUFZLEdBQUcsU0FBUyxDQUFDO1FBTWxCLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO0lBRHpDLENBQUM7SUFJRyxPQUFPLENBQUMsS0FBaUIsRUFBRSxhQUEwQjtRQUMxRCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDZixDQUFDO0lBQ0QsTUFBTTtRQUNKLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLFNBQVMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzVDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQzNLLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDekQsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUMsUUFBUSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztRQUNILE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRTVDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFDRCxJQUFJO1FBQ0YsMERBQTBEO1FBQzFELG1EQUFtRDtRQUNuRCx3RUFBd0U7UUFDeEUsSUFBSTtRQUNKLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxTQUFTLEVBQUU7WUFDM0IsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2I7UUFDRCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUNELElBQUk7UUFDRixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksU0FBUyxFQUFFO1lBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7U0FDeEI7SUFDSCxDQUFDO0lBQ0QsS0FBSztRQUNILE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDekMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2QsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ1gsQ0FBQzs7K0ZBcEVVLGNBQWM7Z0dBQWQsY0FBYztpR0FBZCxrQ0FDRjs7dUZBREUsY0FBYztjQUgxQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7YUFDdkI7NERBRWdCLFNBQVM7a0JBQXZCLEtBQUs7bUJBQUMsTUFBTTtZQUNKLElBQUk7a0JBQVosS0FBSztZQVlDLE9BQU87a0JBRGIsWUFBWTttQkFBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBJbnB1dCwgSG9zdExpc3RlbmVyLCBFdmVudEVtaXR0ZXIsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6ICdbaWQtdG9hc3RdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9hc3REaXJlY3RpdmUge1xyXG4gIEBJbnB1dCgndGV4dCcpIHRvYXN0VGV4dDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHR5cGU6IHN0cmluZztcclxuICB0b2FzdDogSFRNTEVsZW1lbnQ7XHJcbiAgbWF4Q2hhcmFjdGVycyA9IDEwMDtcclxuICB0aW1lb3V0VG9hc3QgPSB1bmRlZmluZWQ7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgLy8gcHJpdmF0ZSBfZWw6IEVsZW1lbnRSZWYsXHJcbiAgICBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyXHJcbiAgKSB7fVxyXG4gIHB1YmxpYyBpZFRvYXN0ID0gbmV3IEV2ZW50RW1pdHRlcjxPYmplY3Q+KCk7XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJywgWyckZXZlbnQnLCAnJGV2ZW50LnRhcmdldCddKVxyXG4gIHB1YmxpYyBvbkNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50LCB0YXJnZXRFbGVtZW50OiBIVE1MRWxlbWVudCk6IHZvaWQge1xyXG4gICAgdGhpcy5zaG93KCk7XHJcbiAgICB0aGlzLnRpbWVyKCk7XHJcbiAgfVxyXG4gIGNyZWF0ZSgpIHtcclxuICAgIHRoaXMudG9hc3QgPSB0aGlzLl9yZW5kZXJlci5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMudG9hc3QsICdhbGVydCcpO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy50b2FzdCwgJ2FsZXJ0LXRvYXN0Jyk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLnRvYXN0LCAnYWxlcnQtZGlzbWlzc2libGUnKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMudG9hc3QsIGBhbGVydC0ke3RoaXMudHlwZX1gKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMudG9hc3QsICdmYWRlJyk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLnRvYXN0LCAnc2hvdycpO1xyXG4gICAgY29uc3Qgc3BhbiA9IHRoaXMuX3JlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2g2Jyk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5hcHBlbmRDaGlsZChzcGFuLCB0aGlzLl9yZW5kZXJlci5jcmVhdGVUZXh0KCh0aGlzLnRvYXN0VGV4dC5sZW5ndGggPiB0aGlzLm1heENoYXJhY3RlcnMpID8gKHRoaXMudG9hc3RUZXh0LnNsaWNlKDAsIHRoaXMubWF4Q2hhcmFjdGVycykpIDogdGhpcy50b2FzdFRleHQpKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFwcGVuZENoaWxkKHRoaXMudG9hc3QsIHNwYW4pO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0QXR0cmlidXRlKHRoaXMudG9hc3QsICdyb2xlJywgJ2FsZXJ0Jyk7XHJcbiAgICBjb25zdCBidG4gPSB0aGlzLl9yZW5kZXJlci5jcmVhdGVFbGVtZW50KCdidXR0b24nKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKGJ0biwgJ2Nsb3NlJyk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRBdHRyaWJ1dGUoYnRuLCAndHlwZScsJ2J1dHRvbicpO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIubGlzdGVuKGJ0biwgJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLnRvYXN0LCAnc2hvdycpO1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDaGlsZChkb2N1bWVudC5ib2R5LCB0aGlzLnRvYXN0KTtcclxuICAgICAgdGhpcy50b2FzdCA9IG51bGw7XHJcbiAgICB9KTtcclxuICAgIGNvbnN0IGljb24gPSB0aGlzLl9yZW5kZXJlci5jcmVhdGVFbGVtZW50KCdpJyk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyhpY29uLCAnbWF0ZXJpYWwtaWNvbnMnKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFwcGVuZENoaWxkKGljb24sIHRoaXMuX3JlbmRlcmVyLmNyZWF0ZVRleHQoJ2Nsb3NlJykpO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuYXBwZW5kQ2hpbGQoYnRuLCBpY29uKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFwcGVuZENoaWxkKHRoaXMudG9hc3QsIGJ0bik7XHJcblxyXG4gICAgdGhpcy5fcmVuZGVyZXIuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuYm9keSwgdGhpcy50b2FzdCk7XHJcbiAgfVxyXG4gIHNob3coKSB7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuYWxlcnQtdG9hc3QnKSk7XHJcbiAgICAvLyBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmFsZXJ0LXRvYXN0JykpIHtcclxuICAgIC8vICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmFsZXJ0LXRvYXN0JykuZm9yRWFjaChlID0+IGUucmVtb3ZlKCkpO1xyXG4gICAgLy8gfVxyXG4gICAgaWYgKHRoaXMudG9hc3QgIT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5jcmVhdGUoKTtcclxuICB9XHJcbiAgaGlkZSgpIHtcclxuICAgIGlmICh0aGlzLnRvYXN0ICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLnRvYXN0LCAnc2hvdycpO1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDaGlsZChkb2N1bWVudC5ib2R5LCB0aGlzLnRvYXN0KTtcclxuICAgICAgdGhpcy50b2FzdCA9IHVuZGVmaW5lZDtcclxuICAgIH1cclxuICB9XHJcbiAgdGltZXIoKSB7XHJcbiAgICB3aW5kb3cuY2xlYXJUaW1lb3V0KHRoaXMudGltZW91dFRvYXN0KTtcclxuICAgIHRoaXMudGltZW91dFRvYXN0ID0gd2luZG93LnNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICB0aGlzLmhpZGUoKTtcclxuICAgIH0sIDUwMDApO1xyXG4gIH1cclxufVxyXG5cclxuIl19