import { Directive, Input, HostListener } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "../services/analytics.service";
export class AnaltyticsDirective {
    constructor(analyticsService) {
        this.analyticsService = analyticsService;
        this.typeEvent = 'click';
    }
    onClick() {
        this.analyticsService.eventRegister(this.categoryEvent, this.typeEvent, this.labelEvent);
    }
}
/** @nocollapse */ AnaltyticsDirective.ɵfac = function AnaltyticsDirective_Factory(t) { return new (t || AnaltyticsDirective)(i0.ɵɵdirectiveInject(i1.AnalyticsService)); };
/** @nocollapse */ AnaltyticsDirective.ɵdir = /** @pureOrBreakMyCode */ i0.ɵɵdefineDirective({ type: AnaltyticsDirective, selectors: [["", "id-analtytics", ""]], hostBindings: function AnaltyticsDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("click", function AnaltyticsDirective_click_HostBindingHandler() { return ctx.onClick(); });
    } }, inputs: { categoryEvent: "categoryEvent", labelEvent: "labelEvent", typeEvent: "typeEvent" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AnaltyticsDirective, [{
        type: Directive,
        args: [{
                selector: '[id-analtytics]'
            }]
    }], function () { return [{ type: i1.AnalyticsService }]; }, { categoryEvent: [{
            type: Input
        }], labelEvent: [{
            type: Input
        }], typeEvent: [{
            type: Input
        }], onClick: [{
            type: HostListener,
            args: ['click']
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5hbHR5dGljcy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiQzovVXNlcnMvVXN1YXJpby9EZXNrdG9wL2xpYnJlcmlhbmd1bGFyLTI5LTEwLTIxL2NvcGlhLWFuZ3VsYXItbGliLXVwZC0xMi1zaW4tcnV0L3Byb2plY3RzL2lzYXByZS1kaWdpdGFsL3NyYy8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvYW5hbHR5dGljcy5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7QUFNL0QsTUFBTSxPQUFPLG1CQUFtQjtJQUs5QixZQUNVLGdCQUFrQztRQUFsQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBSG5DLGNBQVMsR0FBRyxPQUFPLENBQUM7SUFJekIsQ0FBQztJQUVrQixPQUFPO1FBQzVCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzRixDQUFDOzt5R0FYVSxtQkFBbUI7cUdBQW5CLG1CQUFtQjtnR0FBbkIsYUFBUzs7dUZBQVQsbUJBQW1CO2NBSC9CLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2FBQzVCO21FQUVVLGFBQWE7a0JBQXJCLEtBQUs7WUFDRyxVQUFVO2tCQUFsQixLQUFLO1lBQ0csU0FBUztrQkFBakIsS0FBSztZQU1pQixPQUFPO2tCQUE3QixZQUFZO21CQUFDLE9BQU8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIElucHV0LCBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQW5hbHl0aWNzU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FuYWx5dGljcy5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gIHNlbGVjdG9yOiAnW2lkLWFuYWx0eXRpY3NdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQW5hbHR5dGljc0RpcmVjdGl2ZSB7XHJcbiAgQElucHV0KCkgY2F0ZWdvcnlFdmVudDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGxhYmVsRXZlbnQ6IHN0cmluZztcclxuICBASW5wdXQoKSB0eXBlRXZlbnQgPSAnY2xpY2snO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgYW5hbHl0aWNzU2VydmljZTogQW5hbHl0aWNzU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJykgb25DbGljaygpIHtcclxuICAgIHRoaXMuYW5hbHl0aWNzU2VydmljZS5ldmVudFJlZ2lzdGVyKHRoaXMuY2F0ZWdvcnlFdmVudCwgdGhpcy50eXBlRXZlbnQsIHRoaXMubGFiZWxFdmVudCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=