import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import * as _rut from 'rut-helpers';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "ngx-swiper-wrapper";
import * as i3 from "../../directives/tooltip.directive";
const _c0 = ["c"];
function CarouselComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "div", 13);
} }
function CarouselComponent_div_1_div_5_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 25);
    i0.ɵɵelementStart(1, "i", 11);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function CarouselComponent_div_1_div_5_ng_container_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelement(1, "img", 26);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const beneficiary_r9 = ctx.$implicit;
    const ctx_r7 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("src", beneficiary_r9.avatar == null || beneficiary_r9.avatar == "" ? ctx_r7.defaultAvatar : beneficiary_r9.avatar, i0.ɵɵsanitizeUrl)("alt", beneficiary_r9.name);
} }
function CarouselComponent_div_1_div_5_div_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 27);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" +", ctx_r8.beneficiaries.length - ctx_r8.quantityAvatarOnFamilyCard, " ");
} }
const _c1 = function (a0) { return { "m-center": a0 }; };
function CarouselComponent_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r11 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 13);
    i0.ɵɵelementStart(1, "div", 14);
    i0.ɵɵelementStart(2, "div", 15);
    i0.ɵɵlistener("click", function CarouselComponent_div_1_div_5_Template_div_click_2_listener() { i0.ɵɵrestoreView(_r11); const ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.selectBeneficiary(); });
    i0.ɵɵtemplate(3, CarouselComponent_div_1_div_5_span_3_Template, 3, 0, "span", 16);
    i0.ɵɵelementStart(4, "div", 17);
    i0.ɵɵelementStart(5, "div", 18);
    i0.ɵɵelementStart(6, "div", 19);
    i0.ɵɵtemplate(7, CarouselComponent_div_1_div_5_ng_container_7_Template, 2, 2, "ng-container", 20);
    i0.ɵɵpipe(8, "slice");
    i0.ɵɵtemplate(9, CarouselComponent_div_1_div_5_div_9_Template, 2, 1, "div", 21);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 22);
    i0.ɵɵelementStart(11, "h6", 23);
    i0.ɵɵtext(12, "Grupo Familiar");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "small", 24);
    i0.ɵɵtext(14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r3.beneficiarySelected == null ? null : ctx_r3.beneficiarySelected.familyGroup);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(9, _c1, ctx_r3.beneficiaries.length > 2));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", i0.ɵɵpipeBind3(8, 5, ctx_r3.beneficiaries, 0, ctx_r3.quantityAvatarOnFamilyCard));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r3.beneficiaries.length > ctx_r3.quantityAvatarOnFamilyCard);
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate1("", ctx_r3.beneficiaries.length, " beneficiarios");
} }
function CarouselComponent_div_1_div_6_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 35);
    i0.ɵɵelementStart(1, "small");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.label);
} }
function CarouselComponent_div_1_div_6_span_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 35);
    i0.ɵɵelementStart(1, "small", 36);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.label);
} }
function CarouselComponent_div_1_div_6_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 25);
    i0.ɵɵelementStart(1, "i", 11);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function CarouselComponent_div_1_div_6_h6_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "h6", 37);
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "slice");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵpropertyInterpolate("text", beneficiary_r12.name);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(beneficiary_r12.name.length > 20 ? i0.ɵɵpipeBind3(2, 2, beneficiary_r12.name, 0, 20) + "..." : beneficiary_r12.name);
} }
function CarouselComponent_div_1_div_6_h6_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "h6", 23);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(beneficiary_r12.name);
} }
const _c2 = function (a0) { return { "deciduous": a0 }; };
function CarouselComponent_div_1_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r24 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 13);
    i0.ɵɵelementStart(1, "div", 14);
    i0.ɵɵelementStart(2, "div", 28);
    i0.ɵɵlistener("click", function CarouselComponent_div_1_div_6_Template_div_click_2_listener() { const restoredCtx = i0.ɵɵrestoreView(_r24); const beneficiary_r12 = restoredCtx.$implicit; const i_r13 = restoredCtx.index; const ctx_r23 = i0.ɵɵnextContext(2); return ctx_r23.selectBeneficiary(beneficiary_r12, i_r13); });
    i0.ɵɵtemplate(3, CarouselComponent_div_1_div_6_span_3_Template, 3, 1, "span", 29);
    i0.ɵɵtemplate(4, CarouselComponent_div_1_div_6_span_4_Template, 3, 1, "span", 29);
    i0.ɵɵtemplate(5, CarouselComponent_div_1_div_6_span_5_Template, 3, 0, "span", 16);
    i0.ɵɵelementStart(6, "div", 30);
    i0.ɵɵelementStart(7, "div", 31);
    i0.ɵɵelement(8, "img", 32);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "div", 22);
    i0.ɵɵtemplate(10, CarouselComponent_div_1_div_6_h6_10_Template, 3, 6, "h6", 33);
    i0.ɵɵtemplate(11, CarouselComponent_div_1_div_6_h6_11_Template, 2, 1, "h6", 34);
    i0.ɵɵelementStart(12, "small", 24);
    i0.ɵɵtext(13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = ctx.$implicit;
    const i_r13 = ctx.index;
    const ctx_r4 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(9, _c2, beneficiary_r12.deciduous));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", (beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.state) && (beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.acronym));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", (beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.state) && !(beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.acronym));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", beneficiary_r12.rut === (ctx_r4.beneficiarySelected == null ? null : ctx_r4.beneficiarySelected.beneficiary == null ? null : ctx_r4.beneficiarySelected.beneficiary.rut) && i_r13 === (ctx_r4.beneficiarySelected == null ? null : ctx_r4.beneficiarySelected.index));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("src", beneficiary_r12.avatar == null || beneficiary_r12.avatar == "" ? ctx_r4.defaultAvatar : beneficiary_r12.avatar, i0.ɵɵsanitizeUrl)("alt", beneficiary_r12.name);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", beneficiary_r12.name.length > 20);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", beneficiary_r12.name.length <= 20);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(beneficiary_r12.rutcomplete);
} }
function CarouselComponent_div_1_ng_container_7_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "div", 13);
} }
function CarouselComponent_div_1_ng_container_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, CarouselComponent_div_1_ng_container_7_div_1_Template, 1, 0, "div", 7);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r5.ghostCards);
} }
const _c3 = function (a0, a1) { return { "next": a0, "prev": a1 }; };
function CarouselComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r28 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 2);
    i0.ɵɵelementStart(1, "div", 3, 4);
    i0.ɵɵlistener("indexChange", function CarouselComponent_div_1_Template_div_indexChange_1_listener() { i0.ɵɵrestoreView(_r28); const ctx_r27 = i0.ɵɵnextContext(); return ctx_r27.getSwiperState(); })("reachEnd", function CarouselComponent_div_1_Template_div_reachEnd_1_listener() { i0.ɵɵrestoreView(_r28); const ctx_r29 = i0.ɵɵnextContext(); return ctx_r29.swiperNextEnd(); })("reachBeginning", function CarouselComponent_div_1_Template_div_reachBeginning_1_listener() { i0.ɵɵrestoreView(_r28); const ctx_r30 = i0.ɵɵnextContext(); return ctx_r30.swiperPrevEnd(); });
    i0.ɵɵelementStart(3, "div", 5);
    i0.ɵɵtemplate(4, CarouselComponent_div_1_div_4_Template, 1, 0, "div", 6);
    i0.ɵɵtemplate(5, CarouselComponent_div_1_div_5_Template, 15, 11, "div", 6);
    i0.ɵɵtemplate(6, CarouselComponent_div_1_div_6_Template, 14, 11, "div", 7);
    i0.ɵɵtemplate(7, CarouselComponent_div_1_ng_container_7_Template, 2, 1, "ng-container", 8);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(8, "div", 9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "div", 10);
    i0.ɵɵelementStart(10, "i", 11);
    i0.ɵɵtext(11, " navigate_before ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(12, "div", 12);
    i0.ɵɵelementStart(13, "i", 11);
    i0.ɵɵtext(14, " navigate_next ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("swiper", ctx_r0.swiperConfig)("ngClass", i0.ɵɵpureFunction2(6, _c3, ctx_r0.swiperNext, ctx_r0.swiperPrev));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r0.beneficiaries.length >= 4 && !ctx_r0.mobile && ctx_r0.ui.familyGroup || ctx_r0.beneficiaries.length >= 5 && !ctx_r0.mobile);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r0.ui.familyGroup);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r0.beneficiaries);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r0.beneficiaries.length >= 4 && !ctx_r0.mobile && ctx_r0.ui.familyGroup || ctx_r0.beneficiaries.length >= 5 && !ctx_r0.mobile);
} }
export class CarouselComponent {
    constructor(renderer, el) {
        this.renderer = renderer;
        this.el = el;
        // var
        this.ui = {
            familyGroup: false,
        };
        this.dataEntry = [];
        this.dataOutput = new EventEmitter();
        this.defaultAvatar = 'http://mxdev.cl/banmedica/img/avatars-vidatres/Avatar_v3_67.svg';
        this.quantityAvatarOnFamilyCard = 3;
        this.beneficiaries = [];
        this.swiperNext = true;
        this.swiperPrev = false;
        this.mobile = false;
        this.cardsmultiple = 5;
        this.ghostCards = [];
        this.swiperConfig = {
            direction: 'horizontal',
            observer: true,
            slidesPerView: 5,
            slidesPerGroup: 3,
            mousewheel: false,
            scrollbar: false,
            navigation: true,
            simulateTouch: true,
            watchOverflow: true,
            // centeredSlides: true,
            // spaceBetween: 0,
            speed: 1000,
            // initialSlide:1,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
            breakpoints: {
                560: {
                    slidesPerView: 'auto',
                    centeredSlides: true,
                    slidesPerGroup: 1,
                    spaceBetween: 0,
                },
                850: {
                    slidesPerView: 3,
                    slidesPerGroup: 1
                },
                1200: {
                    slidesPerView: 3,
                    slidesPerGroup: 1
                },
                1400: {
                    slidesPerView: 4,
                    slidesPerGroup: 2
                },
            }
        };
    }
    ngOnInit() {
        this.setCardsLength();
        if (window.screen.width <= 560) {
            this.mobile = true;
        }
        if (!this.dataEntry) {
            console.warn('No se ha encontrado la data de beneficiarios en la directiva dataEntry');
        }
        for (const beneficiary of this.dataEntry) {
            if (beneficiary.rut !== null) {
                beneficiary.rutcomplete = `${beneficiary.rut}-${beneficiary.dv}`;
                if (_rut.rutValidate(beneficiary.rutcomplete)) {
                    this.beneficiaries.push(beneficiary);
                }
                else {
                    console.warn(`El Rut ${beneficiary.rutcomplete} no es válido`);
                }
            }
            else {
                this.beneficiaries.push(beneficiary);
            }
        }
    }
    // methods
    selectBeneficiary(beneficiary, index) {
        if (beneficiary) {
            this.beneficiarySelected = { beneficiary, index, familyGroup: false };
            this.dataOutput.emit(this.beneficiarySelected);
        }
        else {
            this.beneficiarySelected = { familyGroup: true };
            this.dataOutput.emit(this.beneficiarySelected);
        }
    }
    getSwiperState() {
        this.swiperNext = true;
        this.swiperPrev = true;
    }
    swiperNextEnd() {
        setTimeout(() => {
            this.swiperNext = false;
            this.swiperPrev = true;
        }, 100);
    }
    swiperPrevEnd() {
        setTimeout(() => {
            this.swiperNext = true;
            this.swiperPrev = false;
        }, 100);
    }
    setCardsLength() {
        const currentCard = this.ui.familyGroup ? 2 : 1;
        const cardsBeneficiaries = this.dataEntry.length;
        const totalCards = currentCard + cardsBeneficiaries;
        const missingCards = totalCards >= 5 ? 2 - (totalCards % 3) : 0;
        console.log(missingCards);
        if (totalCards <= 5) {
            this.swiperConfig.slidesPerView = 4;
            this.swiperNext = false;
        }
        if (totalCards > 5) {
            this.swiperConfig.slidesPerView = 5;
        }
        for (let i = 0; i < missingCards; i++) {
            this.ghostCards.push({});
        }
    }
}
/** @nocollapse */ CarouselComponent.ɵfac = function CarouselComponent_Factory(t) { return new (t || CarouselComponent)(i0.ɵɵdirectiveInject(i0.Renderer2), i0.ɵɵdirectiveInject(i0.ElementRef)); };
/** @nocollapse */ CarouselComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: CarouselComponent, selectors: [["id-carousel"]], viewQuery: function CarouselComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.swiper = _t.first);
    } }, inputs: { ui: "ui", dataEntry: "dataEntry" }, outputs: { dataOutput: "dataOutput" }, decls: 2, vars: 1, consts: [[1, "id-carrusel"], ["class", "swipe-content", 4, "ngIf"], [1, "swipe-content"], [1, "swiper-container", 3, "swiper", "ngClass", "indexChange", "reachEnd", "reachBeginning"], ["swiper", ""], [1, "swiper-wrapper", "pb-2"], ["class", "swiper-slide pt-4 pb-4", 4, "ngIf"], ["class", "swiper-slide pt-4 pb-4", 4, "ngFor", "ngForOf"], [4, "ngIf"], [1, "swiper-pagination"], [1, "swiper-button-prev"], [1, "material-icons"], [1, "swiper-button-next"], [1, "swiper-slide", "pt-4", "pb-4"], [1, "col"], [1, "card", "shadow-sm", "card-family-group", 3, "click"], ["class", "badge badge-success", 4, "ngIf"], [1, "avatar-group"], [1, "m-auto", "d-flex", "position-relative"], [1, "d-flex", 3, "ngClass"], [4, "ngFor", "ngForOf"], ["class", "card-img-group item-extra", 4, "ngIf"], [1, "card-body", "mt-3"], [1, "card-title"], [1, "card-subtitle"], [1, "badge", "badge-success"], [1, "card-img-top", "card-img-group", 3, "src", "alt"], [1, "card-img-group", "item-extra"], [1, "card", "shadow-sm", 3, "ngClass", "click"], ["class", "tag", 4, "ngIf"], [1, "avatar-box"], [1, "avatar-card"], [1, "card-img-top", "avatar-card", 3, "src", "alt"], ["class", "card-title", "id-tooltip", "", "placement", "top", 3, "text", 4, "ngIf"], ["class", "card-title", 4, "ngIf"], [1, "tag"], [1, "capital"], ["id-tooltip", "", "placement", "top", 1, "card-title", 3, "text"]], template: function CarouselComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, CarouselComponent_div_1_Template, 15, 9, "div", 1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.dataEntry);
    } }, directives: [i1.NgIf, i2.SwiperDirective, i1.NgClass, i1.NgForOf, i3.TooltipDirective], pipes: [i1.SlicePipe], styles: [".swiper-container[_ngcontent-%COMP%]{list-style:none;margin-left:auto;margin-right:auto;overflow:hidden;padding:0;position:relative;z-index:1}.swiper-container-no-flexbox[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{float:left}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{flex-direction:column}.swiper-wrapper[_ngcontent-%COMP%]{box-sizing:content-box;display:flex;height:100%;position:relative;transition-property:transform;width:100%;z-index:1}.swiper-container-android[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%], .swiper-wrapper[_ngcontent-%COMP%]{transform:translateZ(0)}.swiper-container-multirow[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{flex-wrap:wrap}.swiper-container-free-mode[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{margin:0 auto;transition-timing-function:ease-out}.swiper-slide[_ngcontent-%COMP%]{flex-shrink:0;height:100%;position:relative;transition-property:transform;width:100%}.swiper-slide-invisible-blank[_ngcontent-%COMP%]{visibility:hidden}.swiper-container-autoheight[_ngcontent-%COMP%], .swiper-container-autoheight[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{height:auto}.swiper-container-autoheight[_ngcontent-%COMP%]   .swiper-wrapper[_ngcontent-%COMP%]{align-items:flex-start;transition-property:transform,height}.swiper-container-3d[_ngcontent-%COMP%]{perspective:1200px}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-cube-shadow[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-wrapper[_ngcontent-%COMP%]{transform-style:preserve-3d}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%]{height:100%;left:0;pointer-events:none;position:absolute;top:0;width:100%;z-index:10}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%]{background-image:linear-gradient(270deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%]{background-image:linear-gradient(90deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%]{background-image:linear-gradient(0deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%]{background-image:linear-gradient(180deg,rgba(0,0,0,.5),transparent)}.swiper-container-wp8-horizontal[_ngcontent-%COMP%], .swiper-container-wp8-horizontal[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{touch-action:pan-y}.swiper-container-wp8-vertical[_ngcontent-%COMP%], .swiper-container-wp8-vertical[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{touch-action:pan-x}.swiper-button-next[_ngcontent-%COMP%], .swiper-button-prev[_ngcontent-%COMP%]{background-position:50%;background-repeat:no-repeat;background-size:27px 44px;cursor:pointer;height:44px;margin-top:-22px;position:absolute;top:50%;width:27px;z-index:10}.swiper-button-next.swiper-button-disabled[_ngcontent-%COMP%], .swiper-button-prev.swiper-button-disabled[_ngcontent-%COMP%]{cursor:auto;opacity:.35;pointer-events:none}.swiper-button-prev[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E\");left:10px;right:auto}.swiper-button-next[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E\");left:auto;right:10px}.swiper-button-prev.swiper-button-white[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next.swiper-button-white[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E\")}.swiper-button-next.swiper-button-white[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev.swiper-button-white[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E\")}.swiper-button-prev.swiper-button-black[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next.swiper-button-black[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E\")}.swiper-button-next.swiper-button-black[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev.swiper-button-black[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E\")}.swiper-button-lock[_ngcontent-%COMP%]{display:none}.swiper-pagination[_ngcontent-%COMP%]{position:absolute;text-align:center;transform:translateZ(0);transition:opacity .3s;z-index:10}.swiper-pagination.swiper-pagination-hidden[_ngcontent-%COMP%]{opacity:0}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-bullets[_ngcontent-%COMP%], .swiper-pagination-custom[_ngcontent-%COMP%], .swiper-pagination-fraction[_ngcontent-%COMP%]{bottom:10px;left:0;width:100%}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]{font-size:0;overflow:hidden}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{position:relative;transform:scale(.33)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active[_ngcontent-%COMP%], .swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-main[_ngcontent-%COMP%]{transform:scale(1)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-prev[_ngcontent-%COMP%]{transform:scale(.66)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-prev-prev[_ngcontent-%COMP%]{transform:scale(.33)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-next[_ngcontent-%COMP%]{transform:scale(.66)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-next-next[_ngcontent-%COMP%]{transform:scale(.33)}.swiper-pagination-bullet[_ngcontent-%COMP%]{background:#000;border-radius:100%;display:inline-block;height:8px;opacity:.2;width:8px}button.swiper-pagination-bullet[_ngcontent-%COMP%]{-moz-appearance:none;-webkit-appearance:none;appearance:none;border:none;box-shadow:none;margin:0;padding:0}.swiper-pagination-clickable[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{cursor:pointer}.swiper-pagination-bullet-active[_ngcontent-%COMP%]{background:#007aff;opacity:1}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-bullets[_ngcontent-%COMP%]{right:10px;top:50%;transform:translate3d(0,-50%,0)}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-bullets[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{display:block;margin:6px 0}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]{top:50%;transform:translateY(-50%);width:8px}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{display:inline-block;transition:top .2s,-webkit-transform .2s;transition:transform .2s,top .2s;transition:transform .2s,top .2s,-webkit-transform .2s}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-bullets[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{margin:0 4px}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]{left:50%;transform:translateX(-50%);white-space:nowrap}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{transition:left .2s,-webkit-transform .2s;transition:transform .2s,left .2s;transition:transform .2s,left .2s,-webkit-transform .2s}.swiper-container-horizontal.swiper-container-rtl[_ngcontent-%COMP%] > .swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{transition:right .2s,-webkit-transform .2s;transition:transform .2s,right .2s;transition:transform .2s,right .2s,-webkit-transform .2s}.swiper-pagination-progressbar[_ngcontent-%COMP%]{background:rgba(0,0,0,.25);position:absolute}.swiper-pagination-progressbar[_ngcontent-%COMP%]   .swiper-pagination-progressbar-fill[_ngcontent-%COMP%]{background:#007aff;height:100%;left:0;position:absolute;top:0;transform:scale(0);transform-origin:left top;width:100%}.swiper-container-rtl[_ngcontent-%COMP%]   .swiper-pagination-progressbar[_ngcontent-%COMP%]   .swiper-pagination-progressbar-fill[_ngcontent-%COMP%]{transform-origin:right top}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-progressbar[_ngcontent-%COMP%], .swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-progressbar.swiper-pagination-progressbar-opposite[_ngcontent-%COMP%]{height:4px;left:0;top:0;width:100%}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-progressbar.swiper-pagination-progressbar-opposite[_ngcontent-%COMP%], .swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-progressbar[_ngcontent-%COMP%]{height:100%;left:0;top:0;width:4px}.swiper-pagination-white[_ngcontent-%COMP%]   .swiper-pagination-bullet-active[_ngcontent-%COMP%]{background:#fff}.swiper-pagination-progressbar.swiper-pagination-white[_ngcontent-%COMP%]{background:hsla(0,0%,100%,.25)}.swiper-pagination-progressbar.swiper-pagination-white[_ngcontent-%COMP%]   .swiper-pagination-progressbar-fill[_ngcontent-%COMP%]{background:#fff}.swiper-pagination-black[_ngcontent-%COMP%]   .swiper-pagination-bullet-active[_ngcontent-%COMP%]{background:#000}.swiper-pagination-progressbar.swiper-pagination-black[_ngcontent-%COMP%]{background:rgba(0,0,0,.25)}.swiper-pagination-progressbar.swiper-pagination-black[_ngcontent-%COMP%]   .swiper-pagination-progressbar-fill[_ngcontent-%COMP%]{background:#000}.swiper-pagination-lock[_ngcontent-%COMP%]{display:none}.swiper-scrollbar[_ngcontent-%COMP%]{-ms-touch-action:none;background:rgba(0,0,0,.1);border-radius:10px;position:relative}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-scrollbar[_ngcontent-%COMP%]{bottom:3px;height:5px;left:1%;position:absolute;width:98%;z-index:50}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-scrollbar[_ngcontent-%COMP%]{height:98%;position:absolute;right:3px;top:1%;width:5px;z-index:50}.swiper-scrollbar-drag[_ngcontent-%COMP%]{background:rgba(0,0,0,.5);border-radius:10px;height:100%;left:0;position:relative;top:0;width:100%}.swiper-scrollbar-cursor-drag[_ngcontent-%COMP%]{cursor:move}.swiper-scrollbar-lock[_ngcontent-%COMP%]{display:none}.swiper-zoom-container[_ngcontent-%COMP%]{align-items:center;display:flex;height:100%;justify-content:center;text-align:center;width:100%}.swiper-zoom-container[_ngcontent-%COMP%] > canvas[_ngcontent-%COMP%], .swiper-zoom-container[_ngcontent-%COMP%] > img[_ngcontent-%COMP%], .swiper-zoom-container[_ngcontent-%COMP%] > svg[_ngcontent-%COMP%]{-o-object-fit:contain;max-height:100%;max-width:100%;object-fit:contain}.swiper-slide-zoomed[_ngcontent-%COMP%]{cursor:move}.swiper-lazy-preloader[_ngcontent-%COMP%]{-webkit-animation:swiper-preloader-spin 1s steps(12) infinite;animation:swiper-preloader-spin 1s steps(12) infinite;height:42px;left:50%;margin-left:-21px;margin-top:-21px;position:absolute;top:50%;transform-origin:50%;width:42px;z-index:10}.swiper-lazy-preloader[_ngcontent-%COMP%]:after{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%236c6c6c'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E\");background-position:50%;background-repeat:no-repeat;background-size:100%;content:\"\";display:block;height:100%;width:100%}.swiper-lazy-preloader-white[_ngcontent-%COMP%]:after{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%23fff'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E\")}@-webkit-keyframes swiper-preloader-spin{to{transform:rotate(1turn)}}@keyframes swiper-preloader-spin{to{transform:rotate(1turn)}}.swiper-container[_ngcontent-%COMP%]   .swiper-notification[_ngcontent-%COMP%]{left:0;opacity:0;pointer-events:none;position:absolute;top:0;z-index:-1000}.swiper-container-fade.swiper-container-free-mode[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{transition-timing-function:ease-out}.swiper-container-fade[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{pointer-events:none;transition-property:opacity}.swiper-container-fade[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{pointer-events:none}.swiper-container-fade[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%], .swiper-container-fade[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]{pointer-events:auto}.swiper-container-cube[_ngcontent-%COMP%]{overflow:visible}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{-webkit-backface-visibility:hidden;backface-visibility:hidden;height:100%;pointer-events:none;transform-origin:0 0;visibility:hidden;width:100%;z-index:1}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{pointer-events:none}.swiper-container-cube.swiper-container-rtl[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{transform-origin:100% 0}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]{pointer-events:auto}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-next[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-next[_ngcontent-%COMP%] + .swiper-slide[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-prev[_ngcontent-%COMP%]{pointer-events:auto;visibility:visible}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%]{-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:0}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-cube-shadow[_ngcontent-%COMP%]{background:#000;bottom:0;filter:blur(50px);height:100%;left:0;opacity:.6;position:absolute;width:100%;z-index:0}.swiper-container-flip[_ngcontent-%COMP%]{overflow:visible}.swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{-webkit-backface-visibility:hidden;backface-visibility:hidden;pointer-events:none;z-index:1}.swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{pointer-events:none}.swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%], .swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]{pointer-events:auto}.swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%], .swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%], .swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%], .swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%]{-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:0}.swiper-container-coverflow[_ngcontent-%COMP%]   .swiper-wrapper[_ngcontent-%COMP%]{-ms-perspective:1200px}.swipe-content[_ngcontent-%COMP%]{position:relative}.swiper-slide[_ngcontent-%COMP%]{height:auto!important}.swiper-slide[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%], .swiper-slide[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]{height:100%}.swiper-button-next[_ngcontent-%COMP%], .swiper-button-prev[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev[_ngcontent-%COMP%]{background-image:none}.swiper-button-next[_ngcontent-%COMP%], .swiper-button-prev[_ngcontent-%COMP%]{-moz-user-select:none;-ms-user-select:none;-webkit-user-select:none;height:auto;user-select:none;width:auto}.swiper-button-next[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next[_ngcontent-%COMP%]{right:0}.swiper-button-next.swiper-button-disabled[_ngcontent-%COMP%]{right:-25px}.swiper-button-next[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next[_ngcontent-%COMP%]:focus{outline:0}.swiper-button-prev[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev[_ngcontent-%COMP%]{left:0}.swiper-button-prev[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev[_ngcontent-%COMP%]:focus{outline:0}.swiper-button-next[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%], .swiper-button-prev[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%]{font-size:35px}@media screen and (min-width:960px){.id-carrusel[_ngcontent-%COMP%]   .swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination.swiper-pagination-bullets.swiper-pagination-lock[_ngcontent-%COMP%]{display:none!important}}.id-carrusel[_ngcontent-%COMP%]   .card.deciduous[_ngcontent-%COMP%]   .card-subtitle[_ngcontent-%COMP%], .id-carrusel[_ngcontent-%COMP%]   .card.deciduous[_ngcontent-%COMP%]   .card-title[_ngcontent-%COMP%]{color:#767677!important}.id-carrusel[_ngcontent-%COMP%]   .card.deciduous[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%]{background-color:#757576!important}.id-carrusel[_ngcontent-%COMP%]   .card.deciduous[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%]:before{border-right:10px solid #757576!important}.id-carrusel[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%]{text-transform:inherit!important}.swiper-button-prev[_ngcontent-%COMP%]{transition:left .3s ease-in-out}.swiper-button-next[_ngcontent-%COMP%]{transition:right .3s ease-in-out}.swiper-container[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]{margin-left:auto;margin-right:auto;max-width:200px}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(CarouselComponent, [{
        type: Component,
        args: [{
                selector: 'id-carousel',
                templateUrl: './carousel.component.html',
                styleUrls: ['./carousel.component.css']
            }]
    }], function () { return [{ type: i0.Renderer2 }, { type: i0.ElementRef }]; }, { ui: [{
            type: Input
        }], dataEntry: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }], swiper: [{
            type: ViewChild,
            args: ['c', { static: false }]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2Fyb3VzZWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IkM6L1VzZXJzL1VzdWFyaW8vRGVza3RvcC9saWJyZXJpYW5ndWxhci0yOS0xMC0yMS9jb3BpYS1hbmd1bGFyLWxpYi11cGQtMTItc2luLXJ1dC9wcm9qZWN0cy9pc2FwcmUtZGlnaXRhbC9zcmMvIiwic291cmNlcyI6WyJsaWIvY2Fyb3VzZWwvY2Fyb3VzZWwuY29tcG9uZW50LnRzIiwibGliL2Nhcm91c2VsL2Nhcm91c2VsLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUF5QixNQUFNLGVBQWUsQ0FBQztBQUlqSCxPQUFPLEtBQUssSUFBSSxNQUFNLGFBQWEsQ0FBQzs7Ozs7OztJQ0FwQiwwQkFBNkk7OztJQUsvSSxnQ0FBMkU7SUFDbkUsNkJBQTBCO0lBQUEsNEJBQVk7SUFBQSxpQkFBSTtJQUMzQyxpQkFBTzs7O0lBSVIsNkJBQTZGO0lBQzNGLDBCQUFzSztJQUN4SywwQkFBZTs7OztJQURSLGVBQW1HO0lBQW5HLG1KQUFtRyw0QkFBQTs7O0lBRTFHLCtCQUFpRztJQUMvRixZQUNGO0lBQUEsaUJBQU07OztJQURKLGVBQ0Y7SUFERSxpR0FDRjs7Ozs7SUFkWiwrQkFBMkQ7SUFDekQsK0JBQWlCO0lBQ2YsK0JBQTRFO0lBQTlCLG1NQUE2QjtJQUN6RSxpRkFFYztJQUNkLCtCQUEwQjtJQUN4QiwrQkFBNkM7SUFDM0MsK0JBQXdFO0lBQ3RFLGlHQUVlOztJQUNmLCtFQUVNO0lBQ1IsaUJBQU07SUFDUixpQkFBTTtJQUNSLGlCQUFNO0lBQ04sZ0NBQTRCO0lBQzFCLCtCQUF1QjtJQUFBLCtCQUFjO0lBQUEsaUJBQUs7SUFDMUMsa0NBQTZCO0lBQUEsYUFBd0M7SUFBQSxpQkFBUTtJQUMvRSxpQkFBTTtJQUNSLGlCQUFNO0lBQ1IsaUJBQU07SUFDUixpQkFBTTs7O0lBckJtQyxlQUFzQztJQUF0Qyx5R0FBc0M7SUFLakQsZUFBbUQ7SUFBbkQscUZBQW1EO0lBQy9CLGVBQXFEO0lBQXJELDBHQUFxRDtJQUduRCxlQUF1RDtJQUF2RCxzRkFBdUQ7SUFRdEUsZUFBd0M7SUFBeEMsd0VBQXdDOzs7SUFTdkUsZ0NBQStFO0lBQ3ZFLDZCQUFPO0lBQUEsWUFBMkI7SUFBQSxpQkFBUTtJQUMzQyxpQkFBTzs7O0lBREMsZUFBMkI7SUFBM0IscUhBQTJCOzs7SUFFMUMsZ0NBQWdGO0lBQ3hFLGlDQUF1QjtJQUFBLFlBQTJCO0lBQUEsaUJBQVE7SUFDM0QsaUJBQU87OztJQURpQixlQUEyQjtJQUEzQixxSEFBMkI7OztJQUUxRCxnQ0FBd0k7SUFDaEksNkJBQTBCO0lBQUEsNEJBQVk7SUFBQSxpQkFBSTtJQUMzQyxpQkFBTzs7O0lBT1osOEJBQWlIO0lBQUEsWUFBNEY7O0lBQUEsaUJBQUs7OztJQUE3SSxzREFBMkI7SUFBaUIsZUFBNEY7SUFBNUYseUlBQTRGOzs7SUFDN00sOEJBQTJEO0lBQUEsWUFBc0I7SUFBQSxpQkFBSzs7O0lBQTNCLGVBQXNCO0lBQXRCLDBDQUFzQjs7Ozs7SUFuQnpGLCtCQUEwRjtJQUN4RiwrQkFBaUI7SUFDZiwrQkFBeUg7SUFBN0YsNlRBQTJDO0lBQ3JFLGlGQUVjO0lBQ2QsaUZBRWM7SUFDZCxpRkFFYztJQUNkLCtCQUF3QjtJQUNSLCtCQUF5QjtJQUNyQiwwQkFBbUs7SUFDdkssaUJBQU07SUFDdEIsaUJBQU07SUFDTiwrQkFBNEI7SUFDMUIsK0VBQWtOO0lBQ2xOLCtFQUFzRjtJQUN0RixrQ0FBNkI7SUFBQSxhQUEyQjtJQUFBLGlCQUFRO0lBQ2xFLGlCQUFNO0lBQ1IsaUJBQU07SUFDUixpQkFBTTtJQUNSLGlCQUFNOzs7OztJQXRCc0UsZUFBZ0Q7SUFBaEQsK0VBQWdEO0lBQ25HLGVBQTBEO0lBQTFELCtOQUEwRDtJQUcxRCxlQUEyRDtJQUEzRCxnT0FBMkQ7SUFHM0MsZUFBbUc7SUFBbkcsMlJBQW1HO0lBSzdHLGVBQW1HO0lBQW5HLHNKQUFtRyw2QkFBQTtJQUlsRyxlQUFnQztJQUFoQyx1REFBZ0M7SUFDaEMsZUFBaUM7SUFBakMsd0RBQWlDO0lBQzVCLGVBQTJCO0lBQTNCLGlEQUEyQjs7O0lBTXRELDBCQUEyRTs7O0lBRDdFLDZCQUFpSDtJQUMvRyx1RkFBMkU7SUFDN0UsMEJBQWU7OztJQUR3QyxlQUFjO0lBQWQsMkNBQWM7Ozs7O0lBekRuRiw4QkFBNkM7SUFDM0MsaUNBQW9OO0lBQXpHLHFNQUFnQyxpTEFBQSw2TEFBQTtJQUN6SSw4QkFBaUM7SUFDdkIsd0VBQTZJO0lBRXJKLDBFQXdCTTtJQUVOLDBFQXdCTTtJQUNFLDBGQUVlO0lBQ3pCLGlCQUFNO0lBQ04seUJBQXFDO0lBQ3ZDLGlCQUFNO0lBQ04sK0JBQWdDO0lBQzlCLDhCQUEwQjtJQUM1QixrQ0FDRDtJQUFBLGlCQUFJO0lBQ0gsaUJBQU07SUFDTixnQ0FBZ0M7SUFDOUIsOEJBQTBCO0lBQzVCLGdDQUNEO0lBQUEsaUJBQUk7SUFDSCxpQkFBTTtJQUNSLGlCQUFNOzs7SUF2RTBCLGVBQXVCO0lBQXZCLDRDQUF1Qiw2RUFBQTtJQUVKLGVBQWdHO0lBQWhHLHdKQUFnRztJQUV4RyxlQUFvQjtJQUFwQiw0Q0FBb0I7SUEwQkcsZUFBa0I7SUFBbEIsOENBQWtCO0lBeUJ2RCxlQUFnRztJQUFoRyx3SkFBZ0c7O0FEOUMvSCxNQUFNLE9BQU8saUJBQWlCO0lBa0I1QixZQUNVLFFBQW1CLEVBQ25CLEVBQWM7UUFEZCxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLE9BQUUsR0FBRixFQUFFLENBQVk7UUFsQnhCLE1BQU07UUFDRyxPQUFFLEdBQWdCO1lBQ3pCLFdBQVcsRUFBRSxLQUFLO1NBQ25CLENBQUM7UUFDTyxjQUFTLEdBQW9CLEVBQUUsQ0FBQztRQUMvQixlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQXVCLENBQUM7UUFFL0Qsa0JBQWEsR0FBVyxpRUFBaUUsQ0FBQztRQUNuRiwrQkFBMEIsR0FBRyxDQUFDLENBQUM7UUFDdEMsa0JBQWEsR0FBaUIsRUFBRSxDQUFDO1FBQ2pDLGVBQVUsR0FBWSxJQUFJLENBQUM7UUFDM0IsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixXQUFNLEdBQVcsS0FBSyxDQUFDO1FBRXZCLGtCQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLGVBQVUsR0FBVSxFQUFFLENBQUM7UUFNaEIsaUJBQVksR0FBMEI7WUFDM0MsU0FBUyxFQUFFLFlBQVk7WUFDdkIsUUFBUSxFQUFFLElBQUk7WUFDZCxhQUFhLEVBQUUsQ0FBQztZQUNoQixjQUFjLEVBQUUsQ0FBQztZQUNqQixVQUFVLEVBQUUsS0FBSztZQUNqQixTQUFTLEVBQUUsS0FBSztZQUNoQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixhQUFhLEVBQUUsSUFBSTtZQUNuQix3QkFBd0I7WUFDeEIsbUJBQW1CO1lBQ25CLEtBQUssRUFBRSxJQUFJO1lBQ1gsa0JBQWtCO1lBQ2xCLFVBQVUsRUFBRTtnQkFDVixFQUFFLEVBQUUsb0JBQW9CO2dCQUN4QixJQUFJLEVBQUUsU0FBUztnQkFDZixTQUFTLEVBQUUsSUFBSTthQUNoQjtZQUNELFdBQVcsRUFBRTtnQkFDWCxHQUFHLEVBQUU7b0JBQ0gsYUFBYSxFQUFFLE1BQU07b0JBQ3JCLGNBQWMsRUFBRSxJQUFJO29CQUNwQixjQUFjLEVBQUUsQ0FBQztvQkFDakIsWUFBWSxFQUFFLENBQUM7aUJBQ2hCO2dCQUNELEdBQUcsRUFBRTtvQkFDSCxhQUFhLEVBQUUsQ0FBQztvQkFDaEIsY0FBYyxFQUFFLENBQUM7aUJBQ2xCO2dCQUNELElBQUksRUFBRTtvQkFDSixhQUFhLEVBQUUsQ0FBQztvQkFDaEIsY0FBYyxFQUFFLENBQUM7aUJBQ2xCO2dCQUNELElBQUksRUFBRTtvQkFDSixhQUFhLEVBQUUsQ0FBQztvQkFDaEIsY0FBYyxFQUFFLENBQUM7aUJBQ2xCO2FBQ0Y7U0FDRixDQUFDO0lBekNFLENBQUM7SUEyQ0wsUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLEdBQUcsRUFBRTtZQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztTQUNwQjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25CLE9BQU8sQ0FBQyxJQUFJLENBQUMsd0VBQXdFLENBQUMsQ0FBQztTQUN4RjtRQUNELEtBQUssTUFBTSxXQUFXLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUV4QyxJQUFJLFdBQVcsQ0FBQyxHQUFHLEtBQUssSUFBSSxFQUFFO2dCQUM1QixXQUFXLENBQUMsV0FBVyxHQUFHLEdBQUcsV0FBVyxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQ2pFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEVBQUU7b0JBQzdDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lCQUN0QztxQkFBSTtvQkFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsV0FBVyxDQUFDLFdBQVcsZUFBZSxDQUFDLENBQUM7aUJBQ2hFO2FBQ0Y7aUJBQUk7Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdEM7U0FDRjtJQUNILENBQUM7SUFDRCxVQUFVO0lBQ1YsaUJBQWlCLENBQUMsV0FBeUIsRUFBRSxLQUFhO1FBQ3hELElBQUksV0FBVyxFQUFFO1lBQ2YsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUMsS0FBSyxFQUFDLENBQUM7WUFDcEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDaEQ7YUFBSTtZQUNILElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztTQUNoRDtJQUNILENBQUM7SUFDRCxjQUFjO1FBQ1osSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDekIsQ0FBQztJQUNELGFBQWE7UUFDWCxVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDekIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1YsQ0FBQztJQUNELGFBQWE7UUFDWCxVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDMUIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1YsQ0FBQztJQUNELGNBQWM7UUFDWixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEQsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztRQUNqRCxNQUFNLFVBQVUsR0FBRyxXQUFXLEdBQUMsa0JBQWtCLENBQUM7UUFDbEQsTUFBTSxZQUFZLEdBQUcsVUFBVSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUUxQixJQUFJLFVBQVUsSUFBSSxDQUFDLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1NBQ3pCO1FBQ0QsSUFBSSxVQUFVLEdBQUcsQ0FBQyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztTQUNyQztRQUVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDckMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDMUI7SUFFSCxDQUFDOztxR0FwSVUsaUJBQWlCO21HQUFqQixpQkFBaUI7Ozs7OztRQ1g5Qiw4QkFBeUI7UUFDdkIsbUVBd0VNO1FBQ1IsaUJBQU07O1FBekV3QixlQUFlO1FBQWYsb0NBQWU7O3VGRFVoQyxpQkFBaUI7Y0FMN0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxhQUFhO2dCQUN2QixXQUFXLEVBQUUsMkJBQTJCO2dCQUN4QyxTQUFTLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQzthQUN4QztxRkFJVSxFQUFFO2tCQUFWLEtBQUs7WUFHRyxTQUFTO2tCQUFqQixLQUFLO1lBQ0ksVUFBVTtrQkFBbkIsTUFBTTtZQVE0QixNQUFNO2tCQUF4QyxTQUFTO21CQUFDLEdBQUcsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTd2lwZXJDb25maWdJbnRlcmZhY2UgfSBmcm9tICduZ3gtc3dpcGVyLXdyYXBwZXInO1xyXG5pbXBvcnQgeyBCZW5lZmljaWFyeSwgQmVuZWZpY2lhcnlTZWxlY3RlZCB9IGZyb20gJy4uLy4uL21vZGVscy9iZW5lZmljaWFyeS5tb2RlbCc7XHJcbmltcG9ydCB7IENhcm91c2VsVWkgfSBmcm9tICcuLi8uLi9tb2RlbHMvY2Fyb3VzZWwubW9kZWwnO1xyXG5pbXBvcnQgKiBhcyBfcnV0IGZyb20gJ3J1dC1oZWxwZXJzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnaWQtY2Fyb3VzZWwnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jYXJvdXNlbC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY2Fyb3VzZWwuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJvdXNlbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIC8vIHZhclxyXG4gIEBJbnB1dCgpIHVpPzogQ2Fyb3VzZWxVaSA9IHtcclxuICAgIGZhbWlseUdyb3VwOiBmYWxzZSxcclxuICB9O1xyXG4gIEBJbnB1dCgpIGRhdGFFbnRyeT86IHN0cmluZyB8IGFueVtdID0gW107XHJcbiAgQE91dHB1dCgpIGRhdGFPdXRwdXQgPSBuZXcgRXZlbnRFbWl0dGVyPEJlbmVmaWNpYXJ5U2VsZWN0ZWQ+KCk7XHJcbiAgYmVuZWZpY2lhcnlTZWxlY3RlZDogQmVuZWZpY2lhcnlTZWxlY3RlZDtcclxuICBkZWZhdWx0QXZhdGFyOiBzdHJpbmcgPSAnaHR0cDovL214ZGV2LmNsL2Jhbm1lZGljYS9pbWcvYXZhdGFycy12aWRhdHJlcy9BdmF0YXJfdjNfNjcuc3ZnJztcclxuICBwdWJsaWMgcXVhbnRpdHlBdmF0YXJPbkZhbWlseUNhcmQgPSAzO1xyXG4gIGJlbmVmaWNpYXJpZXM6IEJlbmVmaWNpYXJ5W10gPVtdO1xyXG4gIHN3aXBlck5leHQ6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIHN3aXBlclByZXY6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBtb2JpbGU6Ym9vbGVhbiA9IGZhbHNlO1xyXG4gIEBWaWV3Q2hpbGQoJ2MnLCB7IHN0YXRpYzogZmFsc2UgfSkgc3dpcGVyOiBFbGVtZW50UmVmO1xyXG4gIGNhcmRzbXVsdGlwbGU6IG51bWJlciA9IDU7XHJcbiAgZ2hvc3RDYXJkczogYW55W10gPSBbXTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgIHByaXZhdGUgZWw6IEVsZW1lbnRSZWZcclxuICApIHsgfVxyXG5cclxuICBwdWJsaWMgc3dpcGVyQ29uZmlnOiBTd2lwZXJDb25maWdJbnRlcmZhY2UgPSB7XHJcbiAgICBkaXJlY3Rpb246ICdob3Jpem9udGFsJyxcclxuICAgIG9ic2VydmVyOiB0cnVlLFxyXG4gICAgc2xpZGVzUGVyVmlldzogNSxcclxuICAgIHNsaWRlc1Blckdyb3VwOiAzLFxyXG4gICAgbW91c2V3aGVlbDogZmFsc2UsXHJcbiAgICBzY3JvbGxiYXI6IGZhbHNlLFxyXG4gICAgbmF2aWdhdGlvbjogdHJ1ZSxcclxuICAgIHNpbXVsYXRlVG91Y2g6IHRydWUsXHJcbiAgICB3YXRjaE92ZXJmbG93OiB0cnVlLFxyXG4gICAgLy8gY2VudGVyZWRTbGlkZXM6IHRydWUsXHJcbiAgICAvLyBzcGFjZUJldHdlZW46IDAsXHJcbiAgICBzcGVlZDogMTAwMCxcclxuICAgIC8vIGluaXRpYWxTbGlkZToxLFxyXG4gICAgcGFnaW5hdGlvbjoge1xyXG4gICAgICBlbDogJy5zd2lwZXItcGFnaW5hdGlvbicsXHJcbiAgICAgIHR5cGU6ICdidWxsZXRzJyxcclxuICAgICAgY2xpY2thYmxlOiB0cnVlXHJcbiAgICB9LFxyXG4gICAgYnJlYWtwb2ludHM6IHtcclxuICAgICAgNTYwOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogJ2F1dG8nLFxyXG4gICAgICAgIGNlbnRlcmVkU2xpZGVzOiB0cnVlLFxyXG4gICAgICAgIHNsaWRlc1Blckdyb3VwOiAxLFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMCxcclxuICAgICAgfSxcclxuICAgICAgODUwOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMyxcclxuICAgICAgICBzbGlkZXNQZXJHcm91cDogMVxyXG4gICAgICB9LFxyXG4gICAgICAxMjAwOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMyxcclxuICAgICAgICBzbGlkZXNQZXJHcm91cDogMVxyXG4gICAgICB9LFxyXG4gICAgICAxNDAwOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogNCxcclxuICAgICAgICBzbGlkZXNQZXJHcm91cDogMlxyXG4gICAgICB9LFxyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5zZXRDYXJkc0xlbmd0aCgpO1xyXG4gICAgaWYgKHdpbmRvdy5zY3JlZW4ud2lkdGggPD0gNTYwKSB7XHJcbiAgICAgIHRoaXMubW9iaWxlID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIXRoaXMuZGF0YUVudHJ5KSB7XHJcbiAgICAgIGNvbnNvbGUud2FybignTm8gc2UgaGEgZW5jb250cmFkbyBsYSBkYXRhIGRlIGJlbmVmaWNpYXJpb3MgZW4gbGEgZGlyZWN0aXZhIGRhdGFFbnRyeScpO1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBiZW5lZmljaWFyeSBvZiB0aGlzLmRhdGFFbnRyeSkge1xyXG5cclxuICAgICAgaWYgKGJlbmVmaWNpYXJ5LnJ1dCAhPT0gbnVsbCkge1xyXG4gICAgICAgIGJlbmVmaWNpYXJ5LnJ1dGNvbXBsZXRlID0gYCR7YmVuZWZpY2lhcnkucnV0fS0ke2JlbmVmaWNpYXJ5LmR2fWA7XHJcbiAgICAgICAgaWYgKF9ydXQucnV0VmFsaWRhdGUoYmVuZWZpY2lhcnkucnV0Y29tcGxldGUpKSB7XHJcbiAgICAgICAgICB0aGlzLmJlbmVmaWNpYXJpZXMucHVzaChiZW5lZmljaWFyeSk7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICBjb25zb2xlLndhcm4oYEVsIFJ1dCAke2JlbmVmaWNpYXJ5LnJ1dGNvbXBsZXRlfSBubyBlcyB2w6FsaWRvYCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICB0aGlzLmJlbmVmaWNpYXJpZXMucHVzaChiZW5lZmljaWFyeSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLy8gbWV0aG9kc1xyXG4gIHNlbGVjdEJlbmVmaWNpYXJ5KGJlbmVmaWNpYXJ5PzogQmVuZWZpY2lhcnksIGluZGV4PzpudW1iZXIpe1xyXG4gICAgaWYgKGJlbmVmaWNpYXJ5KSB7XHJcbiAgICAgIHRoaXMuYmVuZWZpY2lhcnlTZWxlY3RlZCA9IHsgYmVuZWZpY2lhcnksIGluZGV4LCBmYW1pbHlHcm91cDpmYWxzZX07XHJcbiAgICAgIHRoaXMuZGF0YU91dHB1dC5lbWl0KHRoaXMuYmVuZWZpY2lhcnlTZWxlY3RlZCk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgdGhpcy5iZW5lZmljaWFyeVNlbGVjdGVkID0geyBmYW1pbHlHcm91cDogdHJ1ZX07XHJcbiAgICAgIHRoaXMuZGF0YU91dHB1dC5lbWl0KHRoaXMuYmVuZWZpY2lhcnlTZWxlY3RlZCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldFN3aXBlclN0YXRlKCl7XHJcbiAgICB0aGlzLnN3aXBlck5leHQgPSB0cnVlO1xyXG4gICAgdGhpcy5zd2lwZXJQcmV2ID0gdHJ1ZTtcclxuICB9XHJcbiAgc3dpcGVyTmV4dEVuZCgpe1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIHRoaXMuc3dpcGVyTmV4dCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLnN3aXBlclByZXYgPSB0cnVlO1xyXG4gICAgfSwgMTAwKTtcclxuICB9XHJcbiAgc3dpcGVyUHJldkVuZCgpe1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIHRoaXMuc3dpcGVyTmV4dCA9IHRydWU7XHJcbiAgICAgIHRoaXMuc3dpcGVyUHJldiA9IGZhbHNlO1xyXG4gICAgfSwgMTAwKTtcclxuICB9XHJcbiAgc2V0Q2FyZHNMZW5ndGgoKXtcclxuICAgIGNvbnN0IGN1cnJlbnRDYXJkID0gdGhpcy51aS5mYW1pbHlHcm91cCA/IDIgOiAxO1xyXG4gICAgY29uc3QgY2FyZHNCZW5lZmljaWFyaWVzID0gdGhpcy5kYXRhRW50cnkubGVuZ3RoO1xyXG4gICAgY29uc3QgdG90YWxDYXJkcyA9IGN1cnJlbnRDYXJkK2NhcmRzQmVuZWZpY2lhcmllcztcclxuICAgIGNvbnN0IG1pc3NpbmdDYXJkcyA9IHRvdGFsQ2FyZHMgPj0gNSA/IDIgLSAodG90YWxDYXJkcyAlIDMpIDogMDtcclxuICAgIGNvbnNvbGUubG9nKG1pc3NpbmdDYXJkcyk7XHJcblxyXG4gICAgaWYgKHRvdGFsQ2FyZHMgPD0gNSkge1xyXG4gICAgICB0aGlzLnN3aXBlckNvbmZpZy5zbGlkZXNQZXJWaWV3ID0gNDtcclxuICAgICAgdGhpcy5zd2lwZXJOZXh0ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAodG90YWxDYXJkcyA+IDUpIHtcclxuICAgICAgdGhpcy5zd2lwZXJDb25maWcuc2xpZGVzUGVyVmlldyA9IDU7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBtaXNzaW5nQ2FyZHM7IGkrKykge1xyXG4gICAgICB0aGlzLmdob3N0Q2FyZHMucHVzaCh7fSk7XHJcbiAgICB9XHJcblxyXG4gIH1cclxuXHJcbn1cclxuIiwiPGRpdiBjbGFzcz1cImlkLWNhcnJ1c2VsXCI+XHJcblx0XHQ8ZGl2IGNsYXNzPVwic3dpcGUtY29udGVudFwiICpuZ0lmPVwiZGF0YUVudHJ5XCI+XHJcblx0XHRcdFx0PGRpdiBjbGFzcz1cInN3aXBlci1jb250YWluZXJcIiBbc3dpcGVyXT1cInN3aXBlckNvbmZpZ1wiIFtuZ0NsYXNzXT1cInsnbmV4dCc6IHN3aXBlck5leHQsICdwcmV2Jzogc3dpcGVyUHJldn1cIiAoaW5kZXhDaGFuZ2UpPVwiZ2V0U3dpcGVyU3RhdGUoKVwiIChyZWFjaEVuZCk9XCJzd2lwZXJOZXh0RW5kKClcIiAocmVhY2hCZWdpbm5pbmcpPVwic3dpcGVyUHJldkVuZCgpXCIgI3N3aXBlcj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInN3aXBlci13cmFwcGVyIHBiLTJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzd2lwZXItc2xpZGUgcHQtNCBwYi00XCIgKm5nSWY9XCJiZW5lZmljaWFyaWVzLmxlbmd0aD49NCAmJiAhbW9iaWxlICYmIHVpLmZhbWlseUdyb3VwIHx8IGJlbmVmaWNpYXJpZXMubGVuZ3RoPj01ICYmICFtb2JpbGVcIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdDwhLS0gQ2FyZCBHcm91cEZhbWlseSAtLT5cclxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJzd2lwZXItc2xpZGUgcHQtNCBwYi00XCIgKm5nSWY9XCJ1aS5mYW1pbHlHcm91cFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2xcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNhcmQgc2hhZG93LXNtIGNhcmQtZmFtaWx5LWdyb3VwXCIgKGNsaWNrKT1cInNlbGVjdEJlbmVmaWNpYXJ5KClcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2Utc3VjY2Vzc1wiICpuZ0lmPVwiYmVuZWZpY2lhcnlTZWxlY3RlZD8uZmFtaWx5R3JvdXBcIj5cclxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPmNoZWNrX2NpcmNsZTwvaT5cclxuXHRcdFx0XHRcdFx0XHQgICAgICAgICAgICAgIDwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJhdmF0YXItZ3JvdXBcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibS1hdXRvIGQtZmxleCBwb3NpdGlvbi1yZWxhdGl2ZVwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZC1mbGV4XCIgW25nQ2xhc3NdPVwieydtLWNlbnRlcicgOiBiZW5lZmljaWFyaWVzLmxlbmd0aCA+IDJ9XCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8bmctY29udGFpbmVyICpuZ0Zvcj1cImxldCBiZW5lZmljaWFyeSBvZiBiZW5lZmljaWFyaWVzIHwgc2xpY2U6MDpxdWFudGl0eUF2YXRhck9uRmFtaWx5Q2FyZFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxpbWcgW3NyY109XCJiZW5lZmljaWFyeS5hdmF0YXIgPT0gbnVsbCB8fCBiZW5lZmljaWFyeS5hdmF0YXIgPT0gJycgPyBkZWZhdWx0QXZhdGFyIDogYmVuZWZpY2lhcnkuYXZhdGFyXCIgY2xhc3M9XCJjYXJkLWltZy10b3AgY2FyZC1pbWctZ3JvdXBcIiBbYWx0XT1cImJlbmVmaWNpYXJ5Lm5hbWVcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvbmctY29udGFpbmVyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNhcmQtaW1nLWdyb3VwIGl0ZW0tZXh0cmFcIiAqbmdJZj1cImJlbmVmaWNpYXJpZXMubGVuZ3RoID4gcXVhbnRpdHlBdmF0YXJPbkZhbWlseUNhcmRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQre3sgYmVuZWZpY2lhcmllcy5sZW5ndGggLSBxdWFudGl0eUF2YXRhck9uRmFtaWx5Q2FyZCB9fVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5IG10LTNcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8aDYgY2xhc3M9XCJjYXJkLXRpdGxlXCI+R3J1cG8gRmFtaWxpYXI8L2g2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxzbWFsbCBjbGFzcz1cImNhcmQtc3VidGl0bGVcIj57eyBiZW5lZmljaWFyaWVzLmxlbmd0aCB9fSBiZW5lZmljaWFyaW9zPC9zbWFsbD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0PCEtLSBJdGVyYXRpb24gRGF0YSBJbnB1dCAtLT5cclxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJzd2lwZXItc2xpZGUgcHQtNCBwYi00XCIgKm5nRm9yPVwibGV0IGJlbmVmaWNpYXJ5IG9mIGJlbmVmaWNpYXJpZXM7IGluZGV4IGFzIGlcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkIHNoYWRvdy1zbVwiIChjbGljayk9XCJzZWxlY3RCZW5lZmljaWFyeShiZW5lZmljaWFyeSwgaSlcIiBbbmdDbGFzc109XCJ7J2RlY2lkdW91cyc6IGJlbmVmaWNpYXJ5LmRlY2lkdW91c31cIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwidGFnXCIgKm5nSWY9XCJiZW5lZmljaWFyeT8udGFnPy5zdGF0ZSAmJiBiZW5lZmljaWFyeT8udGFnPy5hY3JvbnltXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQgICAgICAgICAgICAgIDxzbWFsbD57e2JlbmVmaWNpYXJ5Py50YWc/LmxhYmVsfX08L3NtYWxsPlxyXG5cdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgPC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJ0YWdcIiAqbmdJZj1cImJlbmVmaWNpYXJ5Py50YWc/LnN0YXRlICYmICFiZW5lZmljaWFyeT8udGFnPy5hY3JvbnltXCI+XHJcblx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgICBcdDxzbWFsbCBjbGFzcz1cImNhcGl0YWxcIj57e2JlbmVmaWNpYXJ5Py50YWc/LmxhYmVsfX08L3NtYWxsPlxyXG5cdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgPC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJiYWRnZSBiYWRnZS1zdWNjZXNzXCIgKm5nSWY9XCJiZW5lZmljaWFyeS5ydXQgPT09IGJlbmVmaWNpYXJ5U2VsZWN0ZWQ/LmJlbmVmaWNpYXJ5Py5ydXQgJiYgaSA9PT0gYmVuZWZpY2lhcnlTZWxlY3RlZD8uaW5kZXhcIj5cclxuXHRcdFx0XHRcdFx0XHQgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPmNoZWNrX2NpcmNsZTwvaT5cclxuXHRcdFx0XHRcdFx0XHQgICAgICAgICAgICAgIDwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJhdmF0YXItYm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdmF0YXItY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBbc3JjXT1cImJlbmVmaWNpYXJ5LmF2YXRhciA9PSBudWxsIHx8IGJlbmVmaWNpYXJ5LmF2YXRhciA9PSAnJyA/IGRlZmF1bHRBdmF0YXIgOiBiZW5lZmljaWFyeS5hdmF0YXJcIiBjbGFzcz1cImNhcmQtaW1nLXRvcCBhdmF0YXItY2FyZFwiIFthbHRdPVwiYmVuZWZpY2lhcnkubmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNhcmQtYm9keSBtdC0zXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGg2IGNsYXNzPVwiY2FyZC10aXRsZVwiICpuZ0lmPVwiYmVuZWZpY2lhcnkubmFtZS5sZW5ndGg+MjBcIiBpZC10b29sdGlwIHRleHQ9XCJ7e2JlbmVmaWNpYXJ5Lm5hbWV9fVwiIHBsYWNlbWVudD1cInRvcFwiPnt7IChiZW5lZmljaWFyeS5uYW1lLmxlbmd0aD4yMCk/IChiZW5lZmljaWFyeS5uYW1lIHwgc2xpY2U6MDoyMCkrJy4uLic6KGJlbmVmaWNpYXJ5Lm5hbWUpIH19PC9oNj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8aDYgY2xhc3M9XCJjYXJkLXRpdGxlXCIgKm5nSWY9XCJiZW5lZmljaWFyeS5uYW1lLmxlbmd0aDw9MjBcIj57eyBiZW5lZmljaWFyeS5uYW1lIH19PC9oNj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8c21hbGwgY2xhc3M9XCJjYXJkLXN1YnRpdGxlXCI+e3tiZW5lZmljaWFyeS5ydXRjb21wbGV0ZX19PC9zbWFsbD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cImJlbmVmaWNpYXJpZXMubGVuZ3RoPj00ICYmICFtb2JpbGUgJiYgdWkuZmFtaWx5R3JvdXAgfHwgYmVuZWZpY2lhcmllcy5sZW5ndGg+PTUgJiYgIW1vYmlsZVwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3dpcGVyLXNsaWRlIHB0LTQgcGItNFwiICpuZ0Zvcj1cImxldCBjYXJkIG9mIGdob3N0Q2FyZHM7XCI+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJzd2lwZXItcGFnaW5hdGlvblwiPjwvZGl2PlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJzd2lwZXItYnV0dG9uLXByZXZcIj5cclxuXHRcdFx0XHRcdFx0PGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPlxyXG5cdFx0XHRcdG5hdmlnYXRlX2JlZm9yZVxyXG5cdFx0XHQ8L2k+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PGRpdiBjbGFzcz1cInN3aXBlci1idXR0b24tbmV4dFwiPlxyXG5cdFx0XHRcdFx0XHQ8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCI+XHJcblx0XHRcdFx0bmF2aWdhdGVfbmV4dFxyXG5cdFx0XHQ8L2k+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHQ8L2Rpdj5cclxuPC9kaXY+XHJcbiJdfQ==