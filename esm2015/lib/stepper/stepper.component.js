import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
const _c0 = function (a0, a1) { return { "step-active": a0, "step-past": a1 }; };
function StepperComponent_div_0_li_4_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "li", 6);
    i0.ɵɵelementStart(1, "a", 7);
    i0.ɵɵlistener("click", function StepperComponent_div_0_li_4_Template_a_click_1_listener() { const restoredCtx = i0.ɵɵrestoreView(_r5); const i_r3 = restoredCtx.index; const ctx_r4 = i0.ɵɵnextContext(2); return ctx_r4.linkPrev(i_r3); });
    i0.ɵɵelementStart(2, "span", 8);
    i0.ɵɵelementStart(3, "small");
    i0.ɵɵelementStart(4, "strong");
    i0.ɵɵtext(5);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "small");
    i0.ɵɵelementStart(7, "strong");
    i0.ɵɵtext(8);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const step_r2 = ctx.$implicit;
    const i_r3 = ctx.index;
    const ctx_r1 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction2(3, _c0, i_r3 + 1 === ctx_r1.ui.activeStep, i_r3 + 1 < ctx_r1.ui.activeStep));
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate(i_r3 + 1);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(step_r2.name);
} }
function StepperComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 1);
    i0.ɵɵelement(1, "div", 2);
    i0.ɵɵelementStart(2, "div", 3);
    i0.ɵɵelementStart(3, "ul", 4);
    i0.ɵɵtemplate(4, StepperComponent_div_0_li_4_Template, 9, 6, "li", 5);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("ngForOf", ctx_r0.steps);
} }
export class StepperComponent {
    constructor() {
        this.steps = [];
        this.dataOutput = new EventEmitter();
    }
    ngDoCheck() {
        if (this.ui.activeStep > 1 && this.ui.activeStep < this.steps.length) {
            this.dataOutput.emit(this.ui.activeStep);
        }
        else if (this.ui.activeStep <= 0) {
            this.dataOutput.emit(1);
        }
        else if (this.ui.activeStep >= this.steps.length) {
            this.dataOutput.emit(this.steps.length);
        }
    }
    ngOnInit() {
        /*steps min-max*/
        if (this.steps !== undefined) {
            if (this.steps.length < 3 || this.steps.length > 6) {
                console.warn('La cantidad de pasos del flujo no debe ser menor a 3 ni mayor a 6');
            }
        }
        /*steps undefined*/
        if (this.steps === undefined) {
            this.steps = [
                {
                    name: 'Paso'
                },
                {
                    name: 'Paso'
                },
                {
                    name: 'Paso'
                }
            ];
        }
        /* stepActive*/
        if (this.ui === undefined || this.ui.activeStep === undefined) {
            this.ui.activeStep = 0;
        }
    }
    updateUi(state) {
        this.dataOutput.emit(state.activeStep);
    }
    linkPrev(i) {
        if (i + 1 < this.ui.activeStep) {
            this.ui.activeStep = i + 1;
            this.dataOutput.emit(this.ui.activeStep);
        }
    }
}
/** @nocollapse */ StepperComponent.ɵfac = function StepperComponent_Factory(t) { return new (t || StepperComponent)(); };
/** @nocollapse */ StepperComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: StepperComponent, selectors: [["id-stepper"]], inputs: { ui: "ui", steps: "steps" }, outputs: { dataOutput: "dataOutput" }, decls: 1, vars: 1, consts: [["class", "row w-100 no-gutters border-bottom-1 pb-4 stepbystep", 4, "ngIf"], [1, "row", "w-100", "no-gutters", "border-bottom-1", "pb-4", "stepbystep"], [1, "d-none"], [1, "col-12"], [1, "list-group", "list-group-horizontal", "justify-content-between"], ["class", "list-group-item p-0 border-0", 3, "ngClass", 4, "ngFor", "ngForOf"], [1, "list-group-item", "p-0", "border-0", 3, "ngClass"], [3, "click"], [1, "rounded-circle"]], template: function StepperComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, StepperComponent_div_0_Template, 5, 1, "div", 0);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", ctx.steps.length > 2 && ctx.steps.length < 7);
    } }, directives: [i1.NgIf, i1.NgForOf, i1.NgClass], styles: ["[_nghost-%COMP%]   .step-past[_ngcontent-%COMP%]:hover{cursor:pointer}[_nghost-%COMP%]   .list-group-item[_ngcontent-%COMP%]   small[_ngcontent-%COMP%]{line-height:normal}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(StepperComponent, [{
        type: Component,
        args: [{
                selector: 'id-stepper',
                templateUrl: './stepper.component.html',
                styleUrls: ['./stepper.component.css']
            }]
    }], function () { return []; }, { ui: [{
            type: Input
        }], steps: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcHBlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiQzovVXNlcnMvVXN1YXJpby9EZXNrdG9wL2xpYnJlcmlhbmd1bGFyLTI5LTEwLTIxL2NvcGlhLWFuZ3VsYXItbGliLXVwZC0xMi1zaW4tcnV0L3Byb2plY3RzL2lzYXByZS1kaWdpdGFsL3NyYy8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcGVyL3N0ZXBwZXIuY29tcG9uZW50LnRzIiwibGliL3N0ZXBwZXIvc3RlcHBlci5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFVLE1BQU0sZUFBZSxDQUFDOzs7Ozs7SUNJM0UsNkJBQXFLO0lBQ2pLLDRCQUF5QjtJQUF0QiwyT0FBcUI7SUFDcEIsK0JBQTZCO0lBQy9CLDZCQUFPO0lBQUEsOEJBQVE7SUFBQSxZQUFXO0lBQUEsaUJBQVM7SUFBQSxpQkFBUTtJQUM3QyxpQkFBTztJQUNILDZCQUFPO0lBQ1QsOEJBQVE7SUFBQSxZQUFlO0lBQUEsaUJBQVM7SUFDbEMsaUJBQVE7SUFDUixpQkFBSTtJQUNSLGlCQUFLOzs7OztJQVQyRSx3SEFBb0Y7SUFHL0ksZUFBVztJQUFYLDhCQUFXO0lBR2xCLGVBQWU7SUFBZixrQ0FBZTs7O0lBVnpDLDhCQUErRztJQUMzRyx5QkFBMEI7SUFDMUIsOEJBQW9CO0lBQ2hCLDZCQUFxRTtJQUNqRSxxRUFTSztJQUNULGlCQUFLO0lBQ1QsaUJBQU07SUFDVixpQkFBTTs7O0lBWjJCLGVBQVU7SUFBVixzQ0FBVTs7QURLM0MsTUFBTSxPQUFPLGdCQUFnQjtJQVEzQjtRQUpTLFVBQUssR0FBaUIsRUFBRSxDQUFBO1FBRXZCLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO0lBRS9CLENBQUM7SUFDakIsU0FBUztRQUNQLElBQUksSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ3BFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDMUM7YUFBTSxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxJQUFLLENBQUMsRUFBQztZQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN6QjthQUFNLElBQUksSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUM7WUFDakQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6QztJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04saUJBQWlCO1FBQ2pCLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxTQUFTLEVBQUU7WUFDNUIsSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFHO2dCQUNsRCxPQUFPLENBQUMsSUFBSSxDQUFDLG1FQUFtRSxDQUFDLENBQUM7YUFDbkY7U0FDRjtRQUNELG1CQUFtQjtRQUNuQixJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssU0FBUyxFQUFFO1lBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUc7Z0JBQ1g7b0JBQ0EsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0Q7b0JBQ0UsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0Q7b0JBQ0UsSUFBSSxFQUFFLE1BQU07aUJBQ2I7YUFDQSxDQUFBO1NBQ0Y7UUFDRCxlQUFlO1FBQ2YsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsS0FBSyxTQUFTLEVBQUU7WUFDN0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFnQjtRQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELFFBQVEsQ0FBQyxDQUFNO1FBQ2IsSUFBSyxDQUFDLEdBQUcsQ0FBQyxHQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUMxQztJQUNILENBQUM7O21HQXZEVSxnQkFBZ0I7a0dBQWhCLGdCQUFnQjtRQ1Q3QixpRUFnQk07O1FBaEJBLG1FQUEwQzs7dUZEU25DLGdCQUFnQjtjQUw1QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLFdBQVcsRUFBRSwwQkFBMEI7Z0JBQ3ZDLFNBQVMsRUFBRSxDQUFDLHlCQUF5QixDQUFDO2FBQ3ZDO3NDQUdVLEVBQUU7a0JBQVYsS0FBSztZQUVHLEtBQUs7a0JBQWIsS0FBSztZQUVJLFVBQVU7a0JBQW5CLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBEb0NoZWNrfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3RlcHBlclVpLCBTdGVwcyB9IGZyb20gJy4uLy4uL21vZGVscy9zdGVwcGVyLm1vZGVsJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2lkLXN0ZXBwZXInLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9zdGVwcGVyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9zdGVwcGVyLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU3RlcHBlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgRG9DaGVjayB7XHJcblxyXG4gIEBJbnB1dCgpIHVpOiBTdGVwcGVyVWk7XHJcblxyXG4gIEBJbnB1dCgpIHN0ZXBzOiBBcnJheTxTdGVwcz4gPSBbXVxyXG5cclxuICBAT3V0cHV0KCkgZGF0YU91dHB1dCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG4gIG5nRG9DaGVjaygpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLnVpLmFjdGl2ZVN0ZXAgPiAxICYmIHRoaXMudWkuYWN0aXZlU3RlcCA8IHRoaXMuc3RlcHMubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuZGF0YU91dHB1dC5lbWl0KHRoaXMudWkuYWN0aXZlU3RlcCk7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMudWkuYWN0aXZlU3RlcCA8PSAgMCl7XHJcbiAgICAgIHRoaXMuZGF0YU91dHB1dC5lbWl0KDEpO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLnVpLmFjdGl2ZVN0ZXAgPj0gdGhpcy5zdGVwcy5sZW5ndGgpe1xyXG4gICAgICB0aGlzLmRhdGFPdXRwdXQuZW1pdCh0aGlzLnN0ZXBzLmxlbmd0aCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIC8qc3RlcHMgbWluLW1heCovXHJcbiAgICBpZiAodGhpcy5zdGVwcyAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgIGlmKHRoaXMuc3RlcHMubGVuZ3RoIDwgMyB8fCB0aGlzLnN0ZXBzLmxlbmd0aCA+IDYgKSB7XHJcbiAgICAgICAgY29uc29sZS53YXJuKCdMYSBjYW50aWRhZCBkZSBwYXNvcyBkZWwgZmx1am8gbm8gZGViZSBzZXIgbWVub3IgYSAzIG5pIG1heW9yIGEgNicpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAvKnN0ZXBzIHVuZGVmaW5lZCovXHJcbiAgICBpZiAodGhpcy5zdGVwcyA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgIHRoaXMuc3RlcHMgPSBbXHJcbiAgICAgICAge1xyXG4gICAgICAgIG5hbWU6ICdQYXNvJ1xyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgbmFtZTogJ1Bhc28nXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBuYW1lOiAnUGFzbydcclxuICAgICAgfVxyXG4gICAgICBdXHJcbiAgICB9XHJcbiAgICAvKiBzdGVwQWN0aXZlKi9cclxuICAgIGlmICh0aGlzLnVpID09PSB1bmRlZmluZWQgfHwgdGhpcy51aS5hY3RpdmVTdGVwID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgdGhpcy51aS5hY3RpdmVTdGVwID0gMDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHVwZGF0ZVVpKHN0YXRlOiBTdGVwcGVyVWkpIHtcclxuICAgIHRoaXMuZGF0YU91dHB1dC5lbWl0KHN0YXRlLmFjdGl2ZVN0ZXApO1xyXG4gIH1cclxuXHJcbiAgbGlua1ByZXYoaTogYW55KSB7XHJcbiAgICBpZiAoIGkgKyAxIDwgIHRoaXMudWkuYWN0aXZlU3RlcCkge1xyXG4gICAgICB0aGlzLnVpLmFjdGl2ZVN0ZXAgPSBpICsgMTtcclxuICAgICAgdGhpcy5kYXRhT3V0cHV0LmVtaXQodGhpcy51aS5hY3RpdmVTdGVwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiIsIjxkaXYgKm5nSWY9XCJzdGVwcy5sZW5ndGggPiAyICYmIHN0ZXBzLmxlbmd0aCA8IDdcIiBjbGFzcz1cInJvdyB3LTEwMCBuby1ndXR0ZXJzIGJvcmRlci1ib3R0b20tMSBwYi00IHN0ZXBieXN0ZXBcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJkLW5vbmVcIj48L2Rpdj5cclxuICAgIDxkaXYgY2xhc3M9XCJjb2wtMTJcIj5cclxuICAgICAgICA8dWwgY2xhc3M9XCJsaXN0LWdyb3VwIGxpc3QtZ3JvdXAtaG9yaXpvbnRhbCBqdXN0aWZ5LWNvbnRlbnQtYmV0d2VlblwiPlxyXG4gICAgICAgICAgICA8bGkgKm5nRm9yPVwibGV0IHN0ZXAgb2Ygc3RlcHM7IGluZGV4IGFzIGlcIiBjbGFzcz1cImxpc3QtZ3JvdXAtaXRlbSBwLTAgYm9yZGVyLTBcIiBbbmdDbGFzc109XCJ7J3N0ZXAtYWN0aXZlJzogaSsxID09PSB1aS5hY3RpdmVTdGVwLCAnc3RlcC1wYXN0JzogaSsxIDwgdWkuYWN0aXZlU3RlcH1cIj5cclxuICAgICAgICAgICAgICAgIDxhIChjbGljayk9XCJsaW5rUHJldihpKVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwicm91bmRlZC1jaXJjbGVcIj5cclxuICAgICAgICAgICAgICAgICAgPHNtYWxsPjxzdHJvbmc+e3sgaSArIDEgfX08L3N0cm9uZz48L3NtYWxsPlxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzbWFsbD5cclxuICAgICAgICAgICAgICAgICAgPHN0cm9uZz57eyBzdGVwLm5hbWUgfX08L3N0cm9uZz5cclxuICAgICAgICAgICAgICAgIDwvc21hbGw+XHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgPC91bD5cclxuICAgIDwvZGl2PlxyXG48L2Rpdj4iXX0=