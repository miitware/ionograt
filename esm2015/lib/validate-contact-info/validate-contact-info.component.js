import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "@angular/common";
function ValidateContactInfoComponent_a_9_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "a", 10);
    i0.ɵɵlistener("click", function ValidateContactInfoComponent_a_9_Template_a_click_0_listener($event) { i0.ɵɵrestoreView(_r5); const ctx_r4 = i0.ɵɵnextContext(); return ctx_r4.editInput($event, "email"); });
    i0.ɵɵelementStart(1, "i", 5);
    i0.ɵɵtext(2, "edit");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_small_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1, "* El correo electr\u00F3nico es obligatorio");
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_small_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1, "* Debes ingresar un correo electr\u00F3nico valido");
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_div_12_a_7_Template(rf, ctx) { if (rf & 1) {
    const _r10 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "a", 10);
    i0.ɵɵlistener("click", function ValidateContactInfoComponent_div_12_a_7_Template_a_click_0_listener($event) { i0.ɵɵrestoreView(_r10); const ctx_r9 = i0.ɵɵnextContext(2); return ctx_r9.editInput($event, "phone"); });
    i0.ɵɵelementStart(1, "i", 5);
    i0.ɵɵtext(2, "edit");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_div_12_small_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1, "* Debes ingresar 9 d\u00EDgitos");
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_div_12_small_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1, "* Debes ingresar s\u00F3lo n\u00FAmeros");
    i0.ɵɵelementEnd();
} }
const _c0 = function (a0) { return { "fg-invalid": a0 }; };
function ValidateContactInfoComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 11);
    i0.ɵɵelementStart(1, "div", 3);
    i0.ɵɵelementStart(2, "input", 12);
    i0.ɵɵlistener("keypress", function ValidateContactInfoComponent_div_12_Template_input_keypress_2_listener($event) { i0.ɵɵrestoreView(_r12); const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.numberOnly($event); })("blur", function ValidateContactInfoComponent_div_12_Template_input_blur_2_listener() { i0.ɵɵrestoreView(_r12); const ctx_r13 = i0.ɵɵnextContext(); return ctx_r13.emitDynamicallyData(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "i", 5);
    i0.ɵɵtext(4, "local_phone");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "label", 13);
    i0.ɵɵtext(6, "Tel\u00E9fono celular");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(7, ValidateContactInfoComponent_div_12_a_7_Template, 3, 0, "a", 7);
    i0.ɵɵtemplate(8, ValidateContactInfoComponent_div_12_small_8_Template, 2, 0, "small", 8);
    i0.ɵɵtemplate(9, ValidateContactInfoComponent_div_12_small_9_Template, 2, 0, "small", 8);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(4, _c0, ctx_r3.formValidateContactInfo.get("phone").invalid));
    i0.ɵɵadvance(6);
    i0.ɵɵproperty("ngIf", ctx_r3.formValidateContactInfo.get("phone").disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r3.formValidateContactInfo.get("phone").hasError("minlength") || ctx_r3.formValidateContactInfo.get("phone").hasError("maxlength"));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r3.formValidateContactInfo.get("phone").hasError("pattern"));
} }
const _c1 = function (a0) { return [a0]; };
export class ValidateContactInfoComponent {
    constructor(_fb, _el) {
        this._fb = _fb;
        this._el = _el;
        this.dataOutput = new EventEmitter();
        this.validationOfIssuedEmail = false;
        this.formValidateContactInfo = this._fb.group({
            email: new FormControl({ value: undefined, disabled: false }, {
                validators: [
                    Validators.required,
                    Validators.pattern('^(?![.])(?!.*[-_.]$)[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+[.][a-zA-Z0-9-.]+$')
                ],
                updateOn: 'blur'
            }),
            phone: new FormControl({ value: undefined, disabled: false }, {
                validators: [
                    Validators.maxLength(9),
                    Validators.minLength(9),
                    Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')
                ],
                updateOn: 'blur'
            })
        });
    }
    ngOnInit() {
        if (this.dataEntry) {
            this.formValidateContactInfo.get('email').setValue(this.dataEntry.email, { emitEvent: false });
            if (this.dataEntry.phone != undefined) {
                this.formValidateContactInfo.get('phone').setValue(this.dataEntry.phone.toString(), { emitEvent: false });
            }
            if (!this.dataEntry.email) {
                this.formValidateContactInfo.controls['email'].enable();
            }
            else {
                if (this.formValidateContactInfo.get('email').valid) {
                    this.formValidateContactInfo.controls['email'].disable();
                }
                else {
                    this.validationOfIssuedEmail = true;
                }
            }
            if (!this.dataEntry.phone) {
                this.formValidateContactInfo.controls['phone'].enable();
            }
            else {
                if (this.formValidateContactInfo.get('phone').valid) {
                    this.formValidateContactInfo.controls['phone'].disable();
                }
            }
        }
        else {
            this.formValidateContactInfo.get('email').setValue(undefined, { emitEvent: false });
            this.formValidateContactInfo.controls['email'].enable();
            this.formValidateContactInfo.get('phone').setValue(undefined, { emitEvent: false });
            this.formValidateContactInfo.controls['phone'].enable();
        }
        this.emitDynamicallyData();
    }
    editInput(event, input) {
        const field = this._el.nativeElement.querySelector('#' + input);
        event.preventDefault();
        this.emitDynamicallyData(false);
        this.formValidateContactInfo.controls[input].enable();
        // set focus to edit
        field.focus();
        // this.formValidateContactInfo.get(input).reset();
    }
    numberOnly(event) {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    emitDynamicallyData(stateForm) {
        let forceSateForm = stateForm != undefined ? stateForm : !this.formValidateContactInfo.invalid;
        let formStatus = { 'validForm': forceSateForm };
        let formDataOutput;
        if (!this.formValidateContactInfo.invalid) {
            this.formValidateContactInfo.controls['email'].disable();
            if (this.formValidateContactInfo.get('phone').value != null && this.formValidateContactInfo.get('phone').value.length > 0) {
                this.formValidateContactInfo.controls['phone'].disable();
            }
        }
        formDataOutput = Object.assign(Object.assign({}, this.formValidateContactInfo.getRawValue()), formStatus);
        this.dataOutput.emit(formDataOutput);
    }
}
/** @nocollapse */ ValidateContactInfoComponent.ɵfac = function ValidateContactInfoComponent_Factory(t) { return new (t || ValidateContactInfoComponent)(i0.ɵɵdirectiveInject(i1.FormBuilder), i0.ɵɵdirectiveInject(i0.ElementRef)); };
/** @nocollapse */ ValidateContactInfoComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: ValidateContactInfoComponent, selectors: [["id-validate-contact-info"]], inputs: { ui: "ui", dataEntry: "dataEntry" }, outputs: { dataOutput: "dataOutput" }, decls: 13, vars: 11, consts: [[3, "formGroup"], [1, "row"], [3, "ngClass"], [1, "form-group", "fg-icon", "fg-icon-action", 3, "ngClass"], ["type", "email", "formControlName", "email", "id", "email", "placeholder", "Correo electr\u00F3nico", "maxlength", "50", "autocomplete", "off", 1, "form-control", 3, "keydown.space", "blur"], [1, "material-icons"], ["for", "email"], ["href", "#", 3, "click", 4, "ngIf"], [4, "ngIf"], ["class", "col-sm-6", 4, "ngIf"], ["href", "#", 3, "click"], [1, "col-sm-6"], ["type", "text", "formControlName", "phone", "id", "phone", "placeholder", "Tel\u00E9fono celular", "maxlength", "9", "minlength", "9", 1, "form-control", 3, "keypress", "blur"], ["for", "phone"]], template: function ValidateContactInfoComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelementStart(3, "div", 3);
        i0.ɵɵelementStart(4, "input", 4);
        i0.ɵɵlistener("keydown.space", function ValidateContactInfoComponent_Template_input_keydown_space_4_listener($event) { return $event.preventDefault(); })("blur", function ValidateContactInfoComponent_Template_input_blur_4_listener() { return ctx.emitDynamicallyData(); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "i", 5);
        i0.ɵɵtext(6, "mail");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(7, "label", 6);
        i0.ɵɵtext(8, "Correo electro\u0301nico");
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(9, ValidateContactInfoComponent_a_9_Template, 3, 0, "a", 7);
        i0.ɵɵtemplate(10, ValidateContactInfoComponent_small_10_Template, 2, 0, "small", 8);
        i0.ɵɵtemplate(11, ValidateContactInfoComponent_small_11_Template, 2, 0, "small", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(12, ValidateContactInfoComponent_div_12_Template, 10, 6, "div", 9);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("formGroup", ctx.formValidateContactInfo);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(7, _c1, (ctx.ui == null ? null : ctx.ui.enablePhoneField) != false ? "col-sm-6" : "col-sm-12"));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(9, _c0, ctx.formValidateContactInfo.get("email").invalid && ctx.validationOfIssuedEmail));
        i0.ɵɵadvance(6);
        i0.ɵɵproperty("ngIf", ctx.formValidateContactInfo.get("email").disabled);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formValidateContactInfo.get("email").hasError("required") && !ctx.formValidateContactInfo.get("email").pristine);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formValidateContactInfo.get("email").hasError("pattern"));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", (ctx.ui == null ? null : ctx.ui.enablePhoneField) != false);
    } }, directives: [i1.NgControlStatusGroup, i1.FormGroupDirective, i2.NgClass, i1.DefaultValueAccessor, i1.NgControlStatus, i1.FormControlName, i1.MaxLengthValidator, i2.NgIf, i1.MinLengthValidator], styles: [".form-control[_ngcontent-%COMP%]:focus{color:#173181}.btn-outline-primary.disabled[_ngcontent-%COMP%], .btn-outline-primary[_ngcontent-%COMP%]:disabled{border-color:#ebebeb;color:#9ba5b7}[_nghost-%COMP%]     .form-control.ng-touched.ng-dirty.ng-invalid{border-color:#ed2c2d}[_nghost-%COMP%]     .ng-touched.ng-dirty.ng-invalid small{color:#ed2c2d}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ValidateContactInfoComponent, [{
        type: Component,
        args: [{
                selector: 'id-validate-contact-info',
                templateUrl: './validate-contact-info.component.html',
                styleUrls: ['./validate-contact-info.component.css']
            }]
    }], function () { return [{ type: i1.FormBuilder }, { type: i0.ElementRef }]; }, { ui: [{
            type: Input
        }], dataEntry: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGUtY29udGFjdC1pbmZvLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJDOi9Vc2Vycy9Vc3VhcmlvL0Rlc2t0b3AvbGlicmVyaWFuZ3VsYXItMjktMTAtMjEvY29waWEtYW5ndWxhci1saWItdXBkLTEyLXNpbi1ydXQvcHJvamVjdHMvaXNhcHJlLWRpZ2l0YWwvc3JjLyIsInNvdXJjZXMiOlsibGliL3ZhbGlkYXRlLWNvbnRhY3QtaW5mby92YWxpZGF0ZS1jb250YWN0LWluZm8uY29tcG9uZW50LnRzIiwibGliL3ZhbGlkYXRlLWNvbnRhY3QtaW5mby92YWxpZGF0ZS1jb250YWN0LWluZm8uY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBNkIsTUFBTSxlQUFlLENBQUM7QUFDMUcsT0FBTyxFQUEwQixVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7OztJQ016RSw2QkFBdUc7SUFBM0YsaU1BQTJCLE9BQU8sS0FBRTtJQUM5Qyw0QkFBMEI7SUFBQSxvQkFBSTtJQUFBLGlCQUFJO0lBQ3BDLGlCQUFJOzs7SUFDSiw2QkFBMkg7SUFBQSwyREFBc0M7SUFBQSxpQkFBUTs7O0lBQ3pLLDZCQUF3RTtJQUFBLGtFQUE2QztJQUFBLGlCQUFROzs7O0lBUTdILDZCQUF1RztJQUEzRiwwTUFBMkIsT0FBTyxLQUFFO0lBQzlDLDRCQUEwQjtJQUFBLG9CQUFJO0lBQUEsaUJBQUk7SUFDcEMsaUJBQUk7OztJQUNKLDZCQUF3STtJQUFBLCtDQUEwQjtJQUFBLGlCQUFROzs7SUFDMUssNkJBQXdFO0lBQUEsdURBQTZCO0lBQUEsaUJBQVE7Ozs7O0lBVGpILCtCQUE0RDtJQUMxRCw4QkFBd0g7SUFDdEgsaUNBQXFNO0lBQS9ELHFOQUErQiw2TEFBQTtJQUFySyxpQkFBcU07SUFDck0sNEJBQTBCO0lBQUEsMkJBQVc7SUFBQSxpQkFBSTtJQUN6QyxpQ0FBbUI7SUFBQSxxQ0FBZ0I7SUFBQSxpQkFBUTtJQUMzQyxnRkFFSTtJQUNKLHdGQUEwSztJQUMxSyx3RkFBNkc7SUFDL0csaUJBQU07SUFDUixpQkFBTTs7O0lBVjJDLGVBQXdFO0lBQXhFLHlHQUF3RTtJQUluRSxlQUFtRDtJQUFuRCwyRUFBbUQ7SUFHN0YsZUFBOEg7SUFBOUgsNkpBQThIO0lBQzlILGVBQThEO0lBQTlELHNGQUE4RDs7O0FEZDlFLE1BQU0sT0FBTyw0QkFBNEI7SUFRdkMsWUFDVSxHQUFnQixFQUNoQixHQUFlO1FBRGYsUUFBRyxHQUFILEdBQUcsQ0FBYTtRQUNoQixRQUFHLEdBQUgsR0FBRyxDQUFZO1FBTmYsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFpQyxDQUFDO1FBQ3pFLDRCQUF1QixHQUFXLEtBQUssQ0FBQztRQU90QyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDNUMsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0JBQzVELFVBQVUsRUFBRTtvQkFDVixVQUFVLENBQUMsUUFBUTtvQkFDbkIsVUFBVSxDQUFDLE9BQU8sQ0FBQyxzRUFBc0UsQ0FBQztpQkFDM0Y7Z0JBQ0QsUUFBUSxFQUFFLE1BQU07YUFDakIsQ0FBQztZQUNGLEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUM1RCxVQUFVLEVBQUU7b0JBQ1YsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUN2QixVQUFVLENBQUMsT0FBTyxDQUFDLDZCQUE2QixDQUFDO2lCQUNsRDtnQkFDRCxRQUFRLEVBQUUsTUFBTTthQUNqQixDQUFDO1NBQ0gsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFFBQVE7UUFFTixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUMvRixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxJQUFJLFNBQVMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQzthQUMzRztZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRTtnQkFDekIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUN6RDtpQkFBSTtnQkFDSCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFO29CQUNuRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO2lCQUMxRDtxQkFBSTtvQkFDSCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDO2lCQUNyQzthQUNGO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFO2dCQUN6QixJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ3pEO2lCQUFJO2dCQUNILElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUU7b0JBQ25ELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7aUJBQzFEO2FBQ0Y7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDcEYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUN4RCxJQUFJLENBQUMsdUJBQXVCLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUNwRixJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ3pEO1FBRUQsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELFNBQVMsQ0FBQyxLQUFZLEVBQUUsS0FBYTtRQUNuQyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQ2hFLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN0RCxvQkFBb0I7UUFDcEIsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2QsbURBQW1EO0lBQ3JELENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNkLE1BQU0sUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzdELElBQUksUUFBUSxHQUFHLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFO1lBQ3JELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxTQUFrQjtRQUNwQyxJQUFJLGFBQWEsR0FBRyxTQUFTLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQztRQUMvRixJQUFJLFVBQVUsR0FBRyxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUMsQ0FBQTtRQUM5QyxJQUFJLGNBQTZDLENBQUM7UUFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUU7WUFDekMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN6RCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN6SCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQzFEO1NBQ0Y7UUFDRCxjQUFjLG1DQUFRLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsR0FBSyxVQUFVLENBQUMsQ0FBQTtRQUNoRixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN2QyxDQUFDOzsySEE5RlUsNEJBQTRCOzhHQUE1Qiw0QkFBNEI7UUNUekMsOEJBQTJDO1FBQ3pDLDhCQUFpQjtRQUNmLDhCQUE4RTtRQUM1RSw4QkFBbUo7UUFDakosZ0NBQXdOO1FBQXBNLDhIQUFpQix1QkFBdUIsSUFBQyx5RkFBb0kseUJBQXFCLElBQXpKO1FBQTdELGlCQUF3TjtRQUN4Tiw0QkFBMEI7UUFBQSxvQkFBSTtRQUFBLGlCQUFJO1FBQ2xDLGdDQUFtQjtRQUFBLHdDQUFtQjtRQUFBLGlCQUFRO1FBQzlDLHlFQUVJO1FBQ0osbUZBQXlLO1FBQ3pLLG1GQUE2SDtRQUMvSCxpQkFBTTtRQUNSLGlCQUFNO1FBQ04sZ0ZBV007UUFDUixpQkFBTTtRQUNSLGlCQUFNOztRQTNCRCx1REFBcUM7UUFFakMsZUFBd0U7UUFBeEUsMklBQXdFO1FBQzVCLGVBQW1HO1FBQW5HLHFJQUFtRztRQUk5RixlQUFtRDtRQUFuRCx3RUFBbUQ7UUFHN0YsZUFBaUg7UUFBakgsMElBQWlIO1FBQ2pILGVBQThEO1FBQTlELG1GQUE4RDtRQUduRCxlQUFtQztRQUFuQyxpRkFBbUM7O3VGRExqRCw0QkFBNEI7Y0FMeEMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSwwQkFBMEI7Z0JBQ3BDLFdBQVcsRUFBRSx3Q0FBd0M7Z0JBQ3JELFNBQVMsRUFBRSxDQUFDLHVDQUF1QyxDQUFDO2FBQ3JEO3VGQUdVLEVBQUU7a0JBQVYsS0FBSztZQUNHLFNBQVM7a0JBQWpCLEtBQUs7WUFDSSxVQUFVO2tCQUFuQixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgRWxlbWVudFJlZiwgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzLCBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgVmFsaWRhdGVDb250YWN0SW5mb1VpLCBWYWxpZGF0ZUNvbnRhY3RJbmZvRGF0YSwgVmFsaWRhdGVDb250YWN0SW5mb091dHB1dERhdGEgfSBmcm9tICcuLi8uLi9tb2RlbHMvdmFsaWRhdGUtY29udGFjdC1pbmZvLm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnaWQtdmFsaWRhdGUtY29udGFjdC1pbmZvJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vdmFsaWRhdGUtY29udGFjdC1pbmZvLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi92YWxpZGF0ZS1jb250YWN0LWluZm8uY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBWYWxpZGF0ZUNvbnRhY3RJbmZvQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgdWk/OiBWYWxpZGF0ZUNvbnRhY3RJbmZvVWk7XHJcbiAgQElucHV0KCkgZGF0YUVudHJ5PzogVmFsaWRhdGVDb250YWN0SW5mb0RhdGE7XHJcbiAgQE91dHB1dCgpIGRhdGFPdXRwdXQgPSBuZXcgRXZlbnRFbWl0dGVyPFZhbGlkYXRlQ29udGFjdEluZm9PdXRwdXREYXRhPigpO1xyXG4gIHZhbGlkYXRpb25PZklzc3VlZEVtYWlsOmJvb2xlYW4gPSBmYWxzZTtcclxuICBwdWJsaWMgZm9ybVZhbGlkYXRlQ29udGFjdEluZm86IEZvcm1Hcm91cDtcclxuICBzdWJzY3JpcHRpb246IGFueTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2ZiOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgX2VsOiBFbGVtZW50UmVmXHJcbiAgKSB7XHJcbiAgICB0aGlzLmZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvID0gdGhpcy5fZmIuZ3JvdXAoe1xyXG4gICAgICBlbWFpbDogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHVuZGVmaW5lZCwgZGlzYWJsZWQ6IGZhbHNlIH0sIHtcclxuICAgICAgICB2YWxpZGF0b3JzOiBbXHJcbiAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxyXG4gICAgICAgICAgVmFsaWRhdG9ycy5wYXR0ZXJuKCdeKD8hWy5dKSg/IS4qWy1fLl0kKVthLXpBLVowLTlfListXStAW2EtekEtWjAtOS1dK1suXVthLXpBLVowLTktLl0rJCcpXHJcbiAgICAgICAgXSxcclxuICAgICAgICB1cGRhdGVPbjogJ2JsdXInXHJcbiAgICAgIH0pLFxyXG4gICAgICBwaG9uZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHVuZGVmaW5lZCwgZGlzYWJsZWQ6IGZhbHNlIH0sIHtcclxuICAgICAgICB2YWxpZGF0b3JzOiBbXHJcbiAgICAgICAgICBWYWxpZGF0b3JzLm1heExlbmd0aCg5KSxcclxuICAgICAgICAgIFZhbGlkYXRvcnMubWluTGVuZ3RoKDkpLFxyXG4gICAgICAgICAgVmFsaWRhdG9ycy5wYXR0ZXJuKCdeLT9bMC05XVxcXFxkKihcXFxcLlxcXFxkezEsMn0pPyQnKVxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgdXBkYXRlT246ICdibHVyJ1xyXG4gICAgICB9KVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuXHJcbiAgICBpZiAodGhpcy5kYXRhRW50cnkpIHtcclxuICAgICAgdGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5nZXQoJ2VtYWlsJykuc2V0VmFsdWUodGhpcy5kYXRhRW50cnkuZW1haWwsIHsgZW1pdEV2ZW50OiBmYWxzZSB9KTtcclxuICAgICAgaWYgKHRoaXMuZGF0YUVudHJ5LnBob25lICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHRoaXMuZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uZ2V0KCdwaG9uZScpLnNldFZhbHVlKHRoaXMuZGF0YUVudHJ5LnBob25lLnRvU3RyaW5nKCksIHsgZW1pdEV2ZW50OiBmYWxzZSB9KTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoIXRoaXMuZGF0YUVudHJ5LmVtYWlsKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5jb250cm9sc1snZW1haWwnXS5lbmFibGUoKTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgaWYgKHRoaXMuZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uZ2V0KCdlbWFpbCcpLnZhbGlkKSB7XHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmNvbnRyb2xzWydlbWFpbCddLmRpc2FibGUoKTtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIHRoaXMudmFsaWRhdGlvbk9mSXNzdWVkRW1haWwgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAoIXRoaXMuZGF0YUVudHJ5LnBob25lKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5jb250cm9sc1sncGhvbmUnXS5lbmFibGUoKTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgaWYgKHRoaXMuZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uZ2V0KCdwaG9uZScpLnZhbGlkKSB7XHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmNvbnRyb2xzWydwaG9uZSddLmRpc2FibGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uZ2V0KCdlbWFpbCcpLnNldFZhbHVlKHVuZGVmaW5lZCwgeyBlbWl0RXZlbnQ6IGZhbHNlIH0pO1xyXG4gICAgICB0aGlzLmZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmNvbnRyb2xzWydlbWFpbCddLmVuYWJsZSgpO1xyXG4gICAgICB0aGlzLmZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmdldCgncGhvbmUnKS5zZXRWYWx1ZSh1bmRlZmluZWQsIHsgZW1pdEV2ZW50OiBmYWxzZSB9KTtcclxuICAgICAgdGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5jb250cm9sc1sncGhvbmUnXS5lbmFibGUoKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmVtaXREeW5hbWljYWxseURhdGEoKTtcclxuICB9XHJcblxyXG4gIGVkaXRJbnB1dChldmVudDogRXZlbnQsIGlucHV0OiBzdHJpbmcpIHtcclxuICAgIGNvbnN0IGZpZWxkID0gdGhpcy5fZWwubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcjJyArIGlucHV0KTtcclxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB0aGlzLmVtaXREeW5hbWljYWxseURhdGEoZmFsc2UpO1xyXG4gICAgdGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5jb250cm9sc1tpbnB1dF0uZW5hYmxlKCk7XHJcbiAgICAvLyBzZXQgZm9jdXMgdG8gZWRpdFxyXG4gICAgZmllbGQuZm9jdXMoKTtcclxuICAgIC8vIHRoaXMuZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uZ2V0KGlucHV0KS5yZXNldCgpO1xyXG4gIH1cclxuXHJcbiAgbnVtYmVyT25seShldmVudCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgY2hhckNvZGUgPSAoZXZlbnQud2hpY2gpID8gZXZlbnQud2hpY2ggOiBldmVudC5rZXlDb2RlO1xyXG4gICAgaWYgKGNoYXJDb2RlID4gMzEgJiYgKGNoYXJDb2RlIDwgNDggfHwgY2hhckNvZGUgPiA1NykpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBlbWl0RHluYW1pY2FsbHlEYXRhKHN0YXRlRm9ybT86Ym9vbGVhbil7XHJcbiAgICBsZXQgZm9yY2VTYXRlRm9ybSA9IHN0YXRlRm9ybSAhPSB1bmRlZmluZWQgPyBzdGF0ZUZvcm0gOiAhdGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5pbnZhbGlkO1xyXG4gICAgbGV0IGZvcm1TdGF0dXMgPSB7ICd2YWxpZEZvcm0nOiBmb3JjZVNhdGVGb3JtfVxyXG4gICAgbGV0IGZvcm1EYXRhT3V0cHV0OiBWYWxpZGF0ZUNvbnRhY3RJbmZvT3V0cHV0RGF0YTtcclxuICAgIGlmICghdGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5pbnZhbGlkKSB7XHJcbiAgICAgIHRoaXMuZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uY29udHJvbHNbJ2VtYWlsJ10uZGlzYWJsZSgpO1xyXG4gICAgICBpZiAodGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5nZXQoJ3Bob25lJykudmFsdWUgIT0gbnVsbCAmJiB0aGlzLmZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmdldCgncGhvbmUnKS52YWx1ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtVmFsaWRhdGVDb250YWN0SW5mby5jb250cm9sc1sncGhvbmUnXS5kaXNhYmxlKCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGZvcm1EYXRhT3V0cHV0ID0geyAuLi50aGlzLmZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmdldFJhd1ZhbHVlKCksIC4uLmZvcm1TdGF0dXN9XHJcbiAgICB0aGlzLmRhdGFPdXRwdXQuZW1pdChmb3JtRGF0YU91dHB1dCk7XHJcbiAgfVxyXG59XHJcbiIsIjxkaXYgW2Zvcm1Hcm91cF09XCJmb3JtVmFsaWRhdGVDb250YWN0SW5mb1wiPlxyXG4gIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgIDxkaXYgW25nQ2xhc3NdPVwiWyB1aT8uZW5hYmxlUGhvbmVGaWVsZCAhPSBmYWxzZSA/ICdjb2wtc20tNicgOiAnY29sLXNtLTEyJyBdXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGZnLWljb24gZmctaWNvbi1hY3Rpb25cIiBbbmdDbGFzc109XCJ7J2ZnLWludmFsaWQnOiBmb3JtVmFsaWRhdGVDb250YWN0SW5mby5nZXQoJ2VtYWlsJykuaW52YWxpZCAmJiB2YWxpZGF0aW9uT2ZJc3N1ZWRFbWFpbH1cIj5cclxuICAgICAgICA8aW5wdXQgdHlwZT1cImVtYWlsXCIgKGtleWRvd24uc3BhY2UpPVwiJGV2ZW50LnByZXZlbnREZWZhdWx0KClcIiBmb3JtQ29udHJvbE5hbWU9XCJlbWFpbFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJlbWFpbFwiIHBsYWNlaG9sZGVyPVwiQ29ycmVvIGVsZWN0csOzbmljb1wiIG1heGxlbmd0aD1cIjUwXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgKGJsdXIpPVwiZW1pdER5bmFtaWNhbGx5RGF0YSgpXCI+XHJcbiAgICAgICAgPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPm1haWw8L2k+XHJcbiAgICAgICAgPGxhYmVsIGZvcj1cImVtYWlsXCI+Q29ycmVvIGVsZWN0cm/MgW5pY288L2xhYmVsPlxyXG4gICAgICAgIDxhIGhyZWY9XCIjXCIgKGNsaWNrKT1cImVkaXRJbnB1dCgkZXZlbnQsICdlbWFpbCcpXCIgKm5nSWY9XCJmb3JtVmFsaWRhdGVDb250YWN0SW5mby5nZXQoJ2VtYWlsJykuZGlzYWJsZWRcIj5cclxuICAgICAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5lZGl0PC9pPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgICA8c21hbGwgKm5nSWY9XCJmb3JtVmFsaWRhdGVDb250YWN0SW5mby5nZXQoJ2VtYWlsJykuaGFzRXJyb3IoJ3JlcXVpcmVkJykgJiYgIWZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmdldCgnZW1haWwnKS5wcmlzdGluZVwiPiogRWwgY29ycmVvIGVsZWN0csOzbmljbyBlcyBvYmxpZ2F0b3Jpbzwvc21hbGw+XHJcbiAgICAgICAgPHNtYWxsICpuZ0lmPVwiZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uZ2V0KCdlbWFpbCcpLmhhc0Vycm9yKCdwYXR0ZXJuJylcIj4qIERlYmVzIGluZ3Jlc2FyIHVuIGNvcnJlbyBlbGVjdHLDs25pY28gdmFsaWRvPC9zbWFsbD5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tNlwiICpuZ0lmPVwidWk/LmVuYWJsZVBob25lRmllbGQgIT0gZmFsc2VcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgZmctaWNvbiBmZy1pY29uLWFjdGlvblwiIFtuZ0NsYXNzXT1cInsnZmctaW52YWxpZCc6IGZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmdldCgncGhvbmUnKS5pbnZhbGlkfVwiPlxyXG4gICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGZvcm1Db250cm9sTmFtZT1cInBob25lXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cInBob25lXCIgcGxhY2Vob2xkZXI9XCJUZWzDqWZvbm8gY2VsdWxhclwiIG1heGxlbmd0aD1cIjlcIiBtaW5sZW5ndGg9XCI5XCIgKGtleXByZXNzKT1cIm51bWJlck9ubHkoJGV2ZW50KVwiIChibHVyKT1cImVtaXREeW5hbWljYWxseURhdGEoKVwiPlxyXG4gICAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5sb2NhbF9waG9uZTwvaT5cclxuICAgICAgICA8bGFiZWwgZm9yPVwicGhvbmVcIj5UZWzDqWZvbm8gY2VsdWxhcjwvbGFiZWw+XHJcbiAgICAgICAgPGEgaHJlZj1cIiNcIiAoY2xpY2spPVwiZWRpdElucHV0KCRldmVudCwgJ3Bob25lJylcIiAqbmdJZj1cImZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmdldCgncGhvbmUnKS5kaXNhYmxlZFwiPlxyXG4gICAgICAgICAgPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPmVkaXQ8L2k+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICAgIDxzbWFsbCAqbmdJZj1cImZvcm1WYWxpZGF0ZUNvbnRhY3RJbmZvLmdldCgncGhvbmUnKS5oYXNFcnJvcignbWlubGVuZ3RoJykgfHwgZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uZ2V0KCdwaG9uZScpLmhhc0Vycm9yKCdtYXhsZW5ndGgnKVwiPiogRGViZXMgaW5ncmVzYXIgOSBkw61naXRvczwvc21hbGw+XHJcbiAgICAgICAgPHNtYWxsICpuZ0lmPVwiZm9ybVZhbGlkYXRlQ29udGFjdEluZm8uZ2V0KCdwaG9uZScpLmhhc0Vycm9yKCdwYXR0ZXJuJylcIj4qIERlYmVzIGluZ3Jlc2FyIHPDs2xvIG7Dum1lcm9zPC9zbWFsbD5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC9kaXY+XHJcbiJdfQ==