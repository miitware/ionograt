import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
function PagerComponent_nav_0_li_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 8);
    i0.ɵɵelementStart(1, "a", 4);
    i0.ɵɵtext(2, "...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
const _c0 = function (a0, a1) { return { "active": a0, "hide": a1 }; };
function PagerComponent_nav_0_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    const _r6 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_ng_container_10_Template_li_click_1_listener() { const restoredCtx = i0.ɵɵrestoreView(_r6); const page_r4 = restoredCtx.$implicit; const ctx_r5 = i0.ɵɵnextContext(2); return ctx_r5.goToPage(page_r4); });
    i0.ɵɵelementStart(2, "a", 4);
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const page_r4 = ctx.$implicit;
    const ctx_r2 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction2(2, _c0, page_r4 === (ctx_r2.pagerOption == null ? null : ctx_r2.pagerOption.currentPage), page_r4 == 1 || page_r4 == (ctx_r2.pagerOption == null ? null : ctx_r2.pagerOption.totalPages)));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(page_r4);
} }
function PagerComponent_nav_0_li_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 8);
    i0.ɵɵelementStart(1, "a", 4);
    i0.ɵɵtext(2, "...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
const _c1 = function (a0) { return { "disabled": a0 }; };
const _c2 = function (a0) { return { "active": a0 }; };
function PagerComponent_nav_0_Template(rf, ctx) { if (rf & 1) {
    const _r8 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "nav", 1);
    i0.ɵɵelementStart(1, "ul", 2);
    i0.ɵɵelementStart(2, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_Template_li_click_2_listener() { i0.ɵɵrestoreView(_r8); const ctx_r7 = i0.ɵɵnextContext(); return ctx_r7.goToPage((ctx_r7.pagerOption == null ? null : ctx_r7.pagerOption.currentPage) - 1); });
    i0.ɵɵelementStart(3, "a", 4);
    i0.ɵɵelementStart(4, "span", 5);
    i0.ɵɵtext(5, "keyboard_arrow_left");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_Template_li_click_6_listener() { i0.ɵɵrestoreView(_r8); const ctx_r9 = i0.ɵɵnextContext(); return ctx_r9.goToPage(1); });
    i0.ɵɵelementStart(7, "a", 4);
    i0.ɵɵtext(8, "1");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(9, PagerComponent_nav_0_li_9_Template, 3, 0, "li", 6);
    i0.ɵɵtemplate(10, PagerComponent_nav_0_ng_container_10_Template, 4, 5, "ng-container", 7);
    i0.ɵɵtemplate(11, PagerComponent_nav_0_li_11_Template, 3, 0, "li", 6);
    i0.ɵɵelementStart(12, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_Template_li_click_12_listener() { i0.ɵɵrestoreView(_r8); const ctx_r10 = i0.ɵɵnextContext(); return ctx_r10.goToPage(ctx_r10.pagerOption.totalPages); });
    i0.ɵɵelementStart(13, "a", 4);
    i0.ɵɵtext(14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(15, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_Template_li_click_15_listener() { i0.ɵɵrestoreView(_r8); const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.goToPage((ctx_r11.pagerOption == null ? null : ctx_r11.pagerOption.currentPage) + 1); });
    i0.ɵɵelementStart(16, "a", 4);
    i0.ɵɵelementStart(17, "span", 5);
    i0.ɵɵtext(18, " keyboard_arrow_right ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(8, _c1, (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) === 1));
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(10, _c2, (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) === 1));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.totalPages) > ctx_r0.maxPages && (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) > ctx_r0.maxPages - ctx_r0.siblingPage);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.pages);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.totalPages) > ctx_r0.maxPages && (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) < ctx_r0.pagerOption.totalPages - ctx_r0.siblingPage);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(12, _c2, (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) === ctx_r0.pagerOption.totalPages));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r0.pagerOption.totalPages);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(14, _c1, (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) === (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.totalPages)));
} }
export class PagerComponent {
    constructor() {
        this.currentPage = 1;
        this.perPage = 10;
        this.pageChange = new EventEmitter();
        this.maxPages = 5;
        this.siblingPage = 2;
        this.pagerOption = {
            currentPage: this.currentPage,
            perPage: this.perPage
        };
    }
    ngOnInit() {
    }
    ngOnChanges(changes) {
        const newValue = changes.totalItems;
        if (newValue) {
            this.pagerOption.totalPages = Math.ceil(this.totalItems / this.perPage);
            if (this.pagerOption.totalPages <= this.maxPages) {
                this.pagerOption.pages = Array.apply(null, Array(this.pagerOption.totalPages)).map((x, i) => i + 1);
            }
            else {
                this.calculePages();
            }
            this.goToPage(1);
        }
    }
    goToPage(page) {
        this.pagerOption.currentPage = page;
        const since = (this.pagerOption.currentPage - 1) * this.pagerOption.perPage;
        const until = this.pagerOption.currentPage * this.pagerOption.perPage;
        const ouput = { since, until };
        this.pageChange.emit(ouput);
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        if (this.pagerOption.totalPages > this.maxPages) {
            this.calculePages();
        }
    }
    calculePages() {
        let since = this.pagerOption.currentPage - this.siblingPage;
        let until = this.pagerOption.currentPage + this.siblingPage;
        if (since <= 0) {
            since = 1;
            until = since + (this.maxPages - 1);
        }
        else if (until > this.pagerOption.totalPages) {
            until = this.pagerOption.totalPages;
            since = this.pagerOption.totalPages - (this.maxPages - 1);
        }
        const pages = [];
        for (let i = since; i <= until; i++) {
            pages.push(i);
        }
        this.pagerOption.pages = pages;
    }
}
/** @nocollapse */ PagerComponent.ɵfac = function PagerComponent_Factory(t) { return new (t || PagerComponent)(); };
/** @nocollapse */ PagerComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: PagerComponent, selectors: [["id-pager"]], inputs: { totalItems: "totalItems", currentPage: "currentPage", perPage: "perPage" }, outputs: { pageChange: "pageChange" }, features: [i0.ɵɵNgOnChangesFeature], decls: 1, vars: 1, consts: [["aria-label", "pager", 4, "ngIf"], ["aria-label", "pager"], [1, "pagination", "justify-content-center", "m-0"], [1, "page-item", 3, "ngClass", "click"], ["href", "javascript:", 1, "page-link"], [1, "material-icons"], ["class", "page-item disabled xs-hide", 4, "ngIf"], [4, "ngFor", "ngForOf"], [1, "page-item", "disabled", "xs-hide"]], template: function PagerComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, PagerComponent_nav_0_Template, 19, 16, "nav", 0);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", (ctx.pagerOption == null ? null : ctx.pagerOption.totalPages) > 1);
    } }, directives: [i1.NgIf, i1.NgClass, i1.NgForOf], styles: ["@media (min-width:480px){.hide[_ngcontent-%COMP%]{display:none}}@media (max-width:480px){.pagination[_ngcontent-%COMP%]   .page-item[_ngcontent-%COMP%]:nth-last-child(-n+2), .xs-hide[_ngcontent-%COMP%]{display:none}.pagination[_ngcontent-%COMP%]   .page-item[_ngcontent-%COMP%]:last-child{display:flex}.pagination[_ngcontent-%COMP%]   .page-item[_ngcontent-%COMP%]:nth-child(2){display:none}}.page-link[_ngcontent-%COMP%]:focus{box-shadow:none}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(PagerComponent, [{
        type: Component,
        args: [{
                selector: 'id-pager',
                templateUrl: './pager.component.html',
                styleUrls: ['./pager.component.css'],
            }]
    }], function () { return []; }, { totalItems: [{
            type: Input
        }], currentPage: [{
            type: Input
        }], perPage: [{
            type: Input
        }], pageChange: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IkM6L1VzZXJzL1VzdWFyaW8vRGVza3RvcC9saWJyZXJpYW5ndWxhci0yOS0xMC0yMS9jb3BpYS1hbmd1bGFyLWxpYi11cGQtMTItc2luLXJ1dC9wcm9qZWN0cy9pc2FwcmUtZGlnaXRhbC9zcmMvIiwic291cmNlcyI6WyJsaWIvcGFnZXIvcGFnZXIuY29tcG9uZW50LnRzIiwibGliL3BhZ2VyL3BhZ2VyLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQTRCLE1BQU0sZUFBZSxDQUFDOzs7O0lDVXJHLDZCQUF5STtJQUN2SSw0QkFBd0M7SUFBQSxtQkFBRztJQUFBLGlCQUFJO0lBQ2pELGlCQUFLOzs7OztJQUNMLDZCQUF1RDtJQUNyRCw2QkFBZ0s7SUFBMUksK1BBQXdCO0lBQzVDLDRCQUF3QztJQUFBLFlBQVU7SUFBQSxpQkFBSTtJQUN4RCxpQkFBSztJQUNQLDBCQUFlOzs7O0lBSGtDLGVBQWdIO0lBQWhILHNPQUFnSDtJQUNySCxlQUFVO0lBQVYsNkJBQVU7OztJQUd0RCw2QkFBdUo7SUFDckosNEJBQXdDO0lBQUEsbUJBQUc7SUFBQSxpQkFBSTtJQUNqRCxpQkFBSzs7Ozs7O0lBcEJULDhCQUE0RDtJQUMxRCw2QkFBa0Q7SUFDaEQsNkJBQWdJO0lBQWpELDhPQUE2QyxDQUFDLEtBQUU7SUFDN0gsNEJBQXdDO0lBQ3RDLCtCQUE2QjtJQUFBLG1DQUFtQjtJQUFBLGlCQUFPO0lBQ3pELGlCQUFJO0lBQ04saUJBQUs7SUFDTCw2QkFBb0c7SUFBOUUsdUtBQWtCLENBQUMsS0FBRTtJQUN6Qyw0QkFBd0M7SUFBQSxpQkFBQztJQUFBLGlCQUFJO0lBQy9DLGlCQUFLO0lBQ0wsbUVBRUs7SUFDTCx5RkFJZTtJQUNmLHFFQUVLO0lBQ0wsOEJBQTZJO0lBQXZILDZNQUEwQztJQUM5RCw2QkFBd0M7SUFBQSxhQUEwQjtJQUFBLGlCQUFJO0lBQ3hFLGlCQUFLO0lBQ0wsOEJBQXNKO0lBQWpELG1QQUE2QyxDQUFDLEtBQUU7SUFDbkosNkJBQXdDO0lBQ3RDLGdDQUE2QjtJQUMzQix1Q0FDRjtJQUFBLGlCQUFPO0lBQ1QsaUJBQUk7SUFDTixpQkFBSztJQUNQLGlCQUFLO0lBQ1AsaUJBQU07OztJQTlCb0IsZUFBd0Q7SUFBeEQsZ0lBQXdEO0lBS2xDLGVBQXVEO0lBQXZELGlJQUF1RDtJQUczRCxlQUErRjtJQUEvRiwyTkFBK0Y7SUFHeEcsZUFBc0I7SUFBdEIsc0ZBQXNCO0lBS2IsZUFBNkc7SUFBN0cseU9BQTZHO0lBR3BGLGVBQTJFO0lBQTNFLDZKQUEyRTtJQUNsRyxlQUEwQjtJQUExQixtREFBMEI7SUFFOUMsZUFBOEU7SUFBOUUsbU1BQThFOztBRGpCeEcsTUFBTSxPQUFPLGNBQWM7SUFTekI7UUFOUyxnQkFBVyxHQUFHLENBQUMsQ0FBQztRQUNoQixZQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ1osZUFBVSxHQUFpQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pFLGFBQVEsR0FBRyxDQUFDLENBQUM7UUFFcEIsZ0JBQVcsR0FBVSxDQUFDLENBQUM7UUFFckIsSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNqQixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7WUFDN0IsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPO1NBQ3RCLENBQUM7SUFDSixDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQztRQUNwQyxJQUFJLFFBQVEsRUFBRTtZQUNaLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNoRCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQU0sRUFBRSxDQUFTLEVBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUNsSDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDckI7WUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQztJQUVNLFFBQVEsQ0FBQyxJQUFZO1FBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUNwQyxNQUFNLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1FBQzVFLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1FBRXRFLE1BQU0sS0FBSyxHQUFtQixFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQztRQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU1QixRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxhQUFhO1FBQzFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLG9DQUFvQztRQUU1RSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDL0MsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQztJQUVPLFlBQVk7UUFDbEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM1RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVELElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNkLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDVixLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUNyQzthQUFNLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFO1lBQzlDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQztZQUNwQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQzNEO1FBQ0QsTUFBTSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBRWpCLEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsSUFBSSxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbkMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNmO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7OytGQWhFVSxjQUFjO2dHQUFkLGNBQWM7UUNQM0IsaUVBZ0NNOztRQWhDbUIsd0ZBQWlDOzt1RkRPN0MsY0FBYztjQUwxQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLFdBQVcsRUFBRSx3QkFBd0I7Z0JBQ3JDLFNBQVMsRUFBRSxDQUFDLHVCQUF1QixDQUFDO2FBQ3JDO3NDQUdVLFVBQVU7a0JBQWxCLEtBQUs7WUFDRyxXQUFXO2tCQUFuQixLQUFLO1lBQ0csT0FBTztrQkFBZixLQUFLO1lBQ0ksVUFBVTtrQkFBbkIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJUGFnaW5hdGVPcHRpb24sIElQYWdpbmF0ZU91cHV0IH0gZnJvbSAnLi4vLi4vbW9kZWxzL3BhZ2VyLm1vZGVsJztcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdpZC1wYWdlcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3BhZ2VyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9wYWdlci5jb21wb25lbnQuY3NzJ10sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQYWdlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgdG90YWxJdGVtcz86IG51bWJlcjtcclxuICBASW5wdXQoKSBjdXJyZW50UGFnZT89IDE7XHJcbiAgQElucHV0KCkgcGVyUGFnZT89IDEwO1xyXG4gIEBPdXRwdXQoKSBwYWdlQ2hhbmdlOiBFdmVudEVtaXR0ZXI8SVBhZ2luYXRlT3VwdXQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIHB1YmxpYyBtYXhQYWdlcyA9IDU7XHJcbiAgcHVibGljIHBhZ2VyT3B0aW9uOiBJUGFnaW5hdGVPcHRpb247XHJcbiAgc2libGluZ1BhZ2U6bnVtYmVyID0gMjtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMucGFnZXJPcHRpb24gPSB7XHJcbiAgICAgIGN1cnJlbnRQYWdlOiB0aGlzLmN1cnJlbnRQYWdlLFxyXG4gICAgICBwZXJQYWdlOiB0aGlzLnBlclBhZ2VcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGNvbnN0IG5ld1ZhbHVlID0gY2hhbmdlcy50b3RhbEl0ZW1zO1xyXG4gICAgaWYgKG5ld1ZhbHVlKSB7XHJcbiAgICAgIHRoaXMucGFnZXJPcHRpb24udG90YWxQYWdlcyA9IE1hdGguY2VpbCh0aGlzLnRvdGFsSXRlbXMgLyB0aGlzLnBlclBhZ2UpO1xyXG4gICAgICBpZiAodGhpcy5wYWdlck9wdGlvbi50b3RhbFBhZ2VzIDw9IHRoaXMubWF4UGFnZXMpIHtcclxuICAgICAgICB0aGlzLnBhZ2VyT3B0aW9uLnBhZ2VzID0gQXJyYXkuYXBwbHkobnVsbCwgQXJyYXkodGhpcy5wYWdlck9wdGlvbi50b3RhbFBhZ2VzKSkubWFwKCh4OiBhbnksIGk6IG51bWJlcikgPT4gaSArIDEpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuY2FsY3VsZVBhZ2VzKCk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5nb1RvUGFnZSgxKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnb1RvUGFnZShwYWdlOiBudW1iZXIpIHtcclxuICAgIHRoaXMucGFnZXJPcHRpb24uY3VycmVudFBhZ2UgPSBwYWdlO1xyXG4gICAgY29uc3Qgc2luY2UgPSAodGhpcy5wYWdlck9wdGlvbi5jdXJyZW50UGFnZSAtIDEpICogdGhpcy5wYWdlck9wdGlvbi5wZXJQYWdlO1xyXG4gICAgY29uc3QgdW50aWwgPSB0aGlzLnBhZ2VyT3B0aW9uLmN1cnJlbnRQYWdlICogdGhpcy5wYWdlck9wdGlvbi5wZXJQYWdlO1xyXG5cclxuICAgIGNvbnN0IG91cHV0OiBJUGFnaW5hdGVPdXB1dCA9IHsgc2luY2UsIHVudGlsIH07XHJcbiAgICB0aGlzLnBhZ2VDaGFuZ2UuZW1pdChvdXB1dCk7XHJcblxyXG4gICAgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgPSAwOyAvLyBGb3IgU2FmYXJpXHJcbiAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wID0gMDsgLy8gRm9yIENocm9tZSwgRmlyZWZveCwgSUUgYW5kIE9wZXJhXHJcblxyXG4gICAgaWYgKHRoaXMucGFnZXJPcHRpb24udG90YWxQYWdlcyA+IHRoaXMubWF4UGFnZXMpIHtcclxuICAgICAgdGhpcy5jYWxjdWxlUGFnZXMoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgY2FsY3VsZVBhZ2VzKCkge1xyXG4gICAgbGV0IHNpbmNlID0gdGhpcy5wYWdlck9wdGlvbi5jdXJyZW50UGFnZSAtIHRoaXMuc2libGluZ1BhZ2U7XHJcbiAgICBsZXQgdW50aWwgPSB0aGlzLnBhZ2VyT3B0aW9uLmN1cnJlbnRQYWdlICsgdGhpcy5zaWJsaW5nUGFnZTtcclxuICAgIGlmIChzaW5jZSA8PSAwKSB7XHJcbiAgICAgIHNpbmNlID0gMTtcclxuICAgICAgdW50aWwgPSBzaW5jZSArICh0aGlzLm1heFBhZ2VzIC0gMSk7XHJcbiAgICB9IGVsc2UgaWYgKHVudGlsID4gdGhpcy5wYWdlck9wdGlvbi50b3RhbFBhZ2VzKSB7XHJcbiAgICAgIHVudGlsID0gdGhpcy5wYWdlck9wdGlvbi50b3RhbFBhZ2VzO1xyXG4gICAgICBzaW5jZSA9IHRoaXMucGFnZXJPcHRpb24udG90YWxQYWdlcyAtICh0aGlzLm1heFBhZ2VzIC0gMSk7XHJcbiAgICB9XHJcbiAgICBjb25zdCBwYWdlcyA9IFtdO1xyXG5cclxuICAgIGZvciAobGV0IGkgPSBzaW5jZTsgaSA8PSB1bnRpbDsgaSsrKSB7XHJcbiAgICAgIHBhZ2VzLnB1c2goaSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnBhZ2VyT3B0aW9uLnBhZ2VzID0gcGFnZXM7XHJcbiAgfVxyXG59XHJcbiIsIjxuYXYgYXJpYS1sYWJlbD1cInBhZ2VyXCIgKm5nSWY9XCJwYWdlck9wdGlvbj8udG90YWxQYWdlcyA+IDFcIj5cclxuICA8dWwgY2xhc3M9XCJwYWdpbmF0aW9uIGp1c3RpZnktY29udGVudC1jZW50ZXIgbS0wXCI+XHJcbiAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW1cIiBbbmdDbGFzc109XCJ7J2Rpc2FibGVkJzogcGFnZXJPcHRpb24/LmN1cnJlbnRQYWdlID09PSAxfVwiIChjbGljayk9XCJnb1RvUGFnZShwYWdlck9wdGlvbj8uY3VycmVudFBhZ2UgLSAxKVwiPlxyXG4gICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDpcIiBjbGFzcz1cInBhZ2UtbGlua1wiPlxyXG4gICAgICAgIDxzcGFuIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5rZXlib2FyZF9hcnJvd19sZWZ0PC9zcGFuPlxyXG4gICAgICA8L2E+XHJcbiAgICA8L2xpPlxyXG4gICAgPGxpIGNsYXNzPVwicGFnZS1pdGVtXCIgKGNsaWNrKT1cImdvVG9QYWdlKDEpXCIgW25nQ2xhc3NdPVwieydhY3RpdmUnOiBwYWdlck9wdGlvbj8uY3VycmVudFBhZ2UgPT09IDEgfVwiPlxyXG4gICAgICA8YSBjbGFzcz1cInBhZ2UtbGlua1wiIGhyZWY9XCJqYXZhc2NyaXB0OlwiPjE8L2E+XHJcbiAgICA8L2xpPlxyXG4gICAgPGxpIGNsYXNzPVwicGFnZS1pdGVtIGRpc2FibGVkIHhzLWhpZGVcIiAqbmdJZj1cInBhZ2VyT3B0aW9uPy50b3RhbFBhZ2VzID4gbWF4UGFnZXMgJiYgcGFnZXJPcHRpb24/LmN1cnJlbnRQYWdlID4gKG1heFBhZ2VzIC0gc2libGluZ1BhZ2UpXCI+XHJcbiAgICAgIDxhIGNsYXNzPVwicGFnZS1saW5rXCIgaHJlZj1cImphdmFzY3JpcHQ6XCI+Li4uPC9hPlxyXG4gICAgPC9saT5cclxuICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IHBhZ2Ugb2YgcGFnZXJPcHRpb24/LnBhZ2VzO1wiPlxyXG4gICAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW1cIiAoY2xpY2spPVwiZ29Ub1BhZ2UocGFnZSlcIiBbbmdDbGFzc109XCJ7J2FjdGl2ZSc6IHBhZ2UgPT09IHBhZ2VyT3B0aW9uPy5jdXJyZW50UGFnZSwgJ2hpZGUnIDogcGFnZSA9PSAxIHx8IHBhZ2UgPT0gcGFnZXJPcHRpb24/LnRvdGFsUGFnZXN9XCI+XHJcbiAgICAgICAgPGEgY2xhc3M9XCJwYWdlLWxpbmtcIiBocmVmPVwiamF2YXNjcmlwdDpcIj57eyBwYWdlIH19PC9hPlxyXG4gICAgICA8L2xpPlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcbiAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW0gZGlzYWJsZWQgeHMtaGlkZVwiICpuZ0lmPVwicGFnZXJPcHRpb24/LnRvdGFsUGFnZXMgPiBtYXhQYWdlcyAmJiBwYWdlck9wdGlvbj8uY3VycmVudFBhZ2UgPCAocGFnZXJPcHRpb24udG90YWxQYWdlcyAtIHNpYmxpbmdQYWdlKVwiPlxyXG4gICAgICA8YSBjbGFzcz1cInBhZ2UtbGlua1wiIGhyZWY9XCJqYXZhc2NyaXB0OlwiPi4uLjwvYT5cclxuICAgIDwvbGk+XHJcbiAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW1cIiAoY2xpY2spPVwiZ29Ub1BhZ2UocGFnZXJPcHRpb24udG90YWxQYWdlcylcIiBbbmdDbGFzc109XCJ7J2FjdGl2ZSc6IHBhZ2VyT3B0aW9uPy5jdXJyZW50UGFnZSA9PT0gcGFnZXJPcHRpb24udG90YWxQYWdlc31cIj5cclxuICAgICAgPGEgY2xhc3M9XCJwYWdlLWxpbmtcIiBocmVmPVwiamF2YXNjcmlwdDpcIj57e3BhZ2VyT3B0aW9uLnRvdGFsUGFnZXN9fTwvYT5cclxuICAgIDwvbGk+XHJcbiAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW1cIiBbbmdDbGFzc109XCJ7J2Rpc2FibGVkJzogcGFnZXJPcHRpb24/LmN1cnJlbnRQYWdlID09PSBwYWdlck9wdGlvbj8udG90YWxQYWdlc31cIiAoY2xpY2spPVwiZ29Ub1BhZ2UocGFnZXJPcHRpb24/LmN1cnJlbnRQYWdlICsgMSlcIj5cclxuICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6XCIgY2xhc3M9XCJwYWdlLWxpbmtcIj5cclxuICAgICAgICA8c3BhbiBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCI+XHJcbiAgICAgICAgICBrZXlib2FyZF9hcnJvd19yaWdodFxyXG4gICAgICAgIDwvc3Bhbj5cclxuICAgICAgPC9hPlxyXG4gICAgPC9saT5cclxuICA8L3VsPlxyXG48L25hdj5cclxuIl19