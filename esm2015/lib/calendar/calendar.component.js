import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from 'saturn-datepicker';
import * as moment from 'moment';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/material/input";
import * as i4 from "saturn-datepicker";
import * as i5 from "angular2-text-mask";
const _c0 = ["calendario"];
const _c1 = ["calendar"];
const _c2 = function (a0) { return { mask: a0, guide: false }; };
function CalendarComponent_ng_container_0_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "mat-form-field", 1);
    i0.ɵɵelementStart(2, "mat-label");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "input", 2, 3);
    i0.ɵɵlistener("dateChange", function CalendarComponent_ng_container_0_Template_input_dateChange_4_listener($event) { i0.ɵɵrestoreView(_r5); const ctx_r4 = i0.ɵɵnextContext(); return ctx_r4.dateChange($event); })("keypress", function CalendarComponent_ng_container_0_Template_input_keypress_4_listener($event) { i0.ɵɵrestoreView(_r5); const ctx_r6 = i0.ɵɵnextContext(); return ctx_r6.validKey($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "sat-datepicker", 4, 5);
    i0.ɵɵlistener("opened", function CalendarComponent_ng_container_0_Template_sat_datepicker_opened_6_listener() { i0.ɵɵrestoreView(_r5); const ctx_r7 = i0.ɵɵnextContext(); return ctx_r7.setInitialDate(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelement(8, "sat-datepicker-toggle", 6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const _r3 = i0.ɵɵreference(7);
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("mat-focused", ctx_r0.rangestate)("mat-error", !ctx_r0.validDate);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r0.ui.label ? ctx_r0.ui.label : "Selecciona");
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("placeholder", ctx_r0.ui.placeholder);
    i0.ɵɵproperty("satDatepicker", _r3)("textMask", i0.ɵɵpureFunction1(15, _c2, ctx_r0.ui.range ? ctx_r0.maskRange : ctx_r0.maskSingle))("min", ctx_r0.ui.min)("max", ctx_r0.ui.max)("value", ctx_r0.initialDate ? ctx_r0.initialDate : "")("disabled", ctx_r0.ui.disabled);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("rangeMode", ctx_r0.ui == null ? null : ctx_r0.ui.range)("selectFirstDateOnClose", true);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("for", _r3);
} }
function CalendarComponent_section_1_Template(rf, ctx) { if (rf & 1) {
    const _r9 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section");
    i0.ɵɵelementStart(1, "div", 7);
    i0.ɵɵelementStart(2, "sat-calendar", 8);
    i0.ɵɵlistener("dateRangesChange", function CalendarComponent_section_1_Template_sat_calendar_dateRangesChange_2_listener($event) { i0.ɵɵrestoreView(_r9); const ctx_r8 = i0.ɵɵnextContext(); return ctx_r8.inlineRangeChange($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("rangeMode", true)("selected", ctx_r1.selectedDate);
} }
export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'DD MMMM YYYY'
    },
};
export class CalendarComponent {
    constructor() {
        this.maskSingle = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        this.maskRange = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        // dateRange;
        this.ui = {
            range: false,
            inlineCalendar: false,
            disabled: false,
            min: null,
            max: null,
            label: 'Fecha',
            placeholder: 'Fecha'
        };
        this.dataOutput = new EventEmitter();
        this.rangestate = false;
        this.validDate = true;
    }
    set initialDate(value) {
        this._InitialDate = value;
        this.updateInitialDate(this._InitialDate);
    }
    get initialDate() {
        return this._InitialDate;
    }
    ngOnInit() { }
    validKey($event) {
        const value = ($event.which) ? $event.which : $event.keyCode;
        if (value == 8) {
            return true;
        }
        else if (value >= 48 && value <= 57 || value == 45 || value == 47) {
            return true;
        }
        else {
            return false;
        }
    }
    dateChange($event) {
        // console.log($event);
        const inputDate = $event.target.value;
        const range = { begin: null, end: null };
        if (inputDate != null) {
            this.validDate = true;
            if (moment.isMoment(inputDate)) {
                range.begin = inputDate.format('DD/MM/YYYY');
            }
            else {
                range.begin = inputDate.begin.format('DD/MM/YYYY');
                range.end = inputDate.end.format('DD/MM/YYYY');
            }
            // this.dataOutput.emit(range);
        }
        else {
            this.validDate = false;
        }
        this.dataOutput.emit(range);
    }
    inlineRangeChange($event) {
        const range = { begin: null, end: null };
        range.begin = $event.begin.format('DD/MM/YYYY');
        range.end = $event.end ? $event.end.format('DD/MM/YYYY') : null;
        this.dataOutput.emit(range);
    }
    updateInitialDate(date) {
        // console.log('updateInitialDate');
        // console.log(date);
        if (this.ui.limitedRange) {
            this.ui.min = date.begin;
        }
    }
    selectedBeginDate(event) {
        // console.log('selectedBeginDate');
        // console.log(event);
    }
    setInitialDate() {
        // console.log('setInitialDate');
        // setTimeout(() => {
        //   console.log(this.calendario.nativeElement);
        // }, 1000);
    }
}
/** @nocollapse */ CalendarComponent.ɵfac = function CalendarComponent_Factory(t) { return new (t || CalendarComponent)(); };
/** @nocollapse */ CalendarComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: CalendarComponent, selectors: [["id-calendar"]], viewQuery: function CalendarComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, 5);
        i0.ɵɵviewQuery(_c1, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.calendario = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.calendar = _t.first);
    } }, inputs: { ui: "ui", initialDate: "initialDate" }, outputs: { dataOutput: "dataOutput" }, features: [i0.ɵɵProvidersFeature([
            { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
            { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
            { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
        ])], decls: 2, vars: 2, consts: [[4, "ngIf"], ["appendTo", "body"], ["matInput", "", 3, "placeholder", "satDatepicker", "textMask", "min", "max", "value", "disabled", "dateChange", "keypress"], ["calendario", ""], [3, "rangeMode", "selectFirstDateOnClose", "opened"], ["calendar", ""], ["matSuffix", "", 3, "for"], [1, "inlineCalendarContainer"], [3, "rangeMode", "selected", "dateRangesChange"]], template: function CalendarComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, CalendarComponent_ng_container_0_Template, 9, 17, "ng-container", 0);
        i0.ɵɵtemplate(1, CalendarComponent_section_1_Template, 3, 2, "section", 0);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", !ctx.ui.inlineCalendar);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.ui.inlineCalendar);
    } }, directives: [i1.NgIf, i2.MatFormField, i2.MatLabel, i3.MatInput, i4.SatDatepickerInput, i5.MaskedInputDirective, i4.SatDatepicker, i4.SatDatepickerToggle, i2.MatSuffix, i4.SatCalendar], styles: [""] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(CalendarComponent, [{
        type: Component,
        args: [{
                selector: 'id-calendar',
                templateUrl: './calendar.component.html',
                styleUrls: ['./calendar.component.css'],
                providers: [
                    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
                    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
                    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
                ]
            }]
    }], function () { return []; }, { calendario: [{
            type: ViewChild,
            args: ['calendario', { static: false }]
        }], calendar: [{
            type: ViewChild,
            args: ['calendar', { static: false }]
        }], ui: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }], initialDate: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FsZW5kYXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IkM6L1VzZXJzL1VzdWFyaW8vRGVza3RvcC9saWJyZXJpYW5ndWxhci0yOS0xMC0yMS9jb3BpYS1hbmd1bGFyLWxpYi11cGQtMTItc2luLXJ1dC9wcm9qZWN0cy9pc2FwcmUtZGlnaXRhbC9zcmMvIiwic291cmNlcyI6WyJsaWIvY2FsZW5kYXIvY2FsZW5kYXIuY29tcG9uZW50LnRzIiwibGliL2NhbGVuZGFyL2NhbGVuZGFyLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQWMsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3RHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFbkYsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7Ozs7Ozs7Ozs7OztJQ0pqQyw2QkFBeUM7SUFDckMseUNBQWdHO0lBQzVGLGlDQUFXO0lBQUEsWUFBc0M7SUFBQSxpQkFBWTtJQUM3RCxtQ0FBNlQ7SUFBaEssbU5BQWlDLGdNQUFBO0lBQTlMLGlCQUE2VDtJQUU3VCw0Q0FBOEc7SUFBNUIsNE1BQTJCO0lBQUMsaUJBQWlCO0lBQy9ILDJDQUEwRTtJQUM5RSxpQkFBaUI7SUFDckIsMEJBQWU7Ozs7SUFQSyxlQUFnQztJQUFoQyxnREFBZ0MsZ0NBQUE7SUFDakMsZUFBc0M7SUFBdEMsc0VBQXNDO0lBQ3JCLGVBQWdDO0lBQWhDLDhEQUFnQztJQUFDLG1DQUEwQixpR0FBQSxzQkFBQSxzQkFBQSx1REFBQSxnQ0FBQTtJQUU3RCxlQUF1QjtJQUF2QixzRUFBdUIsZ0NBQUE7SUFDaEIsZUFBZ0I7SUFBaEIseUJBQWdCOzs7O0lBSXpELCtCQUFtQztJQUMvQiw4QkFBcUM7SUFDakMsdUNBQTBHO0lBQXpFLHdPQUE4QztJQUMvRSxpQkFBZTtJQUNuQixpQkFBTTtJQUNWLGlCQUFVOzs7SUFIWSxlQUFrQjtJQUFsQixnQ0FBa0IsaUNBQUE7O0FERHhDLE1BQU0sQ0FBQyxNQUFNLFVBQVUsR0FBRztJQUN4QixLQUFLLEVBQUU7UUFDTCxTQUFTLEVBQUUsWUFBWTtLQUN4QjtJQUNELE9BQU8sRUFBRTtRQUNQLFNBQVMsRUFBRSxZQUFZO1FBQ3ZCLGNBQWMsRUFBRSxVQUFVO1FBQzFCLGFBQWEsRUFBRSxJQUFJO1FBQ25CLGtCQUFrQixFQUFFLGNBQWM7S0FDbkM7Q0FDRixDQUFDO0FBWUYsTUFBTSxPQUFPLGlCQUFpQjtJQWtDNUI7UUFqQ0EsZUFBVSxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkUsY0FBUyxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLElBQUksRUFBRSxJQUFJLEVBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBS3pJLGFBQWE7UUFDSixPQUFFLEdBQWdCO1lBQ3pCLEtBQUssRUFBQyxLQUFLO1lBQ1gsY0FBYyxFQUFFLEtBQUs7WUFDckIsUUFBUSxFQUFFLEtBQUs7WUFDZixHQUFHLEVBQUMsSUFBSTtZQUNSLEdBQUcsRUFBQyxJQUFJO1lBQ1IsS0FBSyxFQUFDLE9BQU87WUFDYixXQUFXLEVBQUMsT0FBTztTQUNwQixDQUFDO1FBRVEsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFhbEQsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixjQUFTLEdBQVksSUFBSSxDQUFDO0lBRVgsQ0FBQztJQWRoQixJQUFhLFdBQVcsQ0FBQyxLQUFXO1FBQ2xDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELElBQUksV0FBVztRQUNiLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMzQixDQUFDO0lBU0QsUUFBUSxLQUFJLENBQUM7SUFFYixRQUFRLENBQUMsTUFBVztRQUNsQixNQUFNLEtBQUssR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUM3RCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUc7WUFDZixPQUFPLElBQUksQ0FBQztTQUNiO2FBQU0sSUFBSSxLQUFLLElBQUksRUFBRSxJQUFJLEtBQUssSUFBSSxFQUFFLElBQUksS0FBSyxJQUFJLEVBQUUsSUFBSSxLQUFLLElBQUksRUFBRSxFQUFHO1lBQ3BFLE9BQU8sSUFBSSxDQUFDO1NBQ2I7YUFBTTtZQUNMLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7SUFDTCxDQUFDO0lBRUMsVUFBVSxDQUFDLE1BQW9DO1FBQzdDLHVCQUF1QjtRQUN2QixNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUN0QyxNQUFNLEtBQUssR0FBRyxFQUFFLEtBQUssRUFBQyxJQUFJLEVBQUUsR0FBRyxFQUFDLElBQUksRUFBQyxDQUFDO1FBQ3RDLElBQUksU0FBUyxJQUFJLElBQUksRUFBRTtZQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0JBQzlCLEtBQUssQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUM5QztpQkFBSTtnQkFDSCxLQUFLLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNuRCxLQUFLLENBQUMsR0FBRyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ2hEO1lBQ0QsK0JBQStCO1NBQ2hDO2FBQUk7WUFDSCxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUN4QjtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxNQUFNO1FBQ3RCLE1BQU0sS0FBSyxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUM7UUFDekMsS0FBSyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNoRCxLQUFLLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDaEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVELGlCQUFpQixDQUFDLElBQUk7UUFDcEIsb0NBQW9DO1FBQ3BDLHFCQUFxQjtRQUNyQixJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDMUI7SUFDSCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsS0FBSztRQUNyQixvQ0FBb0M7UUFDcEMsc0JBQXNCO0lBQ3hCLENBQUM7SUFFRCxjQUFjO1FBQ1osaUNBQWlDO1FBQ2pDLHFCQUFxQjtRQUNyQixnREFBZ0Q7UUFDaEQsWUFBWTtJQUNkLENBQUM7O3FHQTdGVSxpQkFBaUI7bUdBQWpCLGlCQUFpQjs7Ozs7OzttSUFOakI7WUFDVCxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRTtZQUMvQyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixFQUFFLElBQUksRUFBRSxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQzlFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUU7U0FDcEQ7UUMvQkgscUZBUWU7UUFFZiwwRUFLVTs7UUFmSyw2Q0FBd0I7UUFVN0IsZUFBdUI7UUFBdkIsNENBQXVCOzt1RkR1QnBCLGlCQUFpQjtjQVY3QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLFdBQVcsRUFBRSwyQkFBMkI7Z0JBQ3hDLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO2dCQUN2QyxTQUFTLEVBQUU7b0JBQ1QsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUU7b0JBQy9DLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFFLENBQUMsZUFBZSxDQUFDLEVBQUU7b0JBQzlFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUU7aUJBQ3BEO2FBQ0Y7c0NBTTZDLFVBQVU7a0JBQXJELFNBQVM7bUJBQUMsWUFBWSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtZQUNBLFFBQVE7a0JBQWpELFNBQVM7bUJBQUMsVUFBVSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtZQUUvQixFQUFFO2tCQUFWLEtBQUs7WUFVSSxVQUFVO2tCQUFuQixNQUFNO1lBRU0sV0FBVztrQkFBdkIsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIEVsZW1lbnRSZWYsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNb21lbnREYXRlQWRhcHRlciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsLW1vbWVudC1hZGFwdGVyJztcclxuaW1wb3J0IHsgRGF0ZUFkYXB0ZXIsIE1BVF9EQVRFX0ZPUk1BVFMsIE1BVF9EQVRFX0xPQ0FMRSB9IGZyb20gJ3NhdHVybi1kYXRlcGlja2VyJztcclxuaW1wb3J0IHsgY2FsZW5kYXJVSSB9IGZyb20gJy4uLy4uL21vZGVscy9jYWxlbmRhci5tb2RlbCc7XHJcbmltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBEYXRlIHtcclxuICBiZWdpbjogYW55LFxyXG4gIGVuZDogYW55XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBNWV9GT1JNQVRTID0ge1xyXG4gIHBhcnNlOiB7XHJcbiAgICBkYXRlSW5wdXQ6ICdERC9NTS9ZWVlZJyxcclxuICB9LFxyXG4gIGRpc3BsYXk6IHtcclxuICAgIGRhdGVJbnB1dDogJ0REL01NL1lZWVknLFxyXG4gICAgbW9udGhZZWFyTGFiZWw6ICdNTU0gWVlZWScsXHJcbiAgICBkYXRlQTExeUxhYmVsOiAnTEwnLFxyXG4gICAgbW9udGhZZWFyQTExeUxhYmVsOiAnREQgTU1NTSBZWVlZJ1xyXG4gIH0sXHJcbn07XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2lkLWNhbGVuZGFyJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY2FsZW5kYXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NhbGVuZGFyLmNvbXBvbmVudC5jc3MnXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHsgcHJvdmlkZTogTUFUX0RBVEVfTE9DQUxFLCB1c2VWYWx1ZTogJ2VzLUVTJyB9LFxyXG4gICAgeyBwcm92aWRlOiBEYXRlQWRhcHRlciwgdXNlQ2xhc3M6IE1vbWVudERhdGVBZGFwdGVyLCBkZXBzOiBbTUFUX0RBVEVfTE9DQUxFXSB9LFxyXG4gICAgeyBwcm92aWRlOiBNQVRfREFURV9GT1JNQVRTLCB1c2VWYWx1ZTogTVlfRk9STUFUUyB9XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FsZW5kYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIG1hc2tTaW5nbGUgPSBbL1xcZC8sIC9cXGQvLCAnLycsIC9cXGQvLCAvXFxkLywnLycsIC9cXGQvLCAvXFxkLywgL1xcZC8sIC9cXGQvXTtcclxuICBtYXNrUmFuZ2UgPSBbL1xcZC8sIC9cXGQvLCAnLycsIC9cXGQvLCAvXFxkLywnLycsIC9cXGQvLCAvXFxkLywgL1xcZC8sIC9cXGQvLCcgJywnLScsJyAnLC9cXGQvLCAvXFxkLywnLycsIC9cXGQvLCAvXFxkLywnLycsIC9cXGQvLCAvXFxkLywgL1xcZC8sIC9cXGQvXTtcclxuXHJcbiAgc2VsZWN0ZWREYXRlOkRhdGU7XHJcbiAgQFZpZXdDaGlsZCgnY2FsZW5kYXJpbycsIHsgc3RhdGljOiBmYWxzZSB9KSBjYWxlbmRhcmlvOiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoJ2NhbGVuZGFyJywgeyBzdGF0aWM6IGZhbHNlIH0pIGNhbGVuZGFyOiBFbGVtZW50UmVmO1xyXG4gIC8vIGRhdGVSYW5nZTtcclxuICBASW5wdXQoKSB1aT86IGNhbGVuZGFyVUkgPSB7XHJcbiAgICByYW5nZTpmYWxzZSxcclxuICAgIGlubGluZUNhbGVuZGFyOiBmYWxzZSxcclxuICAgIGRpc2FibGVkOiBmYWxzZSxcclxuICAgIG1pbjpudWxsLFxyXG4gICAgbWF4Om51bGwsXHJcbiAgICBsYWJlbDonRmVjaGEnLFxyXG4gICAgcGxhY2Vob2xkZXI6J0ZlY2hhJ1xyXG4gIH07XHJcblxyXG4gIEBPdXRwdXQoKSBkYXRhT3V0cHV0ID0gbmV3IEV2ZW50RW1pdHRlcjxvYmplY3Q+KCk7XHJcblxyXG4gIEBJbnB1dCgpIHNldCBpbml0aWFsRGF0ZSh2YWx1ZTogRGF0ZSkge1xyXG4gICAgdGhpcy5fSW5pdGlhbERhdGUgPSB2YWx1ZTtcclxuICAgIHRoaXMudXBkYXRlSW5pdGlhbERhdGUodGhpcy5fSW5pdGlhbERhdGUpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGluaXRpYWxEYXRlKCk6IERhdGUge1xyXG4gICAgcmV0dXJuIHRoaXMuX0luaXRpYWxEYXRlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfSW5pdGlhbERhdGU6IERhdGU7XHJcblxyXG4gIHJhbmdlc3RhdGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICB2YWxpZERhdGU6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge31cclxuXHJcbiAgdmFsaWRLZXkoJGV2ZW50OiBhbnkpe1xyXG4gICAgY29uc3QgdmFsdWUgPSAoJGV2ZW50LndoaWNoKSA/ICRldmVudC53aGljaCA6ICRldmVudC5rZXlDb2RlO1xyXG4gICAgaWYoIHZhbHVlID09IDggKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSBlbHNlIGlmKCB2YWx1ZSA+PSA0OCAmJiB2YWx1ZSA8PSA1NyB8fCB2YWx1ZSA9PSA0NSB8fCB2YWx1ZSA9PSA0NyApIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuXHJcbiAgZGF0ZUNoYW5nZSgkZXZlbnQ6IHsgdGFyZ2V0OiB7IHZhbHVlOiBhbnk7IH07IH0pIHtcclxuICAgIC8vIGNvbnNvbGUubG9nKCRldmVudCk7XHJcbiAgICBjb25zdCBpbnB1dERhdGUgPSAkZXZlbnQudGFyZ2V0LnZhbHVlO1xyXG4gICAgY29uc3QgcmFuZ2UgPSB7IGJlZ2luOm51bGwsIGVuZDpudWxsfTtcclxuICAgIGlmIChpbnB1dERhdGUgIT0gbnVsbCkge1xyXG4gICAgICB0aGlzLnZhbGlkRGF0ZSA9IHRydWU7XHJcbiAgICAgIGlmIChtb21lbnQuaXNNb21lbnQoaW5wdXREYXRlKSkge1xyXG4gICAgICAgIHJhbmdlLmJlZ2luID0gaW5wdXREYXRlLmZvcm1hdCgnREQvTU0vWVlZWScpO1xyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICByYW5nZS5iZWdpbiA9IGlucHV0RGF0ZS5iZWdpbi5mb3JtYXQoJ0REL01NL1lZWVknKTtcclxuICAgICAgICByYW5nZS5lbmQgPSBpbnB1dERhdGUuZW5kLmZvcm1hdCgnREQvTU0vWVlZWScpO1xyXG4gICAgICB9XHJcbiAgICAgIC8vIHRoaXMuZGF0YU91dHB1dC5lbWl0KHJhbmdlKTtcclxuICAgIH1lbHNle1xyXG4gICAgICB0aGlzLnZhbGlkRGF0ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgdGhpcy5kYXRhT3V0cHV0LmVtaXQocmFuZ2UpO1xyXG4gIH1cclxuXHJcbiAgaW5saW5lUmFuZ2VDaGFuZ2UoJGV2ZW50KSB7XHJcbiAgICBjb25zdCByYW5nZSA9IHsgYmVnaW46IG51bGwsIGVuZDogbnVsbCB9O1xyXG4gICAgcmFuZ2UuYmVnaW4gPSAkZXZlbnQuYmVnaW4uZm9ybWF0KCdERC9NTS9ZWVlZJyk7XHJcbiAgICByYW5nZS5lbmQgPSAkZXZlbnQuZW5kID8gJGV2ZW50LmVuZC5mb3JtYXQoJ0REL01NL1lZWVknKSA6IG51bGw7XHJcbiAgICB0aGlzLmRhdGFPdXRwdXQuZW1pdChyYW5nZSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVJbml0aWFsRGF0ZShkYXRlKSB7XHJcbiAgICAvLyBjb25zb2xlLmxvZygndXBkYXRlSW5pdGlhbERhdGUnKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKGRhdGUpO1xyXG4gICAgaWYgKHRoaXMudWkubGltaXRlZFJhbmdlKSB7XHJcbiAgICAgIHRoaXMudWkubWluID0gZGF0ZS5iZWdpbjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNlbGVjdGVkQmVnaW5EYXRlKGV2ZW50KXtcclxuICAgIC8vIGNvbnNvbGUubG9nKCdzZWxlY3RlZEJlZ2luRGF0ZScpO1xyXG4gICAgLy8gY29uc29sZS5sb2coZXZlbnQpO1xyXG4gIH1cclxuXHJcbiAgc2V0SW5pdGlhbERhdGUoKXtcclxuICAgIC8vIGNvbnNvbGUubG9nKCdzZXRJbml0aWFsRGF0ZScpO1xyXG4gICAgLy8gc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAvLyAgIGNvbnNvbGUubG9nKHRoaXMuY2FsZW5kYXJpby5uYXRpdmVFbGVtZW50KTtcclxuICAgIC8vIH0sIDEwMDApO1xyXG4gIH1cclxufVxyXG4iLCI8bmctY29udGFpbmVyICpuZ0lmPVwiIXVpLmlubGluZUNhbGVuZGFyXCI+XHJcbiAgICA8bWF0LWZvcm0tZmllbGQgW2NsYXNzLm1hdC1mb2N1c2VkXT1cInJhbmdlc3RhdGVcIiBbY2xhc3MubWF0LWVycm9yXT1cIiF2YWxpZERhdGVcIiBhcHBlbmRUbz1cImJvZHlcIj5cclxuICAgICAgICA8bWF0LWxhYmVsPnt7dWkubGFiZWwgPyB1aS5sYWJlbCA6ICdTZWxlY2Npb25hJ319PC9tYXQtbGFiZWw+XHJcbiAgICAgICAgPGlucHV0IG1hdElucHV0ICNjYWxlbmRhcmlvIHBsYWNlaG9sZGVyPVwie3t1aS5wbGFjZWhvbGRlcn19XCIgW3NhdERhdGVwaWNrZXJdPVwiY2FsZW5kYXJcIiBbdGV4dE1hc2tdPVwie21hc2s6IHVpLnJhbmdlID8gbWFza1JhbmdlIDogbWFza1NpbmdsZSwgZ3VpZGU6IGZhbHNlfVwiIChkYXRlQ2hhbmdlKT1cImRhdGVDaGFuZ2UoJGV2ZW50KVwiIFttaW5dPVwidWkubWluXCIgW21heF09XCJ1aS5tYXhcIiBbdmFsdWVdPVwiaW5pdGlhbERhdGUgPyBpbml0aWFsRGF0ZSA6ICcnXCIgW2Rpc2FibGVkXT1cInVpLmRpc2FibGVkXCIgKGtleXByZXNzKT1cInZhbGlkS2V5KCRldmVudClcIj5cclxuICAgICAgICA8IS0tIChmb2N1cyk9XCJjYWxlbmRhci5vcGVuKClcIiAtLT5cclxuICAgICAgICA8c2F0LWRhdGVwaWNrZXIgI2NhbGVuZGFyIFtyYW5nZU1vZGVdPVwidWk/LnJhbmdlXCIgW3NlbGVjdEZpcnN0RGF0ZU9uQ2xvc2VdPVwidHJ1ZVwiIChvcGVuZWQpPVwic2V0SW5pdGlhbERhdGUoKVwiPjwvc2F0LWRhdGVwaWNrZXI+XHJcbiAgICAgICAgPHNhdC1kYXRlcGlja2VyLXRvZ2dsZSBtYXRTdWZmaXggW2Zvcl09XCJjYWxlbmRhclwiPjwvc2F0LWRhdGVwaWNrZXItdG9nZ2xlPlxyXG4gICAgPC9tYXQtZm9ybS1maWVsZD5cclxuPC9uZy1jb250YWluZXI+XHJcblxyXG48c2VjdGlvbiAqbmdJZj1cInVpLmlubGluZUNhbGVuZGFyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwiaW5saW5lQ2FsZW5kYXJDb250YWluZXJcIj5cclxuICAgICAgICA8c2F0LWNhbGVuZGFyIFtyYW5nZU1vZGVdPVwidHJ1ZVwiIChkYXRlUmFuZ2VzQ2hhbmdlKT1cImlubGluZVJhbmdlQ2hhbmdlKCRldmVudClcIiBbc2VsZWN0ZWRdPVwic2VsZWN0ZWREYXRlXCI+XHJcbiAgICAgICAgPC9zYXQtY2FsZW5kYXI+XHJcbiAgICA8L2Rpdj5cclxuPC9zZWN0aW9uPlxyXG4iXX0=