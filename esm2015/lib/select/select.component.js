import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { NgSelectComponent } from '@ng-select/ng-select';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@ng-select/ng-select";
import * as i3 from "@angular/forms";
function SelectComponent_ng_option_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "ng-option", 6);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const opt_r3 = ctx.$implicit;
    i0.ɵɵproperty("value", opt_r3.value);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(opt_r3.label);
} }
function SelectComponent_small_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.ui.message);
} }
const _c0 = function (a0) { return { "fg-invalid": a0 }; };
export class SelectComponent {
    constructor() {
        this.ui = {
            label: 'Seleccionar',
            placeholder: 'Seleccionar',
            state: true,
            message: 'Este campo es requerido',
            disabled: false,
            notfound: 'Sin resultados'
        };
        this.options = [{
                label: 'Seleccionar',
                value: null
            }];
        this.dataOutput = new EventEmitter();
    }
    set clear(value) {
        this._Clear = value;
        this.clearSelect(this._Clear);
    }
    get currentTrader() {
        return this._Clear;
    }
    ngOnInit() {
        if (this.ui.notfound === undefined) {
            this.ui.notfound = 'Sin resultados';
        }
    }
    emitChanges(event) {
        const selected = this.options.filter((option) => option.value === event);
        const output = {};
        output['label'] = selected.length != 0 ? selected[0].label : undefined;
        output['state'] = selected.length != 0 ? this.ui.state : undefined;
        output['value'] = selected.length != 0 ? selected[0].value : undefined;
        this.dataOutput.emit(output);
    }
    clearSelect(clear) {
        if (clear) {
            this.ngSelectComponent.handleClearClick();
        }
    }
}
/** @nocollapse */ SelectComponent.ɵfac = function SelectComponent_Factory(t) { return new (t || SelectComponent)(); };
/** @nocollapse */ SelectComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: SelectComponent, selectors: [["id-select"]], viewQuery: function SelectComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(NgSelectComponent, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.ngSelectComponent = _t.first);
    } }, inputs: { selected: "selected", ui: "ui", options: "options", clear: "clear" }, outputs: { dataOutput: "dataOutput" }, decls: 7, vars: 10, consts: [[1, "form-group", "mb-0", "w-100", 3, "ngClass"], ["id", "select", 1, "custom", 3, "placeholder", "disabled", "notFoundText", "ngModel", "ngModelChange", "change"], ["selector", ""], [3, "value", 4, "ngFor", "ngForOf"], ["for", "buscar"], [4, "ngIf"], [3, "value"]], template: function SelectComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "ng-select", 1, 2);
        i0.ɵɵlistener("ngModelChange", function SelectComponent_Template_ng_select_ngModelChange_1_listener($event) { return ctx.selected = $event; })("change", function SelectComponent_Template_ng_select_change_1_listener($event) { return ctx.emitChanges($event); });
        i0.ɵɵtemplate(3, SelectComponent_ng_option_3_Template, 2, 2, "ng-option", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "label", 4);
        i0.ɵɵtext(5);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(6, SelectComponent_small_6_Template, 2, 1, "small", 5);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(8, _c0, !(ctx.ui == null ? null : ctx.ui.state)));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("placeholder", ctx.ui.placeholder)("disabled", ctx.ui.disabled)("notFoundText", ctx.ui.notfound)("ngModel", ctx.selected);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngForOf", ctx.options);
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.ui.label);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !(ctx.ui == null ? null : ctx.ui.state));
    } }, directives: [i1.NgClass, i2.NgSelectComponent, i3.NgControlStatus, i3.NgModel, i1.NgForOf, i1.NgIf, i2.ɵr], styles: [""] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(SelectComponent, [{
        type: Component,
        args: [{
                selector: 'id-select',
                templateUrl: './select.component.html',
                styleUrls: ['./select.component.css']
            }]
    }], function () { return []; }, { selected: [{
            type: Input
        }], ui: [{
            type: Input
        }], options: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }], ngSelectComponent: [{
            type: ViewChild,
            args: [NgSelectComponent, { static: false }]
        }], clear: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJDOi9Vc2Vycy9Vc3VhcmlvL0Rlc2t0b3AvbGlicmVyaWFuZ3VsYXItMjktMTAtMjEvY29waWEtYW5ndWxhci1saWItdXBkLTEyLXNpbi1ydXQvcHJvamVjdHMvaXNhcHJlLWRpZ2l0YWwvc3JjLyIsInNvdXJjZXMiOlsibGliL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50LnRzIiwibGliL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYyxZQUFZLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdEcsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7Ozs7OztJQ0NqRCxvQ0FBMkQ7SUFBQSxZQUFhO0lBQUEsaUJBQVk7OztJQUE3QyxvQ0FBbUI7SUFBQyxlQUFhO0lBQWIsa0NBQWE7OztJQUc1RSw2QkFBMEI7SUFBQSxZQUFjO0lBQUEsaUJBQVE7OztJQUF0QixlQUFjO0lBQWQsdUNBQWM7OztBREs1QyxNQUFNLE9BQU8sZUFBZTtJQWlDMUI7UUE3QlMsT0FBRSxHQUFhO1lBQ3RCLEtBQUssRUFBRSxhQUFhO1lBQ3BCLFdBQVcsRUFBRSxhQUFhO1lBQzFCLEtBQUssRUFBRSxJQUFJO1lBQ1gsT0FBTyxFQUFFLHlCQUF5QjtZQUNsQyxRQUFRLEVBQUUsS0FBSztZQUNmLFFBQVEsRUFBRSxnQkFBZ0I7U0FDM0IsQ0FBQTtRQUVRLFlBQU8sR0FBa0IsQ0FBQztnQkFDakMsS0FBSyxFQUFFLGFBQWE7Z0JBQ3BCLEtBQUssRUFBRSxJQUFJO2FBQ1osQ0FBQyxDQUFBO1FBRVEsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7SUFnQmxELENBQUM7SUFaRCxJQUFhLEtBQUssQ0FBQyxLQUFjO1FBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxJQUFJLGFBQWE7UUFDZixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDckIsQ0FBQztJQU9ELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRTtZQUNsQyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztTQUNyQztJQUNILENBQUM7SUFFRCxXQUFXLENBQUMsS0FBSztRQUNmLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxDQUFDO1FBQ3pFLE1BQU0sTUFBTSxHQUFHLEVBQUUsQ0FBQTtRQUNqQixNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUN2RSxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDbkUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDdkUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFLO1FBQ2YsSUFBRyxLQUFLLEVBQUM7WUFDUCxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUMzQztJQUNILENBQUM7O2lHQXZEVSxlQUFlO2lHQUFmLGVBQWU7dUJBb0JmLGlCQUFpQjs7Ozs7UUM5QjlCLDhCQUF5RTtRQUNyRSx1Q0FBMkw7UUFBdEQsOElBQXNCLDBGQUFXLHVCQUFtQixJQUE5QjtRQUN2Siw0RUFBb0Y7UUFDeEYsaUJBQVk7UUFDWixnQ0FBb0I7UUFBQSxZQUFZO1FBQUEsaUJBQVE7UUFDeEMsb0VBQWdEO1FBQ3BELGlCQUFNOztRQU42Qiw2RkFBcUM7UUFDcEIsZUFBOEI7UUFBOUIsZ0RBQThCLDZCQUFBLGlDQUFBLHlCQUFBO1FBQy9DLGVBQVU7UUFBVixxQ0FBVTtRQUVyQixlQUFZO1FBQVosa0NBQVk7UUFDeEIsZUFBZ0I7UUFBaEIsOERBQWdCOzt1RkRLZixlQUFlO2NBTDNCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsV0FBVztnQkFDckIsV0FBVyxFQUFFLHlCQUF5QjtnQkFDdEMsU0FBUyxFQUFFLENBQUMsd0JBQXdCLENBQUM7YUFDdEM7c0NBR1UsUUFBUTtrQkFBaEIsS0FBSztZQUVHLEVBQUU7a0JBQVYsS0FBSztZQVNHLE9BQU87a0JBQWYsS0FBSztZQUtJLFVBQVU7a0JBQW5CLE1BQU07WUFFMEMsaUJBQWlCO2tCQUFqRSxTQUFTO21CQUFDLGlCQUFpQixFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtZQUVsQyxLQUFLO2tCQUFqQixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5nU2VsZWN0Q29tcG9uZW50IH0gZnJvbSAnQG5nLXNlbGVjdC9uZy1zZWxlY3QnO1xyXG5pbXBvcnQgeyBmcm9tIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE9wdGlvbiwgU2VsZWN0VWksIFNlbGVjdE91dHB1dCB9IGZyb20gJy4uLy4uL21vZGVscy9zZWxlY3QubW9kZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2lkLXNlbGVjdCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3NlbGVjdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vc2VsZWN0LmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VsZWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgc2VsZWN0ZWQ7XHJcblxyXG4gIEBJbnB1dCgpIHVpOiBTZWxlY3RVaSA9IHtcclxuICAgIGxhYmVsOiAnU2VsZWNjaW9uYXInLFxyXG4gICAgcGxhY2Vob2xkZXI6ICdTZWxlY2Npb25hcicsXHJcbiAgICBzdGF0ZTogdHJ1ZSxcclxuICAgIG1lc3NhZ2U6ICdFc3RlIGNhbXBvIGVzIHJlcXVlcmlkbycsXHJcbiAgICBkaXNhYmxlZDogZmFsc2UsXHJcbiAgICBub3Rmb3VuZDogJ1NpbiByZXN1bHRhZG9zJ1xyXG4gIH1cclxuXHJcbiAgQElucHV0KCkgb3B0aW9uczogQXJyYXk8T3B0aW9uPiA9IFt7XHJcbiAgICBsYWJlbDogJ1NlbGVjY2lvbmFyJywgXHJcbiAgICB2YWx1ZTogbnVsbFxyXG4gIH1dXHJcblxyXG4gIEBPdXRwdXQoKSBkYXRhT3V0cHV0ID0gbmV3IEV2ZW50RW1pdHRlcjxvYmplY3Q+KCk7XHJcblxyXG4gIEBWaWV3Q2hpbGQoTmdTZWxlY3RDb21wb25lbnQsIHsgc3RhdGljOiBmYWxzZSB9KSBuZ1NlbGVjdENvbXBvbmVudDogTmdTZWxlY3RDb21wb25lbnQ7XHJcblxyXG4gIEBJbnB1dCgpIHNldCBjbGVhcih2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fQ2xlYXIgPSB2YWx1ZTtcclxuICAgIHRoaXMuY2xlYXJTZWxlY3QodGhpcy5fQ2xlYXIpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRUcmFkZXIoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5fQ2xlYXI7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9DbGVhcjogYm9vbGVhbjtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIGlmICh0aGlzLnVpLm5vdGZvdW5kID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgdGhpcy51aS5ub3Rmb3VuZCA9ICdTaW4gcmVzdWx0YWRvcyc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBlbWl0Q2hhbmdlcyhldmVudCl7XHJcbiAgICBjb25zdCBzZWxlY3RlZCA9IHRoaXMub3B0aW9ucy5maWx0ZXIoKG9wdGlvbikgPT4gb3B0aW9uLnZhbHVlID09PSBldmVudCk7XHJcbiAgICBjb25zdCBvdXRwdXQgPSB7fVxyXG4gICAgb3V0cHV0WydsYWJlbCddID0gc2VsZWN0ZWQubGVuZ3RoICE9IDAgPyBzZWxlY3RlZFswXS5sYWJlbCA6IHVuZGVmaW5lZDtcclxuICAgIG91dHB1dFsnc3RhdGUnXSA9IHNlbGVjdGVkLmxlbmd0aCAhPSAwID8gdGhpcy51aS5zdGF0ZSA6IHVuZGVmaW5lZDtcclxuICAgIG91dHB1dFsndmFsdWUnXSA9IHNlbGVjdGVkLmxlbmd0aCAhPSAwID8gc2VsZWN0ZWRbMF0udmFsdWUgOiB1bmRlZmluZWQ7XHJcbiAgICB0aGlzLmRhdGFPdXRwdXQuZW1pdChvdXRwdXQpO1xyXG4gIH1cclxuXHJcbiAgY2xlYXJTZWxlY3QoY2xlYXIpe1xyXG4gICAgaWYoY2xlYXIpe1xyXG4gICAgICB0aGlzLm5nU2VsZWN0Q29tcG9uZW50LmhhbmRsZUNsZWFyQ2xpY2soKTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiIsIjxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIG1iLTAgdy0xMDBcIiBbbmdDbGFzc109XCJ7J2ZnLWludmFsaWQnOiF1aT8uc3RhdGV9XCI+XHJcbiAgICA8bmctc2VsZWN0IGNsYXNzPVwiY3VzdG9tXCIgI3NlbGVjdG9yIGlkPVwic2VsZWN0XCIgW3BsYWNlaG9sZGVyXT1cInVpLnBsYWNlaG9sZGVyXCIgW2Rpc2FibGVkXT1cInVpLmRpc2FibGVkXCIgW25vdEZvdW5kVGV4dF09XCJ1aS5ub3Rmb3VuZFwiIFsobmdNb2RlbCldPVwic2VsZWN0ZWRcIiAoY2hhbmdlKT1cImVtaXRDaGFuZ2VzKCRldmVudClcIj5cclxuICAgICAgICA8bmctb3B0aW9uICpuZ0Zvcj1cImxldCBvcHQgb2Ygb3B0aW9uc1wiIFt2YWx1ZV09XCJvcHQudmFsdWVcIj57e29wdC5sYWJlbH19PC9uZy1vcHRpb24+XHJcbiAgICA8L25nLXNlbGVjdD5cclxuICAgIDxsYWJlbCBmb3I9XCJidXNjYXJcIj57e3VpLmxhYmVsfX08L2xhYmVsPlxyXG4gICAgPHNtYWxsICpuZ0lmPVwiIXVpPy5zdGF0ZVwiPnt7dWkubWVzc2FnZX19PC9zbWFsbD5cclxuPC9kaXY+Il19