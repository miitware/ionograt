import { Directive, HostListener, HostBinding, Output, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export class DndDirective {
    constructor() {
        this.fileDropped = new EventEmitter();
    }
    onDragOver(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = true;
    }
    onDragLeave(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = false;
    }
    // Drop listener
    ondrop(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = false;
        let files = evt.dataTransfer.files;
        if (files.length > 0) {
            this.fileDropped.emit(files);
        }
    }
}
/** @nocollapse */ DndDirective.ɵfac = function DndDirective_Factory(t) { return new (t || DndDirective)(); };
/** @nocollapse */ DndDirective.ɵdir = /** @pureOrBreakMyCode */ i0.ɵɵdefineDirective({ type: DndDirective, selectors: [["", "id-dnd", ""]], hostVars: 2, hostBindings: function DndDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("dragover", function DndDirective_dragover_HostBindingHandler($event) { return ctx.onDragOver($event); })("dragleave", function DndDirective_dragleave_HostBindingHandler($event) { return ctx.onDragLeave($event); })("drop", function DndDirective_drop_HostBindingHandler($event) { return ctx.ondrop($event); });
    } if (rf & 2) {
        i0.ɵɵclassProp("fileover", ctx.fileOver);
    } }, outputs: { fileDropped: "fileDropped" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DndDirective, [{
        type: Directive,
        args: [{
                selector: '[id-dnd]'
            }]
    }], null, { fileOver: [{
            type: HostBinding,
            args: ['class.fileover']
        }], fileDropped: [{
            type: Output
        }], onDragOver: [{
            type: HostListener,
            args: ['dragover', ['$event']]
        }], onDragLeave: [{
            type: HostListener,
            args: ['dragleave', ['$event']]
        }], ondrop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG5kLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJDOi9Vc2Vycy9Vc3VhcmlvL0Rlc2t0b3AvbGlicmVyaWFuZ3VsYXItMjktMTAtMjEvY29waWEtYW5ndWxhci1saWItdXBkLTEyLXNpbi1ydXQvcHJvamVjdHMvaXNhcHJlLWRpZ2l0YWwvc3JjLyIsInNvdXJjZXMiOlsibGliL2xvYWRmaWxlcy9kbmQuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUszRixNQUFNLE9BQU8sWUFBWTtJQUh6QjtRQUtZLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztLQXdCakQ7SUF0QnVDLFVBQVUsQ0FBQyxHQUFHO1FBQ2xELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNyQixHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUU2QyxXQUFXLENBQUMsR0FBRztRQUMzRCxHQUFHLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDckIsR0FBRyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFDRCxnQkFBZ0I7SUFDeUIsTUFBTSxDQUFDLEdBQUc7UUFDakQsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLEtBQUssR0FBRyxHQUFHLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQztRQUNuQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzlCO0lBQ0gsQ0FBQzs7MkZBeEJVLFlBQVk7OEZBQVosWUFBWTtxR0FBWixzQkFDWCxzRkFEVyx1QkFDViw0RUFEVSxrQkFBYzs7Ozt1RkFBZCxZQUFZO2NBSHhCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTthQUNyQjtnQkFFZ0MsUUFBUTtrQkFBdEMsV0FBVzttQkFBQyxnQkFBZ0I7WUFDbkIsV0FBVztrQkFBcEIsTUFBTTtZQUUrQixVQUFVO2tCQUEvQyxZQUFZO21CQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQU1VLFdBQVc7a0JBQXhELFlBQVk7bUJBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDO1lBTUksTUFBTTtrQkFBOUMsWUFBWTttQkFBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSG9zdEJpbmRpbmcsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICBzZWxlY3RvcjogJ1tpZC1kbmRdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRG5kRGlyZWN0aXZlIHtcclxuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmZpbGVvdmVyJykgZmlsZU92ZXI6IGJvb2xlYW47XHJcbiAgQE91dHB1dCgpIGZpbGVEcm9wcGVkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgXHJcbiAgQEhvc3RMaXN0ZW5lcignZHJhZ292ZXInLCBbJyRldmVudCddKSBvbkRyYWdPdmVyKGV2dCkge1xyXG4gICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBldnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICB0aGlzLmZpbGVPdmVyID0gdHJ1ZTtcclxuICB9IFxyXG4gIFxyXG4gIEBIb3N0TGlzdGVuZXIoJ2RyYWdsZWF2ZScsIFsnJGV2ZW50J10pIHB1YmxpYyBvbkRyYWdMZWF2ZShldnQpIHtcclxuICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgdGhpcy5maWxlT3ZlciA9IGZhbHNlO1xyXG4gIH0gIFxyXG4gIC8vIERyb3AgbGlzdGVuZXJcclxuICBASG9zdExpc3RlbmVyKCdkcm9wJywgWyckZXZlbnQnXSkgcHVibGljIG9uZHJvcChldnQpIHtcclxuICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgdGhpcy5maWxlT3ZlciA9IGZhbHNlO1xyXG4gICAgbGV0IGZpbGVzID0gZXZ0LmRhdGFUcmFuc2Zlci5maWxlcztcclxuICAgIGlmIChmaWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHRoaXMuZmlsZURyb3BwZWQuZW1pdChmaWxlcyk7XHJcbiAgICB9XHJcbiAgfSAgXHJcblxyXG59XHJcbiJdfQ==