import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { PanZoomConfig } from 'ngx-panzoom';
import * as i0 from "@angular/core";
import * as i1 from "@angular/platform-browser";
import * as i2 from "@angular/common";
import * as i3 from "ngx-panzoom";
import * as i4 from "ng2-pdfjs-viewer";
const _c0 = ["pdfViewer"];
const _c1 = ["scene"];
function DocumentsComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 10);
    i0.ɵɵelementStart(1, "div", 11);
    i0.ɵɵelementStart(2, "span", 12);
    i0.ɵɵtext(3, "Loading...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
const _c2 = function (a0) { return { "disabled": a0 }; };
function DocumentsComponent_ng_container_3_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r8 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 15);
    i0.ɵɵelementStart(1, "button", 16);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_3_div_1_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r8); const ctx_r7 = i0.ɵɵnextContext(2); return ctx_r7.zoomOut(); });
    i0.ɵɵelementStart(2, "span", 7);
    i0.ɵɵtext(3, "remove");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "button", 17);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_3_div_1_Template_button_click_4_listener() { i0.ɵɵrestoreView(_r8); const ctx_r9 = i0.ɵɵnextContext(2); return ctx_r9.resetView(); });
    i0.ɵɵelementStart(5, "span", 7);
    i0.ɵɵtext(6, " filter_center_focus ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "button", 16);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_3_div_1_Template_button_click_7_listener() { i0.ɵɵrestoreView(_r8); const ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.zoomIn(); });
    i0.ɵɵelementStart(8, "span", 7);
    i0.ɵɵtext(9, "add");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(2, _c2, ctx_r5.disabledLess));
    i0.ɵɵadvance(6);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(4, _c2, ctx_r5.disabledMore));
} }
function DocumentsComponent_ng_container_3_pan_zoom_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "pan-zoom", 18);
    i0.ɵɵelementStart(1, "div", 19);
    i0.ɵɵelement(2, "img", 20, 21);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("config", ctx_r6.panzoomConfig);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("src", ctx_r6.doc, i0.ɵɵsanitizeUrl);
} }
function DocumentsComponent_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, DocumentsComponent_ng_container_3_div_1_Template, 10, 6, "div", 13);
    i0.ɵɵtemplate(2, DocumentsComponent_ng_container_3_pan_zoom_2_Template, 4, 2, "pan-zoom", 14);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.doc);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.doc);
} }
function DocumentsComponent_ng_container_4_ng2_pdfjs_viewer_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "ng2-pdfjs-viewer", 23, 24);
} if (rf & 2) {
    const ctx_r12 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("pdfSrc", ctx_r12.doc)("find", false)("print", false)("download", false)("startPrint", false)("fullScreen", false)("openFile", false)("viewBookmark", false);
} }
function DocumentsComponent_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, DocumentsComponent_ng_container_4_ng2_pdfjs_viewer_1_Template, 2, 8, "ng2-pdfjs-viewer", 22);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r2.doc);
} }
const _c3 = function (a0) { return { "active": a0 }; };
function DocumentsComponent_ng_container_6_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r18 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 26);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_6_button_1_Template_button_click_0_listener() { const restoredCtx = i0.ɵɵrestoreView(_r18); const item_r15 = restoredCtx.$implicit; const index_r16 = restoredCtx.index; const ctx_r17 = i0.ɵɵnextContext(2); return ctx_r17.requestNextFile(item_r15, index_r16); });
    i0.ɵɵelementStart(1, "i", 7);
    i0.ɵɵtext(2, "lens");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const index_r16 = ctx.index;
    const ctx_r14 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(1, _c3, ctx_r14.visible == index_r16));
} }
function DocumentsComponent_ng_container_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, DocumentsComponent_ng_container_6_button_1_Template, 3, 3, "button", 25);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r3.documents);
} }
function DocumentsComponent_ng_container_19_Template(rf, ctx) { if (rf & 1) {
    const _r20 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 27);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_19_Template_div_click_1_listener() { i0.ɵɵrestoreView(_r20); const ctx_r19 = i0.ɵɵnextContext(); return ctx_r19.requestNextFile(ctx_r19.documents[ctx_r19.visible - 1], ctx_r19.visible - 1); });
    i0.ɵɵelementStart(2, "i", 7);
    i0.ɵɵtext(3, " navigate_before ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 28);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_19_Template_div_click_4_listener() { i0.ɵɵrestoreView(_r20); const ctx_r21 = i0.ɵɵnextContext(); return ctx_r21.requestNextFile(ctx_r21.documents[ctx_r21.visible + 1], ctx_r21.visible + 1); });
    i0.ɵɵelementStart(5, "i", 7);
    i0.ɵɵtext(6, " navigate_next ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(2, _c2, ctx_r4.visible == 0));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(4, _c2, ctx_r4.visible == ctx_r4.documents.length - 1));
} }
export class DocumentsComponent {
    constructor(_domSanitizer) {
        this._domSanitizer = _domSanitizer;
        this.panzoomConfig = new PanZoomConfig({
            zoomLevels: 8,
            scalePerZoomLevel: 2.0,
            zoomStepDuration: 0.2,
            zoomToFitZoomLevelFactor: 2,
            zoomOnMouseWheel: false,
            zoomOnDoubleClick: false,
            // initialZoomToFit: {
            //   x: 0,
            //   y: 0,
            //   width: 600,
            //   height: 600
            // }
        });
        this.fileRequest = new EventEmitter();
        this.fileDownload = new EventEmitter();
        this.loaded = false;
        this.disabledLess = false;
        this.disabledMore = false;
        this.visible = 0;
        this.persistenceDocuments = [];
    }
    ngOnInit() {
        if (this.doc) {
            this.typefile = this.getFileExtension(this.doc);
            this.loaded = true;
        }
    }
    getFileExtension(doc) {
        let extension = null;
        extension = doc.type;
        return extension;
    }
    requestNextFile(name, index) {
        this.doc = undefined;
        this.loaded = false;
        this.visible = index != this.visible ? index : this.visible;
        let existe = this.persistenceDocuments.some(item => item.name === name);
        if (existe) {
            this.prepareDocs(this.persistenceDocuments[index].file);
        }
        else {
            this.fileRequest.emit(name);
        }
    }
    ngOnChanges(changes) {
        if (!changes.doc.firstChange) {
            if (changes.doc.previousValue != changes.doc.currentValue) {
                const document = changes.doc.currentValue;
                this.persistenceData(document);
                if (document) {
                    this.prepareDocs(document);
                }
            }
        }
    }
    downloadFile(name) {
        this.fileDownload.emit(name);
    }
    downloadFiles(name) {
        this.fileDownload.emit({ category: true, filename: name });
    }
    zoomIn() {
        this.disabledLess = false;
        if (this.panZoomAPI.model.zoomLevel >= 5) {
            this.disabledMore = true;
        }
        else {
            this.disabledMore = false;
        }
        this.panZoomAPI.zoomIn();
        //
    }
    zoomOut() {
        this.disabledMore = false;
        if (this.panZoomAPI.model.zoomLevel <= 2) {
            this.disabledLess = true;
        }
        else {
            this.disabledLess = false;
        }
        this.panZoomAPI.zoomOut();
    }
    resetView() {
        this.disabledLess = false;
        this.disabledMore = false;
        this.panZoomAPI.resetView();
    }
    ngAfterViewInit() {
        if (this.typefile === 'image/jpeg' || this.typefile === 'image/png') {
            this.apiSubscription = this.panzoomConfig.api.subscribe((api) => this.panZoomAPI = api);
            this.panZoomAPI.resetView();
        }
    }
    ngOnDestroy() {
        if (this.apiSubscription) {
            this.apiSubscription.unsubscribe();
        }
    }
    persistenceData(doc) {
        const item = {};
        item.name = this.documents[this.visible];
        item.file = doc;
        this.persistenceDocuments.splice(this.visible, 0, item);
    }
    prepareDocs(blobFile) {
        this.typefile = this.getFileExtension(blobFile);
        if (this.typefile === 'image/jpeg' || this.typefile === 'image/png') {
            this.apiSubscription = this.panzoomConfig.api.subscribe((api) => this.panZoomAPI = api);
            const blob = blobFile;
            const url = URL.createObjectURL(blob);
            const image = this._domSanitizer.bypassSecurityTrustUrl(url);
            this.loaded = true;
            this.doc = image;
        }
        else {
            if (this.apiSubscription !== undefined) {
                this.apiSubscription.unsubscribe();
            }
            this.loaded = true;
            this.doc = blobFile;
            if (this.pdfViewer !== undefined) {
                this.pdfViewer.pdfSrc = this.doc;
                this.pdfViewer.refresh();
            }
        }
    }
}
/** @nocollapse */ DocumentsComponent.ɵfac = function DocumentsComponent_Factory(t) { return new (t || DocumentsComponent)(i0.ɵɵdirectiveInject(i1.DomSanitizer)); };
/** @nocollapse */ DocumentsComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: DocumentsComponent, selectors: [["id-documents"]], viewQuery: function DocumentsComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, 5);
        i0.ɵɵviewQuery(_c1, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.pdfViewer = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.scene = _t.first);
    } }, inputs: { doc: "doc", documents: "documents" }, outputs: { fileRequest: "fileRequest", fileDownload: "fileDownload" }, features: [i0.ɵɵNgOnChangesFeature], decls: 20, vars: 5, consts: [[1, "id-documents"], [1, "doc-content"], ["class", "status", 4, "ngIf"], [4, "ngIf"], [1, "doc-pagination"], [1, "d-flex", "flex-column", "justify-content-center", "align-items-center", "mt-4"], ["type", "button", 1, "btn", "btn-sm", "btn-outline-secondary", "d-flex", "justify-content-center", "align-items-center", "px-4", "mb-4", 3, "click"], [1, "material-icons"], ["href", "javascript:void(0);", 1, "link", "link-icon", 3, "click"], [1, "material-icons", "link"], [1, "status"], ["role", "status", 1, "spinner-border", "text-primary"], [1, "sr-only"], ["class", "control-zoom", 4, "ngIf"], [3, "config", 4, "ngIf"], [1, "control-zoom"], [1, "btn", "btn-primary", 3, "ngClass", "click"], [1, "btn", "btn-primary", 3, "click"], [3, "config"], [2, "position", "relative", "text-align", "center"], ["id", "scene", "alt", "", 3, "src"], ["scene", ""], [3, "pdfSrc", "find", "print", "download", "startPrint", "fullScreen", "openFile", "viewBookmark", 4, "ngIf"], [3, "pdfSrc", "find", "print", "download", "startPrint", "fullScreen", "openFile", "viewBookmark"], ["pdfViewer", ""], [3, "ngClass", "click", 4, "ngFor", "ngForOf"], [3, "ngClass", "click"], [1, "doc-prev", 3, "ngClass", "click"], [1, "doc-next", 3, "ngClass", "click"]], template: function DocumentsComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵtemplate(2, DocumentsComponent_div_2_Template, 4, 0, "div", 2);
        i0.ɵɵtemplate(3, DocumentsComponent_ng_container_3_Template, 3, 2, "ng-container", 3);
        i0.ɵɵtemplate(4, DocumentsComponent_ng_container_4_Template, 2, 1, "ng-container", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "div", 4);
        i0.ɵɵtemplate(6, DocumentsComponent_ng_container_6_Template, 2, 1, "ng-container", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(7, "div", 5);
        i0.ɵɵelementStart(8, "button", 6);
        i0.ɵɵlistener("click", function DocumentsComponent_Template_button_click_8_listener() { return ctx.downloadFiles(ctx.documents[ctx.visible]); });
        i0.ɵɵelementStart(9, "i", 7);
        i0.ɵɵtext(10, " get_app ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(11, "span");
        i0.ɵɵtext(12, "Descargar documentos categor\u00EDa");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(13, "p");
        i0.ɵɵelementStart(14, "a", 8);
        i0.ɵɵlistener("click", function DocumentsComponent_Template_a_click_14_listener() { return ctx.downloadFile(ctx.documents[ctx.visible]); });
        i0.ɵɵelementStart(15, "i", 9);
        i0.ɵɵtext(16, "insert_drive_file");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(17, "span");
        i0.ɵɵtext(18, "Descargar solo este documento");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(19, DocumentsComponent_ng_container_19_Template, 7, 6, "ng-container", 3);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", !ctx.loaded);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.typefile === "image/jpeg" || ctx.typefile === "image/png");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.typefile === "application/pdf");
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.documents.length > 1);
        i0.ɵɵadvance(13);
        i0.ɵɵproperty("ngIf", ctx.documents.length > 1);
    } }, directives: [i2.NgIf, i2.NgClass, i3.PanZoomComponent, i4.PdfJsViewerComponent, i2.NgForOf], styles: ["[_nghost-%COMP%]    {display:block;width:100%}[_nghost-%COMP%]     iframe{min-height:600px!important}ng2-pdfjs-viewer[_ngcontent-%COMP%]{max-height:600px;min-height:300px;width:100%}.id-documents[_ngcontent-%COMP%]{height:100%;position:relative;width:100%}.doc-content[_ngcontent-%COMP%]{height:600px;min-height:300px;overflow:hidden;padding:0 36px;position:relative;width:auto}.control-zoom[_ngcontent-%COMP%], .doc-content[_ngcontent-%COMP%]{align-items:center;display:flex;justify-content:center}.control-zoom[_ngcontent-%COMP%]{bottom:0;position:absolute;z-index:999}.control-zoom[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]{display:flex;margin:5px;padding:3px}.control-zoom[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%]{font-size:22px}.doc-content[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{-o-object-fit:cover;height:100%;object-fit:cover;width:auto;z-index:2}.doc-prev[_ngcontent-%COMP%]{left:0}.doc-next[_ngcontent-%COMP%], .doc-prev[_ngcontent-%COMP%]{position:absolute;top:50%;transform:translateY(-50%)}.doc-next[_ngcontent-%COMP%]{right:0}.doc-next[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], .doc-prev[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{font-size:36px}.doc-next[_ngcontent-%COMP%]:hover, .doc-prev[_ngcontent-%COMP%]:hover{cursor:pointer}.img-full[_ngcontent-%COMP%]{height:auto;width:100%}.doc-pagination[_ngcontent-%COMP%]{align-items:center;display:flex;justify-content:center;margin-top:16px}.doc-pagination[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{background-color:transparent;border:0;display:flex;margin:0 3px;opacity:.5;padding:0}.doc-pagination[_ngcontent-%COMP%]   button.active[_ngcontent-%COMP%]{opacity:1}.doc-pagination[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:var(--primary);display:flex;font-size:12px}.disabled[_ngcontent-%COMP%]{opacity:.35}.disabled[_ngcontent-%COMP%], .disabled[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%]{color:var(--primary)}.status[_ngcontent-%COMP%]{left:0;margin:0 auto;position:absolute;right:0;top:50%;transform:translateY(-50%);width:50px;z-index:0}.disabled[_ngcontent-%COMP%]{opacity:.5;pointer-events:none}pan-zoom[_ngcontent-%COMP%]{align-items:center;display:flex;height:100%;justify-content:center;width:100%}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DocumentsComponent, [{
        type: Component,
        args: [{
                selector: 'id-documents',
                templateUrl: './documents.component.html',
                styleUrls: ['./documents.component.css']
            }]
    }], function () { return [{ type: i1.DomSanitizer }]; }, { doc: [{
            type: Input
        }], documents: [{
            type: Input
        }], fileRequest: [{
            type: Output
        }], fileDownload: [{
            type: Output
        }], pdfViewer: [{
            type: ViewChild,
            args: ['pdfViewer', { static: false }]
        }], scene: [{
            type: ViewChild,
            args: ['scene', { static: false }]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9jdW1lbnRzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJDOi9Vc2Vycy9Vc3VhcmlvL0Rlc2t0b3AvbGlicmVyaWFuZ3VsYXItMjktMTAtMjEvY29waWEtYW5ndWxhci1saWItdXBkLTEyLXNpbi1ydXQvcHJvamVjdHMvaXNhcHJlLWRpZ2l0YWwvc3JjLyIsInNvdXJjZXMiOlsibGliL2RvY3VtZW50cy9kb2N1bWVudHMuY29tcG9uZW50LnRzIiwibGliL2RvY3VtZW50cy9kb2N1bWVudHMuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQTZCLE1BQU0sZUFBZSxDQUFDO0FBRXJILE9BQU8sRUFBRSxhQUFhLEVBQWMsTUFBTSxhQUFhLENBQUM7Ozs7Ozs7OztJQ0FwRCwrQkFBb0M7SUFDbEMsK0JBQXVEO0lBQ3JELGdDQUFzQjtJQUFBLDBCQUFVO0lBQUEsaUJBQU87SUFDekMsaUJBQU07SUFDUixpQkFBTTs7Ozs7SUFFSiwrQkFBc0M7SUFDcEMsa0NBQTJGO0lBQXBCLG1NQUFtQjtJQUN4RiwrQkFBNkI7SUFBQSxzQkFBTTtJQUFBLGlCQUFPO0lBQzVDLGlCQUFTO0lBQ1Qsa0NBQXNEO0lBQXRCLHFNQUFxQjtJQUNuRCwrQkFBNkI7SUFDM0IscUNBQ0Y7SUFBQSxpQkFBTztJQUNULGlCQUFTO0lBQ1Qsa0NBQTBGO0lBQW5CLG9NQUFrQjtJQUN2RiwrQkFBNkI7SUFBQSxtQkFBRztJQUFBLGlCQUFPO0lBQ3pDLGlCQUFTO0lBQ1gsaUJBQU07OztJQVg0QixlQUFzQztJQUF0Qyx5RUFBc0M7SUFRdEMsZUFBc0M7SUFBdEMseUVBQXNDOzs7SUFJeEUsb0NBQStDO0lBQzdDLCtCQUFxRDtJQUNuRCw4QkFBMEM7SUFDNUMsaUJBQU07SUFDUixpQkFBVzs7O0lBSkQsNkNBQXdCO0lBRVAsZUFBVztJQUFYLGtEQUFXOzs7SUFoQnhDLDZCQUE0RTtJQUMxRSxvRkFZTTtJQUNOLDZGQUlXO0lBQ2IsMEJBQWU7OztJQWxCYyxlQUFTO0lBQVQsaUNBQVM7SUFhQSxlQUFTO0lBQVQsaUNBQVM7OztJQU83QywyQ0FFd0Q7OztJQUYzQixvQ0FBYyxlQUFBLGdCQUFBLG1CQUFBLHFCQUFBLHFCQUFBLG1CQUFBLHVCQUFBOzs7SUFEN0MsNkJBQXFEO0lBQ25ELDZHQUV3RDtJQUMxRCwwQkFBZTs7O0lBRGEsZUFBUztJQUFULGlDQUFTOzs7OztJQUtuQyxrQ0FBMEk7SUFBdkMscVVBQXNDO0lBQ3ZJLDRCQUEwQjtJQUFBLG9CQUFJO0lBQUEsaUJBQUk7SUFDcEMsaUJBQVM7Ozs7SUFGa0Qsa0ZBQXVDOzs7SUFEcEcsNkJBQTJDO0lBQ3pDLHlGQUVTO0lBQ1gsMEJBQWU7OztJQUhZLGVBQWM7SUFBZCwwQ0FBYzs7OztJQW9CM0MsNkJBQTJDO0lBQ3pDLCtCQUF3SDtJQUEzRCxvT0FBMkMsQ0FBQyxxQkFBVyxDQUFDLEtBQUU7SUFDckgsNEJBQTBCO0lBQ3hCLGlDQUNGO0lBQUEsaUJBQUk7SUFDTixpQkFBTTtJQUNOLCtCQUEwSTtJQUEzRCxvT0FBMkMsQ0FBQyxxQkFBVyxDQUFDLEtBQUU7SUFDdkksNEJBQTBCO0lBQ3hCLCtCQUNGO0lBQUEsaUJBQUk7SUFDTixpQkFBTTtJQUNSLDBCQUFlOzs7SUFWUyxlQUFzQztJQUF0Qyx5RUFBc0M7SUFLdEMsZUFBd0Q7SUFBeEQsbUdBQXdEOztBRGxEbEYsTUFBTSxPQUFPLGtCQUFrQjtJQWlDN0IsWUFDVSxhQUEyQjtRQUEzQixrQkFBYSxHQUFiLGFBQWEsQ0FBYztRQWpDckMsa0JBQWEsR0FBa0IsSUFBSSxhQUFhLENBQUM7WUFDL0MsVUFBVSxFQUFFLENBQUM7WUFDYixpQkFBaUIsRUFBRSxHQUFHO1lBQ3RCLGdCQUFnQixFQUFFLEdBQUc7WUFDckIsd0JBQXdCLEVBQUUsQ0FBQztZQUMzQixnQkFBZ0IsRUFBRSxLQUFLO1lBQ3ZCLGlCQUFpQixFQUFFLEtBQUs7WUFDeEIsc0JBQXNCO1lBQ3RCLFVBQVU7WUFDVixVQUFVO1lBQ1YsZ0JBQWdCO1lBQ2hCLGdCQUFnQjtZQUNoQixJQUFJO1NBRUwsQ0FBQyxDQUFDO1FBTU8sZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3RDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUdqRCxXQUFNLEdBQVksS0FBSyxDQUFDO1FBQ3hCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBRzlCLFlBQU8sR0FBVyxDQUFDLENBQUM7UUFDcEIseUJBQW9CLEdBQWUsRUFBRSxDQUFDO0lBS3RDLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3BCO0lBQ0gsQ0FBQztJQUVELGdCQUFnQixDQUFDLEdBQUc7UUFDbEIsSUFBSSxTQUFTLEdBQVcsSUFBSSxDQUFDO1FBQzdCLFNBQVMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQ3JCLE9BQU8sU0FBUyxDQUFDO0lBQ25CLENBQUM7SUFFRCxlQUFlLENBQUMsSUFBWSxFQUFFLEtBQWM7UUFDMUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxTQUFTLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzVELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDO1FBQ3hFLElBQUksTUFBTSxFQUFFO1lBQ1YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDekQ7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzdCO0lBQ0gsQ0FBQztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUU7WUFDNUIsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRTtnQkFDekQsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUM7Z0JBQzFDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQy9CLElBQUksUUFBUSxFQUFFO29CQUNaLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQzVCO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFRCxZQUFZLENBQUMsSUFBSTtRQUNmLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxhQUFhLENBQUMsSUFBSTtRQUNoQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFDLFFBQVEsRUFBQyxJQUFJLEVBQUUsUUFBUSxFQUFDLElBQUksRUFBQyxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVNLE1BQU07UUFDWCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7U0FDMUI7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1NBQzNCO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN6QixFQUFFO0lBQ0osQ0FBQztJQUVNLE9BQU87UUFDWixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7U0FDMUI7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1NBQzNCO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRU0sU0FBUztRQUNkLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssWUFBWSxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssV0FBVyxFQUFFO1lBQ25FLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBZSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ3BHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDN0I7SUFDSCxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztJQUVELGVBQWUsQ0FBQyxHQUFHO1FBQ2pCLE1BQU0sSUFBSSxHQUFRLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVELFdBQVcsQ0FBQyxRQUFhO1FBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hELElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxZQUFZLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxXQUFXLEVBQUU7WUFDbkUsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFlLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDcEcsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDO1lBQ3RCLE1BQU0sR0FBRyxHQUFHLEdBQUcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM3RCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQixJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztTQUNsQjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLFNBQVMsRUFBRTtnQkFDdEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNwQztZQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLElBQUksQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDO1lBQ3BCLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDMUI7U0FDRjtJQUNILENBQUM7O3VHQXRKVSxrQkFBa0I7b0dBQWxCLGtCQUFrQjs7Ozs7Ozs7UUNYL0IsOEJBQTBCO1FBQ3hCLDhCQUF5QjtRQUN2QixtRUFJTTtRQUNOLHFGQW1CZTtRQUNmLHFGQUllO1FBQ2pCLGlCQUFNO1FBQ04sOEJBQTRCO1FBQzFCLHFGQUllO1FBQ2pCLGlCQUFNO1FBQ04sOEJBQStFO1FBQzdFLGlDQUM4QztRQUE1QywrRkFBUyw2Q0FBaUMsSUFBQztRQUMzQyw0QkFBMEI7UUFDeEIsMEJBQ0Y7UUFBQSxpQkFBSTtRQUNKLDZCQUFNO1FBQUEsb0RBQThCO1FBQUEsaUJBQU87UUFDN0MsaUJBQVM7UUFDVCwwQkFBRztRQUNELDZCQUFnRztRQUEzQywyRkFBUyw0Q0FBZ0MsSUFBQztRQUM3Riw2QkFBK0I7UUFBQSxrQ0FBaUI7UUFBQSxpQkFBSTtRQUNwRCw2QkFBTTtRQUFBLDhDQUE2QjtRQUFBLGlCQUFPO1FBQzVDLGlCQUFJO1FBQ04saUJBQUk7UUFDTixpQkFBTTtRQUNOLHVGQVdlO1FBQ2pCLGlCQUFNOztRQWpFbUIsZUFBYTtRQUFiLGtDQUFhO1FBS25CLGVBQTJEO1FBQTNELG9GQUEyRDtRQW9CM0QsZUFBb0M7UUFBcEMseURBQW9DO1FBT3BDLGVBQTBCO1FBQTFCLCtDQUEwQjtRQXFCNUIsZ0JBQTBCO1FBQTFCLCtDQUEwQjs7dUZENUM5QixrQkFBa0I7Y0FMOUIsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2dCQUN4QixXQUFXLEVBQUUsNEJBQTRCO2dCQUN6QyxTQUFTLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQzthQUN6QzsrREFvQlUsR0FBRztrQkFBWCxLQUFLO1lBQ0csU0FBUztrQkFBakIsS0FBSztZQUNJLFdBQVc7a0JBQXBCLE1BQU07WUFDRyxZQUFZO2tCQUFyQixNQUFNO1lBQzJDLFNBQVM7a0JBQTFELFNBQVM7bUJBQUMsV0FBVyxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtZQUNGLEtBQUs7a0JBQTNDLFNBQVM7bUJBQUMsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiwgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIsIFNhZmVSZXNvdXJjZVVybCwgU2FmZVVybCB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5pbXBvcnQgeyBQYW5ab29tQ29uZmlnLCBQYW5ab29tQVBJIH0gZnJvbSAnbmd4LXBhbnpvb20nO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2lkLWRvY3VtZW50cycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2RvY3VtZW50cy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZG9jdW1lbnRzLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRG9jdW1lbnRzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBwYW56b29tQ29uZmlnOiBQYW5ab29tQ29uZmlnID0gbmV3IFBhblpvb21Db25maWcoe1xyXG4gICAgem9vbUxldmVsczogOCxcclxuICAgIHNjYWxlUGVyWm9vbUxldmVsOiAyLjAsXHJcbiAgICB6b29tU3RlcER1cmF0aW9uOiAwLjIsXHJcbiAgICB6b29tVG9GaXRab29tTGV2ZWxGYWN0b3I6IDIsXHJcbiAgICB6b29tT25Nb3VzZVdoZWVsOiBmYWxzZSxcclxuICAgIHpvb21PbkRvdWJsZUNsaWNrOiBmYWxzZSxcclxuICAgIC8vIGluaXRpYWxab29tVG9GaXQ6IHtcclxuICAgIC8vICAgeDogMCxcclxuICAgIC8vICAgeTogMCxcclxuICAgIC8vICAgd2lkdGg6IDYwMCxcclxuICAgIC8vICAgaGVpZ2h0OiA2MDBcclxuICAgIC8vIH1cclxuXHJcbiAgfSk7XHJcbiAgcGFuWm9vbUFQSTogUGFuWm9vbUFQSTtcclxuICBhcGlTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgQElucHV0KCkgZG9jOiBhbnk7XHJcbiAgQElucHV0KCkgZG9jdW1lbnRzOiBzdHJpbmdbXTtcclxuICBAT3V0cHV0KCkgZmlsZVJlcXVlc3QgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgZmlsZURvd25sb2FkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQFZpZXdDaGlsZCgncGRmVmlld2VyJywgeyBzdGF0aWM6IGZhbHNlIH0pIHB1YmxpYyBwZGZWaWV3ZXI7XHJcbiAgQFZpZXdDaGlsZCgnc2NlbmUnLCB7IHN0YXRpYzogZmFsc2UgfSkgc2NlbmU6IEVsZW1lbnRSZWY7XHJcbiAgbG9hZGVkOiBib29sZWFuID0gZmFsc2U7XHJcbiAgZGlzYWJsZWRMZXNzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgZGlzYWJsZWRNb3JlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgdXJsZmlsZTogc3RyaW5nO1xyXG4gIHR5cGVmaWxlOiBzdHJpbmc7XHJcbiAgdmlzaWJsZTogbnVtYmVyID0gMDtcclxuICBwZXJzaXN0ZW5jZURvY3VtZW50czogQXJyYXk8YW55PiA9IFtdO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2RvbVNhbml0aXplcjogRG9tU2FuaXRpemVyXHJcbiAgKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIGlmICh0aGlzLmRvYykge1xyXG4gICAgICB0aGlzLnR5cGVmaWxlID0gdGhpcy5nZXRGaWxlRXh0ZW5zaW9uKHRoaXMuZG9jKTtcclxuICAgICAgdGhpcy5sb2FkZWQgPSB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0RmlsZUV4dGVuc2lvbihkb2MpIHtcclxuICAgIGxldCBleHRlbnNpb246IHN0cmluZyA9IG51bGw7XHJcbiAgICBleHRlbnNpb24gPSBkb2MudHlwZTtcclxuICAgIHJldHVybiBleHRlbnNpb247XHJcbiAgfVxyXG5cclxuICByZXF1ZXN0TmV4dEZpbGUobmFtZTogc3RyaW5nLCBpbmRleD86IG51bWJlcikge1xyXG4gICAgdGhpcy5kb2MgPSB1bmRlZmluZWQ7XHJcbiAgICB0aGlzLmxvYWRlZCA9IGZhbHNlO1xyXG4gICAgdGhpcy52aXNpYmxlID0gaW5kZXggIT0gdGhpcy52aXNpYmxlID8gaW5kZXggOiB0aGlzLnZpc2libGU7XHJcbiAgICBsZXQgZXhpc3RlID0gdGhpcy5wZXJzaXN0ZW5jZURvY3VtZW50cy5zb21lKGl0ZW0gPT4gaXRlbS5uYW1lID09PSBuYW1lKTtcclxuICAgIGlmIChleGlzdGUpIHtcclxuICAgICAgdGhpcy5wcmVwYXJlRG9jcyh0aGlzLnBlcnNpc3RlbmNlRG9jdW1lbnRzW2luZGV4XS5maWxlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZmlsZVJlcXVlc3QuZW1pdChuYW1lKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGlmICghY2hhbmdlcy5kb2MuZmlyc3RDaGFuZ2UpIHtcclxuICAgICAgaWYgKGNoYW5nZXMuZG9jLnByZXZpb3VzVmFsdWUgIT0gY2hhbmdlcy5kb2MuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgY29uc3QgZG9jdW1lbnQgPSBjaGFuZ2VzLmRvYy5jdXJyZW50VmFsdWU7XHJcbiAgICAgICAgdGhpcy5wZXJzaXN0ZW5jZURhdGEoZG9jdW1lbnQpO1xyXG4gICAgICAgIGlmIChkb2N1bWVudCkge1xyXG4gICAgICAgICAgdGhpcy5wcmVwYXJlRG9jcyhkb2N1bWVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBkb3dubG9hZEZpbGUobmFtZSkge1xyXG4gICAgdGhpcy5maWxlRG93bmxvYWQuZW1pdChuYW1lKTtcclxuICB9XHJcblxyXG4gIGRvd25sb2FkRmlsZXMobmFtZSkge1xyXG4gICAgdGhpcy5maWxlRG93bmxvYWQuZW1pdCh7Y2F0ZWdvcnk6dHJ1ZSwgZmlsZW5hbWU6bmFtZX0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHpvb21JbigpOiB2b2lkIHtcclxuICAgIHRoaXMuZGlzYWJsZWRMZXNzID0gZmFsc2U7XHJcbiAgICBpZiAodGhpcy5wYW5ab29tQVBJLm1vZGVsLnpvb21MZXZlbCA+PSA1KSB7XHJcbiAgICAgIHRoaXMuZGlzYWJsZWRNb3JlID0gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZGlzYWJsZWRNb3JlID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICB0aGlzLnBhblpvb21BUEkuem9vbUluKCk7XHJcbiAgICAvL1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHpvb21PdXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmRpc2FibGVkTW9yZSA9IGZhbHNlO1xyXG4gICAgaWYgKHRoaXMucGFuWm9vbUFQSS5tb2RlbC56b29tTGV2ZWwgPD0gMikge1xyXG4gICAgICB0aGlzLmRpc2FibGVkTGVzcyA9IHRydWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmRpc2FibGVkTGVzcyA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgdGhpcy5wYW5ab29tQVBJLnpvb21PdXQoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyByZXNldFZpZXcoKTogdm9pZCB7XHJcbiAgICB0aGlzLmRpc2FibGVkTGVzcyA9IGZhbHNlO1xyXG4gICAgdGhpcy5kaXNhYmxlZE1vcmUgPSBmYWxzZTtcclxuICAgIHRoaXMucGFuWm9vbUFQSS5yZXNldFZpZXcoKTtcclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgIGlmICh0aGlzLnR5cGVmaWxlID09PSAnaW1hZ2UvanBlZycgfHwgdGhpcy50eXBlZmlsZSA9PT0gJ2ltYWdlL3BuZycpIHtcclxuICAgICAgdGhpcy5hcGlTdWJzY3JpcHRpb24gPSB0aGlzLnBhbnpvb21Db25maWcuYXBpLnN1YnNjcmliZSgoYXBpOiBQYW5ab29tQVBJKSA9PiB0aGlzLnBhblpvb21BUEkgPSBhcGkpO1xyXG4gICAgICB0aGlzLnBhblpvb21BUEkucmVzZXRWaWV3KCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmFwaVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLmFwaVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcGVyc2lzdGVuY2VEYXRhKGRvYykge1xyXG4gICAgY29uc3QgaXRlbTogYW55ID0ge307XHJcbiAgICBpdGVtLm5hbWUgPSB0aGlzLmRvY3VtZW50c1t0aGlzLnZpc2libGVdO1xyXG4gICAgaXRlbS5maWxlID0gZG9jO1xyXG4gICAgdGhpcy5wZXJzaXN0ZW5jZURvY3VtZW50cy5zcGxpY2UodGhpcy52aXNpYmxlLCAwLCBpdGVtKTtcclxuICB9XHJcblxyXG4gIHByZXBhcmVEb2NzKGJsb2JGaWxlOiBhbnkpIHtcclxuICAgIHRoaXMudHlwZWZpbGUgPSB0aGlzLmdldEZpbGVFeHRlbnNpb24oYmxvYkZpbGUpO1xyXG4gICAgaWYgKHRoaXMudHlwZWZpbGUgPT09ICdpbWFnZS9qcGVnJyB8fCB0aGlzLnR5cGVmaWxlID09PSAnaW1hZ2UvcG5nJykge1xyXG4gICAgICB0aGlzLmFwaVN1YnNjcmlwdGlvbiA9IHRoaXMucGFuem9vbUNvbmZpZy5hcGkuc3Vic2NyaWJlKChhcGk6IFBhblpvb21BUEkpID0+IHRoaXMucGFuWm9vbUFQSSA9IGFwaSk7XHJcbiAgICAgIGNvbnN0IGJsb2IgPSBibG9iRmlsZTtcclxuICAgICAgY29uc3QgdXJsID0gVVJMLmNyZWF0ZU9iamVjdFVSTChibG9iKTtcclxuICAgICAgY29uc3QgaW1hZ2UgPSB0aGlzLl9kb21TYW5pdGl6ZXIuYnlwYXNzU2VjdXJpdHlUcnVzdFVybCh1cmwpO1xyXG4gICAgICB0aGlzLmxvYWRlZCA9IHRydWU7XHJcbiAgICAgIHRoaXMuZG9jID0gaW1hZ2U7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAodGhpcy5hcGlTdWJzY3JpcHRpb24gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHRoaXMuYXBpU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5sb2FkZWQgPSB0cnVlO1xyXG4gICAgICB0aGlzLmRvYyA9IGJsb2JGaWxlO1xyXG4gICAgICBpZiAodGhpcy5wZGZWaWV3ZXIgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHRoaXMucGRmVmlld2VyLnBkZlNyYyA9IHRoaXMuZG9jO1xyXG4gICAgICAgIHRoaXMucGRmVmlld2VyLnJlZnJlc2goKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIiwiPGRpdiBjbGFzcz1cImlkLWRvY3VtZW50c1wiPlxyXG4gIDxkaXYgY2xhc3M9XCJkb2MtY29udGVudFwiPlxyXG4gICAgPGRpdiBjbGFzcz1cInN0YXR1c1wiICpuZ0lmPVwiIWxvYWRlZFwiPlxyXG4gICAgICA8ZGl2IHJvbGU9XCJzdGF0dXNcIiBjbGFzcz1cInNwaW5uZXItYm9yZGVyIHRleHQtcHJpbWFyeVwiPlxyXG4gICAgICAgIDxzcGFuIGNsYXNzPVwic3Itb25seVwiPkxvYWRpbmcuLi48L3NwYW4+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgICA8bmctY29udGFpbmVyICpuZ0lmPVwidHlwZWZpbGUgPT09ICdpbWFnZS9qcGVnJyB8fCB0eXBlZmlsZSA9PT0gJ2ltYWdlL3BuZydcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbnRyb2wtem9vbVwiICpuZ0lmPVwiZG9jXCI+XHJcbiAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIFtuZ0NsYXNzXT1cInsnZGlzYWJsZWQnOiBkaXNhYmxlZExlc3N9XCIgKGNsaWNrKT1cInpvb21PdXQoKVwiPlxyXG4gICAgICAgICAgPHNwYW4gY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPnJlbW92ZTwvc3Bhbj5cclxuICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5XCIgKGNsaWNrKT1cInJlc2V0VmlldygpXCI+XHJcbiAgICAgICAgICA8c3BhbiBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCI+XHJcbiAgICAgICAgICAgIGZpbHRlcl9jZW50ZXJfZm9jdXNcclxuICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5XCIgW25nQ2xhc3NdPVwieydkaXNhYmxlZCc6IGRpc2FibGVkTW9yZX1cIiAoY2xpY2spPVwiem9vbUluKClcIj5cclxuICAgICAgICAgIDxzcGFuIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5hZGQ8L3NwYW4+XHJcbiAgICAgICAgPC9idXR0b24+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8cGFuLXpvb20gW2NvbmZpZ109XCJwYW56b29tQ29uZmlnXCIgKm5nSWY9XCJkb2NcIj5cclxuICAgICAgICA8ZGl2IHN0eWxlPVwicG9zaXRpb246IHJlbGF0aXZlOyB0ZXh0LWFsaWduOiBjZW50ZXI7XCI+XHJcbiAgICAgICAgICA8aW1nIGlkPVwic2NlbmVcIiAjc2NlbmUgW3NyY109XCJkb2NcIiBhbHQ9XCJcIj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9wYW4tem9vbT5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cInR5cGVmaWxlID09PSAnYXBwbGljYXRpb24vcGRmJ1wiPlxyXG4gICAgICA8bmcyLXBkZmpzLXZpZXdlciAjcGRmVmlld2VyIFtwZGZTcmNdPVwiZG9jXCJcclxuICAgICAgICBbZmluZF09XCJmYWxzZVwiIFtwcmludF09XCJmYWxzZVwiIFtkb3dubG9hZF09XCJmYWxzZVwiIFtzdGFydFByaW50XT1cImZhbHNlXCIgW2Z1bGxTY3JlZW5dPVwiZmFsc2VcIiBbb3BlbkZpbGVdPVwiZmFsc2VcIlxyXG4gICAgICAgIFt2aWV3Qm9va21hcmtdPVwiZmFsc2VcIiAqbmdJZj1cImRvY1wiPjwvbmcyLXBkZmpzLXZpZXdlcj5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJkb2MtcGFnaW5hdGlvblwiPlxyXG4gICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cImRvY3VtZW50cy5sZW5ndGggPiAxXCI+XHJcbiAgICAgIDxidXR0b24gKm5nRm9yPVwibGV0IGl0ZW0gb2YgZG9jdW1lbnRzOyBsZXQgaW5kZXggPSBpbmRleDtcIiBbbmdDbGFzc109XCJ7J2FjdGl2ZSc6dmlzaWJsZSA9PSBpbmRleH1cIiAoY2xpY2spPVwicmVxdWVzdE5leHRGaWxlKGl0ZW0sIGluZGV4KVwiPlxyXG4gICAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5sZW5zPC9pPlxyXG4gICAgICA8L2J1dHRvbj5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJkLWZsZXggZmxleC1jb2x1bW4ganVzdGlmeS1jb250ZW50LWNlbnRlciBhbGlnbi1pdGVtcy1jZW50ZXIgbXQtNFwiPlxyXG4gICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXNtIGJ0bi1vdXRsaW5lLXNlY29uZGFyeSBkLWZsZXgganVzdGlmeS1jb250ZW50LWNlbnRlciBhbGlnbi1pdGVtcy1jZW50ZXIgcHgtNCBtYi00XCJcclxuICAgICAgKGNsaWNrKT1cImRvd25sb2FkRmlsZXMoZG9jdW1lbnRzW3Zpc2libGVdKVwiPlxyXG4gICAgICA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCI+XHJcbiAgICAgICAgZ2V0X2FwcFxyXG4gICAgICA8L2k+XHJcbiAgICAgIDxzcGFuPkRlc2NhcmdhciBkb2N1bWVudG9zIGNhdGVnb3LDrWE8L3NwYW4+XHJcbiAgICA8L2J1dHRvbj5cclxuICAgIDxwPlxyXG4gICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApO1wiIGNsYXNzPVwibGluayBsaW5rLWljb25cIiAoY2xpY2spPVwiZG93bmxvYWRGaWxlKGRvY3VtZW50c1t2aXNpYmxlXSlcIj5cclxuICAgICAgICA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zIGxpbmtcIj5pbnNlcnRfZHJpdmVfZmlsZTwvaT5cclxuICAgICAgICA8c3Bhbj5EZXNjYXJnYXIgc29sbyBlc3RlIGRvY3VtZW50bzwvc3Bhbj5cclxuICAgICAgPC9hPlxyXG4gICAgPC9wPlxyXG4gIDwvZGl2PlxyXG4gIDxuZy1jb250YWluZXIgKm5nSWY9XCJkb2N1bWVudHMubGVuZ3RoID4gMVwiPlxyXG4gICAgPGRpdiBjbGFzcz1cImRvYy1wcmV2XCIgW25nQ2xhc3NdPVwieydkaXNhYmxlZCc6IHZpc2libGUgPT0gMH1cIiAoY2xpY2spPVwicmVxdWVzdE5leHRGaWxlKGRvY3VtZW50c1t2aXNpYmxlLTFdLCB2aXNpYmxlLTEpXCI+XHJcbiAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5cclxuICAgICAgICBuYXZpZ2F0ZV9iZWZvcmVcclxuICAgICAgPC9pPlxyXG4gICAgPC9kaXY+XHJcbiAgICA8ZGl2IGNsYXNzPVwiZG9jLW5leHRcIiBbbmdDbGFzc109XCJ7J2Rpc2FibGVkJyA6IHZpc2libGUgPT0gZG9jdW1lbnRzLmxlbmd0aC0xfVwiIChjbGljayk9XCJyZXF1ZXN0TmV4dEZpbGUoZG9jdW1lbnRzW3Zpc2libGUrMV0sIHZpc2libGUrMSlcIj5cclxuICAgICAgPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPlxyXG4gICAgICAgIG5hdmlnYXRlX25leHRcclxuICAgICAgPC9pPlxyXG4gICAgPC9kaXY+XHJcbiAgPC9uZy1jb250YWluZXI+XHJcbjwvZGl2PlxyXG4iXX0=