import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
function ProgressBarComponent_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 4);
    i0.ɵɵelementStart(1, "i", 5);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
export class ProgressBarComponent {
    constructor() {
        this.progress = 0;
        this.status = new EventEmitter();
    }
    ngOnInit() {
    }
    ngOnChanges(changes) {
        let uploadProgress = changes.progress.currentValue;
        if (uploadProgress === 100) {
            this.status.emit(true);
        }
    }
}
/** @nocollapse */ ProgressBarComponent.ɵfac = function ProgressBarComponent_Factory(t) { return new (t || ProgressBarComponent)(); };
/** @nocollapse */ ProgressBarComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: ProgressBarComponent, selectors: [["id-progress-bar"]], inputs: { progress: "progress" }, outputs: { status: "status" }, features: [i0.ɵɵNgOnChangesFeature], decls: 5, vars: 4, consts: [[1, "progress-container"], ["class", "progress-status d-flex text-decoration-none", 4, "ngIf"], [1, "progress"], ["role", "progressbar", 1, "progress-bar"], [1, "progress-status", "d-flex", "text-decoration-none"], [1, "material-icons", "md-18"]], template: function ProgressBarComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, ProgressBarComponent_span_1_Template, 3, 0, "span", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelementStart(3, "div", 3);
        i0.ɵɵtext(4);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.progress == 100);
        i0.ɵɵadvance(2);
        i0.ɵɵstyleProp("width", ctx.progress + "%");
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate1("", ctx.progress, "%");
    } }, directives: [i1.NgIf], styles: [".progress[_ngcontent-%COMP%], .progress-container[_ngcontent-%COMP%]{position:relative}.progress-bar[_ngcontent-%COMP%]{height:100%;left:0;position:absolute;top:0;transition:all .3s;width:0;z-index:1}.progress-status[_ngcontent-%COMP%]{position:absolute;right:0;top:-20px}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ProgressBarComponent, [{
        type: Component,
        args: [{
                selector: 'id-progress-bar',
                templateUrl: './progress-bar.component.html',
                styleUrls: ['./progress-bar.component.css']
            }]
    }], function () { return []; }, { progress: [{
            type: Input
        }], status: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3Jlc3MtYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJDOi9Vc2Vycy9Vc3VhcmlvL0Rlc2t0b3AvbGlicmVyaWFuZ3VsYXItMjktMTAtMjEvY29waWEtYW5ndWxhci1saWItdXBkLTEyLXNpbi1ydXQvcHJvamVjdHMvaXNhcHJlLWRpZ2l0YWwvc3JjLyIsInNvdXJjZXMiOlsibGliL3Byb2dyZXNzLWJhci9wcm9ncmVzcy1iYXIuY29tcG9uZW50LnRzIiwibGliL3Byb2dyZXNzLWJhci9wcm9ncmVzcy1iYXIuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBaUIsTUFBTSxlQUFlLENBQUM7Ozs7SUNDNUYsK0JBQWtGO0lBQUEsNEJBQWdDO0lBQUEsNEJBQVk7SUFBQSxpQkFBSTtJQUFBLGlCQUFPOztBRE0zSSxNQUFNLE9BQU8sb0JBQW9CO0lBRy9CO1FBRlMsYUFBUSxHQUFHLENBQUMsQ0FBQztRQUNaLFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO0lBQy9CLENBQUM7SUFFakIsUUFBUTtJQUVSLENBQUM7SUFDRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxjQUFjLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7UUFDbkQsSUFBSSxjQUFjLEtBQUssR0FBRyxFQUFFO1lBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQzs7MkdBYlUsb0JBQW9CO3NHQUFwQixvQkFBb0I7UUNQakMsOEJBQWdDO1FBQzlCLHVFQUF5STtRQUN6SSw4QkFBc0I7UUFDcEIsOEJBQTRFO1FBQUEsWUFBYTtRQUFBLGlCQUFNO1FBQ2pHLGlCQUFNO1FBQ1IsaUJBQU07O1FBSnVELGVBQXFCO1FBQXJCLDBDQUFxQjtRQUVqQyxlQUE4QjtRQUE5QiwyQ0FBOEI7UUFBQyxlQUFhO1FBQWIsNENBQWE7O3VGREloRixvQkFBb0I7Y0FMaEMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLFdBQVcsRUFBRSwrQkFBK0I7Z0JBQzVDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO2FBQzVDO3NDQUVVLFFBQVE7a0JBQWhCLEtBQUs7WUFDSSxNQUFNO2tCQUFmLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2lkLXByb2dyZXNzLWJhcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3Byb2dyZXNzLWJhci5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vcHJvZ3Jlc3MtYmFyLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUHJvZ3Jlc3NCYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIHByb2dyZXNzID0gMDtcclxuICBAT3V0cHV0KCkgc3RhdHVzID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG5cclxuICB9XHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgbGV0IHVwbG9hZFByb2dyZXNzID0gY2hhbmdlcy5wcm9ncmVzcy5jdXJyZW50VmFsdWU7XHJcbiAgICBpZiAodXBsb2FkUHJvZ3Jlc3MgPT09IDEwMCkge1xyXG4gICAgICB0aGlzLnN0YXR1cy5lbWl0KHRydWUpO1xyXG4gICAgfSAgICBcclxuICB9XHJcblxyXG59XHJcbiIsIjxkaXYgY2xhc3M9XCJwcm9ncmVzcy1jb250YWluZXJcIj5cclxuICA8c3BhbiBjbGFzcz1cInByb2dyZXNzLXN0YXR1cyBkLWZsZXggdGV4dC1kZWNvcmF0aW9uLW5vbmVcIiAqbmdJZj1cInByb2dyZXNzID09IDEwMFwiPjxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnMgbWQtMThcIj5jaGVja19jaXJjbGU8L2k+PC9zcGFuPlxyXG4gIDxkaXYgY2xhc3M9XCJwcm9ncmVzc1wiPlxyXG4gICAgPGRpdiBjbGFzcz1cInByb2dyZXNzLWJhclwiIHJvbGU9XCJwcm9ncmVzc2JhclwiIFtzdHlsZS53aWR0aF09XCJwcm9ncmVzcyArICclJ1wiPnt7cHJvZ3Jlc3N9fSU8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC9kaXY+Il19