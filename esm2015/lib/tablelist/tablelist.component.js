import { Component, Output, Input, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
function TablelistComponent_ng_container_4_th_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const head_r3 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(head_r3);
} }
function TablelistComponent_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, TablelistComponent_ng_container_4_th_1_Template, 2, 1, "th", 3);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const index_r4 = ctx.index;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", index_r4 <= 2);
} }
function TablelistComponent_tr_6_td_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td");
    i0.ɵɵelementStart(1, "span", 4);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "span", 5);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext();
    const index_r8 = ctx_r10.index;
    const row_r7 = ctx_r10.$implicit;
    const ctx_r9 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r9.colhead[index_r8]);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(row_r7.qty);
} }
function TablelistComponent_tr_6_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "tr");
    i0.ɵɵelementStart(1, "td");
    i0.ɵɵelementStart(2, "span", 4);
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "span", 5);
    i0.ɵɵtext(5);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(6, TablelistComponent_tr_6_td_6_Template, 5, 2, "td", 3);
    i0.ɵɵelementStart(7, "td");
    i0.ɵɵelementStart(8, "span", 4);
    i0.ɵɵtext(9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "p", 6);
    i0.ɵɵelementStart(11, "a", 7);
    i0.ɵɵlistener("click", function TablelistComponent_tr_6_Template_a_click_11_listener() { const restoredCtx = i0.ɵɵrestoreView(_r12); const row_r7 = restoredCtx.$implicit; const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.linkDoc(row_r7.item); });
    i0.ɵɵelementStart(12, "i", 8);
    i0.ɵɵtext(13, "insert_drive_file");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(14, "span");
    i0.ɵɵtext(15);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r7 = ctx.$implicit;
    const index_r8 = ctx.index;
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r1.colhead[index_r8]);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(row_r7.item);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.quantity);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r1.colhead[index_r8]);
    i0.ɵɵadvance(6);
    i0.ɵɵtextInterpolate(row_r7.namelink);
} }
function TablelistComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 9);
    i0.ɵɵelementStart(1, "div", 10);
    i0.ɵɵelementStart(2, "span", 11);
    i0.ɵɵtext(3, "Loading...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
export class TablelistComponent {
    constructor() {
        this.download = new EventEmitter();
        this.quantity = true;
        this.value = [];
        this.colhead = [];
    }
    set wait(value) {
        this._Wait = value;
    }
    get wait() {
        return this._Wait;
    }
    ngOnInit() {
        /* en caso de que no se agregue la col de cantidad */
        this.rows.forEach(row => {
            if (row.qty == undefined) {
                this.value.push(row.qty);
                if (this.value.length == this.rows.length) {
                    this.quantity = false;
                    this.colhead.forEach((name, index) => {
                        if (name == 'Cantidad de documentos' || name == 'Cantidad') {
                            this.colhead.splice(index, 1);
                        }
                    });
                }
            }
        });
    }
    linkDoc(event) {
        this.download.emit(event);
    }
}
/** @nocollapse */ TablelistComponent.ɵfac = function TablelistComponent_Factory(t) { return new (t || TablelistComponent)(); };
/** @nocollapse */ TablelistComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: TablelistComponent, selectors: [["id-tablelist"]], inputs: { rows: "rows", wait: "wait", colhead: "colhead" }, outputs: { download: "download" }, decls: 8, vars: 3, consts: [[1, "id-table", "id-table--mobile", "table", "table-striped"], [4, "ngFor", "ngForOf"], ["class", "loaderblock", 4, "ngIf"], [4, "ngIf"], [1, "term"], [1, "val"], [1, "val", "mb-0"], ["href", "javascript:void(0);", 1, "link", "link-icon", 3, "click"], [1, "material-icons", "link"], [1, "loaderblock"], ["role", "status", 1, "spinner-border", "text-primary"], [1, "sr-only"]], template: function TablelistComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementContainerStart(0);
        i0.ɵɵelementStart(1, "table", 0);
        i0.ɵɵelementStart(2, "thead");
        i0.ɵɵelementStart(3, "tr");
        i0.ɵɵtemplate(4, TablelistComponent_ng_container_4_Template, 2, 1, "ng-container", 1);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "tbody");
        i0.ɵɵtemplate(6, TablelistComponent_tr_6_Template, 16, 5, "tr", 1);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementContainerEnd();
        i0.ɵɵtemplate(7, TablelistComponent_div_7_Template, 4, 0, "div", 2);
    } if (rf & 2) {
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngForOf", ctx.colhead);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngForOf", ctx.rows);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.wait);
    } }, directives: [i1.NgForOf, i1.NgIf], styles: [".loaderblock[_ngcontent-%COMP%]{align-items:center;background-color:hsla(0,0%,100%,.9);display:flex;height:100vh;justify-content:center;left:0;position:fixed;top:0;width:100%;z-index:100000}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(TablelistComponent, [{
        type: Component,
        args: [{
                selector: 'id-tablelist',
                templateUrl: './tablelist.component.html',
                styleUrls: ['./tablelist.component.css']
            }]
    }], function () { return []; }, { download: [{
            type: Output
        }], rows: [{
            type: Input
        }], wait: [{
            type: Input
        }], colhead: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGVsaXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJDOi9Vc2Vycy9Vc3VhcmlvL0Rlc2t0b3AvbGlicmVyaWFuZ3VsYXItMjktMTAtMjEvY29waWEtYW5ndWxhci1saWItdXBkLTEyLXNpbi1ydXQvcHJvamVjdHMvaXNhcHJlLWRpZ2l0YWwvc3JjLyIsInNvdXJjZXMiOlsibGliL3RhYmxlbGlzdC90YWJsZWxpc3QuY29tcG9uZW50LnRzIiwibGliL3RhYmxlbGlzdC90YWJsZWxpc3QuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQzs7OztJQ0szRCwwQkFBdUI7SUFBQSxZQUFRO0lBQUEsaUJBQUs7OztJQUFiLGVBQVE7SUFBUiw2QkFBUTs7O0lBRG5DLDZCQUE4RDtJQUMxRCxnRkFBb0M7SUFDeEMsMEJBQWU7OztJQUROLGVBQWdCO0lBQWhCLG9DQUFnQjs7O0lBT3pCLDBCQUFxQjtJQUFBLCtCQUFtQjtJQUFBLFlBQWtCO0lBQUEsaUJBQU87SUFBQywrQkFBa0I7SUFBQSxZQUFXO0lBQUEsaUJBQU87SUFBQSxpQkFBSzs7Ozs7O0lBQW5FLGVBQWtCO0lBQWxCLDhDQUFrQjtJQUEwQixlQUFXO0lBQVgsZ0NBQVc7Ozs7SUFGbkcsMEJBQWdEO0lBQzVDLDBCQUFJO0lBQUEsK0JBQW1CO0lBQUEsWUFBa0I7SUFBQSxpQkFBTztJQUFDLCtCQUFrQjtJQUFBLFlBQVk7SUFBQSxpQkFBTztJQUFBLGlCQUFLO0lBQzNGLHNFQUEyRztJQUMzRywwQkFBSTtJQUNBLCtCQUFtQjtJQUFBLFlBQWtCO0lBQUEsaUJBQU87SUFDNUMsNkJBQW9CO0lBQ2hCLDZCQUFpRjtJQUFuRCxzUEFBMkI7SUFDckQsNkJBQStCO0lBQUEsa0NBQWlCO0lBQUEsaUJBQUk7SUFDcEQsNkJBQU07SUFBQSxhQUFnQjtJQUFBLGlCQUFPO0lBQ2pDLGlCQUFJO0lBRVIsaUJBQUk7SUFDUixpQkFBSztJQUNULGlCQUFLOzs7OztJQVpzQixlQUFrQjtJQUFsQiw4Q0FBa0I7SUFBMEIsZUFBWTtJQUFaLGlDQUFZO0lBQzFFLGVBQWM7SUFBZCxzQ0FBYztJQUVJLGVBQWtCO0lBQWxCLDhDQUFrQjtJQUl2QixlQUFnQjtJQUFoQixxQ0FBZ0I7OztJQVNsRCw4QkFBc0M7SUFDbEMsK0JBQXVEO0lBQUEsZ0NBQXNCO0lBQUEsMEJBQVU7SUFBQSxpQkFBTztJQUFBLGlCQUFNO0lBQ3hHLGlCQUFNOztBRHBCTixNQUFNLE9BQU8sa0JBQWtCO0lBc0I3QjtRQXBCVSxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQVFoRCxhQUFRLEdBQVksSUFBSSxDQUFDO1FBRXpCLFVBQUssR0FBRyxFQUFFLENBQUM7UUFRRixZQUFPLEdBQWtCLEVBQUUsQ0FBQztJQUVyQixDQUFDO0lBaEJqQixJQUFhLElBQUksQ0FBQyxLQUFjO1FBQzlCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3JCLENBQUM7SUFNRCxJQUFJLElBQUk7UUFDTixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDcEIsQ0FBQztJQVFELFFBQVE7UUFFTixxREFBcUQ7UUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDdEIsSUFBSSxHQUFHLENBQUMsR0FBRyxJQUFJLFNBQVMsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO29CQUN6QyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztvQkFDdEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUMsS0FBSyxFQUFFLEVBQUU7d0JBQ2xDLElBQUksSUFBSSxJQUFJLHdCQUF3QixJQUFJLElBQUksSUFBSSxVQUFVLEVBQUU7NEJBQzFELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRyxDQUFDLENBQUMsQ0FBQzt5QkFDaEM7b0JBQ0gsQ0FBQyxDQUFDLENBQUE7aUJBQ0g7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFBO0lBRUosQ0FBQztJQUVELE9BQU8sQ0FBQyxLQUFLO1FBQ1gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7dUdBN0NVLGtCQUFrQjtvR0FBbEIsa0JBQWtCO1FDVC9CLDZCQUFjO1FBQ1YsZ0NBQTZEO1FBQ3pELDZCQUFPO1FBQ0gsMEJBQUk7UUFDQSxxRkFFZTtRQUNuQixpQkFBSztRQUNULGlCQUFRO1FBQ1IsNkJBQU87UUFDSCxrRUFhSztRQUNULGlCQUFRO1FBQ1osaUJBQVE7UUFDWiwwQkFBZTtRQUNmLG1FQUVNOztRQXpCeUMsZUFBWTtRQUFaLHFDQUFZO1FBTTNCLGVBQVM7UUFBVCxrQ0FBUztRQWlCZixlQUFVO1FBQVYsK0JBQVU7O3VGRGxCdkIsa0JBQWtCO2NBTDlCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsY0FBYztnQkFDeEIsV0FBVyxFQUFFLDRCQUE0QjtnQkFDekMsU0FBUyxFQUFFLENBQUMsMkJBQTJCLENBQUM7YUFDekM7c0NBR1csUUFBUTtrQkFBakIsTUFBTTtZQUVFLElBQUk7a0JBQVosS0FBSztZQUVPLElBQUk7a0JBQWhCLEtBQUs7WUFjRyxPQUFPO2tCQUFmLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT3V0cHV0LCBJbnB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvd3MgfSBmcm9tICcuLi8uLi9tb2RlbHMvdGFibGVsaXN0Lm1vZGVsJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2lkLXRhYmxlbGlzdCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3RhYmxlbGlzdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vdGFibGVsaXN0LmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFibGVsaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQE91dHB1dCgpIGRvd25sb2FkID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XHJcblxyXG4gIEBJbnB1dCgpIHJvd3M6IEFycmF5PFJvd3M+O1xyXG5cclxuICBASW5wdXQoKSBzZXQgd2FpdCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fV2FpdCA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgcXVhbnRpdHk6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICB2YWx1ZSA9IFtdO1xyXG5cclxuICBnZXQgd2FpdCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9XYWl0O1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfV2FpdDogYm9vbGVhbjtcclxuXHJcbiAgQElucHV0KCkgY29saGVhZDogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuXHJcbiAgICAvKiBlbiBjYXNvIGRlIHF1ZSBubyBzZSBhZ3JlZ3VlIGxhIGNvbCBkZSBjYW50aWRhZCAqL1xyXG4gICAgdGhpcy5yb3dzLmZvckVhY2gocm93ID0+IHtcclxuICAgICAgaWYgKHJvdy5xdHkgPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgdGhpcy52YWx1ZS5wdXNoKHJvdy5xdHkpO1xyXG4gICAgICAgIGlmICh0aGlzLnZhbHVlLmxlbmd0aCA9PSB0aGlzLnJvd3MubGVuZ3RoKSB7XHJcbiAgICAgICAgICB0aGlzLnF1YW50aXR5ID0gZmFsc2U7XHJcbiAgICAgICAgICB0aGlzLmNvbGhlYWQuZm9yRWFjaCgobmFtZSxpbmRleCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAobmFtZSA9PSAnQ2FudGlkYWQgZGUgZG9jdW1lbnRvcycgfHwgbmFtZSA9PSAnQ2FudGlkYWQnKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5jb2xoZWFkLnNwbGljZShpbmRleCwgIDEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSlcclxuXHJcbiAgfVxyXG5cclxuICBsaW5rRG9jKGV2ZW50KSB7XHJcbiAgICB0aGlzLmRvd25sb2FkLmVtaXQoZXZlbnQpOyBcclxuICB9XHJcblxyXG5cclxufVxyXG4iLCI8bmctY29udGFpbmVyPlxyXG4gICAgPHRhYmxlIGNsYXNzPVwiaWQtdGFibGUgaWQtdGFibGUtLW1vYmlsZSB0YWJsZSB0YWJsZS1zdHJpcGVkXCI+XHJcbiAgICAgICAgPHRoZWFkPlxyXG4gICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0Zvcj1cImxldCBoZWFkIG9mIGNvbGhlYWQsIGxldCBpbmRleCA9IGluZGV4XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRoICpuZ0lmPVwiaW5kZXggPD0gMlwiPnt7aGVhZH19PC90aD5cclxuICAgICAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgIDwvdGhlYWQ+XHJcbiAgICAgICAgPHRib2R5PlxyXG4gICAgICAgICAgICA8dHIgKm5nRm9yPVwibGV0IHJvdyBvZiByb3dzLCBsZXQgaW5kZXggPSBpbmRleFwiPlxyXG4gICAgICAgICAgICAgICAgPHRkPjxzcGFuIGNsYXNzPVwidGVybVwiPnt7Y29saGVhZFtpbmRleF19fTwvc3Bhbj4gPHNwYW4gY2xhc3M9XCJ2YWxcIj57e3Jvdy5pdGVtfX08L3NwYW4+PC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZCAqbmdJZj1cInF1YW50aXR5XCI+PHNwYW4gY2xhc3M9XCJ0ZXJtXCI+e3tjb2xoZWFkW2luZGV4XX19PC9zcGFuPiA8c3BhbiBjbGFzcz1cInZhbFwiPnt7cm93LnF0eX19PC9zcGFuPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0ZXJtXCI+e3tjb2xoZWFkW2luZGV4XX19PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwidmFsIG1iLTBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKTtcIiAoY2xpY2spPVwibGlua0RvYyhyb3cuaXRlbSlcIiBjbGFzcz1cImxpbmsgbGluay1pY29uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zIGxpbmtcIj5pbnNlcnRfZHJpdmVfZmlsZTwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPnt7cm93Lm5hbWVsaW5rfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICA8L3Rib2R5PlxyXG4gICAgPC90YWJsZT5cclxuPC9uZy1jb250YWluZXI+XHJcbjxkaXYgY2xhc3M9XCJsb2FkZXJibG9ja1wiICpuZ0lmPVwid2FpdFwiPlxyXG4gICAgPGRpdiByb2xlPVwic3RhdHVzXCIgY2xhc3M9XCJzcGlubmVyLWJvcmRlciB0ZXh0LXByaW1hcnlcIj48c3BhbiBjbGFzcz1cInNyLW9ubHlcIj5Mb2FkaW5nLi4uPC9zcGFuPjwvZGl2PlxyXG48L2Rpdj4iXX0=