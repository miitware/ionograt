import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
// calendar
// import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import { TextMaskModule } from 'angular2-text-mask';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { NgxPanZoomModule } from 'ngx-panzoom';
import { IsapreDigitalComponent } from './isapre-digital.component';
import { ValidateContactInfoComponent } from './validate-contact-info/validate-contact-info.component';
import { CarouselComponent } from './carousel/carousel.component';
import { LoadfilesComponent } from './loadfiles/loadfiles.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { DndDirective } from './loadfiles/dnd.directive';
import { TooltipDirective } from '../directives/tooltip.directive';
import { AnaltyticsDirective } from '../directives/analtytics.directive';
import { ToastDirective } from '../directives/toast.directive';
import { PagerComponent } from './pager/pager.component';
import { CalendarComponent } from './calendar/calendar.component';
import { SelectComponent } from './select/select.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { DocumentsComponent } from './documents/documents.component';
import { TablelistComponent } from './tablelist/tablelist.component';
import { StepperComponent } from './stepper/stepper.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import * as i0 from "@angular/core";
const externalModules = [
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    SatDatepickerModule,
    SatNativeDateModule,
    NgSelectModule,
    NgxPanZoomModule
];
export class IsapreDigitalModule {
}
/** @nocollapse */ IsapreDigitalModule.ɵfac = function IsapreDigitalModule_Factory(t) { return new (t || IsapreDigitalModule)(); };
/** @nocollapse */ IsapreDigitalModule.ɵmod = /** @pureOrBreakMyCode */ i0.ɵɵdefineNgModule({ type: IsapreDigitalModule });
/** @nocollapse */ IsapreDigitalModule.ɵinj = /** @pureOrBreakMyCode */ i0.ɵɵdefineInjector({ providers: [
        {
            provide: LOCALE_ID,
            useValue: 'es-CL'
        }
    ], imports: [[
            FormsModule,
            ReactiveFormsModule,
            SwiperModule,
            PdfViewerModule,
            PdfJsViewerModule,
            externalModules,
            TextMaskModule
        ], MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        NgSelectModule,
        NgxPanZoomModule] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(IsapreDigitalModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    IsapreDigitalComponent,
                    ValidateContactInfoComponent,
                    CarouselComponent,
                    LoadfilesComponent,
                    DndDirective,
                    ProgressBarComponent,
                    TooltipDirective,
                    AnaltyticsDirective,
                    ToastDirective,
                    PagerComponent,
                    CalendarComponent,
                    SelectComponent,
                    DocumentsComponent,
                    TablelistComponent,
                    StepperComponent
                ],
                imports: [
                    FormsModule,
                    ReactiveFormsModule,
                    SwiperModule,
                    PdfViewerModule,
                    PdfJsViewerModule,
                    externalModules,
                    TextMaskModule
                ],
                exports: [
                    IsapreDigitalComponent,
                    ValidateContactInfoComponent,
                    CarouselComponent,
                    LoadfilesComponent,
                    PagerComponent,
                    TooltipDirective,
                    ToastDirective,
                    AnaltyticsDirective,
                    externalModules,
                    CalendarComponent,
                    SelectComponent,
                    DocumentsComponent,
                    TablelistComponent,
                    StepperComponent
                ],
                providers: [
                    {
                        provide: LOCALE_ID,
                        useValue: 'es-CL'
                    }
                ]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(IsapreDigitalModule, { declarations: [IsapreDigitalComponent,
        ValidateContactInfoComponent,
        CarouselComponent,
        LoadfilesComponent,
        DndDirective,
        ProgressBarComponent,
        TooltipDirective,
        AnaltyticsDirective,
        ToastDirective,
        PagerComponent,
        CalendarComponent,
        SelectComponent,
        DocumentsComponent,
        TablelistComponent,
        StepperComponent], imports: [FormsModule,
        ReactiveFormsModule,
        SwiperModule,
        PdfViewerModule,
        PdfJsViewerModule, MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        NgSelectModule,
        NgxPanZoomModule, TextMaskModule], exports: [IsapreDigitalComponent,
        ValidateContactInfoComponent,
        CarouselComponent,
        LoadfilesComponent,
        PagerComponent,
        TooltipDirective,
        ToastDirective,
        AnaltyticsDirective, MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        NgSelectModule,
        NgxPanZoomModule, CalendarComponent,
        SelectComponent,
        DocumentsComponent,
        TablelistComponent,
        StepperComponent] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXNhcHJlLWRpZ2l0YWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IkM6L1VzZXJzL1VzdWFyaW8vRGVza3RvcC9saWJyZXJpYW5ndWxhci0yOS0xMC0yMS9jb3BpYS1hbmd1bGFyLWxpYi11cGQtMTItc2luLXJ1dC9wcm9qZWN0cy9pc2FwcmUtZGlnaXRhbC9zcmMvIiwic291cmNlcyI6WyJsaWIvaXNhcHJlLWRpZ2l0YWwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUdyRCxXQUFXO0FBQ1gsZ0dBQWdHO0FBQ2hHLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzdFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDbEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUsvQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUN2RyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDekQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDbkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDekUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQy9ELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ25FLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQzs7QUFFN0QsTUFBTSxlQUFlLEdBQUc7SUFDdEIsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2QsZ0JBQWdCO0NBQ2pCLENBQUE7QUFzREQsTUFBTSxPQUFPLG1CQUFtQjs7eUdBQW5CLG1CQUFtQjtvR0FBbkIsbUJBQW1CO3lHQVJuQjtRQUNUO1lBQ0UsT0FBTyxFQUFFLFNBQVM7WUFDbEIsUUFBUSxFQUFFLE9BQU87U0FDbEI7S0FDRixZQTlCUTtZQUNQLFdBQVc7WUFDWCxtQkFBbUI7WUFDbkIsWUFBWTtZQUNaLGVBQWU7WUFDZixpQkFBaUI7WUFDakIsZUFBZTtZQUNmLGNBQWM7U0FDZixFQXBDRCxtQkFBbUI7UUFDbkIsY0FBYztRQUNkLG1CQUFtQjtRQUNuQixtQkFBbUI7UUFDbkIsbUJBQW1CO1FBQ25CLGNBQWM7UUFDZCxnQkFBZ0I7dUZBdURMLG1CQUFtQjtjQW5EL0IsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixzQkFBc0I7b0JBQ3RCLDRCQUE0QjtvQkFDNUIsaUJBQWlCO29CQUNqQixrQkFBa0I7b0JBQ2xCLFlBQVk7b0JBQ1osb0JBQW9CO29CQUNwQixnQkFBZ0I7b0JBQ2hCLG1CQUFtQjtvQkFDbkIsY0FBYztvQkFDZCxjQUFjO29CQUNkLGlCQUFpQjtvQkFDakIsZUFBZTtvQkFDZixrQkFBa0I7b0JBQ2xCLGtCQUFrQjtvQkFDbEIsZ0JBQWdCO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLFlBQVk7b0JBQ1osZUFBZTtvQkFDZixpQkFBaUI7b0JBQ2pCLGVBQWU7b0JBQ2YsY0FBYztpQkFDZjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1Asc0JBQXNCO29CQUN0Qiw0QkFBNEI7b0JBQzVCLGlCQUFpQjtvQkFDakIsa0JBQWtCO29CQUNsQixjQUFjO29CQUNkLGdCQUFnQjtvQkFDaEIsY0FBYztvQkFDZCxtQkFBbUI7b0JBQ25CLGVBQWU7b0JBQ2YsaUJBQWlCO29CQUNqQixlQUFlO29CQUNmLGtCQUFrQjtvQkFDbEIsa0JBQWtCO29CQUNsQixnQkFBZ0I7aUJBQ2pCO2dCQUNELFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsU0FBUzt3QkFDbEIsUUFBUSxFQUFFLE9BQU87cUJBQ2xCO2lCQUNGO2FBRUY7O3dGQUNZLG1CQUFtQixtQkFqRDVCLHNCQUFzQjtRQUN0Qiw0QkFBNEI7UUFDNUIsaUJBQWlCO1FBQ2pCLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osb0JBQW9CO1FBQ3BCLGdCQUFnQjtRQUNoQixtQkFBbUI7UUFDbkIsY0FBYztRQUNkLGNBQWM7UUFDZCxpQkFBaUI7UUFDakIsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEIsZ0JBQWdCLGFBR2hCLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsWUFBWTtRQUNaLGVBQWU7UUFDZixpQkFBaUIsRUFqQ25CLG1CQUFtQjtRQUNuQixjQUFjO1FBQ2QsbUJBQW1CO1FBQ25CLG1CQUFtQjtRQUNuQixtQkFBbUI7UUFDbkIsY0FBYztRQUNkLGdCQUFnQixFQTZCZCxjQUFjLGFBR2Qsc0JBQXNCO1FBQ3RCLDRCQUE0QjtRQUM1QixpQkFBaUI7UUFDakIsa0JBQWtCO1FBQ2xCLGNBQWM7UUFDZCxnQkFBZ0I7UUFDaEIsY0FBYztRQUNkLG1CQUFtQixFQTdDckIsbUJBQW1CO1FBQ25CLGNBQWM7UUFDZCxtQkFBbUI7UUFDbkIsbUJBQW1CO1FBQ25CLG1CQUFtQjtRQUNuQixjQUFjO1FBQ2QsZ0JBQWdCLEVBeUNkLGlCQUFpQjtRQUNqQixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLGtCQUFrQjtRQUNsQixnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTE9DQUxFX0lEIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuXHJcbi8vIGNhbGVuZGFyXHJcbi8vIGltcG9ydCB7IE1hdERhdGVwaWNrZXJNb2R1bGUsIE1hdElucHV0TW9kdWxlLCBNYXROYXRpdmVEYXRlTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBTYXREYXRlcGlja2VyTW9kdWxlLCBTYXROYXRpdmVEYXRlTW9kdWxlIH0gZnJvbSAnc2F0dXJuLWRhdGVwaWNrZXInO1xyXG5pbXBvcnQgeyBUZXh0TWFza01vZHVsZSB9IGZyb20gJ2FuZ3VsYXIyLXRleHQtbWFzayc7XHJcblxyXG5pbXBvcnQgeyBTd2lwZXJNb2R1bGUgfSBmcm9tICduZ3gtc3dpcGVyLXdyYXBwZXInO1xyXG5pbXBvcnQgeyBQZGZWaWV3ZXJNb2R1bGUgfSBmcm9tICduZzItcGRmLXZpZXdlcic7XHJcbmltcG9ydCB7IFBkZkpzVmlld2VyTW9kdWxlIH0gZnJvbSAnbmcyLXBkZmpzLXZpZXdlcic7XHJcbmltcG9ydCB7IE5neFBhblpvb21Nb2R1bGUgfSBmcm9tICduZ3gtcGFuem9vbSc7XHJcblxyXG5cclxuXHJcblxyXG5pbXBvcnQgeyBJc2FwcmVEaWdpdGFsQ29tcG9uZW50IH0gZnJvbSAnLi9pc2FwcmUtZGlnaXRhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBWYWxpZGF0ZUNvbnRhY3RJbmZvQ29tcG9uZW50IH0gZnJvbSAnLi92YWxpZGF0ZS1jb250YWN0LWluZm8vdmFsaWRhdGUtY29udGFjdC1pbmZvLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcm91c2VsQ29tcG9uZW50IH0gZnJvbSAnLi9jYXJvdXNlbC9jYXJvdXNlbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMb2FkZmlsZXNDb21wb25lbnQgfSBmcm9tICcuL2xvYWRmaWxlcy9sb2FkZmlsZXMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUHJvZ3Jlc3NCYXJDb21wb25lbnQgfSBmcm9tICcuL3Byb2dyZXNzLWJhci9wcm9ncmVzcy1iYXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRG5kRGlyZWN0aXZlIH0gZnJvbSAnLi9sb2FkZmlsZXMvZG5kLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFRvb2x0aXBEaXJlY3RpdmUgfSBmcm9tICcuLi9kaXJlY3RpdmVzL3Rvb2x0aXAuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgQW5hbHR5dGljc0RpcmVjdGl2ZSB9IGZyb20gJy4uL2RpcmVjdGl2ZXMvYW5hbHR5dGljcy5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBUb2FzdERpcmVjdGl2ZSB9IGZyb20gJy4uL2RpcmVjdGl2ZXMvdG9hc3QuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgUGFnZXJDb21wb25lbnQgfSBmcm9tICcuL3BhZ2VyL3BhZ2VyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhbGVuZGFyQ29tcG9uZW50IH0gZnJvbSAnLi9jYWxlbmRhci9jYWxlbmRhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTZWxlY3RDb21wb25lbnQgfSBmcm9tICcuL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmdTZWxlY3RNb2R1bGUgfSBmcm9tICdAbmctc2VsZWN0L25nLXNlbGVjdCc7XHJcbmltcG9ydCB7IERvY3VtZW50c0NvbXBvbmVudCB9IGZyb20gJy4vZG9jdW1lbnRzL2RvY3VtZW50cy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUYWJsZWxpc3RDb21wb25lbnQgfSBmcm9tICcuL3RhYmxlbGlzdC90YWJsZWxpc3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU3RlcHBlckNvbXBvbmVudCB9IGZyb20gJy4vc3RlcHBlci9zdGVwcGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdERhdGVwaWNrZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kYXRlcGlja2VyJztcclxuaW1wb3J0IHsgTWF0SW5wdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9pbnB1dCc7XHJcbmltcG9ydCB7IE1hdE5hdGl2ZURhdGVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jb3JlJztcclxuICBcclxuY29uc3QgZXh0ZXJuYWxNb2R1bGVzID0gW1xyXG4gIE1hdERhdGVwaWNrZXJNb2R1bGUsXHJcbiAgTWF0SW5wdXRNb2R1bGUsXHJcbiAgTWF0TmF0aXZlRGF0ZU1vZHVsZSxcclxuICBTYXREYXRlcGlja2VyTW9kdWxlLFxyXG4gIFNhdE5hdGl2ZURhdGVNb2R1bGUsXHJcbiAgTmdTZWxlY3RNb2R1bGUsXHJcbiAgTmd4UGFuWm9vbU1vZHVsZVxyXG5dXHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIElzYXByZURpZ2l0YWxDb21wb25lbnQsXHJcbiAgICBWYWxpZGF0ZUNvbnRhY3RJbmZvQ29tcG9uZW50LFxyXG4gICAgQ2Fyb3VzZWxDb21wb25lbnQsXHJcbiAgICBMb2FkZmlsZXNDb21wb25lbnQsXHJcbiAgICBEbmREaXJlY3RpdmUsXHJcbiAgICBQcm9ncmVzc0JhckNvbXBvbmVudCxcclxuICAgIFRvb2x0aXBEaXJlY3RpdmUsXHJcbiAgICBBbmFsdHl0aWNzRGlyZWN0aXZlLFxyXG4gICAgVG9hc3REaXJlY3RpdmUsXHJcbiAgICBQYWdlckNvbXBvbmVudCxcclxuICAgIENhbGVuZGFyQ29tcG9uZW50LFxyXG4gICAgU2VsZWN0Q29tcG9uZW50LFxyXG4gICAgRG9jdW1lbnRzQ29tcG9uZW50LFxyXG4gICAgVGFibGVsaXN0Q29tcG9uZW50LFxyXG4gICAgU3RlcHBlckNvbXBvbmVudFxyXG4gIF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgRm9ybXNNb2R1bGUsXHJcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgU3dpcGVyTW9kdWxlLFxyXG4gICAgUGRmVmlld2VyTW9kdWxlLFxyXG4gICAgUGRmSnNWaWV3ZXJNb2R1bGUsXHJcbiAgICBleHRlcm5hbE1vZHVsZXMsXHJcbiAgICBUZXh0TWFza01vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgSXNhcHJlRGlnaXRhbENvbXBvbmVudCxcclxuICAgIFZhbGlkYXRlQ29udGFjdEluZm9Db21wb25lbnQsXHJcbiAgICBDYXJvdXNlbENvbXBvbmVudCxcclxuICAgIExvYWRmaWxlc0NvbXBvbmVudCxcclxuICAgIFBhZ2VyQ29tcG9uZW50LFxyXG4gICAgVG9vbHRpcERpcmVjdGl2ZSxcclxuICAgIFRvYXN0RGlyZWN0aXZlLFxyXG4gICAgQW5hbHR5dGljc0RpcmVjdGl2ZSxcclxuICAgIGV4dGVybmFsTW9kdWxlcyxcclxuICAgIENhbGVuZGFyQ29tcG9uZW50LFxyXG4gICAgU2VsZWN0Q29tcG9uZW50LFxyXG4gICAgRG9jdW1lbnRzQ29tcG9uZW50LFxyXG4gICAgVGFibGVsaXN0Q29tcG9uZW50LFxyXG4gICAgU3RlcHBlckNvbXBvbmVudFxyXG4gIF0sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IExPQ0FMRV9JRCxcclxuICAgICAgdXNlVmFsdWU6ICdlcy1DTCdcclxuICAgIH1cclxuICBdXHJcblxyXG59KVxyXG5leHBvcnQgY2xhc3MgSXNhcHJlRGlnaXRhbE1vZHVsZSB7IH1cclxuIl19