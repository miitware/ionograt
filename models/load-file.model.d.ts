export interface LoadFileUi {
    multiple: boolean;
    filesUpload?: number;
    controlProcess?: boolean;
    disabled?: boolean;
    filesExtend?: boolean;
    maxSize?: number;
}
export interface ProcessControl {
    name: string;
    loading: boolean;
    completed: boolean;
}
export interface LoadFileData {
    name: string;
    lastModified: number;
    lastModifiedDate: Date;
    webkitRelativePath: string;
    size: number;
    type: string;
    progress: number;
    valid: boolean;
    alert: boolean;
    preview: string;
    length: number;
}
