export interface Rows {
    item: string;
    qty?: number;
    namelink: string;
}
