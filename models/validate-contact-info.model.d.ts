export interface ValidateContactInfoUi {
    enablePhoneField?: boolean;
}
export interface ValidateContactInfoData {
    email: string;
    phone: string;
}
export interface ValidateContactInfoOutputData {
    email?: string;
    phone?: string;
    validForm: boolean;
}
