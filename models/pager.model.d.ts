export interface IPaginateOption {
    currentPage: number;
    perPage: number;
    totalPages?: number;
    pages?: number[];
}
export interface IPaginateOuput {
    since: number;
    until: number;
}
