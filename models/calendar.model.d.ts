export interface calendarUI {
    range?: boolean;
    inlineCalendar?: boolean;
    disabled?: boolean;
    min?: Date;
    max?: Date;
    label?: string;
    placeholder?: string;
    limitedRange?: boolean;
}
