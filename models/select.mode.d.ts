export interface SelectUi {
    label?: string;
    placeholder?: string;
    state: boolean;
    message?: string;
    disabled?: boolean;
    notfound?: string;
}
export interface Option {
    label?: string | number;
    value: string;
}
export interface SelectOutput {
    label?: string | number;
    state?: boolean;
    value: string;
}
