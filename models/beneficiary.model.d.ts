export interface Beneficiary {
    name: string;
    dv: string;
    rut: number;
    avatar: string;
    tag: TagBeneficiary;
    rutcomplete?: string;
    deciduous?: boolean;
}
export interface TagBeneficiary {
    label: string;
    state: boolean;
    acronym?: boolean;
}
export interface BeneficiarySelected {
    beneficiary?: Beneficiary;
    familyGroup: boolean;
    index?: number;
}
