# Documentation
### General
La librería de isapre-digital se creo para estandarizar los componentes dentro de todos los proyectos y células.

##### Uso:
Se deben instalar la librería y dependencias con el comando de npm
```sh
npm i git+http://10.195.195.8:7990/scm/bdu/libreria-angular.git ngx-swiper-wrapper
```
Agregar la librería en los import del app.module.ts
```sh
import { IsapreDigitalModule } from 'isapre-digital';

import[
 ...,
 IsapreDigitalModule,
],
```
##### Estilos:
La librería no posee los estilos del webkit, para tener los estilos de las isapres deben instalar la dependencia de webkit
```sh
<!-- Agregar el webkit a los proyectos angular  -->

npm i git+http://10.195.195.8:7990/scm/web/webkit.git --save

// package.json
'bootstrap-banmedica':'git+http://10.195.195.8:7990/scm/web/webkit.git'
```
## Modelos
Existen los siguientes modelos de datos cómo ayuda para la estructura de datos.
```sh
import { Beneficiary } from 'isapre-digital/models/beneficiary.model';

export interface Beneficiary {
    name: string;
    rut: string;
    avatar: string;
    gender: string;
    tag: TagBeneficiary;
}
export interface TagBeneficiary {
    label: string;
    state: boolean;
}
export interface BeneficiarySelected {
    beneficiary?: Beneficiary;
    familyGroup: boolean;
}
```
```sh
import { Beneficiary } from 'isapre-digital/models/carousel.model';

export interface CarouselUi {
    familyGroup: boolean;
}

```
```sh
import { Beneficiary } from 'isapre-digital/models/validate-contact-info.model';

export interface ValidateContactInfoUi {
  enablePhoneField?:boolean
}
export interface ValidateContactInfoData {
    email: string;
    phone: string;
}
export interface ValidateContactInfoOutputData {
  email?: string,
  phone?: string,
  validForm:boolean
}


```


## Componente Validación datos de contacto

Recibe los datos del usuario permitiendo editarlos y enviarlos cómo parametros de salida.
##### Uso:
Debes agregar el tag
`<id-validate-contact-info></id-validate-contact-info>` 
##### Directivas:
##### ui
Acepta un object con un parametro para mostrar o no el campo de teléfono
```sh
<!-- ValidateContactInfoUi: object = { 
enablePhoneField:boolean
} -->

 <id-validate-contact-info [ui]="ValidateContactInfoUi"></id-validate-contact-info>
```

##### dataEntry
Recibe un object datos del usuario cómo email y teléfono
```sh
<!-- ValidateContactInfoDataEntry: object = {
 email: 'string',
 phone: 'string',  
} -->

 <id-validate-contact-info [dataEntry]="ValidateContactInfoDataEntry"></id-validate-contact-info>
```
##### dataOutput
Entrega la data actualizada para su posterior procesamiento.
* También entrega el key validForm en true/false para poder gestionar eventos externos
```sh
// Salida
{email: 'santiagoyanez@multiplica.com',
phone: '569111111',
validForm: true} 

 <id-validate-contact-info (dataOutput)="getValidateContactInfoData($event)"></id-validate-contact-info>
```

## Componente Carrusel
Permite la selección de un beneficiario o grupo familiar a través de un carrusel de cards
- Máximo 4 cards por vista.
- Si hay 4 cards o menos desaparecen las flechas y puntos.
- Si hay 3 cards o menos se alinean a la izquierda (no centrados).
- Beneficiarios: si hay solo 1 card, se salta el paso.
##### Uso:
Debes agregar el tag
`<id-carousel></id-carousel>` 
##### Directivas:
##### ui
Acepta un object con un parametro para mostrar o no la card de grupo familiar.
```sh
// CarouselUi: object = { familyGroup: true/false }

 <id-carousel [ui]="CarouselUi"></id-carousel>
```
##### dataOutput
Permite obtener el beneficiario seleccionado y/o grupo familiar
```sh
CarouselDataEntry: Array<any> = [{
 name: 'Alfonso Tapia Parada',
 rut: '13.829.259-2',
 gender: 'M',
 avatar: '../Avatar_v3_2.svg',
 tag: { 
  label: null(string),
  state: false/true 
 }
}]

 <id-carousel [dataEntry]="CarouselDataEntry"></id-carousel>
```
##### dataEntry
Acepta un array con beneficiarios, los cuales serán iterados para generar las card. (obligatorio)
```sh
// Salida
beneficiary: {name: 'Alfonso Tapia Parada',...}
familyGroup: true/false 

<id-carousel (dataOutput)="getCarouselData($event)"></id-carousel>
```

## Componente Carga de archivos
- La carga de archivos permite archivos JPG, PNG, PDF y con un maximo de 5MB.
- Para renderizar el thumb del pdf se utiliza el plugin ng2-pdf-viewer
##### Uso:
Permite la selección de uno o mas archivos y genera una miniatura del documento
`<id-loadfiles></id-loadfiles>` 
##### Directivas:
##### ui
Acepta un object con el parametro multiple que le permite al input recibir o no multiples archivos
```sh
// LoadFileUi: object = { multiple: true/false }

 <id-loadfiles [ui]="LoadFileUi"></id-loadfiles>
```
##### dataEntry
(Opcional)
Acepta un array con un array de archivos, los cuales serán iterados para generar los thumbs de las imágenes ya guardadas en la base de datos. idealmente injectar los mismo datos que genera el ouput. Cómo mínimo debemos enviar name, type, preview
```sh
<!-- filesUploadedFromDataBase: Array<any> = [{
 name: 'avatar.jpg',
 type: 'image/jpeg',
 progress: 100,
 preview:'data:image/jpeg;base64,....',
 ...
 }
}] -->

<id-loadfiles [dataEntry]="filesUploadedFromDataBase"></id-loadfiles>
```

##### dataOutput
Permite obtener los archivos cargados
```sh

//  0: File{
        name: "name.jpg"
        lastModified: 1580474042526
        lastModifiedDate: Fri Jan 31 2020 09:34:02 GMT-0300 (hora de verano de Chile)
        ...
    }

<id-loadfiles (dataOutput)="getLoadFileData($event)"></id-loadfiles>
```
## Directiva Tooltip

Los tooltips, son un cuadro de diálogo que permite extenderse sobre un tema puntal.
##### Uso:
id-tooltip es una directiva que se puede utilizar en diferentes selectores, permite un máximo de 200 caracteres, si la cantidad de texto es mayor se realiza un trim.
#### Parametros de Tooltip
**text**: Es un input de la directiva que permite pasar un valor de tipo string el cual se visualizara dentro del tooltip
**placement**: Es un input de la directiva que permite indicar la posición en la que se motrara el tooltip en relación al elemento que posee la directiva.

`<span id-tooltip text="string" placement="posición"></span>` 

## Analitica y Marcaje
Esta utilidad es una ayuda para realizar mediciones web avanzadas basadas en Google Tag Manager y Google Analytics.

#### Directiva
En los elementos en los que se desee registar un evento, se debe agregar la directiva id-analtytics con sol siguietnes atributos:

**categoryEvent**: Hace referencia a la sección del sitio web en donde el usuario está interactuando.
**labelEvent**: Texto para identificar el elemento que gatilla el evento.
**typeEvent (opcional)**: Tipo de evento, el valor por defecto es 'click'.
```sh
<button id-analtytics [categoryEvent]="'Home'" [labelEvent]="'Volver'"></button>
```

#### Servicio
En los elementos en los que se desee registar un evento, se debe agregar la directiva id-analtytics con sol siguietnes atributos:
```sh
import { AnalyticsService } from 'isapre-digital';

constructor (
 private analyticsService: AnalyticsService,
) { }
```
El servicio **AnalyticsService** se utiliara para registar eventos de forma independiente de los elementos visuales, como por ejemplo el enrtar a una pantalla en especifico. Este servicio posee 2 funciones:

**eventRegister**, que recibe los parametros:
*categoryEvent*: Hace referencia a la sección del sitio web en donde el usuario está interactuando.
*labelEvent*: Texto para identificar el elemento que gatilla el evento.
*typeEvent (opcional)*: Tipo de evento, el valor por defecto es 'click'.

**pageLoadRegister**, que recibe los parametros:
*categoryEvent*: Hace referencia a la sección del sitio web en donde el usuario está interactuando.
*pageEvent*: Direccion o identificador de la pagina actual.
*userRut*: Rut del usuario logeado.
