import { EventEmitter, Renderer2 } from '@angular/core';
import * as i0 from "@angular/core";
export declare class ToastDirective {
    private _renderer;
    toastText: string;
    type: string;
    toast: HTMLElement;
    maxCharacters: number;
    timeoutToast: any;
    constructor(_renderer: Renderer2);
    idToast: EventEmitter<Object>;
    onClick(event: MouseEvent, targetElement: HTMLElement): void;
    create(): void;
    show(): void;
    hide(): void;
    timer(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ToastDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<ToastDirective, "[id-toast]", never, { "toastText": "text"; "type": "type"; }, {}, never>;
}
