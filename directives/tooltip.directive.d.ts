import { Renderer2, ElementRef, OnDestroy } from '@angular/core';
import * as i0 from "@angular/core";
export declare class TooltipDirective implements OnDestroy {
    private _el;
    private _renderer;
    tooltipTex: string;
    tooltip: HTMLElement;
    arrow: HTMLElement;
    placement: string;
    offset: number;
    maxCharacters: number;
    constructor(_el: ElementRef, _renderer: Renderer2);
    onMouseEnter(): void;
    onMouseLeave(): void;
    show(): void;
    hide(): void;
    create(): void;
    setPosition(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TooltipDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<TooltipDirective, "[id-tooltip]", never, { "tooltipTex": "text"; "placement": "placement"; }, {}, never>;
}
