import { AnalyticsService } from '../services/analytics.service';
import * as i0 from "@angular/core";
export declare class AnaltyticsDirective {
    private analyticsService;
    categoryEvent: string;
    labelEvent: string;
    typeEvent: string;
    constructor(analyticsService: AnalyticsService);
    onClick(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<AnaltyticsDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<AnaltyticsDirective, "[id-analtytics]", never, { "categoryEvent": "categoryEvent"; "labelEvent": "labelEvent"; "typeEvent": "typeEvent"; }, {}, never>;
}
