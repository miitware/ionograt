import * as i0 from '@angular/core';
import { Injectable, Component, EventEmitter, Input, Output, Directive, HostListener, ViewChild, HostBinding, LOCALE_ID, NgModule } from '@angular/core';
import * as i1 from '@angular/forms';
import { FormControl, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as i4 from 'saturn-datepicker';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import * as i5 from 'angular2-text-mask';
import { TextMaskModule } from 'angular2-text-mask';
import * as i2 from 'ngx-swiper-wrapper';
import { SwiperModule } from 'ngx-swiper-wrapper';
import * as i2$1 from 'ng2-pdf-viewer';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import * as i4$1 from 'ng2-pdfjs-viewer';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import * as i3$1 from 'ngx-panzoom';
import { PanZoomConfig, NgxPanZoomModule } from 'ngx-panzoom';
import * as i1$1 from '@angular/common';
import * as _rut from 'rut-helpers';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';
import * as i2$2 from '@angular/material/form-field';
import * as i3 from '@angular/material/input';
import { MatInputModule } from '@angular/material/input';
import * as i2$3 from '@ng-select/ng-select';
import { NgSelectComponent, NgSelectModule } from '@ng-select/ng-select';
import * as i1$2 from '@angular/platform-browser';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

class IsapreDigitalService {
    constructor() { }
}
/** @nocollapse */ IsapreDigitalService.ɵfac = function IsapreDigitalService_Factory(t) { return new (t || IsapreDigitalService)(); };
/** @nocollapse */ IsapreDigitalService.ɵprov = /** @pureOrBreakMyCode */ i0.ɵɵdefineInjectable({ token: IsapreDigitalService, factory: IsapreDigitalService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(IsapreDigitalService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();

class IsapreDigitalComponent {
    constructor() { }
    ngOnInit() {
    }
}
/** @nocollapse */ IsapreDigitalComponent.ɵfac = function IsapreDigitalComponent_Factory(t) { return new (t || IsapreDigitalComponent)(); };
/** @nocollapse */ IsapreDigitalComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: IsapreDigitalComponent, selectors: [["id-isapre-digital"]], decls: 2, vars: 0, template: function IsapreDigitalComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "p");
        i0.ɵɵtext(1, " isapre-digital works!!!! ");
        i0.ɵɵelementEnd();
    } }, encapsulation: 2 });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(IsapreDigitalComponent, [{
        type: Component,
        args: [{
                selector: 'id-isapre-digital',
                template: `
    <p>
      isapre-digital works!!!!
    </p>
  `,
                styles: []
            }]
    }], function () { return []; }, null); })();

function ValidateContactInfoComponent_a_9_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "a", 10);
    i0.ɵɵlistener("click", function ValidateContactInfoComponent_a_9_Template_a_click_0_listener($event) { i0.ɵɵrestoreView(_r5); const ctx_r4 = i0.ɵɵnextContext(); return ctx_r4.editInput($event, "email"); });
    i0.ɵɵelementStart(1, "i", 5);
    i0.ɵɵtext(2, "edit");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_small_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1, "* El correo electr\u00F3nico es obligatorio");
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_small_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1, "* Debes ingresar un correo electr\u00F3nico valido");
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_div_12_a_7_Template(rf, ctx) { if (rf & 1) {
    const _r10 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "a", 10);
    i0.ɵɵlistener("click", function ValidateContactInfoComponent_div_12_a_7_Template_a_click_0_listener($event) { i0.ɵɵrestoreView(_r10); const ctx_r9 = i0.ɵɵnextContext(2); return ctx_r9.editInput($event, "phone"); });
    i0.ɵɵelementStart(1, "i", 5);
    i0.ɵɵtext(2, "edit");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_div_12_small_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1, "* Debes ingresar 9 d\u00EDgitos");
    i0.ɵɵelementEnd();
} }
function ValidateContactInfoComponent_div_12_small_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1, "* Debes ingresar s\u00F3lo n\u00FAmeros");
    i0.ɵɵelementEnd();
} }
const _c0$7 = function (a0) { return { "fg-invalid": a0 }; };
function ValidateContactInfoComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 11);
    i0.ɵɵelementStart(1, "div", 3);
    i0.ɵɵelementStart(2, "input", 12);
    i0.ɵɵlistener("keypress", function ValidateContactInfoComponent_div_12_Template_input_keypress_2_listener($event) { i0.ɵɵrestoreView(_r12); const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.numberOnly($event); })("blur", function ValidateContactInfoComponent_div_12_Template_input_blur_2_listener() { i0.ɵɵrestoreView(_r12); const ctx_r13 = i0.ɵɵnextContext(); return ctx_r13.emitDynamicallyData(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "i", 5);
    i0.ɵɵtext(4, "local_phone");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "label", 13);
    i0.ɵɵtext(6, "Tel\u00E9fono celular");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(7, ValidateContactInfoComponent_div_12_a_7_Template, 3, 0, "a", 7);
    i0.ɵɵtemplate(8, ValidateContactInfoComponent_div_12_small_8_Template, 2, 0, "small", 8);
    i0.ɵɵtemplate(9, ValidateContactInfoComponent_div_12_small_9_Template, 2, 0, "small", 8);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(4, _c0$7, ctx_r3.formValidateContactInfo.get("phone").invalid));
    i0.ɵɵadvance(6);
    i0.ɵɵproperty("ngIf", ctx_r3.formValidateContactInfo.get("phone").disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r3.formValidateContactInfo.get("phone").hasError("minlength") || ctx_r3.formValidateContactInfo.get("phone").hasError("maxlength"));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r3.formValidateContactInfo.get("phone").hasError("pattern"));
} }
const _c1$5 = function (a0) { return [a0]; };
class ValidateContactInfoComponent {
    constructor(_fb, _el) {
        this._fb = _fb;
        this._el = _el;
        this.dataOutput = new EventEmitter();
        this.validationOfIssuedEmail = false;
        this.formValidateContactInfo = this._fb.group({
            email: new FormControl({ value: undefined, disabled: false }, {
                validators: [
                    Validators.required,
                    Validators.pattern('^(?![.])(?!.*[-_.]$)[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+[.][a-zA-Z0-9-.]+$')
                ],
                updateOn: 'blur'
            }),
            phone: new FormControl({ value: undefined, disabled: false }, {
                validators: [
                    Validators.maxLength(9),
                    Validators.minLength(9),
                    Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')
                ],
                updateOn: 'blur'
            })
        });
    }
    ngOnInit() {
        if (this.dataEntry) {
            this.formValidateContactInfo.get('email').setValue(this.dataEntry.email, { emitEvent: false });
            if (this.dataEntry.phone != undefined) {
                this.formValidateContactInfo.get('phone').setValue(this.dataEntry.phone.toString(), { emitEvent: false });
            }
            if (!this.dataEntry.email) {
                this.formValidateContactInfo.controls['email'].enable();
            }
            else {
                if (this.formValidateContactInfo.get('email').valid) {
                    this.formValidateContactInfo.controls['email'].disable();
                }
                else {
                    this.validationOfIssuedEmail = true;
                }
            }
            if (!this.dataEntry.phone) {
                this.formValidateContactInfo.controls['phone'].enable();
            }
            else {
                if (this.formValidateContactInfo.get('phone').valid) {
                    this.formValidateContactInfo.controls['phone'].disable();
                }
            }
        }
        else {
            this.formValidateContactInfo.get('email').setValue(undefined, { emitEvent: false });
            this.formValidateContactInfo.controls['email'].enable();
            this.formValidateContactInfo.get('phone').setValue(undefined, { emitEvent: false });
            this.formValidateContactInfo.controls['phone'].enable();
        }
        this.emitDynamicallyData();
    }
    editInput(event, input) {
        const field = this._el.nativeElement.querySelector('#' + input);
        event.preventDefault();
        this.emitDynamicallyData(false);
        this.formValidateContactInfo.controls[input].enable();
        // set focus to edit
        field.focus();
        // this.formValidateContactInfo.get(input).reset();
    }
    numberOnly(event) {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    emitDynamicallyData(stateForm) {
        let forceSateForm = stateForm != undefined ? stateForm : !this.formValidateContactInfo.invalid;
        let formStatus = { 'validForm': forceSateForm };
        let formDataOutput;
        if (!this.formValidateContactInfo.invalid) {
            this.formValidateContactInfo.controls['email'].disable();
            if (this.formValidateContactInfo.get('phone').value != null && this.formValidateContactInfo.get('phone').value.length > 0) {
                this.formValidateContactInfo.controls['phone'].disable();
            }
        }
        formDataOutput = Object.assign(Object.assign({}, this.formValidateContactInfo.getRawValue()), formStatus);
        this.dataOutput.emit(formDataOutput);
    }
}
/** @nocollapse */ ValidateContactInfoComponent.ɵfac = function ValidateContactInfoComponent_Factory(t) { return new (t || ValidateContactInfoComponent)(i0.ɵɵdirectiveInject(i1.FormBuilder), i0.ɵɵdirectiveInject(i0.ElementRef)); };
/** @nocollapse */ ValidateContactInfoComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: ValidateContactInfoComponent, selectors: [["id-validate-contact-info"]], inputs: { ui: "ui", dataEntry: "dataEntry" }, outputs: { dataOutput: "dataOutput" }, decls: 13, vars: 11, consts: [[3, "formGroup"], [1, "row"], [3, "ngClass"], [1, "form-group", "fg-icon", "fg-icon-action", 3, "ngClass"], ["type", "email", "formControlName", "email", "id", "email", "placeholder", "Correo electr\u00F3nico", "maxlength", "50", "autocomplete", "off", 1, "form-control", 3, "keydown.space", "blur"], [1, "material-icons"], ["for", "email"], ["href", "#", 3, "click", 4, "ngIf"], [4, "ngIf"], ["class", "col-sm-6", 4, "ngIf"], ["href", "#", 3, "click"], [1, "col-sm-6"], ["type", "text", "formControlName", "phone", "id", "phone", "placeholder", "Tel\u00E9fono celular", "maxlength", "9", "minlength", "9", 1, "form-control", 3, "keypress", "blur"], ["for", "phone"]], template: function ValidateContactInfoComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelementStart(3, "div", 3);
        i0.ɵɵelementStart(4, "input", 4);
        i0.ɵɵlistener("keydown.space", function ValidateContactInfoComponent_Template_input_keydown_space_4_listener($event) { return $event.preventDefault(); })("blur", function ValidateContactInfoComponent_Template_input_blur_4_listener() { return ctx.emitDynamicallyData(); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "i", 5);
        i0.ɵɵtext(6, "mail");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(7, "label", 6);
        i0.ɵɵtext(8, "Correo electro\u0301nico");
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(9, ValidateContactInfoComponent_a_9_Template, 3, 0, "a", 7);
        i0.ɵɵtemplate(10, ValidateContactInfoComponent_small_10_Template, 2, 0, "small", 8);
        i0.ɵɵtemplate(11, ValidateContactInfoComponent_small_11_Template, 2, 0, "small", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(12, ValidateContactInfoComponent_div_12_Template, 10, 6, "div", 9);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("formGroup", ctx.formValidateContactInfo);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(7, _c1$5, (ctx.ui == null ? null : ctx.ui.enablePhoneField) != false ? "col-sm-6" : "col-sm-12"));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(9, _c0$7, ctx.formValidateContactInfo.get("email").invalid && ctx.validationOfIssuedEmail));
        i0.ɵɵadvance(6);
        i0.ɵɵproperty("ngIf", ctx.formValidateContactInfo.get("email").disabled);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formValidateContactInfo.get("email").hasError("required") && !ctx.formValidateContactInfo.get("email").pristine);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.formValidateContactInfo.get("email").hasError("pattern"));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", (ctx.ui == null ? null : ctx.ui.enablePhoneField) != false);
    } }, directives: [i1.NgControlStatusGroup, i1.FormGroupDirective, i1$1.NgClass, i1.DefaultValueAccessor, i1.NgControlStatus, i1.FormControlName, i1.MaxLengthValidator, i1$1.NgIf, i1.MinLengthValidator], styles: [".form-control[_ngcontent-%COMP%]:focus{color:#173181}.btn-outline-primary.disabled[_ngcontent-%COMP%], .btn-outline-primary[_ngcontent-%COMP%]:disabled{border-color:#ebebeb;color:#9ba5b7}[_nghost-%COMP%]     .form-control.ng-touched.ng-dirty.ng-invalid{border-color:#ed2c2d}[_nghost-%COMP%]     .ng-touched.ng-dirty.ng-invalid small{color:#ed2c2d}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ValidateContactInfoComponent, [{
        type: Component,
        args: [{
                selector: 'id-validate-contact-info',
                templateUrl: './validate-contact-info.component.html',
                styleUrls: ['./validate-contact-info.component.css']
            }]
    }], function () { return [{ type: i1.FormBuilder }, { type: i0.ElementRef }]; }, { ui: [{
            type: Input
        }], dataEntry: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }] }); })();

class TooltipDirective {
    constructor(_el, _renderer) {
        this._el = _el;
        this._renderer = _renderer;
        this.offset = 6;
        this.maxCharacters = 200;
    }
    onMouseEnter() {
        if (!this.tooltip) {
            this.show();
        }
    }
    onMouseLeave() {
        if (this.tooltip) {
            this.hide();
        }
    }
    show() {
        this.create();
        this.setPosition();
        this._renderer.addClass(this.tooltip, 'show');
    }
    hide() {
        this._renderer.removeClass(this.tooltip, 'show');
        window.setTimeout(() => {
            this._renderer.removeChild(document.body, this.tooltip);
            this.tooltip = null;
        }, 0);
    }
    create() {
        this.tooltip = this._renderer.createElement('div');
        const inner = this._renderer.createElement('div');
        this.arrow = this._renderer.createElement('div');
        this._renderer.addClass(this.tooltip, 'tooltip');
        this._renderer.addClass(inner, 'tooltip-inner');
        this._renderer.addClass(this.arrow, 'arrow');
        this._renderer.appendChild(inner, this._renderer.createText((this.tooltipTex.length > this.maxCharacters) ? (this.tooltipTex.slice(0, this.maxCharacters)) + '...' : this.tooltipTex));
        this._renderer.appendChild(this.tooltip, this.arrow);
        this._renderer.appendChild(this.tooltip, inner);
        this._renderer.addClass(this.tooltip, `bs-tooltip-${this.placement}`);
        this._renderer.appendChild(document.body, this.tooltip);
    }
    setPosition() {
        const viwportSize = window.outerWidth;
        const hostPos = this._el.nativeElement.getBoundingClientRect();
        const tooltipPos = this.tooltip.getBoundingClientRect();
        // console.log(tooltipPos);
        const tooltipWidth = tooltipPos.width;
        // console.log(tooltipPos.width);
        const tooltipHeight = tooltipPos.height;
        const scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        let top, left;
        if (this.placement === 'top') {
            top = hostPos.top - tooltipPos.height - this.offset;
            left = (hostPos.left + (hostPos.width - tooltipPos.width) / 2) >= 0 ? hostPos.left + (hostPos.width - tooltipPos.width) / 2 : 15;
            let arrowPositionX = (tooltipWidth / 2) - 6;
            let tootipPositionX = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
            this._renderer.setStyle(this.arrow, 'left', `${tootipPositionX < 0 ? arrowPositionX - Math.abs(tootipPositionX) - 15 : arrowPositionX}px`);
            if (hostPos.top - tooltipPos.height - this.offset <= 0) {
                top = hostPos.bottom + this.offset;
                left = (hostPos.left + (hostPos.width - tooltipPos.width) / 2) >= 0 ? hostPos.left + (hostPos.width - tooltipPos.width) / 2 : 15;
                let arrowPositionX = (tooltipWidth / 2) - 6;
                let tootipPositionX = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-right');
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-left');
                this._renderer.addClass(this.tooltip, 'bs-tooltip-bottom');
                this._renderer.setStyle(this.arrow, 'top', 0);
                this._renderer.setStyle(this.arrow, 'left', `${tootipPositionX < 0 ? arrowPositionX - Math.abs(tootipPositionX) - 15 : arrowPositionX}px`);
            }
            if ((left * 100) / viwportSize > 70) {
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-right');
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-left');
                left = (hostPos.left + 20) - tooltipPos.width;
                this._renderer.setStyle(this.arrow, 'top', `${tooltipHeight - 6}px`);
                this._renderer.setStyle(this.arrow, 'left', `${tooltipWidth - 20}px`);
            }
        }
        if (this.placement === 'bottom') {
            top = hostPos.bottom + this.offset;
            left = (hostPos.left + (hostPos.width - tooltipPos.width) / 2) >= 0 ? hostPos.left + (hostPos.width - tooltipPos.width) / 2 : 15;
            let arrowPositionX = (tooltipWidth / 2) - 6;
            let tootipPositionX = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
            this._renderer.setStyle(this.arrow, 'left', `${tootipPositionX < 0 ? arrowPositionX - Math.abs(tootipPositionX) - 15 : arrowPositionX}px`);
        }
        if (this.placement === 'left') {
            top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
            left = hostPos.left - tooltipPos.width - this.offset <= 0 ? hostPos.right + this.offset : hostPos.left - tooltipPos.width - this.offset;
            if (hostPos.left - tooltipPos.width - this.offset <= 0) {
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-left');
                this._renderer.addClass(this.tooltip, 'bs-tooltip-right');
            }
            this._renderer.setStyle(this.arrow, 'top', `${(tooltipHeight / 2) - 6}px`);
        }
        if (this.placement === 'right') {
            top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
            left = hostPos.right + this.offset;
            if (viwportSize <= 768 && viwportSize > 480 && hostPos.x > (viwportSize / 3)) {
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-right');
                this._renderer.addClass(this.tooltip, 'bs-tooltip-left');
                left = hostPos.left - tooltipPos.width - this.offset;
                this._renderer.setStyle(this.arrow, 'top', `${(tooltipHeight / 2) - 6}px`);
            }
            else {
                this._renderer.setStyle(this.arrow, 'top', `${(tooltipHeight / 2) - 6}px`);
            }
            if (viwportSize <= 480 && hostPos.x > (viwportSize / 3) || (left * 100) / viwportSize > 90) {
                top = hostPos.bottom + this.offset;
                left = (hostPos.left + (hostPos.width - tooltipPos.width) / 2) >= 0 ? hostPos.left + (hostPos.width - tooltipPos.width) / 2 : 15;
                let arrowPositionX = (tooltipWidth / 2) - 6;
                let tootipPositionX = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-right');
                this._renderer.removeClass(this.tooltip, 'bs-tooltip-left');
                this._renderer.addClass(this.tooltip, 'bs-tooltip-bottom');
                this._renderer.setStyle(this.arrow, 'top', 0);
                this._renderer.setStyle(this.arrow, 'left', `${tootipPositionX < 0 ? arrowPositionX - Math.abs(tootipPositionX) - 15 : arrowPositionX}px`);
            }
        }
        this._renderer.setStyle(this.tooltip, 'top', `${top + scrollPos}px`);
        this._renderer.setStyle(this.tooltip, 'left', `${left}px`);
    }
    ngOnDestroy() {
        if (this.tooltip) {
            this.hide();
        }
    }
}
/** @nocollapse */ TooltipDirective.ɵfac = function TooltipDirective_Factory(t) { return new (t || TooltipDirective)(i0.ɵɵdirectiveInject(i0.ElementRef), i0.ɵɵdirectiveInject(i0.Renderer2)); };
/** @nocollapse */ TooltipDirective.ɵdir = /** @pureOrBreakMyCode */ i0.ɵɵdefineDirective({ type: TooltipDirective, selectors: [["", "id-tooltip", ""]], hostBindings: function TooltipDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("mouseenter", function TooltipDirective_mouseenter_HostBindingHandler() { return ctx.onMouseEnter(); })("mouseleave", function TooltipDirective_mouseleave_HostBindingHandler() { return ctx.onMouseLeave(); });
    } }, inputs: { tooltipTex: ["text", "tooltipTex"], placement: "placement" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(TooltipDirective, [{
        type: Directive,
        args: [{
                selector: '[id-tooltip]'
            }]
    }], function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }]; }, { tooltipTex: [{
            type: Input,
            args: ['text']
        }], placement: [{
            type: Input
        }], onMouseEnter: [{
            type: HostListener,
            args: ['mouseenter']
        }], onMouseLeave: [{
            type: HostListener,
            args: ['mouseleave']
        }] }); })();

const _c0$6 = ["c"];
function CarouselComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "div", 13);
} }
function CarouselComponent_div_1_div_5_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 25);
    i0.ɵɵelementStart(1, "i", 11);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function CarouselComponent_div_1_div_5_ng_container_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelement(1, "img", 26);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const beneficiary_r9 = ctx.$implicit;
    const ctx_r7 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("src", beneficiary_r9.avatar == null || beneficiary_r9.avatar == "" ? ctx_r7.defaultAvatar : beneficiary_r9.avatar, i0.ɵɵsanitizeUrl)("alt", beneficiary_r9.name);
} }
function CarouselComponent_div_1_div_5_div_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 27);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" +", ctx_r8.beneficiaries.length - ctx_r8.quantityAvatarOnFamilyCard, " ");
} }
const _c1$4 = function (a0) { return { "m-center": a0 }; };
function CarouselComponent_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r11 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 13);
    i0.ɵɵelementStart(1, "div", 14);
    i0.ɵɵelementStart(2, "div", 15);
    i0.ɵɵlistener("click", function CarouselComponent_div_1_div_5_Template_div_click_2_listener() { i0.ɵɵrestoreView(_r11); const ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.selectBeneficiary(); });
    i0.ɵɵtemplate(3, CarouselComponent_div_1_div_5_span_3_Template, 3, 0, "span", 16);
    i0.ɵɵelementStart(4, "div", 17);
    i0.ɵɵelementStart(5, "div", 18);
    i0.ɵɵelementStart(6, "div", 19);
    i0.ɵɵtemplate(7, CarouselComponent_div_1_div_5_ng_container_7_Template, 2, 2, "ng-container", 20);
    i0.ɵɵpipe(8, "slice");
    i0.ɵɵtemplate(9, CarouselComponent_div_1_div_5_div_9_Template, 2, 1, "div", 21);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 22);
    i0.ɵɵelementStart(11, "h6", 23);
    i0.ɵɵtext(12, "Grupo Familiar");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "small", 24);
    i0.ɵɵtext(14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r3.beneficiarySelected == null ? null : ctx_r3.beneficiarySelected.familyGroup);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(9, _c1$4, ctx_r3.beneficiaries.length > 2));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", i0.ɵɵpipeBind3(8, 5, ctx_r3.beneficiaries, 0, ctx_r3.quantityAvatarOnFamilyCard));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r3.beneficiaries.length > ctx_r3.quantityAvatarOnFamilyCard);
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate1("", ctx_r3.beneficiaries.length, " beneficiarios");
} }
function CarouselComponent_div_1_div_6_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 35);
    i0.ɵɵelementStart(1, "small");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.label);
} }
function CarouselComponent_div_1_div_6_span_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 35);
    i0.ɵɵelementStart(1, "small", 36);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.label);
} }
function CarouselComponent_div_1_div_6_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 25);
    i0.ɵɵelementStart(1, "i", 11);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function CarouselComponent_div_1_div_6_h6_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "h6", 37);
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "slice");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵpropertyInterpolate("text", beneficiary_r12.name);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(beneficiary_r12.name.length > 20 ? i0.ɵɵpipeBind3(2, 2, beneficiary_r12.name, 0, 20) + "..." : beneficiary_r12.name);
} }
function CarouselComponent_div_1_div_6_h6_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "h6", 23);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(beneficiary_r12.name);
} }
const _c2$4 = function (a0) { return { "deciduous": a0 }; };
function CarouselComponent_div_1_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r24 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 13);
    i0.ɵɵelementStart(1, "div", 14);
    i0.ɵɵelementStart(2, "div", 28);
    i0.ɵɵlistener("click", function CarouselComponent_div_1_div_6_Template_div_click_2_listener() { const restoredCtx = i0.ɵɵrestoreView(_r24); const beneficiary_r12 = restoredCtx.$implicit; const i_r13 = restoredCtx.index; const ctx_r23 = i0.ɵɵnextContext(2); return ctx_r23.selectBeneficiary(beneficiary_r12, i_r13); });
    i0.ɵɵtemplate(3, CarouselComponent_div_1_div_6_span_3_Template, 3, 1, "span", 29);
    i0.ɵɵtemplate(4, CarouselComponent_div_1_div_6_span_4_Template, 3, 1, "span", 29);
    i0.ɵɵtemplate(5, CarouselComponent_div_1_div_6_span_5_Template, 3, 0, "span", 16);
    i0.ɵɵelementStart(6, "div", 30);
    i0.ɵɵelementStart(7, "div", 31);
    i0.ɵɵelement(8, "img", 32);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "div", 22);
    i0.ɵɵtemplate(10, CarouselComponent_div_1_div_6_h6_10_Template, 3, 6, "h6", 33);
    i0.ɵɵtemplate(11, CarouselComponent_div_1_div_6_h6_11_Template, 2, 1, "h6", 34);
    i0.ɵɵelementStart(12, "small", 24);
    i0.ɵɵtext(13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const beneficiary_r12 = ctx.$implicit;
    const i_r13 = ctx.index;
    const ctx_r4 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(9, _c2$4, beneficiary_r12.deciduous));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", (beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.state) && (beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.acronym));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", (beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.state) && !(beneficiary_r12 == null ? null : beneficiary_r12.tag == null ? null : beneficiary_r12.tag.acronym));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", beneficiary_r12.rut === (ctx_r4.beneficiarySelected == null ? null : ctx_r4.beneficiarySelected.beneficiary == null ? null : ctx_r4.beneficiarySelected.beneficiary.rut) && i_r13 === (ctx_r4.beneficiarySelected == null ? null : ctx_r4.beneficiarySelected.index));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("src", beneficiary_r12.avatar == null || beneficiary_r12.avatar == "" ? ctx_r4.defaultAvatar : beneficiary_r12.avatar, i0.ɵɵsanitizeUrl)("alt", beneficiary_r12.name);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", beneficiary_r12.name.length > 20);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", beneficiary_r12.name.length <= 20);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(beneficiary_r12.rutcomplete);
} }
function CarouselComponent_div_1_ng_container_7_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "div", 13);
} }
function CarouselComponent_div_1_ng_container_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, CarouselComponent_div_1_ng_container_7_div_1_Template, 1, 0, "div", 7);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r5.ghostCards);
} }
const _c3$1 = function (a0, a1) { return { "next": a0, "prev": a1 }; };
function CarouselComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r28 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 2);
    i0.ɵɵelementStart(1, "div", 3, 4);
    i0.ɵɵlistener("indexChange", function CarouselComponent_div_1_Template_div_indexChange_1_listener() { i0.ɵɵrestoreView(_r28); const ctx_r27 = i0.ɵɵnextContext(); return ctx_r27.getSwiperState(); })("reachEnd", function CarouselComponent_div_1_Template_div_reachEnd_1_listener() { i0.ɵɵrestoreView(_r28); const ctx_r29 = i0.ɵɵnextContext(); return ctx_r29.swiperNextEnd(); })("reachBeginning", function CarouselComponent_div_1_Template_div_reachBeginning_1_listener() { i0.ɵɵrestoreView(_r28); const ctx_r30 = i0.ɵɵnextContext(); return ctx_r30.swiperPrevEnd(); });
    i0.ɵɵelementStart(3, "div", 5);
    i0.ɵɵtemplate(4, CarouselComponent_div_1_div_4_Template, 1, 0, "div", 6);
    i0.ɵɵtemplate(5, CarouselComponent_div_1_div_5_Template, 15, 11, "div", 6);
    i0.ɵɵtemplate(6, CarouselComponent_div_1_div_6_Template, 14, 11, "div", 7);
    i0.ɵɵtemplate(7, CarouselComponent_div_1_ng_container_7_Template, 2, 1, "ng-container", 8);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(8, "div", 9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "div", 10);
    i0.ɵɵelementStart(10, "i", 11);
    i0.ɵɵtext(11, " navigate_before ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(12, "div", 12);
    i0.ɵɵelementStart(13, "i", 11);
    i0.ɵɵtext(14, " navigate_next ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("swiper", ctx_r0.swiperConfig)("ngClass", i0.ɵɵpureFunction2(6, _c3$1, ctx_r0.swiperNext, ctx_r0.swiperPrev));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r0.beneficiaries.length >= 4 && !ctx_r0.mobile && ctx_r0.ui.familyGroup || ctx_r0.beneficiaries.length >= 5 && !ctx_r0.mobile);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r0.ui.familyGroup);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r0.beneficiaries);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r0.beneficiaries.length >= 4 && !ctx_r0.mobile && ctx_r0.ui.familyGroup || ctx_r0.beneficiaries.length >= 5 && !ctx_r0.mobile);
} }
class CarouselComponent {
    constructor(renderer, el) {
        this.renderer = renderer;
        this.el = el;
        // var
        this.ui = {
            familyGroup: false,
        };
        this.dataEntry = [];
        this.dataOutput = new EventEmitter();
        this.defaultAvatar = 'http://mxdev.cl/banmedica/img/avatars-vidatres/Avatar_v3_67.svg';
        this.quantityAvatarOnFamilyCard = 3;
        this.beneficiaries = [];
        this.swiperNext = true;
        this.swiperPrev = false;
        this.mobile = false;
        this.cardsmultiple = 5;
        this.ghostCards = [];
        this.swiperConfig = {
            direction: 'horizontal',
            observer: true,
            slidesPerView: 5,
            slidesPerGroup: 3,
            mousewheel: false,
            scrollbar: false,
            navigation: true,
            simulateTouch: true,
            watchOverflow: true,
            // centeredSlides: true,
            // spaceBetween: 0,
            speed: 1000,
            // initialSlide:1,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
            breakpoints: {
                560: {
                    slidesPerView: 'auto',
                    centeredSlides: true,
                    slidesPerGroup: 1,
                    spaceBetween: 0,
                },
                850: {
                    slidesPerView: 3,
                    slidesPerGroup: 1
                },
                1200: {
                    slidesPerView: 3,
                    slidesPerGroup: 1
                },
                1400: {
                    slidesPerView: 4,
                    slidesPerGroup: 2
                },
            }
        };
    }
    ngOnInit() {
        this.setCardsLength();
        if (window.screen.width <= 560) {
            this.mobile = true;
        }
        if (!this.dataEntry) {
            console.warn('No se ha encontrado la data de beneficiarios en la directiva dataEntry');
        }
        for (const beneficiary of this.dataEntry) {
            if (beneficiary.rut !== null) {
                beneficiary.rutcomplete = `${beneficiary.rut}-${beneficiary.dv}`;
                if (_rut.rutValidate(beneficiary.rutcomplete)) {
                    this.beneficiaries.push(beneficiary);
                }
                else {
                    console.warn(`El Rut ${beneficiary.rutcomplete} no es válido`);
                }
            }
            else {
                this.beneficiaries.push(beneficiary);
            }
        }
    }
    // methods
    selectBeneficiary(beneficiary, index) {
        if (beneficiary) {
            this.beneficiarySelected = { beneficiary, index, familyGroup: false };
            this.dataOutput.emit(this.beneficiarySelected);
        }
        else {
            this.beneficiarySelected = { familyGroup: true };
            this.dataOutput.emit(this.beneficiarySelected);
        }
    }
    getSwiperState() {
        this.swiperNext = true;
        this.swiperPrev = true;
    }
    swiperNextEnd() {
        setTimeout(() => {
            this.swiperNext = false;
            this.swiperPrev = true;
        }, 100);
    }
    swiperPrevEnd() {
        setTimeout(() => {
            this.swiperNext = true;
            this.swiperPrev = false;
        }, 100);
    }
    setCardsLength() {
        const currentCard = this.ui.familyGroup ? 2 : 1;
        const cardsBeneficiaries = this.dataEntry.length;
        const totalCards = currentCard + cardsBeneficiaries;
        const missingCards = totalCards >= 5 ? 2 - (totalCards % 3) : 0;
        console.log(missingCards);
        if (totalCards <= 5) {
            this.swiperConfig.slidesPerView = 4;
            this.swiperNext = false;
        }
        if (totalCards > 5) {
            this.swiperConfig.slidesPerView = 5;
        }
        for (let i = 0; i < missingCards; i++) {
            this.ghostCards.push({});
        }
    }
}
/** @nocollapse */ CarouselComponent.ɵfac = function CarouselComponent_Factory(t) { return new (t || CarouselComponent)(i0.ɵɵdirectiveInject(i0.Renderer2), i0.ɵɵdirectiveInject(i0.ElementRef)); };
/** @nocollapse */ CarouselComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: CarouselComponent, selectors: [["id-carousel"]], viewQuery: function CarouselComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0$6, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.swiper = _t.first);
    } }, inputs: { ui: "ui", dataEntry: "dataEntry" }, outputs: { dataOutput: "dataOutput" }, decls: 2, vars: 1, consts: [[1, "id-carrusel"], ["class", "swipe-content", 4, "ngIf"], [1, "swipe-content"], [1, "swiper-container", 3, "swiper", "ngClass", "indexChange", "reachEnd", "reachBeginning"], ["swiper", ""], [1, "swiper-wrapper", "pb-2"], ["class", "swiper-slide pt-4 pb-4", 4, "ngIf"], ["class", "swiper-slide pt-4 pb-4", 4, "ngFor", "ngForOf"], [4, "ngIf"], [1, "swiper-pagination"], [1, "swiper-button-prev"], [1, "material-icons"], [1, "swiper-button-next"], [1, "swiper-slide", "pt-4", "pb-4"], [1, "col"], [1, "card", "shadow-sm", "card-family-group", 3, "click"], ["class", "badge badge-success", 4, "ngIf"], [1, "avatar-group"], [1, "m-auto", "d-flex", "position-relative"], [1, "d-flex", 3, "ngClass"], [4, "ngFor", "ngForOf"], ["class", "card-img-group item-extra", 4, "ngIf"], [1, "card-body", "mt-3"], [1, "card-title"], [1, "card-subtitle"], [1, "badge", "badge-success"], [1, "card-img-top", "card-img-group", 3, "src", "alt"], [1, "card-img-group", "item-extra"], [1, "card", "shadow-sm", 3, "ngClass", "click"], ["class", "tag", 4, "ngIf"], [1, "avatar-box"], [1, "avatar-card"], [1, "card-img-top", "avatar-card", 3, "src", "alt"], ["class", "card-title", "id-tooltip", "", "placement", "top", 3, "text", 4, "ngIf"], ["class", "card-title", 4, "ngIf"], [1, "tag"], [1, "capital"], ["id-tooltip", "", "placement", "top", 1, "card-title", 3, "text"]], template: function CarouselComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, CarouselComponent_div_1_Template, 15, 9, "div", 1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.dataEntry);
    } }, directives: [i1$1.NgIf, i2.SwiperDirective, i1$1.NgClass, i1$1.NgForOf, TooltipDirective], pipes: [i1$1.SlicePipe], styles: [".swiper-container[_ngcontent-%COMP%]{list-style:none;margin-left:auto;margin-right:auto;overflow:hidden;padding:0;position:relative;z-index:1}.swiper-container-no-flexbox[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{float:left}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{flex-direction:column}.swiper-wrapper[_ngcontent-%COMP%]{box-sizing:content-box;display:flex;height:100%;position:relative;transition-property:transform;width:100%;z-index:1}.swiper-container-android[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%], .swiper-wrapper[_ngcontent-%COMP%]{transform:translateZ(0)}.swiper-container-multirow[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{flex-wrap:wrap}.swiper-container-free-mode[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{margin:0 auto;transition-timing-function:ease-out}.swiper-slide[_ngcontent-%COMP%]{flex-shrink:0;height:100%;position:relative;transition-property:transform;width:100%}.swiper-slide-invisible-blank[_ngcontent-%COMP%]{visibility:hidden}.swiper-container-autoheight[_ngcontent-%COMP%], .swiper-container-autoheight[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{height:auto}.swiper-container-autoheight[_ngcontent-%COMP%]   .swiper-wrapper[_ngcontent-%COMP%]{align-items:flex-start;transition-property:transform,height}.swiper-container-3d[_ngcontent-%COMP%]{perspective:1200px}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-cube-shadow[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-wrapper[_ngcontent-%COMP%]{transform-style:preserve-3d}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%], .swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%]{height:100%;left:0;pointer-events:none;position:absolute;top:0;width:100%;z-index:10}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%]{background-image:linear-gradient(270deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%]{background-image:linear-gradient(90deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%]{background-image:linear-gradient(0deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%]{background-image:linear-gradient(180deg,rgba(0,0,0,.5),transparent)}.swiper-container-wp8-horizontal[_ngcontent-%COMP%], .swiper-container-wp8-horizontal[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{touch-action:pan-y}.swiper-container-wp8-vertical[_ngcontent-%COMP%], .swiper-container-wp8-vertical[_ngcontent-%COMP%] > .swiper-wrapper[_ngcontent-%COMP%]{touch-action:pan-x}.swiper-button-next[_ngcontent-%COMP%], .swiper-button-prev[_ngcontent-%COMP%]{background-position:50%;background-repeat:no-repeat;background-size:27px 44px;cursor:pointer;height:44px;margin-top:-22px;position:absolute;top:50%;width:27px;z-index:10}.swiper-button-next.swiper-button-disabled[_ngcontent-%COMP%], .swiper-button-prev.swiper-button-disabled[_ngcontent-%COMP%]{cursor:auto;opacity:.35;pointer-events:none}.swiper-button-prev[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E\");left:10px;right:auto}.swiper-button-next[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E\");left:auto;right:10px}.swiper-button-prev.swiper-button-white[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next.swiper-button-white[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E\")}.swiper-button-next.swiper-button-white[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev.swiper-button-white[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E\")}.swiper-button-prev.swiper-button-black[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next.swiper-button-black[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E\")}.swiper-button-next.swiper-button-black[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev.swiper-button-black[_ngcontent-%COMP%]{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E\")}.swiper-button-lock[_ngcontent-%COMP%]{display:none}.swiper-pagination[_ngcontent-%COMP%]{position:absolute;text-align:center;transform:translateZ(0);transition:opacity .3s;z-index:10}.swiper-pagination.swiper-pagination-hidden[_ngcontent-%COMP%]{opacity:0}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-bullets[_ngcontent-%COMP%], .swiper-pagination-custom[_ngcontent-%COMP%], .swiper-pagination-fraction[_ngcontent-%COMP%]{bottom:10px;left:0;width:100%}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]{font-size:0;overflow:hidden}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{position:relative;transform:scale(.33)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active[_ngcontent-%COMP%], .swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-main[_ngcontent-%COMP%]{transform:scale(1)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-prev[_ngcontent-%COMP%]{transform:scale(.66)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-prev-prev[_ngcontent-%COMP%]{transform:scale(.33)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-next[_ngcontent-%COMP%]{transform:scale(.66)}.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet-active-next-next[_ngcontent-%COMP%]{transform:scale(.33)}.swiper-pagination-bullet[_ngcontent-%COMP%]{background:#000;border-radius:100%;display:inline-block;height:8px;opacity:.2;width:8px}button.swiper-pagination-bullet[_ngcontent-%COMP%]{-moz-appearance:none;-webkit-appearance:none;appearance:none;border:none;box-shadow:none;margin:0;padding:0}.swiper-pagination-clickable[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{cursor:pointer}.swiper-pagination-bullet-active[_ngcontent-%COMP%]{background:#007aff;opacity:1}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-bullets[_ngcontent-%COMP%]{right:10px;top:50%;transform:translate3d(0,-50%,0)}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-bullets[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{display:block;margin:6px 0}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]{top:50%;transform:translateY(-50%);width:8px}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{display:inline-block;transition:top .2s,-webkit-transform .2s;transition:transform .2s,top .2s;transition:transform .2s,top .2s,-webkit-transform .2s}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-bullets[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{margin:0 4px}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]{left:50%;transform:translateX(-50%);white-space:nowrap}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{transition:left .2s,-webkit-transform .2s;transition:transform .2s,left .2s;transition:transform .2s,left .2s,-webkit-transform .2s}.swiper-container-horizontal.swiper-container-rtl[_ngcontent-%COMP%] > .swiper-pagination-bullets-dynamic[_ngcontent-%COMP%]   .swiper-pagination-bullet[_ngcontent-%COMP%]{transition:right .2s,-webkit-transform .2s;transition:transform .2s,right .2s;transition:transform .2s,right .2s,-webkit-transform .2s}.swiper-pagination-progressbar[_ngcontent-%COMP%]{background:rgba(0,0,0,.25);position:absolute}.swiper-pagination-progressbar[_ngcontent-%COMP%]   .swiper-pagination-progressbar-fill[_ngcontent-%COMP%]{background:#007aff;height:100%;left:0;position:absolute;top:0;transform:scale(0);transform-origin:left top;width:100%}.swiper-container-rtl[_ngcontent-%COMP%]   .swiper-pagination-progressbar[_ngcontent-%COMP%]   .swiper-pagination-progressbar-fill[_ngcontent-%COMP%]{transform-origin:right top}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-progressbar[_ngcontent-%COMP%], .swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-progressbar.swiper-pagination-progressbar-opposite[_ngcontent-%COMP%]{height:4px;left:0;top:0;width:100%}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination-progressbar.swiper-pagination-progressbar-opposite[_ngcontent-%COMP%], .swiper-container-vertical[_ngcontent-%COMP%] > .swiper-pagination-progressbar[_ngcontent-%COMP%]{height:100%;left:0;top:0;width:4px}.swiper-pagination-white[_ngcontent-%COMP%]   .swiper-pagination-bullet-active[_ngcontent-%COMP%]{background:#fff}.swiper-pagination-progressbar.swiper-pagination-white[_ngcontent-%COMP%]{background:hsla(0,0%,100%,.25)}.swiper-pagination-progressbar.swiper-pagination-white[_ngcontent-%COMP%]   .swiper-pagination-progressbar-fill[_ngcontent-%COMP%]{background:#fff}.swiper-pagination-black[_ngcontent-%COMP%]   .swiper-pagination-bullet-active[_ngcontent-%COMP%]{background:#000}.swiper-pagination-progressbar.swiper-pagination-black[_ngcontent-%COMP%]{background:rgba(0,0,0,.25)}.swiper-pagination-progressbar.swiper-pagination-black[_ngcontent-%COMP%]   .swiper-pagination-progressbar-fill[_ngcontent-%COMP%]{background:#000}.swiper-pagination-lock[_ngcontent-%COMP%]{display:none}.swiper-scrollbar[_ngcontent-%COMP%]{-ms-touch-action:none;background:rgba(0,0,0,.1);border-radius:10px;position:relative}.swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-scrollbar[_ngcontent-%COMP%]{bottom:3px;height:5px;left:1%;position:absolute;width:98%;z-index:50}.swiper-container-vertical[_ngcontent-%COMP%] > .swiper-scrollbar[_ngcontent-%COMP%]{height:98%;position:absolute;right:3px;top:1%;width:5px;z-index:50}.swiper-scrollbar-drag[_ngcontent-%COMP%]{background:rgba(0,0,0,.5);border-radius:10px;height:100%;left:0;position:relative;top:0;width:100%}.swiper-scrollbar-cursor-drag[_ngcontent-%COMP%]{cursor:move}.swiper-scrollbar-lock[_ngcontent-%COMP%]{display:none}.swiper-zoom-container[_ngcontent-%COMP%]{align-items:center;display:flex;height:100%;justify-content:center;text-align:center;width:100%}.swiper-zoom-container[_ngcontent-%COMP%] > canvas[_ngcontent-%COMP%], .swiper-zoom-container[_ngcontent-%COMP%] > img[_ngcontent-%COMP%], .swiper-zoom-container[_ngcontent-%COMP%] > svg[_ngcontent-%COMP%]{-o-object-fit:contain;max-height:100%;max-width:100%;object-fit:contain}.swiper-slide-zoomed[_ngcontent-%COMP%]{cursor:move}.swiper-lazy-preloader[_ngcontent-%COMP%]{-webkit-animation:swiper-preloader-spin 1s steps(12) infinite;animation:swiper-preloader-spin 1s steps(12) infinite;height:42px;left:50%;margin-left:-21px;margin-top:-21px;position:absolute;top:50%;transform-origin:50%;width:42px;z-index:10}.swiper-lazy-preloader[_ngcontent-%COMP%]:after{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%236c6c6c'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E\");background-position:50%;background-repeat:no-repeat;background-size:100%;content:\"\";display:block;height:100%;width:100%}.swiper-lazy-preloader-white[_ngcontent-%COMP%]:after{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%23fff'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E\")}@-webkit-keyframes swiper-preloader-spin{to{transform:rotate(1turn)}}@keyframes swiper-preloader-spin{to{transform:rotate(1turn)}}.swiper-container[_ngcontent-%COMP%]   .swiper-notification[_ngcontent-%COMP%]{left:0;opacity:0;pointer-events:none;position:absolute;top:0;z-index:-1000}.swiper-container-fade.swiper-container-free-mode[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{transition-timing-function:ease-out}.swiper-container-fade[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{pointer-events:none;transition-property:opacity}.swiper-container-fade[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{pointer-events:none}.swiper-container-fade[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%], .swiper-container-fade[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]{pointer-events:auto}.swiper-container-cube[_ngcontent-%COMP%]{overflow:visible}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{-webkit-backface-visibility:hidden;backface-visibility:hidden;height:100%;pointer-events:none;transform-origin:0 0;visibility:hidden;width:100%;z-index:1}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{pointer-events:none}.swiper-container-cube.swiper-container-rtl[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{transform-origin:100% 0}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]{pointer-events:auto}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-next[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-next[_ngcontent-%COMP%] + .swiper-slide[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-prev[_ngcontent-%COMP%]{pointer-events:auto;visibility:visible}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%], .swiper-container-cube[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%]{-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:0}.swiper-container-cube[_ngcontent-%COMP%]   .swiper-cube-shadow[_ngcontent-%COMP%]{background:#000;bottom:0;filter:blur(50px);height:100%;left:0;opacity:.6;position:absolute;width:100%;z-index:0}.swiper-container-flip[_ngcontent-%COMP%]{overflow:visible}.swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{-webkit-backface-visibility:hidden;backface-visibility:hidden;pointer-events:none;z-index:1}.swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]{pointer-events:none}.swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%], .swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]   .swiper-slide-active[_ngcontent-%COMP%]{pointer-events:auto}.swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-shadow-bottom[_ngcontent-%COMP%], .swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-shadow-left[_ngcontent-%COMP%], .swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-shadow-right[_ngcontent-%COMP%], .swiper-container-flip[_ngcontent-%COMP%]   .swiper-slide-shadow-top[_ngcontent-%COMP%]{-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:0}.swiper-container-coverflow[_ngcontent-%COMP%]   .swiper-wrapper[_ngcontent-%COMP%]{-ms-perspective:1200px}.swipe-content[_ngcontent-%COMP%]{position:relative}.swiper-slide[_ngcontent-%COMP%]{height:auto!important}.swiper-slide[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%], .swiper-slide[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]{height:100%}.swiper-button-next[_ngcontent-%COMP%], .swiper-button-prev[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev[_ngcontent-%COMP%]{background-image:none}.swiper-button-next[_ngcontent-%COMP%], .swiper-button-prev[_ngcontent-%COMP%]{-moz-user-select:none;-ms-user-select:none;-webkit-user-select:none;height:auto;user-select:none;width:auto}.swiper-button-next[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next[_ngcontent-%COMP%]{right:0}.swiper-button-next.swiper-button-disabled[_ngcontent-%COMP%]{right:-25px}.swiper-button-next[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-next[_ngcontent-%COMP%]:focus{outline:0}.swiper-button-prev[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev[_ngcontent-%COMP%]{left:0}.swiper-button-prev[_ngcontent-%COMP%], .swiper-container-rtl[_ngcontent-%COMP%]   .swiper-button-prev[_ngcontent-%COMP%]:focus{outline:0}.swiper-button-next[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%], .swiper-button-prev[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%]{font-size:35px}@media screen and (min-width:960px){.id-carrusel[_ngcontent-%COMP%]   .swiper-container-horizontal[_ngcontent-%COMP%] > .swiper-pagination.swiper-pagination-bullets.swiper-pagination-lock[_ngcontent-%COMP%]{display:none!important}}.id-carrusel[_ngcontent-%COMP%]   .card.deciduous[_ngcontent-%COMP%]   .card-subtitle[_ngcontent-%COMP%], .id-carrusel[_ngcontent-%COMP%]   .card.deciduous[_ngcontent-%COMP%]   .card-title[_ngcontent-%COMP%]{color:#767677!important}.id-carrusel[_ngcontent-%COMP%]   .card.deciduous[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%]{background-color:#757576!important}.id-carrusel[_ngcontent-%COMP%]   .card.deciduous[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%]:before{border-right:10px solid #757576!important}.id-carrusel[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%]{text-transform:inherit!important}.swiper-button-prev[_ngcontent-%COMP%]{transition:left .3s ease-in-out}.swiper-button-next[_ngcontent-%COMP%]{transition:right .3s ease-in-out}.swiper-container[_ngcontent-%COMP%]   .swiper-slide[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]{margin-left:auto;margin-right:auto;max-width:200px}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(CarouselComponent, [{
        type: Component,
        args: [{
                selector: 'id-carousel',
                templateUrl: './carousel.component.html',
                styleUrls: ['./carousel.component.css']
            }]
    }], function () { return [{ type: i0.Renderer2 }, { type: i0.ElementRef }]; }, { ui: [{
            type: Input
        }], dataEntry: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }], swiper: [{
            type: ViewChild,
            args: ['c', { static: false }]
        }] }); })();

function ProgressBarComponent_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 4);
    i0.ɵɵelementStart(1, "i", 5);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
class ProgressBarComponent {
    constructor() {
        this.progress = 0;
        this.status = new EventEmitter();
    }
    ngOnInit() {
    }
    ngOnChanges(changes) {
        let uploadProgress = changes.progress.currentValue;
        if (uploadProgress === 100) {
            this.status.emit(true);
        }
    }
}
/** @nocollapse */ ProgressBarComponent.ɵfac = function ProgressBarComponent_Factory(t) { return new (t || ProgressBarComponent)(); };
/** @nocollapse */ ProgressBarComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: ProgressBarComponent, selectors: [["id-progress-bar"]], inputs: { progress: "progress" }, outputs: { status: "status" }, features: [i0.ɵɵNgOnChangesFeature], decls: 5, vars: 4, consts: [[1, "progress-container"], ["class", "progress-status d-flex text-decoration-none", 4, "ngIf"], [1, "progress"], ["role", "progressbar", 1, "progress-bar"], [1, "progress-status", "d-flex", "text-decoration-none"], [1, "material-icons", "md-18"]], template: function ProgressBarComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, ProgressBarComponent_span_1_Template, 3, 0, "span", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelementStart(3, "div", 3);
        i0.ɵɵtext(4);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.progress == 100);
        i0.ɵɵadvance(2);
        i0.ɵɵstyleProp("width", ctx.progress + "%");
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate1("", ctx.progress, "%");
    } }, directives: [i1$1.NgIf], styles: [".progress[_ngcontent-%COMP%], .progress-container[_ngcontent-%COMP%]{position:relative}.progress-bar[_ngcontent-%COMP%]{height:100%;left:0;position:absolute;top:0;transition:all .3s;width:0;z-index:1}.progress-status[_ngcontent-%COMP%]{position:absolute;right:0;top:-20px}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ProgressBarComponent, [{
        type: Component,
        args: [{
                selector: 'id-progress-bar',
                templateUrl: './progress-bar.component.html',
                styleUrls: ['./progress-bar.component.css']
            }]
    }], function () { return []; }, { progress: [{
            type: Input
        }], status: [{
            type: Output
        }] }); })();

class DndDirective {
    constructor() {
        this.fileDropped = new EventEmitter();
    }
    onDragOver(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = true;
    }
    onDragLeave(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = false;
    }
    // Drop listener
    ondrop(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = false;
        let files = evt.dataTransfer.files;
        if (files.length > 0) {
            this.fileDropped.emit(files);
        }
    }
}
/** @nocollapse */ DndDirective.ɵfac = function DndDirective_Factory(t) { return new (t || DndDirective)(); };
/** @nocollapse */ DndDirective.ɵdir = /** @pureOrBreakMyCode */ i0.ɵɵdefineDirective({ type: DndDirective, selectors: [["", "id-dnd", ""]], hostVars: 2, hostBindings: function DndDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("dragover", function DndDirective_dragover_HostBindingHandler($event) { return ctx.onDragOver($event); })("dragleave", function DndDirective_dragleave_HostBindingHandler($event) { return ctx.onDragLeave($event); })("drop", function DndDirective_drop_HostBindingHandler($event) { return ctx.ondrop($event); });
    } if (rf & 2) {
        i0.ɵɵclassProp("fileover", ctx.fileOver);
    } }, outputs: { fileDropped: "fileDropped" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DndDirective, [{
        type: Directive,
        args: [{
                selector: '[id-dnd]'
            }]
    }], null, { fileOver: [{
            type: HostBinding,
            args: ['class.fileover']
        }], fileDropped: [{
            type: Output
        }], onDragOver: [{
            type: HostListener,
            args: ['dragover', ['$event']]
        }], onDragLeave: [{
            type: HostListener,
            args: ['dragleave', ['$event']]
        }], ondrop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }] }); })();

const _c0$5 = ["fileDropRef"];
function LoadfilesComponent_ng_container_1_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 6);
    i0.ɵɵelementStart(1, "div", 7);
    i0.ɵɵelementStart(2, "div", 8);
    i0.ɵɵelementStart(3, "div", 9);
    i0.ɵɵelement(4, "img", 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "span");
    i0.ɵɵtext(6, "Se produjo un error al subir el archivo");
    i0.ɵɵelement(7, "br");
    i0.ɵɵelementStart(8, "small");
    i0.ɵɵtext(9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 8);
    i0.ɵɵelementStart(11, "button", 11);
    i0.ɵɵlistener("click", function LoadfilesComponent_ng_container_1_div_1_div_1_Template_button_click_11_listener() { i0.ɵɵrestoreView(_r12); const i_r5 = i0.ɵɵnextContext().index; const ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.triggerUploader(i_r5); });
    i0.ɵɵelementStart(12, "span", 12);
    i0.ɵɵtext(13, "Reintentar");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(9);
    i0.ɵɵtextInterpolate(file_r4 == null ? null : file_r4.name);
} }
function LoadfilesComponent_ng_container_1_div_1_div_2_ng_container_6_small_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext(3).$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(file_r4.message);
} }
function LoadfilesComponent_ng_container_1_div_1_div_2_ng_container_6_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext(3).$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(file_r4.message);
} }
function LoadfilesComponent_ng_container_1_div_1_div_2_ng_container_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtext(1);
    i0.ɵɵelement(2, "br");
    i0.ɵɵtemplate(3, LoadfilesComponent_ng_container_1_div_1_div_2_ng_container_6_small_3_Template, 2, 1, "small", 13);
    i0.ɵɵtemplate(4, LoadfilesComponent_ng_container_1_div_1_div_2_ng_container_6_ng_template_4_Template, 2, 1, "ng-template", null, 14, i0.ɵɵtemplateRefExtractor);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const _r17 = i0.ɵɵreference(5);
    const file_r4 = i0.ɵɵnextContext(2).$implicit;
    const ctx_r14 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", file_r4.error, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r14.ui.filesExtend)("ngIfElse", _r17);
} }
function LoadfilesComponent_ng_container_1_div_1_div_2_ng_container_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtext(1, " El archivo esta da\u00F1ado ");
    i0.ɵɵelement(2, "br");
    i0.ɵɵelementStart(3, "small");
    i0.ɵɵtext(4, " no se puede acceder a su tama\u00F1o y/o contenido.");
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} }
function LoadfilesComponent_ng_container_1_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r24 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 6);
    i0.ɵɵelementStart(1, "div", 7);
    i0.ɵɵelementStart(2, "div", 8);
    i0.ɵɵelementStart(3, "div", 9);
    i0.ɵɵelement(4, "img", 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "span");
    i0.ɵɵtemplate(6, LoadfilesComponent_ng_container_1_div_1_div_2_ng_container_6_Template, 6, 3, "ng-container", 1);
    i0.ɵɵtemplate(7, LoadfilesComponent_ng_container_1_div_1_div_2_ng_container_7_Template, 5, 0, "ng-container", 1);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(8, "div", 8);
    i0.ɵɵelementStart(9, "button", 11);
    i0.ɵɵlistener("click", function LoadfilesComponent_ng_container_1_div_1_div_2_Template_button_click_9_listener() { i0.ɵɵrestoreView(_r24); const i_r5 = i0.ɵɵnextContext().index; const ctx_r22 = i0.ɵɵnextContext(2); return ctx_r22.triggerUploader(i_r5); });
    i0.ɵɵelementStart(10, "span", 12);
    i0.ɵɵtext(11, "Reintentar");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(6);
    i0.ɵɵproperty("ngIf", file_r4.size != 0);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", file_r4.size == 0);
} }
function LoadfilesComponent_ng_container_1_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r28 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 6);
    i0.ɵɵelementStart(1, "div", 7);
    i0.ɵɵelementStart(2, "div", 8);
    i0.ɵɵelementStart(3, "div", 9);
    i0.ɵɵelement(4, "img", 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "span");
    i0.ɵɵtext(6, "Ya existe un archivo con el nombre");
    i0.ɵɵelement(7, "br");
    i0.ɵɵelementStart(8, "small");
    i0.ɵɵtext(9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 8);
    i0.ɵɵelementStart(11, "button", 11);
    i0.ɵɵlistener("click", function LoadfilesComponent_ng_container_1_div_1_div_3_Template_button_click_11_listener() { i0.ɵɵrestoreView(_r28); const i_r5 = i0.ɵɵnextContext().index; const ctx_r26 = i0.ɵɵnextContext(2); return ctx_r26.triggerUploader(i_r5); });
    i0.ɵɵelementStart(12, "span", 12);
    i0.ɵɵtext(13, "Reintentar");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(9);
    i0.ɵɵtextInterpolate(file_r4 == null ? null : file_r4.name);
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_img_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "img", 24);
} if (rf & 2) {
    i0.ɵɵproperty("src", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAdIAAAFfCAYAAAD6Tk0wAAAAAXNSR0IArs4c6QAAQABJREFUeAHsvQdwlGeaLvp0kjoodUtq5ZwzIAQi52CDMc5hxvaEPTsz99aprT17t+rW1t09s3O2Tp2tjZM8tnECTDAZEwyIJKJACIEklHPOLbVa3epWh/t8LQQyxl4DAguPfspWq/Wn7/2//3vz80hc3PAD3xwOB5qbm+Hv7w8fH58HHy1F5HA4IZFKIZVKHvz4KXjE6OgoWlpaEBQUBI1G89U75HhdkEDywxjqV8f2Lb/ZbDa0trYiJCQEKpXqq3tOlkzE2/YDlqvFYkFHRwfCw8Ph4eFxV4bfMm73EsTJ9iBi+ZbT3b3mFPk0PDyM7u5ut0wUCsXdu/qWQQiZSCbjBeR5puKLbDQa0d/fj4iICMhksrsy4advnA/fIq+vnOAJ/jI4OOh+tnKXy46Bnj44FBr4+npBIXFioLcbdrkaPr4+8JA+wbt6QpdyOq0ccw+MwzZAqoCXnx98vDXwkN1vsC7YzF24dKoYgekzkBgbgruvggtOByc8leukTPonNP5vuozLMQrTYD96egywuuTw8w+Ev58GUrsJPb0DsNickHmoodX6wUujhFTigt0yhO7efoyMAp4ab+i0vlB6yAGey2wahGEY0Pn7Qe3J757CTchkaKAX3T0DGJV4QBsQCJ2vGhKbGPcArHYXZaKBTucHjcoTUpogoxYj96cMHRJ4ennDnzLxlDlhNvajg9875RoEhwTBS+nB/X/Ym902jL7uXgxz7ig8vcbmglJBpUk5WS3o51rTP2iBnH8LCAqEr1qBUfMA59sgrE4JPJQ8RufL+ePBY5ywDou/8Vlwfiq9feDv6w0PhQxOuxVDxkEMWeXQ6/2+4V3+vmUtTAUxbhNl0gcz3xn3+O68H1xrRszo6+nGgHEECpUPAoMD4K2UwTZs4LiHYHNJ4amiTDinVEKOLidGTOJvg7BLFFBRJjrKRC6xY8jQg66+IcjUvggJ1kNNOU3lzWGzYKCvB71cNCSeGgQEBnA+yGHlfOjl2O2QwVMt1hgfKD05dqcD5iGOvc8Ip9QDajpJ/n5ecHBeDZtGoNbqQNFRpsMwDg7B6emLQD++uw9isX1HgcmdoyOoPLMf50yheG7dCiR4NmPz5l3Qz3wGzy2dAwUflNPpoicmu3MDrvt+J8wFejJuA0h6Z9/veB9PaLcxCdpMbdj3yWbU9AzDYTHDwvHNf+EneGbRbOjUYrIJBemESyKFTMrJPdyNc6fOItMvBDHRwZykQnG6+IB7cfV8GfTJyYiNpIJ9DA/oCQkGzlELqguPY/vBMxhwqaGW9POl9cb6N/8CcYoGfLj5CMwOOQaHbPD0ScBr77yO3BR/GBou4o+fHMaI0wmL3RNJi1/CGytmwtJciPe2HEKbPQt/9zcbkRzh+6SGMmnXcXDBK798FNsPX4CZyk/p6IFTEYiNP/4Zgq3l+GDrcThlnug3WOEVmIo3fvoasmO80VV9Fr/bfIx/k1BmKmQufxHPpHvh2Od7UdTeB6MFSF7yEn71/AIEeXtO2v1OvRM50N92C5/+aQd6XAp0G0cRnbUSv3xrBfwc3Ti7fwvyrrdA6eUPa68J3vHz8M5PlsNWfxrvbT9LJaNGd58V+qhs/OinryA9WIKmkuN4d8dpKmUFhl2+mPvMG3hhbjSabpzCH7fnwyNkGf7ur9Yg2Fc59cThXh/s6Gq4ic2bdqNf6kmZOJA0Zy1+8eZiqIZbcXLfFpwt7YLaSwtztxmBGYvx9o8XwHDrGDbtugyVWoPOXisik+bhR2+/gKSAUVRfO4RNey5CqaJMpAFYsv4lzPY3Ye+OA6gcGOb6psG8Z9/Az1ZnwctjairTkcFOXDqxF/vPV8KTTo1ruAO+4dl45dUNMFacwJaD16Bm5KyzZxTxmYvx5o83IN7HhLLL+7D5i6tQqz1gUQRjxQuvI8vDiLKCcqS//ioCnd3IP7gDh0qMyFn3E7y9NB4e8slfqOUymQL6MD9Ubb+I8OgQ2EcvoKzLhvU+/vCwD6KmuhrtfTYERScgJjwQ8tFBNDc2oK17CF4B4YiLCYPKZURTWx9MtAhtCi1iYyIR4DNVJzLDtNZhtPWrkDJjJuLCNKgtOo68vfvgo9Nj1axImDpqUdvQAZc6BCnJDD1QsVqGzWhvqMY1hYWWjx4J0UHorbxGo+NTxC3diBeffw6xAfTop+Y8/ZZFRUwqBwzNRfj08/1whObgmfk58JebMMiXMMjHg4ucAUbvZCyYmQytdABX8g5iy04vaP9yHXxGBmDyTcWirChIem9h54mTSI72gydDpC6nARXtvRi00PR+qjbKhJGa7oYr+GTXF/CMX4z1c7PgIzHCOGSFP63kEcMAhv3SsXJOMpTWLhScPIJPdvhC9xcrILEOwuyfidWzImBquYkDR/OQGLYBSTkrEOeyoO7GaRw4eRqr56Yh0DvoB+qVjs2rYb43PWY9F/dsDHaU49Sp47ieHQG/rgs4eLELs1Y8i6z4UNgMlJlLA5V0FAaLCTb9LKydn4zR/jpcOn0KH+/S4W/fno1hGjejQXPw7JwgNJVfx/Ejx5EUuh79TS00fgdQ1tzH6IhzCs82B4aGzDDYQrBqYzbaqotx6fwxFM8Mgbw2D0euDmDeM88hLVIPCz1vmwe9a9DbtozAGToHqxfEw9RWgQvnjuHjvf74f95Ihck6AlfYPDyT449bxYU4eugkon+yGrMWP4tZGKSxfwpnj5/F2txkJAaop5hsOE8cNFiv0Yg/fgWJizZifko4JHRerBIvqKVWtFlskEXMx7MLYtHfcAPnLxzG5oMB+OsXozFkG4U8ahHWZ/vgasFVfHn0FHznx6C7s4vzgHIe7EVTczcsVNQtjHw8rpkhBy3qyBmLsfziZRSd3IWz7T1IX/06ZsVrUbh3K/IbzPDylaH1VAnWbFyDGFTieFEjrFYzGmp3Y/6GH2FBpA2ffLADt7qkmLNoLV4PCp6aivT2FHK6JFB5RSAtaw5yMkMxIyUEvf/wn2ispgL1aMLhg+ch8/KFrCsP5TmrsTTbG1JLN47sPIyO3HD09A9h0bOvImiA+9eUYlgXhznzFyNc9xQqUnrXLtsIWsoL0CxLxH9b9yIWJfvTcOCLSzkpFE70DHogKDYLObm5iAmQIFpnxd/+rgD1HXORoVZBH52JrJmp8Oyx4fMzV2BzeiBr9kKGZWyo22MHTzTFXt5vvx0RpndywW4qu4oOdRb+asMLmB3lDbNZyIShNbkdbT1KhMQnYe68OQjxtSHM14K/23QdzT2zEMG0SHBMAmZkJ2BEO4S9F0sAZSAWrpgFuXMYelsd8koGGBp/sLzgt9/1VPyreO4yrh+hiImNYaSjA3Z66cOGbrRfLkFQzgvYsH45/GU2mG0OpkhkDMUxjcAwb0RSMuYtmAlf5UxoPU34571X0WZIgaenD8ISEjFrdhj86G0cK6HBxpBd9sLlUNDb2nSeHv6Unm7i5uTw9otwy0Q22IB8WQ9M3W3ouFaJyIVvYMMzC+HtGmHo1wkpHR0V+t1h3qiUDMxfmEHlkgGVbAjvnixA57pYSFV+CE9Mw+ycAEiG2nCu1gBNQCRy0jIgHemEvbUcJYUeoOin3CbqTmx97aiqKIczeiXe2PAMglVMg1gSIZEr6Mx1oUmjQ0zaTCxclALFvCRIHEZsK7yC7tWhkKt1nCszkJPjg6GuJtwspKHFeeShVjJqqIAuOB4rV6+G5+XT6Bapu8c0N9yJK4VGj2UrF+HEv3yMKtlsvJWVDj9LPX53phSymJmYmeCPpoKjuHkrDhEL4zGXlo+ZGr7tfD7q61qR5KvgoipFytylWLc8G3rfewo1ptTjG5OkyG2OjnKR56b0DUFCmBIttn4UHS9GdY8CS9Lj4elqw5dnihGpT+Nk9UL2siVYuzwRlcc2oaSmBWsz4pCZOBcp8+cjOdKfC+yUGuh3uhnxbrmcduYzhxAYm41grRotVw/hwz0n0WWSYd3bf4FkTypb5grtLFACX2tdeDyClIXMSzlgtztRceEYPqw8DvNAK4LS1iI5KhzBegVG2nwgo1cqwv1P1UahODk3LGYTghKWIMhLjrqL+/DR3rMYdKrx/Ns/QyTHdFcmCgQImShK4WDOVMyrsrOH8X6VC4bedkTN2IA45rrkfIsH2qtwLr8CKXPWITLQ94EKbJ4qGbpvVsp0hw09JUfwT3/3BUZoOAQvfBtJgRoUce6kZMZDZuliNOgDHCrsgFfkXPzlz1a67S6n3Tb2fipVCIyIR4C0FXbmTPlQ+I7uwe8rHOjo7EXy3Fco2yDoVazpuKWG1MHnMqUnnBQeEgtar+3Hr6v2Ypj+ZsyKnyFWq2D4W4KUjAS4BppxdPcmHL3Rh8DExfhvb8/nO8p0E98/u/C2VRoEhcfBH0VumViHjLhydCesZTY0dxqROe8NhLLeRcqsYkt1CYpudCB70QrovaZmGmGUeeFRjl0oRG+HAZe/2I3PviyCX2Q6XnplLVOLTC/aRzkfXDSWvDn2GPhdreHopLAM9OPS0W0YvmFBY+cIZix/E8FqRpM4B1x847y8VIiM0EN5nYVvj8sd5VwfW/olcj6wXOSGf4nIyDREhOnh7L2Blv5WeOtCmfz1RWxWGkIidDB11SP/3HVallLmeuzwEwsKrU5VUBLmzZnrDtM8HS+8yPmOlXrYGYrrYe5vlHnRPoYEHM5ADLDoRqeLpMcVCj8NE9tKLWbNy0b2rAR4Vmlxk96Xws8fgboQhEfEQM9Q9lOoR90GmvDAZHIZhnpYFDJiR3jcTKxYZseJz7ahtaMbURHMkXOfsYplB6yDAzA7OVoJZcgXQM0Ch9DoUIQGL0RS2ixE+7MKmCFMrmnuisGnrhCLc1oUkElpLRu7elgo46LFPxsrlthxavcetHT1IcifdWrCo3RXcTswYhyXCesD+E/t44swpj1m5i5Fano2wv08MdxXi+M796FCloR31s5m2HxqLmyT9/5y8edb4Rs9A7kzA9FaV4IqFtHQqWdxiJOKsJ/J4kSkL1yBPudFfJlfjt6hBVAJuYr5xv8YLsEIKyMtVDju74SHy+LAiIRwZOUGISMrGwGiroHv7tMx35haYtFaQFw2lmXrUF1ZhrYBC18lD1BNooNzSxobgRlLVqHHcR7nim6h3zyb7+mYTNzvEoslzUyjWXge8bvw5L39dIhKokwWhCAraxa0zKz1NRTj8O4T6A+Zg1+uSIOX59TMO0m59tACxQDfNZssEvHZS7Gk04Rijr1tcDF8xfvoHifng8MKi3EIVuaXxXsmldK71+kQy3DwzEXhyM7OgrX2+h1jioe6P4viNvc5xJx6DNudokGpB8MprHgK5xOQs+pSplBRm/shIDgG8cmZWLzmGWQn6tFCL7RtUIbQxGQEsXpRVCm6/43yE63xqb8J8VO4DguGTCzB7mtG4akDuNzpi9CwSBoRXvDQ8EWNisfMOYvx7OocRAR6QWLpRVVlDVpaG1FfPwQNw3dKSm+E1Zt9AwYM08J+Gkb/tedDy03CZx2WkAl5cwGOnS5ALwsWsjIzEMWcsVx4ASwus9E7G2KFXFdDGU58fhKjAYxOBOnohbsQmjwDy1etw4vr1iIzRs88Mb0ytkEMscR9hMeZTMNuI+VpkY+YzzJWDYYnpMNZm49j+UUY8gzGjMw0hPsGQErjQRTcWYeNHCMrcatvIm/3WUjCUxDm7wNPVp1FUHmuXL0eLzy7GqmMVoyaWnFs+ybsLTZj7opFiA7wZEvVUzpnvjaJvukLVnU7aWTrErByw0t46fnl8G28gPoBQB8biqJjW3G5vBV+MVlITWOIUs2Qunu60VijbI1DfWgqLcSpgyyyictACI1VD4Z9YmbMw9o1z+H5tcuRECK8ejE/zZyfQxhhftXEvKx9yqYTnLw3ekr6ZKx56VW8+Mx8qDjHmi1Ket7+KDi8BYV1PQhImMH6jCiGy26vr4wajQiZGHtRV1SAs18WwydxBoI0cngqPZGQvQDPMre6YdVixOi9YGwvw+7NH+FclzeWrcoFgwAYnYIyEdEDpTYYkeHh6GIkLK+oiU5ZHGalpUCn9KahLpYfjp1rtXGwB9UFF3D+9C1oU2YwCsE0i0aN5JzFWPfsBqxfMR9h7DIQSnMsKkFDjpW9w8NDXIPYdWBhHn6Ehtw3TddH+F5uZahAIadG4NnlHkp6JmzuoKem0qfj+WU5OFpwEZsqr8LHLxEvvr4W+rgoWE4U4ejBVqBzCLNYli5jC4la4QHFVO+xdEuQ3he9adlAMbb87iR20XBQ+wQh96WXsJjFH47gflR9chxfbvkQZ1hinrb0BazN9II3ZVN97jP8S7EXTLZQvLI8C7HhGiRFSHD+i0+h1mnxzJw0eN/tjXmEx/IkD6VQ+PyCkhbirQ09OHhwH35zfi8rlu0YdujxZlgYNAoz6goO4H+f2w1fhnm1Aal4+/U1SGGR2kCtFBpWEnqx73I8B2OzDODygY+x81AeKkpH8PH7TbD+6r9jSWYcDZAnObaHvJYIDcpUiEhbirfW9eLQ4e34n6d20OkZxYg0HG+HBLOCtwtV53fhNxc+hxefeUBwFn7y2irEBXmj0yBzy0TD/LHbB6BX1ddShn3HzuFqM3febMCNvFCsffMvsSgtivmuh7zPp+AwGdcSYVgJLyw0KgVzEvLRSEd045IXmXvfga1//D/YL9o7bFao45ZBr/OBuWsUN05sxt+flzJnKkFI9Fz87JVlXCRZBc2coRfnm0bFter2+M2GNpze+Sds//IyKholeO+9Trz+F7/E3FgadXdchakiLK4/jOQo6JHbJQw7xqZgZsxVNAwosG4lZWLciU3//k/YRSvdbB2Ff9pqBPh5o6PWgquHP0B9vpwtiTJ65PPxs5cWQe89gg6uvd4aFuZQobqH6xhGM4uY9hy/iBYH84jvNqEgLgXrf/Rz5vp17DqYKrLgfdAglXj6Y8bCtdjYtgtnPvw3nN4hh5mtiT7hOVit98dAtxHn972LmtNypgrkiE5dip9unA+dahByhSdb8TR3x05FJqGX6uHpyXk3itaqG9j2h49YCV0GoxcLZI0v4K231iGEUcbJ3CT/a1ez69UFAUgMYXl6fS1GlDr2Lun5crtg6m1FRWU1FwYzNX8wUjOS4KcYZmK4Cl2DTnizby40Lp6VhxK0tI8gMCiAL8JUqwpjNOAOIEMAARm84aQX2VBZhabOPoZTZNAGMZyQEMPWF8XY38orUNfWzTADxxebioRQL/Q2tWFgqNMdAvbUxSAzNRZ+Sgm6G6tQ09SJgNh0xIYFsX9tMh/P4zvX/QAZRs39qLtVheauXo7dA75BUUiJj4LaNYiyihr0s4pXxokbygrueIZyRaRoxNiNtiEFggPYR3k7dORgK01bTRnz5gNuz0DKFz0yKQtRQdopuLDdlfHXARnY+mTqQ21ZJVpYuTwq94Q2OJoyiYRitA+3yms5J9gDSSMrLCYJcVHBzH+xt83Ygw6TJ8IC/dhTK4xUWsUDHSipaKR1zJA356OCrR0xqVmICPCFsGN/KNtXARnksAwNop25q9AoPTwlo+jraGTbhx7RQVSYvS0oqayFkT1/HipfhMdShhFa2NyyqqdnKb5npCw+eaxjwGkb6+kdUSNC73unjUH07TZWlqClj1WZNIIUai+G+rIQqtXcMe6+T/neBWSIYPGeBKaBAXT12tglEcSIzwh6O5oxyNaNGLqNg91NnCd1GGalqqdGi8i4JK4rPjCxIKesop5FOOJ7H0RRJlGh/qxBYE+8oQ/9Ni/WcrB/VFizNPj6u5tRWtXCvCIDxnY7nQUd4jnfgv3Y//19CuP2tb8OyMBq5u52VN+qZm2GGS56o6GMCiZGBsDMFOOtykaM0LBQeWsRnZDMaJgf8+HsQzYYMOjwZr2BNx26sZObmZYzsH/WL4JdKINdqCzjHGPdgsslg58+nNGPOBq/k7NQjwMySF7+12rX36wPRm6S9xQQ7+O5hbuK9CGRjR7PbX2vZ72fIv1eb2gKXPzrinQK3NRTdgtfVaQTkI2esnFM5u3eVaThVKST6wlN5n0+yXN9XZE+yatP3rXGFSnTocxtsipK/Cesua9tIsl7+8s71XDiO+4qovdPw+bORXFs7uovMdb7jfOegbiT+uI77jtxlOPfTzzH/b6753RT7lchCzGG+8nkaRzPowpYjHlaJo8mxf9Khvc/O9cS9wIj8lr33+Np/vZemYi16L/axt8/9wr7ZyCTr8jjfvrmKztMnV8mPlu5ZdRBSLgetHsRdostIT/ETSgMM4sRxCQWFsREJfhDHO93GZNQGsJ76OrqclvJf+4yES+FmB9innR2dkIuZ7PKD3Fl/y6T4yH3ETIUYUQhw/b2djeG6p+7DIVMRPRnhEUubW1t7krwhxTvD+YwIRMR/bFarW5sa/H707hNHAeLjVyszvWCr59KVJBP2iYu8p1fIiHIh1m0vuE48VgmmgRigRSTWYxTrX4SOdx772BMrPeXyf33dZvp3yqTBzvu3r3Fgidk4u3tDaVyCqJQTdpM/O4nEvIYl4knixWmtweXgFgcxdwS5BDTYcwx+QklKtYgP7bt3AvQ/uAS/mEcIYwtoR+ETESb2dO6iXGI+S5XsoTLj827Xt5ekzgWETIU1VOip27iNha+cQeLJ/xBhJVFpfA3GSbu0DPPdC/zyn2PEzjAbE+YuK/wvoQnKhSpUByPe3PfLwfj7oO7c7Exmdw7ab5t36/L787JxsLxX7sG6ww41nuPExNW6OSJMhEPX8hEyOPJGBd3732qfhJKVORuhBKYNi4e7ikJpWEymUiA4TutSG+LUBgUYsEV80pEOqY3sRZJ3R6pmCf3rolPk3yEcyTmu/z/eykUsUEP65EIJoNB1DcYoNPrCU6ggYMVsS2s/m1q5yIdEoMENqV7seagt72ZTATE4mWjmJyQXlFRofBhI3Vvcy2rXrsg8w5BUkIkfL0I7TQuSSrF4b4WVNW2wCzxRlxiHIK07OlkVVpPcx1qxXE+4rgoHseG5mFWWFbXosckQVh8IqvYBAvEXc/4O3vI49d/wJ8C6cbQ2YTqxnbYiZGZwPsN9FGzEphYxLXsQe0yQxsei/ioELYLOdHf0YQa977aO/u67CNob6pGU2s/lP5RSIwLp5wmFCiwp8rIaseq2lbiGvMaSWPXcLFRubuxBnU8zlMbhiRWlnoL5gQTe69YGWmwKlj9mIjwQHoKbFMal8X4zwcc6gPuzn5C6xC6ugfhQQxnnbcS9hH+3t4F06gM/kEhLPEnGhYrGHtIN2UmKATNAVYaEvDCj/fLtcdGRJtBVjuS0gFab/VXKl1Fn5mFlXod3QbYpUoEhQZRZoKdhpEJPpOBXjL92AlzqNdCyX4IG6s829s62MYkhX9IOAKJxCV/4jJ5QBE+JbuPz6fxn4962y4HK3X7CSlI9g8Z+7vDgwOhYp+7i2uAabAP7R0GUqgQkjE8FBqpYLLqQi8xVZW+gQgRfc58/62caz3tHTCS7MlPHww92UOkLrL6kGmkq9dIsBUfBBOExkswitxzww6SehjIjmWRqBHCSmGBFiSO6+Rx0onHsTLbPDSAjvZuIjgRcCE4zA2TKopox2Ux/vOeS/xZ/joui/Gfj0cIbOEzGsguNArfAD27TOT3PF8X+9wH0dtvJBFHAAK4rogKe4voWWXFuKdfAPxIBCAYdsysPu8kPeCIRAN9cAh0xCAXfvT4/cve++3//rW7RP9BR8KTj5i7ceXYx/hoTw2xWBMRFaxExald2H70GroH+nD53AlYVIEI9Vfg9OZPcSj/Opq6OgmC7yD4gR7EDsOHW/ajlug5Ndcuo82mQVhkOBWv+xZhIWD1559uQVFtG9prr+FKKzFNQ0Mg6SjCpi0HUNfVTcaSAnQ4vIg0o8T1/Z8R9Lka3R3VBDZmozebfMMIJC8GK7wv4Xk9tpAdH8BASxHe/2grajsJXFB5BUWtQCghcNov78G2E6UEbujChfyTcNJoUFka8NEnW1Dd3o/eumsoaLKTBzMAhrJj+Gh7HuU3iMarl9Bs8yZyUjDUbpk4iSdZiR0fbUFpSzda2N9b2AmWwethbSpws6209vaivOASemQ6QjVKUfD5Fhy/UY/utkqcv9yFoJgIGiNqt0c7SO9LeOlf4Y180HnwHfZ3kkqr9NRn2LR1D0x+SUT5keLa3k/x8Y48nMk7g0s3+xHNfkpPWys+/t0HOHnxIuEoCzEgDUFceBBkI204tff3+Pizg7DoU9kOEOhuvRm7tAPG1jJs/9PHuHijHNfPXUBBO5AQHw5fvjhDXWX4+D8+JED6ENJnsuydoPtndr6PzXvOo/T6OVws6kFoQhz0VOROht+ERyq89Omw5Hd4sPfZRUQ6hIUuvK9HDmPSsGotP8++0C04VXQTp88XwKig4RzmjZ7aq/jgva04VVzJud0EZUAILC1X8N6mT3HuYj7OFdwAAhMQ7m3HrVOb8cHmnbh07jiuXK+CPikdbIDGjg8349y168g/cxm1I37IiA+B8iu9SKPoqi3AH/5lMy7WyDB7djgJLcqwddNmXCi6jrNniTdt80NWgp5MLZXY88GH2H/iBpVsG0wewYgN9mMv41iOVFTuTopM7iPzp/ErkQIQNRqP0yO19DUhb+fvseN0CfxiZyBCp3Ib1+Pyco30ofDk5/jXbadh8SHca4QPDC3X8Pmmf8bBC9XQxmcjUqckml8Njm7/CDsPn8QNrhlljVbEpCSy/VHm9qrFs33oOIOdGJnHt27Glm3bcF2+Cs8J2jFab41XbhILMp0g0rPQdOwDeiHd5G1U4GYdFUXqTMyfFw8NaZP8NS6UHM6D0SMEK1YuhrL5DLZeq0KiQExKC6RhYENbST6u9XoSB3glkrxa8MHOYpTFkIi66gxMnmFYuXIBFE1n8NmVMugk7Thf1IfExUuxIFmFY3/YharyVPKHBsHvoUc5LvL/+qfwioSlqghKx9ysFKg6zuKPZ+qQk0o2mSslUGjnY868BFQd/ZA9t23wbClFuzwC65euQKSsDp9uvYxKsqZYrxVAHT4Ly1fMhr36DD6+UIHUZGKNJgVQvhY03zyLYqMfKe+WIcRVQ0zcqyiP0cFRdBo2v3jKMhvWKgJNnL8B7Sgh6W4MkT5oNbJCHfjyj4Snq8xELBFUNPea3v/1EB9yD4aQ28tx4NANFrXRA+UL5JCwxzJ5Dl6OJCNI9y1s234G1yuy4RXnQB/HlrOQPblx5P0MioFazqgEAb1b6Ywaq+phMJCXcGICnL2F/cSvPV9hxfpXF8Db2oBNZyvQvioHISorSvMPo0TwO7pYTDdiQnd/GQ5c6iW12Uok+5mQt/UELt+cjagQ9sE+MZk8pCj/zA4bHaJRWJSPWnk83lyThPaKApzOu4jMUCdazx1BtzoOaxdkIsJfzWgDEY5U0Vi+/nVIzVwLTnyBQydKkB21gChKs7HhzUwMt5Tj+L6TOFfcjNdyAzF7+Tr2mZpwg4r31JV6vLEqiyD5dxcLS18HDbOjuMVIiK8naQWJK+3lG8Tj1jOqM4Ti8+dw9moT+gnL13TtHC50+mDdurmkVNTCJzDwTp/rn9ljmxrDdQyhpvQ6jh4rhzw6DkMjRBET68add9xBR6QE5w8ew5A8Cn39XJdGjDD09xKmkdGKjjoMMjLG+BWaqq7hemkDckm1qTXcxMVrF3CpkihKcyLupCPvzpoHGj7zn2zYj0jLxatvkZfzkgd5nB1kKiAZdKgf2m8242axk/Q1FiSn0wqg59rWX4VrJxmyrAtA2qIN8PVJYcjEhYwZWZg5Ix2aCCtO0OocpTVLU9LtMQ2z0Tg6LROZM2ciQROFnLwqgQCA9i4eN5vHZWVAGWbDiatXiIbSBFd0GnEmZyItQQPTvHOoVJBcmuEc7UOO8kFEIiE6UEDsHLwZLoOZNEfn2awv85nDIi4vyLlIt1TW4yaROKpbrciYSZmQSkkbHIfomFhEKlzQWvejp5OE6gYrwlKi+H0M7A42Eh8oIdrJEIunAtwg8WbCEcZlzSPG6AyESIKRfaqWcHxD6O2RYtbymZhBmTj9LThZWMiG90FICa02IzMLGSESDOWeQx1pqgSrmYaN4U9isxHmreh0HlxczFald0HOELyURNiJc4nLS67Xnlobdn1xknNH4PkyVGLrZNN5PMP/DLVoxzxDjT4RS1fSY6xuoafIu56oSIkSo2SDur+sl/CNDfCx1iI6bg4NNQl66otxhPNwwZpVaKdHIafSHSatUp8mBbkLFyIrkLKoLUQeGY+Mw3ZMapnAkxDuD/wajlGywjDMpglfiJkz6VF49ONC4WW0toXgVmEtAuZmQM5ox4hTR55O1j/o0rAymhWhvTVoLz2ONnqXCjb2B6cvQhTnVk+VBqUnrpMIXEmktlCuO96kRrzB1EsfAvQaKr4JTfqk9qojTVt+rTeeeX4GKfUEapDAtA3FrCw12huLx44L8oJ9qB+NZdcB/7k8B6Exzb4ETmeKSkzo6e17kUBfQwWKr5ciYtE6JHA9lIk2zwl3Yu1vRPHNmxiMWIqNKVr0CyQdmZrRqblYvdqCy3lnaWSJIwTE4AhGJTpGtHJA4Ds0NYg03SDrUSLunPEhVQxhrjy0ZBlYQBxaK72eOoJp0CMFCw0cjDkTmLrf4EHW9l4EEGHCQgqqOUuXEOSdMWcnGVVOfenO30kJDRVAxnLBmiLxUBGGj9BZ48PlGCQEJg4mYoWSiDkOEgP7krxVFMxIlMQA5nFMlRBeiqwPRKkQoDqCTV5FmCwm2AjZR1QTnng8hn1nxI/rA4ul1H5koSfSSHn1AKnWGGe3kR6JOWQjeV17GDLsMzjRzXzLEDktfUNoBR04gcPkC9SPlqKorgKLFr+GWKKd5J06Qb7LFlhu5vNFF4+ShUq371tGtKWQAG8uBjLKXAE/ykTOxL1UpXXnGQUkms0tE7JMsJAsMCSQ6DrUPi47VP5efG5kVxWyfVxymHheB+nZKq7iQIkca5/PQmDfOdSTxcHOYjCKi+D3nShgaNcVMRepBOoO8BsmLVkKek0tOLzpMkZil+FXb60idZs/c2MmEvvKiUR1zyYhZFxANJL0vdi1axNsDhnW/F9robD1khHjS/gmL0NWqg5dNfWwjYxyrkmJChPBuUbAb57Ny18F6wBZbG4XvN1z9ulfv0cJCMzr4IhYNH6+C//YfAGjDdXooJKy0XBs6DDBs6MTza5esn/k4da6t/HaslTmSYm8VnoZF+tGsOQdht80XA842e2mTty4ko8GeRx+nhoOD6cFlQXHsPXgcVwl4lTqi8/RmBt/K1zob6lA4ZkLCJ+7EanhDs4fGrkMhQiy96orx/DZgeMorGpG5usvsk6EMH11HTB4tKONzkPRmWPIu7kBf/XWInLXTqhv+B5l+ed0aae5C4WFN3C9PwI/Xh8HAw3qEWKhi+5O99NwcY4UFzEKNoylL6+C3NBAtFtW7DO37eXNVKRAiPIQc4FFmqAOCg2Fl2Mv3vunv4IW3eTr9sT8DEFacVeqD6lIeQKuhHIZl3eGNAWQg4Tae9Tchxt1HgQwX4a1SzPQlajCllIWBKRE4pkNLzJJGwRPawd6b/6GeY1WKI3tUNDitJHNTEYS1kEWhtCBnLCNoJsFShYugBJyWw72j8BL8BaSo85A6iB+hOfoMI+zw8rFubu7HyZCaAlWkmGe106P9JsqgSdcZHI+Mkc6QmDkIYcCISlz8eI7alj+tIuQe9dRUa3BhudWYllONBqjlfi8rh9zU7Px8hpPNJPtXuEZzTynCZGhwciK2ginbzH6rYSRi45AVL+CfIzEM+ZdilZuJ/NGotDBSqG5XIQH6xtBgPg82on+IXLxiUcySlJugjWTtISh9X43ZqeYEsOmQTh87E9GifJebcODKP7iFMpK6GVHXIKz5QJaVQ4EBYbAO9KTedP9OFrkwPq3NiA5gkUkEl+SHb8Oq9mAW5eO4V/2M6qwYgaiAqLcBpGYt6JKbvyZCiPJaTOhueIyqpS5eOevkuE51IBTV/NwwSMSJy4xN5Lpj/N9duRfboVXqBq5ESbCh/HemMtzMs9vZZ4m0NfT7Y0IIPrpbepIQKr0Rfr8jfh/vRLRZ7KQl9KJtnIbn78U3gERmLt8OeZHSHHFts/NHLNmVggM7UXYf6gAoXPewPKMaOY8+T6QfP7G+S9w8FI5MbX/GmkRY2TZ4j19wycMydfO4oviPNxYkY2lySTYpjfaUleM41dLEKOOwpmSTpwtMiF6ViyeX5KEsNRcvCmOKzyLI9dJyh28jEQXAeQkXo5V84NRefEEPj9H1pYXc6YV6fcwnfqbGnH1YD4qnbEoKmhB7a1ieFCpJuqf4X8a8p8S+rbwCG7cGoI+XoWG0krUO3owLykYC9JYuyP0mfjHhYZxMoQlzcHrv/hrwsb2wthdhuHieqiEkzZhbA+vSG+fRHg3QpGKf6LiycqKSD+GeL28tEy4K+h9kI+w5hTqqkKQM4/MBCoylY/IEUUKsrBIF4rrahFTHw9Vy03S1zogV3nAxJVulFxLooCg/lojahMaSNXVihsGO+bTGw0LteNSbS3i4mPh0cTjaEl60cMzl5ezYpbhUo0K18taoJ5Hz9RtWUwY8WP66LRb0Xx9N/aVa7GaDAyhrEwdoILX0xO0jwyy8pSek0ZLPFp6Vcz/9rfTuh7WIoWk4t7mKhQQfFpD77K//hpGvMKQlhIKY0ULvJnj1dCqHerrJci1hJW8wag5U4P65GjKtgY3jcBqtS9Cgm0oJDtNIrn3rARqHqRn7hUYBBOpiKqaE+HvHKVM2hEYyhS2cFC/Mg0ej1AEq0zc/GX4y3iWhyvtrChmtS17VmUkty49Q87T3VcQs+RV5LBYw8nc6QDDwFaCxYswnZLg+CpGJBQE0BT5Z2EYDfQRVGNwiFRvNnhRrh0NBMOnZ9nBKucBZTJmzVkEndELl09ewahsFla+8lOCXANmFh140BgRTBEeSg2GmktR1ZgOFSv6CmnoRTyrdUdIXJyr09tUkgDngHcAZuYsYPV+HUxNlxE5IxtxoWHoClJg0CwMeCJSyZgmCVKhr64IX2zZgWa/GfiLxdlQSwgMz2rx8oJD+MO2o/QuX8airHAy/A3BQKPfYCfpRGoq00LFsJ8uI0iABYauZnT2WKAJJMj7a+/QyJWjo56V8MQVl8tYjGboIpmDF+kCU2HqvI4D5yshJ2B8cHQgbg5YGTGjycvOBKmCXs1T3B85lWbBg96Lh18wFj7/LGKdrLg1NzOsq2aIn7RrtkG0NrQw9ylFXM46vBRp5RpjQTvbAlTsG+dSwqjDiLvgcJBrjZFsQmYrK/3lGpIE5CAsdggXT5IonSmG9BjyC4/VxLpv75EVqQAx9yKDimCnV6gDsXxJGraf2IX8L/cwjOiD3HUvYw5v4MjnB/DumSPso3LBN2UZcnNmI8yhQc17+/DH/3mOrObeTOb+CHEhSlzbuQ290TmYlbkAcwK24sB7/wu7qagicl5GZkYq/GNGcPP9A/jjP5wluLo35r3wY8yaHQt5fzsO7v8TmSBs0EZk461kguxTOHTOHvsmmOy1wbHAkX34/T8egpTj9EtahvlzliBRNozPyRBzfA+9eJICLNq4CKkRZlRuO4AtR/vJBO+BuIUvIzk2HKMk4r1+8CCOkh+V/FNY/vI6kvQCBdu2wpS+FDMSFyK7aAs+++2v2eQ9SkX1I6SlJUNNa6v4T1/gX/O/hFOhxWIuAjNmBcPW3oRDW/8Th9kjGRibi+UJsW6GGh762De50gtpK1YjSUQMqLRCHb1oCs5FqMaCoydO4cq1crQTiLq14gyyljyHGaoWHDp9BW3dJhqF3li87hVkEMTc0MjKyfe3I7+QhWzdn6CtdwQ/WRGF4j37och9BsnxcxBx6QR+x/lA3YuA9OWYM2c5ogIpOL4Y3R3laDPUIXcGK351VqxOKsW+D/4Z+xhF0cbMw3Mp0W6ZkEt6eptKEiAoeVP5Gby3jVXshmGoQ5Lw4ptLkBIhx8i8ZLy/7Xc4R2XlHZZMAuhYDNQdQd6lQgzrevFuZyXiknLx4oaZuMF34prgtjQp0V5ZisSZS7Ai0YYv9pxCB3lkR2lUzl31OjKDZKi/fgpflOjwq58vwQvx2cyIDKOmUgejsx9zUsNg7byJLZ8eISnBIBthQEq8VzGfNR6Dnt0o2LQVf3+B4UOvUCzd+A7JPNjSNb09cQl4BYVh/sbn3aHcwYZSeDP3LcnOhi+r//NPHUVf9Kv48cL1SKGRbR9sZMpQCX97FhICgbKCL/C7j7aTmKUR3n96HwMvvopnk6Q4s3s7Ltd3YJR0ozmrX8dsFpSJCNm4VypheGz880MMmOHc4X7UtluYrGfPH/vxLIPdqKioRAfDjypdBFJTEkh6LUNnQyWq69tgdnmydDgDcWx/8WBOtaWynB5TJ1zqYGRkJjOMacGx//w9bDNWu3n0ZN3VRP5vohWoQHw6GURC/Fk0wurVigpUt7D3QxOCtIxkBGt5bbrsZWSm6TU5ya+ZhqRI0l0xaShQRZqbm+Hv/3hB6112Wjf1HGdDO6zsO4tJTkcMGRocpi6UlVexYnSYXmIUZRJPCiAX2qrHxuCiwkxOS0Moc59OWspVtyrQQmWi4r6Z6fTWR3tx5D/fhWzxS1jKilZHRxVukdnBShqmpIwshLNfVmo3o/FWGWra+yD3DUUGZRLgTTLp3iaUllezj5S5wYQMJDCEKnKnAnygpaUFQUH0eElD9Ng3WjMmtgWYmRf3UthJb1SDbhZR2Zi7oMWFgAj2CHuY2ZPcgn4TPU6/EBaqJZEmygMWsoGUVjbAJEL8DHIryQmqMVXiw22VWP+LN7E4WYs2zrkGhrGd5BGNiEtm24xokRFTnTyMDP+2tQ8jKETnZmPpb61HeU0jzJxT4Qlk9xEy4b7ToPWPPgsmFbSeUYeBnha2QtXCzHoAfVQiUuLYg00jddjQiXIyFfWZnfAP5/f0EEbYc17N3nKr1cacNxWsNhhxscEYbq9hsaOF6ZBRONhn7KePYN5disaaBtYu8D3z9afSZX7VWIdDH25Fb/or+DnDsqK9QSyVFrMRXT1MT7CQ0sZK4opbNWSBomJnVC2W8yecLXajTEfUMyLWRAYar4AwpKQmQMtIkpiB06D1X59XTwq03slK/cFBlvz7aNFy9SgdmmtY9PO/waoU/RgzEFNlgwOsZ3GyzZBteT3EASivbXenkpxSFQLDYxDjL0Ez9ZRgF/Jhz3si50oA1yWxibbKbnamPKIi/bqAHvUbB/N7taU1tD4j2VdJpfmoJ+TxT0qRTsKt3vcUdhYs1ZTWwScyFsFUmuL1ftTtiSvSR73hicezoKvx5kWUDOiRO1so28kp6JhWpBOF/HCfJ1WRPtwtPORRTvS11uIa22ASFy1AFAtORORuMrZpRfp1KT4pRXrnynYjqspKUNWjxNLFM+EjqlMnYRtXpHKhZKbUxh7DiJRUhoDpjvPeJuPuxBjdhSmszJxy4/0uwmfeMJoeq1xw6E3LhDlTKcPUM7BMoRmr6J6kOfzUz5PvMpce8z7jDDpClk/Xu+aCxj8MOStC2QalYrnH5Kw9QtzjMhE/ny6ZPL7JMi6TJyUPl8sD4WwFDI5XgsX/k/YcxschaWhoeITQ7uMRNOsHROHUpG1isALrUqAaPa2INY9LJgJT9mnE/xRFJu6SqUmcJ+KlFvNEpSJk4DQm6kO9fwLZSHilAkXskZGNHuoOHuWgsYrwR8p23efyQiYCg1jI5GnGlb3P0B76KxERE+hGIq009i4/9Km+84GPY80Q4xDPVq7T6b7zjTytO4oFUkzmaYD2u09QyENMAiEToTimN+ZSKQ8hFwFb9tigJH/gghaLo3jfBKvH02q0TvYjEoaFMOaFTKYNtDHpinC3MFi0Wu1TbVyIcYg1Qy7wHyd/Y+MyIQOlMoZnJ56cghtzIO72Ak788zd9/ib2F6cox2XP6MTGWNELSOIZyCaUnotJPECYL2H9CMXxuDf3fTHDMpFtRXhPTspEco9M7r/vf32H9z/u9jVYgu922G6f5n4yEQ9fyGTauLgra6FIxxlxptlf7srlQT4J63yIbQNiXfleFakIaU18CR5kEN9132+4hljjJq57QnlOs798VajCOxRGl5gnT7uX7mZ/+erwHvQ3wf4ygNp6A/z1QdD7a9i3ZWQzcy0ayf7iExKNBLZ0sHgUPW3sz+odcLO/KNgLKthf/LyU1C8EMjC0obFfiiACNmi9JhSOUCmaeptRWdPsZn+JT4pnda432V9s6CZrjJv9xTsUSYnR8GMV1aipF9XVrKgj+0t4XBJB9LVPmP3Fhr6O24wut5lZ9KSoExi5bfXVaGzrg0dAONs1ouCjlJD9pdFd4esgSlQCx6b31YwZBaxWNPe2otnogdAQPZlM7pZciZ5KY3cTqupaYeU1EnlcII+j4NHZWI26lj6yv/AaiYL9RQHrUDeq2F9qGFEgMiEJEcQk/T7YX0YJ7djVNQAPVtz6+6hgt5Apg2QFwzYZtPoQBGo1boBvYXCIfTu5r2Dw0HFfFwvQuoliMzBsY6VkEAuuBIvL3WIBIRPzgMDINLCNSkkmkGASH7jQS9LyYQJbuEM6NLiUXgG8jpIwhEPua5vZAqTVk6XDj9emNTYe0hv/+aBvw/T+fHpCuXAb//moMnFxXg/2dZHoYoz9JZJIXXICkXQRTnOESEPuxnlWfbuZgtg1IAA6ujva2CfqImRgMML0Pm5Gqu72ToiOMp+AYBJcEFCf7C9GQw86egaJ0evD9ywY3sqxKtvxexbMQSbjIAaJRKbl2qQmlJpQkA7Rb9rTji5W4QvWkIhwAs2wMsnMFq/2O+wvglWIPdM8YFwW4z/Hz//n/HNcFuM/H4ssuI6ajP0kCxmFX6BYR7/6fMVaMzI84Ab9UfqSvYxMXcL2EgxcdxijuP4qCWU6zDapjg52Y4DsLyGh8CeIi7uN9PZ8l/2a20MNgkpuZLgLl49+hI92VzOJS/YXNkVXnNqJbV8WkcqoH5fzyf7C3tIQsr+c+vRTHCaQekt3F0vRyf5C2iN/Ks0hIusf+vBjfFnjQHxSLHuvxsqKxSDNfbXYSYaUYvbvdAiGlBZRgh4CkP3lA7K/NHT3oobYm+32MfaXon1bcfRaDXo7a3HufIub/SU88MmxvwjmgPfIzFJHHMauqgIUNgNhZHTpI+7np7svs2SeWKEX8zFAZhavkUZ8svkz1HQOoI8gDBfryWxD5hudWkoWmVvY/8EnON3mgWQCL2jvKFIaFp0V2PbxVtxq7UEr2V+udrjG2F/Ya/n+1kNoJZhFZcFFdEt1CPKV4PLOzThR0kgauyrkX+xws78Ea58w+4t1GCUnt+KDrbth0iUjwk+Gov2f4vO8cyikPPKv9iAime0vZGBxEYLtZt4W7ruX/YApiNGr0XxlLz7adhzXCo8i/9x54jKnc4HUgl083AT7Syk+e/djsshU3GZ/YfuTzoErB7cRjjIfVwsv4eChU6h2BSMpgGAeuzdh74UbhAkj6PilFgQnxlNWpLtjSHKa/eWhVoM7B4lIx2Syv7TcOo8/kf3ldHEJ2V+uYFChh564yid2fIo8ssEUFORj35cX0KmKRbLOjitHtmHL/rOoI+VVi1GB6GAFqs7twSe7TqK48BAuXS1DUFIGJGR/+fS9D3Hg3AUcP1OAVocOGbFkfxEYm2IjLWF72WV8/K+/x7HCOsRlz0KAlyfnpwE3zu3Fn7buw636VrS2DCE8KQbOvmrs/WAT9ufdIF2kYH8hcxExtqfZX8bEee//nwz7SyPnye/G2F/iZ5LJ5R72F0sfrubtdLO/jAj2lyh/RjIF/ncxtvzHv+N8tQFJs2dCQhayo9s+xM4jp4ghfw6lDWR/SSVGweSwv3TimGB/2b4NxYo1eJ5AhkKTN10phVSTjtmLst3sL909PWT9UKCknsozbSYWkAFFo/GnZ6kmq0cZNn3wEXbtPIGAZ2Lc8HbjAnezv5Tm47pBTchBMnWQ/eX97cW4FUdKMTbwW9SRWLV6jP1lC18Of2kbzhcPIGXpMrK/qHHs9zvI/sJeUu7/pNhfTH19UIXOIJBEMpQdZ/GH07WoT9GhNa8Quphs5M5JRG/tDZhp1zSVnEOXRxSeIwtJBNlfPvmkAA1Z7D2zjeCDd9/DgYMXEPFSFhgNvrO5aAkL9pebQzpCDi5HKJGNNu26hvI49qoWnoFdl0RUpTH2lx0XbkJnJ/tLiRlzn1uDGSGC/WUPKqqyEBfJPky3Erpz6sf4gSHk9jIcPFyCfrLjWIhK5CRqQiTBNl5IcWCwsxyfbS0h3+QqpEf5wnh7X4Ohl6gi7Ad0yaELz8C6l5JhG2zGqc+2oYG9srNTY0GwIpZEWtFHI+FilR3Pvb54jP3ldDV6FyUjZ9ULSBymR9NTj73vfk4UE4LUD3fj8tVihK15C2m+dhzbcpJePOcXGXHu+v2PURzTp/7OEhDsLxVFxMf1TMaPV5H9pbwAJ45fwsz/ez0WrnsNM6m0u2puYN/Wk1DJ+G7UXMO+i62YvWo1MuNDoWYEwlutQkjcbLwQNJuIVjdwhgZVfReVX7Qe81Y8j1xi8xYT0u9k3lmsZ4+2L71T8WrYWHTW19kCIxfWJhryIrIhYM87mytx6shl6DNXY0FmLLTEA/aDETcL83GhR4sNG3Ld7C/e0+wv3/k5P5Ydyf5STfaXYyeq4BETxx70e9lf7MQwuInzh+jsKaKJDT/Cp0v7ydSGkhuXcLZ2mEA6hJklKEhj1XUU32rGwpd/Bl0/jyG95aWKBUTKiryTPXjItYMhFZkSUZkL8QYtuNGLijH2FznZX8L80HajCcXX7GhsHEZKGq0ASw/aBfsLB9VQ5Y/UhRtI5hyIQKL8zFm6gagyEpR4eDOvelekLmoQs6GfbR/pZDoh6oQmErOPV0BCkt6OHpD9JQNZmWlQhlpxrKAAI2zclsSkEtRhBlLi1RiadwYVHoSWYzjnSbG/BMbl4g1if5pab+E8e2EVPtlQya1oayE2roUveWMRNAFkMlkWAAkxcn31KYiMjEIEAQp8R3bT4CBIYmwkFq56kS+oB8oZanCIhO/tzV1VyubiuBnzkZ6RSfYXPbLzqhkmHUZvnxQzl88g8lManLph5F0pJEDFIOTxlBP3TQ9m83BuPtlfWFX5BNlfrMZeXDt9EpL4HKzKFATugv1FjWhCs2m62nG91gQTPXQlIdhsDM1fO0UmmIQ5WKUWTDGkMBBgDVHJPK6HHmQD+Vl9kcDiqDuRXQnhvbiY+cu6Od9qyP5SR/aXXJIwhyI+SABNjKLhcjuJYWcTHo68ozojggNUhBdsZpiY4b3QJP5Oz4Gr5wSbZVzk0z+/Rwk4BfuLeYjGaTTS0zMQIu3GmUtXiWbmg+wZMXxgJhR3c6GMnof5CT4YKLmIFqsXlshGSYdlhC/bWTxVXgiJT4WqtxOFNObb7H5YpFIz1BeKhcsiyZzQC0drCS42EgKQ4f1x+1JGVK6Y7EVYK1dicEexO4QMF3GuW5txpcmBNbkywnayxoBwdE7O8SZiaku0uW5aNsOQBgHhhKSbWLzxPcrxz+/STOvUl5P9pQxRS9YhXnd/9pfrN0tgjFyO51P80Ec4WwkBghrKrhE8pwOrnlsHH8KSyiSikt9KQHvCuabPQDBhautrKxlZNbKA7K5kH1KRCvYXP2Tm5iLc34wvr99mf+FEM5HppN9EtAijEv39jE/3Gcn+4ovc5cvhoQ2A1tmKQ2RHCCTL+IYlmViwIAg6UykRbYiZeTvePH57bvYXov2Msb/Iv8L+4i/YX5gmc7PGcFX1YETGP4jsL8JN4QBVzLkJ9hcmKMZP93h/EoOT5pcAAEAASURBVEhbxfyfih5SRRWZXoiYMmjphskWQdSMYThUicidGYqOm4W4cDME84laNFBwAofIk6m3ExawvgJ+VPpKv0gsWkRL2liCxg4hk6/etpv9RUsybo5ZsL8IRhyxAIyxvxCqkXKwUVF584NAMAogI46SWJJu9hfBiPNE2V8sZH+5goOlClJRZZIJiOwvo8QeFuG//nLs2noAR04VoF2zlHlvWojlRdzXE89uzIR/Xz73JRINc+i24Q6c3L0DeScYVunxwmoWbN1ZowT7i38kkoL6sHvPx7A6ZVj1i2fhQ0NEbJb+DoZvLsA7Yxl5WIPh6+GJ5IQwfLFjH87YGAJe9gvmskh1x32nEQLdIpsy//Pw8kMIEa+ad+7Cb5rOu9lf2szB7py3eC2MpM0rOl+EoDmvMV2gwrWeVlgMo+hsb0HrrYs4fvoWfvXfXyYvbyu++PQznDhzBa2yODd+s8w9gexory1lhKIZsxa9iRDm48c3Gfn6vANCmW7wYv5zTL26WJthNnagnehtnZ3tMLffxNEzeVi6aD69YRJpqPk30iRe5/qWd+M5/PU7S6ZB68cF+gR/Os3dKLx6A0UEqn+b7C/99ddhJc2niO65I/dkf6m8To7RChOWvbIaUkM9IR8d6CVmd+H1EpgjlmJ5tAr1RTcYJZVCHxYKH+fuMfYXCdlfupWMRkwi+4tMyjvjAjiR/aW4zhMrVq24zf6ixqclfWgj+8ua9RvJ/qJ3s790Fv2GObt20oxlIog8lKNcWCd6XndlPoIuFiiNs78MkP3F283+0gkDiwAEa8wd9hfaE2PsL1ZqVxnzNIMYVQimiLtne6yfRNHUkAFGwf6SSvYXbw3M725HS4OeOdFQLF+0FBuWp6DJdxC/LTLCIzMXLz/jjaaeEXhK4hETbkFYgC+Bk2kVW52UyejXDAtx/05hFbNAQsCdCfYXA2GrxthfOtBPyL1x9pcBVjQL9pfO7j6GpQSaqAumIeKDPlH2FyPZX86gnOwv4eEX4Wg5T/YXO4JIi5YdFoPVG19DYmYGju48g8uXi+FXloeKMhfCIy/CzoWzjeTvwQyR5TB3MXflRkQkZuLSsZ24wlD+glQy5vgrWfhhIh7rJVR6zsNP/0caPI0MZxedQwW5KgOS9ehpKsGZRhdW/2UKF0oZOSlv4WSVEi/+/H8gQmXEpbwiXC2diTD/TLAmbnqbQhKQeAr2lxfwdz7JbvaXRkInt5RKCB4vpeFlRlNtCS53q/HjnySSkNtKI1GN2FkZWLF6PuwNhdj9cR7KGldiSWIolmx8E7FpGW6s1fzLFZgbq4OzpxyHdpEgPCgHv1yZwbCuoF0kHSSZpMYq7vnW0BB3lzRxIRFcIBKCowQxurF8xVoawHU4/sUulNR1sYVMi9xlK7CG7C8V3iewM78CfcNzpxXp9zCf+hsbUfjFOdZExKCwoJkMXGR/6SP7S9CzZH9R32Z/+RI3K8j+cp0Ks6yKdHxt0HRrkM8ct0c6I1QNZIi5Xo/BuFlct7Px+q/+FvVtPRjsKoXlei3UdNKEMTe+PaRHOn74mMM3kf1llEwnwoBTqrzd3I8OVk711pzCoYpgN/tLLNlfmi0yxJNFmUVUDEqL2xmbsGNndWCgqxdOsqUoaRE2XGNla3wdlLfZXxYqtQgPc+JCbQ1i4mLg0VwMA18sb1YIj9y6hcqGGEQyL3K9tBWaBZIny/5SvBt7yvyYp1yCUA6O9KMIpRyiNHYM9Rrclag9PZ1uNoHhHuZPB32QkpTkZn+55B3jLgUXi7m7kk28wONPijI0UCG6GDpX+oeg5nQNapOi6EEJ9hcp1jC0GRrCQgtW58YTY9ZaVQwjgRa89cEwFxHLuDGBoMxj7C/6MDJq0FkTy8Pj3iQKNRIWr8Qvkllx6cmQSDs9aQJESxmSa+2Qk4kmigvgCAoVR9xsP7GLV+GXcywE8R8lZRE9Ae4r4yg7W4mHqiXGpa8CNZflKLFY2fQ/TKziBgzLPdHZUsfxJiFzVi60g564cGobjKZhMsYYUHL5DEaiZyElOgweEjLfGzrQ0KPGmoxZyNAa0Xz2NNp6DAx3O+m5P26JTJ//wSQgIWOPFhkz5sLEwsOBuvOIzsqhQaR2M/rcvFYAWeoKJIWS2NvDCF1IGOzVZI6i6yE4fA1qH0aqRlnF2w9fhoeTieVcfNGFGgv5ktsrcHr7+8hr8sePfkEieGJfj7IGYaiP6xM7CJLiOV8wjAGDAQaTiT+NGAnSwU+nR7RnFSwiD0WWFyeB8ENTyUNsDcKtXrM73OdgNbHMQ1TH360sf7BxT+/9KBLw0IVg8UvrkUD2Fww3kVCFawkZwaRkf2mpb8KgVY6E3OfwSgzZXyRmdClZ9U1c3fDYFGx8yZeUnZ4w1vZCyfVH8Fsr5GqExDCsG2Hk2tJKB037eNhfvEmOrGBpuJv9ZVk6th/bg7NH95C6Rov561+h9SfYX/bhvbNHSGkD6NJWICcrBV4MQ7o4GQWLvYbKT1iaomqziGG8/ti5mD1jAXL1W3Dw/X/CnlEronNfQ2ZmCvxjrbhJ1ph3/yfZX+R+WPjiGPuLwtCO/fvfc7O/BETPwdvJT5b9RRcSD/nRffjDbw5TGbigS12JeTkL4dRK8Nn+Pcg/MgKnyh8b3sjgy29CSf5ebPmyFxKFkvieryAxOtiNo+tgmFjBogeNSlA3SeAwD+Dq9s8wnLkMs5IWIqdoM7b/7tdMhDuQsOjHSGcFmVr/LNlfDuDfBPuLhw7LXv+Jm/1ltL0JB7f9FofpwQbFL8CqxBiyIdB+YZ70cW9yUpalLuVCx/sU3nq5vYfsL/MYhpPg6o5PcL6S5OWMXWv0M/D6olmYFeNPJct9WbIewn2bQxYgOlCDxjMHsC+/jF4Ji4/kgVj/zix4s8DjzM7dUMxfh5SEuWR/OYbf/v1pKmySmactchcP2UkQXlk3iKVrWOXrr2akwoHAiBTMj7qO7f/5awKYy+Hpn4JXs2LZjsS5J+g8prepIwE3+8tZvPfZcXQNmMknm4pX3lyEYEGCUdmKpnYr1ryRyUp3tqXIfFlJSYPp7Cb8+z9eIAuQGqnLX0dKoBylx7fhSGEDhtjvJPdLxmvzWHzCyv/9p66ixqTFrndbcTk2Eete3ABp81UcqwjCL34kR/eNw7z2YRSVdWLkPSX63vgZlsekITftMj76t3/ku8k6kdSl+OmC2bCHUElv2oZ/uOCCh084lr64kmQdbO+b3p64BLzY0jZvwwZ3KHegkewvTAWB7C8+I604k3eEuuU1vL3oWSQzyuAg+4sPdU/ZaBoWzk2CkhFSF9eJ+iItScD9kZOTDAeJU/Zt24JL9Z1wKH0wd+0byI6adPYXsh50WuAf6I8ATpwRI4tCqsi1yb4vlZbkt6LP0VuKrqYa1DS0wcIAWnRSGmLYDyZynNSkzJe1o9vigUCeQ+kw4Mvf/tHN/rJ05VzIe2tRXsU+UiepxlLTEUGrUE7E/taqStS0dpH9JRipaQQu9+O1eZ5yXtvN/hKXinhycz5Z9pcRdLCXs7axAzZaOFGJqYgm44jgwasmO0lrDz0r5vPSkmNYOu1kfoZjaOkWCV0kpqQg2N/LrUhdVCamPsrExopDvQ5ScyeO/PY9N/vLkgXpcHZWo6KGfaRkf0lkEUYoQ8JSh9nNiFPrZn8hI04aOUhZrm/ua0ZZZS2tMCki4tPIihLg7q0V4ANPnP2lrwMWT3J/0u3urmXhWUcveUMZKmMeLD4qiDypty342+O3eAbAh+G2oY5a1DS2Y4hj8A6MQHKcHobKfPzHh2V47pdvYFGSDu3V1QyTk/3FQ4PwmCTEhnGis1+0icdp6flq2WsrGhucZOjput2DbHGIimC2TcSGse+Uvq/NhtbWVpIlhEyjPT3k8jepoPW0bAbZT11aUe9mfwmMjOezCmaFLnPfLPBpJdGyPiqOC6EHw658tqNm9lJXobqZxqnKDwnE7A5US9DTUsPK7G56kZ70WumZxgWxOrObRlYrbEx7CINU5e3DBdeGK7v2YDDrNbyzLg128tjWtvaRSYb0zuTWDSL7THQgCxl7yBBSQy5cqMk0lEQmKy0c7IturKxEs5v9hb3tSXHwY1RK3Nc0aP3XJ9OTAq13svXOKNhfvP3YRkf2l8NFWMK0zgrB/iJuizUtxkHB/qJxG9vjmcCRISNZYUzwDWO7pakPDWR/aWVqUbC/xLMf399rrAZjHLR+SrK/1N2qgzo4nGTV7Ov5+jN44G9EteuToFF74Bv7jgcIkIvaW/XwiYhGUOA0+wvLlNFYWoAyox5zsxMROBHE4zvK9H67TSvS+0nlwb6bVEX6YJd+5L0NpFsrvNKAxIXzyGVL4JdHPuPYCaYV6dcF+aQU6Z0rk/2l+lYpqntVWLIwi8b8ZGiWuzRqU479xSUhSn9SMjEp6T9QAU7oiLkjkwf9IBSpyDsKqEDx+anbRAiJHuvjkImQx9MmE5dL5mZ/WcIcrJLl2pN1/+I8Yp48jTKZKnP66ZWhk4GhUOQsC4EXEdcEOMdkbRNl8rTD4U2WTMRaPP6uTdY5v+08LkY0w2LTERSnhJo6VDyTydjGxyGZiuwvkzHAiecQg33a2V8mjmcyPouJJLyHp5X9RZRjM5wyqSVTQibT7C+PNrtEa5OYV08n+wvT6JxX7mK/RxPDV44WMhEYxIIc4uljxPnKUCbtFxH9Ef89WUacscrrySyzFCmySWN/cWOZTljU3L/fFvmdSSkWvgmPYfz7++47YT9O7bstLBOu4V5I770Gf79zvgn7CkUqJrMAaBfA9ePX/splJuuXCeP8/9l7D6g2z3zd9wEEooMoEr13U1ywjXHvduJUO5lMMinTZ/aafe7e956z7l237b7PPWevPXsmM5MedzuO7cRJ3BuuYIPB9N6baEIIRBGSkO7zfkI2dpzsxAYyzvBlOQIhfeX/vd9b/uX5TT3OnfPice68P+Wz4vD296d+lu/y/ftP7q5N7N+RPjFlf1Pfv7O/KTYR9hADhxCNFg/41M/ff7Tp+l2cx9Tj2M/r7nt3r8t+zLt/s71z/z7snxOv/9n+pu7r/s+K38VDIewiKB2C/jL181OPM/fzgy0gbGinvwiqhxCt//Y2FP3EvRMk+73izqZMnOxt5d7n40GfvfPe5GnfPacH72Pq1dm/e/c7/Kv9OZtyPvbP2b9r/7x4X0wsRB8kSFtzA6ntORXubgE3EDaZ6VW6uAf2+2G/P7bXB9x/+72d/ODU79nvsf098fu00l9ER8993t2YQCTqQh2Z/j31fXEC4p84AftJiYFCkGIoA0Kdw6k7ubs74WaxfunvNtIJETN3i/P5FUGKESIGTswAtm+iEQ8wjX3W6C9fcb7iOh3uP98H2sQ2qEq2IsXmQVb5Kps88BgPsIkYMAT9xdOTZUjkJH4/NjHjeJC1vtnViYHUbpM5+ss3s9n9nxKD50zoFX+pj7n/wFN+/1at4Ft9+OEOIgZP0eGKifwcRu2uDcVK7nGnv4g+WkwIHrGO1AqjYQANjaS/kI6gYtapeXyQhdL1TE0fhFdQNBJiw1laQPpLRwvUFBIwsthZ0F+iI0PhxdqtLsq6NbX2wkpx+/gECpcL9R376EuRA31vG6obmLXLgoe4pDiE+FELkwojPcyOrW+l5Jx3CDODo6FgKqhxuA91NXUS/SU0LknKmJWzfERcrNjsr3dv5fT+JPSBNZ3NqGOmqFmQWXi+Kl8v0kZGScQh/YUZtS6ULUtmCYqgv/S2N6Ob2c0m2sTFzQ8x0SHwcDahpb6ZtWtjggUHD/9wRIUEsk7ONjEQpJPBnmbUNHTCKB0jXqLGYIIZw021aBD0F78wHiOK6j6kvwz1oLqa9JdxG/0lUsViY05Y7Lawv06vJe7fmyC6DKKLAv1yEl38ScUwj9pIGYL+4hcUKpEyTPo+2mOIpcVE0HE8dJK5wJ8ZtF6s83JgWxgdoKLMoImfp0IRi2HtQ6aV2Z3ibx3UjjQykzk4IhwKN2cYdPy8dlRSR3JwcqdCCQkfTLYb7if1o1dLUgzrw8KD4csaMzaTWbbJ/Tb6fvxub0/2129+VexLxlivqSUhiiVtQQFUm2IsS7wnZPm0I1b4qkIQEigy1A0k+3QRjDFGNTGlBIZwpYqXYZSwiM5uCr1Y4RMQLGXBW8YGJCqQmc+Y6FacWGrmE6CCh4y12X1q9A8a4eLphzDpGbvbHVoEAaS/d5IO44NQZm96u8pgNoywLrWD4icTVD5SIZSEKatpBN1dPRKNxpETfh4EHmznSqqvifm83Rb2129uk+/vJ+22sL/OyJWyXxge7IdGZ4Qva+p9JrOoeUckgo9W08P+ZhguXn68v0FwEe2qm5QgCuI4inGDw6Mns30DWTon6FJq9t9WuReCQkI43tj6H/v532053/ZKuOocG+7CjdO78dFlOV75+SvUMXVFdc5RHM5TE2Hkia5TX2DJtlewKTMCFw8eRL56hKUIgWyAiUw39yRG7Dp2HyuBp0oJx8GzyLmdjVe2b0JCkNBItdNf9qPLwqQSlsXkVCzFyxRrDxgqxXv7z8PBy4cp6mdRmbUNW1ckoPrz/bjaSfUjquecPFOO7a89g2UpTF+ejY0dvUR/2XkUVs9geBh6cbVsFV7alo2JxgvYf5I6sEoPtKtPY8Gml7B1fgDO7t6LEh1dPsH+8FGlQMEyFouxFu99uB8GJ94wlr6EzlvHGykKzsVAaiEth/SXDw5igBKNMkM/cqpW4fVtKyDvLsA7By9RsEGB8d4zqFm7A5sWheL2kT242U98lNyIk2cr8fJPnsHCWGr9zoZNJo9hYW1w2bl92He2FAt2/C2enK9C2ee7cbamm5g3PYzuC/CzXz5FZZGb+PBoAfRjTATQtWFY5oVf/D9/h2wi4cycEJz68E1crDfi5f/6vyErIZR1XmIjsUVdg4PvHUT54DhLhXj/0zfhZ88tweDtI9h9phFW6mi6BCThtdeeQ6SrBh+/fxDNgwZitEbhveBJ/PUP1yKU5VNz23dnAYH7u37yYxy6XIvodT/GXz9DnWq275un9uLoxTLIWFNtdPLFjtd+imBDLXZ+dJJ1oWMYl/lhyyu/wZYUL1RfPYLDOU0kM3ZgQh6Bl3/xG/gNVWDfvpMY5mTaRMnOQVkoXvjprxHYn4+PTubDw9uHE28rVj33U2zPjmcJlngyzMQ0lmHPBx+jmiUQw5yELtzyCn61PhZN+V9gD7/n6CyHmbWrz77xC6S49+HUvgNoGrLAyBKcDp0zMrf/Gn+9dR48JifA351l/3KPPMrypbMfvY0rHe547qd/g1XxdK1LHZ8RrdU3sZcSpY1csIxZfbDuuR/hmQRHXPriY9ygzKNlnIufDiMWb/ghXlvvi0uHj+B6rVCUAxJXbMOvfrkJyikKLk5/z+1hTG0e68KpPXvwwYeHkDegwuaNyxAbSDzWx4fR7RGP7DXL4aouhM4tFEHEeZ0/VQ1lUgbWrFlErdMYBPvJ0ZCbgwGfRKzesAaJPgPErPUjIjYGcSHefBh4sQWf47NqK5atIWlhni9yc4rh7seGX5CDFocIrNuyUfreqYJ+uFu7cTGvAwlLV2PT8hT0XsnBCFerkXERrDuzSsBm4cIUsa+Z2KwWEzQtVWga8SPlZTFi3HpxKn8IEaGuqDx/EbIo6gqvyESIxwScvZRQOOhw8nQjYjIXYeXyBUikTUKI+RloIuGizgFLlmQje2EqYqMjObP1kIQZxMq24cYxnGhww7pNG7Ewxg3XLpRwtuWDjmvnoPZMwabNqxHBweJkgRbeUONcbg8WrtuAVQui0X0pB0ZVNCKjgylLaKFNhiTXrouLrSZqJuwiVA60pG7s2XWOwvr9UGWsoFgC2Y4UDo9OSkZksAsKr5YjauECxMdGITwyBunJMZSC7EBeoydWrF+BqABntNw+g70XaqgQZcKC1SQvKDmR4AkLAY+am2wnVY7YtHUDFkTKkJdzhQi9aDj0VGJIuRArV6zA0vlEsnES58pscGdPFealxMEPvcgpMSFrcaKEcBMx45lwS86IXf9MdypCBt8eo0bFKW0n9VHLubKrw0hQJtakBWO4s4yayCcQRLGSdYviMdxciLLhYIqSRFCFZh7S4sMwxPdudgVg2fwo+DD/ITwxnbgsTliLbsNt3iokRfOzrDVNTU+kUtEgrpTr+HwmQ1t1FgWGVOxYmwGZvhVFvb7IzoiEh1QWITL8rXClsEImiUwuwy3ILe1DLCEIFcfPwpq+BU8uT4aJgvmF3ZQGzIgjfCIKiWmpUMotaCmoQviKdVgQw1p5ulZEyEC4doUbcy5Gamu4M45RmxhCZcF1HProCmTeHhJtKo7iLrboIVek1ND19I/EkrRIGEiPKqwfxfKVixEZFomElHREegmvJydwsSuxMjWM9y4QGVlpcDN3oLC4DjGLlyOcbGMj4eTi3j7kitRGf4lZuBo/YjG0+SpLEKiB60gJO0WYL9qpUVhYMI6WRj07LDv9pQa3SG9prPZHMukvzxGBlrrhReLRAli47Ii6VivVfGSQTRblS/QX3QCiqY+Zmp6OOPdwZAZUwpEdZ7fGEWmL05CemgzXoDGo8m4S4tABxxjSX0g6SYxzx+CyHFRTlm5W6S9xpL9EOGCorQJXSmt5AxdTopD0lw49DGMFONJUAA+/BGxKZn3seBnatdWoOVGPqlJ/pK/ZAT9FOqXL6JqmxN8XFLAoYvH42me2IzjQD3Iuv6RMW2oMx5H+Mo92CXIIwMJzNXQ3jUKjlWH++nSkzUvBhO8Izt0ooEDFEGTxwk5pSCH9RZd1CQ2kvxioauQpzbxnvjcep7tWEF2cEpdgg1sPHFnI7OhCsYqkJLh1daCnRkdUFYU43D3hRyJHAAUXTMSlNZ+TIXvzRsRQ0GKYHdbl8wVIX74Gzq2tJOoIf4Vtm6D0oZ7ItcCUbCzLyoa/UYWqy3mSRrOMHdgAubhj44TLe3uyVIaykyTNpCRRGaezDpWDnfBTRhKKMIMTiZk38ffgCE58LiKwZuMmuly1KBKQBYYwRlkUrzUGYf2KVcgM5uA20IK3c7Vwfm4VViR58pmvp4To52ijmIagvPgrWDZHl29h3TjaTVQ8Y/zflxSgQCrdTOjbiCA0ISFrC9IoF9nUEYbxW+2oqzWiv30QGVso9CC3d4cU/mB4JHs1MVnjFHJovs1VCr1kVFcz6E0IJY0oJYUYyI465BVqKfitQHw6PV/svI2t5XCJzMIqesLcbMDc78H9edwugUxRQX8prkT02m2IU4wwTCQgePZNxlBkNFaFxAD6ZnSWFqGh1QOePv4ICQlCDGVJC/puw+Ibig2rqaRH8IcqOIzpF8MYUefjEgVuXBh3mOrVs7cc+xG+4auN/pK6ZClCFMM4QfCtxSzqgsYwZBoknX4cwyMeVIwYgEYzhNFIH2Rv2Eg5Nq7EJtrx2cVzFH4OxfNr0uFsHkV13knsO1mF9OwXMC828M45CPqLiLvK+aBMWGWMUbAT5pTCwdUHfj5ud+gvnhx8hQfFX8kOWXSKzF1yE/EJyhbypO7sb0Z/YGzEzdtfor9oaxnrGaFu52gP/e1h0JP+YvFI4gozFOqSm8gpCMP6GDnWbt3KuA9XSWPNOE7pKhVXWaEeZIpujeSNC4a+tQgnj+UwDhOApYlK6fQF/SWI9BdnYgwk+gttYqO/ECXG2CPbC9V9XBlrnaS/qPw5CAs7mOEqiDi0lTDJ1EYwU3axUuKtrTIfn1W44MlnKCLfr0UjpQpNJMBI9Jf9x3DqYj7U7quJUpgskBaqVXWFyGl3JnOVGsFyA4ovXEC9PBXbqGyl7myBmcxW5k/xO7bb6+wgJ9nFj4L/gojjQv1eF5goYK6kdGBSUw/ai8/i4vF+bHrtb7F+Xhi6K65i/+fncLWgFIHrsjkBnA1rzJSVvw/7ZVuVe1BRKhAe1DZ1YPuUxOMdXTlGxUEhZ8IdZdtcqJ07QSCFlQl4mCDBoyQX15tMWPNGIrGDBFX01OL4vv24kHML7bIY2ypBPP4OJAs1ViC/qBerXluIYIaSjOFx8G76A/aUMeGFOMO/IY9WLh4eabMnQ5rYFssISWjHohWvIz7ODxq6kD89+O+oveSJCjIqJyKfY59k+5a2tQG380oRkv1DRAZ4S0pakzuce5lFC1hGelCQXyzRX17fFoN+if5iA6OI/BDR+4lQNt1ZqCu9jYpaeik2b4ff5ERqjMpxFeU1cAxfiwwuDG3NworemmIUknQWsmgT4gJsoSDRvMT2kAMpv8kzeRD9paRBjg0bN2Dr2jT0JLjjw5J+qEcJ4X7yaSjZgF2N3egs+AfCmNXo00Wi68YhxirqEbbkaWzfvAQqz6mnJEgnOoxyCeXgbiRjcAzek/QXLVdm4xSHdiX9Y3DCTLkuR/T29DO+Ns5z40Ol18Ekm136yxg1YocmXCT6y3a6E0b/eID0F5VEf1m/YjWeXJ+ENrqwf1swABM5q088tZiBa8YrdS1oz/8X9Hb3IyltKZ7OCEAIk4K0LZ649Xefo6ePUnocSMW9F/SXrt5BGO6nv5i7iG4T9BfO3IVNJPqLA4XyNaS/GDkbI/1lSEf6C20p7vwsbMaRIZQcv4yacgPCw5xhbr9Lf8mkNN/W519GyoIF7PzOo7i8EfNjlARza1F6LRduKUuQHK2EhTPGMxfICARdsde0aMm9BoWTEn5MPEkI9JQ6XDMnY10aPdvDBNwnOInTT0Dh7oXoBZlIzjBwxVHDY/wWV4vr6W4jGis+Ey+8EoYF86/jg1NXcKtuKXGAZJ7Ogk3mDvF1FhDdki2rX5REiCSyYXo0hjnxstLtbh5nfaooqyE3sq38Fj49cQtRy17GurQoTqo52fYKwdpnX0Vi+gJcOnscl29UYWlcEBFYlAMsKYLaOxs/SQiCkVKipXm3EPn0T/BSki/qSwpxjtJxGdF87rxF2RPpLxy4NS0lpMOchiY0C79el0xqkRwrX/wFAjIIS2ASk5vVhCqKgkgJRhY9mjjoFg544/WseCbIzbWmr7vTM/k3Lb1WRcevod4ag4IblHkk/UWuEfSXJ9lnuHFSJiohjFBX5OKzz6/DkrwGT62KY9hH9IxGNNdVorlpDGvfWMZJuRiPrNB1VuPkZ6fRYZ6HN55Zx2TGqePUowykk5YQqxsRTxD/Cd1cs5G6hnyVsYGJQ/EdaBou4PMq1R36S6uB9BdmULUXHMWuvZegWPgU1mYns2EaWdxK1i61EScIdHYLDEMLxabrYushF/QXJuasZHZreJgVV+vrEM34oUR/4ZTBm3SHcUpA1TQxzuZKNmF5J7xWOBJ3MzvDhsVMd1LxYRwut9FfQjmL7ueqKZiz6CjSX3QcDNVqEmC6uhizjcJAw3kUlYRi9arlUE7Q7WR0QRrDt40XKdDtl4hVK1JgaOmAQS5m6S4Y6uuTSobcSMRpuEA934Rw3nLSX/SO2EL6SyjpLzeraxEb4g8jie56N1fqQiowVlSJ6mbO6unmLKroIsGAK2cu2qX7NXkPZ+pFALwT1m7CX6XqmZxBoks3a1Y96SkgD7BNPYZAVSjmyYZxw3mM5A1i4xgX17SX40zlIJa9kkJvBzupUQVWPfkiUpiA5GTUoJcZtl5MVHPigNlWzRWBuwcsFJKuy69EYxqz6YaKUWbwwtPedLEPkETkRUYr24d51Ave7GyNg13opMZuEGMh1kGC4kd6pdrRWZtdzJSxH/P9WjgZ1tODNaDrx7CbBoPDxKLRfeZmaOaKoQmeTCSrLGFsKi6FcdEiHGHCWEdAJn7GuJWLmDwNjTBjl8QOZRhiZHrcusKKAE7ArexT+tqE9F8bFjzxLIIVbjA0MWO8rgeKzc9jYaaKMY8m5Ob1QU/0YpdWgwEHFZQyNT7Z/R4utKvw6q8WwdeFk3WzM2HwQUjL9Gf4pRDVfGaXLWU8n33ZSA+fxdu3IE99AolB0yPj+Zjf0u/s9F38iM174Rkkkv5iHW5FBz2YrsRaOlHzvK2hicmNofAxVGH/rt0oNqTgZytTSYGhQISFHfBQO8pLaqBxnY8lCYEQY+uopgknCdk4XWbCM68zP4P6zWPjnLRPkRm8d1h9iEsX6eTeHOVduCx29lBiPYP3+08fJf2FZA5Xf6x8+kVkxQ7j1Mef4N3/cUpKK1amb8CiZMayDn2CkuJqeBKq2k0Rcu/oFXh9ezZ0l05AE7kESxYw4SZoL754/19wlCoYMctfQoZEfzGi7J2jNvoLkTarX3iVD0QMXIjI+uSz95Dz0TgCY7PwejLpL4wtcnE245sjsW9+oUmQk3zz1j+d4BSFFJLUTcgm/cVK+sveTz/F/31iP6y00TM/nI+MwG7U7j+KN698TteiI90F2wj+ngcrk5SuHP4YOV+MQyb3w+ItLyA+iBOD/XugT1uHzCQmzgj6yx/+QRLbTlr9GlLnJcGds62St47ht5fPcGAJwIaX30DGwiCYutrw2YE3cZIDVXDiKmxO4IPPuz4r9Be661JWr0MCVxPjpL8IoktrUDYiSH/J/2g3rnIgHCP9xyt4IV5ekkga0DhqKDruGJ2F+UwkcRMp6IQSrNq4mZM1JgJpumBuG0QYtTK9HYZw7uMjcF3zLBZlLMeCW7vxwf+4ytIDBySu+SFiAxyQu+dPuMHSqTHWPfhHLcPLnJx4GFuw98NPUKPupZvQjOTs7chi8oqYb80CEGfG2+HjeQCWoqgrse9P7+PirVvolmkgZznTay8sR/aKOOzb+Vt8wWm5ImIRXlsYgoGqL3ClqBTDvlr8kd+LSSLb97llaL5+BKcLmjA0YoazXxJeXJkOTwrRVzFu2u2cjG3zw+DB9uEcyAnc0nn4+Iv38N+OjbD0zBNrn/glfMxa3LpwGA1+TG5UNOPkldsSHebQH1uRS3H6Lc88C1nzRXxykcfmYiCe/dgL2QnwlFnQ1tfJ8j4LNm9JozfElk/+eN6Lx/+sPRkXz9r2lKQnYKO/0Jcn0V/akHP+FAajtmLeRAlOXi/BgKwPu/69CpeTF2D76z9DkLaLZU9jWLBxOb2jIjHVjO7mGlw9m4eqXnpJ9mpRcC0FG597BU8sDJdKqoTFHlG0ninlRHy19IzBL8AP/pz9G/Qa1Nc3oIf1ka6+dL+x4w7wJFC5vQENLWob/SU+GeEB7oQut7COR4dxChUIEQVn7yBEBbmiYPcumDI2Ys36JZD1N6Gmvl2iv8QkpTBbUwEnxtE660l06CQ5hQNTUnK8VIdo4EBaw2P3j1gQEp1EKkMg44M2LdbZEK23svasm2SRptZu0l/cERGXiMggP3oLBtFAOkmnRH8Jl2o8fV0nINXQtvVgnDWOMQm0CWOkghTTVNeA9l6dlF2axOxSD6sOZ94k/WXl85DoL70NqBV1pKyZjE2Zh2B/xmMYjxREnMZuLZOcgpHMGlY/JvaMaTtQVdco0V/CYkXmqv93Rn8Z0RJHxcQMD66I+5pIa6ErW9BflMywjYlQwZWg+MHedmjMnghjbFeQe6ZuFiYWaVkr6sqszO6qq/j9h1V4mvSX1alKaFu5Su/g4OjshVjaMtDTgauOehJ3BmAmESY4Mk4iwoD1rI1c4XRztSpjglM4STFhBAFINYucrM3RX6Za/Nv//HCi9VYYhhk/rxHtlG5c9gWuXgGkbDDZZ7Qf1Xwe9CYnKBnXjCdpZYwZvs0dGoY3jEzAc2DNphLRpMKMk8rS3NFHyosLS8oiEC+oPhzkBjkodw04IjKS9dgimVHUp7MtNjQ0Q8uVr4evP48Vg6HGm9i95wwWvPxrrIhyJeiC5VniGAwZyD29mf0bCcehTiam9DEPgZzh2AT2R8zEZQb86FA/uroGEBhBTCFrmKf6wURWZ29vL+H2YZLa07e36vfvG7MlWi/KWPQMAzLbEK03Bf2lGGt+/FfI8DOjpb2H99YME++vh48fKzwS4GEeRr9GC1dVhER4IXGW+Ry9TJxth579j/i8zJ0ewMgYjmGeUjWIuLePOJBO/w22MomgvqIRHqowBHEQmo5Ig8h2nY2BdPqtYduj2ThEynuzjf4ikGnTcCCRkj+rGLVpOOc7u+CERdBfqoZV9FrEcaI2PSuAOfrLHQs/9A8PN5A+9OGm74sWTkQbKnC7xYoVy+fDT8z2pmmbG0i/bMjZGkjvHJn0l/qqCtT1u2PV8nQKs0xHLzqF/iLqvqZ/E1lvnPjdlzFrkwVkdI4zzq/crM4IS0jkCoEXynMTZ/dV35Pe587u2R0PLGaDU48tBlLxu5AKtP/8lcefhj88+Hy/nU2mXpvVwRURLBcR9BfrNNtE2GM2bPJAs4p7df/9e+AH733TyhqwwJgMrGIcXs4Hwt6Gp9rszje+4hjis1PbiPjdbofv1CZ3Tvzx+2HmbPjgZ+fBFhKf/U/6mDtfnLJfqmn5hySwJEJONu3dNnXno1N/+Io2NfUj9p/tNrH3PeL3v/TtfptMfQ5nzDakv4QQyq4k/cWNni8zq0wedRPXIe6rOH+Hpqame8ahR935dHzf1ti+6cPwnx9RXKigegjhAaEF+jhuwibT2eBEAxCrByFQ8Tjqf4rMOyllivd2ujZhEzv9Za5w/uGsKiYhol3NLtXj4c71/m+JZ0wMc5ZpbFPiGGKiJwQIBBxipgXa77+mP9ffhUfMTn+x9fczfaa2SZM4ynT2o+I6xL2VBQQEzMAVTJ40G+S93dyUGeCUo0qG/NJn737Abuj7DWB7/74Bd/JhmPpZ+wpDKIvMhkD7A8+LjyhPbdpu4oOPMbl6v9+WD7CJeLj7mAksSCdzAu22tiYeCtFW7PSXuy1w7qdvagHRqYgJiZ3+8k2/933+nBBnF9AMQTp5HCetM3FvhLtbiL37+/s/1pMLcR3i3soEkeBRNzFo2Qc7aV+iDIYJRI5OMtZY3d27VGTNkZVDCstQ7X+gXBODvUxdlQSe73767k8S6YTfmUp0EasR2/ec+D37vvguszsneAwZSx3sm3iwxcWKQXQ6rte+3696nWAqv1ALuOd8J23iRJuIAfWeTQx8k4Pd1Pf59pc/O/mBBx5j0iYO99ldZLyKZK6pNhEDqZ2IMxuTi6nX9U1+/lKbEl/iNbAZfO32wO99xTfu350YSOfoL19hrG/49qzSX+6/gV97jg/+8IPfnbKjr3sIp3zs634U3g0hmzhHf7nXSmKCIWzyna7Sv6LvvfdM7/5mW6DZFkXiXfH79NBfxvpR16hDAIkcgthgNjCLt16QTkh/CY5GErVu7aSTToormFju4OyqQHQ09QtdrehsqKVcF2W/FKFISoiEHzN/7ww0zK4b6mlBdT3pLw6C/hKPEH8f0l/GmZJM+kuLjf6SnBTD78kxPtyL2mob/SUsLknKUJWzDMV28baLvmuS6f/Jwno2TUcTaptZpuHig8SkBKioQjRBQk5zbR3aeijaHxpDIg4l6yYz+8zGEbQwi5QBGoQFMVNV8liyEJ3Um9ZBOTP9VKQW3K1SEpq+uu5mEnE6JEpGQnICgnw9Wc8yBnVjDekvWqrBkP6SKLRHXWAY6rLRXwyC/pLELGKFlLU7OzbhZIci3t3qbpYLsE6UvZaHQkUqhw8JNBaMMJuuX6unYD+L5j1cpSQqE+kafV0dpHKMw8VHhchQyiny2ro725lhaYQnqR4hqgBb9uWUWyi+16tupyiFkdniQYgI9oWZNKBuzQjMbEcOTh6soSVJhrVfIrO8nRl7hgkqZ0WEMeNb0F9mr51MOe3v3Y8P3a440RSklwGRxU+qUUggkWOcCxtHh6BuJ/1llPQXSv2FUqjEiclmfd2d6Jukv4SHKFkcQ63rbjUVxVjEJE2sWYPq4UMRBUr56fqgZha8g5sPn6cQqch+XNSMdnaQFAMoWC4RRBlOF5uiuXRPBGVJT3pQB7OAJwhPCI4IZRYnyyH4/qh+ELrBYfZZKia6sb+iMINeHKOLWejMZ1AGhyHA10YUEjt7aJt871rH3QuaWZsIoIqOYjR9kmiPA2k8ioAgPuced8YW0Y8OMRu3nZUfFmb6B4eF8/7KMMbsa3WnGqNWF45poaSR+UjKRhOmMWj7eqDuGaSmgQIh7De8GUu3X4fTw4rWixTy0SE1rh9/D+8faUAIO2lRulJ57iD2n2F9DlVtbl4+jRFXJUIoOn5+1y6cul6GToGwGrAilIo+o42XsJPEkl4q8rTkX0TjqAdTzMPZ2dniX6OaOhzYuRelxKz1NhUhr8XA74XAQX0L7+ylxqaWCLfCXLQbPSTVkcIje3GaOr8DfQ24cqWFwuXhTE/nipuzhsHBQWlFOlOi9cIeA60FeOuDfWjRMIW6Nh83W1iGw8Gx8/rHOHChCvoRLa5dOgOzbwSiOEC6sAi4veQ8fvfOTmg8YpASEwZXh3H0U6/zyFsfIkctx7xkIuLuDKSkv3Sx3u79fahhmYu6rgA3OyzclxKG5ly8tfckujk41dy8hi5Ksqt8gOsHd+N8BQehburVXuuEkiLewSwfEdzWwaGZFq3nbK2PNJs3WdiedwPlNbcx6BQslSUZeR0fvflH7P8iFyEZ8xEmyDdUi8k/dRA7Pz6LOnZyTf1WhIcQUlB6DO/t2odrV87h8tUbcA9PQ7iKbjLRTLiZRrW4cfIgdh0+hwZ1p/S9sCAvKmgdwTsHzqCguASlzTpOJGKZ3t6Nz959B0fO3UTJ1VwK448hlpOOAHaSwq07J1pvs+nD/l94Or69aD1FWIZ7cO34Pvx+z2doc4zEooQQ6lFrkHdiJz44+Bkqqm7j+o1CeFF4xdR5C29/sIciCtdwOa8QEwFxCHIbxc2T+/Hxheu4UZCPY5+cRqVaRqk+PY6w7XxOtayzl26g1exP3Wln1Fzei/f2HsaNa2epl1uJgPhFfC6ESpG48gmWU5Xh4Fvv4vjlQhRfuYFi1hDOS6Fmr7YWx3b+dxw4eRNusYsRy3Y2Sqzh2QMf4vPr+Si4fAUFNeNInh8HX0kVh+2Tng7hApwTrb/bqmZWtN6AuttX8ac3d6Gw8jYqm5ogU8QjJogVD9INtkqygYc/eBefEYRSVnAdFa0OCA91QunZndh16BhuXDmD4poOhCYvJuWFYiA3zuCt94+ioJYlWhzDvMO4SKIK1iOK1jOAPtaDM/v2YN+hIyhx2YQdXG5YhbpPUTVcvNOxeOVCtI02QDtAHpxGzhMFwlIXYcWyeDLeAkg0kaOlcgSRC7KQkhiOnpzf4WJbC5YNLUWwlxvHJdb0lV1Bud4HGyhenkxlo7f3FqCqmmLQ1Zcx4ROL9VtWUNkoBztzSZZxUuNqxQjS1q3DymQPnP79PtSzLi0pPhiKuwu6u3dymn8SM9hhgrJ9o5diQVoC5J05ePNiK9rnx2D4dg08AimkQEyT7+B76KOY/AiZiY5DdTh+8AiuXsqF3+ofc7xnfZu6BH96632cOJGL8B2ZpBTcPVGrkdq1pVdQOR6E57asR6i1Hu8evI0qKnCYC67AKSgV6zctorLRBezLK0PgRAPOVZuw/JktWBBiwak/HEJd3SLEs56OZZazsFmoWGTAoEGJlavnIz6eWsvKaAotWDjra8c4C9e7WRc6xJUmc6rZeZXg44sNSMhei2UZsXBzVyDQ2wtjYYvw/GvJMOq6cGbnbuQVNZAAEokgoSzB1UAfv3c4h/d6xTpkpcXA3dMfJPqhhwpZYQtXYUFSNBF/9IL4yqCpK8eFahk27XgW4bJuHD1wCRUNCxHBesBZaCazYPPH8RAmjA4PoLPXQK3qPvRSp3qC97WfVI68y2VIWvsSFgZbcPPsJzh9owE/WxuNZ3/4M1BBHFdOHsWpCxXIil+DzPXbEZ7FlSS1Us8e3g2dhxxeinCsfvIFrMYIbl8/jdMkA3UsjkJQ4mq8HJaN4dZKnDx0ErnlbUiL9LOVRZgp91dbgRvtCvzwx2vhqC7H6XPnUbkyAdHOGq6OPWFpbaSWtogHQ+KZpq18EnEyKxqL83CCgg2a4S0IJ5pvcq73ON6Ux/ic2RdTlMPRLQFPPbsMCj9fapcr74YATQMoLy0h1tMbL76+Ddb6Aly+chVFmbEU/NmEN1I2o6fsGs6eK0FBXTdUI30oyL1KEswKvJgVB6XCi/0JpQZpIREaENtD9h2C/uKGuMXr8Ia3G3Ze5hzOTn8JV6CtsAEFN0fp4h1CWjJdHGNchWpJfzldhfpKPyQvfxrPb1mFuKWbEUnEWSt1M2+Vd8E104NyW7ZTEvSXMWrDCvrLPOKJ4ijrlBlQAUe6Qnv6nZC6eB4VfRJZODuKoOs3MN7fCVl0Ct9Lo7i0O3TLglEl56qZrhu/h7xKyULf8H8Ojs5Qxi3DSxEOGCR+51JxFZx8suFLozuH+qKRIsjuTnQ9N40gaxlvAjVlCyhUP0rB7CdWjCDEi+UX1Pj09AzCum0vEanmgTIPd5iFP3RykzIiuYqMn78cKcnzSH/xx0K/arqOR9E/4Ez6C0k5yUmY8NbjTB7pL7ohuMSnIo2iDUmkv2izctBAoPEYQ7izQ38R0XBOsAx01Q6EIcwUjhBXOZzlrgietxRbCD1vaj4p1QoLicXetlq0jjpjHt10vQQlB0f7wZmkGK/Y+VBxJjnSW4diIugMlD90FOVR3IQIRl9bDSdtzkijHJz4XgjBBzLh0hfylAOUfhuhqzcslMQGrg6YUTri4IeIKBb3e3Nw9ToB3dAwjLTJLDSTyTs593KvBZzgSfrL2k2bKKDQf4f+MsZklAFTEDZmL8ci0l8mKNX21vUBOD+/GtmJpL/0UYTjphNaKaEpd/Mi9IH8Xk7IGs2t0DkHYOO6DISERSEmMppL3j6MNt3ClUY3uny9EB5DZBZ7wm4PK/Ldb8CTvFNHe0yJnolxanYbnZWUJOSk2HsUpZdvQEO4eGbGIqIKmf+gOSCBMsTj6eZBGVAKoHS3NlAVR4cROcVFqPQ2K3PVew0595tkAUc4UpPcSHlAjS4dvpQM9BC6qPYbYmS74iTIITQT2csWwzGEY1BnC2o1VjydtYwSqg6oJ7u28GoLPCkH2MdJf0slSVHrHYhx7GOlgxdC7e6wSYs/ZN8h6C8+mJe5GME+Q/j85l36y+D4AFcgJowZvDHCWIJGM4iRCF8s37QF7gFK+JL+8ilnd0GhYXiOcoJelOWqMwzTreaEof4BjI5RbFdhK1FxINVDOYX+4kWQ6pfpL4J0YqO/+PFBstNfXEXca5bpL64krbsyfttfw4y0MSM78G4MUpllcIyoMMYoRw0utMmQRGGvoaD2iTonLGMnMaRrxTAhxSbmXMk5g87OCoC7tgj1XSJud++zIegvKlJcbPQXQcRxphohRbbdfEnEmaS/cKDy4I0WMR9BxLlDfxFxGxF44j7tberevU/3bw5wZ5xzxZqFjGf14eyudzEUvgq/+ckTiFeqEDjSztWpbUAUCWXDfWquOjXMJtbAoikjr7UQr/76NWpeBnEw1qEi7yzKjcHYkRoNX7e739P3dWF88nvm3lJC3W/jlZ9uR0BiNlIn1FCXnkfOiY+w+dXfYIF/BGLNl/Du/2yl+hHVbaqbsOmpyeSB+2w93daY299XWYCMWMpJBpFy5M6Jlmifd+kvsfBlqEfQX5wF/cXxLv2luvg6cptNWPfjBOYR2PoMo64TVYUFGA2kJyIqmNg80dIFxaUUedTbXbT6VQST7ys8fMahDhTduIRWrlw2J4dQSWvyqaASVkhwCPz7juDf/q4BnuYatPYYEcFYm4cnVzfMBXFmZ3vnQSJFpLfhFnbtPoZz+fVwTdzOuOnkvr7qkufen0ELOCOEDNplyzTor7uF3OPnEbJ2B/5qxxJ4CbU0uhE8CRURXGZn4fEjHcvN2xlGKUHWAQZNLYk/BRgISENmrA+GCobR3DWM4H4NfHT1uHY+F007fowd2VF3Yq4POZDy4F9Ff2l0Jf1lE7auS0dvohveL9KiMzkCG7Y+eYf+0n7zH5iU04HWliAuk12RtHg944CO+P8OtaGFwf3UEJFJLHo1kk4o8TY6RmKJxzi0pL/4SPQXkk7othP0FzlJJzoKxwr6S0+3RnK3CPqLkIWaffqLBjqznKutLGznamv0zf1oqixGZb0nnnl6C9YvjkZbDG1yswnaUwWooAj/KLUdm/ILYahXID19PoKpQexAF6+ZclQPqmcT9Bd1j26S/mKAlrqQgSYup8xqaAjqlugvZLbqmKlr5MPcRZsMC/qLyC6jTWaT/iKGa7lnMFfYL3AlqEN12Fn860d5qO1cjDgl+Y48J5GtLOgZYjHg5MzkKtJv1hK5F2LhbPLf9nAF342kMG903z6NQ2dvIn3br6jTHMb4sngeKC3JAdhJJkd4WhbWbdwIlamJ39uP+vYBxGavxvZEI7QdFKffS/pLSSvmP7sUv/7ff47aNrYrXS3MXaN05zlxgsbmxonM3PZdWoCeLj73ogLARn8xkf7Si2G6CwT9xUTPi8ck/aW1tACfnixC9PIfYS3pLzbYihm9ZIQW3+7G6h2vQMmJpbipmuZiHP/4NPpDs/Hr9amMXToRYtCPwkufEQHZhFUv/lckh1F6lMcWbcpKBHhYahb+y/+lQGvfEHMfGL+/WEFpy8lKALZb0VxELbMtpupCDnM6XvqZiprfBTj9RTEKqjoR7Rc/xyT9DpqTlb4lZcQ8PPViPCdLbfj8yKc4Tdfss5szEE93u+iXDFywaXoHpARIs5Eueva3KgHqHuxALnXic1uH8MSr2xDj744StgxlRDw2bNmMNJcu7u808q7X4onF4dK+xCU+/EAqvs1NdNB36C90q1hMOp6UmY2MS2mO8FbOJPsbLuJYReAd+kvzmAyxXFHUnjiK8+6RWL0uE87M2psw8nSoOKHTUAORp+amDEfrrRbUxNTB2atdor+sdqdbLtwBV5gZHBUVwRhpEXQEivuEhsJUWYbqxiiEckUm6C8+Kx1JvJ+dmaFwTbYVH8HhUh9s3LgaoYwDauj3UfLZcySj1SJKfGgTKwd9B08FFm5/FVuZDDRq6EdObwV0ceFcUTLZgTaV+nMxyPCftDFepO2mvqcziTgBoWi8UIc6irobJPqLE7Zyf6GMgeYJ+gtB2ON1RRh2d4M3M3QNReW0CWf1HEwLSX8JCXcibFxMU2Z++SUGOT0z3QRaTujmWuiGdZd5SBm3Ilu5X6ORBn8NtS1Hx/2Z0RwJa24nV/IjMDiNoUdOrwd1mtVVF/D7dw/STU0x8QzqnTLuOsYOr5tJBBPUyPRkAor1hlr6ng+t0iv3RLIry3v6tHCgZ0J0zuPDhDxTfMLZRQ6P8ATM99ehtqAF4wHzERXMLGDeJ57u3PYdWcDC52KI5Jd+Qtr1BDcM6A2cIMngbmhBWU0D3HWT9Jf4eRhqKsSh9w6gU7kEP1ucQqqHgd0G9W35LNURl9ZsTcXzXGG6070/1F3FZKN3ca49CK/+cj58SCEaoz541fVj+AMT0WJXvYzF8X7MDjbQy0NSTCf1dx2ZEBfMBMbYFPgF9aKgtwoTQYsQpeIEn21Yq2XeR+8wHJjsODRMnjCB3m1csQSGhCFO0wgvx1F6l8SAPLd9FxYQoS4dXbDjLt6cEnHjosKLhCgnksnaGvthYD/sxPGmr6kYVSRjmWoqOfGeYB7HBPLOfYQPjhdh6VM/RWq4G0ZGQddwICsjGAboG0U8IeEmRy5ewvxZeeB4B3TxyAOpjKgsX6W7jf7irsL6dQsI6f7ERn9xD8TqZ19EdswI6S9H8f7/PM3FogNUC7aQirIA7u0mFBw8hX89e4irEU+kryPYO8ITtz86gD4m7SxdlI3lIXtxQtBfTEYkrHgZGWkp8I8xoezto3j77y8T4OyPNS/a6C9yJqMcIdEh59A4mZPL8XpKEh8cyY4zfj8dZUyXDpsH93OBgfXKAABAAElEQVRH8PY/n+QUxQFBGVuwYukqJMsNzPLbizNHOdflCm3DC1uxYXkiE2IcuSrqgqGiEPr0NAawWcYiNs50XYhG8yYyTEZ7mUcHkX9gH4YzNmBxygosK96DQ3/8R+lhnbfuDaRJ9JdtpL98gv8Q9Be3QGx85SeYv1CFie42fPrRmzgxbkIYuXtPJMQwDsVBw2Q71Ez+34Ep5j31efjgk8sUjx/lKkOBtc+8hNRQbzTfPM5Eqc9QVN2A3t0s6TH8AtvIkVyuLGQ27z8Qoycj7Wc70iN80HgyBxUVdXBk5uR/tFYjJH0zXl4fhWLONN3WbsfyhMVYFvAWDvzu71mq4IzY5c9iXrAMN/e/g7yGVj44QlJwOX60MgVuxnZ8/M5+JhF0wiQPIp3oBaRGq6Q47dw4OpOt4ev2LegvVdj31gfIuVUk0V/e4oD62g9WIHtVAvbu+h2O8w75RS3B64tCoav8HLklldD76PDHznJEx2fhJ3/1QwQw67u7uQ8ZG35AvqzI1mfZVEslqwVKUT/UwmemFXkUml+/eQPU1y+yLKwR/YZjaCsrRGL2M3hlpR9KLh5Bk+opuBqsOL77MMo6B+DkG4tNL/4AcUy2aCk7j//gIF5RUQX5rvfR37sD2zPdcPnAARTxsybSUsMz1mFFKl3F7APmttm3gIVgivJrx1gpUY5RJh15KZOw40dbSBlrw4WzJ6GP/wE2zVuIhSX78e9/93+yHNML89c8j3g3PU7S1V/NbN0xC+/xzTwpge2VlYlISy/B3rf/BafoNQtNZV+ymvk5vL+mycXOI4rWC/oLGW99BmZGKWy1nPp+NDY2okc7LNFf4uOiSCFxlOorm1q7SH+hbiypKBEsX3Ci0n5bUyPaWMrh6OpHaoeIhxhw8Y/vwMhBYzXpL87aFtapdkj0l6iEJISS1OHEOGRXIykrzPiEuxIJibEIYCryuK4HdQ18OEYtCI5ihl0I6S+8WJGkMxui9VbWkfa2NaG53UZ/CY+NZ/mNLyZGOFNuaEK/jq4pv1DExUVC4SnnSonPOkc0LUs9JphpqvD15iyH73EGNTLQg34Ta9L8SYQZ68apN9+HbOVzWJVN1FofGa1NaqlmLSYpGUF+XnCYEEScOjT3CJUNFRITY3gMQX/pJCmmWaK/hJJ0EsUVqzMHZ5GSP+Oi9byOYa0a9c0d0A2bJEpHTEI0/JhQJupkGzv7mOTDeTtd8b7BUYjkjH+sr431wcJ+rhKNIYjuuQF1E7r6hzFGF7bF6kQyCNuAthwfHGrEM7/8IValBWOYn2kQ1B2SdKLi4qFkm+uizTsZdzc7k/4SHoMokkMcTMzI5ODdrR2BmyIYcQlR8PWw0TrmROsfvdN7ONF64TEYQFN9M4YE/YWnIffwRyy9NKALto7tV29yhDI0lqVTPhgbUKOlUytJzJktjMMLUgzvoyupUJqubjj5h7I/oBwffTvDfI7q2S5MRiPDJaS4MNFIxRI6q06NviEmFPE5mGAuhm+gEo4DlTh44DwWM5a+OpblU7UN6GNH7E3GaRzr4b1cLMzj6EJdC+vEqfpidZDBi8eKUrqgt6UZatZEO3DlExwRhQh6g0TugtjmROslM9zzv5kUrbew5lOjbmNNfS8XG07wD2JeRIwf6nNP4hDpL+t//r9iZZQn+ul9aORnnFgdEBVHWpaLCd3tzejTi7ZiZjzejd8NR1y4D0ZJEBK0oGF611RhMYgOC5CSzURZ5Z8l/cXCwbWxqoX0FxbDEkw9GZW45yZ8219E3KO1tVWSoxK1XI/bJugvTdWt8KbrUxlAZNo0XMCsDKTTcJ4P3IXFiLbyGxL9JZP1egEej+xYkQ4zN5A+0Nrf6s2HG0i/1SFm6MOciNZVoLgNzOTMIP3Flrw0HQebG0i/bMWZHEi/fDS+I+gv1ZVo6PfASi5GPO3x7gd++Ju/aR9IZWLk/bPaqCgRGh8nydnZSSePen5iRSrijeL1z+56v8HFWTljDk9IYMxIxBm5KvsG3/nPPiLs8LjahOEN+EenY4WMIQUGQabrnj7ONvnP7vds/d3+rAlbfqfSb9/2gpmY5xcSj5Wkv7gzk2262pQ4DWETMZkXr/dIqX7bc/wefd5uE/szN9OXZmUMPSQqBaoYV6rHsTBumsY9e3v/M6W/0L05jZF6MWA8/vSX6bWJeLAfb/qL5Bif1nYibCLaiRDxnxMXf7iuTXRQQkP1caS/cJSzhVums/OhGYVN5ugv97Yn4RGbXfoLI0iSq10kcd57Lo/ym7gO0d5lgYGBj7Kfx+K7YtYg/gm3rocHyy7mNunhFr59QToReKe5zSblJtqJoHTMmJTk99zQYsDo7++XbPi4Igun+xaJCasARAjSydwEzWZd4e4W7l1hk8cZWSiuQ/QZMk/PyUzRR2g9YsV3j8uCvjcR2HdkBqWtzkrsnDMBloPYJgOiTuzejDZpH2I+eO/b0llNcEZnZealBPu+c54Ppr8I0gkzz9lgne7sSqw0tFqtNEuejuu9cwpf8cODzlckEIkEBZHSP/XSLSTFCEUjZyGUcN/2JbtO+fuDjsH8ZJhNFubu3EvEEWUoNL1kE/suxCxZ2ERMLL47+ouYHU6KIdhPjK9ffd0steJ13FGgmfId6UfRuB7QfsQUVFQoTuZ+2L4l3ptcgdh3I2aXdiLOHFrObpVv9yoGChE3Es/ZdA6kD2oTD3pPnO0D339QGxCf5b8HNRmxH/smdKnZYd3zuQceQ/Rxop3Zvzj5KlzcQn9Y2GRuILUZRdhPTDC+c/rLffdq8uwe2C+Jv4nzFi3G3peIsUXc00fM0mC23ZgGtfUDUDITTtBfTExBb2b2aEunjf6SHB8JbzcH9LY2QtBfjBaqmLgpmEUVDgVJL2IzGTSoq1HDOziCZA9mZErvipOmiDhJJ1V1rRL9JT45EaEUN3dgdmxXUw2z8Xrg5BWC5ORY+Hu7YlzfgxrWUvYOOyAsLhkxoQFSirLt4u1GmNz5DLyIOsne9iZm9amZKeqLhKQECmHb6C+NNbU2+gszvgQRx4ulL8N97aipa8bAuBPC43m+FPIXWcbi8R4f7aMubg/8wqgpywSjOzZhScmAulGivwhKRiJtEkwZQoofo6Ohmplq/RL9JSU5RspGHWN5TRWJOAOC/pKQjGhm7QrFo9mxiY3+ou7swjCVnkQnI+gvwSxNmGA76ejsoViEo5QZJ9qOyMbu7e6QMr5FJl1kGGkt7iK7WchFatHBbGi9yQpFcCTbgdckrYPHYB2hhjSQnv4hzgwDEB4WRMk3Bk9pKz0lAtso8mF2lDNGEgV/JpGM6zXM4u7GmKC/EJKg9HWXMixnxyYz0PD+jHb50Dbk5Ht8VA+tZgBGF0r9ETYhVNjGWRXQ2dZKSTcrKUGhEqzAcUIQhTqgYdatq3cACUEquFGSzzSmR09HOzR6ljz4qxBGKoyc+reC/tLWpmaFsSszLiOhYlG+ibWk6vZJ+gv3G0IFsKn0F/EMmsdtJKIeZnjLvZWIiqSQvnkE3R1t6NfTw0WiSBgrA5xZKy+ygzvu0F/CEUgtVnvW7kPb5M/ovk73qcysTQT9ZYA0nl5bVYCTC/wCg/mc36W/cFUHo0FP5T0K+8CTVCBCMDjejAxqqAPOvsEig78qjFQgQX9h5QcpVpqebhv9hdKiAq7izYQl+3U8Ev1lhCoQ11i3+f7RBoSykxb0l4qz+3HgTCl0Y6S/XBL0l0B2ei44t5P0l9wykl806BkgFSXclqJu5QmWXNyHP+38DK4RKYiLVE2O7oxrkhxy4MO9KGunbFxzIa43jyE0NBjEeuCdfV+gfWAQjUW5aDPY6C8Fh/fgbHELdH2NuHxJ0F+o7zqL9BetoL98uA+t/RQbqMvHjWZeZ7A/Oq4dwoGLNRxMBnA9h/QXH6K7XPX4Yud+FBN7NtR7Czm5/QiJjubA64aJcT11iT/kNZ6BIj5DwsHZBlKm36srsJeEmVrifLp5jDxBfwm10V/e3ncSPZQgrCX9RQ0F6QQOuHZgFy5SZUXXU4dLVzok+kuI/yzSX3pr8e7v30XODRv9Zcg5BOEKF9z+Yi8Ok9RRdOMK/9aLiKRIFriX4r0P9+Dm7WJcunwZ/c6hSIhQwjrSis8/fB+HjhcSjdQKLTWGRWmTu8i842DZ15CHP723C1euX+U1XoPONQLxfDBG2m/jvbd242wRJ1dtjXAOJNIP/Tj+/rukv+SjjESQ3IZRxCZxgCUOS6zc5+gvj9ZlCk/Hw9BfDJwEX2Ob+B3pL61OkchM5KBl6EPe8Q/wwUefo7K6mPSXW/AgmtFI+tP7uw+isPQ2Ll2jzrYiATEKR1RePYw/7j2C0opyXLt5ixrXSfC19ODU7j/h0GnCHYqvIb98BOFRvmjJPYgP9wv6yznk3SqHf3wmQii9afcWiYle0cXDeOvAZ1TB6oJaPcJBWIHGvGOkLH2CsspSXL9ZCJdwijZM9OPCIUF/KUDBlSvIrzYgeUHCHP3la5rSTNNfaouu4K1J+kt1c/N99BfKQ7Ik8fqZj/H//vYomrQKZC+NgYO+jQSigzj4xVUU5x8jOaYVYfMWI0BOMZDcMyQOHcGtuibi1/rhFUZ85fTQX7pxdt9e7Pv4E5TIN+NF4Tqhuk/H7Vq4+qRjycpFaBtpxIBuAJo+0l/aHBCRlonlk/SXYGrochmF3sab2H/0FM7lGpD98t2SeEF/6Sy7iopRBTZt2STRX97afZOrqxC4VV2FlVic9VtXUtkoBx9cq4ZKpsa1KgMy1q/HihR3nPndXjTUNCGZOKbZor+MUKIvIDYb89Pi4dJ5EW9eaEfHwlgMl9TDU7Wc9JcEKIYapbqj2pFOlIz70R6LEe+nRxkfcBkZnYJ60VV7HbsPn8Wlchds/tndHF2LRH+5ihpTCJ5/gvQXNOCd/cWoJv3FVHCVHUcGVZVIf6m7gD155VBONOJ8rQWrnt2MBaGkv7x5EHX1i5AQpZw9+ovJAL0xCKvXkv6SoIIikB4KsmNjFm/AywupYtVVgd27bqObq8momBg8teMNWMaHUHr+I5SrSdpgHGW85jIutLpi7ZNrMT9RyVrcYHjaNOHYhqhe5ReFbTt+QtfGIIrO7uVgWoalcb7QXP8C3W5ReHLjEsSpPBCg9IGu8Rou1Lpg64vbqZ3ajSP7LqCyIROR/Nsjume+ptuY+9PXW4BqQyMDrBU2wcuBqkFUGZLoL12kv1ytxLwNr9joL2eO4OzNBvxiQyx2vPpz1oJ2IvfCF8Sl9SPLbxRl54oQkvks1id5oPTaaZy7kA/nhDHpfm97hdrLI004+/F15NelYUXCKrwWvRJDpL+cOPgF8kh/yYj0n0Q4WtDTVo2cM4WIXPwkVqSTd+ztA5ehXpSfv42wpc9jfYILiojaOnu5Cqk7MljQ/wwSnSxoKM7F8fPl6B9+AhFz9Jevv+0z9lczFYkYt3RPwtPbs230lyCKrthnSfQ26HWD9FYNwoeKRa39esY5LXCSK5CwaAOUGTKoiy/iOqlinfR6KIfrcYsSgy7xq/DcsjgEUv5V6T9t9Bd3JGRtxE8Jrv4gh+61cRETlUFB+kvzrXq45FLot1aHjCRXif7SrSP95VQlassVSFr+DHb4UxhgvAtniQtTJmdj42gvVwsixmDbJPqLXoeYeelInseVqnsIMgPLKPNEgLNWhtQlKWR1xkOuHEYQGZVGFv47xyQjJWUei6fdsXRZCKpcmYU5y/SXH5D+omspQQ4Zh44+yyX6iwuLyOtLq+lY6qYbXI8lGXKY+FAKt1XeZ9R1dKLe8KpNCKDW4wgZrGdP5iNq0VqstfaCa8c7NhErJgOJGPEZ2UhKTkEQmaML/Stpe0JndS5UdElFCt3JE95DUF7Pp26kHq4JqbRTMqj7jv4sJekvE98B/aUNvZpgiiKEQMl6FbmrG8LjYuDcSW1l0lq0vA5Xd094+akQb3Fiu+lEVwth8ZFsOxSaaCkthMk1it4Yuvm6HBDlE3w3xk4Rc4/AKCxRsvyeYPeeQmd4yp2p5KZF9a0GeBCibh7k++Sg+itFOx2nKAjB0aFRiPbxRqTXF3P0lxnr0L7pjgX9JfIO/aWQIuLC0zCmp2a0KRibsrKQSfqLiZzh/GsDcHh6GeJ9RlDdWYG+jmGostmpUdjEoDchMCgKMbHeHCAjkHOpE70BdA8rObHPzITSFMhJahFuU0Dm2ZVLkEj5yW65GTfc/OHtTqKQvaO1UJ5S3Y78djO2ZI3TXayGU5wXAkzjMA2bCe6O5DHcybQMw5VCLaxUd4ugYlhnUy2B41oMUzFLuJrvj5V+U2vMfe5RLSDoL2MU+WhGjyaVAvV08fN+TL0hXgEhWLVpM/uCqzjbaHPRyghFCIqKhXNXC0rozu+Y8GV4zBH9je1ooldPscEKXTcFPyj6EuzEuMOU7SEn4Tb6S/LCRVB5DeDTvCbWN1Jb0kqNQ8OAJDJtNPlKMY8+0l+GIxVYufkJdngqif5y9MwFXA30gv9ALhosvli2fB5Gm+kGHhuVEnLAGJ7YBP0lkDgxOV14E1S0ER2kjf7iDQWVSwh9gYPzXfqLInAK/YUxUyfh+pOCw1OueKZ+ZDKUK8npcsZv+zmwjRDlMjzUDd2IRqK/jJhc6K9nHJdxoH6ilkLBeFCXCSvXrUGYXI1zeZcY22P2bMNptLiEYGVWDIbqznBmRZtYKM4/+ZA70d+vFPQXUVNqorIKG4gIqzq6+XDWLJfiShbqybrz70LByI82cREdk9UMOXVnHWed/hKEVWsXk/6ixbk970EXthr/y483INBYh08OfopTOQVQu6/kBcgY6xxFNdVHPjlzHPnEFm1aTQ8F7dnTpsVAjyv66YLpIcnlM+rx/u0vnkZ8kPBqiMA/DUBVm5ZKwpvLB5G9Ix6B7hNo7yVezkuLSH8Zyi+cw+1lz2FLchAH60t479/s9JdmbJ6jv8zUU/EN98u2KnenAIs/ByDGtjmbFrEnK9Wt5ArG+kl/4UjJ3AovWDlZH2d88/atT3Hk9CnUtFkkb5gX+5awjAAcPvwfqLvqRfm/Drp8l8CdiLPImFC4MeFHwBHcfPgcMkFESJWa9R0ovJGDVvcEtgt6uuz0Fw7iBn03ekks6qMetIlau6cvncfWp59DYIoCxw/9FjWXPFBR2QHEbKcng/i+xiLs3vkJzuc3kP7y/Kx1O9/QwH9hHxM1o/FYvnyAHqhC7D1xHsFrtuM3L2bZ6C/sM1w4mXdRUSeZE3uHO04/M/o7KvHJrgM4f60clshsTuTHyMrVo7Wb+slMzlQPNuLaBUF/+QlezI6+k3T0kAMp7wsbpRNdkUIQWFBKHByEXKAWJYL+somarmvT0UP6y7ucsXWOhmPd5q136C9tudTTbCrDxdIqqEecMKCpQUF+AUpdIpCxKBXpXMFJTxPTA7qo0D9ip7+QdOLNla+DuYti53b6i/4O/aW7S4MhuoXg4E7SiW526S+0w+hQn0R/CUrJwg7K/Y3+fg9nMqS/1An6y1asXxKN1mg3vHeri67JfrhGZmLp2nVI9+lFa8F/R1tzOfKvVKPL3E6XZxFjN8VoDkwhbDYBCUpPaUIl6C+d3QMwUDvXyp/7NQbOtDngkP7SR/6ooL9MMIhup7+oKaM4Ypikv9BmEv1l1qbKgv4ShLXbdjChTIca0l/++UAeVxLkiyYSuvvS65iftQTHPjyJotI6zAtdiPilm/BjFk5n5n6BEwXFaErype6wNzJXrsHGLZkwN3uhZFczJf4GEU2NZ5FLScFD9BKXdOToRfhnvICNZNV66GuYIKLCStp348IQ1Lto8WFBA9ZnbsKv/o9fMu5FofyBWlh6DFLSAPtZDtp/Yf3Nn93linQ0/uOk0U5/0Q/1MDRwl/7iRl+c4H+mrXoegXHpuJlzEjdy8nlfX8KKH/ySOt6UE6Sb2N/lOopHvTjZnOAqUUNElo2oZGTNX7DSFdZRDQooXH+ysBlrfvDfJPqLI48tABz083Fi546gsKUkWT2BoIkmOH7yEVpGXOkq/AWilrbxGFr4yq6hgqIgnLGT/pKBH/1ShSVLC3DyWBHpL2rEzNFfvpMWZqe/bHshnp6/dsiOfoJTN65DvXUB6S9y0cJsbl4xYeMZiom4NBnnYs1XFY+n3vgN5i/Jw5mc68zfqMMKAlaCIgX9ZQvSXdT47Mgp3Mitw7YlEeLb0jU+/EA6aSIxc7xLf+FpmdlZjzPGwezcCUFLdpzAQONFfFIeYKO/uJMBOSpDfEgsBaiXwjSih0nfRCJ5FeHLCnIFZVw+d8HExummjOAA04ya6Brqx5L+MmjBampwRtB9ermuFpERxGmR/jLIFZdPWDgmKkuYzcrMX65Sb5Wr4buKbj/57IwaFq6c2ouP4lCJFzZuEPQXzlKpA6rkylA2QaQbtT4Fc9XMV5mbN1VUoiGrH0Nfbwe6qaXbYvLCEt7EN36eSpsMU2K0Fh0ldUgI96eclQMGupgJTGF8t8BwNJ2rR018KEat9SjlRORJclBDQ63IrapBDDEFNvqLB3yCFTAWlaGyMQbeZMTequhGWATpL1Q/u+swnryRM/AiJe/0kclqdmb2tBVGrtQ96AaTWUbQwpWEr8KPmpWBULiMs83QFu1NXHXQ26CMRICXJzPoTFyoulPVKQQ36erVD45iQk/xe1dKR1rMaK0qZ6w8BJ6GVuz9406UIg5vZKWRxsHJlsyXuqxudCkPMoHIF0Ps+PzDE+HB/XpRpDqNmXc1BY0wBCxAFLOl5+gvM9AAvsUuBf1FpyUNSNuHIVe6+wfHeO/ppje2oqyqDm7acVQw1yAiMgGjmg6Y6K5TBkWQS+tK1No4+xt6bUhBimN+RF99Mcr4XKzISoW/ewuGWopRUZ9I/m0ViuvGsGChM6rzP8Mf959B3GrGX6N9YRhhUb0Ln9mOJugc+cz5KhHtWgkty1a86e0wDDpD5ekGD28/xM/zQndNIdzYjy1fE0Xf8SBae0fgHxyEyCAfeJAOMiFKZua278QCZtJfBvp6YXBmZr8oL2RZm7dbkER/aa3vxSiTERNCCIYf0HIh0k/qkBzaAR2szmb0cTHmwfsYHML6Vi5V9RY5/ANUCFJYuYAZQrSCORt0CKvChdauI30Rtu2RB1JBf1EwkUPuKiN4V4UNGxZh7/FjpL8c5UpChbXPkf4SO4pTh47gg387I7lUQhY+geVLliEpRMEVrZlMzmBU3uxE4kJmwLlO4Oae/dAwaSdrcTZWhu3FyQ//FUeNJiSt/hHmpyfDL86M8rcO491/vMxU+QCs58pmYWY03AbV+JhZfjmHOOtMWoU1s0x/CYxIg/cFnte/noKFA2jogidIf1mJFGZ97Tu1H2ePWODiHYrNL27B6kS6sIaP4Nhb/4yPuJoNW/gsMlMXIpbC7YKaousJQEVBLzIXJMLTyYDr+/dieP5GLJm3HNnFu3H4T//EWbYVaRt+TPpLItxJrCj90xH8jgkQFt6HLT+y0V8spL8cPfQHnDSYEZG6DtsoZj+b9Jfe+hvM6r6EDnY0js68V6QBpTC7O/fgHlyuaOFkwBGKiMX44ZI4QFOIdw5fQJdmmPW2Plj55MtIJvTA0XMtVPkH8eY/nQZnV1j57KsIcmd0Y9enkC9/EtGoRH5VNZpManz4u0ZmhM/Haz9+Bpkbs5n5fBh/f8rMhyMBL76xgNHRbhz+w37cZBzW7BpCOpGgv/Ah4/MwtyD9Tvo9HpT0l85K7H/7A1wqKmY8ux9vsQzh9R+sJP0lCXv2/J70FxkCYpbitaxo6JsuY9fnudBzAHX2CMbWF9ZBxWesIucw9jE7d5jlZIkLNuO5NQvgZVBi3e1y7Pz3f2T7c0H8kqeRFuiIgguXUVtLZNr4MbSW3iL95Vn8aJUCxczUbQx6AS8vSiOy7xb2/v6f4ejkhvj5m/FkuCtqr+zHR2dLMELkY/LCrdixnHKm+npc3r8Tt9QEJBDcFblA0F9CJ0vZviub/uUe10r6S8X1z7D/fCnpLxZ4q5Lxwmukv+hbcf7sCehTXoViogMXP3obxy5VoUPnjPd26rBxXTbUzMrO4SKM8yomji7Bq1lJiPTVY15aKfa++684zUVaeNoKvEr6iyhVNE6GDh+Z/mJmmUs73Yu+Cl+JaDI+rEVzczMTggT9hVy/mAgoRMBW3cpVCOtz2NDCYxJYlqKQSCfidltZA9nFRuimUDC2N4pzf3gHZtJfVgn6y0ArGpo6WdfDBhrHukzWkTqRSC9YlC102YCotrj4aPgzE3ScSSUNTazLJP1FFRGHSHImZ5v+0tfRgtYO1i8xvhMWHUdajQ8m6PJuaGwmjHwM7n4hkk183Z0w2NOOxuZOOrAJEo6JZ5mQL0UnbA+AhWVBXWodPAMY45wYwOk/vA/nlc9i5bJUZg21oKGZq3YHV0QlJELFWlVBf+lqaEBLr46rdyXi4wXVxAWGAdIqGjkrNzpKcYMIggBmlf7C4zexrnZw1AR370BExzJrV24hLaOJgyuxUzJXBLJ+OIKrZ0HJaWxpx+CwEe6sB41g2/FnLJwFgsRhNaG9bxCO9EjERKkwUHMVv99Th2d+vgOLwp0lgPkoa1WFGIezqw+iWavr4ci4aWMT47OsFAsgxSEqCHK2wBZSPXoGKAXoG0TCSAR85ugv09brPpxoPeumR3S8V602Ny4dW3IPeiuiQ0F8EZ+dFgybnRBAL05MCImzOnq1WtVk+bKTZF1yZDTLmkhmGejic9Gh4crCE6GRMXz2BBWJdKWuNjSxBnmCSSKhfCZVXuyPOlvQy4xMO/3Fx59ZSdoK0l8uYMlr/wUbiUEbJyGmgeQio9WNNKlYhLJsa5DlV40sx7O6eLGWMJrH8GQJtx4dJF5160ZIf/GiSzgCYayHt9eRzonWf7l5zaRovYWVAv3dTBBi/biJyYt+rAeNifRFbe4pHD5ZivU//xtkhcjQ084+eURMny1sbz5QKZUwDnSgk32oydGD/VI4ougxk9OrqmedcBPb5zDHIWVoFL1YgkcKSXzkz5T+MoKmGtJflMzw5GA7Oa58+U58i3eE+sTjTn9prmkn/YWF3v5z9BcBV26tLEDtqAqLMijGwXDAdGxz9JdHt+LDDaSPftxH34PAEFaitB1YlpUOhYh/TNM2N5B+2ZAzOZB++Wh8h/SXhuoqNA540OXPHAqX/5+994CP6srSfT/FUipVKVdJVSrlDAIkASLnbBtnt9upp/Pt2zN37p373u/d35s3c2fmznTP9LTtdsY2YMAmGYNNjkJEAQLlnKWSSjmUslSlet8+pUICHDAIYbp1uk1JpRP2WWefHdZe6/uLoIj7336w9BeLhRFX4eGS/uIU/cX6oAX9JSgyXJL4m2j6i0igF/89SpuF6+++pL/4ODLyju3dRJVfnEes+T+KNvmhPL/xNnyU6C8WxjN4qSMwX82I9wmsU+K5CJuIwbz4vEVK9Yfy0B5COYQ+rc0mNnWgB1kMQX9RM4jRP0wGGdc+TcwymYhN3Ico/w+S/jIRNzj+HOKBiZGyM0OdJ1L/c/w1HrWfRQUQ1AIhzv5I6n+KSDtWYBF1N1GbsImoJ1P0l3u3qOgsRL0SIIRHT4xcRG+yTk1kpaIphU2m6C+31qmHQX/hw5Wiwify+U4o/cUaOjzWqI0fdd0cbUg3MWZM2/fj9xV/tX0/tqeo3KO/jW84x51v/DFj5xt7IURH2tTUBIWCEXUUaR+//9h1Juin7yyXuI6tbF9/b2P38HX2EMePHXfLvXzXtcfZT7zcwrfvxXVp0ejdch5xiYncxpVLtFLj2ynbvd5y/Zv72+zEwti+u+348cW863NJp7NWKtt1xbHCtdvS0iKRS0Rnavvb+GtM/fzNFhA2FJ2ooL8IqocYuH5/G442duMuY3uuY3VnrP5bdxutJ7Y6Yjt2XF2xnWN8eWzfid3Hf287XHza9rnz79YyjH0/vkxj9VYcLwZnAobg6+v7CA4uxltjYn4WNhEyksK9K2zyoDwXtmcnSj32nMbfg+2ZjT0v8devP862r9jDur/tPkT/MmH0l/GNu8gtHWbUhwhfH9VWkC4uLsi6LRXUZjxxg4KuIBpXO+am3lQXEeUd3STSCTsPQXQZ2wTpROR7UZBg7CK8tKCpCNLJmLKIuK54uUWH8eAxalbXIDkvkmiCrbwWiiRL9Bu23/Z2JNOMDg6Eq5bJGrfSX7jvsKDnONB+3+DKN5mGeWoH3uf4Haw2sXNwHCeHJVJ9rTZxok1sm+hIbUScyaK/WJ+9rQSjn/xSpO7d/tzvnrgxdj7ryzK+wlv/JtKzbj+/eBlGSJ6xidmIPUXDP0V/GbPnvfwkZqETT38RKXa31xGpxbiziN/wtbUTvrNu3HmC274R7dO4gettf/2aX+8sgGhwu6lIJtqeR9L78zV3eb9fiTZZDDAEEcfWF9zvOe/leNFmjO84v/MctzViwoslBgRjLet3nuHrdrBSSopLSX8JCoSaUXLDjLKrLClBdQNRSqpQxEWFMDfUDk3VFVKU5hDTHZxdvREeroXC1czAonKKK/TAwhfQw0eLMC3JHUylEZuFajxdhkrSX2rRCzki46Kh9VNKEaoNFcWkrFjpL3FxDDihas+g0YCiQsp0kf4SFElZQY2gvzDNWty8dD7rp/TLA/hH0F+aastRUmWlv0THxiCQmsImCnKXUmaqm5Gr9swF9Wdyr4j2G2qrQ2EJ89aGBP0ljnmPHLEOM8eypIjSeV1woch6bJSOikUufJWtm4WpMe315SgsYzShIMzQJoHenhSp6ENdWSHKa9vg7KVBPG3i5eGM/o4GFDAPr33QSn8JY7SZjAOPybEJBxW8n4a6BvRQFEJ0XB7eKgT6iYhtVsD2ZpJZmimnQDJLaCj8eZ8mikk01ZPgwUhbGaO+Q/kMHcw9PEc9OnoGJZ1dQfUYL8EmdJmNrY3M82rHECOZ1cFa+Hq6YWSIx9VUS1G77pQEC6a4vxtzcvuNrSTPMJ9MkGQCgiXCw+TZ5AFUvB/QKe+5XnHwONDbxbw+RnI7+zDq1VNS6RpgNG89n2E76S9KRl9q2MYMdjahvq2HI28hx2GBs5s7fPx8CbkwoKN3WBJ0sLCdcWNeaaDak7J+7ahrYASnhSk0atJfvGQY6m4hraWdA34OpvhO+jGQz5vgAtvAyjIyRD1W7lPP4+BkPY6wBxGJKzTFO5sb0NzHSHjmsrvbm6TfG9v7MELXsCOjxjXBzG8W0mvc7tkmP6DnOtFFebA2oTRsN4WAGgT9hSMwPl8fvyA+dxv9xUr2aSYxysB65CxnhgCfl8LJwjapiYSXTk7GmP8v1LZYL+TUBGhrNlDpijkx3GQe3tCwn3Id147ee0dK5ExPpx4XmLf52Xl3vPbrV4n7ckHhyT3YlUH8l78S9ce+QtL6l7Fmtg6nPvsM1xoHmBTvT/xQFBQiraO7Clu3bUOHyRUa4r1U0YsgQOPWjpTGIDnks82fosXeE24kLJzOT8LLG1fCtzMLH+w4BSdvH5ibT6BAvwHrF0WhYN82XGw2M+3DhCNH8/D0T57CgmkMoZ+MjZ1+R+01EgI+h6NSC49+A9JzmW+0cRGca05h8y7SKDx8+DCVmL5cAbeRBlIpdqPBwRNyx3qKX5fhxy+tg7wlHZsPFDHU3w8D+sMoWPgcnlo9F35MIRJh2hL95cOd6HFnQvBAM84UzsdPHl8MF8NlvLfzHHOmfDFgOIHiRU/T7hpk7tyKa0ZqILsM4uixArz486eQHOV/s2N+sKaxoKe1Alvf2YxGCkK4ejpSZ/lFPL0kEZaWHHz80RcoH2A6lJcr5qx/ASlhXsg7vhP7zwuRfz+4B87Es6tnUmDiKLYfLOJLQexVnxOe/NXfY9msSJBEx42NWF02tr67C20O7himGPVw5DL8+rnZ6Mo+wOMuUz7RlR05sO7V/4aF4TKc2bEJl9vs4cG0oo5BCoP89WtICv3zB9w/2Gd9f2cX9Jf0r0jeOENRkVW/wN8+mQy34WZc+nIz9lIYXuZOeU2LHI//6FUoWm7g06MFfB0oTm4oh50qHD/7r6+h8dIhpJW2cfBmgr6qBdoZT+OvXwimnvUuFAy4UUChDQOuyfjFr9disPwE/rTtPNwpralUaLHqlVcwO9LWkTKVhnmt+7ZsReEweb08btAtCT//mx8j2tcdnQ2l2P7GGygxa/Hr//d/ItyJsIRjW7DzMpnH7IyVuiT85JWNHPRZhc3vzzJTR39/CwyiMv8S3t/0JeypX+7u7Y8Fq1/F6lkUg5G8l0MUc8lg33MAVb39TK3yINjjVfxqiQq5F/fhw/05HFQp4RcYg/WvvoxwB1JhDmzBwZw+CnK4QRM7H6++uJYKW2OR3ffckQ5TjUeiv+w5gBzSX37EkaFEf6Eaj6tXIlIWzoKqr4Junk4y31xQUGdP+ksK5s+LIMzVjwxNVxhLKtDpGIjEWTPZuAfA00/DnFNr4SzM/9LnnUfhgC9Wr12FOLmeqDXKyxUFwYX0F3u/aKwcpb9sOltspb8UD2LmypVYGOuGo29sRQUTruOjguA1dr/f/5nc5REC3N1HkfiAqIWYmRABJ/1pvHmqXpolyapK4R4ciaSZSQjhbMxf5YX2okPIHvbBouQURJD+kl/Yz/zYAYrPmxC3YBlFArxQSfrE+To9lnCU7ecu4wyrH7U551A6osUzy5cjUNBftl1HEYkow1fPw0U7E6tWCfrLSWy5nA//kQqcKrNg8VMrSX+x4MibO1BWnoLo0Mmjv5go9N1LWs3SFTMRReV8L86yZWYjLp06gEYXHR5bNddKZgnwRk/9DexO4zNbtBKpTGtxc1Ny4OGJ/tA5eOG1ZObE1uLY5q1Snlc/A41dqGlBo6CjsRLXa53x1EtL4DlUiQ9OljP30A1FpzIRSn3d1Dh/FJygAMSVfGg528nKL0HE+p8gQWnGkS1HUUPKTHwIuZJ3+ayndptoCwj6S6eEV/Ry6uTsc4Dei2Hmnpcig2SnxNWvIEk9gsvH9uD0jXr8avUi/Cp0LkZ6mnDl8B4crg2EJ0EGYWueR/QiE4ykxhzd/TG6fZyZ/9eAvJIazHr5t1ARk3XyyxvUTU2GfGgEcg64Hl8QC5UPkYMa5qffXCUxo52s4LyyWiS/9tcI6KzGya9uQN/aDZ1rH0r4Dl5sAPw8h6iDzaEcVbnE8lPc0seRyjru7eUDNb1BNi/SRFtr6nzfZQFBf6H4jTwOTzwzH94+SsrTjqe/UAQmMBIbnv8JWbLtOHXkMK6evYyNs5ZLOf1+09bgmQUkdTF+JJCTQ1M9vR9cZpu55hmkhntD6e0HHyGCP2679bdxf/j2H7nm5OjO2cUa/IKCAx+eMo/RX4LZAVwp4Wy6C1VF7cReucCxvwVNnUW4djgfRTlKHrcRT6+Zj35Cu9tKz+OrkkJk+muwcMPTDPTwggszXQX9ZYBszfD4aQR3xyDMTYUkv2w4Mjm/pcOJ9JdYxEZHQObbA3X6ZZiY+O8cHoc4UlHCwjnDSSX9xZWzWi4lek9CC2ln7wT/yHl4Xse87sosnL5GUo1iIdzdLKgpa0RB5nWU5OZAETQNT25cBQVdBUOUR7ywfw/OO8gQs3A1fDkbV6jWIZpuz9Ir6cjIbYZ8uQfcR13dEv2F8oFRpL9ERcdARZ2eWT4UVybYusMoQ2JKHGKiIjDi0Qn/c1eoM2mlv8Rx30gVMHtuAOkvXJtgJ+RhE+j+9gd9n3/lmpQYYA1UkyMaQGpGAF1kDgTqtqPwWgVcQ5n0zudWzzvx9Faira4UdZRdi6cYhb6mFqpQD2jplvEODMGIA13UlS0o7nFDuJsbnG01186Jo0TO9O3rkJt7jUo2VQgh7sjL2cIG2Zsz12SkTAuAvKcEJw9Qt5k20KrdUJabL+kOt6jiEMROXChJTikb3efjvufDxbJOCJatWkkVrxbcpL8wIKXTTPrLnNlIVlkwQKbu5QsdcHo+CBHhLuiqHsDFHmcsXr+CwG8CMWSUduNTLO8tRbOdHx5bmgi1m56DdkfkX7uBZvtOGIOn0z2soKeE72lDEyHy9JAxMEqIlIx1fCyPXMHjHFBw5Qaa7Aji0CVCw465qfIGrlC2dPmGdZQnrZbczyI+xEQovaGJ3F8V3cxM7B9Ddt2zUaYOvGcLMNaGwiuDlJ5taI4luICkMQHuuPmA6aGj/OycADbWxkqUZihRPKBgv0NwxiDjafSNaOoKoHgOB9fCfUt//xCBGvUGesSoyuYTIGJQbi2crTm69dvv/I2iv04KxM6YCX/3Nnx+voIBLSMMCKDGYV8H+hgoY+ZMwUzCvCCQ9+i8sWjtY/DwZ9NvrsWeQ6dwUe2HWPcYrN6g4fqqFoP1mTh0JB1BgQFYNlMrlcCOjaQv/drOt9BfaA8XTyhv0l+craQT3piXL+krIrGQo0SZnG4asUYxuj76nbd0vztwvUZGd4AzNXdbhwZB0RW6vhvQ2UetzrgFeDLaE0EqV3aQR3Ao7QrmOLdyXcWR6k3LoZXRzXvpDMKidVg+Mwz2Qy1SBKkgvXQ0t1HBhX5JT3ephA72XM8ZR3/xGKW/2HFdxmuU/jL0TfQXrkHai+AkLhXfrFP3e9/ferwd3BRkkS6fi2ZyaU9t+xCfB83DS6v8UU/t1Bb3DsLgG1B05hQy4pchxlxNLd0OBkERldWRT/f8dbzym1eQEDCItP27cPLkeVS0kfgiys//pM3OkR6QIISre/D5kT10/zli1c/WczZLN44ijC5tGV8qsW7lwfWrPlIfxPq8Bvs/PYZLgxZoFv+UXhCmavBkUx3pqE0n/YP0F2dXCrCM0pv4bMUamkR/UYZDKZLnR+kvZr5nZvFO06tRUngdmT2++EWyUMuy1ughLjcVZF7FsHoJputU8GGgXTAh7199uQ8jTu6IXZdMJSslFLpYLJlRgkZKWB4hRWbexp/j5aVxo4n69pArArim7o1DBz/HiLMHEh6bAwcSYa5fuohmsoUf06lRVlGCwX6qGXHwr4tbiPiKNtKL9uPIoRE89Yv/C4uiVZzxTLoxpy5I31JQSBQWLqTOdlUWth86iYDFz+C3z6dSAUv0gKNBZ8TlFd64Ro+FEfMeT6Y6HlXTIuIwn4SX4szj+OLQCax94ZeUVQ1EREIqems6cOXYdhyy+OBHv/zvmB0s4CrW7R47Uh7MRXeJmEGXpoi6FDMPEwONsisF/WUp1i+z0l/eu8pF395gLFmxmi5Nf7gMUWrpXCa1ZFvgnDwdK6OUUKt9ODt1x8ULuyjizgViaCmaJ1rKQS78tpP+MgQ7d+LJSH9RCO4pSSetbJgHTRSqHuzhGusw97RHI0knNvqLkS7lYUceN1kVWXLtNrMsMqjjSX9hAFDP69vQbOjAtMQFdE0FwlfhiHDXRpzd2YYmjy7IgucjZRFfeM9mVFz+dzQ3NqCu1pVuKjdM5wzVm2uK/7bbgDrSXiL9uVBOiwj6i56/95P+ouTPrZRnDGD0sp2pnvSXLkl/1zxgRAfLM8TgngZDsxToIxomY+fDob8sWf8U6S9s+LSkv+y4hJL4RXCQq4iQW4bVKTpUunbh7ZNF8Anjk+dse/mq5VCP1KD12laUVDYiIjAKi5+g7m7yPJzjmtmlK3mYHx+KELpdzEPdqMo9i3zn+VyvmkW95RLsT09DjlcKA5aaYCR/dETYoreXa7RkxTYU4ESeDC/85u8R6taJM/vP4lL2DAaFzQTFCKe2h2gB0YaIQc+t9BcD5S1H6S/stNzZacnoseqmvGYuZ4uq2U9Tn1rB48RmouxbKbKymrGE+tv+ROlVZmYjvTYAv/37f4OiuwLpp87iYnEc1iTOwLPPTkN3czHc9+9DekYZHpsTwUG5MycFPVwWysV5fSD+5v/7MTy6ynEp7TgOnJlB4HwuBnUypLdlouh8Furo0fB9fhUiklciZMYwDEUXqW29Fefy6pDCJRQngYCb2ibVAhYOpv20cVhPnOJwN+kve/fhcMYFGNbPgjsxeiJITcjNi2WyA19lwClxJTbMD4OM5Apd7Bw8FzELnYSh7Nx3HOdYL9bMWICEeWsRnTKM6msunAiewPlCA2ZxOcDWv9x7RzpqGtFA2+gv4kWwM3diiBV+cIgRvaID5Ayqo/IUPs/1QXLqIrpoqZPZ74goroXWpbOgTmrMXzQLjoYK9Dj5Q84Q8e4mAyycjbpy6l1LN0phSBHsBf3FaMFSocGqc2BQQQm02iC6VzJh5CxUqQ2GufAGkWVaqAjKvkrhYZ/Fk0x/yf4cO7M8sGL5ItJf+kl/AQKH2nDmYAYCEhZhTrwG5cUVVFDRUVs2FqVFfew8a4mSM6Da7AEVXbq5X+xEtSIOyxfH0X4DXHd2JkCHazbsZEfopxdEnKrjpSgOV6OX9JfcXkeJ/qLR2OFCQRFC/BUS/aXP3QNKDlCGKQJeUBYKOX3cgv6iDZlk+gtd2J3kpsocCSegK9tN5s01iyBEB7tKiLy2Dm90UKBcFZmAkGiuX6Y3kjvaAXcHIwycZUdTk7ippo6doAI+DMBylTlQd1mQZPpQW1CKXpk7MVl69DuHIyQkEl7dRO0ducTG0I4DjUauPRfD18EH17k+qglaCJmlk5qoLlil1SGC9Jl82RF2uN0YpEdlgpQGJ7XR+HO52IhpCO1tzaT1NKPLlZjETqqbSfSXWuRwTVumGkBuVimXelLgYT/I5ZIsxg/Y44VnYqAclfMzkf1bknUdNXbT8XwMYynsh2Ek4aO11xOhoWHw7jYh1+ECswdaUVfHJR+ugQ0NDpFv2gW5hwx2w4zypq5365Az10jb0NbH48LCoOgcRF5aOoZkAXj8hZclQEevoQwGLwV8fchIHWBkcfMg5Iz7GOynN6rHkzEN9HLYQoD/XB7SI3IfJrYN7c3UdXck/WWAMHZmDCgIN3AY6mC9aUa/qwbKgTx88vEW5JkT8YvZrGvD5ER3At1tHfRmkqBFgszwQA/kgYzy725Dq5EEGSWVuqnNa+pXkHfM+jJuknbfHamjszu81R5Ug3Ei/UVFhFgyPjn4Jekv++BMFuXyp57H/PB+HN21mwSG4yIZDJrZG0h/mQU3/RCu7DiC3x/fCweZD+asew7Twklg2LkNLWGpSJ09D4u1n+Do5t/hC9Jf4pa+PEp/MZP+shub/vkshmX+WMFIvllJgv5iwO7DHyNt9yCC4hZjeXwsQ5o5i5sEn50UQs91FOXp3fjwd0fZ6TGlJXk9UmfNRpPZgE/3bsIX2+zg4hOGp18m1YKDARh348B7/0eivwSnPI0kltfOpR2XP92Pfzm6DQ50Q84lASXczxFXtn+CnsRVmJMwHwtIf9n77v+RZp+Jq/4K0xNi4Kqy0l/ePHccFjc11r1C+ktSACzNNdi7+x3SX4YRMn0lHo8On1z6S3kG6S9nUNfUR2wj6S9PvYg506PRgTq8tXU//vk4U1rUMXju1YVICeKMovhdiWxj4qAhavHzmBVON/ClPdh7KgstbAjtPIIZjZ0EuaUDx3bug+uSJ5AYswChl7/CW/94ivZjmg/vc/asVETadeGDL7fh9H4LPAMT8dpTyYjzbMfC6wXY/dY/cUzqBDnXrF+cGSHBoxl4PbU9FAsw4r2hEJ++txlpN7JJf2nHe11teO2FxViwJBZbtr9Jd5oT/MLn4GfzSN0ghq/FYIB3Ilm+xAza3Ke97DSbazswa9UqpoRxKYSDN11UApJ9P8M7//z/SJAHVWwqngyRofDEBziZ1QgjEX9eYbPwow0pcOOSypkTu1CpehwbwmOR7LMXb/0Tj2N6lipmPl5anIo4coHtuXZkKA/CSIcdEubEwqmnHl++uw1ZLa0wE/sXmbQW65JDxkDhD8Wmf7kXlegvF7/EjhNW+otSHY/nXlkt0V9OnDiC7rANmDZSiDPXS9Hp0IWP/5CP8IQkLFqQhIbzu3GuuItUKhcECcrLmgQM0Iu194NdKGLMDknzSFzwOFYkBjIVamzlcALoL93M6RqAgqMzJSNLhwjVra6uYUAQ6S9cHwsN1XLEaC8RGGr1TRL9RRMiqChKzrREnl8NR4idTFHwZpBQCFNBenHqnU0YFvSXZSlw6qSKP+kh/dRK1IZHQkWXqYNliHmpVahtaiNWy5d+7RCGK5N8T7B2RVU1Ovst8NeGI5hBJM68W5E0W1tbK6mteHp6PrAaJvIZWxtqUCtyz0h/CQwJo8vQU6K/VFbXos04RAoJAyVCmGPGNZ2uJj2qSLEQ9Jcg0iXUviIftJeh+9VoIEfTkaST0DAd3NhpnHjnYzhK9Jd40l9qUFnTJDUMwRER8Pfiy821WUNlBWpJSHFgVHQEjxNUE0HKqKiqoXvMgfmV4aTuKCeV/tLLnL+qWrrnOCN2Y76WTqIBOTHf2IhaUU9IxHH3DkJYiJoapxZ0cYZeTWrDkL0MGtrEX0HeJPmTtcz77CXBxt2HuaUEvzfnn8Gb28qw8ZccqEV7o4XnaiDse4RrbSrSGYL8PDEiqDuVNawPTIlivmiY1g8uXGtr5zVEpO6g2RFKlUYiPLhS7GNKtP7+Xw2RZG9gJ6fRkBVMgYu720Q+ehdqK+vQw9QVsQTq7MZ1R74ndv0dUv3tJf3FRx3MZ+9Lnu0QupiD3GPHgCDOCB1HZ37DA73o4JKRPXOVbTmhZgautdTXSsshJuLQfAK1DBpioJKhCvoWIztJfqfWIjTQk1kCZ/HxJycx99XfYlmMD4x0H+u5NGM7TscUPefRa5m4ZGAkw9LNzwf2jAXRVzBfuZuoQKbTqOkdU/vKpXV3cf9TovV31oIHKVov6C/tzfWorm8l/YURugxkDdV6ovgC6S9Hc7Hyp/8F07xHuOzVxr5BaI2b6fFiugtjbAbayYcW+cAOcil2J1iloHu4HXq2I219xPYxhzQoWIsApZu01GYTrb/PjvROA93vNyPsSKpKakl/UcNfdLb3e0IeL1Q0Hm36SzdFGqz0F19v+YTYRCgbicFFAKMdH7za0wQ8xPGnkOgv11A2oMKsaaHwniCfrNDNrKPPT61WSypY4y859fPdWcDWkWq12kdL19rMqMzKIuTq7ZE6J+Gmu/ju7vrb95rqSO+0j60jDQ4OnhxlI9JfKoqLUNHpgfn0IrhznX0iNltH6igajx/UxoVidVgoNSnpQGHZJsIrK2akojMVn6IDsalq/KDu+1sKQ+0WBEaEMZmYEYsTZBNhB2ET8fmo2cTCICrf0Gk36S8TUYeFTJg4z6Nqk2+pPpP2J2FDW70SthS/PzLvmsWBsQuRmKd2ktbKJ6JOCcOPt4mwzdR2q02EnSdDIlDQX1SM1PYLdWZrSglbIS97n5t4tra+xa6iokKEx/5wNhbOtoY7US+hOI8YKdvoLxN13skymnhgtm2iyi46jPH0l4k6r62cD/LTag9hE1F1mSYxATXY9lKIemKjvzxKNnmQ9r7bcwsbis5iPP3lkbHhaLtzs1ZNRKWi4Ww2EfQXUa8mo9O42+f1sPYTNhEdqFhKEfrn4vcHvVmvIfoWthcT2GaI+xD13VG49iZ+Y4GlGimKPG4bray3v1w2Q97+ve3Ib/q79fvbGtKvuYYYNYynv9jO+6A+v7W8fEHH2+Sb9v2usn3TcV9nk6/bVzR4wiY2+st3Xe8v4e/ipRBEHEEuEXi5qe37W0B0GK2trRLVYwpZaLWfGJwJGIKQP50SrbfaRLi7hXvXn+IVj/LgQlBsxKTEcSLWx0T6i2ispc6TdhJyeYJe4sCcLHojrRs7EHFBaydCQYexP1Abc5jpLoKWItLi79ykv3Nl0GlUBNq6BwkpDEwQikK30l+GqTJCmgpVKmzjHHHdSaW/fN39CKILyysRcW6GxVukkRm+4d5FuQURx2bXMcsIMXi6qPm3W2w2eg07RryOkxP0+wAAQABJREFUt4mZebZmukPH20R0pOKlFsIFD5X+ImoEy3bzYfEmBSlH5HrZnp/tvqWB1uhAyfbd3X3eeY2vO050pDabiNnD1Pb9LSDoL8KGol2ZuI706+gv31w28d7c3jh/HU1InOHryUC2c4vW6vZaaP3b1x4n7X7nMaJtnKK/2Gxqsx9V5/qoXct6cvuzunXP+/9NOBfubEPHynHH9aXJzlh/drME4tGK9mrcycQkbULoLwO9zSgi/SUgKAiB/nIMUeC5QtBf6pmbpQ5BXHQYF+7t0FhVxhQI5piO0l8iI3SM8rVnjmAxSsrqMOCgRCTTP4IDBBnEWnkF/aWzvoK5oTXosxP0lxjSX7xgP0JRApJOyhi5ai8PJOkkEr5Kiloz/aVA0F+67aAR9BdGabpyUdk207V93jTMBP8wYupHY3UZiqsNMFP5SaK/+FL9gqIRlcWFqGogmcWHZJbYCAbIkIhTU4YSaV8lonlvQYzwteaeUQ6tpxHFxY3w0YWTlsLvR8sqIoPb6ssk+sugI4+jzcRxYHRibWk+yuvaGaEt6C+8htwZfYxCyyf9pWNA0F+shBkZI5lttrB9TrApbKWFaagX9dQL7mZOsaC/yBl1G8jo5GFG89Y1Gzno4VoFFWeCdBooRiOZa0lxMTnJERoSLEVfmqkx3GyoRUt7LywyBXTcV0kR85v1mRV/sJ95YM0t6LNXSgAEFwczulvqUdfSAxMBC3aUtNToRAS5VQPVNNiNOn09Rhj1raGijoyKTzZb2D4fjE3+vM9qs53t827vVgyeBns6yYMl/YWpcFqKLIhx8wCzAOqqqm7SX3Qk+AgJUWqIopeCG/VNvVLkta8gB/V3oaGWkeBGE+S+aui0AXC2cIZsqEcDo9ntWHc0IRr4MGd0iFHjYt9W7uvJfYM1AdRuJt5QFJhi+H08d62eAvj8RsC+HZjm56MKooThABrrqnmcmceRKMRryOyGKTDDa7T1SvQXJxcl87W57+jA32YL2+fd2uTPeT+bLWyfE3uv1D03MtK2vomiPZyMcELnQ3KQitkN1jaDE5j+HjQ31DNCV9BfWN90QVQ9IidVEH/qDMwScaCcqRaBzPxwIFmrhXn8TZ1UmGMFkRE+og1mloGQDxS9NDeHf+Qm/fR9/2En100R8bP738OmzyuhjYmj3JsL8pn/+OnxPIax9yLjzGH0uPghyNcZJz7agmOXC6hhyFwvpuMEafxJAMnCpi2foq5jAJ352bisN0MbGgxfprKwNqO3uQjbP9qGggbKxtVcx7nyPgQR1wZ9Bt7bdhCGbnZQ1y+gup+4G17jyq5PcCKXIfQdVUg7XQGFJpgdMzsZ3qyIrhIzrwfmsqM92qqv4O2Pd6CeCLDOsgycr+D9UESgOfsgtu6/QcpAHy5fTEO7vTcUplps2fYZ6o2D6KBM2flyM3ShOr7kXAqnCtCVQx/i3e3H4B09A2FBPqMdKekv9bnY8uEOlFNAu7n8Ki7UmJi+4Y/+ivN4d8dRtFEMozTjHPQjFGpmH35u+xaklTBfrrUcZ87UwJ9pMYG+lNnjqL2LrhXBA7z7NIXvXUlg5DN8//VNSLtyhTi8LBidmGrAAVf1pc/w3mcnkJmTh9zqLgSzXPZteXiPpJi07AKcPXcOlUyIj9Z6o63gCP7w/qcorqjk9+moM/shmhJtbpLcF8cQAx24emI33ti0DTn9AUiO0cGDcoC5p7bhg50ncT03D3m8hi6SqUIMW7c396Py2kH8/t3tFLTwR1I0aT0UerCNLuVy+QTOpr6vzR7t/YWnQ7i7RJqZmJ3e7TbYbcDZA5/gjU/2o9YxFCkUVLAn3ejCgQ/x0e5DKC7JxvmLGXALToCOnayZcn1pu97Cx/vPQBaaAp2cYiZpO/H2ti+QX5yH85euwonC5C591di9eTsus56dS7uAgl4lpmldUZS+B29x3wKx7+WrcKBASkiA0qqdS+k4fcFFbP34U1y9fgWXzn6JoxfzoAgOQ2vWQXzAPO/8Ih6XQaKTZhoCZb3UEf+I7eAF5BYWoKzJhMgYioNQKEJ0zMLTIVyZ39cmd2u7R3E/sQQgXN4KBVWpJIm8ibyLQRRfT8c7f9qCG8XZnNhUw9k7CmFMZbFyiYdQlX8RO7bsRmZeDk6nXUW9nRrxvgO4fGQXdh46j5wr+3EtrxraaakkedXizL6P8QlVkHKKitDQTYpVbDjkHHiJ+xDP9p4FGYb7mnB8xw7s2HMQuaS//JhTXkF/qc+pYF7gdMxemARVbwWnvV1cM6Foez3ll0h/WUD6i6enPwLk9sg9cQYjSlJRUudBNdyAkk66g9ghic1Cd2R97nkUDwdg7YpViPWsxzsfXeAsTYOqggtc3I2T6C+y2jN4P60EaqcGnCs1IZnC1wvj3HH09S2oLK1EfHTQpIjWC3d2P/PIAmOXYEZ8OOkvp/DmyTrUVJeg5nQuNDFzkJIUiYRayiEyv7O3o5WiEUuQGB8Gh9qTeONkPZo4oo0McEZ90Tl8su80LuTL8BiZmbZthMLJNbnnyHzR4dkVKxBkV453t15DcWwAhq5cgJsuCatXJ2G45BQ+upgPlaUCpyvtsezp1ZgZKOgv21Berkd0GEXcpaG37cwP6lOAxoc4ugvCipVJFNqn1rJvMEPPKenY043QOcswKz4Sasq+qRUOKD6dhmpZNH705Fw4tWTig6PsfBMos1ZN4XCXaLzMvOLBstM4WMTZ6fxpHHDJWXAuI/QLtSLAx7kXbRTwHyTZfYT1Z6ivF2GpK5BMNmsA85w1/mJmT6nEliqc3neSxw5hmBQQ0zBdyVPbQ7SAoL8YOZO0g5+sG51Uo7HSX0pIfynGLJJ6ktRmXGIjdzKjlNJ7nugsvYEjaXq4KF3p1Rih9KMeeSezETL/OayMdUf22UM4nlaEuGenY9Vzr8GRilbX00/icIEe9XHOHGTlIGzBC1gR44Is7nsyvQhzo9RwkXNtnDngAZFz8OqvwzkG70F2xmF8fJrSpa0GFKblI2whj4uSIfPsYZw8V4Rpa9WSizhhxUbMk+gv3lBT93pSXrGH+NR+uJemR6F/hDoGCdj4/AKJ/uJHnfcxpSl7+GhiseFFNcU9GnHyyFkCL/QYXDwP8ZQC1KQ4Qn/9FM6dz5cmRYHUNrB3csasdRswL9wHSi/SX0ZBIjYb3GNHSk1MuuPiF67Hr+mK/eCE6Sb9xTtYifIMSvo5dqKqsA0zJfpLK0XLBf0lD4XZdHku2Ignl81Ca1M3Z0pGfL6nCs4yMuFWPQmF3CrOLtY0BtjYhsdNp4s0GqGuAUjyz5LoL62dzoifHUPSCfURfYxQnyX9hcIDLhGxiInhyDLUFSlz1aS/MFqX2T1UG3zgm43+8pyOegkV13EqIxf2nqlwl5mpAdyNnv4LKCs8T6JANNY8EYCw4CgqDdmhpSxT2tdBuYj3zg62tQTHjmYiau4KPvhGuIkBymjpBRhAaMZGJ85HZFQUmSkKzPLNhYWdR2e3C6YzPyo6Ihwjbh1QpQv6Sw/cqOwSGxmNcNIzkgX9xYl2pU3kdGM8+I1uMVH+gWqSE/zIqCWCyJ86lxyBWiiM0EhBiiaSOHyVhLXTAzFMaS87eQjdMAEczWvgZsxD/5AZ3gHRGDFkITuTa8MVtUhcthw+hBZYN8ICPAOp27sCCnM1TpidhAOCLhzen50JhsY6NNGV7K0UMySmD1HOrSQzHeWKJKxdIENRvzuvPLU9XAs40OVPYMNq0l/sm3HNkc0SAfb9PX3oGgnEmuQkJLH+9jeU0KPTiJoKSmRevArNojWIcm0gkMCBMRkmDPea2MgFIFAIdjAX+NhVI999yvxpnVFZVI5G0qZ8g5K4dCR0wbkvOZVBQXI0qgJxIrMDA6xr0saYBTdlAKL433BnBXIGjYiZtw5RBHu38OVRCji9xg1qlRpnsqn5bQ5gZz6A+nq6in1lUHh73wSEP1y7/qVeXWjpDmDAWE6gexT7FgVUai4H3GzyHDkT9kUI+5iy3BtoJWrPL0YBdxJ/FKxLI3UUtqFYQ42JIkNcBnA02VHylkQqfS3afZ2g8GGA1M1zWW18jx2plf4SnTgdvq7NUKaP0V/aSH8ZYAFFQ2kRqiItgv7igyXrN0IewKaf9JfdX53BJR83LoBQGsxMMszqBfCgCPTps1eh41rrAvIjxWalvxCZxbUGM/O83MjOEjFKdi5y0l9cpDUUO44U3Li+xf9DSa7gQ6W/uCtIFyHVhK6cIQbL9LY3kv4ShC4L5fH8ErF2jg51V84g7VoxdJplCFWOoHncvk1kINbl7kGtTIuFKaFoLTiGbnacJgZzyUafnD21i3293BhkJPJsqSVM94L4k7CJF0fTwg6C/uLKHwQCSBBxpCAtzvQlIo7YQdhW+vdB/2MHd6WawIJ5aKZ8W9qOj7EncCF++ypZqpRwTLLo0VFyHu8d2kmR8V8jRkeId9ox/PGNPLi31aC4zoXSkW7w8QpDcO9+HP88k0pWavz8WSU1e21uQ9ZFRxci6Lzg5srZRDfviT2pnTMbutiFSEIjWovT8e7hnVj58l9jFiUCjx4vQ+KGjQj1NCCbHNheMbLAVHDRg64N33x+vr9UpPIV9CYZl3U4EJLWnqhuJVMyxsJGf3HzlJTT8q5cRHqNP157OgRDlVQU6h2Ec7AftDP9sevzP6H0nAcK6bUwhzxDh8UAKq6fwSdfHEdmaR1in7BQjcgfmkQ/7N77JkrOuqOgqA6WkKfHBUDaSkoAdFkeruYasfjnMxGiA2qn+2DPntdRzOPyObu1j3iO7xUbZWppTy9rRelluoEPmfDUL/9vLI5R35QvtJ1x6nMyLOAETWg0Fi82oqsmBzsOn4I/6S9/88K8m/SX4b5O5F04ih1fnUB+4wAWJrLSUeeqTV+I/Vt34NSFAliC57APG4KzMhCRlGbtr21H5olPcWjECz/61d9h7oTRX4RN6NJkOy/NPEwMNMqpEPSXZaP0Fxe8k8FF3152DJxFBKi4oE/6SwU7zA5DI5y6TEicNhMrli6DV6cbqR7n0drcDjM7UjGTEfSXegae9PYNAe4DaGvtg3KQ9BdzAyUI2zmzssCFQSOC/jJEp52hgYLXhALDzg1dnQxaEPQXa78hSvpgN9qhl7PidpMr1HGC/qJA7x8/gr5aBaNDEFbOnY/ly2JQ62zAf15po+RUKVx0gSTFpFr3pSu6qug6R9zlaBgkfq3+Gi5dy0KVXxxmJEYjmhqfovMT9Jc6Q/so/aUfbS1j9JfmjjvpL/VkLvZQtFk0TMImJk+RKP9gTTF2djs4uwdg8donGXRE+kvwMfzTJ5dR3pyM5QkL8ETEIKXayuG49XfIKKxH0uNL8A9/G456yks2l11D7YlmzjI6kX3pFNxSN+IfZkehjW7vE8euIzFKiwSpIttLLhtxS6LOCHKI9MwZYBBEtNUT0SZ01ZfAYdsfcO5qPuzdcsk1zYU5xxtVRj11NT0QQnSflnaelEn6mHGmfrrNAuL53UF/6WpA1yAj1JkFMEwpPhMDz/KPZEIPLa5c6kBj0QX0V3oh9KVnMffpXyEwuQbdPe1QuZ/FjRGi9NhB62Yuw2+08cjKOI4tpw+hYPF0ad+g0X0D3M4ii8GMoqkQSzRmBkSKqPdhBi8WZmehzXcRZoT6cqbpjIXP/QpBKYzDoEC+n8s55DlwoM8OXj1rBZeuTFJ59kr0Fz1mcwlliv5y20OehF8l+osmBuuejoCpRw/HPZ/jEJe+GjYI+otM6lkcXD3pZViPv4uZibSTB3Dk4kHkLEzAtIAYIvD+B2bPv4BDx8/g9KVizHhqBhJSV5P+YiL9xRV7Dh7DxQIDkh8Y/YWtmT3XIgZI+ejro5+6Z0BKgemqPI29BHonz10s0V9K+5wQ6xfATsQbV1hZa8q5ltpVzchcN7jInNDJCCkT/bGuASGou1qJAl0BfZF1yOFsY6mHH6M2HXGG9BeNJhDOtdck+otXsA4W8gkLSqiDaCcj/cUA36WO8Jik1nGEWrf67H349Ib7KP2lGwaqaagV3ohUlKG5Tk9+oRz11L11c4xAS+GXSDunxrKlC6Cy475mB4T6huKnv/yfDMDtpYu3EIZ8rvESxSRYi236OpicXUh/0aH6WCkKw1ToJv0lhyDsxzwZRaZ1wLmCQuj85KS/XEOfB90SgT4w37iB/JIQuPcI+ksT9UuZ8kJX95jD+MHV7BHqWHYxmrFjiPQXBwahdBkJKfcklaMfDXUNcHGlPjJ5rcYOd/hFusOVFB8XlQ7OdNe15LdAl8hADnoeCiub4RyVwohLBgX1eKMny8h0Ai4d5FVhhEBoEVDWR2qIobmJ62s8luLhfvRUNDByXCYnkUO6hhv8ub4RkrAGr2rmckBiQWNJN/xU3nQtu3Imz8o75eN9cJXhO85sZnxFWwtB2/yv05VIwHYqeZH+IjeR+pRfBCcyaXMySxA/ZzWWqWKxmM/LPNCCwQZ/yHx94cE1KycXNwSFhXO5xAh7oz3mLdDCzOj36n4HRmx6QsFIeXti9xw4knSkp0Pat5Ta1FybXbAsEm52dM+WFxAKHozpYUo01pQi81oDZj/9AlTUfBbRvI6kDWnCw9Bc3AHHbnssWE4VNhMjwGuIeGM96u3pR1+3HCqPKfrLdzzyB/ZnEzWX25oa0efIQEpBIuPkSulB78BQJ6pLObGw94GvE8EHAzIEMnZC7syobAbH9rNNqSM9yp25rcrRCN8hyzC6SafqIgFIrnRGD4NDh/sV8PcYlzXAO7lH1+6YDRy5tukbKGejOEp/WTkbn3z5lUR/EWtXK59ejPlhAzi6exe2/vEkYb0OCJ7zGPUOk6EyKVH3yW589G9nMcT0l5R1P0JsiAdu7NyK5vD5mDdnHpbqPsGxrb8n/cWEhOWvYGZiDLwjH0f+u7vw0b/wOBcVVr9opb+4GQ3YeYRRn3sGoCEoemV8zKTSX/xDZ8E3jeX6/TGMMCdWN/txLJi9CBY/J2zddQj/+8hnsPfS4bmXUjA7IAz67buw6XdHuK+jtO+cxFlcy2SUMV/YriYFeYvNSJ4RBXd2POd3bEPPzNWYO30+FuVsxb73/5VrM8DMNT/DdN6nK4kVue/sJq2C9Bf3IKx/lQFGswJg10xX+t73cITuy9AZq7ExZjLpLya0VFzFpr2C/sLINpJ6Vj3zAmJ8HXB2y4ecDVahn/VBHbMULy2IBdpz8Prm/aio74BbUAKe/fESxER4wXFZKrK+PIj/nfYZXeeOSF39E/g79yGd9BiPlc8yJcEVhzdvxrHzWYxWrseHFAR4+YVUFH91GBnllbwGQb+xS/HjZUmI4fpZPJcezPRk6H2BLkZVx3NQIlKCKLQytT0UCzDNTdBfPtiMszdy0ezQgfe7OvDai0uwYGk8Nu94i/QXRwREzMNP18xGAmlTYvba11TL6HcGJ0XNhsp1mJG4O7HtyHX0DDkhPmU9npjPYKH6TOz9cD+q6cGyk7li0bpXMM3XHsXpn2L70RsEIYh4i/V4fEEkHBnhm3FiDyqDXkCk2pG55+3o8pyJ56YHcvBL1SZC53PPfIodx7LRR+5wAo97jBxLdBTj0HukvxDPZmbsSHTyOqxNmaK/PJSqxItauKZdQBrUjuPZ9GZa4BU4Dc+zPXQ31uD48UPoCX8cc12q8dnO42gkLMGBcJBVT7yCCPkALny5Aydu6CVARkD0PPyE9aJX0F/e32mlv7j4YOaiJ0h/UU8w/YXMNkP7IGHUHPG5ixlGJ2praskT7GXUVAB0wUES/aWDgSWC4CFIJ4E6QUVRsBenT7qBka2GFlI7PKX0D7ljH06/+yFMicuxcGkKnLvqSUgxYIAYJU0oaSBepCpYhtDCHLBauoHtXDnLYMqMl6C/dLeimtfuEPQXTSg0XDebbPpLO4Nb6kgWEfQXdXAI6RQix7OHI9YaNJEq4KJUIUzQXzgKamsglULsS35qIPcN4L42koXISW1u4gzOmyH5w+04/u7HcFrwBBbMjYddRx1tQrcn15CCOQL3VTKpmTPixppqEi064cBZexipOwpOPQe6mkmKqUU3ySkqTRiCmBMjriFC8oVA+wMVraebrI/XryH1R9BfXJmvFcw8Pg8nM5pJw2miK9rE9U0/tZblIq2FaSzlggAyQOwZKTk6jR9nz+zg6N6t44y+3dgHR1cPaOiK7Si9gLc/rcSTv3wOqVFeaK6pI+XF2hOKnD9VoDf6qVLUIq7h5MqZJ68RwNxEMfOUNpFLZkT7IDFanLU605U3RX8ZNc19fIiUhnuhvwyxQaurqUcvNVDpLKBXgrmYwSrYMTisis9Wor8EaBHM52ojsFjMDEjiLMLk4gV3R6aGNdehuqEdI04eUDP1Te3tiiHmptbwvJ19lH50pwtWo4M389e7mvUkALXDwn1VpLUEejuhKpv0l22nkEr6y8p4tbSsIHLSA9VsRxicYSEztYOddw2JMBZn6zVU3m6sR93kmDLQiMFR9nQZqoLIRPYRuDXrNiVaf2eFsonWC7jB90mTuvNMd34zQu9GR3ODFDAk0V/8mO+rkaP4vKC/5GPVz/8LZvrY0StGKhXTV9wUPgw600LuZGLOcS0MJG8N2zM2IyCI3GglzGx/6qvrmMss6C9eDDTTwE9BaUNe2iZa/4Okv1QzKMDdT01JLeb93Gmn7/2NUDp55OkvZXooAplMzoHERNhE5PsJ+otKpZo0ZaPv/eC+6QAOGmoLrqGc9JcZCYL+Ygs8+qYD7u572+AiMDBQ0kW9u6Om9hpvAaE72tDQwKWGR5D+UlWMfKbpzZkdByU9bBO1CQUfIccpbCJUn6Y2opiZw97e3o5Jpb+UFKOqywPzUmI4QJ+IVnSsI/1B0l9UoSEMIGGqAmdN96/Rz8A9yjiJzlR0HqKxfNQ2if4SFjqh9BdhB2ET8fmo2UQoCPqEJMCbs1knzkSGJygPVMxIbTaZ6FHyo1bn7rW8tnolbPlIbQww8lJFYK6KsRkOo9KdE3QDNpuIT5sSzgSd+pE9jWiLxbsm6slkvGsS/SU4Bn7MenASuecTQH8Rxrfdxw+P/sLCSTmAIhxGBO5OwCYqr3A5Par0FxGPKkXa8j4myCRSJX5U6S+iSljrCH+YIJuI84kBl6gnU/SXe3vphA1Fw/JI0l9s75hUpSbqLbPWU2GTKfrLWJ0S9UQMKkQnOln0l5tt6AQ+X9t9iPpuRzmv+6o14mS2TRpt8XfxjfjeOvoSHeJoR2Db8bs+xzWO488vDrOec/R84trc1/a9bV9xfenb0fOIkU9jYyOUTPwXIsnWc0iHTfA/d97nzWuN2sV2QfG9tbw3S2stl22/2+7Ndtz4CmH77l5sIl5uQToR9BchnXiznLaTTtCn7Zl85+mkZ/Ut9hvX0Ilz3VreO4/7zuuN28H2LMSLLWziyyhQ0Zneeo1xB0z9+LUWEM9aNCqC/iKoHmLg+r1saKv70tmtA+k7649tgH3bMxf155bjxUlEKyDer7vcRtuLm3t/zfn41t56xm+9hPWPwrUr6C8iHmEyZl83y/8D/UE8UyEjKdy7wiYTLxF4W90YtcMddXH88x199rfXN+mY8fvZbMr9xexG3IdwUU8Q/eVWSomFUadi6uzgJJMEFKRr88JmdmiiYKKw9kKHk7+P0E9nLRMrvPieLt1bq74gndANyTxRIcxwc+NxgqbC/BoKD4z5uwXphDrFfImtOpdif9GRirUJ0Tg+eNIJ3UJDVprNLeWVCi4oFiLX0XaPYl/meNo53nZvZo7WKJdNiosQXrhzs7qeGLT9Nccxz5ZpA3fQX2gTp3E2ER2peKmFPR68TcY6vlsqKp8haw4FJW574nxeIhn0tq9HLfjNTePthA82raMjKlG1bruGqHdcbb4Ze8SzC1qJqCfCHlP0lztr3d18I95fYcMJpb+wgRC56lad1O8qhXjHBP1lXFshDuE5RLcmas/NqnDzuzvrh7Tr6D8j9FTwhOPqqfU9vrUD+IYOfvQcgv4i6tXUGqnVIML78+DpL9/+TMY/41t//rrnyz1s9YUVyNaa2NrR+1z5JqWktwmFxUyA5kK6RH9honJ5cTGj57rgoQpFfEwY5d7sYaAAQW0TRRLYeDkz0jYiTI2hDj3qKLgwROyZ6FpGGCkVHhvBnC9bcjSVj0g6EfSXXpD+QtKJzp/0F6qV1JUVoLS6CQ6C/pIQBT/mcA1Q6qmgsBgtPXYIiohHpM5fWlS2jURsn7cabeJ+GxlmfiS1dUuqDIwUtRJdNH5CCJtmp7JQl6ESxfUjCIvUwdfTAS3MUyssr0G/nSfvLZ73pqQskRGVJUWo0rfz3vxJhYkaR4XhaUh/aa0rRQGDjwZJmImJj4OGEniC/lJTnI+yujaJ/pJADVtv6n32tdUhr6AEHYxODY6KR4SGsy12zjZb2D4nzgrjz8RB0JCIWBb0F+qn8hnLfYIYTU36S08L6RrM6Rq0g7daB41KSYrGENoNjKZknTAxijtM0F/kFJkXtZaDMxGVqW8fhr8uGD6C/jJ6KYtpAO2NY8eFM2LZ03kE9Qym6qIUnNSJcrDi5hVI4QU3RnI2o6qqEQNmGQIICgj0oUg9L2Kzhe1z/J1M/Xx3FrDZzvZ5d0eJNsqMAUbgtjASf5igi2AKjItxcz/FFWqrKtHeayHlRSsBGlzFH7h/DwVQ6ht7oFQHM4rSRUppaqitRiOFW1woFRgWqoELZSIb62qkiEvRibp4BiAkSMlE/RZGk7dy0C0G9awHwZSmlBO8batU7HYFIaZRPxptzwyE8HAGUBE8UV9bRV1sLgFQKjCcEfiO5l7oaxlZTIEYMSi08HyerOdaPw9pImGzhe3zbm3y57yfzRa2z4m9V4rjGNsY6d84Sn+RwZd1R+1to7/wahxIDzAat1HUN7a/ITo/qlAxpaq7nfnndejoGYE761BoMLUKLP2kv5AU09EvqhBcPHwRTDEdkRJlK/990V+MlHE7s+8dK/0lNh6hpL/kHSb95WQ+w9UHcOXMQXS7UI7LzxnHPtqCE1cK0cxcLMrrIjDQiykM13Hh6nWUsWKe2rMPp682I3zGNInuQAE89DYV4hPSXwoNRnTUXkd6ec9N+su72w6jieHmVTcukBLiytQRZ2SQ/nIyjzlAnTVIO1VO+gvD5UVeJkcSk0N/ycDbH32GBuMQusqv4FzZMAUQQuBLnNlAVw0OfPA2Nh9pQvysaOa/leP9tz5BbSf1ROsycSynjyHa/ujI+Qof7r1CiUEzGnLP4nyVHcLDmN5DoXv2JlTpycGWTTtQQaRYK3M0z1cPW+kv5efwzo7j7DD7UHblHGrNSgQoLEjfvgVnS1so8FCO06cF/SVkcukvTcV4/41NSL9yjeg35nU5a6D1csKNr7bj87TLyGJZT10wICSWie0deXjnbeYS5hQh/Vw6Kno8EBuugZy5o/0cdO1/93Xs+PISAhOTpM5YmnMwFaqp7CLefnsrzuaK486hqs8DwUozLn/5GU6cvYgbNzLwxf6jKLULQpRXP45v3oR9Zyjecekizhf3cPAWCj82omLmIdxNU/SXe2/WxAj9nugvxkaJ/vK6oL84hWK2oL/0N+Hc/k34ePdhlJTm4PyFy3DVTielhfQX5oyf3vkn0l/SSH9JRriXHUouHcB/frSHoiwlSDt/ET3uOqgcjDj+4dvYf/k6igpz0UJyUJxOjtqcw/jP97lvbjEqyhvhFR7LFBh3ijVY712otF09uYuEooOoaGihXnY/1Gxsa64fxpsf7UUhI0DTWJ5e5mz7UR/4xK5tOJuRiauX0/H54QtoU0QjJYLubTa2Yj1wiv5ya5160PQXoaf99ptbkV2aixKmHjr7jKe/cL7CgVQ68/r//j/3orLDm2mFEXAeYB08uBXv7z6BmqoSnLyURcEXojudmkk5E/SXK8grKkZDj9NE0l8aceJT0l/2HkGuy2q8zBGcoL805FXCw4f0lwVJUJP+0tVt5JpJE4oaHBE8LVGivyg8/Th79YOd5zL4x86lcH0hPirIQ0jyDEQE+0sqEYL+oif9pdSsxrrVqxFH+svbm86R/qKV6C8ydTxWr1tEZaMzeO90GQKdDESRjSBl9SosEvSXP36MqrIqJMRoJon+MsKctn5oE5ZhRhyJLnWn8MaJRjSzw4vyt0Ne+jHsT8tAFmeRQyND6G3To7RRiccWpiJEXouMfcSiMaHbsb4d0alLsCQ1FoPFh/GvB0irIHcuzJ/rmBROrs0hXswhDM+uWg4NKvDOlitsZAIwmHER8rAUrF2VhKHSk/jwfAHU/HtalSOWP7Mas4KAw29sRXlFHWLCJ5H+wty7AYsWK0mliab2qNI7iHmbrIgLNkAz34J2YuE2f3SNuYf16Gs8hzr3OLy4jvSX1ky8d/AqilOnwY8axjW5Z3CugS62IbZ0zE+l81fazOQKVmbzOI94vLx+DuybruH9w1dRM+MFLH7mrzBncAidhlJ89sZnCHAfoTRlCc5WKfDES09D59iInR8foldgDgeBSkbzTW0PxwLMB+0zoq2HCkRuHFjymVnpL6W4cpG0l8d/RvrLCC4d/oyAhxLMDrXSX46mG+Dh5UpJPw6Um+uRd+ky/Gc+hqfnqlB8+RhOnspA7MZ4nkuO1U+tRYTaH17egcwX7McwCUHK2FV4cmEs1NT49aNQx9gqCmMqaoqQxoT+6IVPYsG0MPgQ9yVjx37p0hWokp/AU7P9qNV6DGdOZGL+327Ahpd/Q8qRCfq8y9j7yWl61WSc6Y72yg/HqH/BV6WqHnPR3bym48kXFsBHPF+/8fQXEqC6uql4NMSJmRsMXX3svShMb2TuaV0lLCHL8PQ8d6Sfv4wb1FOer6TXgqpyyRuewLwIH8aV+FJj/lZn7q2/3bXpuTpH8HLCksfxmyBfvH9siHJdYm3OEd46JcouFXIK3E4JtxYkRbnAsX+U/nIoF4U36MZMfQIvbFyFMIpHyxX9uHplJ+xjFmLVuuWcSVB4nJtY7xrs7UEE6S9R0VHQufqR/nIdTnTrthllVCOJRlRECN2YsxB45jLMBPG6cFQZHRXNabcrkuYGTjL9hcorkfPwbIgFrWXXceJyFuwUi0DPJBqLM/Dl1X7MXbseAzV+XMOhi4niBF6mCkqQyVBmqYRZngwvfxInIl7GTDcvuBDfdI0P29WTLk8XwWcV/QfDxRm4EDV9JiIjIhEAT9JfstnBUrO2xxXT50YjMjyUsGoOYthpm8hrdYtOQExkFEIDSH+Zo0KZE93xzACaXPpLJerqfahVSsICZw6OFNVX0WWir6kioL0STRZrcI9wjXMhiXQFX3g7M7+1O5t4LQK7G5twNi0Xsxcvg31FGdMTJHNI/wgg9PDg2HEKRxVcjdlUfbLnDIIDGoKdSxuy0BeQhHkJOsjpEek3u0Gp8KM70B6BouGmZ0Mst091pGN2ndyfSH/xDZHoL+72TWP0l94+GIngWztrJmaS/tJbX4SL55tQU1lC+ss16JasQaSsAQ4kdvTzvejrsmDm6hmYNkMNj0EDMuiJMA5E8NkaUV1VB09Hulzp3rcj3YV9HtrrxNIS3y/i+DjWHdvII20x1OGq3oTVw10Uri/DYHgMsYS9dD+PYOaGmZg+ww+yvnpcycpEn8UV0eFqDvI60JTNJQxdKubHEsnG2ejU9jAsQP1tif5Siuq6cIbRUH1PPOBxj0Php8HiVas4+DmLo+UO7G+YZCj3YycZiI4LZF+bXNDa0EMspwausiYMUee5trYSkWy/PJTe408l3eDXRbLcxZ2zhyaeKGpaAtmbYVAwiEUs8I+MUOOwp4MC8swNYqvkQLRQS3MnVXVcsOyxp/A3f/ff8Opzy1GdeRYXs0tBASL0tpTi+CUD3WszKA/oe0sBBf3FhyBmZybPCvqLqzPpL8IelCVU0BUnlkoE/UWQTsRoUunDl4JlEdMVGfE39iLplqPVSdkYHONM5RQvlksorozQAP3t9VxPycH+Tz+HJ3WBZyeGwHOgD70cARkZxdfOgpl4E+I+HOhq6GOAltw3AG6WbmSd+QJ7ztZj4eJkRGm5dspN3ImN/uLIGx4RRBweLNZ1JCLOKP2FkVbSOqgIOBI2caJcoTjYmX+3Hx12j6tT0rkfzD+C/hKIZasWwous0PSdW/Bv7x1CBeGhvc1l+HLPbmzek4Y66gDbUVpNHTaDs+mzeP3NN/G7/9iOYn2PtE6VlX4c5W7TEBUaDleuEfeRiCNmFGIT2qeBkaTGlKThj2/+Cb/ncSX1vYxVso4R+1rrkcGZif/MRK5naaAO1CHavgQf//FP+MPv/4DThRV85cYFoDwYQ0yd9VstwPfXyQU+0vt7O/0ldJT+woGOq5wDyQ7kXb6AkzUBSJ2mY+dI3i3rw4DZDk5uKqioZiQgWvZUs3JmgJC7tx8H/Ivg59DJGe2HeP31HShrt0AVMg0rUgLQVn2NywJvYntaMfrIppU2ItwGuI7e2tOGDupDG2qzsG3Ln5Be2sqGRSUpJomoDgdHV7hIs05rXWytLsONywXknM6Blutx99i4fqulpv54NxagJGhYDJYuTcKAPh+fvfM+3t13BUbb8+X77iTjoMqXKlkMMLTjYxcDcns3JTQBZJSWZ2DPgdMoNzohgLE3Mq8gRE5fiHjFEG6c+gxvv/MWrum7pPbYVpp7nJHycC6qSxWFi/4ioo7LrjD1tSO7kvSXlcuxfnkimjkbfesy6S89GsxfslSiv7iS/lJy+jLhyoLUQrdfVTYaXKOQGkai/OiUwNr3iZMOQU8uXA/3u53+IrBcEv1loJvElWFoOcpsqG9CV+94+gsb6MmqzXQ39nYY0DbsClUciS5EunX/+3uozBrGpfI6yBpPwnC9AfnX+3B2egjd2nlQU4B749pldDHWY+g/tkBfpUdzAN3AR7Zi/4UuJK17EY8tToBSJoKDrI9M0F9qG8boL62t/YSim4neJBiccnhD7GDM/V3oYHmGmWReX9+IboKSxfFdtNnk01/8sWjNRpgF/UV7HP+4la5oQwoCY2LwzKu/xLylC7Hn3X3ILKhD3Opl+Me/C0dDew8ay0itP9bAhtNAIk4hSszd8LI0oPrieSjs/emK0yDK140DK2fopq/GP/yPSN4/jyu9irrjLYyOdIAdBacbK7Nw1uCCJzdGMsCL+DnXBPzqf/0W1Qbi/VqLcKzjOBQyRxGUKRjhU9tDtIBoQ8boL5wlSILhlPcj/WVE0F8oRm6i16LgWCYa7LTIuNRG2spFDFZ7wXUONVF7KQnZ1c8BPXNZOUPtocfG0zuA7N5wJJl6UXRZiZ1bTqO0aQOWx83AU5oEDuRL4PHFXsZnVODJ1EgOypmyw2tRZxOB2rlYvXo9AkxVcNq/HaW1LfAyNhFA3s8MBDdegwg+e6LfnBx5jBHlJTm40euLX6aEk0M8zm3yEG36l3hpif4SFIM1T0UQWqBnIOFefHXtEgyPJ8NDor/QKzaa6SGaVTt2EmJi0lZZiKwbRZj5s/8FQqhwnjEcXx24ipjfLEH83FWISTZzEuiKXV+R/lLYiBROcNgNStu9d6SjT0hELYmUDvE/ER0pSPR9jL4zdg/QD00OJ1MxjFWnsYdA76S5o/SXXidM96b70n6YsOsCaENnUz/VRzrjCNcP2/WNMFFgWtBf6jOqkK8jvNpDj2zSX5Zx+h0S4oxTpcU8RgWn2qvo5ujCS8vOqeAa8oupgwgZrpD+4j/Z9JecfdiR6YblyxchiMLb9SPUwg1JxX9d8CQsfd0MiklDTlMbwjRqeFpK0K83cjTN/5zIzRsRLfkA8k9ux6ZdmYhZ+RxmRvqi39jFiGUX0k3aMCJj+g5tUnO0BPmkwnRZypHbz46EkNrgYEeczS9AsI/7KP3FC15BPrDcyEResY7uzmFczW9BSBhTEzhgEc/rQW8S/YXRtG2DDhL9pbO9g5QOrjXRRVbBwYVcIWdksRvLQ6oGYcsiqFzhE8hZswENOU0Ini5modHQPPsyFvRy3ay/GcZsbw7ISPxgkFFVTgmBACEI8nLmzDuI6SsNqOdxusREUnAYkMJZ/o2LZ+EQOR/RwWqqPLOvFHXU0x/BbDBLmnLQ5zsToUHU9GW7J9rPqe3hWEDQX1qaDDDwv3ZXAt/bQhmo4wxPsx5ZuQVwJP0lO7MY0+asxVLGRyylG8bU3wITMXz9/v7wp051i/sQrmXmIMxRjbzrNzDsFwHHAQKaG/o5GDWj09iBNlYCOy4PGRiAIly6w8ZudBs76ernTJadbX15IcgOgtzLH6HO2dAzr9hhpAPGdr7LyT4wtfEa17IRwn2ys27ArJ7JrAQZuhlUl813TTHjGURSz3qqG3049UhcVaK/kCDWw8GQU79RIvJ4MbvDia73qhIDel3DEBvkjj7Sopq4bNTR5oBGTuws9J52Gnqh5BKYRkuxe6ULB130SnQ0o73bRPoLyWSM3B3uIyj8lgjvCaK/+AV5UqHCSn9ZtWouth44hLNHv6BovYa0j2VYGE76yy6SGd44zRQX4sJSH0fqzFh42HNRuM8FwVpvKCg6LzYzR52Zez5FW+QCzJtL+kvoJzj6yR+wn6PSxJWvYVZiLLwiLch/Zyc+/td00l/UWPPSq0hKDoFHtwGfcTaXtneAjfAKrGa6jEJ4eiehgbTnSx/AYJ8AEig2/8cJmJmjGTrnCaTOXYQwETk8PIBG7z5cYArQ9Bgd4j1XIjd/G7b++xmY6AbWJa9FXKgcmVtLmC5Sja5Te1CeeQw+McvxVxuToP/qc/SQ3pJK+suSHM5YN/1Oor8krf05EuOjrfSXt3fjnX8R9BctHnttLekv5Lq21BJ2/D6OMg0kfNYazBqlv3AS/8A3O84MWiqvYdOe0wQM9BJzpcKqZ19AvNqVNJutOJ1bCcrQE282D49xFG9uzcHbm79Aub4THozOfPbF5YgO5QsQGsLBGiNq25owXNeFoPnT4MHG7eCeA5CveAoI6MT2Hft5HFOutIl4jtSYUKa59DWW87ojWLk2nhGZXKymz7+vvQZfkNRxpUSPEQ8dVj77PBJCSLx/4NaYusA3W4BpbqS/7NzEyOvsArQ4dOEDrmX/5MWlpL8kYPNn7+DICGMQoubjZ2tSkKCy0l96mQ6lMBvRGZmE2Egl5EuXImfzHvzz8RGm3sXS45ECS0cp/rTlIDrbO7n04oE5j7+C6X4WZB38EIeuG9BjcoZPZApeeiwFroNNOHl8N6q0L+DllGlYmHwDn777O+ZzuyOWpJenl8yG0a8bH23diX86YoFcHYdnfjqXwXD2THtrYoqOK9YtiCMs4r7nJ99sqqm/fKcFJPpLxiFsJ6Wnt28EPppEvPCalf5y9PhB9CW8Bt8RPU7t3IRD6aUwGB3x4cdGbFi3ArGzQnHg09eRbuqBPCAUa5/dAM6A8MWHu1HQ1UVIih9mLd6IFdPV0jKjzVN4n6L1dOey42vsJP3FUw5PN2f21qQ46OvQxrQOGaNzg7VBULjaobO5gW7aFkZwOktUFJUgndAt3En+4IhMDjmPF0uaQ32tOPn+RzBPX44FS5IhYyRVTV0TBsj2DAwJkegv9py1tvIa+pYOJvV4M8VEw/QQQX9p5761ZFICflwLC2IOpxPXMETyrxBo9/HxYTnZqT2gTeR4dpC/qTcwP82eLl5SJQK8PUdFKYStOCslzNxL6cm1zRG0MWeyvrENg3YuJLNo4c9Aq04qMHWK4BcGFom8SycPb76oJlwgtd153uOYPzcO9p31vE/SX3icJjSUEYXMtaSQezPvXU9ygSPznEJ0QfDk4GbA2ILqWj16hu3hHyRSX6yEGRGS/+DpL+y4eP06uty7+0l/8fCBhkFGHszxbKvXo7GNZBbm3Hn7ByKQuaWWwS6SPlhWPj+5jxraQN9bxKUt5Jsa27vYIDoxOjINb+2o+v/Zew+4uM40zfcpoBJVBUUORc4gCUUrS7asHJzddju33b09M3fuzM7Mzt57d+9vZ+a3c2d6ejq426kdZGVZwZasHFFCQhIggcixyFBAkSlCkeo+3ykKkIy6bQlwq5vTLSMVp054z3e+8IbnTwjvC3gsXIPqKkGNAev3WCvKlbiImw9YCQhnnZjOyxdaV1F3Srd3H1cdTDxpogtQyZV8MK9Hxww84aERykY1NTUICAiQpMsmqZn8SR/2gekvXDnUVpmkPAHhK5GTWxsU7EcnDZN9qkmFGXCCl28Q641JYhFJAdwk+ouFbUjJQZQJRwPiGCTFNFsGGBsleYo10zaroMfUoYcZdmoyegNJf3FTDDKBjZSP5k5yj1VM8iPlw0+DKpabbdt9Act+9Dd4MoH0lw5BLqpHn03Jd4d1iPT2DPZ2oKaCJJCuQfs5eI1qEop6u9qpcNMJPWNsIpfDfoXSZUqlL0IxKygoSBL9sH/65/1fh2j9ZNBfRPVIq7keNexb+4ecoScAJThQg3zSX748k4sNP/1bLAhQSP1ve68IkA5J8XdfAjyc2V5EOMzSN8iMcPZXBtYJk1VqqiRXmbXwLho98ywC4U0PhnjGf9T0l8rSGrh6+8PHe5r+Il63QWLYKkpq4RZggLcAzk7AOyjq/R5V+osQYKguuI0yqz9X42HwGJvG+xC2cUwupukvD25EIRH4aNJfhJhKIfLqnLFwAT1ZnIRO1DZNf/mmJR0D6VTSX8qKi1DRrsPiBTF3TdC/eXXf/hPHQOoiZuGTs4nu/kHicHThhIZyFceVJK+NWeoPvd1Lf3GoUTz0gSfoAH/YUi4ICKNNmE4/ICQFJ+C8YiAVmdbi+QtpvD82m/zeW6R2s1foDDv9xVncw8P77kV8X9jCYRMhn/hI2eT3GmxqfnmvDcVZHxkbsk3p/SKx0M9Of5mofvFem4j29ee+CZuM7X/ullqcJOuQZ+0XHAvvUAU9oQN81x/+PGPv44+W/jKRL6A4loP+8qhqXYqHNpE2ES+0WD0IYfFH0SbCHmKbaJs46C/T4uIP1tGISatoV4LqMSUd5INd5rjfmow2JU4kBg2h5PMo2mRcQ03Ah8L7I/5MHf1FZOdOfJ8hnq1o7zJKVz30AufeTt5xwWJF6gjG8jZGUoXFcxjtAEc/H/3s7iflON5dv6dR7GYZe6xRY4kTO25MvNwCrOtOdRKtVjvm3HefZ2L+9fvv525b3Wff+9zb6PV9t+857DfWJqIBCJt4enpKYtp32Xb0RBP2N8c13HWe4fu86zOecdx9x7Sfe/d3XKT9e2PbnHQwqZ2M/51RX4D4rliFTNNfHNb87j+FDUWn8sD0l/ud8j7tRHQo0hMc7WTGP8LwfuKX47eD8b9230/vc97x2p/4TLh2BSFEkE4exUnrfe3wgL8QNhEyksIt6s+45Pc54RrvmTna1dj+UrrVe567+K6AEUj0l4kgf4jVjTio/aLYWJkU0sfaRmex2hlODBABXTv9RRpSWeLAOj9xdUw4EqQToeCgYILIN7dhmgrzKhUM4o9sw9+7l5AySEk6IUQtSCeOo4nrEysMQfQQM6DJ3RxEF0FmGXO90kmZ7MI6TyfWMNknARTGFj6GB6G/8Hv3pcaw5GgsEWegn8pTdF2NJeKIgVTYRNhj8m0ytgMbnQSINvFN+suYgXC44dqf1+jnjnZ273MUalg21oQ5mpzd5DwH7318coiYatmfhNhXdHIOm0zTXyTrfef/iGcjbCj6FREyeKDt7sfCxiP6DnYRYyhP0nGlz/k+sRKAp73PNtpupJ7nvvvd5+vjfcyBW9TOf6NN8XpEJey91yIGb9G2pukvo8YU/Y/QHxY2meyBVMyz7n0mjiuxj133PDN+QTRBsd3V14j2xr5EhB0dm1hVi/Z+b0/v+P23/El9WUsD8gpamIkbAoOfDlbW7pVQ2NdOfwnDzPhIeGoolmAsJNVjmP7i6o3Y6DDoXW2oLM5HCVPHZRpfzGAZhz+zSiWxEF6BjQojLTXFyC2qQrdMS0JKAgXtPeE82IPKolH6y8xZsczmVVNJqAa5JJ2YLYAheiZiQ6kSpJgq0gmLK0hgqRXyZRUmDFJTNzYhHsGk1TjoL211RhTUDCEylqQJLWiTAhSSFDOo0CMmIYG0CBb4MkOsvDgPxirq7rr7YyZt4ssEI8ezszE711xVZKe/uPAcM2cgmElZMtbAlRfkoLS6BQq9AbNmxcDLTUWx+krSX4rR2itHMOkvMSE+U05/qaqoQke3nf7i5s3MS9bZ9Yts4oo6ql4xmzgoHCGBejhRGauyvJIZvoxVcoAT+4b4UdWJtYBllSZJ2lBMOnyCwggp0MGhwDY00MMMvGpUmlqZwamjglEYPFlOJQQyKkmY6eI5vAPCJMKMSqSGc+sX2aDMEB7S+CDU3xtKTuIcqxXHT2nH6f98Jws4bOf4+W2/LOgvPazzFFnWA6S/hAaIjHuGZJiJX1FmZJYslcv8g0jjoAQkH3xPO+keVZVoozqkm08wItmu5TKWSLVQlKGVyDLfUPhR09naXo/yKma4swOUke4hlLC8AwU9yJntw8x9KaPpy+x6ZmE6+h3pmtmZ9lHYpL7BzBIZDUlE/mwjlClspUShaM/UfPYMCEGowY/1ySzfodZvhTgPs/UDQ8IR6Kdl+7R3uA5bOH5+W5v8Ke/nsIXj56TcK2vNW1ktYhLEKGZre+kUI9PmQfbVTaxxr2B/6UxJ1uDIUPi6ytDeRL1dUwsnbk7MHKcoR6hIZnRCZ2s922ElLIMKisHwubOiQMGJneP6H5j+YmPAtqOpHBe+Iv3lUDlCE+z0l+yTO7A3KY8ahlakXhL0Fz8YvIfpL2mFlN1qJ/2FmDMWwfdWX8WHu85LiCMThcdzWPQcyqQavZSFSfpLPekvn+1GQUMn2qsySDGx019s1Tfw0e6TaOzpQcVtQX9R2ekv+3YiKa+WL1kVLp4vpixfMDtJd7Ecmhr6S/kNfPD5F6hn8W6H8SavV9Bfwu30l7YKfP3x+/j8lBkz5kZB3noL736yFyam6neR/nKxsJvp8X7oMV7Ee3suEslkRVX6ZRR0uSE8LBg6qhsJ6Z32mjv4/NO9KGdhcDNrNJPL+0h/8UN3yRV8tPcs2vp6UZpG+suAHr5upL/s2k5qThO6Woy4kFQOnwgKGHhzFOfqrZ2kE+HqFnHSydkoDt1QgN+9S/pL+i0UlGahS2mnv9z6ehd2f3UFKZeScSmtDuGJUXAdqMbHv/4EV9NJ6hD7KoIRZfBAe9lF/PKjvbiVkQ9jfiXUQdGU8vKQyqUoNIyG4hS8/8FOJOcUIvnqFZTRZhE+SmSe2IOvr6Qi8+YVJKXU8xyxCKDkpIwoLGPqMfzso93I7vLDgthgCkU4S2VS0/SXh2sJYqXx3ekv1H8mzeXy1zvw7o7DpL9EYGG8nf5y5fAn2PblaZSUZJP+ch2uoYnwcuogLWob9p1ORR3VumpJ44gliKG/OR9fbf0Zdn2dDNfoJRRGUKKx6Dp2btuHtIx0pCYfxfErd+A7YwE8+ytw+PN/J03oKjQxixFJosxYh9gAJ1rplOn8p1/uRqpRhZVLWas92IJbZ7Zh+/6vkZ5yHilp2fCJXwRdfyPO7tiKAydu4tqFK7hV0o0Z82Mp1GDvuMWqZZr+cne7mlz6i/1cFtZ/Htv2C+y/nMt6/McQ5jWMZKSEbVV2Mra9T9JUei4yklNR0E6t8ihXgggOse74GEq4+Kiu64R/3Ay4dlfgNGuZ952+gqK8NOorF8Arai6CWJsu7kM82wdekQ50C/rLF9j71VnSX9bhLV67qN8x5VZQL3Y2HiP9xb+rlILgpDo01aPARNEBif4SzVilHwI8VKjMyqKKiQcWP7YMfhQIPtzIQdbSh1BPIdPFmr6cqyiFAZtWb6CAQQ3e/4Q3UkT6S+51qA2J2LB5BZRVF/HhOavqxyEAAEAASURBVNJfFCZcLQMWbVhL+ouW9JfPWDJSgVnxwVNHf7FYETZ7LWYnEAlWeR7vnmtAU2sXbCyHy7p8BkeupOGOwhO9fT2or7oFq988PPPUWoQ4l+GTD86gvDQcHi3Z6HALwLqFS6A2UnSAdZNtVPUJdHPBEL9XmXUVFfIovLx+DQwy0l8+v076C9muqdehj1pEUs589Bcn4ZPL+Xb6S6WCIggUZzDYcOo322E01hBN5g/dqHfi7hY+of8aYqJFP6wIwfqNCxAXF0jh+kB2MM6IX/40QpaxDqs+Gx998DVySp5gYTxXirJQUn0eI6ggkKQY1niSYdDOSYU6fCVWL5zJgdUTXqzz5LgnbYL+YuQkrNZtFt7asoT0l1R8dDwVxnkRiFv1PCIeHyT3NQNbP7+FpjYLpyJeVDQpQ9LhC5R2owOHrvZBQYKf3r5HCwhJv060ditg0FnppaBWNeUBm+uKiSUrxeJn/4L0l0GknNyHC8m3CKew4uSdLqx94QXMCg+AhgAIN5dB9h0keshDoGvNRq8gyDiRMxqzBO/811h6tyzIYpH+x6fqoWGmt5Xtpk/s25Ir7etw5TmM0N3dBUuHld4NHXK6qGZD2SsnFb0d85/C/zH7GbRT2vQIB/3UXEpbPhGOxVtexbznZJQEvYDdh64gv3oDQohlY4np9PY9WMDW34KCO0RvprXCM1aPvoFhF794HtYW5BeUwjg4A3/zD6vQeOsSLt64iMKlXNzJ1DDMexovrYyV8kf8fZ1QnpJFr2sH1r35D4iGEafPJCGJkpLzQyleP/x8H3AgtdNfEp98Bp7Env3uVC8LlQdG6C/FKbn0MZtRlm3GY4L+Qtecub0A6aS/5N3WIWrpM3jlqVVUjghFV8UVpF0dJA4tG0q6aPUUVhCbUOO3cqSPik+kVmYUggkDn+d3i/QXK+WaVEhYGI3oiFAo9aS/JF3HUHsjXKPiERMdwyJ7O/0lT0PXEBV8qH0/6ZvMienVMUvxYriNs+B0nLueQfrL4xQCAEwFN3H8lhVLNz+FvgofaeUzRGF7tasaWqqtuFL8Wt5VheZ2C/w8gtFRkoqbim4MUK5Mu3ghCTL2GxD0l36uwmMT5xMyHEn6i5b0lwyOPoL+4opZi2MITA/DkGouAi6kYogBfS1nVDFRUWQ42jCP0lcliqmlvwjtVPSWobLKA1o3Lak3VARRuiKYHgwZ1aLrZOUYoI00nL0rnPthE/tWerJz1JJ8E0CtZI6YNheuxKtQEaDj5yqWKYwOfEJseoD3zwA4NEwmc5f5Ug4xky/OEF0w/mgoK0Z5YalEmHGlvW19bSggq7DCazE2JMqR36WhG3l6+34tMIb+ImtAmoAsMKzTwwGs02bA7DmJmB1gg6WWDNCjpSglkaWhn4zflnoUERwfGuMKfy/SfELnY90admkUcJBTctJG/W21zhsR/NPXasTtzibELl2POLYLH1dfrF1DL09Nnd2zcY8BNO6+WLJ6DfM8ZKhO5sSeXi0nBUVWImYhgF+rtZopLOJB8LyG59DDO9GDR+iBpTqFYHIC7Klu5AjH3HPo6X9OugWGYGJ4LDOrBLM2c8KubpVyckYmS0KH3Mp4p5MG7npvqve4w80pH23d1CznOGauqEBFtA5U8EAA6VGdXNwNqMIxf24iIqmtXFxUifMF9aRIUeh+2Fn8gAOpnf4SzficJzmguiQjPYVCc5ewacY0BtiC5BQCV8qottPYis5QH6x5+kUKCnCVMVCJPXTpXTMEIJqDoqyP6voKJiYo5RRVb2eZCuVpJFVUEeiVw3Ms/UUkg3AGIGOcY4T+wsQaFX0ywi3j7kEd12H6i0IzTDrhCzAlG6cmCleqM/GeGoUmIfl1veYaosIyce3MEbiFrsT8yH6kF5h5jzZEhCSg58IZnPi6Fd79ubhTUQpP1kO2NnRT89MVKr60fUyu6qS0mbWXCUlC65CbEwdsLxIJJPpLPzuKEfqLIOIopbhMH4k4SsaRRFDc3dNBfxmAQkubDCdsTM1EWQZX0l9Wb3gcDS3NuHpgB770WYK//6tnEe+vo2B4BS6fugh19BOYGRlMUfluorRWUny+hftux5d+y/AP/2UjlUQSsGG5Dc3NRuz89AzCVryOv3x2KXwZA3MhCchAN0vfpTN4970cuJorUWjSSIkgFlMhjnz5FU4nZ6BZu5KrYysajLk4eaYEic88hxBdHW43cOXBmKzNppqSZjJ9kvEsYKe/eBKzJxLixNxLij1xRalwH6a/0JfgotKQ5StgA3Xo79TThUx5wIoMXLyYj5/+tx8jkUxRT8Y6Xe5dBtL9X2XMQWp2F1b9ZLakBCZC5WJf53v3Hb48oRGu8/Lke0j3rJhpDfcjzGXjAG7EjasXUOs+A88mEPcnHYNLh9IcpCTfgeGxzYgNmNbbHe9JT8VnAx0kPl2/jfSWEPx0uR8V/mpIjOom0EOEg9jzqfSMqftDc+Q4fvZPOXBqzSCJKwDzqJAVFTsbq/qMKM++hCOnLmDTSz9EkJOObFN6L+jJYA8sLQSGmGA0dnvAgZSH4MDBNsUGNii1MSG/Zqe/qEl/WYMta2ajIUaJ9663oqYzGItXPs5UZ8pp9UWjMCmFSUTlyC4pJP1jFdY+swqaphj8+tM7qGBSSVyQG4/Nt4n0l+ox9Jempm54WOliGayTOmY7/aUDrXQf9gn6S10D3aDMPpBpKC7cgn7nvimjv9g4eHa11qGZEwOJ/kKYbMfPP0J55gCul9dC2ZiEulu1pL904dqCmZixcT5eelaJuuYeSp4tpLtViUAmQDRcK8fjmzZi08aFnC1H4N1t+aiqNZMnqpHmPkOcIVXWNlPyrA96KtWazT0IYIa0bICSexx0+zhLGqG/8K2vrSb9hdJWomNqYwr+VNNf5Jz5L1//DN3SnSgKOYN/3nYLpXXLEaTpxfVTX+KKUY8X/8tmRAd6Qj6kxXKSYsS+hUGn8S870lDaQPD5zHhsfiqKLEgTDCrqE2cZYVoxG96uWgxxshUyez3+5b/HoqGtE6aiVNScaeSEgeL8fvF4+cd/jRWrc0mYOYybaTmUY0xBcXUeZHe8YGyvRnKhFmFcoYT4zgAXH9Pb92gB0YeI915kwzqxoxL0lw7KYbZZ6eqla7Wvmx4WYtK07l6IfGw51m1cjiFi0PZ+dBSltW0UImfSHZ+hE6lHIhNUIvrwfvraTeQgs7P0WYnZEX6Q0g34uXjcYl/ZyL4iq17QrJhoMhwwZY4SjymyvqXeDn0d9bh27hCS8hux4Y23EcMcDOqsMkRBHOSRoyjqjqc29momOtm5yt+jOf9sT91Cj0ReUjrqEIpr14pRmnsHqqZgxAY+jTgfVm3QfRuz+En8d59ISrZ2oCrTDWm36dHUuiGYMo7BsfOYk3Mbsq9O40Z6OTaGs1SnrZFJZzZq9FopFNTDSZarlETq8GY9+EA6/JhEBy2VtfAFEA1OISN/tL0FLYwNtjIjzkVOnFjFBRzI1JH+QgF31wYUdrkg3oeuvgYnVFhIiiEVZICDgGXAfjktddVSfEPtH466m2XICb6DQdJfsiwyrCa5IzxcifOkvxgC/CDoLxZSUTyZKefEQHBuAXUQSV1JzamH35Nyul+mqHfkSrQ26zB2p5O9+qSgvzShZkiFZRHL8LePvwCwE6gvuYgcsxmRJN30mvNw3diL2TGc6XQXokPNhBc9E6Po5qxj9mI7eaU2/ukaFC8ktWlraBO5Eq60SdXJIuSE+aCV9JecXgXCqRkbSsWOS7kk6YgAOFFivTpPeBK6jtvpyC4IhrKd9Jc8MyIi5RDec2nGP8mvmp3+Ql3bXpYuOQ+g2dxEV7cXb7EHt08dwicHb2PuU68h2E2GVsaCYSFab4BlQ6QCNQ3vq2GTaKgqJ7WFWrl06TU3N8HNPUYCdpdlFaPfi9m4pL9o9RTolw2gspNJRXTB+FGfuKy4Amq9G7V0hdeCK3Il4x/xm/FOwBKGRun+KehmHN+LiWoaYpXYTh5eIGmSLfqnenj2IXTPN5LYUcc/TWrq4DaG8ZmR6kNx8dt3cuFE+kvmrSLMnLsBYfSs3Lhcitq6CNiazKjX0q3LnItBayd1UskA5kSzq5aT7WY/vg8Ktp8SpKeZsOilV+DvTs8Dn70ANTv27RH7RvnCX9UPU0URGl1CMZPxr/6WBu5jIh3ElZrYjeQua5B9cT/e25uExPVvI8rLmR2shRnBLTi7fyv2JTdjyxvroZf1oLXTCg/hAZqi7udPtWU8yH25MmS44cevYwEzb/voxWrns1NyrFAPtKGsoADdrmGI8iXHmnrMAVxsFfZRGzl4AfzVA6irLKeerhss9KB1kcylD9KzwoLqb13lSMvMISaToYXSOsQ+uZlEKyf2ZHaP50MPpHKWG/gH61kPJOgvTJJZvwQ7vj6F//eMoL+EYMMP1kj0l679+7D3PZJOCFyOXPYsVixithtXYTU7j+C3/3yOwvUazF/7MhJCdcjctxPmyOVYtnQpVkfuxKndv8IR+rTnrCf9JZH0lyjSXz7ah+3/QfqLOhCbXv+Rnf5iqccXJ3bh8le9RGmtxcaZJDHQUyQ8rZO9yVyY2BC5EIHJ+7DjV+dJf5EjcsmzpL8s52pymP7iYcE1lgDNijbA4CaHa+0u7Lt4CDa6GhZteZOQ9Dj0K9eieNdx/DL5CH30eiwmkzTSR47U3TtgYSeydM4yrAregSOf/ZwwYycs3EL6y8w4qAKeQfYH+/DRv52j4nsInn57M+bMJdWkqQr7Dn+KM6S/RC3YZKe/0CsxVfSXpvJbEv2lki5UhZqknpd/SI6oDUd255AfW4nu07thzDyPeaufx3xOlo6cYTJVvdg3EBt/+BriA1xRcPJTfHWlgAOyMzRkSL706jJ4EVN3bD9d5mtegMy/Dbt2fw1jbTt0oXPxw9dXIcjNitMH9uJCbiUbOyUWY1fg+SWzEMsXI5YZy1Kn68mVRKAPqTssbeBAOgFKg5PdzP5Ej8+QhqkA+z7biStZzOxnVu5nne14+3WSo1YnYuv+3+EM6S/+cSvwF08uRMBgAEoyPsanv/xXOGvc+Q68QyIM+5K8y/hoxxcoYAmVyxc70dLcj797aRZa21ph8ZqPFTMD7W5YZm1X5l7ERzv3sfysEnLu29Y6gB+tcEdq0leoDHkFASoLUr/aiq8uZqGhEdi5oxVbnl4PE7N/G+vNSOeqtCwjFTNWbMazc+XISitAA6lFZw58iIxrEXji+R/h+aXkmw6XW/2JPrg/ytvSePlhzqonJU9pR5UPNCLmPmc2XLuKcebccfTMegWqtiYc+PwgCpus8ApfgBfe3ALPARPOff0JknLb0Mv4aejcx/HGuscQ7tyIqtJifPEBSUByHWIXrsfLC4Kl2Hq3fRwVXolh5/8DmYQzSc7sGtr6SG/RcuZP+osgMHBW2EIWqZJB/qAgsjdVTqzPMaGW9Bcr459+rOnx89CR89eDxro6iQIypNByXwN0Lj24+Ok20l+exPLH50PBFUY1awF7qZUYGBIKbz1rKpmI0FxHcgrpLzK1J5OLDKxJlaPP0sp9a9DeyyU4a7wCWF85pfQXXpeg3NQJ6oATS3JIjPAV92n3gdtpJB0D0JN0o2bGaIuJs2fGkGXMBjQEB0Gv5Wy5rxsNdYR0kyQhUwtXgwEqxoXOf7IDimVPYemiBDjTVVVVQxYjY0iB1CUWTE9BxDFXVxOK3cHOxQuhwXaqiZUJFhI9g/FUn8AQJmWw/pLTZJGSP+n0F878ezq4Mq9jLR6zvlQk2RiCGZtghmUj3fAWquD005UmakN1nj7MvKSykJkgdw76Kq0H9w2QPAodxGWZmti4Bxm34iwyiIi0mjvn8f4XlRL9ZQHpL7V05wimu86LLEG6iRV0CzaxjZhpjwEXEj4IQfD31Y+QQ7gsIY3HwnIhF7hrVFI7EeIY0/SXB+oIRr704PQXuuVJ3ehmmEJM8gX9JcBAL4O1g++0oL84S5QgA8ueXNjWxXtWa24DoaIkugSz1s+FNe0tJC+xBpDtjumQTGxj/+OrZc2wBa30ZvmyflnU/olwVFcHy2b4ng6y+2N1KVx1OvSaMrF3XzKW/4jhgGgvdDbWMJOYYjHcx5k5Dx6eXhjqamZNNOXtWOYzyHaroTfIlxn1LQ1N6BFiMHRBy0R78w0gzUmUW8ik8ohp+stIE5H+4hCtnwz6y9gz2ejpEBnYMo5NxSmn8dXZfGz86V9jtrcTGkh06RygxrKPP/MwvOFMOIioLW1kcfIQ0XnefuwzqGsgKGUdZJJWs8/qo2fMx9/AsYX9Ok/kEK1/yIF07CVPzN+HBjlbLK2FxtsP3kSt2SMTD3dsoV5RWVk56Ri1h7vK+39b0F8q6U4QyVpenEhMhLdI1Ps9yvSXmoIMlPf7IzGewh4smJ6IzTG5mKa/PLg1hUTgI0l/sRH2XUb6S70LFs6LI4LwoZ11I0acpr+MmGLkL0Jar7m5GVNJfykvKUZFB+kv8+kpGI6Bj1zQA/7FMZBOIv3lAa+MgX4/rs5EdqkgnUzENpb+MlFUh4m4rm97DBvLP/xDgyWJtP4JsokYNMQEQ9jjUdP/5IIDnqS/eNCdLmfsdYJMItnCYRNHcsm3fUbT+9ktINqTsKEoVH8oZ9dUG5SrYHc/CkGQ/qJ0sr8XE3UJDps4fk7UcR/l4zhsIdqJkNib7M3G0IBvENXenBT0hE5cnyEWJKK9/5HSXyRvyoTZVrzQjz79ZWJtIh6+WD082vSX4SLrCWopwibT9JeHM+ajTH8RKb/C2zPREwDR2YoBQ2haT0/Q7O1LTOTFH2GTu/RsH675/d5v288zsX2GuAfxbGV0OwyHS3/vNXznX4qL/rYNUrpBKVYx/mnuZ4Dxv2cXRR97bvFy19fXQ6/XQ6PRjH+SCfz0ftf7XU4x/r2NHuF+5xj3e+N0EKIBjKW/jB75z/dvDpt4e3tLgIM/X0s8+J076C+CdPLAovUPfvo/ym866C+CdPKoeX8my6Bj6S9TsSKdzPsQLmqXiSB/DLJ2UcYEFhFYF5uNqj1WJg3IGaCXygrEZ/THCWWeIQ6YMtY3Oo+hv1hZJyZjacf96S9CxNyFxdFj4hYMAPfxe1RyuOt7gnQiFN+UKsVIfFWsNMTDUiqVU0A6Ean1gszCkouR66VYhXTvIglC1KQ530UQsHGgt4latmH7UR+Rs5yB32sT+znutYn9e060iaMOTjwPh01UtIkjvio6OWET8fwnog2I89x/s88C+eil2afjNsX+ov5W0FnGkj2kz/j0xlIWuCdtyBpDe+bWuKeSXCy0o+MepZ14Upqex7/rU573m6QY0ck5bDJNfxnXxH/wQzGRc9jwQQdSezu551Tjfij6FdGmxuzLfuZeQofjt2LFMHZX0aYEjWls23Pse/dPkZBkX63aPxffo6oN35+7jif6OLZbdoV3bWJSL9qWeM+mB1K7acQqfbLpL+Kpif9LGxvJPY9F+niI/Y+oXh7b10h9A/f+Bt1n+FBj25GYfIv2PmZkGt7rO/2gK4xF8jkFrcweDaGQuBvpL40oyi9AZV07C+LDMCshmnU4FAYoFfSXFvTZWCdI+ktcbDjclaz1K8yRSCcK9yAkzIhhTZ92hMIg6C9NgnRSXIku6BDNcpZwfy+J/lLB7xWTGuOkCyTpJA5+rJ/saa4m6aSQ9BeK4keR/sKyBs0U0l8GSW6pMRaiiESXAUF/mTEDocwUFUobeQUVaGfmqgsHOf/IeIrpe7Lkgup+JEwUUmxd5svapmBfuDCxqKwwlxQX0l8okTdrJukvnswQG24Fgv7SUFFA+kstrKS/xM2agRCJGsPv5eegpLqZsolBmEWBdm93JdWDKpGVWyTRX0JiZyKGko4iJd+xYnf8/E6P/VvvzMxY1vZVsMTAQX9xJ6kjhLQVUSva3FArkTkGmX0cTGlHQevoaKhCGUUk+lhtGhQZhUDqlQ50N5MKU40WSy801N8NDzVAyyw8u0ns2be1VRXM7G1nDZgPIigdqSf9xcp63PKyMrT0DMKD9bfhwaS8OPWhqVaQYszMrNYiJDKCus92UofDFo6f3/o2p3ccsYDDdo6fI7/4A3+R6C+sP69vaMaA2g/hpAEJCqEofm9iHWAL6zIDIiLgzppgkXXbzX0bmHU7oOG+AdzXaRCdbQ2oovhJB+uY9L4EVjDrW7T1vu52mOqZJT6gRWSE+IzcVGb4VrFtNHUOUhs8gHSXILjyhby7sxX0mSaUsz0qPIIos+mOoV4W8JcbmVXfC5WHH6IjQ6BhBn4rsz0lygzpL4bQCBio3DVNf7n/Q3e0D8fP++/5IL8ZhKWd1QrMyu3lqkpkUfsGhsLAigXHpGtooBfN9dWkStVjSOGGoLAo9gNydDazIqKyjtAVZ1KCQhFMuIqoFxWbVbQZsb+rD6JCmeXLGZPj+p3/hZu013f8jxjk2hvLSWF4n/SXCtJfZrJBK5F1fAe+uFDAchUKAFw8hg6VL8sVSH/5bAfOpxeR9EIRe4sTAilK0JJ9kjVil8C2jMY7VylW4MwbCmYpixjfh2Cpz8OOz/agqNGCzppMXC7qRKAhELbK66S/nIa5l4SUjGsotdjpL9e/2IGLBcRtdZL+cs5OfwmhVJeYtorsKsG+E6vSSdm4imwuu4H3P9+Hxm5euzGV1zuAEJZ79BSfxif7kji5YOo8xScUHEwMvizRkPXCePMYfvHBVrTp4zAzzA/mnBMs+L7CVHqWp1BMOd9Ckkn4MP2FHUhbdaZEf6mkoHZrWRqulDnoL6yh23uetrSiNJVasv16+JH+cmnnNor5t6CnlaSe82XwiST9xWcK6S+NpL/8+lNcvZ2BImM2LAoSXYK8qEl6Cx9/tAsXcsrRUlcJhTfLfHqrsffDz3E2oxC5N64hpZyi4eE+bCensOPgJeRkn8elpPNQsVY0NMDbrpHKdthYkoLfbd2NG6k3cOnKZTQrQkh6kOPW8U+x/dBFlBSmIDmlCB6RMXC3mbH/0+04nnITSZeuIKNRjtmc1InyKRECmKa/PNzbIVYaD0p/uXR4O97d+TUk+gsBByoZ3wFCGj77j3dx7Hox4pYsRoCbmqSYOlw8tA3v7jqCGmUkFsYZ4NLXgoyk/dh99Bry75xE8rV0eCcshp/rEO5cPoR/+sUu3ChVYuWyGLja2pF2eg8+3HsMhSX5uHr9JmuR4xHhL5CHo/c/1NuKzKS9+PfPj6JRGYX5wUoUXP8a735+GEWsK7x07To6NXyflJ24sGc7Dp5MtdNfiruRsGCa/jJqyW/+TcQVRT6CO/WxJz5u3ItC6ml/8NudyCnN5eKiCkqvGD5fcS4xVRpCfXEq9n7M+vobhci/dRV3SgfZV8uRc/ErfHnmBrLTSfjJLkPQTLYhNyVkg50ouHEC//7v+1HSTtDKonCCM5yk+OjD0V96GnB+n6C/XECOeh2pHRyvWEdVT8yVm6C/LJuPAEspJfuY5tzUgIJ60l8SZ2P5kmjoOZPzo5rN1bQ78I9fSIrLKqiqr+DnX1aheHYzO0GDdKzabCLSZEHYsnYDEtxq8f7HFykYHCLRXzTBs7Fp80ooqy/i/TOlCFKS/lLhjCWb1pP+osHpX33GkpEKtCSQPchJ7GRvwh3Q292HqHnrkRgXBqeqc/jN+UbOsFsg5wzWLSyWosfzEUpFp8CQAArVE2NWnYsTB08g+3YWwp8eYA1aH6UTSX9xD8SGhYuh4gB4vI16omPoL1W0SbUyBi9vWIMgGPHBVkrelQTAevMGPImD2rx+AfpLzuN31B8VdJgrNWqsf4lCDAbg5Lufo6yM9Jcof9D8U7DZ6S/9zmHYsP4xxMXbiS6qwQ5cOX8E7R6x+METSxDl7yZpJ5ffOIUCp3C8/BpJMNYCfLrtNAoqZmFxzCr8hFrFXU0VOPHZVirWdLCmlHoVUrKfE9zYCb7641BJ+Sj9xGc4eT0L8/wtuH6zEIuffgcLwlyRcvAz3MwqRdTqmbTH21hL+cny2yfxwZk0lG1YglAvSkpNb9+TBez0lzarGqF6Isc4GA8MyVij3suVBSfAYQb05HQxXEQfPeWneqkSJvYN4b6WfrEvnXMKQWbZQDILazqNGTi5/yjr2zkB9yCOsbMfsVF6ZLRT3o2T6g7WF+ddzEP8mrewPkGN2xePkCpTgGVEt/mz07RvAyyHycPlE9fhQdUsWz8lNk1VyLt5CyFLXsQLi3yI3DqFc+duYnn8Fix+5nU89oKM4iIXsPPLZBSQ/hI6TX/5ntoT2bZWggO85+CF1x6HtzfVieh1GHHf9jUjNzsPWZ2h+OnfPYehohs4QyTf7VqOT8ufRfRKOSpTz+HS5SyYuGBJNOjQTMWrdBK2uqkH7cxSKcdK1HGDD+jatdNf5qx5Hj5hAfjwZA+L24fpL2F6FF3NYYNtRHlWwzD9pQnNHQW4dSILubfs9JcX1y2Q4oYiFqVSUbhepZTEqFsoRD1EdBoDZ7CykDY6YTYioyIRJOgvvmmSKHxrpxoJi6LppqH0nfscGM5dh42gaE10HF0tUTAEqTF3USDymFfUyzAqw5WTvjnoLy+E2dBQlIazV2+ROEwNYV5DRZkZuWm3kZ95CxrfePzgtZexLFyB9PNnIYtehOc5CPtr6cOnJJqbTyjaqRd7Xd6JgZI8aJcuhBtXS2ITMcB+ivrHzprPVWoE6S8azPW5zSBoHzpIMZm5NIYruBAMKueSiHOTxeMt0MbOICUnkoBsO/2lmPQXEVqeGmFZxhl4KlsPRaAr3KHW8DlTzk1ua0bhbSrKhAdx4mBEUYcvYriq6CGtJjBuCeIJOfchTHmO52m0dvRA7hEJTWMFSipKkNEixws0KpkI9o3xZlcPA2Z4yuhCN6FQNcTCfQ0UdOn19zNOTVlAnc4N3p5yFFMkxCZ3o8vdl/1xB7qLXVkvKCQT5fe49IaPPf1jiiwg6C/hWEM0oAYmpHGmLzoqZ9I3Ihc+gXWk9tTWpEiatsx24EQ9HGvXr4WGpJhbYl/2FSLHQu9L70+lEWXGMuR1arBMw+fr4YPFqwXFxYayS3LhnJI8DwN9FP2gCL67uw56iiqYM9vZV3B2NrxZ22qQTRWj9uj12JLgghK2pV72R1aGjRLnzkLCTF84d1ThejrlOFmkL8I4ztS+bq+gKA3rmt2ow3lvrNRx7Omfk20BxjypZ9bTXogSYygGBqPYDvjgHYsHQr3FBEwRlEjMZiyctB3ILymAsbkPm2YForWmBIXGahj73PE0aVO27nrc4WKnZGAGnttiQFPNaP6N404c3ZHj39/yJztI+pUjE+Khd66F9qxRasyC/mKmYsgQ/SNqYrHUxGKZG1rRIegvz74EfaAB7gTq7uaMLZVC4VEJcbhMAftDHARlxnTGF5l0xNYn5p2iAxb0Fw8SGkSnOMj6UpVIBhGfU07QjTqW/JiDD+u+eD4RQ3SjrqpEj+ABFKIhCz+NeHOmYqPzXaEm/UXBGCbP6aTWoKehBi1ckXvOfhKvzaMCk78aRQTHnjh3CRafdtwoV9DVNBttVTnU1u2gMk8vWs3drHMiWo3H6qMNO5pbJYH6UfqLC2fIY+gvTNqSbEWauzvjgsIOg0wkUtBQYgbm7uHGWkv+g65nuSDiDPv7HW1qck1D+otHINZsIvOPGrnXv9yJQz6P4a2NBtRR+7elrQu9Pa0wXr2ElILlpC8MIjSShAZe/BCVbFz5bG28h/7uBlw7dRjnz18n7d5VUo8Rnae9p+KdiJsZ6ELxncs4l9eNZa/N5ITCEzNi5Di36+e45eHElbgJnus3M/lNtCBmcRuz6NotxpxlLyKMLr3p7fu0gKC/KOHBtuqgv0jvLeVEVToPuLvyXZeCW/Z32dmxL9v56OvNlWcr8VaH9uPilQy0ytmBkmvqTHiBytODVCg7xYX5QMQ3BiBsfgD2f/0hSq66Mn+hBoh8kW1DvEnchrpRnpOGSykmrHhjEfTdRlj4fnZQM9qF8Vt/D5Zs8H/OxB8q2Z4kkX1+zVySg6ukvwQL+gu9LKKlTW/fhwXkzK9IIDyF/WlDIQ6cuQSP5c/jH99YQb1kPhUuSDQ6ohxDmFnOJmVjH6qkqLeNlKDmugKc2rcfF67lwRaykDS/LpJgbiKDEPe4jW+Q0GVCOSf8XVzQ6Kiq5dgecCAVX+dgKjowxu1E8xbkBkF/ySoT9Je1pL/MQSPpL+9epWxXZxAWLVsO/wB/qPsjkEc9VQtFyr2WPI5nVWRGWtjp62cglqtaT7VKaoBSnhzdb1V1TbDQZQpND8ykvwjUmPOgkNBr5gzSBiWD/xL9hSuT2tox9Bf+vs+nj41cXOvkbzYW+Xa11MLcRzZiwhK8SHdC+39upRReC2bFzcfswCD46OXk2xkJBG7AjZvZqKb62MXWapSkEvJd5oGESAPabpdjlaC/bFpI+ks4fr01T6K/RAzTX2yC/lLTjO5h+kujuQeBnEk7kf5ikugvzCjsbkOrWL0yO7qGiRKdjCWLGX4rbTKg65/CmTJxemofLF/7NBukoL+44V+23UTJXK4Y3f2x8snV2LAoDOWaNvzqeCn8QurRqG4h+HwAWuqhtnWSKcrBVOHqj9U/+AnmrVyLiwc/RAptt2JmBMK8lcyOZpYuiQy1eTfw5eFrCF36KlbP54qWsm0bf/Q/EC9kwLrqkXJ8D9rd2JnKBtFem4fje4/C5LsQP103T0pymvwWMn2GP2QB0YdwfWkfmO55cW0cSEUszTEBlPaVOfa1f+qqD8VT7/xXLF2dg5NfMv7OznBZjIHygdyb3xcZxWJPpbsfVvzgrxC6uBaWTjNS3C8gA3bilMjitFKHtTL3CkpqKuCRqkdLeQmy2sLga42XSDKNbd3MBGdyY5cFFvIpRTJKm6kIZ0h/KbUmSPQX32n6yx963JP2eyFg4x0Yg/XPRmGwS7BpD+LI7RswPbsQGj1d96wS6CYlrI7SsxxCyNKmR5VhBF93LbwMkXj5//wfWL4qGV+fOIez5DovUucj1ViJGZnJuMMw3Z1qBeKXxOPpJ2baJ/G8k4cYSO12EB30CP2FI6vSqZ2rKzPxXu3808HVhScslRew/7aW7taViCT9paBbjkS9Bi0FV1Dc5oHw8GhYS3OgDqI+LOkMrTWV6HVmWYZ/BEykv2TdyUC/hvSXLhnWuDFLL0KNs0UFCPBjwknVTVjoovFippxLfiqy87IoPqzEzZwGBKwhFUTpePUm7bnZDzzYh9rsr7EzVUn6ywrSXxpRy8wvA3VyLx7ZA4+4ZVg4IxjGrCIERM7ApheIlaOWfVeXGee7ytAeF0ctWDf08jl3MuOshfQTWxMJBDauIrlGb6qupFvSbpOak4XIDmWyFkh/sSoRofdBaJiCiTs5CNKrYC0R9BdveAV5w+l2Gu7kB0HeyuSv3Ca6ySmWP6X0lyomX3G1TgWiRmZOajRC8zYULoxbllXWoMJfjmoSPwzxidS1lOF2VjayI/Tw7MpCzpA3XvBUopEdmY2zP6VYqQprcCUyQPdMGWkMfT5hcO81YutvPkOeeibejg9BfztFyl20jJ0p4e3jxwzPSs4i3fBEdCDk1hoc+vw9HMxW4Yc/iYJioJ1AXxVdvI7Y2CS3k+nDj2MB9iHURK1n7LKG+tNmVSXqGujON3gwS7ZJkrKsJeWluqoW4d4a6OmmFftW80+TayVMDeFw9nBBdWk5ZAQ1y8jo5dSS7cSFbtw+0j/qOKGsRWuTirrPJg6s/mxH9Nb4+BD4XYOBZmDFhli4MvmvpigbNVZPhC56CX8R2UNkYxeKetvQqGXWbrAnqivJFGaYJmTIh+6+TFJDHoOipx7nv/4M+64046m31kM3ZEFLhyuh39P0l3Ee9qR/JDS0zWwbHQwUyHtIlGKs3EsfAwUT0soEkNtZJyUKdVTexq3sIOaUZKKkDliyjFKybEOuvj5wkryg9DrQ8xo9dx1+4jGfpUv9yKXny9Qnhy/la4X3zzG6PPRAKolLh3jAla5UORFZ69YvJf3lDP4XYdYqj1Bs/ME6rIykcPCBfdj/4WUMyFwQveJ5LF+QCF1LHy5sP46kY8fg5BmOZ15l4gklutI+/RzmmBVYsXQp1kaV4MReZu2R/jJvw9uYPzsOHtHPIO+jL7Dz58l0fxqw+Q3SX+aHQUf6y54Te5B8qBdhTPrZTI4lFyaMxU76s6OLmWUtUUsQfPUL7H73AlmocsQsew7LFiyB2akZO7kiOn5gCFomxvzwzWVYOTuU4u2gH5+raFIkLCxjMQQY4L15PQpIf/n11WMS/WXJltcQQfpL2u7tsMzbiGVzl+HJnB049vkv0EtM0KItPyU1Jpb0l2eRQ/rLxz9Lou8qFM++8xTmzGEcp4mZsEe24izFt2PocpofFwktvRtTR3+5ja0HmbEs0V8M2ECiy8LEKLQ6rUDGtpP4xcVuwmoS8epbjyPRgyvpmk9x4MMUuqeVeGzzTzCDJSsVyfux/+wtNHbSPe3BRKtn50M72IzjB49Bs+oZRDuVoriBpQ8sc9j+fgkCgufi1dfXoena1zh1Mxed9HHMW/sanpgRisGGVKTk0aNRa8XxHe8hjcLUW97+W6yZE/nws8rJb2Z/omew018OfL4byUTjNTl3YjvdZz/+0Xp0Mot9574zKCek4cCu7ejqfxtPhPTh4LY9JMWUoMX5S2zjyvCNHy5HReZpHLtWyIkR5f5C5uKtVbPhxKTIpAPbJIqLucGGnTva8eIrL0FRcR77z2QwsUmN2cuewVNLI+HcRWD32YMoD34FP12/GBGzOBh3NcKX+QtoicCCBQaEKBqRt+0r/NvZIegNiXj5p/OkyVk2KxKa6kl/2e+gv7w9TX/5nlqrjSV3BWmnset0JgXrbfAJnYdX3l4P145ynDxzDL2z3sKWxIVYmr8bv/v5v0LBvI0lG17GHF8bUo4dxOnbVYQnyGFIWI63Hp9DXrEok2NMwNbJ2LeafdAAZpBry250JLTwkKL1nElaCZdu74NWp2Vtn5zJMKQ4mOqYJNLNC/SGIdAPOtJfOprrOctsttNf6Ob01bOmR6K/1KKxtRty7htE2oOSqelJn2zDUOIqLFs5H0pLg0QPsQ7JiWsLJt1eI9FfWngOUzPpDyoPiTDjzoSc/q421JA8w0QrePoHwd9TxAeZ/ceyBiHQ7uXlBTc3LgMnaZNKgsz1nCEToE36iw9jwj5ceQ/1tpOdWE8CRT80HiwHYumGq/DVcxMuYQuVMYYYE9UyOUI2QBc2762hlfQXEjCCDAFQDrYh6dOddvrLQtJfmFRTzVKaAZkSASEkX+goPTYk6iNr6d7tYC2lp2QTncoF1s5m7mtiB+REagFpNJ6smRRxRxYSTwn9heevq6d7nkQXNYku/mwPIsFnwEo+K6+r1ULpNHdfTiKoJkQqTHujCSZSfQZdXEn/IBGHy+du1gc2mFvQ3ecEV644Ar3VqMtOwvv7q/H8T4hfC1GhmfcthD0GGTuVkyTkQ0pIPycpjU0W2lHNOrJgeDKu3s9QQB2fj9Aslorq6Tr2JM3BU6uWhCum6S8P93I8KP1FEFrqSdfoEWoqdLe5kIjkx2c4yNq9RnKNRUicSQ9w8/KFXjkEM2tLxb4iRirnvr6s1+5nrkVjczusEiXIB4GMfTtzRdpKUkwbJ5IiXOTECa+ntw9kPc2op9dsSK6Br38AfNyd6eG4hO17rmD523+NJ2cYGJcVtqDASk8Xa1DJRtXKMcR2a6ph/9Y1yHfZh/2bF5zoHTEL+gvfKdHXiEm1O2lDPszvmKa/jN+eJpP+IqpH2lsa2Y+0oZ+LDXdPXwT6qZF39RTpLwXY+Bd/g4VBWva7JtQ1tsFJtLXAQLgphtBmNsHM9jbABDI9wSn+PiSIjYQHmajZ0834qI2VJ6SQsX04ROsfciAd30gP86mgv1QZ60h/8YUXB8KRe3iIg4ps10ed/lJlNLHMIxCeHJgd7oSHMAkTdh5d+gtTxFFdmMlaWX9m3VF8gRO1idgck4tp+suDW/ORpb8M9sBUWSSV6S2YF0v040M760aMOE1/GTHFyF++H/pLCao6daT7RE08/UUUxk78JgL7YtkrppGjm6QDK+aFd318775OVOk3SIkFA7w2sev43xv+nAe7+3D2hIKx5xYDqfgjBg9BHRj7u9Grm7i/jX+9997n7z+fdIyRe3NhIo6diNM/QTYRthA2EfYQsmVTYZPxziHu897P/6D9+MA9QxJIf1FQPUbIIt7VAu4y7N12vOtXd/1D7OcgUoifIrHl3uu66wvT//iGBcbaUPQrwn5TbkMpoejePuYblyp9cG/bcPel+9aXVQBMSLNaf1886Nu/yw6biJWqsIn4+ee+3WuTiRdkGLaw1BaGxyEmIPkGUWXPidKog+wzBu19htQG7h2TvmUbEt919KN/lPSXiW5o4mV+1OkvE20TMYg+yvQXMVMTmZt8tBO2CZtM018ezpxioBDtSujKTloH+XCXeP9vT0KbEicTna0YRB9Jm9zfWg/1G+H9EX+ETeyD2UMd7tt9eRKer7gH8WwnhP5y76g+1jCjM1L7LM5+x6Md4Pj7jrXLmO+NrNAcq9Tho43pTUePN3oO8XI76C9arXZyZ8nSwxq+/pHrHXMP0q+Gr23cfe++NzFSfGOs+Bbf+0N2Fy+3sImIGwvpxNH9x9p+Yv4++kzE8Uafy8jnY+5x5DNp17H3PmrD8a91/N+PHG/MOeyDsOPe7Ncj9hMrUUHE8WE2pxAKGf88ju9N/7zXAsKGYhA1m82Mb/pJiL7vbEOpbY+2Ecc5xLHHHmvkuYodxjzbkc/HfCZ2cXw+7jHu2Vfs79jG+574nePzsefmp6JpSZvjPGI/B/1F2GRatN5uO+HaFfHFgICASZtwjTwj8UTu+4zFMxu/vQ0/yJH+13G8sc/W4aKeIPoLA+ys53Oo5Q8N9rNTGmDqsEpK9pEuiP8Z6LMy+CvILKrR2CfrUCX6C7NcFUxb/+ZmI03FypA/i2ZVYxRo7vO9AabR9w+yDOc+9JdJp3qw4luQWYZEMfld9JdBKRFG3J8Ti8QluSruaxWkmLv25TNnApLdJgqp5OObNhk+BzOgR8/Bvfi9XlJjnOSKu4g4/bT7ICXXFGNsIgZSQS2wK0upvnmKCf3E3lDZlqUOyNHZ8EYlUs9IITzPyT3Ff0S7v3tf/k5MiGSsFxZB/vG2IeE2u/f345xj7EtlY02Zo2RR2GPqbDLeHfxpfCZsKFYak0l/cXRm32gn/EDIADo7HuqwSUV9KKvVR5619DHbhngv7iUDjX0K45OIxDvK93mYWjS2OYo2KKphxx5TeDocNpkeSO3WFSs5oVE7mav0u9oIO42xz+muZ8wndu9vBZGLvQ2f22jtsr0tCHLX6LfF5Fs824eMqA+hmwLSOfmkv4RRKZ+FkdbOBhTmFaBC0F/8QzFrRgx8dFToMVG2q8CIpp4hUhxmIj4yCBpnK8oLslEkKC6sL5yVGI8AbyYYOWZ1FCQ3VxYgt4jpyDItoinDFRFI1X0W65cX5KCosgHOWhJS+D0/6lr2NFchO1fQXwBDNM9B0oOG2bGjBmXvPInbYF8Xa9nyUVhRz/INQX+ZiVASIwbba0nIKScBZYAKLgoERCUgyEvF+sg87tvAffWI472F+Olho1B2SX4eymtbeW/eiCXxJoQZiSLTVmy2AdbbVeST/lIj0V9iZ81CGH8vIzWmNDcLpTUtUOgNSEyMY9agChZzBbJySH+xuiA4ZhbiKIw/1fSXciPJN93Up2TDdGMNaWiAF2UD21BWWkqIwQDc/SIQGeoLhY0qIkZhJxEbt+8bxn2dWbtVU12OxpYukhrcKQ0Zzuxt15F2MiSyJmtZK2pitjQ1V6MjI+FNcXNrJ+vGSo1oJf1Fz5rkCJ5DBWqoVhhR39HFdjFEBSx/iQiiVQqVHHv7cPycxKbyJ3toh+0cP7/tjQr6SzeFEEwkugxSgCOCNaQsA2V2N7PY6TkR9BdDRJQEtOil9GUZC+S7OVkXk1CvgHAE++hgba/n52xrjJl7BoYiIsTAeKegDJEoVNGIQdYPBlFuNNBDiR5mgpeXVaOzj2UsfiEIDwkcyaQX12xjtm+buYYSc6QsMYMzIDSa7VborDIL2FSDsqoG9JEqIn1OCISNfVJ9TRXBFNTWdnVHWAz7PZYEikmjwxaOn9/WJn/K+zls4fg5sfdK+kubmbKkNSP0Fz9DGII4toxO4tne2D/UUPBnwMUDUSxnUbCPHexne6sjHarKTMqLnQpjcHdhW6iDkRQrK4lRgSGUXGVbcBnzbB+K/tLWYMT5A+/jk8MVCOegIegvd45tx76LhehzGkC6oL8ofKgcw5q9nXuRVcs09u58XEquYUMPYo1jOj7ZcwpNLFuoybxGzVUVgsNC4K4SK1MSVEy5Ev2lmIpGnbWZuFjUIdFfhgT9Ze9pNPN7VRlXUTKW/lLIBm6pwcWzhdAFhUiNny15augv5Xb6i5np0Zbym7hU2C/RX7qLBf3lAqrqW9HZ2gYVU+NdOinKvuMAmnpBUgypJcUkxQT5oOnWYXx2KAM2hQwNuZdxqdSGqMhQlmdQRYFzpLaqDGz9ZC+zz/hCl6fjstFKPJgvdWNJfyFhxkJvgDEtGeXUifRzG7LTX8rb2MmQ/nLOCO8INih2OkImq72jA8LVraB82uRsFAgn/eWjX3+GFBavF5fnoFtJjBp1b28f/4xklguEENwgqSMf7mFR0NlM+IykmJRM7lvGfVWhFLR3hynrKH7+8T7irGoowXaZ9+aN+HADNEpODVn2U1+UjN9+sAsp+SVIuZaMEmbmRfopkXliK3YcvojS4uukvxRCT9kwL3kPLnz6Pg5du4XCwizU9ugoaB4q4bnEinea/vJwLUF4Oh6I/tJehwuHd+A3Ow6jShmJRRL9pQ/Vd5Lx6X/8xk5/WbYEgW5y1JWk471fbUUGcYPFFcUAi+3D3IeQe2kXth84its3kkh0yYRnLMvnOkqw571PcC4tH7kp15FaNYSYcB3Krh/G3mPXUZB1Elepi+0dv4iYLXtJA4c+tFL96uDHH+LolUKUkkBzPaMVEbOplGPOJaFoK87dNHJF1Ywe10BEss67IjMJH3x8EBnEuFmaTBSTiCGWSyMV7TtWX6L0TqxepjciyehlnEz6S8Gty6S/7EJeWT4XF9Xsc2PH0F94fpZKXabmwP/69Vcob/XCyiWRLL/rR2VOMnZ8uBPX7lTCwrJFm1sQ3K3lOLLzdzhyJR1F2cnI4DgUNnMmvDVy6T4mgP6yH18cJt5KNUp/aaBupbtPIh5bOh/+Fq4GKEJfmlWOgiEvzJk9H7MC+uFd2k1RgG7cOZcC16DZ2LhlFVxrLuK3Z6pRSvpLyJwAif5SQ9JJuTNlv9YN019+l4SSonzSX26wiH/uCP3lvdNGZAj6S5UcyyT6i5b0l09YRkP6y4wQeE+BaL2gv1i7+yl6sAmJsSGQVZ7Hb8+b0dDYChfKSrmHx5D+soB8Ui/GBXxgpXqT2Hc297VVnMNvkxoZn2uGM7V2Zz+xFo8vjoO1wAX/+yvWOpk7KVShgY1sRkF/qXGlqMNG0l9kpL98ehXFxYHovXkT3nFLsWX9fPQXJ+EjouyCnEh/qdVg08ubMddgw6l3t3IWXoOE6IAppb8MuoRh48aFiCddw8OTNXwdxUi5kY9FT72DRdEeuH7gIyqMFMNvjhsnEOHYtHYh4ihi7072qMalGwVVWWh3T8Rz6xbCWnweR1ke1dzZA1+djvJenShjR9fgNQ/vPL0Uzo1p+ODQddw2WJCZWoxlz/2Y9BcNrh34GGk5JQhRBVGgQ40nt2xEQlQ4tZy94es22a7t6a7z91uAIQnWanb2axDuRXIHB2M7/cVKCb9O6Mj8tGZZGC4S7lmGNRgfkusS8NxLK1ln7c260EB6WYZIf3kGfzv3eXRW5ODQ1v1IzyyATUdFtM5I/OXfbIBLfRa+2n8ZRfXzMGf+U/ibeU4wF9/CqQPHUN/WI4WdKLvKZXAnygtzcK3OB2/99Q/g1V6C43vO4erNTPh3piFPFofX316K0EDqAHNSbG0pRtql81DGr8JLK2ayfp1Ka8KLNOxZ+/33Pv3bibcAE96srB/1nYcX3iD9xcvjbvoLJ0qdFnqkGAKMifJAU5cIHRJy0VqF9PRUVLvPxzvPPQY/H09+T4uSa0Qx1lgpHfj/wNN8C2eTruFGgQnR3hEjK9wHdO0K+osb5q1/kZBqA94/3jVCf/EKc0dBcjbjlPUozzRhfjil1yhMbm2rxvWjRlxnXCF8+TokalUUb1AjjjU9kcQkqd0SEZSUASe6csQmRMn7WPwq0V8iw2FQe2CubyoUXIG0WFwRv4jfIz1E4TYbhrPXqavXBG10HFdvdN0Y1Jgj0V+Y/CBIJ1MwkI7QX8LJuitIxenkNM5mnrTTX8qbSH/JQF4G6S8+8Xjh1ZewauYyvBg1hDpqxJ5OTpf29aTKTlT0m5inptugvxU3Glso+u5DAXu7fN3QIPVzqZsbO2sBwsPCSH8h5caHlBm6oTp7tJgZF0X3ZTBdxbMReP4mbF2tcItLoJ3o+iL9Zc7CABTzUFzITyn9Bb2lMJZRkJwx2li1Hhp6CPpZ4K6gGIIrtZU9qUFc2NlFkQQ1L84Io9EdCqUCUdTp9fRSU/9yBgaP3sTVSx0YKC/F3C1rKHQxjD3jsQYp0zTE+LozY++Cb6jqrENXdxQL5B3nUEvnKG5noTWL+J1kXTAWGylb6IzoKHZ6XmTWTm/fowVcoKPU45r1qynqVodUBqGEy8+ZIiVRCwX9RYXaKkF/EZcoABXM+OwslPCBgyy4V+oo0kAikE9YPHw5eNV1m1gn6EHRF9Zc9w23NbkrXEkB8nDppLiCDRovH/RXl6CkuBg5ghRDURNpEBWnGOwlhqsHsoAZSIiJhbdVgdI5NwhEKEFMcwagXUFlrCJ0tHkjYT7f1eYGlGTXQLW0E9XFBWj3DsJsvS+0zOmY3r4PC4g20sNQAcNsxUEID42ChvrKjBaNbHqfIKxct45CNRdxqpRxUP6ug2I6taTAyNieasrz0dRswOx58aTHsElQEEjJsJwbxe7VKhtFQhhmYr6PY3vAgZQJGoxFRcTFwl1WDa2Lg/5iQQPJ9QI9ItT1tYpBNFGRplPbRu1YGZauexqJ3k348mQybgew06SqiLeguHAW6KC4jInj8uZIf2GsSy7pHgr6CxOaeOUyJfFIdHUKuIvje0L30I1IJAV1WDm9oNC5oL/wQ76QU7LxSciJfXIT9Bc+MblWh17GUlosnfCauwZvLPCiX12NwuSDOJd8CzEhT2FGoBz1vCNp33pqgTKO6BoaSIWWBtxMOkz3YyOeeHYTYoL10i2IO3FiTEjYxIX3NtTPZKERmwgizjfpL4KIM0J/ETYZjpSPaVOTaB5BfzFg7aY1ZIiacfPQbhy6sgh/9eoCJM5wxZkvfoXbHs7EzNXCY81aorSCsG7Taq7AzbhxaBe+vLwU//iXGxnDCkVY/1GknM2jkpU/4nUUvR+e7juTemOIWYDBK6fw/ge5UDdVsqjeFZu9QjEnTonze36BdL2gv9TBe8MWqHV6zFi1Bk5Uxim9cRAnDwfix//tL7Eoli/a9PY9WUDG9kwMGtXOpAQl0dDFe8vEMaVWDx0nki5ScEukojnDMyASG+jFamkx4+yuFByNW4//8dMNdPsy9tnMyfqV86jRc8U6JwZuzVaE9O7F+/9RCs1QMWoaBhDhZEN3SyWSDh+USDHtijD0i6Q/0TGKDobnVqgVCA71h4pxM5GtqdY4o7e+Fw2V1HEO6oS1V4GS9Ku4lNuMl+bZUNvWD10XB+COWoLEySNtV+DHmyiDKsIP09uFgVarAABAAElEQVQUW4DPjnrm69Zz0WUuwZdnL0O/9Dn845uPUxVLuBzY3hjO0goyFgPxMmlAFDxpZpxXtaArhLg8rlJvp6TgZtnTeH1JMPw1R7Hzl/8XdGhCYas7Vs3nWDSmE33AgVTYhYOpOBCTBES7FzV9DvrL6rXr8JSgv8Qq8asLTVwFNUEVPBMzH1uI5UFdhOpeI/3FDKdeO8Wlh7NGGf3RLZR26pNeGHF8fiboL7VNpHf0cRbYA0E68WJygKC/1FNWr4fS/aoe8T1mCXOAqa0RMnxc0co0EulkqukvluYa0l808EtYjBd99Gj7+WdoZALMzOg5hMMK+osCPv1FOHW4FRUluUy2ioT/DJJiuG/rz7fCRFFuM/vzjOPbcTytB0ufeQObViRwcBap/8ImwiqC/tJE+ks/9OQfjtJfqimZyJU/C40Hu1vRwqxCQX+prjIR/2QvjpfoL24UGxjTAOxHnZz/ioxYudobS9duIRjZQvqLDv+0lbg8yxI8+db/RMITVejoFGSW3WhmJ6jS+nPfp5hg0onCYC3+edttJlVF0CBnoVr2Av5txQw055zHoVPpkvs8MVSsJOUITlyH//1/x6GxrRN1haQ8nG6EzjMYT7zzPzG3shodFhOuHduNNp0Lr0eH+GVrEMtEtppCA7b+5y4YqxuQGO0H+7p/cmwxfdQ/bAExYDlx2Smy/8dm3UrdjPidEMlgT6P1DMGqzUFMzGsmseMwfnviJqrbV8BD1obr5w/hfL4Zm958BzGBZN+S8PN3/xqAOkoHNhrTcP5YOhMQXaChtvezP/l7LFuTjRMHd+JCSj6WRvtDT5boIN+hPtKVavnuWOnBULESoKurGwZvV3pHAhCyej02rgxEId/nnacq0ZJA6dLAcKzcsB5LAwag6z+AY+ml6FwVx4F0Omzwh5/8xO5hszkzAS0aa5+OYk6OoL8cwNeZqTA9v5ghRQXbEBcvYhUmzZo4XEgVJ/ZFml8w82rWbcLmBWq44iTOZpdD/fKLeP3v/42Ski1oKE+F6lomdIIRPaYffYiB1H7zwgUzSn9h2rszaRpN1Jtl9p2ZyTUqCgIHhsTDmMOMVmM+iiksXtirwWOMfwXFaHGBMU8/H28oa2+ik5murlyhNldVoJesPzVnnvWC/kIgdh/pL9mkiKylLmtEJFczpL/4+3hBXnMTXewcvekeluffJP0lE/p+JW7kNMAwTH8ZHoMm9mndezS6V2uzj5D+osCqJ5Yj0MWEGqppGKgdfPHoHuZDLJXoL2UU5fb0jUZ3xRlsveGLxx9fCn8nE2rZjcc49yHn7A58sv82Zm18GbGBaokC48Kkha7GRurxakjEiUTNiULcCfVEk81Of4mkG0kfrkIS6S9iVm4tTUOfGyUWKfjuknETd3KD6H7qQ2peE6KjFVNGfxFlPG2mSjR0cYbvNAhTbT31hH2Y2MPG3M+sXHdPajCXwdipxdJwziCYAW5solYuXXd13FfHtqOjN6KujhShMLXk5RikLfr6OfPvakdpZgkGvcIRxOQlF+oSu6s5WLfUIIyx6GDSGRg7gLveE+1NpSiz6PBEpD8U/WYU1vXAlVSgepJGTJygzuf1DC9w732q0/+eEgtwEs5naqquRFVNDVF65ag2hUMd7IUh0jsqKitRY25EFSdFwVp2gGwnLQwNKEnuaaxvgSeZt3xDkHnpMN7ddQaz1r+NUCbamanZ7EHB+SFqL3voelBGSHy/YSHfERldsEWQuTF7nAN3PzteKJXUDe+g/rQRjYPU6GX/01d3B+lZjKN35iGjZACPPRMDJ2UZkpnUVhE8QGxjM+TuIdRxNSCcPuWCggoE9DK008Y8DyZLuTITfHqbegsI+ktjbTXabfTc9ZjoubDA2yMWStJfjPk1sLhGY2awhgms1XzetWhqoIQskWr+7I+8AvTIyi1CpZcn6hs7oHJPgJxhRplMTU+gFgUkIrQ7+2FeFIEg0gzPfn8POZByxcEOLDDMk7FAQX/xxrqNy5mNeQ7/fO4o1B5h2PzDjViV6AG3ga9IP/kAJwnbjVryPBbOTgShaSj5aC/2/OpnEsVly5urEc3M3/RPt5FluhIrl5P+EkP6y77f4jjpL/M3vUP6C2Hi0TLSX/Zi9y+vcoANwlNv/gjzBP2F3Mk9x75A8uFeJh5swNOzuJrjHXJxNumbEKoOiFmGsOt7sfe9ixL9JXb5C6TcLEGTUwt2HP8CJw8OQReYgFfeXIm5Hs1o2f0F9vwmCf10R8eveAGzw91x41IV4d4mpF/4EqXpZ+EVvwY/Ju2k6sh+WOZuwvJ5y7AmdAdObv8VegadseTpv8DsmbFQBz5H+ssX+PQ/L9DHHYbn33kGc+f4cgAl/eXY5zjHMpPYRU9hXnyURH9hGdekbzIOpC0VGdgm6C/1XXTTBWHTD19DvK8KGYc/x9EUAs3Zic1b9ypWzQpHT9F5bD2QhCqSYpTaYGx85TUsmBkGU9cK5B86g/+4coAueyVWPPUT+MotuLDrGNzWPg9n/3bs3HUYxtp2JnU9hlffWIVAzQBSvtiGozfzYOGqdf7616VzDJkzsXPrQXbYjaw1VmLO+tewLCEUKr4U9HtMb9+LBZiNXleIg9v3Ijm7FM3OXdjJFaCD/rJn/1lUmsz4as8utG15DtGDOdh3+iaa6U5184nFS29thr+ciKvcTIqVtyPjwhFU3ElD3LLNeGGRFqd2HCRDso3leLPw3JvPINxdhuuXTuHra0Vo75HBI2wB3npiFpwJkL9xhm0j7BW8PGcBNsYXYN+H/8nBU4dZT7yIJxbMRrdXN7I/3o9f/cwKrV88nnljCWLjmQi4ch5+t3cbUgmkD5y5HD96YgZhHcKNOL1NtQUE/aUo/Qx2nibdhyQg3/D5eIUkIXW7kdUcx2Cd82P4y2qR9MV2nE8pRUOHM3bs6sYrr/8Qc+YtQDarKf6/FDDmPh8/+NFCyFoqcGLX57hSXA+VTzDWvfA2+dIspeGNORZpDylaz9UoM0mbOvqg0Wqk4LqYDdTXk+rB2Z9S68kMVT9SYVzQ204KiInEBjI6vSm+7sN4iDN1UtsaSPtoEhQXPTPw/Lku62B5wnaJ/rKUjVNlMaOWL5HVJoefgZQON9JfbP1oZW1ZPekvMn4vkN8TRJF+rlLqSE6R6C9+gfAVPnBOG0RZw1TRXzqaGiUXgER/IVVC0GqGONM1mYhLI/3FVU9iBEknameSBrhybxCkE5JivKnw4cF06o4murJJXx9gYpFw3bswOUev7MW1Xf8/e+8BHdd5pmm+AAo5F1CFnHNmJsFMSpRIilSgZGsty3a3Ja/dPTM9vedM95kze3a3T+90t+c4tC0rU4ESxSAmMecgkmBEIpFjIeecCoVChX2/WygSpKhekQQgS8a1qQIKVTd8979/+ML77ITr0s3IXpRC+ksHbcJ6O0dXxSZ+Cv2F9XIT1BgnD3+FuuNF4W0j6RnNbe3EAjkigDNnLakFIgYhKfkzQX8xDPVxZtfDVPJxuHn6kbIQpODjBjqbGTcd4UqAZBa2Bz9OxIzDfWjjZ0cIeFc+G6JlZ6RigtUQOjs6MDBkYAIKaR1MDuoqu4i397bgxV+8gPnRXgpJhuWnjHtoyan1hwvbVh/jzlJ76sBjaEJCaV9XxW3cwnsxzBiIs7sHNFq+z8Q3iShIcfUs/eXxur1Hp7+McHXZxYxdznrZO6nIn9VoSX9hzWh3PxM7eFoODN94+qjpcmPdMPuMUSYNefH3oGAN7/c4nx22EVKGpATHzBJ5iYcHkhnYw/s9aCCtxS8AwcyYl7YhNKouEoOMTCLxYkJfqNadGfEXsW3HJawk/WVlkhZG1iK2MuHPQjZyIJ/lQNJcLEY9cWnt6CG1yE1oRtyfB70mY/pBejjaMczZmI+a1JBAUkOUJYswh3lt9CiFh5MLLDkcs5tSZtbb24sIEr2muiRIaoAHe7vRzjZiYjKaD+kvwRpXlJD+sv90BZ755X/GvCAX9r8dGOICTcKSTuwjArUM71gMXKF2KPXtXiRNBQcJ3WeEtaXMYRkhwcqPfZg2mKV3tknSnzX9pUnHOizikgLURK1NQaMTZRGhvwQGBsKbJRPftc1CQYLG2nbSX4JZrjFLf6EPjvHNQjSYgpGeJHXHU5PQYZ9czNJfHv0J+W7TX6pQ0emCBXMSlAnco1vh3m/O0l/utYf8ZpfWi4yMVGLfX/3EFL9jGkQ9BWAah32waE6skqQ5FUeQunORFVVJYeyf1WZ1RGBYqJJwINKAU7HJilQGU1lx/Nld7ze4QCttookIVVaSU2UTmbWLTcQeUz0j/AaX9Fgfkbi8fwRd/EwcUHElMkXNRFml221i19V8rBP9C/yytCe7DeX1O7NxFeyjjcF8rQouXM/+x/SXh7sqsYn0QfbXh/v29/PT0heLTWTiNRP9j5X5KoFh8VCzEsSBojX/IdznIUwu1yHt/C+K/iJulVnXiq2VyM0XN5wrkyy+k/qf9MUyLnEnRvEQbf9rP2q3iegPz8TD/bUn8h3+g71znE4N1ekzj5S62HShp/IYMmmVQfS7aZOptMTdfYn3R/6JTWZs0ip9BnsMewXE3bN59J/kGuTeOrAztcdLH31vX/mmNEi+eV9HZzfYw+orft33bO/fZxjFWHLou5clD3dbWxvr1PwUSbyvnO4Uv/Hg852wiWKWu+f24M9KSrYtlD3pMu45ywd/z36Me23yoM9KA5hMf7ln5zP5i+0yZ/KIX3ssmV1Opr987Qdn//C1Frif/vK1H/wL+oOd/hLM0Mx3ctI6DfdKZCQlvig2+S5PWu0uatVU0FBMTBCQGq879BfWg45RUFrlQjGFSXUF42MGynA5KMSRe2sZSQFhjoHUjinjxz03ji5Z1nRZHO6jvzAj1ECJHgfCnd1E0WFiMxkNTCCYOMbEe7LSkJslq6+puF77sR74yqpumaFYhcxyR9lE1HfoXubfpA5OZae/MIVijNdm5bXd/SwnAbw2hf5y37XdPZ587/5j8K8TNnFkBrErEyDs2zhtIpJrk6k78kCLTWaG/iJnImB1qgo5CoHDHvlm2QPfc6KkjP0d5ZOc+FjZEO7WEk5MDCYmZg7STuwXN+n1a+kvrAu8/xh2esdk8ozYY2ZtMunkv0c/ig1lpfGo3h+ZPN7fD8jE2D4hvGMq1rCbeG9FpIRNwrbxc2Ym6sn9nrzd3zZkf8px5EPSmPhP1iv3bzIJfzBxiN+/7xu2NuXI5/ve/dj7H3nWZgdSm4VlIm9vJ9PFrbXdY1m08M6yz7n3rtjOQ9qFA9vrPX9jPy3tSjWpDSkLM+5KuetsnPa2KJNvuY7HLH8R+ksLbpf2IjyGMnSkvxgG21FeWkYKwiA8qUiTwdIMrY8LBlpqUVJB+ovejJC4DOqcRsBH0sOZgdvfVMX6PxfExESxwPqugLqVcoCd9UJ/aWCVGOkvFAqOY5G1k3kEutIiVJH+4ugVisysVAQHeELfXT9Bf3FAaHw6UuNC4cXsKsUItJv91WbCqf+v2TiMhupSVNZN0F/SSWYJ9iP9hXqfpToMMqNQ5eyK0IQ0RGk8ed1lKK4koUDlg2ShuDDblKmrqCkrQk1TN1Q+waS4yLVR7HriTltNrLfTlaKkuoX0CR8kZWYq33MwDpH+couakKS/+FLaijbR+rlhqEPHOlKhv1C4gPSXlJhgJcvQbgv769RbY2KP7K1Gh3rQWKdjdjczd9XhSOJ9cRY90+pKdPSPwSMwjMSWSNaMWtDbIgSNHpaikKwRG0uBfS9YhjtQRQqHCHCI2lUQBe6FEmSfKwj9pYN0mDv0l/h4CvZ7wMCM5QYdC+aZzuvGWtu42EilzY30tFBerpHkDwdoIhMQG84sau7Mbgv767TZ5Hu8Y7vt7K/f9FKF/jLSxyzZtm5YPEMQF66eoL9QdKS1jdrKRpJbhP5Cogo/O9TL+tK6OgwYWI/MfiYpWkuBhn7U1VSja9BMlaxQxLOPcbMa0NpYh4aOPjhQZjQuIR4BLpTmJAGog9UGrMVnjMsJ3n5BJIBQyUiZlLO2dJTiHg31rF8dYNY3aS4JsQim5KDIcfZ1tioScV6hsQilVKWjlVnAzMCvqanDMLOAtRGU6gwPgOvEQ2u3hf31m9rk+/w5uy3sr1N7rWwffZ3Q1TUp9BdHZzdWN0hfYieLUdaG8qlNcn+7hygzq2VbiYWWVRP6AVJjdKwlHhAqVTgSCQxxtwrZh7XM3dTn5SDqTrlBaVueFHWwn/9j0V/62mtwavcbpL80IIaDRqzQXw59hF0XqmByMpP+ckihvwRTTefwJztR3MZyh9EyynK1ICCchfQBTmilaPSOP7yDM40qpGdy0CVyzbZZMNRajI+3foaaXgNGWBx9vnxAob9YG66Q/nIKfYw9KNSYIZaBBDjjyo5t+LKqi2Uwk+gvoZTXY2cubgQBWMuqdFo2Pkzduqv404efk0rjgBEqYJyvsNFfRiuP4/3PL6CpYwAj/aS/aCiybajD9g93MYvMiIHGPFyoHkV0eDD0Nafxxs5LLBKn7Fj+BZQM+SA2xj7pMKOP9Jet7+9CC+tC++v5vZpRxIQHkf5ygfSX89Czk9HdvAjdBP3l/LaPcaVhAMaBetJfahT6S5h2pugvnBeMdOPLA59g+8HLaO1nKc6AA8KDPKC7xJrXnQdRXVdJDd2zMLHm2N/Ugk/e+BC3KKxfefMqzlQZkZIUjPGWy/jdO7twq6gSdZVN8IxI5KSNmC1ZhXCy1VZxkfSX7RSSrsHVnEuoIv0lPsQLJce2Y+uhQrr1y3Hp7FGYtZxcuOlxatub+PxUEVF81/HlpQYEU+oyjOLUwiCcpb883tPxWPSX/R/h3z/5Ak2upL+khLK2l8pmhaS//C8b/SWF9JcQH3eMEA14bNdH2Edt7s6hAXToXRET7ILSc7vw1q6jqNZVIufqNTgEs2Z6jHXUH27Hmbx8nL2Qg2q9D1KCnHDr3D4cupyL/IJcHNhzBCVtvliencgSPllbmNBRfxs7P96NwooKts8cFA36YVFqMAydxdj97v/EjiMEZyQvQ1KID0a7anF0+1vYc/o6aitycDW/BaGpGQgmDEGcL7L6khKYWfrL3bYlXrvppL9U5F3Em29sR1ldOevLqYMcmIw4kqRs3jAj6gk++ejDnThfkI/TpLp0OgQhK8QR18/sZj96mpKiFfjyWgHM/uxrqMz35Rcf4rMThaisZd3pqAuhGnHwphSuXMdj01/O7v4cuw5cQpH7UzCxwVjp0u2saoGfNgsLsuchaKiGtTdcKd2uQ4U1EHPS5yI92IQAjR7+lNod7CjHp5/txOFL16Bdv5Suz7uGln21FOeg0TlGob+k+bbgjbfOEIlF1mnxDWK35mHjplVwazqPPxzTodCV9JdmVyx/Zj1Wpnri5O/eRZOsRtKjZoz+YiT3MmXxJsrXRdiILme6WT9G+ku9juebiHlU3IlmXVIwuaPtN46S4pKEl9c/hUjnOvzpHeLB6hPh310OPfVp0xcugVtAD44OkiRPqkyYrzPHDNJfaJNWr1S8QvpLmIOO9JeLXF2FYezaDWhTl2Hz0wswXn0Gb7JeKsKxFpfbvfHMy5tIfwHpL1tRR8xTWkIofOyusLsmn/qfyG7soiDD/qudWPT0s1iWxcZHDWLXsUYcPl+MjCd/ilVzwlF2civOXy+GNgOopI7l8xufRICV13asAz19nDFygPOKX4N12ZlI5EzfX0uu6ITnTqG/kIjTpVmA158j/aXjBv54IBcliT7oKK6FJm0Dnl6g5qD6AUugmlBnHsXpKnc885MfIU2tx1Hi125SBiyFKxrfe72BU2+P2T1+jQUYpjGMYMTsgwQNPRi833b6i54DkF8itXBvDcHIMAA130hmycXx4jFs/tHLyIgmycjPH44czEovlCHjqdewId2DogxfcMJeiXnUdX7p5/+ZalnDnNgTnXb1BvrWzcWqLb/AfMqN9jcX4+CO9zAUG0CdbruDzpH82gS88Nd/S0ZuD66fO4HDNZ0s2RiGilmmDt7JCBwp4QBpotfQhM7mctwu78Szv/hH6vi24swXe3C1pBEp7Li9XO5xGn7N9c++PbUWYDawkfcwaB5e+ukaaAL9Cb8IUqoebMdxgiYqE6/+kmUw6MKJQweQcz0XZWEUcmCfkbnh53gh3RHnjp/A1fN5WEC5UReySbNf2IiViYHw8w+AxsPeVmx7vPe3b3w1jDm5+GD++h/STRmONw6RqsEiekfG3QKof1p28RZl3FqhK2jFQtJfHAhxNvQ14cpB8iKpvxq97CnMp+vN0ysSW378N0iPCsAFvZcSJ7Wfgo3+YkBCSiZiY6MRQmGCuUHXbfSXEdJf0uNIOgmFixepMScpQzHcA++EJLrvYikC4YasxWEo8Zxh+kvSUmyJtaCt7BqOX7xBogtpFl6gC2qC/kKpQ09NMp7lDQkeNtCdOw/RBKIHO5JWo7lJIXZHuAdGo6/8Mi47DsBUXQTP5QvhI7qO3CTWYmJMNTkjCVH8ntbqpnzP0WzCkMELaUlxiI4Mg1mVSSIO6S/6EfgK/SWaDFLSX7IWBdvoL3z+WZWu7HM6/2MdN6KbhI125rONsAC+hCisoOgkRLjr6U3QYm5ysoJLc+5agP07WTw9JxhhqhrkXLTA09CAyIQXERroB8cmRww0NaIqkKL7jolI86VSFhWOlI0rdwuv3+zEWRjbnyf5qq4EPJvpAg5JDcGRS1/C3OPLMEAYfrqBlCFLMcbV8UhMSESShgIMiwJxmIXbQ5ysUCNkdvtWLKCCT2AMnnzqCXhYW+6hv8SR/vIk6S9N9Tl0oVJIhANuB2X8uojJGmplKIeuuOjUeQgVARPmITgxBu/CWKQ7xT+6qobh6OaPJMrBgUSYahdXgh0C4UNSjJohAzUDCEP01tSNBeJlalp7UzjGtjHG6+YNjfcgKkqqUVlVhgCq43h4+sBPvQBPPcHcz+YWBZrB5QMXADwvE2Uqxe1HWUAPDwc0kw4isVpmfnwrFv3LPijj1KS/jJL+UlYehuioOIpnBMJ3Ahgl98RHHQzfAPapfRb4Orvz2deSPMVJkoljB8NicYnO6KgpR+WZWgyZwkkDGiYxqgThLtGIZtmdRk2d70mLEXvLeUi7U0Da2RsxSQnwQT19xToFe2axUNWI7jtHJtn4+HlRANqKbirVDHj3oa/bEcvXv0D6Sxd2HyL9hT7m0FVZSOLKYVznhyt1X41hOjBhx4+uHDv9xdXJlmQi9BdvLzv9hXVffF/CEV5Cf2FnyrZN7VUmIEiiEzvaGdn4EDm7eTIGOoZ2Bl7cfPxgIGm9hzzFwHlP4a8WB9AdSfrL5b049eVNrPSlAP8cDyZF8KGk1J8bA9biWuxtH+YY58/r5mNO8YW+7l7oR1lPyxWpbJJY4efNpAVesFXoL4pNGDucIOKIHcxS5sPnVxSMvGkTFfdNdwFVkpxnlP5ioTbjCGNZlpFBNkQO9u3FOH/yBlmgpLV4x8CPEn1OTEKSGIaV5+/srUFEKPDFtfMwUJFkeSqvw8UN3sRZbVpDdiTJLru2nUbY0lfxn15cQdeZM9FpXtRsXgBcPIY/vV0K965GVLR7UrXIjfHPcOjbr+B0tRHOoSvg5krRerMjwqNCKFzOejKumF09VbAOSuIJ24kEzGa3b8ECTCxjYp2vr5ctQUkeWbkfHBRdOXh5E7+n0F+kbRNQoeekzDxKGgvF5Ieri3HxYhV+8toGRC2MxOdH30PtVU+UVTTBGvsDPo/S9seIObuJnLw2LFu/kfFzusO4GSifWZ5XANfYp5BBT4cz92/fRLGtrugK9h46i8LaHsRHSxIj+xUXD/hSD5yPoXKOViquBQZHIiFsFPve+n/Y5+lR2WJERqyI7Nv3Nvs6sxYg/YU5Mk9voERpZy3VjL6ED+kv//iz1XfoL+yu2SUO4db1ayjW6bHqR/NZl97IyZIGGiVPR9okxx72oW7+4cTlPQGHhi6U5xzAyTMeePlv/wdWxjKnZWJ7xIFUvi2kBr4omajym43+UlTnjifWPW2jvyS64Ddne+DX26PQX1LnL8DSiBEUnbmM4X4STJip68aOVDLjLJMGPOnUbNlRRooJd5H+ItpvQjrRQ63QX5pJOrHTX4hooxtYob80UZpwROgvHjb6i4qkkxlqzJJpO9zdhA6F/pKNFylv1vdv75H+0oP0uEw+qDb6S4CxEod2dKMXPH9SXBZSOs/qSAmxbj1UgwMYKNBh1cYNeGbjYqA5Gr99vwhNLV3073vRxjQ3Z9H1dvqLlbPzzlGEjTNr2tRMwgXpL5RNu4f+0kDJRDv9hTYzMV6t3De5hdO8yfm6UNIvLGMpniCcPdKxGd033oCOVI22TsHGjdILwQxkxrlcXcfpyj+HW45L8V//5R/gM1CCD/adxa0F6VidnoynNyZgnBSXcNdP8EFRPdrXzKV7hXFNob9krMM///cUdPUPobn8KjpPthGBW4dzJ25i00//DktT/JB/Yie9BAXYlDhKcXQDJQtZEO5JWTkySoPVbnAjEUQSXma3b88Ckll5h/7CCaJts2VbSpKHZHfKRNHNW424BXPYUa6AuS4P3W8eQMvI81j5wq+o4832PtCJKxdOI9fCmBgH0dbyQhzadwHmdFKpVqWQyCKrRCOTiSpQUDSANT+fjwDKRMrTZWbehZkrTEeVG+IWb8A/pi1BXs4hvHX4CPKfWILgzDClT5HVsRJv4/n4hSTjB//pn7Gsg891B3NATp5ghyy5HvZrsF3J7H9nxgJCf1EzPv7k5nh65cRz8DkOCP3lxQn6CxdoTkwgqrl5DodO3IJf9nPYsDgOfYU6DA9TtrXfAAQ6MvZJpqmTL1mmAQgJXouU+VTIy3XDzgNHcaO8HUuj/e9klz/GQGozigx6k+kvHipizZjV1twSiq6WXupdBnEFkIo6uvUaa0pQPmyjv2TzYfCQyaKIp3MfMpAqa0e+0alrxDhXGu4h8egg/aUwPxcG0l9u653wNLPrQuO9cKKiFPkkn6sU+osPAgn5dmUners4Hz4Gob90IeIpV2btyhA/A5vQX4oPYds1FVYr9JdmNFldSH8ZxIVD2+GTkI2FaRF0d5NaE5PKGa4PTuVWoDDEFz2OOpQYXRDF+KGnF2knnHG3t7TA2trObGV3dh4EydbXwcrZsAeJOC2Hy1EY4YtOKzOhjW6Ip76sf6w7zhQVIcTLmfSXG1zNBiEwghipgmvsLPjwd5H+UtZLD4ArV2Ni8um3iiMzlDWE6uJcEXTMpjS5tKHOTYPlXCmayktQWFgIT04Aiq6UIDI8m1i5erpkSafhCtWb2s1qyxCzJwcpj1jFDpYrcmM3mlo74B+QSPUZI2rYLsa1cYjwJ1NQReCucz/1M5sQM2cek1IIPxjsh5pkF3e2JV/62I2kB7kwA9OxJxd5tyiS79eLK8VDSH+Z0pFsjOIRmN2+DQtwEs4VYEtjPRqaOBl10xGfGA33yABOCjsZ1ycNprOD2ZR096cHQx0aCUN+Gapr6b4g87eV2e3r1Czs52TWhe3G2kYPSBewakMcxplzsOvdN3GhKxavb2SyUD91d93CSALpREVeHto95+Ov44OUmLuZSMYGEqo6LMGI8CCtqtdCTXBH6jNTuIRjopeLI5mVzAyu53k2dmG4oRHNkWrSZPgZitVLm62vHESrKQAvJ4XAfQbCJ9/G3fpzP6ZplFzslgb0W4T+QroLM3M1gSm8532siGjCiEcMAvSFTNr8EJXOi/GreCIvezphpofMqtLjOuPomm4HFJZWwi1qBRyHulBVN8IB1RlNzT3Q6wOQrKbmu6wUJrbHHEiFN+mHqDjRsCWgm/SX9c+swsf7TuOfzxyGe0AcEwI2Y22GP/wtzJLb9zaOM5aatOIHHFQSuRplh053mod/CMJGOLAyC8pKX3Tu55+hJ3k1Vq1cRgJDLQ7vJTXGYMXiTa9hQRal4RKdWGKzAzv//X/B6BWF53+2HgvmR8KfT8/2Q58j5xBjq4uewcLMtBmlv4Qmr0TCjR3Y/eaXMDq5Im31D7By0RJ0uwziY57XiX1m+IRn4sd/tQoLI51g7v0Ex7b/kUkWTlj2/K9ItkmHxY9lIdsO4o3/9yixqoFY+dyriNM648ZHWzG8YBNWLliO9fHbcPSTPxIAoMJyzsLncsXmFvYiSt/agQ9/d55u4Di89NoWzM3UwLW/BduPbsO5ESZCLX0OC1JJi+cEhomE07/RBtq4RXgquRDHdrwBg4Mb5m0khYWJaL0eery5+wiunjYiIC4bP396GaJMIWh8fy8+/M0FWFmHmJK9Canhfmi+RCD4hVJ0GZxJd5mPV15ZQamvbuzbfQjqDXTfhZD+8onQXwbhH78Er776JDJCHDD8xEJC5D/Fmd0WCpMn4YWfL0FGvAdebK7H/n3v4bTFCYlLmASVHqMQcYyz4+j0t4kHHoH0l7ZK7Nu2C1dKmSDoeAifGkbx2k+fxlDJUezcd4Zgix58seMTGF5+DU/Qlb8y5ENsf4NkFu8ArNjyOlLUjqjN2YOPj+RCb/HC/FUvYvOiMAzUnMctxkEHB4qx971G5HBi9+Lrf4dU1yGQcYGVm5YjhGg+2Ubo6r12ci+ao7ZgQ+gQTuw4hGoe19nbH09veQ1zQhmrLf0SW3d9AR1F6FX7dkE/ZMFPVmpxfc9HOFnUAEdfDdY8/xoWx2jojn7gxc6+Od0WYAlhVd4ZfHoiH8Ok+4SwD3pV6C/9lTh74giMaSRtOTShlP3FoPUGPvljCaLTF2DLD7dg8dJsbN+xHf96lAjMlGX4ybMLYO0pw7F3d6KEFRcqn1AsWfcSnswMUbwj9vXI49Nfxg0K1cSdpSUejEGZxkhHIDtwgIBtF09/aLWBilK+YbifqjHdGGPnpdYEke3myRieWJTOSj0bNWd0Hp6syTL24BzT1a0Zq5G9fC7B3T38HusKlVgEA8Re7hN1W12s5RpkbNAHQUEaJW3dxNqvDs5cCWaAnyZYIa+oOG0Q1/FM0F9EEGGILEIhU5hIZglgdqk/z9fCG9vZ2a1k33qQgRhEt68bnzL9QDfPV+gvLgpdwod0EhqQGLVO9AwKGcWLGb5cVY730iY7SH95BksWJEPFkpJ21sUJ/UVDZRD5nszG+7s66N4chpOHH78XqExMxsl/be/ohN7kCP8Jm4h7TFLyp53+IneXrv9Rkn+6CFYed3RmxnYQV4euJOLYiBhCX/D0DSTRxQcqqSlmx9U7OMyZoStXnhpSYZxZm9xNAgizNs1MJqKbReOrQkPBaby9vx0vvf48hP7SyWs0sC7Ug3+XfXFOxjrSfnT3MMZMYU03EnE0WjXrBLmqIJGmk7FnIycifoEaqBmLFptIcfUs/eXxerlHpb9Iv9FDcLKBhfDSJ6hc6JFQ+zAWOqCQpBR/FYVL5P77MWNSnp2uvmFy3UnwCdLCiwHOEb7X2TvEtuMONe+rtB0jyUHdfE/6AEn+UTm78J5r4e7IGnjmLzh60nXHnA4HuoBrC7/EJ7uvYPVf/RKLo3xIiOllEt8YMzaZmKTmYoF5F2M8H+UYPE0RZHDls6YmMH6YK5reIX6Wno8ADc+HfaF9m6W/2C1x91XKzKaP/jKO4YFehe5jok65N6k/GrUKxZdO4MDZGmz6xS+QytLLgQFShZjHIe3C2Y280YBA5lBw/OI4JRQfL2bnagO82Vfp6WUllWuM4gtMQgsIDGDikq1M88+Y/kIXD4UWPNhw1UxYmopJnWS7fpfpL2bSX1rqO+FNzI8fs52nwiZS7yeTC5Hokvra79Jm5WSjteo2mi0hSGHWuA/d91Ox2ScXs/SXR7fmd5n+0t5UwxWoK+ZmxClCLo9uhXu/OUt/udce8ptdWm/m6C9DFGfRMZbOapPM6DtiGV89s4d75w79RRr+n9XGmZ6aM0wJ5I9N0bnZZqM2+sKjypZ9mzayMrEhgFzXqbSJDBpiF7n/0yXRNV02k/iub3gS6z6ZtcuVxBQ1E2WVbrfJdJ37932/0p7sNpTX78zGNuUdEIWsQBVUlnG2qamLfUy2yaxEoK1FiJCBtA/xYIjE3nRvQtAKCImBPxONrMz2nqrba78Oh9ra2unPOpluK/3/7F86Xrlhs/SXu4aSVbrYZJb+8lWbiCbqTDzcd4/8/fnJPoh+N0knoqFKJ/IU94iz9Jevtm+ZyMu/GaW/0Jc31fdXrkEGUwfOlh672Sg1OZP3wjdszrZ7G6Vd6Fda6t2Py8XZDP11maT279379wd/z/7ZyceQh3sm6S/K1YgNJl+n3SaT31Nu7P3Xfve67M3v3uu2vWu/znv/Zv/ug+0++bPSAGaa/iLnPPkcpGhqKhv2V/c/YUHF9vfa5EHHlhjpLP3F3uoe7VVWX11dXcxbCIKLi13u89H29cjfmnjWJrc15Xm559n7hnt/YNth6/lKW/76/Ylrt4elZyEhIbOi9RNmEvpLP5N3xCbf5qT1Ye7jg+6w3UWtmgrtWZHKEtqCCADIZpalM8ksKhbZuzJl3LYxqYgPGUsemfghJR0T7xKyOiqCA0wucWfh9cTbE9+RFwsMowb+V4gSTKqZ+ItV3C+jFJ3m96TQ3r4ZmaquHINZn/ZjyOpL3JfyYE/F9dqP9R+9mjlQWUl5sdNFzEzKMrAGVoLadjFr1lvw2g2sI3XmtduuTQrOJfgtEw25yQ7MalZRumxyqvWDvifnYmUJzih9FiJw4KbUyinv2uwuRByKFNhtIo1XbCL2mBGbMOlonDfGSUV37MRJCC1DVGGc7tQM2pKT2N8pqwJbXaH9jtuuhbfya0gOQpIRUgdrxOxNTr7CTWxqYW2ZMzOQ7HtT1JB4bJdJhAexx4zaxHZ636v/yuAlNpRV/aOFUVgKx7bKXdyzWXnjhfojmySw2QbJibvJ58SR/+yb3FtRE3Se9NxIfSgbGtv/3c8pn5fBVZ4z+5fve7W1E/YdVB27s/H4CrWItcf3f0/OTZ7ZyZtM5OV5k+ds1rVrs4xMWsUm0k6kvUz9ZiuplI5U+hNpO/f0ocoB2Wdw7HJkAtpX/ibtgnfX1lykTcrv3I/8U9oLf+L/DVyNyvnfHYEe6UosGO5rRGFxLyLj4xAZ6ss6rRaUFJehsW0A7toozMlMITXBFb2NVSgpJ/2FerTBsRnISBRVfdaUlhSjrqmH5Q7+SExLJzVBw4zWiQfGPIa22hISUhqhhycSMjKREBEElYnpzUWFqGrsZNZdCLLmpiOUkl8jQjoprkA3k/lC4jOQnmgTe7fPTO2vj3SpD/Elo74XJfnFPIkEJFML1IFCArfyi9DaOwpvCq7PS4uDn8s4dOW3UdnA+iWVL5JJcYnSeqKdMlR1Hb0wseO3ckC1sCA4Yz6vT+2llHdbTEzGonRgSXUrxlTepL/MQVxIALOdacvbBahp7iP9JVSxSTBr6wZba1BQXIn+MRUVgDKRHh/KOlLuW1oXN/vrQ1zew32UHYu+vwu62lqqPJH+EhCBlLhgmAbaqBHczEw4RwRFxiMhVsus3SFUVVRjgAIc7I7gHxxLCghr/DgZs3LC1UtKkI71sBFJQhRihrftAjCm7+P+qynSwYxlzwAkxpP+IiUNtFVbUz1qWfMnWcAxKekIoWrJMNWPapjQNsp6Fz/W5cZFBc+sTR7Ogt+pT9vbk/31m568CJoM9Xaw/rwLFi9SN6LUHLwovceawLamZvQMjyMqOYUCXxb0tNSgqqlPekdm8Jvh6k2wQ0oKS92s6GFpU7WuDeOUfQuJpRwlldzaG2pR1y70FzXiExMRzhpAmA3oZg1qRw/F5CMSEM72MnkCJpSl7rZ6VAhlidnxodEp7Jv8YBrupN53FbpZ9uKj4XkmxMDH3YlNbQhtjc0kDVl4nklUOGIm8MTF221hf/2mNvk+f85uC/vr1F4r4RO97aiubiD9hZNr4jxDSXmKFPqLMmJaWQnQw5phHfvkEZY3BSIuKQ7Bvu6E+1D4p6UBXUNm1sAnIpQVAkPcV021jmVVXHxwOLVQ1S8onH2TiORM9KNO/8TtUS5CEGfdzRU4vuPf8f7BJsRlZLFDd0UBVfJ3X6pl6YYDCs4fRL+LFkHeBhxmjVhZ9xicxqtI3GhhJxmK0aqT+GjfTRbCumOo5jJOlegRFRPDgZcVpuyAB1tu46OtO1E/SFHrjts4U9qn0F8sdZep0H8GQxwLWm/noHyA9BemN+fs+ASXantZi9qO8ydK4R0eiagwP+Vip53+Yjci06crLx/Ar/+0FYPqVKRF+qH45IfYebqeM+Ix3Lh0HmNeQaRS1GDrp/swYKaAQv11nC0bQ2SEFsO6AuRyMtDQWo+TOz7HtcoxZCyZQ1KO3DRKCNbn4v33d6N9zIrhxnycqxpR6C8jFefwzucXiVZzQH3uRVQbeLO9zTi7bRtutDDNe6SJSj9VCIgldSfIR4R7McAUdC9q006nC26MHc+5fduw89g1hdTRNuRE0oorio/vwd6j15F7JQdnrzQgJitBEWd493cf4EZxCXRNFRhzj0R8pBS2O1IWrg673vg9dh6+hsj5ixGp9aWPghs9E521JDl8tpe4PSF1nEWbcyRSWMzfX0tK0Ds7ca2mjcSQFrgFR1HSsgeH3v0dTpY1oq36Jk6fpkBGWirPyZuD9Sz9xd6MH/X1Uekvo/2tOLtP6C8H0OSWgCWpYTb6S8FFvP9r0l+uVSNtxTJo3Myozj2LTw/kULu5CJcPf47z+RQaX5ENUHxh+5sf4Mu8Rq4U+jHOunZvYxNlJXfhfOEtnPvyMipIBlqQEgZjVzF2vfcv2HEoB96sF0wI8ZvQzlUaFXoai7D7nbdxgoIw9SVXcSWvB2ExalRf3IP3WddaX1+FK1evwhqchji255YC0pf+9Q84mV+P9OXL+Ozd9Z5JGGWW/nJvi5K4ouRo+PoKkWWqV6QGlOd+ibf+tANVzdWob28jcete+ktd6XXsJn2qvLYSFy5cR924FkuSAxWC17Y//jPJQNUIylrBe+uCjtpCnNi7B7kl5biZc5aSgwWwhGdhEUEH4/S+Pib9pRPn9+3H/kNUzXd7kjWNHPso1ddd286s2yzMWzwP2sFq9I2OoPa2DlWOFClPnYPUYDPrcAycsZnQ023EvDVPITt7DlR1bvi/tnejpX0AWRS+FzdlCxtwq0cCnlu/gULlrfjjn06gtpr0l6JcBCYswjNCf2k+j98frMMtd9Jf2j2wevNGrEql8tHv3kJzPYu7M6KhmalQjQx0Dbdx7IszxClVIZV1kfrBZiKddFjwxCtYtzQRVafexTlyC4OpujFv1RakxIZgvPYkbp7pJa/UCfNXbkLsolF0NtxAZ2k14jZQ5IE6oNLUbPQXSuD5kVzwzDqEUxHpjbfPo7omAoZreQjNWEVb2egvfzxehWhqIF/t9sdzP3oWc8NBm7zLMqBWpCeFwfcrvox7G/qU/DZBf/niRg+WbnweyzKF/uILf6LyfNaR3LHekYX4hfj9b3agXNeK4ES6UtzisGl9NlJTI+Drp2G9lop1XAOoZoF1wYg32w3d45wVSj4otY64KnGCf1gGfv43iZws9ODmkY+QNzDEmFQnqolOGw+fix+vXowYrQ/regOg77hFT0YHMl94HplqK458eIQ1pf0wJobY9jclFz67k4ezgIDsRzHmGIC0MGeMcBJtpnvXzM7WwDpObWYyavN5j4x0w7mqkbL8OfyfmRtgHmrB5QM7caSF4i5sI9J5lqnS8FevLUN0mJrth6ta6yhe/sXfU5loGDfO7cOn1/LRsjELWrp6XQOyEFpGVBZXLTb/zMRZCyu3qhTXuiPwy79/GQED1Tiy/QhOnPKH6+0qLNzyN1if5MJndj8u5lRS9tSTZJpxnmcSmksoe3n//h7OGLOffmwLsEJjnACV0AX4wc/WQqtRw9+fAhl3+jwnekXn4tW/SYazsRHHD59Bnk4Ui4YwRnEcT3UGgqgRPs42YrGq6N1ciFf/LpGqa+MoOXsMh46XkovtLQ4RDnq2k31E167QX3ypNPQKolKj8Yf9JJUQWq3QXyh9V3K+gLG6JugKm1nYTP8zG/lYL1VFSH/J4VI4MvspZKk1SHjmFSx0pUtlvB83Gjhr8PMls89W0yj0l3E+RInJySSdRHAm6o052itw5eDUS9JJclYMouj2c3VPR7jqChxH+uCbmISYqCgW3rshY2EoSii3J9qz1JObgc1KF2YLcs6cg1v6CrxE9zQRl7wZvKkqAsmjhMCihUNGGnIKvREYR93hUA/U37qEM+evw+Kzjg++J9y9REzAjDxK+znPWYfVKxdBM0GlkFiv2ThO8g1dURERpL+4IEtzDU4iDj/mg9REwtXDgjmpoU2OX2MmGZV+6A6L5mdDSH9JXxiCKi722R/R3TH9JrHRX2rQOcrVb0czCvN6ERCZhkWZBHPHxfIETLD2sa1Qxs+XQuJu1EamviEqKzzZgE2I54TIm4XPg62luHRFh5Vr18JSUQZXrrrtbjNF2JxiC159OpRWFSI3twHaLRQVN/Wj4lYTHAnmba4qJjA8AFnZCxBIkZDECC9cO3cOjZ7ULdakY0s4sWzcIc0yu30rFpigvzy9lnSeJlynj1WefydPb+rdrsYTJP3U02MlbjSJP3qIvChJPV1V9XT7jmPNsys4MRzHjdIi5hssQHNdMd2yfkiYuwQpEXTnJgTAOsKQEyddvj4h8KYQfpB6Pp5a6wBTnY4d7D3DqCKKMsaVhio0BQnRlJMjXzgh0xtf1PchkX2SB1eYFnYqDs5e6K4eIHCB/NvFa/EEcyJayBa2d67fiilnD0oLOPGeGkh/KUFRkRYRkXHsM/3gJdAOxT4UdqEG+KhTN26XkDFaXw3tonWKaItPUjbWrR7G2aHTStxU4qTOlBjVkG9sIdN5gMIwjvHLsDBWrXjE7C3Htt+HNj6D+2xEUXExiIukSgiDxuLrtphJle9ljI5uDU0QuZs+TqS/dKOvn/SXXmfM2/BD/JwzhI7bl1BQWQ8nH1+oqGR08/Q+7M3pwDIOGulxZNtMbEJ/8SZpQUgnFtYBiRK/KOc6MJnGk6olSiiVySLOvAqZbAhCSwnm8+qcJAFJklrsV2rf6TS9Wsb1JLscxaEKqVeim5Lh5w4K1usl6crbF65MfJDEKUm2EYKLs6s7E4x4DYzdeWs01ARtRtfAIFdaZCS2l+NcgYHxZYl/3sv2ku/6UGCbJqfNmSxjtwn350WbKHk8SpKT2MQBnj60iXJ/aA4qs3w1qj5NBpGr5QCv72PMi14JSQAz9FRgz9bPkFfdpgxaEr89e+xL+KdR0i8uEoGBodjw3AZEspav8Ojn+PW7h1BcU4sb50+hxjMNIVRFspBv299LBZxxZpNMbGbGs2oJgD555gjKeroxrh+mWtYA2gYJQmdMnjR5lJ09gG0HKXA+4oxgAgTaq2/j8o1S4tecSduxUYXs+5t9nWkLCGnDmc+6x53nV3lweV9c3L2ojGZDlE0+K4uhB5UleShxjEL2HHEDm9BNybdRKqpZhfZy8xje/uQ88yU4ObMYUHXrOnLyu7Bi3SKGjqSfcIMXJ2kKxWXyjpWfrUSxuXCyquVKln0OuxFJVPMLC6EofhQKj23Fm++9h61fXIE8sSomHUlnazvPr+xs9o0Zt4AzEYwZ5FU/AdfhJhz+eBv+9HkOBqhwZt9M1PCuKriMgyevoqBxhCvREapqcYxhbN2L2ErOue7bzKgvo1Z8ZRuy1nBxY8c4TnzqEVek8m1bA5tMfxln0kcxXbRrn9iA556ai854FX59qhf+br2kv2QgaQ79yuF6kjjOY6Svh/JtbSg9/jEOXRvGkmd/QlRWJpNwbCUSMjA7OHLF0NSFeSOi10TSCekvgUbKh1kpVN1Fegx/duMxe5iIEskOsbmpFX3Dep6bJ3q72aEGC/2FT8EMbCZm5TbcbkBfZTMuj7ai6kYBDLVMBorywlAPqSeU/BvnoD5EQsuoQYX+jhrUWEMI487GFi3Zif/yLjqaKVIfH4D2mtsY0GRwNhwJD2Wqc7d0Q+gvOiZZ6ecZ4UdJ+3bSX8IlG9bchJauHmUFbqKLs4eD2DgxVE11LeiXzGDas4c2Mfswq3dmTKKsGl09iDnLiCf9hQOkQxPar7+NRhJ92oMYQz+yFzfaw/CjX6230evN7li8ZiMTQUaQGOaJ//v9KyjT+aOksAVNdLNctdRDdy2Hq49ghHCFnxhIDB3jK5L1LLrGsQvX4Pa5nfjoYh7mRi2EV0AI5q99gmDvcLIou/HmxQoUBzbh6C1X/PQffo0U714cZLz5csEcRAQthOcjTitnoHn9RRzim9BfbLfIir62ehTcLELc0p8hijJuaBMhlyAsWfQU1q+JRm2AM97d38z+oA/jjSX4Yt8lOM5dj80rkxTxeTGoDJCOnKArGZ0Tz5lJVptM+R3jyrOxrhmj9Ci5UlB/iM9vXFIs1i5cjqXru9Df14pLZ0+jwNFHaYNKf8h9ygpGYn6zTUks/O1sIr7gT5jFmmfiOYdu5eJiF/bezkX78DJ4UY6VXSFDSD5IX/MS/mnJapw5TN32y4dQsnYxVsWSf8yGIe1C7qOyMOFlWEbaUVRehjrXWPwgK1xZvE2+uscYSG27kQ7azJiGJAc7ciXkTTHortYGxu0YuK3rpDZmKBOIMtFQOABdWSGRaoxljHoj29MNNec/xTufXkHsEy8hRuPArLcWOAT5YbSDWXd05bqHJqL7ahXybl7FsAcpIRyANlDgPjzRF0crSnCTJGan5qsYZaamNjYCngRiF966CY9hFxLquxEdTooI5WvFcNO9qaj5m/3K60jYzNkNxfNPjfEhTp6LmJg4jPhdoJuxEN6OnEVfLoNX2HIM1Z3C4WNuWL4ym2DvRjSQ8pJG/WEHrq6aK8oRl0BuYiCTgrhJOVG7rh5mLz94hCagPbecNvFCiLUWpWYPJAcEIzDeGydv30aQuwMH8Oswk6GnjWIGdEEO8gu5kms14npZP9JSuJqnW3cytm66bOPoIvSXBOBMAcrLIjDi3I46dw2SvS2c1X+Mt/cUYskLP4anZRAtrWRO6tvRpedEiZOF2ppGePuEIzwsAak/e50oPZZODXfAWKFDcEIUBz0TVx3XYdJEw2u8A50kA3m7WjE4xMmTM7Uw/UKREu2BsrJKhLoNMzbcgojk5fDhz8YxZt8xS5RrCZZUUUFrolxCiXlMlzFm9/sfWIBhHOqZNtbWoLauHq3uzM5uYHY3hd/NzHivqmbCSFurkjkZxvBHiMcY6qsKcL3bG79YEA9vJqOZff0Qzmzw/NslKNOOookJZm5q5g60l2Lfh2/iUncCfrGRlI/OZpafhcKNGK3qGjkeczro9ainCziKyXkttaXogJYrUCL6GE+/ejMcfkOkNOmsWLOaKcDMBDfT9TzS04WRTgesfo4DM4P1glCsrK5Bg3KetaQwpSDIl6UdMlrPbjNqAaG/tDXq0E1It4oYtTbm3YQEZcDN2IvqkgYMqkIQ7DyAxl4w+9/I2CjJLiw/9HZhAudgJyrZHnR1bbDW6NDERLIIVoR06AgJr6xG9OK/QSTJUvff1UfO2rVZhg/AcA/KO0xISWEZQTgF1F3HkHPxPI4cO4uKPm88tXE91iydA18mHl2g8v6ZK0WIXvgknlwUQ1rDFRSU1lFcuAXFhQW43cLU8XB/1BzcjVJmncbOmQv/wSKcO34UF/OqMPfJzXhyxTxEhWrReuMajh07gcIWFzy9eTNWkFsZ5tTFWeJxgrPz4JO+DhvWLmW5A6HRHEmnO2vXgas/T381tCEUy+cAb+xoZfJBNjLTksGKHtz48gyOn76Mbl/G5J59AlmxxMyVXMbJY8dx5VYNkpZvxoYVnyKYigAAQABJREFU8yl7N4a6W+XwislCYlw4mGsD41A3Lm79ADWqYCQyxurZVcDvHcX1241YvPEFrM3OZPw1AA2XvsTRE2dR2uWNzVuexZKsZASjFWdOHMX5a8XQznsGG1YthJazC6lVFZ3Iac3apWvelcEsa3s+Tp86iRtsxJnrNjMZzA8Fp86jhDP+7s46lJSWYUAVAM/hKuzZsweHjl1EbbcHNv3oJayYl4TIsDBEEr2mof6yma66lJWL4e84gOMf7sUQy2m8xpuwZ/duHD5xAbpeLzz1EtmUcxMQ6AfSZU7izJc30OWWih9sWY/M2CCYOwpw5uRJ2qQEpqjF2LxhOWIDiN+azdp97A7v0bJ26YJvKcfuDz7Dpdt1JKO1oKffihgm4rXkH8f2A+fQzMl1G8NEjv7xiCNPubmmmF6bxXhmSbIiEO9EALw7wzl1Ocdx7NxVtBkD8MIP1sLP1ICDp26iu68bzZW3UKFrgjoyFsbWfHyw/QuUtXQQzdeNfitLIPz0uHaCfY9DCpamx5EWUobDR47hNsHeiaTJPJUVhPpr+/H2R3two6ITscuexfNPzoU3J4K3z+/D9oNfooUIyfauPjgFxCMpXK2EpWazdr/arKYza9fC8sNbOUfx/rbPcfZiESdZKXjlp89Co6/BmeOH0ewaA42hHvs+2YEjZ86jnTk367f8DMsTfIjWO433dx4n3acL9S3kO7sEYV5yAFp15dA1G7COyayR/m53BlK5DsnaZfz+cdZrjItytdQ/YpN6koYsdTjiQlToLyQjaDQBpMI4YYwc0k5xPbIWx590BF/6ofUDAxgxGFg8LQXWdLOQduJNwPP1nbthTVuJJcvnwn20ly5dumeYPRVA140vV7IOTEQZYixMQM5Cf9EyG9OTxzYZSFkhOUU8wb4BWqi5wptR+stEexFAtIGDlIX8UBFakFlsD9VexE5CJ9EInYQJDkN0b/dwhS4UFzXjpEK2kRIXPb9rZe2Th7vMaBlbHGzD2Y92wS17AxaT/uLMhmKjxrgikAlM3vTXC/1loLsT3SQaOHn4Ioh2d2dcR3ienWwUCv0lUKg7dIdyn/JwzxT9xTDYSwLHoEJ/ESqHN0ujBnv7McpzUMQnuBR0ZxzZw4nXQKKPntlQrh7Msg20ofXsj6EU5Y8OjzAmBdQXnsW7BzqIi3uOblwvxuBJeWHCm6u7N8kf/B7bg2V8VHHxD+pZv8oEFbG7s1A/BvrYsfJ82BY9mdkZyImPM40yS3+xW/rRX6WkQVTEwhmH/uZlVTYeaW83Oy47/YVUFz81yRtsv/1DDE3wfxK48vBhNi7rNse46hhzcFdIQvZVn4XhlX4qCPWzDtmFVJfAAF96eEju6B9hO2MGJiePTiy+9+GEV2Vh2yApiTlN3ChMwoS3vrpc7Nh7A2v/+ldYmqCBSShBpBZZiQP0Z7uVelE96w+7e0knYizNP4CeDyE28dnTk33bNyy65XKeKp6nP/yY3yHnNkt/+Wp7ml76iwkjgwz59Q2xHt8BXr5qBPg7KfSXL87VYvMv/nekMw+jr6effY0Rbkzw9CfpRTx1eo5TPQN6ZaBURICYmBbg587E1xGlf/Gi50P6Cvv2Z0t/sbKBN1OkwJMX5j9Lf1Hul9BfWhu6SX8hGsrbltVsv5GP+vqdp79QlKKFMeaUuDBlYH5UO0z+nn1yMUt/mWyVh/tZJAJbW1sRwUzxR1M2erjjTdmnrQZ0NJD+0kP6S3qsgn6cqn3P0l++akm7tN5M0l8a6+rQovfBvPSou+pyXz21h3rnDv1FZpB/XpsVag4YkngwxnNTJoyPeYIi0SX/ZBkuD/djLcIf81we7esOUAdrlMSpqbpfMpCKTaTjk6D6d8omdF/4hiXChwlmKosUdj9+KxHZL1mR2m0i9+k7ZZNHa1hT+i2xobQnsaG0U3n97tjQyuS0SGSSU+lIsZmp6hbvt8msRCAX7Gwn0hfb24n0P9O+MXlIHRwNP3oLLEaWxkzBASdfx18U/UVcTbMN2daCpCZVOr3vrE34MEpd4eMPoXefKDsRZ5b+ctcmD/uTfSIyS3+5azmZtMrA8d20yd3rmMqfxPsj/2aU/iJ9BnuMxwlm3m8DuQa5t1NAf5kog+ER7LNPGanln/xuf09yzZX/0b08+f3Jn+UfvtoxTuzLZoC7Rnjw92zH5VfuOYY83Hb6i7c3k1+m0pL3WdZ+XrZrsZ+vnJdtJjb52u1fle/IJn+z/2z/m+3Vvp+Jd+02Uex192/2Y997DLtNbJ+zX7s83GKTwMBABextf//e407db5OvSzmWXMPk3dvv/eT37e/J5ya9/8BznfR3m+3v7lw59uR9SUu0H3ziffmMrEiFiKNhvFoe8Ace5+5uZ3+6zwJiQ1mJCv1FgPEySXtYG9rbyd3vffVeKYeddL/vftb2jNlO697nYvKp2j9vP5b8zf7e5M9xb3fayeS/P+h7k9+bvD95X1y73cwbmaW/2KwrNhHXrtBfJIwyPSvSu/fOdtS77eHB99j29/vv4517Oam92fsX+ay4doXsM2X0F6XmZkL1WUgnowYTi5Tvo7/wIRszk0Li6Q6K0yibhbKCo6zZEmKJJCt9dePKSU83EUsVPCSpZuIDQn8Z1XMmcN/3jAa97RgkzNw5Bldfcn7i1v3mCRBfPZNv9A6ThUZ5vhbWNnpMotWYWItmMJhZYO6pKPNM3pdCimHWr1BQLJQaMykuMalHk8Ygxer30QnkGELEcbj3GCKrqCdJRzIY7xJxrKyJG4WRdnefRN2x01/EHtNuE7lYnvM4RRSkREqhv3AAkxIc6aB4lcwjuSuKYKINJLHETs6x2Yr0GJGIY+G+nTI02Yby84O/x0NLRok8CPbGw88qNBCpE5vkVhJbSzsRSseM2OT+C/ge/G4nLYkNHy1GynZBrY3J9d9CYGHLua89CLmD7UTc+5MKo4UoxJwiPjOTSD+yQ27KYMjEtjvth2V7CrnqQfSPiXsh5CAr3YEiCnNnU77H2m32J/ZDSyIcm7Ksd5RnVmpT7d+QSess/eWO9ZQf5Pmy22R6BlJpR3cHzwfTX2znJMmhLN7k/0T4x3Yflb6CN1DpmyY6DqXPYL9kp1fJt6WNy/k/aOSy7f0b/VfoL/XIL+pDVEI8y1J8MdrXjJKiUjS0D8JDE4k5WakIZrpwb30liitIANGbEBSbiczkaHgwbbys8Dbq2vrh7BuCdJJiwllHaicWWUl/aWVSSVFV0x36S1JkMOkvQ6gk6aSqoYv0l2BkzctAmNYbw+21JNGUT9BfeIwkob+QiC4tfNo3PthjQ6gtu40KJktZhMySQSH/MAobD7Sg6HYpmiko4U0//Vxep5a0CYkMGCmecDuvGE6M+SWw9Keb16tr62WWMuOW7BGE/pK1MIP0F2/l85KJ2lx1i4o/rTA6+SApaw7iQwNJf+mnLUl/Ycq2i08o5tAmIQHuGGipukN/CUvIQkZCGMsFZpb+MtLXybrQmgn6SyTS4kNg7m+kNFcPjOz4HFz9kJiSALWbFV31rOlr7YWR8oehCQmIDlHDwSjap+Vo6hiEg4caSSlJbFOsIZ4oopdM8Y56iovf9z0n8yg6mppJhRlBmBBjfFniYhymhFwNdC2d1Hb1QmxSMqKD/BWFKHs7sb9Oe5P5Hh7Abjv76ze9RKG/DHa3oYn0F6t3BJKiAym8wgQg1gNW6doxBleEk+YSH0PdaaqItVBsvKalm3AMP8RSBjNc7YYRqeWsrKWCjRUB4bFIio+FMzPmG9l22phRKwObpzoaKQkkMhn6UM167fZeA1xZm56SHI9AylTaBz8zj9HeyOQjne1ZjqBUaUyQl1LuV11ZgY7eMbj5B7PsLxH+LmZ0NlWjppXZ9gSDu3hpkZwaD18uDGR/dlvYX7+pTb7Pn7Pbwv46tddK+ATFfiqr6kh/IUeKFRBhUUmka9npLxNH4wDa39WkjCP+JACFUse7ke2qc9jMCT/lIwkU0QRHIDbMnYCWOvYZpJSpfNgOExHNsU6U9uybE+Ev/2T/5WFerUzy6Gosx9HtQn9pRnym0F9cUHjgA3x+uQ6OTBVX6C/OpL94jeLQJ7tR0TMOZ3M1Ll5qhn9IGEz15/DpF3lEHlFOL/cYigc8EB0bQ2Fy0b9jnWPzLXywdRcaibQxdpL+UkL6C10BCv1l9zmMcHbZVpSD0j4Xamc64fJnnyCnrh8OLNA/T2Fhr7BIilf7U6Vi+utIZcXVVZODN7ftx5DFBWONN3m+YwgP1aDx7CfYc72dsmMW3DjzBfQ+lFaMDqNusB5lF/cppJjhwHRksPPoKL6GXIoINLU14uRnu3G92jiJ/kKhf9Jf3nt/D7qMDhhuLsDZimFq6ZIaU076y55LMLE4vSH/Iqr0XiRQmBT6y81WJm3pm3H2eAXpL7FU8ZlB+stQJ86Q/rLr+A3K8w2yQ1MhItgHLTd244P9V0hf0KG2XY/o+Gg495fivd9+hHLW9tUUXMfJcgM7vSAMVp7Fb7fuRXVDM65fvYDaUX+kxkfAWyQPWQrVo7uBt3/3ESpaKVSfx+9VjFF4IhxW0oM++d0b+OzIDcQuyUa0xhf67mrs/HAHzuTfwqVLF3Gz1QFzUuOgZu67hADEVSPu/0dbTT3ME/T9/KysvgTa7ONDxR/RsfxGm5W6qC2kv3yMP3z6BZrcE7GU9BfLYAtOf7YNh87eRu6lS7jCCXvK/FiM1N3EO2/sRAHbQ+nVK8htc0BatC/qrn2OnQdPooji9Zcu58I/ZQn8jF049u6bOEWiUr2OiD6nECSzYyzLOYA/fnocjS2NuJyTgy5VONJjg+ktktkZofdVN/HhHz5ATkktKq7fwLV6I1IT1Ki9dhB/3H6cA34jLrEOvtc1GvH+JhSc+Bjbjt2ikEQdmvsdlImhP7VdpauVONos/eXehiBxRQkDTBf9pYxt4O0/7eTkphaNDNe4a5IQGyykmbuD3zjb14X97+HXlA+0BKYi0WsAV04fwMm8UtzKvYp9e0+jzRSOFAp8XPhiLy6XViDv4mXc0pkwZ3EqfFheKNfx2PSXCwcO4uCxAlS6PjFBfxlnp9ZFqa5MzF00F5qBKvTR1aorukkxAS3mpGQp9Bd1oBEBHowd9Plg+SbSTjjY6U7k4twAa8C4Yo3yd2bNl43+0u6ViOc3CP2lDX984xh0NaS/UO5Jm7gYmyboL7/9oh63PNqQ0+mFtc+S/pJG+stv30RLQyN6M1l8y/qg6d7EtWMad8LclVuUFZeJRJfcs/2sqe3DcH03tBHLMW9hPHyIehqj8LywMC1tBTh+8CwaqmuRwYmDo4s30lZtRswSTlLqr6GjtAYJG1YjMZwrTl6A0F+aiq+iRz0Hr256kvSXOrzx1hnU1ERi9Go+wuesxnNPL4Cp5iz+cLQat53rcK0nAM+/8izmkf5y/HfvoLGxBQPJM0V/kevIx8GbvVj+zPNYKvQXb9bXOY+hZ8yE1NUbsTgrhZxAfwSSE9pW1ILaIX+8tOVJBFp1eOtYD+O4rei/cgUhczbixXVzMFx5Em8fLkT98izyA4OZcDROXmArdENqvPziOvibavDWcan1G2K97ShRSClwbqulco64BwF333A897O/xYtO46jNO4zf7r+Nho7ViKN6yez2bVlAknGoXKXSIDPCFUO8Uya65Tx9tFj54utY+aoTiU/nKIRwHsVlC+DZkouByBX4L69kY6TmErbtPoXq7nnIXPwy/nHpjzFQV4TP39qG25UtiEt2Zo2oBpueeZYC9qEIYH05eqpQfqMQyU/+BC9nB6H08hHsv5CLjhXsHKUdjA+hvrYa5ePp+G//sAmOLQXYs+McbuT5YfDmbaSt+yl+uESD4otHcPDLm1gdt4ihKVcs3PwsVqeFI9DfHxoKwdztsr8tu/6lHpcLLxN1B8IW4Ic/e+Iu/WWye576y9W383DzShXiyZqlaww+QfHY/JP/iicYBmsrOYu9+w/BN06LwLBYbPjxr7DeyYArp47jQn4Thtl/g9rm9u0RXbtCf/HHks2vICYtGr/f13+H/qKO9kbxuXzG6hpJf2nCkgn6i6G7EVcOkuHHI0csWYcsP39ERq1DEldlxTnncJarWFci1bztJ8cHaXzMSPpLCqKi2DgpGZgVdJmrOBN6x+g2jYsm6YSi0u5pCv3FSd8Pv8QkREeJ+Lkb0heEzSj9xYGxmiCSA56LNaOJq+TTVFcxez3FQm6KL0R6oeDydYwNlKE8vx1Pz2cSxrBodV6AZ+YavETRa38vplPR1+7mTiF/p17k5l2Fc9aTWLVyIQJlhc5NfP5m3sDktHil4F1jdUaW9qpCf9EbfTljJmEmlMo9DuRr4hqpO6S/EHgcFRGBIK0FaQr9xXEG6S/jdInUoIv0l36usAsMXQiIysR80jgYdqLLjXKJ1FM2xsbBg6tCn4AwRDntwdmTo/A0NCMy7RUOlh5oH7AiaX4cYrmaHlNlIWg/FY1GhhXhe2dqZ8r3Ih33Uq1oFO6kDkWm/BiRwUEIJbB3LSXlSgp7FK+EOPid3XwRE0epNxKHWnJZRsOB1ZfCF7OdntLEvqX/EMSgicGTT62Bm7kR1/kccF4JlYcn3fuxPCc92srGYWCM39fdkYOsmf0OqVBRbA/oRrLXRYaM2BES0i5KYOis4srDHyGUtnJzZS6BuZ/s0mLC5Pu5UmR7Ybb6uMEZ6QwnRIvSVVcy/C5SgJ75BRZ4sa0wNiYYN+Z56IeZbUuRCA+nIYozDPNU+L1U+Z6G6mVJ8L2ay051PvthTswqbkEDhrliEzhhpBgM8wFm29W30aQEZGKjvxTe0iCSbv7kTB/KAKqUBYmc0WBrJVX1quG+9CU8HUGBFwvj4AR/eHh4wNcyRMZ1D9qdwrElO5HEIJJ+xvrYj9zCdbJv+92ylfDY5CuThc4jbEzKcPZEJAezmAhmOCp0EQZ3SX9poSi7Gzu/oBANQrmy7Jqgv/TT/Tp/0/+G1//6CXQWXUJ+RR0sTEby4PLYWYTNOVvsb++m2sjgnfNR6C9UDpHgroUKFc4M+kr2rkJ/oTKS4oXhseVVVuyeXrbGy4+Q/sLBR2YgMxEeVc7YgYo7E0QXzk79ghgH5OShrbMRdV398FJ7Qsta0BA/FcUVGnDz+F4crlRx9R7LzsMJHYz3jBiYPMG0qoG2Mpy7NYa5jH/GksI+eXNkUpKPKKaI91uxCR969joOTDDypE2cJDCuJPSITUh/EZtIx6TYhL3MI97xyefwTX+20V+6YSX9xcSYr7GvGvu2bke+rg/ajKfxZFYcnHrKsIOA5QPXKzDs4EttXWfoSnORW9XByRrvN2+sA7VzNRwQmT/C83ehsohNMcbWSTF5hINjaKgKNSW5yKvq5H1g2gAbhYqIPiFy2Bxsk86anNSm8lxKB5ITu3I+ooOpJTi7fYsWYEIdJ6Je99Bf7KdjQTul/S6cv4WY7GeRHEJkHhM8gpkTwWbNzQlu7AOciXGR3/Wdlbh8/hQ6tFlYkBxCNbRQzH16AwEHDijP2Yu3/rANFR1U/3LXsk1J8iL/x/ak4vEl2UxpU9TZjYyOQbxDBbb+4V1s/ehD5DV00Osmkpcaek9k4iXfo5Yr33PhhC1x/gasTtSgp+ISPnrnNzhX3iaLnNntW7GAMyISMvHMs0/Cg1q7Rz79BG/SfdvPZE9lM/Wi6Npl5DJUsHheKKXjhqhy1Ec1PkricRtkyLKyVIeg9A1IoofCgavX1trbOHjkLK4WN/G+GtlX3wtdlPnbI24TjY6duIxVMsCNU7ruLv1lnkJ/+deTvahxtdFfEjMzsYD0l7xj5zHcwzhYWTXluvyQtGANVx6O+B/vlDL20IN50WoZJdiJjqOuqfMO/aW9Uw/NHfoLZe+E/jLSq9BfovggNNNt2TtBf+np7ppR+ouSLNGhQys1gkMZm9kSpEb3/3wDDZVMliIFZtPLm/F0dgIaIx3wbyeaMdxShiGDETm80RXXSYqpV2PpsmWIoCRVW+0tDGozkRAVodBfJBNQMTLtLPSXWiYzZU/QX9pIf4kgc9XJ0shkph4luG5mApOd/tKoa75Lf2FZgmmC/jIT8wvplERrN4z0l7UT9Je2awSut/dhzpo5eDpxHoY60uDUXYGbTMbya2pCgWo5/o9/WwXfgSK8veMMbnM1P8KV7BhlIsdIuRmjbFu/A6X+mL0r2c5mrrpr8k+hULUC/+3f1pBLegtvfXYKxYsyETY3RplgWaWDlKxoael0BXfU0t286zgGY1fhR9RKDfR8jMdA9jm7TYkF7tBfOAG2xVet6G0sw4kvDnPFkIVfPrcGIa7dhF4MoYqJauOLTTCyE+wZdkQkmb0GJjpePbEPF3V6PP/6s4jnYKuykk28nPxj9iUVIX749P0jqG9PwOhQG1q6h2GO8oR+aBDDKrWSTW8aN3JV6ojg5MX4+3+Jpg74AFrKr+A0Q1hq4oEGDe2270W68XsDGGIyoDuf2aiopYjNsKKzQouxT95DXkUb1qQwD8JtBmeuU3IXvvs7sdFfYrFmY7xCf3F02Im9hXmkvyyHN7GVo53taK3LR1vvCPKvXEB1EXNSrD1YPicWazO8mYRWBl2TI57akqV4b81mN4SlrcF//+fFKD5/FHsP5CG3ch3isiPvVAI8dg8iWVdCf5Hh1JGDma/7ENqbdKioIBqsph1efuGIpgB7Q0E/akvy4d01QEF6L2RTV7bh/GHk69VYvCILLu0NnOF5UO+QiUe1VTA62+gvvVcrkXv9MgY8KGxvVGGjOgQRSf44Ul6C63SfOLVcxZinBkFxFC8vv4TCwutwG3TGlZJeiui7Uz5OGZOnv3UwpttedgwfXbZi+XKK5asaUQ8PLKOuZ5BHCeO1tIlahWZmAfqFJuPpF1Yjwt+RiRntOGkUUswchDEYbjWOoLm8EnHxG6GdoL+Ihmhb7QT9hdm9nblltIkHmkl/KTN7IpX0F02CL46TMKMhwWBMdw0W/0gEkf7iQeZebv4NmJqNuFY+gIxU8vYYM56ebLl7zezIVbImOoH0lzyUFoVi0KUdOrcAJPta0UgkUQe9BobeOmZ4eyBqETsya41yXha62Kx0rakdqIPJfTgHe+BKXh4pLgb0FV2GKZjuFnZQtaS/jPoEYoiTJwfa2swyKqZNQs1O04FovYG+dkLCK6FrbUZ1JWMhjMX7mxrx+dY/YG+JL159TQ19TxM63RgOmCLpxXstMPvbN7OAhHFGUF9dhWomnzW7V6CmPhxx6nGc2Pketp/pwuafrYS5rw2dXixj81KjkUSo6xEGDOuuo94rFhu9x3Hr/F78/tMTmPfMa/CzDqKRxB9Pp0HoOsbg62JCFeOeXfR+Lac2d4/agfGxq9DqNSi9lg/naJKBHIhOKy9Cq0MMMiI8MMh8DfP4MFrb+2GMWIqMxHDQG4gbTDLSjASg5HohXAg9cDP2obywjudFD1x1PbnCzCwPYlanvf7umxlh9lNTZAETJ1qtzOLvGmeJjb6VkpWsVw3JIpC9B1VFDeghymPOhl8iaBn7CX0HHEdG6cmLQZiGsG9CNIoLK2EIzkZWlD/fN6GrowEV9JD5BbEyhIl0zizD9BFmqZzvxIrkMQdSgqVZjhCboKcfmXVjHoF4+pm12LbvJH5z8SQ8AhPx7I8WYXWGH/zM+3Hw6Ec4wyVx2qofYgnZpF7hDij7lMDl359iGUsQ1rzwY6RHeqDgg7fRlbwGq1dnY2NGDQ4d/pDfA7KffR3zSTTxG3VCeckO7H/3DRi9Y7DlZ0ymmRsJ/9EOfHrwIK4fNyApezMWZDJ5gFfIZMxp3xw40wlNXYv0/B049OHvMEZ3Ucbal7EqezHmMCT30b5TuHzyIMtfUrHlxwuwZH40PPmgjQ1p0T1vAUYYywzwdmPsjnEYpyDEMdvXm7Ns2cZHBnBz52cYWcxkhkXLsCGpBkf2vAc907NXvvgrzElLhGvYFpS+/Rl2vJ0DB794/OD1pZiTroErsyE/PbIbl5jSnb7iBcxnLNKDSzMpwZv2jWL82tiF2JBRgCMHtmKUIuMLn/kx5kR64+r2XTjPXslA8e+4BRvJr82G/wjJP2V78dkbObAyVpG6lPc1IxUuISZUvvs5Pv4TY+QBSXjxtWwEOvXhwJ4jCHjmZSYsPYG0gr3Y/sZlWN09kLb8eaRw0GwtPIU9+0+ii/i1C8f2U2jcE2uiBlHZyQ5ypAGnP/8ABZpgbPrr/4In58Q9bi3YtJvz+3sAhjP+P/beOj6u/EzzfUpSCUpQYmZmSzLb7bbbbWi03RhoyiTpJDOZzXxmd+5nZ+/OH/fend2BDCSdZrYbzcwk2RZZzMyMpRKVWPd5T6kkududuG1JHSc6iVt2qQ6953d+8MLzba/CiS+PIK2sE31mfG50pz21MxK1NWRdsQO8dnIvSrID8ODuF/Bg0gNYX7oXn7yVCktHD2z7/l+xdAGoqquFOSdSpdfPoLk4HxGrt2JbzDg+/eI84QU6lr644qGnf8x3KBxdtn2oeP8YXr8xAeeAJPzwZ2uhGW1H8rkjaA/7AdGG0zj2wefIJyzcwS8Jz738GMvp7KAd3o7S949zvym4BK7EC9zPdqIVZ7/cy+zhTkwT6Zi0+WnmQQTAZnkg/W6aLEuXagtTsPdsDoaGzeAdvgYv/Gg7LHX0bpw7jomkn2H1Q6sQLCHDQWL1GBt1HWfc28MW/ZWDjIxpuZolYUrpf+n16OvAzRP7kNumh7mdB1Zv24O1YcYEUJNn7x7pL0yA4WpJz5mbNRM2bLianGRmaW9PN/qFwEAKiZHgYYZRlj50d/cq9BdHkhQcKcqgYpp5f28PegnNVbGTc3VzhpqB3pSPP8V07Gas25gA65E+dPElEPqLEFIcFPrLJAa5X7de6C/2cHM1EmYmqNDfTZcuIStwcHaDM1cZUnwtZQ2NjY3M2HNR0vIX7emyBGaIZBET0cWRqkFCh5hm7acomwwOj8HKjhmqzkyCYGxYNsn2HRlk/RkHFGtrxmx4DANVP4T+IuQYif0K/eXyx1/Cet2jWLMyAmqDjuoxQo2xhAt1ie1IrhAiTj/t3qOX+I8DbeI8Q38hH5bnNkyYwZEZi072NsoxJSV/yegvA33oFdqKmZrEDFeuiFm60ysxCVI5OAFxIJ3B0YGkGyZs9PNzHT8X4oaWq3mtnXw+plA99AOjsBCqh1aNpryLeOtYF54j/WV1iDNB8XP7ORJ44MCktYlhxj4GhhlfF0urYEUXnB1X7PoBnpdlGpO0vYoxLnt+387aklnXY2hublYUaETZaHn79haQkgZRzPr29BeSW3r7ZugvnEsyz8Cez36EbWGcggzyDlONg5m8Tix7Ig+UySA9/cOMo9vBRUhHHPiE+DHMZDwRNBH3rJWNHZNCCAHv0zMreEIRQ3F2NvYVk9Rb1Uk/xSQlGxI9XJ2s0FCQgn37M/DwX/wMa4K0GGB+xyADnRoHRzg7ayGvrNK/dfdggAl0NvzclVBxM9a793X1MlRDgRhLUmuYtevA915ZsdCEy/SXr7ejxaa/DEufQ7qP0F+kzbg4mqHw2lkcvVKLXT/7BZL8WAojl8X+VhTNxpi4acdkpAlmj4vYjQX7Chu10S0/QVZuH+lSA4Zx5t7Y8vk6krjFPpe7//HSX1hE39LUpdBfHFlAb2qMX38Ud/6JqK00MMFH5PCkRvB+2yaJgmpt7GZmI1FrHAgXYpN6P5lciJSbZKrdV9sEhTrIo2yd9mL2tjeBvAvRSmSVbpxcLNNf7r413Lf0F1KnOhiSqum1woqYIIaYjBPdu7fE3J7L9Jc5W5j+9l3QX5oEGm9wQGKMPydFC9Nn/PHSXxhzdeQMUzJORzi7XYhNZrPyR4pn70fResm4dSK424yqKTLjX4hNBlKxiXR8kq14P20S33XwZokBV7jmkzKDXJirl4FUbCI2XooY8sJc9R/XUebTX6SN3Tcb8zxsnf0QS2EXM5EtXcBwkNhEbCHt6n7sfxbjGZpsIpOMOxfuuPsrkQoHR88AaCnxJ96IBeoylDFF+oxl+svdP5v7es9l+svXH98y/eXrNvm2n5gmZ/cn6YTZ3ZysysR1ITcZRGUSf3/aZCEtMXcsmbTKH7HJkk3kJXuf2UEL+XzlHuTZqvife242sqC55eKUCxaj3XrRswbjl796UvndN60CTPvd+ntp9MYHM//z231XXm6BDTsxdmFnd2td5tyjXeC/fdUoJpvc9t7vzH7zr/B29ylxQKNNbm/3+XaSBjCf/jL/2Iv199/3jL96ztt993afLdR+chwT/cXd3Z3xaiZ+LW/f2gKy0ujs7Jylv3zrA9x2h9sPcMo7cNv36TZ9ifL+3fpeyKnupE3d7pJuv9/tr1NWXUIIWaa/zFlyPv1lKVakc2e+w7/9nvYig51p/JL7kPwXi4WgXEinLMYQeolssnQepriAmqQTaxbHGzeWZbBBjbGswYYxuZk47uzvhOwhRdnzlfWNv6SrjenJk5TC0WiYeGLaY4Z0orKwhoaJIqZtjAlHoxM8h63ma/QXcassxP2azvX7fk5SBnBKCsX5RzaF/jI6qdjE6ivZfGP8rjmvzUSlmBofpf3GGNjWKAlDXzsPA+TDQpgh/cV2PhFHbMKEJik7mSPp0O6GYSZsmNEmNopGhRxPiAXyR+yxJDbhNY+zDlToLwrVhY1Rkn2MGwU+2HZmHcxf/a58ia432d9CSDimRjCz9+wPKt6MT8w7h/yCn41R/N9iHuFDEryEPGPcKOgwEy+RznFJbTJzBX9KPxaN/jLNd+mWTsNEf7HgOzbXIIT+IvKCopU8255oYKG4kNcCS/W82OdMOzNn2/hayExK+ngcpZUonSoHSREHkYeltLOZc8w7iVBqJmauc97HilvX1K6WXbvG1i59jowZQgkS2yz8JvQXI8lFWWBIfzf/ocw74Sz9hc/ZtCk16hxtLJhAO3d1LNNiXy1Jb5aKGhDVt9hvy/XfY/nLFHVO65BToENgeBgCfEh/6W1EYWEpGtv6KRTsh4SEWJJLrNBTV47CMtJfDKS/BMVjRVQwnBQ5wGkM9zUgJ7cObiExCA1wn70oob+0kHRSWGGkv4gwfiTpL2rSX8ryc1DZSPqLhvSXlfGKEPtgWxVJNOUs0Aa8QoUw48+61qWiv5gegZHokp9VADPvCEQHUzmjvxk5WYVo0xlY8hPOspxwuDNLlcMr+jprkVM8gMi4cNYx2VG8uwNFuQVo6hyEtbMP4kjP8SG1wPSiTzG1u5FSZEb6C6USExIR7uNK1wLrc3OzUUP6i5r0l8RV8bS7Bn3NFcgpqkDfqAWE/hIf4cva2iWmv/S2o6rKSH+xcQ1ATJg3awLrUV7XzYkVM2dJ8IiIIZ2F5T+G3jbjdwfp9nEJQByfoeVUP6pYd9rWbYCl1gNR0RHM3GX2scnk7PQG5RyVVegdmoScQ2r+eDBUkwChGybhw9EdEZFh0Kqn0VxRiiYdM6OZ22nl4I9onsPBWj3rEZm/cjedYvnnnVnAZDvTzzvbS+ZJrPvtakNDcydT7gMQFWSkv7TXV6NC6C/TpL+ERCI8hGUH46y1ri5HVXM3WP+C4OgY+JP+MtjNer+yaugpUCP0l8iwUNaRTigylSWVzRhTWcErKJpkGRdMDbOmUNoU6S/WpL9ER4XDzUHcjLxiyR7vbEBJeSPrCjjBU00xE14LP1I/HFQUAKkoR/cAJQndKGgeRf1szuN17Y0oK6/GILM/PfyiEBniOVv+YrKF6eed2uRP+XsmW5h+Luy9spyquxXlFbWz9BffQKE83SpaLxm7OirPlZMi5uwbhnBvR0yNkg5VW0PRmy6o7B3hHxqFYHcNq056UVtFAfyOAUqSuiE0OhruJjlbXvw90V86Gkpwat9/4N3jLQhbYaS/5B5+Hwdu1MOcJ8m9egx9lhRLsDXg2N79qNBNwnqqBskpzXBlQ/f3tMcUy1tSjryF37x3Ek6RSYgM8jIOpOwc9c15pL/sR9MwZ5/dRbhQ1AtvH3bCtdfw9pdXMczVTXtRKkp6LYklE/rLPqTV9zMBpRNXThfBVugvRJMtCf3F1BImSXRJFqLLuxhwjUGMvxaFZ0jEudLMVdE4sq5fpoiADzE8juity8fhj/4dey/oEb96BfxczFCefACfkFxjwQlAY9pJlI64ICQogIMfhw0hnZB88Q5JKN2UFRxuycOlMqG/eGCw9CLePpSKSaZwN5L+UjFkpL9c/OhjZLMgXTXSgstnyuAaErTE9JcOXDj4Mfafy0KvYRDtQ3P0lw+PpqOiro70FwOCw4mdUg3i4oGPsP88vzti/K4/STHNmUfxxmcnUNtMHVbqMutsAhAe4M30dOMMckTfhnPc78DFHKo4GffzcbNG0cl9+OhMKXp7a5B66RjG3GPhZTuN5A9ex6nsYtTXVbBom4N4aAAcSOqQEMAy/cXUkO/up8QD75b+cvHgh3jtK/SXi5/tw4krhci5cR2pBT2ISgrGoNBffvcF8qhkVpqRhqzWaUSzXKU+/SC+OH4RxdkpuH79JrQRa2A30oADb7+JC9kNaCrPQGpmJ+XjPNCUcxK/++wsWijWcSP1OjqpqxpHvJ9CfxFJuMosHPjyOIrLi5F94yROpBTCNSgM7RlH8NHxZJaOVSM1ncInrAt3N+vC2b2v49j1Qg7wmUjjuTyj4ikHOldqtkx/ubU9SVxRkq8Wi/5SlpWCN1//ArXtdVyUdBLp+XX6y5i+GVcOvY1/FfqLSwzL6GwJR0jGB29+gfyaTnrxmJLk4IdA7RhyrhzD21Raq2cZzCT7GEe/cHhQO2GM93HP9JcUSnedOFdICUC3WfpLb0O3Qn9JWE3ptb5y6hsaUEP6S43aAwkR8YjymGI9I+kvrAWcpOZpY8FVHL2Uy9UJl+F0P5o2E/2lwyECTz32GOkvrfjtb06hrqocDYU58IhcN0t/+fWRBhSQ/pLa7YCHZ+gv50h/aW1sQs+KYLjPeX5Nh1+cnwrSKxdnj19BU3U9VnDFOczVaGpGA9ZsfxHb10eg8vxbuFJP1SNSLooOH8Ppy+WocwwTQR7FFTFp7oqHn16NYFc1SgfTkNon9WnMfnQwN9JfitPR65qIl57YTvpLLV57/QJZn0J/yYN/4hz95T9PVqGQ9JfMPjc89QKFDXxUpL+8iYbGVq7yKNT+TX6OhbTM9Cg6hf6SrcOmJ57Cxrhg2LOmS2tB+gtd+UJ/WZcQPUN/sUJH+VV+tw9bdj2N9bFBrC91gvVoD9LP5yFs4w+wa0sE2tK+wMdZVdiSFA1nP+LgeI4Oyn0dz+nH1j1PY110IF9OJ1gRn5VGT4Zn/OPYsZoqNCffp+5xLwapWANKDG7f9TBiI0Ph4uAMJ8fleOhCPvZvfywj/WXSit6lgFr0cwUo7lk7Bw9sfu5VbCG0ob7wEl5//zKKytZAQ/pLf+Bm/A3pL4NV1/DxFxdIf1mFFet/gP/+wEvQ1xaQ/rIXBfTEWGlbkKkLxC/+9ntwG6zF8Y+IybqSCfu6XMRuf4UUF9JfbpzAwZQstG8W+gvL48zJr4zZgl/9zzX0CeuRmXIE71zsgdVgDzFqfJf3/BKPR1si88JhJHPw9F01gZLaQTz/X/4BIaoWnDu4DxkljYjzZb3hbGjr21tleY+7tQArNEh/cSX95Xsvb4O7O99xJ3o4TG49OSzLnYT+kp1ejYjgaEqsmsHQVY+srGx0e23Az55ZBXcX7seSyd6q68i9mY2AB57B7rXBcHbSwtmFGrw8jClAdJeuXSP9Zf3uFxHCzvHfD/beSn+5lI0hQwPpL41YT/qL+Vg/RroakHqc9BfGpnyF/sIV12B7Ec5fLkH8lieoaVgDe0YxTBcmVfQTo+OIiKDbhoL2Lta2pL84EO8r9BcCrYMDFNanlXU0/CxSYT6ih1N4OAL8/Si8YI3oVd4K/WWMOrRYkoGUBeG6Fly/nAzbhIfwHKG+ziS6TE4wtqeOQ7BvALxZ1kN0BG7kUfTa3AlbvvcTBK8Ix38cNSiYLzMLqvlsegRhU9SAvHQGl9Ka4PKkLQUXjI/JRH+JigmBD1fmrozHxLvz3rl6H57QIoorKx8vd1oxGr5II+x7AM5RpGT4+LIxTdEmXqhkMTvHMBaO320jvfP9phk772mqQfcwXSgt9cgeboezP8XEw+mKZoxB6C+2KsbTg0MRG+qNbvkuC917W+qQPdRG5RiCyL15oQxrjQ0PYnCIxfns5Hrbh1i2w1gFt2nGk4Uw0816hd7mWmQPtHK/BKwiOzJghS8Onj2L/gZ7rma98ItnguDswJio2RDKioqpDT1IKcZYxuz9SYW5y1fhzs2x/M1vtIAFtG7BeHj7FKzH65HOmBObNMyZS+EVEsiHTJB30ShGmPfgqBH6yxSC6Ir18wukwEsXIuyvopfCCg4eAaS/0GHfVsZ4phN8XDScnE9A7cXViJ8/3Jg7EbbCHgeIeozVmyGRKl/+QR4Y76RaWvIN0l9EZ5chMP7P0oZCL/wzTFGGnqY6RG98HuFUvmmj/KRhcID6z3Ys4rdGb9sABSA0nARTMF9PwRlrxpXYlHr6mcNAaszy9l1YgLHwGfpLTp4LQRjBiFphx1UpyT4zl9PfIvSXamg2PofVvpQsZQ5Of3cz69PLMGnvhoqiLNRQbW/FmlWk/LShvryd0q6tZN32wMEzCKvWOcBT9GdnNtNxTf++w59G+otfkD87aRdYUxBcfN1Tk/0UdNbBxtEO3r6e8HO1ok5hF1VtetHXZ4VVxK797Kc70FN8Hek5WTh15AtUTpEUw8FvaoovC/cdZkKOaVOx5seOwGUT/cVCCWCwcTI5QDOP/iK5BjI7sLGdR3+RgmqZgSxRW54aG0ZZygkcLwO0LuyY+dK2MN4zyKQhM3uKrCtJDtRboTtaRgZzSyGt+8OfNAsrAsql45C7sKTEnTW/a22vpRA2B5eWdvRRice0ifi62IQ/aHMScWQv7qyiEowtGVJG+gu7Av5eanE1tIm5dEy0g0LEucsnbjr/t/mp0F/6urhoZKfCms9JfS2OvE+prdpeuMfvxPaEMKh1Ffjy3X/CkdQitLV3QkVlrGkG8yf0NTj87qcoIpQ8eMsKtOSdwMcfvI03v7zIomoDm4DxRiS5ZFjHWBnPMc3kq/G+ahwmYSavtgPWbh7E1TUiOysP/WxWkvpuqXFA/I7HsCLYGe3FZwkEfw+5dONwbrG8fWcWkGQzqhZJMhyTN259Z6fQVp6HK6S/hAj9xdOJk27SX7hyZLPmRvQgG/ss/aWjHNdIf+n0SMDKKE8qYzEu6kOpTH5Z+ggrCZHI98kodWWegqQPqcwtSXFR83gzyUQmO0wbqP9bhOwqMzy0lhP6UBJh1gWh+OJevPPBB/joZCr07KM8fYMQE6TC6Q//Be9/+Duczapne5LENTnj8rb0FlDDP3wFnty9g679dpz59BPSX67P0V/Ge1GQfh03C3uxJtETk8P9zKruQkcXAQgtnBiNM5FMNYSC5FP4cH8yOqmg1z5AXAifNQztuHb0EA6n1HASxz5l5hHfwzR8ptGxE5exSjqp8WHdHP1lJ+kvwWb436S/OFkRreYXh/C4WEozGZB18gIGumtRXEdQdXcJxnrKkJ6biVJNCDY8kAQXPydlYFaZTaC2sROJlBv0sRuia24Y7jP0l5bOr9BfSANpbmwm/UUGHTt0k3Qy7slBbIka88TECJpLWjFYQwLFGMXSKWg9UufAZCMthiiM3q0fpMyhO/qF0DLOmcxMpuG0yX58IpJw0VpOwX4mP8Rt2AEPrTn+x7vVaOnoQ5S3A20shib9pb4TGxLH4MiYYivpL/4K/aWJsYBeJHIFPEkXlJH+YoGGmibGDRMVe3bTZktPf3GAT2wYtm57hMzQJrSkvY5m3k9CdCLpL4mkv9Ct0lWGTLphXYm18otfz+8+Sp5qI7/7BpNBRpG05UX8Q/zDnFDo2LgJk29zUdBwIuk3wUxeGRx94qIUwoz3RD3aMn6HwoIMtDGGtfuV/4pNsU7IPPYuTl/JRrT3dkSuewiR7OpaIrzR9Y/vor61C0mR3lh28EoD++42yVoX0REzToDNFWbeFHqYh3Hm2AmuGJIU+ounVRdKDdRLru8m/SV4lv4SQK+NQdeE1DOHcL1+FM+8Ksg1a9wsH0VDbSOGx9fChpMtvX4YfoyfT3Gi1cSOM4na3kP9pLgI/YXnHRf6Cz09IuE50tPMeGseRgO2Ioa5FnaOltj49F8ifFMXpSybkXzhPHLNHeHkHYVnfvmPeKCrG13Nhbhw6gRl6YyItu/Omn++Zxb6iyP5tpsfDWVuSCunWp/hQF4u6S8Pwp6TpuGudrQ15KGzj56/G8kUsi9HiwU5snTzu9NzEbRtJx5dw4oTunvP5bZiyM+eiWZh2LbzESRat1If/QiyKX4/siNCpmGKoe9hIDU+KFmJztJfWJrgqKHyfmMViks4mFW1UcfUH8FBCQzu9/KCs6BhJ1pksMcDvivwq82PsNMfYtZuBQz17QiODyROzQptFWUYIWPSxicCurQy3Ey7hj6bFhSPqfGYizf8I11woqwQqSTIWLTcwCiFhD1DmHlZloKcnHSuckh/KelFqL8N2J6V1dhiNysLKwdsePFVRO6h23G4i0SXVuiiViIkJAQG56soY0atzVQHKlNK4RDOOJ6CYKErm7MQKQORGOkkyS9FZyi6Px2ABzaEY7yhHpbUEhaR+bbKMozbOELDTOCuzFJkplvDa6oWZdN2iHH1hHu4I07n58JVzXgT6S/TzoHwCKQOb9413MxKx2jjKOkv/VgRu7T0F/egcKgu3kRhvid06nbU2Lgi0nEaDSWU+OOqcqS3HnVtGoRu9kUQkWlXCvJRkE8hf/M21Nowrs5MO/KxMEANzP6eNnIBDdi0OQLONtOoSE/lZImxVAb+VRwkC/M90KlqRR1Zk5td7dCCEbrWh5j8omHGOmManGQMMQ6S00TdVWqzdlQXo5mlQmscNPw328mcM2Sxm8vy8W+xACfhTC6rZTZsBTMjm2xKSWrxRqjrBM589jY+udSNXa9swihdbx32LD2wc0FTZhrSiGQcrL2JevtQI/3l0n7SX85hJekvtmM9bFf2sLTmyrUzB9fSvOA8XEVviAqrHw3CwHAuMq/fgMuAG8oycmAV9CCT3Uh/KclHi1k41ke7kiBSiaw8PTb9JIl0INHMHuGgyzg7E0x6WlvQ36GiCH4EdX6ZpNap56A+jOamFjRPeuJFTtKsF0i68hZTLf/jD1pANLZbmO3dMUpmtaGVaM4++HgnKPSXisI69IBVDY/+Al4bGR5iuMmC2rpq8zDEx7uifKAcmRlZyGPorYaTNTtPitv7maPOIQcZzEOZcupEG8esCIEScDE0MuM+uceB1Eh/CY0wKKLjanaSjzz+MD4i/eU/rl2ErVs4dv9wNTZzReBE+svRs3txRaG/kNjBUpYgNyZ+cEVm6LPG2vWPIHRFFOzVY0g+8AW6orZi60Pr8cSKGhw7/TH3Azbs/ilLR0ilH7FAefGnOP7e66S/BOPpHz1F+osf6S+dpL8wG+/8CCJJDllN+gvfO2Zj/kHb3/MXBCHnFhgEdx5pdJAz05WrMRTJ0hWvQLg+vQ0ffnIC76WdgzZ8A368Lg5egqXhAGrNTiEsapoiyEJot0PctvXI3XcMH/zmNMtYfLHjecah3c2R8danGFxD+svajXgsivSXQx9gaEqNLTP0F2vSX0ppky/fTgOcw/C9nwj9xRXW+lZ8cuIgbij0l6fp7gpdUvqLW/AqPBqfh5PHPoTBzAZrnngBif4OSP3kc1wl/cWg1iCM9Jcntq6Gj2YQjxQX4eTRDzBipuF3X1HYtD0Fx/HBkWtoJ/M9lu69Zx+MgpWhkdnAZ+Cx5yVsXbGalKA8nGDG+Ii5LdY98TK2rAuD93g39l84hOSjU7B3jcKzr5A4ZNaCT788grrGdoYCiPN78mU8EOVP9zrjsPfcCpYPcHcWYLlCRzVOHTyO9PIe8mYv4TCfxp5HIqmR3U+QhQ43Tn+Kshx/0l9exIMrN2FD+V58/m4aq188sX2G/lLd2MBVpQUq+J69UUr6y4bdeIm82T2rq3D883cxbaXFym3fw7YNidC5DKHmvaN4O530l8AkvPCqrFhbceX8UbSF/wUSg20ZBx2HVegmrI/yYKdJLWa6AKsyT2Ev2aSjZlqs3fosVy6BMOtvwo3DH+J8UQMsnEijIWFmVQCTW4yLlbszyfJed28BapPXMXy47wzpLwYz+ISvw4s/3AbL3hJcI/1lcuXPsWZrkpH+MtBCd78NXCYiEEValv0kkXhMSHs9fwpeYWvw3HOrEebNMsXWWnx8cC+yzewQxbHqexuCFb1ew0zo8N7pLxQQF26fKMGIO2SSs7Y+klmM9BcHUhOcFFGBMTbCblLIx6bMoSWBQct4iDHGQXtNSco8Ffd5DDNmySV//DlUsQ9izYYVbNx67ten0F8kg8peBAi4dBhiNmsP3aVCf3FxcYJGzk2V/h4SHRT6C7O0HLliXVL6i+nRc3IwwoSEKSG6WDFZhnqwfT29tNM4rBUSgT1nQMa3TOylkCRsrGDF2KhkK/dLTHmAjDxrki14b6rRblw10V+SwqFmyVB3d79Cf3Em+cZWob9Msqa3G73KfvakUpAww+NNcKbfTVUVob9oiZESm4i3W0Q0lor+MjqoJ4FjQKG/ODo5UxCc9I4+HdvIMCZJebEnfcN4XbQbYcm9/O4EY1ZCcbFnEtDYsB49rPscZ0KAgyOJMFZTqMk+h7ePd+O5H5P+EuZO+pxuZj8rODnzHNxPaEA6XR9GSPCwovvXycmeiVljjNn3YZiJJWora2gdaT/WkEqsQ5SNlukvpkZ8dz+lpOFu6C9Kv0FC0BiTDOndpQfBkipk1qzfG6KgBmkuFNWQ2KaGtX12nPUM083fO0N/ceY7Yk0Pg4C2DUJ/Ydxc6C+SMOTEleT40ADbRj/j9JZK3yMsSaFW6fhODoxwMst8BFe6betJf/nkwE0I/WV9mBvZwAYMjargQFCE9CMSjzfIeekSFFqT1tFIAZmmNm8/3z09Ox41E6Kc2EZtJEdjZlumv5gsMfdzUekvHE8MhLXrOD6Y6C9OTPAX+suxq3Wkv/wcSb5ao1OWY4mILIwxQ8yWCaIiJqRn/zvAXB0bOy2fJftqeqvG2I/qhE5Ed689aVXamX70j5r+0spCaw0b4zL9xdjwhP7S1tQDezdi2fgAF2KTer/7lf4yzclbG92y7SovhLPuWLBsC7GZJhfL9Je7t6ZIBIocp5+fn6IudPdHWuI9WTvaKfQXnTVWRDP7d95AeK9Xskx/+boFvxP6S30D2kYckBDlt/D0F3nIf1zbNLNenRU5LsMCXZsU2pvEtEXSaXHUNBbTikLEceYKnrPuBbKJDKRiE1lB3F43dDHv5x6PzcCyvXco6S+U52KS17CUON3jJjaQFanJJtJG7r92co9GuMfdxYYykIoNpZ2KDNz9Y8NpaKgkFu3MkAsnanSyLcgmNpF3TN43scmyRKBkuhrbickmiyMR+JXHxyQUR3cq3THzdoJJZwvxeE33Ie19mf7yFXv/ufzzT4L+wkC/JGst1LZMf7l3S0qnIoPpfUk6YQcv1QcL2abEojJgLNNfbm1b4v2RP0tJf5GBT2qrFvL5yrOV9r5A9JdbL1AuWP7cOqs3fib3Mv9z483NGflrM9iZYxkb+JwRTOcQq8wJkN/+HPJym+gvAvb+2jnmTr9gfzPdl+lcKtaK3um9z96bPHTGjL42VvBAUiMq967YcuaqTfspn822FpNNZr47czBpAGITgZ3bstbUdJ0LZoCvHMhkD/l4/rnk8/n/lkIqubXbfRHNMQMAAEAASURBVG/mU37f+Ldb/3vn+82/FtN55DNZkUp8T+gv8oLfel23nm35X1+3gNhQVl9CfxHSyV2tSHmM2w1mX28nxpWN6SrmP6u55yttXr5x523DdLzZn8r1GP81/xzSSJVeb7Yxzp1DTmpqonItEiNdpr/MWlQZG+bTXxZnRTrvecip5z2T2Sv5hmc7137m91VzxzO1A/mexHoXjP4i9BIpqDZRGIR0YmBijZH+Ygq6C/2FZJZJo0iACeQwwVnJJNVHOF7wGGrSGYyJH7M3y2CwQjphMFhj+xX6C8XIVer59Bcj6WSU63aNnS2Lao1HMREpxK0iVIil2CbYKU8zOcJEm5DEFwOTXiw1dkwqMl7Y1CRnZaSViGKRiE9YMjFJKXtlPekQE3GmWfNkS+LNzG3MXTalCIdJxJHkiVvoL3RJDQn9hVloGivTfU6zHIeKQAySa5gIMZPjRDfxHP1lSWzC5zjGRBAzeQYzNbRyQ0JioR9/dvCUz6RNTPMzk+3kM2ab0FZU1OLzU2ykfHjrf2633zQTD8bZHtSWczQQIxVitquj4AezCWY2sYsMAEtiE9NJ/4R+yqT1nmzIDk/pC77ykOWZqfhs5r8L08r7wzbBGnITwUdMOUXSyziblRB/jO1dSDGUSGAfZSIsyfdE9WicNCoRS5l/XPnd7MbEQenfVGy3ljMkJ+PvWPbHagCpeTXtK4QZytvzeHPtSb4r7clkk2XXrtF6YgeTTeTnwm9sR1JWOPOai5DNV5oUTynPltQxaT/zf8k+Y4ylcmq2H46Vs5s832kmPJoSReUXpvu4x/KXKdb21SA7X4egyHAE+jhiuIe6twUlaGStgo2rHxIT4+BDMkN3bSnpL7XoYUq5O6XfEilzZ2/Rj5wMFsrqhqj0Q5kwr0jSSUgxZ1adbNPMdm0i6aSwUugvLJOIT0BUAEXtJ/pRmjtDf6GMU8LKBPhTPnCglaQTFtd2U5PBK4SEGequOrJQ2zSDUA66BP8ZY/lL3s18mPlEUpDBE+PdNcgrKEOXfhwOpAwkxJFeYjOF6jzSWjr0zL5Vw8aetU2UC3S0GkNFwU2KuVNth8XeYUlJvGcPWM9ge4T+0lCWO0t/CV+RhAg/N5iNsj43m7JWrX2wtPdCIkXwvV1s0ddUxuLhChIxSH8JTSB1x2/J6S+DrP+srKwilWdCIbrEM5nD1pw8VAb/m1kU7x8VA09nO2ZJUmCipgp1rb3M1LSEdwSl3aj8NN7fQqpHnSK2Yevii8jICGrkktShPEt2aKNDaK2d2W/KkunukQgiFGBM147S0goK4E/AySsc0RH+sKXIR0NZMRqZBSylV1aO/tTcDVRE603txPRzCZrKn9wpTLYz/bzTG1ToL50tqGsi/UUbiJhgV6Kq6CVgtn9zbT262HZCYmPhQqUiyfLv62hERSV1ecfM4egZjLgoX8qHjkFoMeXVbZgkgtAnNBoBrFluqq5EbYcOxLwgIjoGAaRR6TtIH6poxCAnbY4elJCLYH9EcMFsv8m2MdTXTnpQBTr6RqFxIokpPhJaZngrhBDKWzYPWyJyRQwczOjJIAChqokiEaTUeAVGMQmOpWcz76zJFqafd2qTP4fvLY5NJtHX1UIaTw2rFZi/bWkDP1J/gjxN9BdKqlJxraaiCq29Q1DbuSI0JgJ+hAwM9EpfU46ufpGc9ENsVBhL5igcxDZUWdfFNqRFUEQ0QvwdWd4021rugf5CxFl7fRFO7CX95WQrwhMSEOJpiZzD7+FgaiM7cyvkXz3KInx30l+GcWzfAVTpp2EzXYuUa83EHAXAcboKr3/Az9v6lBKRcUtX+Pt4ws6aq1g2ZH1TLt4j/aXFoMJUD3V5CwnH9fEy0l8OJGOUq5POohso7lWT/mKGa5/uQ3rTIOvOunDlVKFCfwnyZZIOpyWSpqyhdqfw7xZ1owBAydWD+CfSX4bc4hBDVYycI+/hPMUQrC0nkXLiLCaIB/NyGMORN/YhRxRa+L/hSWveuzP6Kq/gnS/PYooPbLSqGBntKoo6+MOZsoBCf+muzSD95RB6iWsytObhYtkACTcz9JfDrCFlZ9CUm4LyQaG/jOPiR3uR1zXBgbYVl0+XwiU4CL5sUCrO1vR0SwjoXGbMi7WNDLTj/IGPcPBCLlFuQwr9JdDfDeMthfjkP3/Hut90BK1bjyBOhPQ16Xj93/eiqp3F9JxMnCk1IDLcA/2lF7D/DCcJlTeQfO4si+dXcdLmrogoKDapycAb//Yxajp7qO98E2fLhxHqa4OCU+/jS5JkWpvycP1aIeyDY+FmNYLLrD8+m1fGGsUqdE+Q/hK2TH9ZqOcvIYO7ob8M65px8eBHeO2To2jWRGADB0ZrCiTU51zFu//6Gk5SXDz2wQfgTXzeQHs1jn7yIU6nV2CAZSw9o9aIIF5N35iLj1//GGnFHVwtsoTKlnWB4y04+Nkh3GB9cvK1GyjV2yMh0A61GSdx5HIBaklNupGSAeeYjfB3ZXnUTN84OtCGayf24sNTqUTzjULfOwafiDAKgZijszYXb//Lr3EppwmxmzbCZrgDVw9/jpSSShRnpuJ6Th/i1nHQ13BFQ8NKLHCZ/nJrC1t0+kt2Mukv+9FAHF5zdyc1DTgpnx1Ix1BfmolDB0+jqqEaKcmZqBnzwEo/IPXs53j3aCo6WuuQkp6NUW0wKPOCC59/iQvppci4eh1FjWOIWxvNRY+FEvu+N/rLSCeuHT+FM5dKUGvpQTejrCDHoWvsgYtHHOJXUuqvtwx9dPPWUgC4lvSXFWGxpL/wAlwm4UpBd11LLcZsmCkXTZHxCG/KMwVS6N24GjXSX9LR7RiFpx4X+ksbfvufJ1BfXYGGglx4RVGs4YnNsG65il8fIgPVtg3pPY7YtvsxbI6xx7mW11gyQvpLQsiS0l+6a3Nw9mQyWusakUj5+Em6adVusdhDw7tYTkCflsuZbh9fzGFU9XGVvT4WqxMD4e4VQCnFKaSmpsGJIgbhcVFw4srNd9INMwt0hf7SXJyBPveVeOlJ0l9UdXjtd+dQWxOAYapuBCZtxe6dqzBRcwn/cawaRZakv+g98MyLu0l/Ac42voHGpjb0c1W6VPSXLlI2jmf3Y/OTT2FDfDBr8iipxs6ord4A71VxsG2rZO2WuGAmMEAWa8OQM557dhsF+Wvw+mkd9CQZxSU+jl/FTFNJphaH33iLEwADV6zgqpYv5/Q4BnUdaBx2xfce2g6niRq8dbYbLU1UKMlvxvYf/grrQuw5yfoP5JZWI8Teny4lLbaR/hIXGUYKEQkPChv21hd9+V9LaQG6WCnNN23jjaSgOurXiloagQ90qY5zReG3Oh6NmT0cHOmnI9ChtiwLFytVeP7l7yM2wJO1xS6wm+hBys0UVNrE4ycvclD0cmEdsiMsp0fww7/8b6wzHUTmpQP44FouOnatQeSm5/B3G83QXnYTxz47wsGScpM8vLH3mWD7LENKSh3W7XoRG6P94aRlramTBmP6RhQR0VWnDoQfwzTiO7Rhtu/W7/0cD1mMoyj9Mr483kCNbboBacK5NctS2vPP/VxG+oubL+kvr2yHh9Bf2Ebm6C/m8A5dhVf+Og7q0XqcPnYeOS06erXYtsqbsP6Zv8IzcWa4dPIE0q9k4aFfPYEnfvo32EMv4s3z+zk5z0Bd51MIUGQgjba+S9eukf6ygaoyYStC8G/7Z+gvahJPAu1ReDEbg8NCf2nA+kD6n030lxOVSGPD8yH9Jcbaknq7rRioTcPZihKk2zkhafuz+P6TG+DjRNVTBkom+CKFR5Cf6Ut5L2sN4rlqsVKR/jLmQNeJP1WDXLnCjISvRQ/pL/1wiggnKcaXIhDWiFpJ+ou9OX3dbM6Lt+Ca12JJf6H+5rXL12CfuBXPcVWttSM5na7nBx57EhPEqV05cxGpTf14msIT00MdpE1U4vzpcuSlW8GfqkXf37mawgWEmesKUFZM2TLKm21+9HlYWRpX0Qr9hcGfqJhgeDOZw4WakvHuFKXn6t0w4YQorly9PYm0m44iYi2N9JdBuERFwt/bG25u3I/0l4olpr8YySzkqJLMcnOwBU7UXN6QFAH/hI3YSuHxwswORXSfwW5oXSkTaLEf504yLDDaAv8VL9Nl78xVM112pSXISUtBSss0XrG3o9jEjOmlyJ77BZjvx5kTemLXWhGQ8AL1VJ1hQfsMUtBe30f9VHPSYShWwYU44xoEDBQWUYFKj+CQOKNrlwIOy9t3ZQEKbbgGkf6yBVbjJvoLo4229ghduwWTFBypKb/OdkItXOYadLEt9VJIoaOmFP2cjPtFrkakE4HMdNlPTa9iZ5iDZupchyVuRFywGwIDnTA12MR+aIpiHf5wsdfQOzWK1qICpKWl4WaPNR6g6AJDpcaNNaXdHS3IbR+BKxWXsvRNcPQNx4Orw9FFAf288kHs2bUDY+Wl1Hymuhvj7xrrKRTm5dPblod28zgSmxgX/a7M+Wd/XnMqXI1hRE+WbDYpQKS/RK+wJdjbRH+xoHfQCurxZuRQajKf8qvum7cztKTnM7Nl6CgI3qSWhQX6o6C8HuOWtlzNutK10M2F0SgmHRwoGWn0NshkSba77D2YNaq2hW8g1YxGKPZs1qfEIacYu2zu0sHOxQF+/t6YIqqmq50YIjsq9eitsXX3Hqx078K+z64iO8QDsR5r8f0fr+VxyNNsuI53D/GmwjgYrA9VLk6hv9A9YqS/yMVyfifRY6G/0IWphCCYKCK5IjLzs6Hr1oIJBTIVNJeCagkgm+5UOeLi/Ud0ckuF/lI6jW3UjeUEGy0U3B8amYSnE0UUhi2oO+xMuSlQUagTA+6ueP5HfwmfIF+o9eV496PzyCK/cJTJQnCKwo/2bIJ5QzI+u5CJiEBfrI90V+5REi5EyYixcy7GVMoDNNJfhIhjpL9M0iASvzfSX5hgxH+IOL6ZYpPFs8FXjyz0F0NfN3X2qe9owcnAYD2OfVAAS9u/xNakQBa9UxZxJs7AdCu2KTu4ezMkUFCEESalrUySJCwmBBh6UJCajBvJWVQrItB8cJjJIxwRRSCXTd/Mkm5sLyvyJzk40jWyeqU5bBjTSkpw4eD6LkqImKytZYbyo3ugobLUip1PQNNO3d+Si7h4PBN/8d9/hQdi+GCWt+/IAnz2fG81TKwzl4Qe0zvLtmHBWKcNJ+j8VHnJJ+kmNTC+pZq04nNXY7i1CAduVGH3M2vR2cakOodJKlaBnONLuFw8iv/3vz0FH/tJlGWn4UZ+LzbveRaehCMYOuuQceUSbqYXsbPwxOAAVba44lVWLZJsMtyDQXrTJKFvytCEs0dSOBHbjaGiZHS4b8ZqzuTKqRjW092LEUdzetcqceliMtKKOGD7hlLLl5M3YtyWB9Pvokmp4RdO+sseC/S01+Pc5+m4Uq7H//zZTuI4lZZEsTlqO5OTfeFaPko7xrCSOSaDjLdbU+bWmYOkNDYVVdcsmABrJgMMPYtVN0mMyW8j9vOHCOZibf52lwOpHGImW40dtLR7SVlX6C/11thC0seenSsV+ss/nu1FDV2Z1r6xCIuJRgLpLxnHz2Okvw9mHkFcXTlzFemOUac+OB0swQA/H+XxLDlgqswnUNPQgcQE0l/smVDSOQQP0l/IeeGAzUFqlG6VwV5006UcQEmxpga6chXkmC26mYI/5rV09JdJZsy2lrVjpKEZGZc7UJ5RQCF+Z6xdEwcDbe7i6YV1O3bDfroVb+R1QT/qxsQHQq0learfBlGa0xjs5YyINx+/KhpJcfGwcO3HsXMZtMkQncQzG1fk1fWkvySR/oJBtJD+EiDZrFONaOroQSJpKJNMdpqlv1Q3QjecQBH7aSLtmMDkwAzrJZpfyOTGylYL75hwPLRtJwLMmtCcSvpLWy8M04HKQD/NzlI6UVC9qSb3HHLMNuJv/3kbHPvy8dsPLyBvZQy1mv3x6Eu/xObHd+P83n/GlcwibCaCTeMihPoBVOacQ556E/7unx+GvS4Pv+WkpFL2e/kfkLizjTKBjYy/vY9+LbNxrTgBXPUgwik0LvSXzv/1DhpIf1m5TH8xtbDv7KdkTpqb6C+zGbIzZWPM4jbj7NGCnZqNlqvMxBXYsZOr1Tp3tBceJMklGlp3D2p2b8fOrQGocbRA2UE++wEOdLX5OHo0DTZr6KLbFK6IjcMhCM/98n9gx5P5OLz3TbJ/S/FglBdlOc3YZzKTU+1IT9hq7Nj+KLwm62DR+xZymdzUWdQJM/8SXGccvyy9EG0+8fDxfATuwevxN/8zCbvyUvDxGydws7QF0V5a8lHlLVjeltICCv3FNRCbHwkh/aWNnozPsD83Dx1Cf2Fi5xQ9VWaUX43d+jxj3A/j/NF9+OTGSZRYb0YfExtbeoaR4GLGfncQQ2p6PSnf0FSWg2Mnr2PKayteeHQNUX63TpHuYSA1mkayrkz0F3MOZs627NyZwZZfoEF3eQscnAKYLJOI5qxuVORnwKpVjyLKND1A91zLjTO42WeLpPXxsOzKw5CtP1ydHdHNpfYYeZ02zHrVp5Yg48ZV9GhaUTJuiUBXHwREuuJ4SSFuUJ/WvDUN43Ze8Ar1h2NZMnKyb8CiR40bJTqEB2og4S9ZxC72ptBfXnoVUU8NG+kv463ojUyEO5OKrnz5KcZ912B1tC/qyb60d/bFQNMV7L1iiVVr1sJdRVzTuBbbA5mxbOGDq/XFyLxGlzhtMmrrTL1ZxhTpRhrXODKBKgI9pL+k3yCTcboO5UTGxbl5wSPCGafyc+DMbNiROiYduYTAM8gdDnlXkXkzFSP1YxQEH2DGsIZapbSJ1Bgs8mZGsop7YDjMLmQgP8cV3eoOVGsYK2fQf1LXghLCtasoNO5ZUgp/J+qnspzJnFmPhl4dLAl1d1Fz+sCVflUhkXRSVjDZx5dBBTtfCmdOGjhZycawo4dS8mPO1e+QTkeSwzicLZhcxWQ4Hb0hBtbwtTXWo2LAGY+H+1CnuBUZFT3MqAQ6qwvQzFKhtQ62y/SXRW4Lv//wQn8ZQHVZCUorq9BgXYyyKk/EhzGxkCGRYraP6qYGtpcSeKwMgotPAEb57LOyCb3oLEezYwC2RQYznOSD41lZyLHrQXNpA2zdQzDSVoQv3vkdrusi8LOdarSTTDVC11xHDXGFGi2metuhG7Wkt8iB3Nx+8keL0WoRCDdXL4RaZyCTmfV+0x3MJLfFqqfWImjDag7oxLs1lmOEYZrICD+oGKbJZWxN42KP3g6ullmD7GTPUr0Zb8vvv/fl3y60BYT+0lxXiXbCTcyH29DQqIOvb6KR/lJQC73aF97qPmZyj5NJO0Yd7z6G4bScOLliuGIcqVeSYdsIZBVWwjH8cUx3lOHAu2/jTJUGL/zEkc+eEysff3iJp3Fmu8eBlPEBW6GXjDAYbw0Lob88sQ0fHTyL11IvK5lSu3+4Zob+cgRHL3yGq4YJxD30PaxPWAGOmyjbexRfvJUMMzt/PPL8DxAX5ICst19HV+RWbN26Hk8m1ODo+U+RPKLCA3t+QvpLOLSGp1Be8hlOffS2Qn955kcbuGr1gzMToPYdJaz5ktBfKGYu9Beu5KXea7E3qYF19ecLyBNJ+UvnyrWkv8QQ3xOKLZtW4cNDF5B+YQSO/gn43g83IcFVj76Wz3D60zcwbmGDVY8xqWEl6SRRtmjd+wUOf5COCY0fHv3By8xoNUf6659iYN1uAoY34vGYGpw4tpf0F0tsefYXZHuGwWqG/nLw/QyonMPx/Az9xUbfgn0njiKd9Je4B5/BqugQaDiZ4sJ18TczKwj95bGEfJw4tRcGlQZrSWZJCnRBR9EZHD1xBf0TOqRfOg5zGy12RGxBYv5x7H8ng4kntojbtBuxQZ5ou3kEB85loYMp6bbe8Xhh6yrYjneQFnIG7k9+n23pYcTnH8T+t9MwzQSQ+Ad3I8LLDhVnPsWJ9BIYLDTY+OQreDA2EJMduTh58CDLLDpYa2yHjbtexkYmXy3TXxa/OXzzGUh/6azB2cOnkV6hh97sKo4yHOH48g4MFF3CqQtplOzT4fLJI1wx/hjbwldhW0ghTux7D+YObtjy9E+QwPyIIauHUVz0Ofa+dx32PrF45gcJsJ4oQdMAO9TBGpz85F3kBIZix649GKtMx6m0MujZ2bqHb8SPHorjgNiAG6S/dET9GH+xIR4PPViGvVytTJg7IHHzU3h4VTzcbNW8DXp3GJqZ7jdHeGI47AzNqEg5hrTaTkxaO9L19yQZuN6M033zHS//ZhEtQO9WQ0ka9p3NwSDpL74R6/HiD4T+UoxrF04CCT+AnaYJF784w7ahh51bAHY9xf53FXNJxjuxl6SuN1Ms4B+3CS8/Fo2Jzjx0dRKqMt6Pi4c+RF5gDHY+8xKeWOk/W2e6APSXMV7sBBNjjPQSoTjoSfYYZMq4WiFuaBUKyZiBBAZSXMYIzXUgncNBhAYk41LfR9LJMFQM6LpwNWpO+kvKvi+gitmE1Qr9Rejlei6uLZjty8QTrkKF/jLM/Xq59FYxPubM/YS2IPSXXir3UwsC9szS0lLAQdxFUiQuAu0uLi5w4Gx00Te6u4VaMcVgjRVLS6bHDXQv0iasZ7Shq1OIApbmRlqFro+EAsZhhFhiz3iwQrZhqY7cmxnjQ3JvqhHSX/YegPW6nVjNRB31CGkoPUaahRNtomG8RvYb7OulK8tIjXFmxqI13Q8iBCE2GSH9xYE0FQcRtaC3SVLyl4L+ImVMo0MzJAZJKHIiIJliEbIC0Q+R5jezMFaT1CErb6E26Ck2IWIUDqTC2LEOeJxtp5/2GOU9WDEBxVGjQkPuBbxzspf0l11YFezE9sAOeN5+tjzWSL+OrpoR2tFKOa/EZKcmRkjvICVkhLwHrpi1WpKDeD2yeFimv9z7m3F39BdOdtlv9JP6MyYhHf4xs7CGPTmxUyyZknijcVPB2taBtB5zJVFM+g2qvsCRFBYNSxEk03+AYST5vlpD8ouWWbWsRe9XksxE+IQJTCy+1xCfJ56OAcbax6csWMNNMpCDGeryr+GTg1nY/hevYm2oO1eoA3xv9awNZVsU0ost+56ZK5linzI2In0cVyUMLQ1Kn0eiEBFWpIM4sCaV35358jL95evtarHpLyMk/vTph4z0F1YKaO2nUXDtHE4k12PXqz9FjIsa/foBjLAfFJEcLduANXMuREyoj5QXaXK2zNR2ZL361NgoY+g8FtvPJD1fQibS2DmwTFOtlFWKktc9D6RfN9G9fSIiDG0tM/QXvkgLsckL1NDQoMjhiUTg/bZNcYbV1tILexdXOBAttRCb1Pvdr/QXERVvqylR6C9hgaw7tjR1b/dmGdPkYpn+cvd2vK/pL811qCMbOS7Sf5n+cvdN4I72/C7oL80NjXT3OiA+Uugvd3SZf/BLMiHo6OiAxULRRP7gGe/0C5yN2nMVZsakg4W6NlmRyh95yZWswDu9lj+S70kcWkvmokplpGosxGXJoCE2kRXE/baJPew8QxDKJCXVhGHBSB0mm0i7k8nX8vbtLSDvmLQrseF9JbNIz4mNozfLaNjDcnU8PLEwkzOx4DL95evtyGQTWa0vRZ88zdIprZsvHCjxN0HPJVVDF2Qztfc/SvqLiAEvpHSUHEsenCj43K9alwttExkopBGI0tNSNOQFabXzDmIUljaJks/7xT38VWwi7UQg9fejTe7h1hdsV9OE9X6kvxjblCQnLmwinthElHykXS2OruyCPb4lO5B4xCSUsvT0l4V9vnIf0o+qeDP33GokFnBL2+MHxvncrR3dNzXUb/rc9FRv/3ueY2bSOL/h3+670pBbWigGwPjckrl2v2qUWZvc+iBvd71yY7ezn8ke8vO2+3Evo03+sN1l9SX0Fzc3Kiex/nYpNrnm+c9KOadyr7de7zdei9zcLQ3tG795yy9ue16xFZNG5jd+E/3Fw4Paxuz0lrdvbwHpVMTVZaK/fPsj3G6Prz8r+dbtn+vt9v/mz/5gk/qDX/jmY5t+I6tzIYSITe6rVbrpBhbh53z6yx/jpPVO29Ys/WUhHqyR/mKs8RKby9J5mGUIagZxJQnIuJFCwmX8KLNFJVtytgyHxc+yvJ9UWVLBxubrBcySWEQSyiSTjWzn/X6aiQVDTFZRMdnAluIMpnOMGoaYWEPCzDz6izwomQnKanQh7nfmZL/3h9Bfplj3prYgWYJuIxm4xieN7kJzJiFZipoE731QKC9MNrJjEtDMvIDSaCMYYuKCOdWcbJlE8bWNcnpDTJSQYvH5+00zdihJXuZCf2Eg3LgJ/WWQknqkv9AmprI2aShiE7HHkthEityZKizZzRYzYt5yfRNUrphkXZeaRfeSBGXcSG5gOxGyxtxGG5LUoJL9b/lcvjFHepBxViX1iLw35Tc8r4kaM383IcXwBLeQaOT7S2oT5Qr/tP4jk1ax4V0TdPiuTJLI8lWaC4+qvEuz1lLeqUkleWjmUc/+StqDiCtIIb28e5OKm97YlqTdzyINFfoL85XY9ua3tLkDsSyH7USoMRamk/B44rkQARGZhMm9Srud5Lst/5bjGM8h5zEeVd4vk03uV4/YnE0W5m8maoqJjLMwR51/FLYBee78v/KcRKRm5nnM/5b8fZqdjdCmZn/PPmOUgi9qejDn+iTjXiJsI25is5lfmO7jNr30V0/z+/49iX6STW7m6RAcFYEgKvMMddcjP7+Y4gADsCapIykpHr4uNuiqKUYB6S+9HGDdSH9Jig2FVj2CisIslNe0YnTaASEJiYgJ9Z0tdp0S+ktp7iz9JTQ+EdFB3pR26kdxThZpC10wY11iwqpE+HuS/kIlpWzSX3oGp+EZsgIJMYFwolDt11ZBv++WFuB3o4NdyM3MI/0lCrGh3lD1VCMjt5K6sZPsYChOH5PIelhLNJTmoLyhG9OWlEKLTUJkAItnBjuRn5WPpu5Bkkm8EZ8QCz9PkgaM4wKLxYdQV5JD+ksbxsztCAtYiSgKWpiN6lCQdRO1BACoWVebtCYRPq4a6BpKkVXEc49SXzIsAYlR/qytnSPiLLpt2PBECrKionKG/hKoPBc79TgaWMtXSXEJzhgQSrKPiIaP6FqpQtSEUY0vokN9mBmnYvZtN6pI2+joJdlerUUUhSx83bSKopW8JqPDvUqNoW5wlJMSFVx9Sd/wd6csI4UxSIDoY32qpaMHYqIjoKXecSOPVdVMwQ4ze4REUyjE24WlCnOr5UW3yQK0sT/WQ5hsZ/p5p9cp9Je+jibUNjAD0ikIsSHusJgyoKW2gjWlbRRpsYZfWDRlMF0wwXektKQMnTqDonMbGxsFN2ZXSj85xWzc5grSfQbUiGSpnHqwDUXFddxfBjZOx1me4hcSAc1YOyrKqkiPYcm1XwhiSBSy53thHPrYpoZ6WddaplBjVDYuiCR5JojUmP7OWlKsmpjByYm5lR1CYmKhNR9ABWtd9SOixMUqAXMbuPlHIsbfSUFumWxh+nmnNvlz+N7i2IR0INJfSqitbSCm0sxSw2ceQ6gKc2/mj4xckPRS+aiEZUsuAVGI9LZnX0VqTCnbVv8ktB4kQ1FMRiEOsZ8ZYs1xaUk9WMPIkkN6F0TxaGYz/3+4mf7xbX4q2bW1hTj28b+T/tKGiMREI/3l0Hs4lN4MKyohFCQfI/3FDe4K/eUQagZUxFjVU4+yBc4+vjDvycR7X5zCiNoBKhY4pzdOwJ8auu4UA6YCPvoac/De+wfRxkFgWleC8wV0jwj9pSYZ7xy6hnEra3SV3EAhBRiE/pLyyT5kNg9RWreH9JcCihcEkIyytPSXaaG/XN6P//PaexhyX4EVwV7oyDmMj0/chG6IvEKmUtu6emCyLRNvsF7JQFk8M5JtLuTJvXlCz/rKz86XwYqqCS3pJ1A84oLQEGK+rPjQ+OC7STp5+73D6FNZYbStABdL+xHg644BElLePcoaUqbot+RdQ+mALTzsxnHho30o6GHx0HgbLp8qIf0lmAPzEtJf+ttwfv9HOHQ5j52WAZ3DagT6u2CoPh1vvXMQeU06jHHA03gEwFkzgayzX+K193nNE35YFxfEyRaxcqlf4F8/OIlulq1kZySjbNAZMWHEwbEMgmsB9LWV4s3/3IvCmmq0tNdh0j4I/o5qZB1+Hx+fr6JCST3SLh3FqGsUPKyHcOLzA0gh6i8t9RoymqaJ9AtVXhZZTYmrRtz/S7JK/zYv3H3y3Xuhv1wg/eV3nwr9JRIbY3ww1d+KS198hjPXS6lHfQPX+Y5EJgXB0JiFvV+eJO4qH9du3IBOE0rsmiesLFjfWZePd0hmuZBVj5h1KzHdVY5jh05S6KEM+elncCy5AG5hURguO8v69KvEMV4nGSgL9uHrEORumpxNoLOBQg77DiOjrJT91Q2U6O2wJtyJ0oNn8Nt959He1Eb50yG4s37VhiIfKSeOEIpQiaK8NBw5ngy9xwqsD5droj4wV7XidZPSuz9GN+Z30bQWl/5iQCnBAm+/eYALkia09nRTnCNyHv3FeMdjfY24dOAt/PpgOqZJ6kpwG8G1M5/hveOEpRCzlyKCL7ZE9MmkbkKPwmuH8L//5TDBGq54YEOIgsmT+5Bne9cr0omRLlw/eRbnr5ajwZK6utLPs56qr1lH+kss4pIS4dTDgmdmwNWR/lJHQsyKkGhEeEQxA3WKiKFxlFxKhwMLp8NiEuCOQHhOsL5UKuO5ybFaSzLQ4xSNp0l/iSX95TekvzTUkP6Snw/v6A148snNsGm+in850ET6Szsy+5yxfbeR/nK+5bdob25C7+gS019qsnH29DV0NDZzxTRF16WUatTxPgMQm5CEQEoF+nk7oflaMWzCt1APcif8LWrR8X8+Q2NdNHzUXnjk2bXwdzZDsf4G0lgwPCSkcir7THEgairJxIDnKuwR+osZ6S+vnUVdbSCGUwsQtHIb9jyyEhPVl/FvR2sU+kvWoCee3WOkv2ibXmftKOkv0UtMf8kdwEO7SH+JC2bdJmv+oMflC6cwHbAKP3xoPYI9tHB2cQQoqDHGtrQy1InthW5fuvhYCMvZZR3GPVZh6441GK84j2MdvehnoZc3Jf/YUhS3moVtKJ58YhNiYknRYb0s4bRor26F94rHsX21K4pOfICOrn6oVqzAMz/+FZ6nqHXNzWP4p/30nnRtQ5jH/VcWJe/Jn8ZmpL+obH2xKqQBfexLJqi8Zaf1xEPf/zke1pijvuAifvP2WVS27cbDFFD4279/gM+4AWe/fAtXsmvw/NZ42Ix1oujmVdRZEXTBVYiKK0Pv6Afxq39YxTmoHlkph/E6yUA+Ht6IDn8J0Q+r0VeThy9e+4AennZSo3xYFsGT811z8o7ES3/9f1FOcADpJH58cKMA7U9EUJxBA9/EXfjBdnKWPVxY/83Bd9oR3//r/5shinHUZifjsw8uIY68SrXJjfSn8ZDuo7ugPvekBYT+8v1XdpD+wufkSIjF/NjO1DCV9rKRfbMBsRExUNP/21VHpjEVqjY++0s8G2+GiyeOIzUlG3s2h2GyvhSZ2YVwiA6ml1NCdOIwntvuciAV+oszNj79Ml2LIfj1lz2YoCiDGWMNzgG2KLiQhYGhetJf6rEhgFJ34wMUia5HKuGoCv1l7cNUBKHgdD/1aZuLOVss5TLZEutJf1lL16dsImE3QVZWREQg5Z08yX6zIf2FQgZ0z/ROaBEh9Bc2ZEt1JPzU3bCgvJdzeBj8fX3ZkVohcqUPipaY/jLY06TQX7Qc0J5j3FZrr2KHYCDbbpB6sFdQWZQPC4cAPP70LvhTlJ1LMRb69qPfrBvDujpq4k5h844dsFPpkcmB5lJqI1x3M04qenbchP4yRaxUJB+ml6cnnOmrjzPRXyadEBniB08qrkxORij0F3O6gV3pcvdjkoOrK/dL8kaFjRmJOIzjyBi0yNs0Z+LdzTXooTu/q7EGGZR7c/aPQYz3JCqKWjEZ0IHa4my0VmoRt2Edwuml2Lx1M+xGSrhypReC98tqacoMJmLqwFkcH6rGREsrVsrL4WQ7c/V0sfEFMR+vpqqNFe3Zh1DCzsPcHRBEtav9J0+ip8oOde2e+C9PB8PN2QmWHGcxRg1owyhRWxx4bU2Q8EU2yPLhv8ECJvrLFCzH6pBO9xsjAuxjbOAR6MeGP4R6enpGKMbh7mwPrbMbnHmk/olGxt2H4EfCiw0nRk0ks+SWDmL3EzswWlqkxOLVFDWx5p8hJkC11VUjasNziPJxI73DqFA0Us+JmBn1vj3ohp0d+Kjna+sEdmWYGmAsneAJF9cQaJmU18PJXQ9rmLNc6HamS3hlYhy82RYd2OdMDbWjt6UJw/4bsDrMfVnZ6Bue9uJ/bKS/jJL+cvOmI0lZQYhOTOCE3X4m5kk+dXMF8kprod1MlT32R/XM0xhhP2WusiP9hYs6PzVC2fYKyhvRQ3xjbW4R2m3XY88qVzRXE5TylXirTL/uYmOwlfQXH38fQqKdSH8RuggbJOkvTZ19cHBzRGCQH0K8NBRK70J3bw8Zkhqse+pl/NUvHoOO7tgcQnCF2TdF8egXf/pzvPp4KLJTbnJmyJjZzCZJARqq/Yg6kfSp5hw4lKxNqpPY0K1nnDxK4NcY5LemyojiOuF3zdT8rtzsrRMH06EX/KdCf0k+rtBfbB28WDw2xRVmG+9xEv4PPIm//Pv/D//yv/4eL6ycwLWbJRh2iIB1VwEunTmM40ePoKKtA+NmTIYi3seKExJbR1eEUD+4q5nC21T1MW28WyW5inkyStCb3iwloUIlRByxCW9ZDCM2keC5NXU/jfQX/luEC+7yiZvO/21+KvQXvdBfCA9gp2hGisaJDz9BekElyF/HEGeNNhozNKafpbTjTbSLBCATypSEDOW5yX8oQ0n5wECrMdSWlKOeFB2xAbu4mUsxg51zAJ589ikOxPYov3ICv377KKo79XShu3GC1YHi4jKMsGcWVRIl6XeawOjiTFy43oi1W1YhgCvi5e27tACfKBN6bGy+Qn+RS+Jza6Fg+OUrhQjbuBtR9OaIQ3+Cg1bmtcvI6XbGzvURlOtrRvKlK2hxpSeM2nz67h7KulFreYJtiEzShuoiZFdbYOvaGDjOMI8HWotx9coF6HySsDrSE0qkYJ4ZpicGUXyTXqEiPbbsWMXJmwsCwlbhuUepDW5oxIHPP8DeC6UYGJW2OIV2rmhysmsRv3U1vLRcEc871vJfl9ICaviHJ2DX04/BaaoHF/d/jtf3X6Om8owu6ngP8tNSkMlQYRKhKaP9vWwrnehmbo0V4+FOSvtgTN3MmkmxhIQUpCEzLZcT9DBOqgbRzgzsHt3gLUPLXa5IjUZRGgoburG74wpyWIfieits2U76C12MHUHAP57RoZbybdZ+sQghGzPeLwDhx86RFacjLxOIjohg8kksnHyHcORUCob0/RiHD8zY45mR/lJbP0d/aVHoLwLfbeaA3aXQX6wHe+boL/XN6Ekc4sXZKqSTMW9msM4PLi/isxT6S1tlN8YIE8+82onydNJfGp2xfm08V4/+CPQJgpcL9Yg7I7C/hrNch1g8tsudMlZMhIEvYzJO8HW0Rkd5CbqdPRGzYRvctSr8/dtVJLz0IdqHcU25fj7YqrqOGfoLxZk7DAhkZ2FB+ktjp9Bf6E4e4OSFg4YMzA21Qn9JVOgvnUJ/sZdZ19LML+R6rW+hvzSjJfU3CsXH0tYdqx7agkfWBqNG3Y7/vNyKHlK8/clwNRFALDihmBjoQOaJM7Dc+Cz+dWcSunJP4N1TNxnsZ9JSgJZtjxnZVk5YuWkbJ1oGTt6skP0W4+alTqg5k4EnX/lbPBTvgvQjb+JMSi7iA5xh21eII1+cx2jkw3ji4QSGGe7pNVjEVvXndWh5V03P3pzZ7jI4ddUX4vTRU2izXI2f79oMdztLer96kJdyDmfT27DlqZ9jY7g7emtrkF/Rix7nUqR261HGZL8WrzgEE9FoPt6C4qxcjAVuRYyfC9QUexnqrsPF04eQ2TaN5199EiFkVYrM5jgnfVJBoFYxJJV7FceI2bNfvxuPbwxVJu8WXhHY4ReD0Z5wWI/vw6niRgw+kgB79DN5KRfFqkD8Xbw/0W9LOGP982omf/Bupxln1LoGYNPOEJgp9JdP8WW20F82k/5iieGuDnTSE6pjbDMn7RqqCsoIKtDDO8mVEpPso+bRXwYoS6rva0eVbgCqHMbja6uR32KF+KJ4eLtTvlXplO+aRzp3L7ISneBgKsOpuYUVoblDaKotRXYOU35KmqmrG8RkmSS03OxEWW4aLJr6UGRwwIMcXHwsfXCukklGyfZw6C/GsJUD3aE26KwoxQjpL9bMeu1PLUY6Z55dNszCmrBCMOkvQVFuOFZUgGssDTFvzcC4vQ8zUv3hXH4VWTevQdVF+kupHpFBdkxIkZXb3PUu1t8srLTYSPpL9NPDzCLtxNkxriSjk+ChnUDKl/sw4rWSqC5fNGQUw8tvFSwGyS8sM/AamZnK7/fZ+Sg80epLJ5Ax4ol168Mx0VDNGZIWdkxYaCktxjhJMLa+kdCR/pJ6zQIeU3WoUNljhbs3PCNdcIKkCkdyUkbr06ByDYVXsAcc868gPeM6htyE/jKEpHgpqZGJ/uIbRaG/BEXA7HwaAbpOaFd3okrjg8cZk/Dtq0QB0UYOk91oK6tHYNyjcLSZRB0zJQtLK1A/okFxqT8s/S1FFhUqDYHOnW0YYrmMmjzWSeqglpElOeYWAleVjqtZrnoJ3a3Kr4azayA7XBuUceU5ysldZ6c5Xex2LDkyx0AHk08++g0OlGjx0iordDdV0KMSCg9H6q8ub9+RBabJnB1AVUkRissrUWddiNIKd4S7T+H0J2/hk8s92PXKRvS31qOB2RSGqmS8/ptPMRS9HQ+pSQcqb2AcLBg//qtfkkc7CV1zJUabehEZxZi81SRDB+XkSPbjwZ8mwFVLF6yhG+nn9uE3+y5gza6fwpzx9MpaCwSQ3dxSlY8W8zAEWdbh/TdfR9pgNF7dCYUmMkz96j6GKEYYJx3prUE1ZTsDQvlOEr2ma60l4SgP/mtfRaCzCSD9HZnzz/y0EwZ6RplL02rg+DDMKoD6Xvj7C+iiB+UFNehW+SHpMbKgH2SMa6gNVpw8WZtHICaKCbLtN3Hj8mVY16lws7CCMdGt2PCAP0LDt9EPNow8GxUGSIAP8nM1LkhmutG7zto1PitZhRJH0wtERQUhyNvz/2fvPeDbOs8034cEOwGw9957kaherGLJklzk7thOz0wyLZM7u7M7JXfv/c3s3fltkskmsePYiRPZsS1bbmqWLKuTVGETe++9d4Ik2EDgPu8BQVKynLFkko4yPIksCsRp7/nOV97y/BmINRGWewXnLmTSDeeJAw/tw45NKXDXN+HqxbPIuFGD6M37ceC+TYgKD8BwdSHOnz2PwmYD7nvkADYleKDwjUOonHFB5Jo0eE1U48rFc8gupRtu30HcvyWVyTq+XJnk48KFSygf0ODBRx/mCi0eQfbDHJQvID2rDF5rH8SBnRvhywxgI0fSEa6KRXhAlHyWYxPgthNFjj08PeFKRJyBmWI+SRuVFbeHagRX0y/jIgPXffZxeObx/VgT5oW+oqu4nJ6Bio4Z7Hj0UdyXFsMYnop4sExcuJSO2l4b7H/8ANaGOSP3tUOoZyJOTHISXHWluHSONqvpwbZHnsDODYlkInqjK+c6zl3MRL3OE489+QjWJ0XD36YPmbTftYIaBG46iP3b0+DhzIGIdW+SoapmTa/Uci3LRjkue4qM2wyW0YWWjiKWMazb9zge3JqCQB97FF3JQMb1fAxrUvHUo7sR5DSMj99mRm1xM4bpmuvtnWJZTBLj3mpUZGfgYnom6rrHcP/BBxHjbsDZN05iwisYzlONOPbBUXxyKQdd4+547GtPY3NyJLTWo8jkeTOu0u7GUDz+6E742/Xh7JUSipEPo7+1BtW19dCEJNC9684V7WrW7hdtB3eXtcvBr6sax978AHm13UwuI69XZ4C3lz3KrhWih+9uX1c9qlkWNevkCbvRRuRVtXL1QCwfSxXa+mcRn5aGmAjmU/j7E1bhQoyeCok7tsBfbUV0WiPqxvzx8ANpyop2YqQHpdcuoKZjDLo+lscwVtZnpNvWZQzXz3yIOlMAvK37kJ5bhXEm+3USrdbY0Q21J9+xghP48OR5XGeM3zlqF7722E4EuREg3dWImoYR7Dqwj9m/llic2ZqrWbufblXLmbVrnBhGee55ZnafwJWcGth6J+P5rz8Ml5FqnDv9EXrd07AjLR4hzMnw92D/x3p9J7947F4XzkXLLArPfoL0/CYytJms9JW9iAkJQECAP/wDPBlvd6BnwgfbNjFbmwOqJWv3C4vWGw0URmAc0M7ejrE9ZpYyS1VHMssYEzlsHUjqcNUqn8/QtzxEcLVCfxECCOkIROhiYoyEBd2oQk1w44xP6C9X3nqP9JdtWE9OqeM0qTGDOoX+4sJEEWcHMyFFryOGjIr8Qn9xI0vQgeeeZVbrMCkrek40NKR6aCRmSg+LlDWsOP2FaiYmrtDt7FjHSj3YEZZuSLzUkQOLK6kUttZz9BdCuyV+6OpKSg7pJEJiH6X9dBSbUBFC7Ub7Qegvb30Ax437sI7YJrtJHYkuo8yUtoUrbeIo9BIOAuPcb3jMTH+R/eyZPDFLesYgB40pCjJomLmmcaLd6Y6Ql3vF6C8UhBhhnNdA+otQNJwpFiGCGqPsIMcZ4LcjJciFgAIVE8nGSGSYYkKVUu7O+1NrWYpCmPkon7ciUkFbaemKbS++iN+eGcHT33oIKUFqjLENTTA5zZ74NS3pG3ZcxRsm9ZwsjGCS4l12jiQ80O4q0zR0Y6IHazCvymUCpCGAWYg0nJm2t7crCjQiXba63bkFRGKxq6uLCYKBdzRBm2W/McpnyMiEslkzD8KZnoepiUl6E0huETETJgbI87WzmoGe9B4hsEjNvYoJilppJ3NZmRKbn+bvbUXqkW7caZac8Z+Mv8u/2bL47CdIB5nkySRubmRPZE/hmJ66XBw5Xoi93/wO1gRrKWQyyeObqTFyPTIRN06NMZGSnzMg40RiiEAk5LQGVidMUETFgWSZxfWFcjNSHiGEELHJalmV+fnKJH5wcBBBQUFLXhIkAguTeiFGUVOXeTXyfrs4G1Es9JfMFjz23e8iJYAll8qliNDLjDK+OCr0F9KCSPzRz5jgpOV+GumXzNcs/xUBGXadcBBSF/8tC7Q/TvqLUegvA0y2cVNuYuEW7v4nUSK51+kv3Z1DUAsGjgpIS7HJyuGepb9wEBb6S6814cshvhTwWAqLLEwuVukvd29PkQgU6UnpIO+pQYNhgL5F9JeljHGKRKDIJopNVpWNzG1r5ekvY2hvbUXPlAuSYyjqs0S5MzIhUOgvMlv649pIf+HqzIortqW6NlmRysAhs2WR6rr3NtrEje4qig8slU3EHvJHXvLlURdZRivTVa/2CaO4BxWaGEAd52pjKTZZpVtsIm1mdbtzC4irS2wo7fSeGkiZ4+Hg4ocYF3O99lK1KbGg9DsWm6wOpOY2tdgmK9Ins8/QMr9GY6ViPF6PJeoyFMF66Sv+COkvIuVlNvZSdfByHHlwFv3PpTqu+SpX4r9Lb5PF9Bd5ue8tmyzYQzLJliJtSpF24wsh7UQE6+89m6xEO/zD5xAbyoAhq1JxjUsp2r3Trpa+TYm1LDaRCcYq/cXcfsQmMmldUfoLz2nx0C5Vm7TcxxLSX+QSFwgecgL5Ixe8cNHmz/gxPxPhX3MjM5t20X8/1TFyPy7DJWYmmaaWTvP255BjUqh60TnkyDJjEPqLu7u7klyz6GxL/qNc1+LNcv+3vV5+V/l8zk6L7+12x5j/7A/sJ8e7vd3Nz0LsLps05JWhv8g9ms9p+e9im5g/k2uz/JZ/y/0p/1z4fN6un2oflv0WzmM5vvzGst/izz7r81X6i8WWd//3F6a/KM9+4bl/1rO6XRtRrnq+7Ug/s9CoPqsdsIUo7XPxd2+6+9scz3IsabQLZ/jstmahv0jIYHVFarbuStBf5p+TnPKWZ2V57ouftaUNLOy3qB0uagfmw5mfvNxHH5PjbJbC/TI1RVIJC6pt52qnhP4yRgk3WycNkzgsNXrkX45RnYSKPs4aZ8VHbWASwAwHOWnvkvUqg6Bcj81i/zX1Zce53yyrRxU6zFynrJBOmDQi9Bc1A7/mzUw6EfqLs9Bf5o5job/I30txv4uNf+vPswZSXjhISXWJFd0I9g7m5B5JhBrXs67VwVlJtlH2472NMWFKkoY0LNewDB6S9GKg2LI8KmsbO6o3kR6xeDBS9htT6C8L+0me0iTGKNyuopLLAhGHCV10s00rdmcixNxxpLGIS0Ve7OW0iQT+ZfWr9FZyhzyvCETIZcjvppkgZMOs4XlRGTGM0Bf4uSR4WNqUfHeGSUg2TN66vXNelLCmYGSmsNjLssnzmGXCweLP5HezTJIT4XF7Hm/xthI2WXy+P7WfZUUqNrR4f+74/jjJFvqLDZPFLJs8Q+McIcjyGRuPQhNSsY18uu1QW5rCJov7EUkSMbLm2I61yTdvrLumF1+IRAtnXPQNJvFNTc3MtUXzvkZej8hX3toWpU3JOYQms3iTd2y1XS22iNjbbJNlpb8IkUeZTHGyxL5/oT2wZ1XamUyE5ibti/olpf9hItpi+os56Ux6ZOnCWOs+114sY8vNT/zme/0c/5rFSF8dcguHSNGIQ3gQ6S99jSgsqqBQwCjsPfyxNo3lKp6kv9SVobiqCYNMn/MKSWVBfTDG2ypQ09xNMgPzd6lbN2PlgrVb1iKMaklKOTazXVtJSCmpbYfe5KhIvyWy7tJWBIRv5JH+0k/6izdS11PD1s8FurYq3BD6C8O+PuFCmAmDOzNRLDONz3FDX+grs9NjqC7IQz0JLLOUPHRy4f2nxkDNYu2i3EK09g3DpPHCunXrSH+xR1tVPkrrSL6xcqZk4jrEh/iQ4tKHnGs30McB0YaZ0O6BiUiNDVbIFHJxQrdoLL+BsgbSX6xJf+F+ccE+UE0N8hy5pL+MkP7ii7Ub1yLIyxmDzZTJKq2lYLwN/CJTqeQRApdF9JcvdMP/wc6ilzzImr7i2k4O5KJMxemQgyNlJVnAbhxDVWkJelj/6UhKUHJSPAkeDmw/baiqIilmnBnOXmFKO7Ee70R5aSX6Rqeg9QlFYmICPIX2MXd+sUlbbQWqWrqZFe6I0PhkEl20GOmoRWVjJ4U7hAhDGgfreNW2BsoV1qG6qZtZvjNwZYF9XMzK2eQ/MNmfzK/v9J2TLO6h7jbUt/TA2j0CSZHesKUealt9tUJ/mTTZU60mEfFRlMCkSEd5WSV6h/WkvwQiKTlBAV1MsHC+orwcXQNC+/ElOSkBXo5G9LTUoaa1Txn8PINIkInyhxM7wllm4LZVlaJ13A5xa5LhOZeJqTwEttWxwU4K41ex/GaG6lk8D7WiDYNtqGTd8zgny65+EUiMJ0nGagod9VWopF7vtBXbX0wizyESgbef8v3JPOQluJE7bSef75QGDPW2U9GsFhMcTIX+EhyZhEi/OfoLqWK9bZQUJcXHyNpyE5+1g9YbYdFUm5tgrXt9HcUamJnr5ocU9kta6zG01lWhrovVJZwkqj2CkZwcBe0i0Y27riMV+ktHfTGOv/5T/PZ0N+IoUh/ha4f8D17FsdwOOLo6odRCf3HS4/hbR1lXqoJG1YqrV6m56ke82FAjymrq0U08zaV330NWlR6JJDaEUifVmjc33JqP3x76ED0zLO8YJv2luA++rOcx1Gfg1aNXMevohP6K6yjtszHTXw6/hRtdk3CwGkT66WI4BQQT7bZS9BeuhodbcOSXh1HSPswpF//NTj3+tnwwAABAAElEQVSAWrh1mR/i2NU2qN2cUFF4jQXBbnA1tOOdt0jHsXKAoa8U58qGqcISBLuRUvz8tRMUGOD0giLYJmLiQvwJ3xauK1eifQ3Z+PWh49BRkHu6pwTny0fM9Jdy0l9O5sGGghadxVdQQUlGb4X+8hZKhyl9ZaCNT5fDPZz0F046rLhKHFnuOlIBD9Tk4tjH2aioqGN7OIkTFwoQmhyLzuyjOHIqnULyHcjJOItxl0gEstIn48M3cCy9FGOzU+ifsOO9u6CfRJz3T11AZVkeMi9cAALWIJKTB0GfgTbqqSMR55fvIL+1B62leUivGiJhhspGV04jvaQG9ZQDPHepFZFpcdCiC++9+FNktgxguK0UFz4pgw8H5iAvLcsiVutIP19H9dnfkhXp2NjYHZJOWApGRRmhv7z09gl0kP6ybY7+conybueuV6Mk+zrpL33Uiw7HZFs+3vzgDGprStmXXMOgYxTifG1RceV9vPjuZfQPdCObtJhuKx/4qLrwwa9fQn7fGHqrcpGZ2YSQdanw09qhr7EQv/7RT3GxoBXxW7fChxM5y9A3RWpR5ok38Oa5POgIjRinfKU3J2cteZ/g9JUKtFRfpihKLrxStsN1dhAZ7x/B+Wzqt2Zdp6TgAOJZ2+2psVcme+KhkuSrVfrLQrtZzjpSpneh6sYV/OaVD9Ex2IGuoQGoSX8JI/VKwajN6glSyaFq1Xk0NVcjK/0k6VvtCI0KYV/9Nl47U0jlvU6kZ+djVBODSMd+XPnoTRy71ojOHkI/jE7U4w2Hhn2ypY70rlekQn/JOnMOl681oN02aJ7+ouscgbtPIhLIFnXtJ6OPLpVmrqBa7H2QTJWbGO9oZqCSF+rpipCkfYhcvwsDLdnoq6rH5h17iazxNa9GpRMm6WTIPWGB/vKzk5S8q0FrcQkCWGeq0F86LuNH77ajTE3tzRFP7HvsIexMUMOj4wX0dLST/hIJb4vnd+E5LsNPrF0a4Wxm2AERG6K5Eg3joM+Vju0QPuIDiErZjBiyQMPIxLNiPWN3xTnofNPwPAW2Q2xb8MLPP0YbaxhVxiYYNUEUpk9FUjiLgENDKZZtdj8K/aWdNhnzW48nFPpLM1584WM0UeNTf70UEev2mukvDZfw70cbUe7QjHy9P55+QugvVtC2vcg6yW6M0Bvgurg4ahmsIYe0Yh1tyNoH8N9jd2GWRdLFp3+P1wsIHpgeptJREzY/9pfYvTYIFWdewdG8MoQY63G6VE9SzBMU5iBCTetK3Uvi4lIP4r+ueQJTQ2348Ec/oYhCB0apmaqhC83I+uTmyhz00Jbfe3YPPCYq8MsXDqOhZxO2PvQtbLAxoJ0D7UsvZ9O1rseEPVVP6BlIefxZpHiYqP17GkPDY2DZ2N2jkJbJfv95Div0FwNUmhBsjCSxiaOZhf6y+7m/wl7SX5pKzuPnL5+hIMfjuD9mG/7rP98HK6G/HPkVLhc3Ym8EQQgF5UjZ/y08t9UP1ddO4cjVXNS5+qGDXo+tTx6An74NnxzNYf05wyyswy7Ly0ArEWwhDIWIB8zsuBOrG9DZXEUhjxZseezr2Mz3xUPK8ajB6rfjGcTuVKGTHfHxdzgRppfEPtAfe7/2N9jnbI3avDN46XeXUN/9FUSTkSzy1qvbSlvAQn9Jw7PfeAC+Pp6sx3dboL/YaBC96SH8Q9IeJot04+OT7+JkhTM87fToGBuBS9x+PLXNCVcyrqGd6lWTQczo1gqkhYJA5JB6UO/Ada5PttzZXQ6klAO0d8e2J75JDmkUfnKEbpM5+osbkQkl5/MwOtZE+ksTtoZQpmlmDBM9TcjikjmbK03fdfcjgbFDJypKGK0oXUclJFPUFmzbvomuGGnS3BhklDih0F8CAnwo9eWAJB9pmGb6S3QoVSm8SfKwId2EajW2FG7wiIlCEFesLq72iFnrT/qLDUknfD1WYiDlCm9C1wf9dCMuX6Db4IYN/FP34cmdoYrAse5GJkqLTLDVhGLPgxwkGPSMjaX+rp833DgPTqLWp42sODnjGWu6gTNN1bjm7Iq0B57Bsw9vgi+RYQr9ZdZEsDUHaV8fCjKb6S+2jANMCv0lPBA+Xh6M/0UjyDqLdhf6SyzBAr7wILpObFJDkfgZulkZvl3+jXEHWxGVoJjU2FQHKksHsXHXTgK8bXEd/lw1BiEwwA/TCSkYvt6KRqdB9FO0oqe5FtlDzXANTMa2dXHw8PKnlikwyI5z0qAlxF0URsw3IApNs4wph0RFsJ0EwI3xrHh3UmD01BTmINpSXYrL5E7WT9Gdp6YUpdZEOLyGiKQTqHCcRrfnejwV5EMvBt+p5bfI6hluawHSX+jGv38PZQ6mGpDFd4MhLIX+4h0cwL5gDA2UhJykmImP0F/cPM30l6km6JmPERxJ/Vy+A4Zpe8RFh8Av0BfTkZFwu5RBsY9EhHvb4ON3DtMVR3ZkyP2I8LJDV10elcEm8DgnpBMkxdgyJjs/5s1OkPjRgaLuCXh31iB3kBhEv0js3JrKbGRBuhUiIzMTeQPUFWfBvqOzEzT8g1mdIl05xQmgD3WzV2Cueltrrn7I3BSbaYrRlyM3V0utc9Jf6DGN5MRGyTVhjNOOuSryZ4Awg/7uHqRs+x7iwnzRS6W0rg+P41C9HWYm7fHoQZbZOXVyokfsWkkOtOPEYEbEkWEcDbV4Cec2iyfD8u/P+TcTVWycqRnLFRNhuHa8MPF1z86MUDh9GK7kv4VHhiKa0m693RRQHyD9ZVSNzRx4v//Xj0BXeRUFQi/nGDfWW41Lpez84lMUesfiC7BigoEjVXAW6C/cQbJ4GKheTH9hf61s9qJkwqCyTC3N9Bd+zJ9XYpOgtY06iEo7f40f/fTH+KcffBP66mzk0aXZy07BM2kn/vKv/wK7AkdxMbsU7f2TUFOqT1wNNB1XohzcOC/WBm/Dt//6h/jJT/4//NWza1CUfhWVjOfJbchtCvlEbEJXvXk/+QV7HSsm2Cg2kS9JbIb/l4QiSbmfp79IAsecrfitFdsEdt5SnoPCWV/GHMKox0xRCSdfNkRJHKLlVIxjU9VpkhQGFRM5bNnAbSY7cPr1t5HP+KoMcFNDLbhy7jiaNFztJ4cRq2e+EeV2aTdvDyaXMe5lYpuR9ii/nqRbJ/t6NuXc2pjUNE5XNlVyKB2n9fTHWH87qhhnZoSEHnNzYteKGWT1RLdYQBLfVCQVMTFPwRot+jUn3u0V+aS/lCFm+yL6y1gXcjIvomjAAw9siqFiF0uWOLl3pztV0oZMFCe3ovi8ypaDnAdF7TuaGH/tJ4zZhAHq5V6+kI5Oj1S48J0Y7u1jUX2vmRQjp+Y5pycGMUFvmsTXrGcIpz/xGq7UdGKEYaiia4QiFNfSgzFDxTIqjPHdlVVsS2kuMq5UI/6+xxDNSf9CNyu/X91WzgK2CIleQ5nUh+Ep7OMP3sWv3s2kd/KW2nCq6FVVVJAU5YI9G6MV+VhXTtY8iVisru/AMHNXTMyjsHULRMrmh3F/nA96KtLx1usv4WrDIBPLFrrTuxxIzSZRujJ24uZOnjNC0ThstseOnfvwtWefxZ89uxtdvaS/0N3rQOpLeGw0EpPJilQzE25Mx9XbDOHLxRjzjGF8UArs5bhkbrJlysBsTd3DBiYjjTDxBrNUpuihIDx7VTu0c8DuZRIJl/CjZvrLjK092pvb0D8qyDES7vliiDSYasWmhcwM5aAVEhGBKPkTlYQYp1kyV/WksHgiMSGek4UkrFkTwYzUKWUmXc8BcpSZvEbDKGk248q9qRkTjYyKpg5xGFeeMdAahpi1PMYkGrEMN6G/NHYrWdFGcl7bSH+Z4arbxtSGlp5+RfZsmvSXAWa5GjhAtTS1YlA/QXtOK52FgfEaMYlyLDneCmz6IcasiMgLXJOKyEAmkfD56Cn63cuM5RmJqfX3QePuCDdPL4KYN2Ln/fuwb88BhGGQMYlBSkv2IOviUZwtHcCB5x8hIN6L8WMjplivOMn9p2YYE2noYXtgNuX0CJNDKAnIRC1n0j++/YP/jhd+/A+4X92KwuJyVDNeerzQDt/6hx/jlX//IZIGc3GloA7Dk2xvSoNeAYOsnuK2FpB3VUUhFmv5m0kgnDWjt6kEp098jB6Hjfj6IzvgpSb9RT+AQsbVP8ntxe4nv4fN0QEUHRdZuA609FJOlIlL46RL6Zip3lZTgIuNvvjHH7+Mn/3Tn8Ov4SxOUr83h3H0oU7CH66dwdU8xtUvF7I9TigxrynGRFV2blQiSsOevfvx8J77sSXABq1dOlirw/DMD36I//Wjf8KeyHFcyK7GGCUDe+uLcOrkOQxqt+LrD28jUejmbPDb3vDqh8tiAaG/aJkQtG3vw3ji2SexfWsIWbUl6KV86tQU+4y5AVXHRMiysgbYxe4lnk+Nvpoy5BKEsfav/hWHfvNvuC/WGSeOZmOUCWRhyVvx0MHH8cT+3Qgk7L2kQZLXePlzfcZdunYX7l8GvMX0Fy+tHi3M0M1xtcZAWStcPCIQHbkOnTnMqMu/CqtmZtxOarCHs0QHZru1VlYjJHg7l9+uykGNTDLpIP1jytmDosFxGGXs73rGeXQ7dqJy1h6RXhSUjvfBsZISpHMFourKxayWJBkGij2qL+EGBd9N3TYK/SU+XA1q1isrt4UrXqafjNPMTL5IUsUY0tZTLN+6E2UGN+yKioFLbw9qCgvgODuAnqIyeHtuRURoGioyKnEt3RqNVi2oZQZuoqMNGi6fQo7eg5m9MZjtIPrJPZIZqk6MqZbBoCZpIigOw6S/yH4+pmbup0WqdwD8Yr1wsvAG0pnpOMWYs7UXBetJf3EruoTsrAyMetGFXqPHulRnMOzIRewKDaVMSutu4HV1OuCph6OZXenIxuyCULcJ5GVdhXHQHVWZlYhNehgJoT7IP5LPz53RRlJMgzOznr0cmeBxFD/+1Tvw2/QVai8PooZZ3MHejmgtKsA0s3jtPIMwmHuDUHUjNOOVqLUPQJrLLBMOaAeGEAzDTZiydaC4uJREjXH1b4WR3k50cCA2UbtZ8V4sU7NYPeznsYDQX4ghYxZ3KfuDRodAZlR6IsbHhNNvCv1lEI9+czOG2hrQNOuDyfp0/PIXb0GfsBc7ueKooei80H5cvByQl5kO1yFv1BXwfYtIhBepQTZMLhlgRvA09XWtGE7yDo7H/nVr6f4zEVxQjel2HRLiI+FsGmcSShG6bCLg50X5SYcsXCMIItDUywHaCdHUa60tzoWRus2GwWb0TpB2Rfby5GAjPn79ZRy5No4nv70d/S1crRojmMDGcM3q7OzzNIAl/Y7QX1rrq9E+Ti4z6S8NTaT0hK6H83Q/qovqMKJmyChOjTpmZFc3mbDn75KZb0Gqi4QBJ1mmyRBdeycn6BzbHBwNGO5pRgO9GTZEPLYVN5A/64dtTNhUkrLnutG7zto137msQkfQPGyFOMb7Qv194a42oYCN7/ylq2id9MaDDz2A+8jkdJ9swfVL55TZf9yWA9i3LQ1udkwEqWmEmgNmVESIopk6w+NdPXQI1QbSX9amwWeqhlibC8gr68DGAwexm/SXYJ5noLgQFy9eRtWgKx567GFsTiX9xWFEgf1mElXmnUb6y45N8KGrZ2XoL3RNOWsw3FCAjEuXkU830Jq9T+DB+zYjKliN0mwSXdKvo906mnGZ/VjH9GlHZiJnXLzAWE0vtisUF6bxewDVuddwKf0a6gec8NAzjyA11AnZh36HBmLnhP7iPlqByxcvsqykD9uZmLNjfQL8SX/p4cBxnq7gxjFvPK7QX6IQYDPAAeYCrhfWIYhcxf3b1rIkaIXoL2wkUtva1VCKYZck7N6QBE+uKGzoLnPRmHAtM4OrgXIYAjbiadJf4ljaZDdUjoyMDJRyxrfhoSexMzEAHUXpqCIKSTc8iNrqOjSPOsJPM4H0d89gJjAeqSmRsO/kYJ1+BVUd47jvsa9gE+NmAvk++fE53KjqYFx+Hw7u34JQX3dYDfIcly8ji8g168htOLhvC0LcnRThDtHO1GgogL6oFnVJ3/I/8YPdXdYuM/S7anD88FHCt/ugYyhINzoLb28HVGYXE7isQ7/QVZraSX/xgv1YMwrq2rnq7ENDFTtM0l8SNqzhwKVBTcYlpOdUYFIdh+e+9ggSQz1g6C7AhfPpfCe74bpmL548sB2JEcHwo0CCC99ZlcmGpJjNcBpvw8Xj75CylIAtSZGUk+vHmTPn6WUbQjjdtXtS/dFRfB4fnPgEWSWtcAzbjucPboV6uod9Wx76CGfo66zndXbChni/mAB3JcFlNWv3041+ObN2hf5SkXcBb73/Ea7n1cHeNwVf/dqD0A5X4+zHp9DrmoI1/iq0tnRgwDYSD+1J4kAq/bcjvYPD9H6dQyYzdmfY1h576gA8prpx/p13cTrjKpp0tlh/4Gns30Q+LRdyk1SskozsL05/YXatngkidqRnCO9RaDCjumGF1GHjIMQNM4VkZnIcw8OEdptYAuNCAogjVwJ0ME5R99BEF6RSmMtl8gyhvZmH34c1s3LXbUriCmSM+43SGciEBFdXJigJ6USoMSOMu+pJf3FWzuHAmh7j9CRB2UOMdViRGuKqxE1kQijKRitCf5Hr4qxXR6KLgSILLrxeZ9qFF0bhhRFSI+hvp0iFq5YrI7qvpifk3iisQCqKhplgingF6S/jozoz/YX35uqqgYn8xIzDH5L+shdpqfTlM/FCMk1lPy33c6TohdBfhIgzQjeTtST4uDA7lg96dlrPc4wo9BexifpLoL/MTE3QNc24Nt2tSvo536tZDrA6khMmWPgs8G8XrihkhjdNasMI3eGz1NHVEEvnxAY+RTtNshB1VnH507ttZUR3ZSZ+f26UMemHkRbhwdWGjiGAcbYlIYGQpEOzT5IsJG63WbYvZ3aYItyhtDl2eDq6lQ1siw6SgOQscWQ+j1X6y6d7vDv8RCQW74r+YphSxEkUdxn7BWs+M0f2ETMTQn8h75h/FPoL65CFBjRBN/48/YVhHQ3fKWshJ3EipPRHpLC4aJwZp5R3UseSnAkCu0lwIbHFQkCSW1NEQXgsW3sTGlk29vYx0l++9WfYEO6l6DiPkEhjMAmFSAhB1uTbSpuidq6R7ZkIQg3bjrzf43J8hlMkIVC5Tie18u5bsf+RjnaV/nJzQ1pu+svUxDjfcfYjQn/hO65xMtNfTl1pI/3lz5Hk5wwDB8FpuoGdlfphPiiuQGfYX+pIqppmQ7R3cqZQjjOsOMaNsx3oJbOciwAN25Ujczpk+6OmvwjpxImDrUC+l2KTl/Cepr8wi1BsoqHEofLiLoFRZOVwz9Jf2GF2N1CgwdoX4cHe9GTwJViCTVYOgpZbpb/cvTHvWfoLqVN9HQSH6xyREB1E1qSSsHH3hli05yr9ZZEx5n78MugvHXy3exlWSowW+sunr+luPpmnv0jx9LJsMhXjCH83m5p1llYsc5Ekmz98BOlAb/2GpLHfrIEpA6kMHDJbFqmu5VHTWHSnvCxiED99Zbczye1ugYe69S40ruTnsTxm3iafcz/zVd16NKZj0R7yx0J/WXabLDLPUvzo5B2CEKG/iBzllKUNfIZRPscJJcNZBlKxiawgxItxr9nkc9zmsn5FbCgDqcWG4h6/d2xopr9EuYjikbSppTGV2ET6HYtNlKqCpTn0PXsUsYn0O2ITGX+kT17+jQQtd3+oWfc3rWdi56d65zu/AsuzVegvjY2Nll7ozo90j+whA6k05rvW/7xH7vNOLlMevnR69vbUJGU50epmDgFIO7HQX1ZtcucWkM5R2pWF/nLnR/jT20NsIjFBscnKDBp//DaUSauEUgSWLgPSvbrJfUh7t+LNfKGBVIwgdpDFpzL7VP4tKyDZRD1f/nCFNfe58qn5y/yt7Gve3/L5rTPYW/cz/37xfubjy+e3fldOLDcng4aF/iKJJLeeQ869VNtnXYMYSf5nXqfKNZvvW74vm1zTH7o35UvKf+5sv8+6Hnm5xSZeXl6MH7JeSp7JMmyW+7v50HIu833L7f9Hp5Zru/k45me+cEyzTczHNN+H5X4s+1ksv7DPp3+SF1uIOD4+PkqnZznGp7+5+sntLCC2lomIgI7FPS4T1zuyofKOWI5sfsbzz2++nSy8O/JOWRrP4jay6GPLwT7H37e2KXOftXjHxedY+Jz7zb3ZC5+Zf7J8Xzwc/f39ik1WJ61mu4prd2hoSBFRWfrJhaU/uPmJ3K4tWtrXzd+85V/SQUmj4mbuRxbaprh25dkuIf1lgXogiUVCf7FjgoeTJNsoG1eFrPFU6C9aNezm0sKNhkmMsn7LioXTWmfWZdy60Z05xv0U+gtVRERTQDahvyj7MfgrSSTmj8kdZbKP0F/UGpJO5s5hUeiXv5e/IRuhZyOZMqrmrsF8vZZrnubn4vJSLo1BbJ24r62ZLMF4sMXBYWRd5OioOWlIw3v71CZJFdzPKPsxSefm/cyEGbXjgt31/O70LIPqtPt8OJENQxqw2GN5bWKuC5YkDGmPck6VkjfOHA3eP1kFSqKa3IOJ1JdZSdaQp8nrk/9Z87tzj1y+oCQPSGLQpzdJFDDTX+xvoW/I5wYRahABCO6qJKnMj94LJAd50VbGJp+++j+VT6QtiQ2ljd9Vu5JnzPKkxfXfQluZpoaj1AYvPHqWzJA6JWIeIsQxv81KXTF7C3palO8yCW+WamDm6ZV0sPzD61PaAY87w3PZMnFk0RHmDyU/SNsxMXFQEvcsmyRUzjBJZWE/Hl8UtpQ2JWeSNqWab7cWm8jfq9ngZita+mSlL+TzWPqNAkFKeMZ8ZCsKfXxWKZKR/Y6JLl/paaRvUAZc6YLYSJQ+aO76hCBkYNKkvd3C9cp9SJv6gj49pq331iCnYBiRiXGICHKjUlEDCgrL0d5L+gt90mnr1iDYywk9NSUorm7C0MQ0PEh/WZ9MzV3VGErz89HQ3g/YuyFpLcUaSECxE8UAbkbSX0SnV+gvExSAj0hJoxZvIOxmqNual4t67mfl6I0169MQFuCKkdYK5JVUY5DK/d6kv6QlR8BjBekvJl5vW1UxSmtaMMwRwjssjWUukQqBxqAfQmlBLiZd4xjsDoYTha5LWfdZ1z7ITFNSXFLWsvzDD1bMWi6mTZqYXKRy8kQK693CAzyVLF+xiRBmGijMXtZA0oS1UGPWs/7SF9aTLFLPyUFT9whsnH2RRvH/IG81BhtLSX9h7dS0SqG/rEukPCHrVS0zeTnm8m3s7Fg8X8ra2X5OlkxsmF5BCcp9GihSXkyii7QHF984JFPP1DjQwDbSoSDfVJxA2dAVFpe2niVMdhhoq0NjWx+0oaQ4+HtSsH7hqmepWtRaXYZK0l9mTBTFoE5xfKgXa/n0aKyuJPmDlCAbB0SRlBOotUNbRREaB3TKwOzgHo7UxHBqZ97h6mnh9Ks/3cYCt5v93+Zr8x8J/WWwqwV1zaS/eEQiJVKgBJQD7W5CaUk5hiat2E5CsYbAAyfWezazBrC5RwejyhkhiYmI8tNiYpi12yWlbGsmqL2oRpMUBdWotLNGxsREE2yW75Qbxcl5DJY0VFc3Y4ydo4tvBJLZf7nMT8iZQzDFWsTaShJdekgI0fAcyYgNcMZIF+U/K1tIf5km/SUSSYnx0FgLwKOc7W+YmfQERJD6lLomHu6czN5uyjd/06s/mAetJbcD6S89rSgtq4WeqmUqVjGERAkRimKsyuxq7oTsY/o7G1DKZ+zNmuMQZwPqK0vRqZvlIMpJlMGOcpMkEcV4oK+lHhXV9Zi2USMwPIkJaf4UAVm48LuuIzUxk7S9thDHDv0UvzvTww5vLbUMSX95/zc4ntcNZw81ykh/GbT1VoqiTxw+jpYJloTYtOHaNUK2/QOBzsv41fs5sKYs00TNVeT22CE0nBJyztLRz2Ko5QZePXQU/RRiUOmqcLaoF77+fgr95bfHsmCiS3KwKgslfSqwLpolIm+jsJt+d5thpJ8i/cWfIvFBHlTBMSlpyuKPl5jg8mycVLQV4U0htxjJSEUTrl7rgW9oKNx4PVkX3sf/evF1TPsR70YllvHmK3j58Mck2BB50pmPM8WTCONAqq85hRePFjFrmTWp5ekoGNIgkjW2rqJBzAffV5+F3xw6gXGu4Gd7yxRqTCi1YnWkv/zuo3zYinhDyVWUjwj9ZRrnXn8LFSOUCjT24tKpcriFhyOYxcQrQn9h6cFITxVeoYh8RVMTeomgMqpDEejCGtIPXsbRyyXo76W6zJUi2PmFw36Uhe3ncompqmfb+QgnLhYidvNmeKr68fHhn+O1148BYZupleu30Iip2NRbm4NXXnoXxe0spObE63LlEKJjgjDZnEVSDjV1ifQzjQ9C7UdpQRXrTw+9gosVDejqbsGI0Z2CISHQstOTGexqHekXezssCSR3Rjoh/WWoHedIf/kVheAV+ktiEEyjrTj/9kv4MLMEQ/3U6r6aCSvvSNj0FOM3v/qQSEY9qm/kIqPVikAMJ5SfFXJHDvr72pFD+otey+c93YHzp8+jtrEWJbnncCKjGD6xCZisy8TZa1XEY2VQ8CUb7iy3C/GkrJ8y8k2joyobr77wJkpIFGouyEd2/TjbiTvaC8/j46vcrzYDWZk58EjaQcKMDjc+fh3vZpBk1duNfr0tYmIj+c6aB1KJo4l7985s8sWewx/73stZR6rQXyj+85tfH0X3SBdxe0NQ+5D+4jNHf5kzztRgCy6++yv8n2N5bFdE9LmMI//KGVypaCTiMQ8njp1HL0IQ6zGAj958FVcqW9FZl4v8yh4EJqaQIGRPr4i5jvSuV6SGyX5kn72IzJwWdNiEcrUhE3y6KimjJfSX+KRUaHvLoaMbpKU8H62kvySFRFEwOpJsPyv4uNhgrLYNs25eCItLIgatG6cHZpgtR18fN3H7dVbmYsQzCU899CASXLvR+9PjaJ2jvwQlsZCekmEOHen40ZEOlGu6cWPUC/sfJ/0lXg33jl+gt5MFt5NR8FmusXPugSjXOzOFLsqRtVFvd0toJIJcw6jpasNJgRXvIxtnL1zFQGc3Xdq8N3Fzcqi978BXESMM1+pxZGWMswaOkO+RDpg8qaoSn8g4XTvO0kWun5O0EvpLR2Ue9AEb8NQjDyDQugkv/uI0mhrrMH69jCSdB0h/WQdDw0X8+4eNqCD9pXAiEF95ivSXQM7o215kXLQLIwkrQ39hi+BzpHyhcxgefHA7GX7hcHfTYrKnFPklnSRm/Bdsi/XE1Tf/N2eFHVj70C78ww93UwKObNWPfo/XCt3ZGTGD29ADTcg2xGr6Ycc2pkibztneyDBCc1UO+vzW4S+fI/1FX0n6y1soKgkHyj6BbeQWfHXHBoT6uFKFhvWm7LChcsH9D+9FSnwsJ2Cu0NI9vrp9mRYg/YUuWVu+M5tiOjDI+I1IWg52cbVAvOAjf/6P2OA/iyvUu73Kdq4J05EQE4yn9u/FaEsh3i8aRldLG2qzSWt54m/xaLIz8s+/hxO5bdjzF7vwg/+xhXPQYeRmfICXPhkkRjCAcp3PImm3NTqoAX3srWMYIeRAhG2UgAgBGK2c+DWq1uH/+aeDUHUU4PBvT1EgZSc2b/sK/nGnNdpLuR+Rb7oJuocZW7exd8Q69lN7koPh40FMIvWkV1ejX1abMtNfvAPX4tmv7adqHukvLovoL3JZs+OoLrlB72kXUhMSYMOHr/WNxsFv/j32M3+lveQsjticgG+iB3pay7lKdcJ3//lf4dGTi5MfnUZedTeS/YjSmHvIdzmQmukv25/6FleiUfjR271m+gtjAEJ/KeaqQjfWiMbCRmwL2TdPf8mur0MO3Ss+afcjfr0jxe1DMVR5BpmTI1C1FMOBwsFuVN1RNg42RhbFxkST5uDvTak/eyRSCNqWWpyDJJ1EhwTAl8tQO1UUAu16SH8Zg2c0f/bzg9bFni5P0l/UlJVmR74Sm0juTY4OQd/ViyuscTTSNxC0YQ+Sqb/rE78Df/P3MYh/7ZeYtmPRNuM63tGbcSBsGo0Fl3HhwnVMu1J5w40x5fFg9BZTnWW6D9O1RXDbux7aOd1OM/0FiIsLZUKMF1x5zkS6b1lkQDkrd8SEB8DH041p5dEcZPvYOCbgFRcDfybPuLkZ6dr0I/1FtXL0F7YyiYfazTahrNQGI0P9xMmlIIA+ERvGHfram9DhrKObbApD1By2tqHMGyk3o/oWuugGsHH3AwgN8CaI3BM7t2ugu3GdtuPTXPRIzfSXGbrrwuHv5w9XFtcneNiQotOOjsIO2CV3orYkF802lFLcsQ2BPLed3SQqi4sIbu6hB4QqJ1SxuRWLtBJtZvUcFgtQbMUzDLvvN8J2oh5ZfHfMQgkUj3cIQ0SIHwIoFxgSEYuPzs/Aaa0XfKfP4vAbbVBRrCR2/9/Dj/zjSttZ9HW1ocPble5dI+HOegIuKNDh6ojx7nbi9GoQv+V56qp60D08jCqR1Lx0Gdn9dthOTw61P8wbz2/DmJqRUnGtBMM7DQ/weiS/w8iCfKC1jF4P7pc7YIddlLy0szdwckdd8Io8aIlqCwiNohRnohIuWB1MLc94Jf+m0IoN4+q6CnomtCRlhZL+wrAh2cZmj4OJcpPVKKpshsfuZ8mynUY9mdcqWzvqpZNKRn3z3K4OtJlC8dyGSEyVNSolUR1NtZjVk85FYYbpMcqLLuqHuD66m43xBtJffAN84eshaDNzbabQX1p6hgnt9kJUdATiQ1zQ092LPmY1jYypsYUD7w/+5iDGq67hRlkVmhoG2HEGITIyjqzOMEwMUqCcKkbzG5e5jg7EsNGvzTFDcdFyOceXg4Zih6jkrNAyjPUqm4jGK4ks8l2x2O2KOecPvrQ/WPFcs0yA0o27YNdXv4vvf3cf+ovTUdzQBht3H86KiOqy4XxXGdjFfkySsBNZKjcERkZD1c+VPXE+rY398PAMp3B9LKJjQzDW00elHwZ95zYJfjssor9Yi2GYoGHFSYygxZQQgPxHniz/EiKOhTBjveL0FyY4uQfjkaefRGKYF1qyz+Knv/kILeNO2LCJHNLL7+LIWy/hdFYlG+fcM+NMsbksGyUmxpmSQin+zWA+kz3seW8q1aKWO28PLjD5P093EmOYEMJ0IQWpZc1JxMAEJbwITtdobdGRfx5vHM/DwIwDV76PYUN8IHF11/D6i4dQSMF7Icysbl+WBfg+cOByYCe2QH+h14YEFxXj/U5ziT7WNoxj8zsqByp+edujq60J7SOcmDK+au/li5j7EtCadxxvv/sOjhDlqGMtupCPYJpEcx29II0O2L0xDq4MHU3oulGck01YfAPs+fSHB0Xsfq592aoREh6NZG0H3v/92zjy/rsobR+Qhoap0V5CxrlfeSP7vRkMDYzA2tEDcZsO4kBqGKY7i/He6z/H+fIOqol9Wfb8z35eOwTHrMHjTx+Et80oMo69j5ffy2Scfe6BUHO3+Ho6chgqTI5Rc0Ldh+4uuuRHJhXDDbVUU4e3mSzlAwj3cOcELpKwDQPOvvULvPvhO8iipKsMPkpfO2fqu1yRmvc2j18W+gtFD0TjUOgv+x7A4wfWoSd4Fv96ehhNjjoK0CchLDoa8UGBiNR8jPHBLtRT63Td1j14+KkHoB4Iwr/85Bpa23oItDbHNa05wxRCSmrqJAK0YySd6CnOPkd/6elFCukvjqP96Gf2XShlwtoaW9G3JoUXp1ZS8KcDpm/K/pu752X7y4oCAQ6BQQiX+3TzQJjTx5QyNEOj7Th9UVy6HPUlsWK4rQqN426IiNmARynY3/4vL6CzsQGzBc3Ytucpuq23AO2++Nef5aG9q59IOhfzdasMqGWi0ZY1U3C1IqSa9JdwuiVsTK1o6e7HmpkoYhF70T9Hf2mub8HQ+BqY3E2c1PTAoCX9RfqWZbPCwoFlxmZt64LUzbvYB00i0s8O2S9cRc/EPtz//D8hdU8PU+BbcfadlzFC+oswISeGOhl7ykdw2uNsvKS8zB1OOkRrTqyko1UprZb0l4kpBQit0F/qScTZOEP5xGF066wpCeYCPx9frN2xAw+sC0atVQd+kdEJnZ6r+LVbEU2ZwU4mtHSUvkyqxwAmObBS7G11+xItYKa/GJVJsA0nmlZ0747216NvfAphGgplDA1yMmqHukImi/k8hR/9/RYMll7Aq0dPoHHj/0DaI3+BmG2ctPe3Iv2TU8gltdSWEjYSCyu7UQhjOD1hge6UEqT8m4YUl+//EAceK8b7v3sBF3OqqescyEGa2d+U//OKTsPf/lsEBoeH0VKSiY+O5dMrRolBNz8887f/N/Y9Woj3Dv0SFzgJvI/g+oCY9UxIUqEv3J2r318xVNGL/ckhjOVbWvCXaNj/ZKc2MhNb684Q254IWE91sr99C0dulKJ3fDcTXG2hJzlsoKcWY0xSLMy6htpS6jUTflC5PgZ+LjOoraxAY4c9Dj4fz/bGxVzIGnz1//oxHqbMazMxamcuZplRkOYBULHuFxpI5QiSnWemv3CQphvFx1WPJmboXqMo+WBZC9y8ohATuR5dOcyoy02HsZH0lwkX7PChykSwGwpaa1FwXQ2HkTJM2FGfkplznRWldOm4cVBKwPi1IpJOzqLToRNVRmZeehNebeWHo0VFuMwVinXXDRhdQxAYHQqvmovIy76M2U4bXK8cRWIkdVQZH128BF+uNiXaoJ4hcdDmNnKmm44R515UzHjgUQ9PONLgMhcSPVwlm5Fxvr66dBy+QBrLhjTOmtrRaO2CWCrWWzP7uISu4RtZvO6ecgonu8GJgdX2shLSXzzgTJvoKMp95bIJ3sZm1Nm4YK1PAPFjPjhRmIdLRLJNteTCxjsWARE+8Cy+iOvXL2PYcwpZtRPYsIb0F7qnVoL+YmKcu7+5ArXdVM/iPTcVVsHDJwI+vIDB1mZyaofQ3lKOqlF3PEx3tZs9631LbyCz2wlfeSwaXhqJWJHGwMlScXEBiiqaYPIoRligH1ICHdB04wamCe21I4x3OIf7XTRAPV6FGodwPBeXCI++CuRn58KBSSvtFS2ITE6Eg6GbyW41dPHMor8xDx2OGmx1l3Is2mR1BbFcr8d/cFyGJvQjJHMUobi8CvUO/iiKc0UAtab9nYbofr2IPo8ZlBS2Ijn1frjo2qAaHOHEsxnjw4TXO7GEy8jwALmi3f1D6GuoRk+nFfY8ydWngxHtzPDNKyHM4C9S4MUOYXKsD+X5BRhlBuYsyUDdegd4ertT6FmHxppCtKmisSbYER3NXUw+60FlHXnAhBsEsE+ryb+OMXrjZokB7JlwgDcJL5O6LtTeaIOJ3pP+esn69MEaJjnaWWr1/oO7X/310lpglvSXFmZct7LbsdZ3c+ExyETOjQr9paqwFv3WofRK/Q2Cd7LsZayDVK5ZOCIWoYx5jnczK5usWSO50IlcvMg0SD/KGHxzK4al8qKqBTr2LxuifZRSTMvY8gUHUivYq72RlGyAp5sjyxU8sY9kkzc/PIPXfpPFTKk4PPbcBmyPd4O78TiOnT+BnAkjUnd/BVtSU6GOcEDXW0eZEfU7zBKbtvPgs0gIUSP/179Hf9z9uH/PRjy2vhHHLpxA3qQ1tj/+Z1iXFA3tlB1qq97B5Q8OY1obiae+uZUEkECq9PfhreMX8Ob1ScRvp/suOR5qumNop+XfGDzxT9iBJ9f14+TFD3DVYI20vV9FWmSwUmNkVNlz1spUeR83uinJ3kzah41lvIfjv8eUjRPSHngOW9amQhWoQsdbx3H09SxYaX2x54mvIsLbBlkvvYOxzU/g/i2bcTC1AScvfIhxTix2P/09pMZHwT7wSVRXHMYn7x2BlUcMvvKdrUiJYyxotAtvkZNYxJhRys6nkEZclCNbB0vtln2TicN4fzM+4TNp7tZDzbKGJ7/6CGJ8nFB49AhOXi/HODul7Qe/ge0JEbCn3ukEy2FSduxjFqYvXWe8RNZ4jXRX4ePzlznZYL0wkzxOM2aq2eaJSycuIeDxr2HX2p14qPZ1Zme+i1l7TtKe+C7WspxB77QPFa+fwIfv5cA9Zgu+sTMFWmM9Dp85ylKaHkXAfNfj38bW2CAFBD697BZZPcHtLTBLJm0z0s+mo7SFbcb6Bs584oRvff0B7GZC4e+OnkPptB3C0x7Acw9shn2XE5peO47Dh67BRuuJTfueQYSbLZqz0/Hmx/mYsHLBJnp1HljHFaHVJKZYV+6WtJuJTCypYduf4KRuqL0Yx69UYWTKll6uHfjmrgSAWePXzp3CYOI3EMr8s4vvH0Fxxxjcw9bjK998GCGuk8i+WojjzNrVTdvDP24nnt+VCLvpVpScP4kbnT2w0nhj3d6nsHdNEFczi5Yst7/x1U+XwwLUJuhoKMSRTwoIT1EhKG4LvvYcvWIDRcg8dxqqzX+DbWsSEcrwn3HMlyE2R3jPRChlmiN1M1C7+uOB7evgQhiIbPoBeslOvY1Mehlcg6Lx2NPfQJy3801XviT0lwkGA0TFRAqjpVh5jPQS/aRkspEHKuIL/NxA/cqRkVGF/qLWauFMDqQV12l6Ejp0pClIUoCLlkgjUsuvvvMhrOO3IG1jEpxmSI0ZEUEGoYFoSRCRlHKuUrjfyJheIdhruZ+9nJtJKzrdsEJ/cda4QO1oLsqWsoYVob/QYTrDlHy5TyFNyH2KIIXEcE2MY85QSkpIN0oRstwDKSSjJBQYGQO0fFdKXMQmo3r6622ZkEX7gTWime8cheN6ob9E8cUdwwhn4rKfaPA6sKDcTMQhAYW0E5W9YMrMdp9lpu8IqTBTsyIQITah3Xk9kpIvAu2i4iPKRsuyibeC5x/jc5rkyG3HCYRaRCHYmU2S3jM6xoJ6ths11aYUmgK/P8Pvz7Do2UHEE5QgBGPPfK5jtIeEx2UTzeHOigy8cWEcT3/zIaylO21GiC4sMTBZS0xUo9iEO/LcpDZMGkhysJA6KGYhwiCEytswHKAmGULsJ9sq/UUxwxf6jygb3Q39RfqNcbYTw9wUX4QWnJ0pH0c+sY4CJ9OEdAvFQ95pmVzp+QzHRZDBhs+Qbd2Rg5bQlHRs/yZOWtUksyhUJHknKaogg6kT2761hFYY9hCi0DiB9wajLfMUSPlwmqO/HC/Gvm99mytSF0zxPRY6kb2zmsdzYnjCsp8QadhGuZ+aIjIKHYTXM8E2JdfjPH9usylX6S+fblLLTX+ZZj88KkQeZuA6yvMjV7T4yjmcvtaBx/78O6S/aCSFhBtDkkweIteFxCgzvWyaGgDW9g5MZJX1qDQ3jmnsQycYSrRjm1TzeELvku2Pmv7S0zXMOkqXJStL+FOgv4hNhP4iGLSl2KTe716mv/Q0VpnpL0HeRK0thUUWJher9Je7t6fojorMYlBQ0L2l4kNvSH9nC+kvTkiICmCnau5E794SC3uu0l8WbGH56cugv3S2tyv0l4Qov3uI/mKx2B3/bYIT2YLiFlwqMo2sSGXgkAb9ubQV7/ial3sHE5xJxJE63TFJb12CTewhf2S2LBONe2mTOLMj46LBlPWanRpbMlKHrNItNpG/V7c7t4AU2ovt5N29p+Tw2KbsND6I0Kpg4GpmqegvYkFZpUvbEpvclWzinT+GP/o9pC+22EQkJZd7E4+gmolizqwwmaL3amqJTijPVtq71X8G+ot0vPLgFPczNUBXN7orOHhKI1ilvyy0BplwiU1W6S8LNrnTn6RTWaW/3Gw1sckq/eVmm8gg+idFf+ENfeFKCFnlKdmoc7ZShKHnPrv5c8oCs95SMkbnT8rvKXELDnaLv7tgdlZOMlbGYC5rSef3YqDs9vtZccZx6zmkgxTSiZubG8XhqUaxbBuv0+w6XziD3JflX/wlLcX7nP/gtvd286p58ff/8H53YhNpyOKCE/qLSCcu77Zgl8XPePF93u7zxZ99nuuzHO+m/RSbc+/Fz4H/vN135cWW+J7EjWUwXd3u3AIyiAr9xY/CKDJxvZtNXqH5V2TuAPK8bnqufJOUd+2W5yofKvsvvGSWI5i/L8e+6Xe3O9vcLvzL3E4+/Q7erv2Y95p7xxcOoUzi+/r6FNLJ6orUbJjF9BcRfl/OTdrJTY980clu+3zn2tDn6TPm6S9L8WAnJ0kcEbIBhRJkm5lkEs04i6SpoetMQQXzZqaiKGQWJoNYhOlnSTrRUdDc2oHJIItEo+d2ogUMlM4zJ+9oNM5zyhQSACY1RkdqDOkvGgn4KzuQ/jKmU5KN1HKOuYpZcR2IwZab/mJgUswUJQ5lwBfagEwQbKiWIUIByus6wwxCJtLYkU4ilyb1pDqKLQj9RUtlFbODQ6gWrI+k2LK88ComxNjPUUvmbUIXr47JDUJ/0ZKIY3GMCDVGR2qMFKwvUGPmaDSkv4hNLKEfOfZK2IR3qRAVFN0I9o5C3jAXyZPKYfmQ1rFm+zE/LiaHTDGxyEooC7d4D+iekeSB29NfmMxFu926n5TgTDFhxMaO3FWLoXhNM0xUMbLudzEpRlbpK2OT+Sf5J/fDPA2Dz/Ou+hZ5xormyvzDUmw0K+LjLCCWTlHZGPqZnuZn8n4t+qqQYvi4mYnJZLW5r1q+P8kESBFCkfdvfmPi0SxDBKr5A8//hj/MEWZEPGXxSfgbw/TkHAHm5vNLItMszyzKXZZNbCJ90HL3P5bz3Qt/W2wibUR+Xr6NCY9UyLOWZ2BpO/MnM/e1Ile1mO6j9BlsW7ZMOFr82GeZtCbUHzsRxJk7hly79BmLWtT80e/gBwrLd1cju2AIUUkJiGRd6GhPHfILytHRxxgJfdLr169FiDeF1ElFKa5uprrEFNyDSX9JjYXGOIiCvDwKCYzAzskHCWmpPIb3/CBrNJDeQfHg0rp26OfoL8ksJ7GfGVJIJ/UdA7By8MKaDetISHHDSEsZcktqKDdngldYCtanRMKDNYs3z0Dv4Pbu4KsmaoUOsGaysLoN49N8cKxrY/IXotffh4QIf6ioH1ucn4NJt3iqaYTA0UAqCmkwtW2DVJhQIzxpHck2/rCZ6sU1imH3MUhjQ1lEj+AkrGWNpVaILdxmqQNaV5yLssYF+ktiGI9PqbR8Kq40deuobetD+ss6hPioMdBQgtyyOqbrq+AbvoY2oZA3xbRXZmNDJf2luIDi9MxONrHBeYWQnBDqTSnFKhTX9BA3R9ICy6ZS1ibBh8nDbVWlFLjvwrRQXBJSkcAyGAfraXS31KKxtRdaSvpFBXgtiNYrNhlDM6kNlc20CfcLTVyD+GBPFuK3o6yiHiMTk7B18abWbyLcHQxsU6W00yAnNY48B3WhSc+RwuvV7cuzgKgTDXQ2UVy+GypP1nGyTs+ezL+p0UE0VNWil+90XBprrin7OT5AoktxGcXIJ+DoTsoLRVh8NCoSgqpJeqlmWwffmygqYyXBjbCH0YFWlJdVolc3A2e3QKStT2JZ3BQ6WW/a2qeHD1Vwwr20izpNTshH+yhcXoaGniHAwQMJKamIYY03teFIGqpEBQVAmEaMwNg1SAii8APHgkkKoZQWVELvEIgNaWGs/17OAeLLe1b3yplNhnG0N5SjrttAOtlaBFL0xTKYTo31s11VoLp5gGWbHiSLCd1HTchGC0pLK9DHckGtTzDWUOrRkyId3VQ7Ki2vwTj7Fx+2reRElmHOlceIPb4Q/aW1Oh8fHvp3/O6TXsSvSyP9xZb0l1dxvIAi4yQpVGSS/mLjpdBfjh8+gTbWXrnadSLregdcfX0xUv4Ra31KiFvzJI0hBznNswgKC4WnRhQDOEg35+FV1osNsMOzHa3G2ULSVEh/maGYwW9P5LBmS4Oh6mwU9apAeU1kvPU2ivpYlG87gsyPiuDoH4JQFkaruPqSNOVlpb9wQBhoJ2GiipSTvkFUXz6CCwW1CEjeDH/HSeQJ/eWXr2PGLxVp0X4YrvwEP3vrLAk2brAVss0NEgVCWM+oK8MLb5xG/wTLRYiRsnL2Q4gfRR3kpTSJkMN1/Oa1j6C3J6C8rwxnS4cRGugNXdk5/O50IRxcqepUdhVlw6yNUk/hLCXOqkZV1Bbtw+VTZXALI/3F33Xl6C+sAX3lxbdR3dKC/mEK8mvCEUqsXtO1t3H4XCnaKCHZTZm3MBJY0FuKV196FyWcIHVW5uNSWT9i4iOgnm7DR++8gNd/fwxW4Zs5uC5CGBmniejLxiu/oowbFYpkv/TKAT53d1R9fBjvZLSw1KEDuZeOQe8RC18nPS6cPIPq1jZUULv3Yj3MqDtOuCQEsEp/+WLdpCXR6M5IJyaqFrXh/Puv4+UjJ9GpjsO2hEDW9k6iKf8yfvezl3E6pxHJu+6Dv5qC4pVX8fbxi2iqr6DYyFX0O0Qj1scWDTnHcOJSNhrKc3E1Iwfq6C1898Zw/aM3KH5SCD0VbybYQQZEBMHQX4b3fvvveP+jLLgm7UQM34mFuRTxWm0V+PDdUyhqqMP1a9dRNuiIDYkBGKKIx2uvvIuy5lGuuOkBUfsjwo+C6ES71d74CD/6yXukELE2+r4YaOa8cRJGWaW/3Nyulpf+IueiIBDFXo788sc4daOB4vNbWY/vbB5ITROozbuE377yASdK/ajJu8G+w4jYUAfknXsPb3xShFHCLa7l5GPMKQwBNp04c/jXOF/SzD6+HDn5RXAmIjPSR7M09JfcC+m4nt+BblUE3ZOMI7C+a7RnjJSNeMQmUnChuwyjdK0p9BdH0l+CpBMNZ/YUBz61EZWlDdSo3IuHHtkNx44M/PMrjahvpgi1vzPHUaG/3MCodzLpLw+R/kIczk+PoY3CwW2cjYYkb5+nv/zvd9pRTnRb/rg3DjzxMOkvGnh0/By9FB4eXCH6i2jdBvJhPUfJPx3VMd7rroVL1F5sig3GeN05nCeqaYTKK/asdzOwLqm3uRLa+N145PF9CLFtQtf/fAPtTU10ITXBJC8n+XiJXKEGRQQviNbT5dlBm0wEbcQzpL8Ekf7S/fNTaG6qJ/2lAtEb9s3RXzzxk/ebUOHYjOKpIDz7jIX+QhlCEmhGEollm6uDurl5L/W/zLKItqS/7Cb9JSUlAq6u7uTJkiNJ/13sffuwlSvRCF8PTn6mUXIuB70KxWUvvKar8cL/+T2q67fCK8KOE4D7iDnqJ/1FBMIXNuPUOFqqctHvvxF/9TzpLxMV+OUv3kR5NbWbm3o5kXkIu9Z7oGyqB4NDeqjU0Xjqz75P39wIqq4dxctX9NCT4LG6fZkWYAY524O9eyS2xnWin++IuPCNdOlITWj0jk3oukZ1Ibp3JZwREHsf/p6UIKvRFpx5+5c4X9yIZ+6LRuLObyByjz1GGorw9s9eRU1TG8I5wGVeaSMV5uvYHBdM4Rh3gjGAAZ0t29VOxLZcV2qwb47JquDmH49v/SAKTkSkXf/kCF7NrEJ7exS6qGzU5r4O33tiEwLZbl3dPVgqA/TzHbx6JR+uydFwpi601Lqvbl+eBWbHu1BWkIeKXnckJfgoIQGupxhb4p/JQdQRRjDosRP/9v8+iO6cT/DR6VxczyVIo6YTO5/9Pp5KtsaFEydxlei+4BlO/Lsd8dW/+yFiTHU4dvQ4MnMbsCvOT4Loyk3epWtX6C8euI8i9PFCfzks9BdGBjiYuAU7ofgsBaNJf2mw0F8MLFbuakJOfR3yuNL0XrsbEUnhisi6bmgAvRRmt23vJTapGaPsGEV0x4rxM+lsYwjB9qMIvobCBgmL6C9RIf4knbgwDkpkmR3xZDOkv8REI8DPlwX59ohMnaO/CBtpaUovFYN99n8Yu2M804nSc/Vl11FniMPTVCEK8XLBpEroL7GIP/Simf7Cp2nL2M4MBbD7+3rpumzFyGAjegibdjZ2YaK1EBcJss520CDl/qfx1YNbSLewV2KvJs6qhf7i7e1JlR4jiThz9BfqcXmPaAAAQABJREFUisZwpebt4coCY9pERSIO48jecbHw8/bmAGamv1Q7rTT9RUXFItJfSlQU+O6hC4Ug8gAWt9Nt3U53bP7UIDWZw5DM5zzDjjMsKoy0Bj94cBYf704SDNuVg28CdpH+MpJ7FdSVkMnm/KbQX/jdsOhQggF8SX+ZQpybA6apnBtApNWp08cor+iI1kFv/LcnI+ntoEh1Zw0Kim7g0oUM2Ic8D416RRrI/DWv/nCrBYT+EqrQX2z0taS/MLGIbVvlrEEEQyNGDkzV1LuV5A9JJnTQesCXhximBq9OP4rQaPYPSqG8lirbsxhlu2cACdEMKY1Ryq+QGt3u1LbO7quDk3cEdm9fD+/Qtdi9DQRolDI+uqhBKZfGc1Cr2Z85eAYdSyV0U/D2i2Mfo0cn3YEGesiqSrNRU+GM6HW7sIaItyrKczbZrMPBfX6oyJ9geb+5g731Tlf/vRIWoBxpZTmKagaw6+lH4D7TqkA95sY8SdRgrggFPChWX1vTggkucGZNoxgYZH6JlRaxkVTJo3c1NCgAJeWFdPMm0vsZgQhqfwdYz9ArGoqiRoaG6MGyBITuciCV5BAnHpB0hhEmsFj1KXHI2ZlhSsENwzPAhzSXCOKHWtFL112v8wAVR7TY//RTWO/Th0OHPkEJi2Lj129A7qkinD1JgkJrGUi1YqxBpfSTSjOkSLk9/dASt5e8FCuhM/AFEx+MQgORu5CV1VybtWPnbE5kEZIIf/epF2S5HyLLbEiVuHq5DjHrHkFshLfiSnDy8IGPrQn2VGuZ4j1YU/3EL3YDtNmc2ZyfgKuhHk29/Qjn4OritwPf+dsHEUZ0VH/1JVILrqOSkw7fNSFzt0n6C928YhPOSWgTQgOYoGHFRC+xiRID4H+EDys72DLJxkJ/sVpkq+W2hPn4c/SXZ55Gb28nSq9exMmcNvzD3z6N4LUP4RGXDnQ1l9Ntchz1+/4csaReedEbYcOMKNMMJxvSTJkQJIlsdhTDtaa28q3KhvLohf7iwWWGQn+hIJQ0ann+9mrqFIuwf+sArAlKn2SygIGD7nBnoxLHryPQw0Ro7xCVtUI8lztzeWUsfm+ehf0JH5i9PZWH5MFZxjX2fDI5FYUZac6Lt5nRDmSlX0TJiC++sTGGXht56gzhtJXh0sVz0IVswDoOsP2Fw3znDLBzpAIYOpBx+vdQeQXi8Y0RykSefepnbiZ6TkqvX0FWpZ6QhXVwtx+Cro8qbJ58T10ciInMwvWaKZh2O+HS+SsI3P8dGCeG0MGFQVfvMDy0pFHN996feZrVXyyxBSZ7m3CFaMrc7gB8fyOFZ6o6YejqwWiwVomZw8EdsTGRiEg/iXcOtQNDRSQFeVHz3I4TKLJknSWHhD0LJUmt5Q8TWm2dHeEkbYVt05r9tDXzTCTJyLLd5UBq2V3+lo5cTjtHf2kh22//XjxBwHRP0Az+5RQHV8dRCtAnIiQyArHB/ghXn2ISyjicI+Px4EE36GZ4YRE2aONo7+7kyG7RXOqi0F+YfLBG6C+aUYX+4k8vnJ1VB1pJMhmbJP2FzMA+ukoV+ktDyxz9xRnd3Uw8WWH6i2QY63rqUWQKwOMxYfASUdu5TRKezEBvfsC314m6s3sefBoTIl+HMPS1uiPYk5Bprhjd3P0REuwFN1MkNLPl0FM0YZo2VuYGjJvWkP6yeS3pL9ChtWcSEUxqsjW1cRLThzXTUfRa9szTX1rqmjE4ngqTGwPmXd2kv5CIw8uy9FWW61uOv6VdWNtqkbJpB6+d1+lni6xfXEH70DDiGMTfEZ5EEkM0jJ1luMYEI09HulsnunH/hmk4TQ6jcxgIp5SkKPgp9BdmzC2mv0wq9BfJyp2gsHgXHtg4DbupEfSMGOBCt9/F9Cw8/M3/gvtTPZD1/os4k34DSf57EZi8G39HMHxz6Vn8t5+8jYK6HYhlLH2lUrCWw9Z/Cse00F/kb5u5CgC2IGVyKHxfydqVd8Aw3ocblz7B+YJB7H3qe9gYxSQ90nxGextw9vSHKOi3w/N/8RAifBygsyem0H8NV7sMhaAFho46dA+MU4ZS5uM8prQpTt75f74UJAoxfDJrxTZnNYH6vAs4caYQnjuewoFNkUBnESd6fkzk24W9u4NR6WyFyndq0d6jRRMncNMMu7T2tiG3YgqxZSmIYFKdWrKQVrcVtYCufwQDtR3sM2aRlVOL2vJicp6ZfBbJpEIP5t9wcAxZy/DAj1MonzqI8swzuJLTRaILCVmj7WjpG0eKuzVlWEcwZs9SOIYHRvq6MMCKDNdZykZShtbVK1FpP5aJ/RceSGWAkKiVRARU1Ib1c5tEY1UhMhxJuC9vhrt3DGKjNqA7qx2l2ZeYKET6y6Qrdru7YbgqA1lNtggIDMUk1fUdgwjDdbNHB0kn447ucOTgO3GtAFcufow2hy5UU6M/xicIPip/fMiA7wW6Ua278zlIhCMohrDrmgvIvX4R0202yK4aQ3KUdsXoL9JSTBzQ+5lwZBPEa/QiyeWW5jNPf2H8V0SzL2R3ISbUH84TJAtoAulitEPzlVO4NkKiy1oOiJ0kvnjGwMvFCZ0lRZjResE5KAFjOeXIuEAXuakF9aS/rPMNQECCL5O8cnGeA8lU6w3Y+sSTc+oDr5ILuMaV4KDHNLLqpv5/9t4DvK0rP/N+AbA3sPfee5NEdUuyXGQV9zo9yW7ybMpuNtlJvuxm90k2O1/yTTIZT/GOxxl3jy25SrLVO0mJYu+9994JgCAB8Hv/F4RESvLYlih6NOF9bBEEbz3n3FP+5f1h4zrqlUoslyzx7/Km0F/aq1HfR8Ysn7m9og6+gTGMgjOi5koeZmldME52o77XDWm7IxHhYsYlugXOn5qDh4FMQOdw3BdCKoduiCtI0uxr21jXZQyWErqG8yL9JRqO/hEk4liPc9PJcZF4PMQHnc4WTAx3obmJL8KsK1OxBKHUjLzqSYa2qzHa2crASx+2Wav+790vkbtc4Pfs6YX+MoG60hKUVtWR3hOI0lgvrE8Nh2m8DUXFpahva0JJURl9+ynsQ87hZz95G4aUB7F9dhC11XaIDifs4vib+PFbJ7Fx/x9A39eMKoQw6DEIcS65uJB7gbD7UQaXuCGH7hDDWDdKiosJd26DJ0kw4YR/p/qr0MkAyh67BEQ7tOGXL72Eq/oU/MddU6hnnxRM0ksw4xau0m92zhJK10QL3CNSkb4+DXEJ99NCxMGX2QkDpI8kRjOPdlGr9Z6tlnv0xj3CYvHEX/wxHqD1b7q3Hhq6e+yTSQJaGKMJvhFT7il0ianQQT/p9Hgv6jqn4UC/e1ayL/Ib85B38jjUDSqUVrbAJ+URxAdPoWGhBMePHUeYqQd17RPIfoqBofTlMxdBKaXbjtq1ljEH0dlp9E6rkZgYhQh2cN4MQ68ovooLlwrQbwrC3n0PYXtOOnyN3bh66TyuVLYjZdtePLx1Hf1+DqgtvIrc/KvoWYjCY48/gowwJ1x59VdoMHshjuHuQfOtKLh0AaUNg9jyyGPYtSmdM0xGvdZU4uLFPLRM+2D/Y/uxKSMJ4S4zKLlyEfkljQjJ2Y89O3IY1OSg+BbvetQuC0RyyMYHGCXqGoq0uAh4KeYma0lZOJDMjAzAMSwRUeEh0FJAe7T2KvLyr6BpWIXdjz+GrVlx8PfVoKXsKgMkCqj76Y79z+5HRoQzClgmbU4hSMxkWeobkHfhIlNgxrHzsadw3/pkhb05UlaM8zyuay4QTzDoal1qLEIdJ3CF5VdU1Y6orU/g4W2ZXPXbM0LVokSoirj37SbOW5/sN/zLlIahtlJ89MkxnM+nr2HOH09+80lkhjqi/LOjJLqcI/h8ECEb9+GJvVsRF8GggAFi81gmjQNG7HjsOWxPi6B/vRrvfXQCLVxJTBOIMDbHDs1Vh7MHT8ISkULyTzych8qRm3cFzUNz2PHEC9jBICY/DthX8vJx+Wo1JjUkzzxxPyLcDbj02TGcupCLtqFZbNv7LTywMUUJZV+L2v0Ndfkl/3R7UbtmTPY349NDR1HVRXHwiXFCCjg4xgQxAv0Cjp4nxWOOMAjmTqtIm3LWd6OaqVDG6VG0N7eA3iSEMt1psI7s3lEKjI8zdaW5CyML/tiYnQhvhxmcZVur65pCzH2PY8/GSEy0F+PdwxfQr5/D+ARXGRp/hLtPIe/YR/R1RiDYfhwFVZ0MeJrBQDvTZMj6DUrJQTwnaKNVHJg5mdU7ReDpb+zjhD0MwUEBjOUglYj54HbMVNgi5ubFNLO1qN2bG8/djNq1Y/6nF0HvARJr4UaTrLMzA0Fz4D5eh1PHjmHCJwWB5h4cfftdnC1qobV0HZ55fg+SIyhOQwNCzYVzuFzdA3ei1557ajcSQn25SJrA6dNnUNc7i6hNj+BxUn886GITDQWJyL5j+ovkTxrmmLxK6T2F/sIBQ+glVvoLhQEoomDPmZlQQKZIcRGVfVemrQgVRUyhBt00Zhg1aSfCCtwXzBHNJf1FI/SXnFTmW+qV48xM0BdCikI64frXqBPSAwUZ7F0oSuCqJNQK/UU62lmFFGElzIjPUDrIVaG/yOqc9zC/oGHSt71iQrU1IfFjmjgzsgj9hT4/smpIf+HAoDyDI5/djTMcGrUZlWrgs83oKXJBWooHvzfrh5H73iekvzyAbObGOpKIMzm5SH9RyoS1z/PPsiynSEnRsCylTKTczRSBmJ6aYr4my91NysRKo5GXe1XoL7y+js9oJGHBgVQaV1I07Oi7nmXj07MRiiiCC/PxXJT7Yr1KmVDOkfgFhcziSH+p5BjqWB7WfGBWKNOA+tjBvnlOj2e+sxdZCv3lhuPoUzUzaV4a+Rwz9O0pUuFGPwe/palcT5IDk/Pp63BxcWXUpTXRf43+Ymutt/9TJBZFHSo0NPQrTdBESEFkPCUMQib5KppxnZ2d2EUQrUeTmrLRJ2Vn78SYDErusT2JgIaETGgomOBMKozZSE1bnkD5ns4m2deFg5mcY2ZGpxBbXNj3OEvbmGebomtANoYwcfBbQB9T6Q4eqcbD3/0uJ/QeFO2Y52sl12AMAu/HiQpg9krbFfKMiW2KNCMGOdkpgQnKqbiviRqy1O0VItOiD03a4BBh0lIm95T+sPWR7sq/d5P+suyGWXcmjlEqlRGVeadx7HIfnvgP30WCjyP0pPvMMXhT6DDOhIFI0JnSLqiJPEsXohMXGQpBjC1EqFTT/N7ECAwhRlkpZL/V9BcjA5Qm4Owh9BenZWVyu7/Ii9DJPEZfX84Y+SLda5uFJqMhila4UeJwjf7C2qOy0yBXCaOaQObQchZ5xw4Ka4uwTS7W6C+3/4bcs/SXBSMFITrRNe2CpFjmKV9PKr39wlg8UiYIIpsoRJzbUnu64zv47TvB10F/6e/pxfC8FskxgRKvuiKbTAikbu3kgX7bNmcux1VcdUxPr0x+nwykYnKSBi2zxNVQOlrZMiURh1KAIik4Pb0y+GkpD/lfZstSPvdamTj5hDEUnfQXA9mtK1DY0i5kRWozTcrPe61MVqAY7ugUUoYykErZSb8iLoN7pwwXYE+zcZSbGvMGBiPdUUlcP1jKRFbpMkkT+svdlcO7ft3f5k9SJtIXS5lIO1kN+gsbIly8AhBBi6hYQVdisz2HtHfVGv1lJYr03juHDJ73NP2FL6OADFYyQEjKRF5wZ/pU1jq822vT4kaRdiUqYqvSQd7ebX7OUSKEf7NA/efs/KW/lo52jf6yvLhkEF11+ov0Gewx2G2s2CbPIXWr4oc7PK00Put9WWef8vvid9LRyV3LDspD2O7f+jDyN+u+cgLrvjc9JI8T8XfZZH/rNeR01ussP47f0V+hnG3JvvJyC/3Fm2BsMe3azqGcdAX/kXta+py8W7k9Xo9fL96X/KI8B697/Rl+w7MxupaHL9uWH2dNP5Idrn/PY64dJzl6N5efvNxSJkJ/cXWlkpTc5F3ZFtvCknPbrqWUl/K9lIn1/pfsZv0o5cVP1/e1ltVN+ylfXL+W9RrXf5eKuPaI0nZsJ1hyfnmxhYgj9BcZTG33adt17edvLgGpIxlExdQl5vHbWZHa6nl52S/W42JdsTXIa7Zss+1vO17+aPtOPt/6+6XnWdI+5ABl+/y/XzvfkvZjO0r5ueR7sfqs0V+ul46UnZhEx8fHFb/x3ZlwLa07ufat6vd6u7idtmJ7Dqlbmuzv3MF0nf5iPdc8zW1TCv1FC1eKrS8wAMcwN2+lopC0oFBRaPaRoBvaURiNR5Org6viE73h/eDzc/kvQUrM7fIg8JsRx8pmJZ3IcW48zkZ/4YpC6C9M5nfXelwTv5cHlv9tBAbrGVb+XxMT/o2coQj9Ra08J/PUmGQuZAGjbooqPfMKEcd9MZpPfH2Tk1MwM7BG6+F27dnMSoSilYijJdnmpo36spNUXBG5NC3LxJapZlGOE/oLy8SVOS7KZoFOgo1If5EysaW1ScORMpGV191dfdGBz4mMkkPLEVFy9mw5gmamC4mwvwPLSOpVAtdMXBVaO0qpM+b4MQBLqXKqT1jpHUzQtz3E4hMu/SGUEIUwsyiBKMFfJuYKOjK47XrbstJfFmgaXkoCkXJYnTJZese/W5/vuAwZ+W5iAIitjdhKxyzmM8kjVSqRhBiRC1yc+wlpyba/BC3NMeBI2pTtvVDOwSAgaT8qBgE68n/bZmLeqASciJjLtdRV2x/5U0hEQodxIk1m2cbgPglsErEQJe+dUfBmpe1K6BLfKQlgWzxAysTW96xEf7vsPu7RX5aWiXxe+U3ceULQkjOz75d+Z7FPWH4tCUZakIHw2tc2YpSI2SxtE9K/z7OtODK33XbHUq/SZ1w/+tppvsoHpnv011FvdxwJGWlW+gs1ZotKSH8ZmYGDZyDWrc+C+2wfKhvaSOGg7BepKEbSUeI2bEd8kBuaSpn+0UOKi4MvsjZuQCxTIBhUp2xCf2mtuIrK5l4YqLofnbEemXHhlOoaRylJJzb6SzaPiwllXmp7Fa5eo79kIiczziqA/1Ue6Tb3lUFghCo9pQ1djOgT+gtfUJOKEmJb4K+ZQDUJLMMzs4wE80f2hnUI97ZDK8k2tcxlMirEkg3ITAiH3SwpLoXF6BycIFrOGxkbNiBuCRHHbJxGU3kBU18GMad2QVxWDtKjSX/hcUWXr6BjkGLaLn5YtzkHkYHuGGkpx9WqFkzPqRFA+ktOZgzTcm7oFG7zmb/4MOYH6kZQzpzfYSLvRG3JPzJDodzMkchRWlGDccMc1ZySkZUahYXRFpTUdrEjZFfESYY9zYMpGzfCx24eteWlTEGYwgLzi9OzSesI8l4WLUmuHiYGGlFey1V2XBZi2bZG2qpR3drH+qAYTSjTZNIj4aqaQWNVJRq6mT6hcmO7zUIq29xKBpd8cbms7XFjCVjMRuZgt6GBAix2fglYlxDIAU+IKmNoqWvEkG4BKaQ8Bbg7Krl/pcVVbNOchTFiNzR2HZIZZTvZ24CyynpQ0Y/0FxJkMjPgTfrL1IgQPWrYBk0K/WX9xgx4kP7SVV+DmuaBaxSXtAiKctgmYLOT6GqpJ2xhiAOkCr6kMKUnh8GVkzgz80TbqkrQoXOi2MgGePNcPY0VqG4f5+SW0Z7aEIUw48MJs20wvfF5136/myVAEld/B8or6qHnYCoLiygSfhJDva9b5xhvMsJ9qht6YecRodB6nOgZH+ptRW19IyYMVJDzDEZ2VjqhKwvob69FRU3TNfpLVloiPK4hQhk5/nfcbueRFjjIdVLJ4/1f/TNePTlCBNo6Rbmm+OAvcaR8GFo/T9TnfYJhjR/RVQvobCUVZWQKLbnv43RRLfwTs+HYV4J3KBascqdGT1MBSvvViIiKYu4P3yCuQMbaC/HK64cxrmJ6y0wTTpQNXKO/vHqkEBqtJyZJ/igbVIOXw4V33kXlqAVax2nSX0oV+ktU+OrRX8apmlLT0IGR0XHSXw7iXEUz/KNi0HL8I1ztMzGvyQ2teZ+iftYDPuoBvPGrIzA7e8JB34gPLk8imkncsy3H8PPD1dBSsGK67iKKx9wRFxNJ9BnnQFydDzXl4+XXP4PRWUvl7RrSX8YRQfrLZNUpvHa8Es4+HhiqzkfV+CL95fV30ah3oAbpKOkvlfCMIv0laPXoL1OD9Xj5p+9xstSD8al+wCMSIR4W5B56CUfy6zEx2oSC3FLYBUYzP7ALp8+XoamZgvt5n+HwmSokbMjCbGsuXvr1KQxz9V5dmovacXekUoBDu4iWk/Y7Nz2Ik2/9HG98+BlUzP+K9rFHzZkjuEwaT1djGU6ebUfsumRoNaP49OBRFDe1EuGXT0WleWSmxsHP3WmN/nI7HcENx4jLQIJqbof+cur9N/DyQdJfXJOt9Bcq5naUnMOvfvwLHCsk/WXnDoRq7dDXXIJf/Ox99E8MYXRyBE6+iQjXLqCp4DA+vViENuaT5l0sWKS/6JB/5A28d76K2lpqzHFADo0JwWRHCV5/+RDqunW0jDG1yj0E0YGeSsoYE+Yw2FqEN3/2M9TqLBhvLmVedxsiuSgI8rDHQEsxfv5PP0ZeVT/SdtxHtZsxFB1/DR/mk3A0NoJxUq4SEmP5zlpXpeJHE/PuVyuTGwr2d+zXu5lHChhQX5KHX71yBKP6EYzRSukRkIhIarXb3Fz68X6cP/o2/uEXn6FzOgAP7kiEna4Tpz76N7xxpoEzOKYcXi3DlBv7S3UPPvv1yzhXwz6srxaFxSSLRa0n7cxdYSBL3d72itRkHKVMVy6KKgYxrI5X6C+yKtCN6OBN+kt8cjpc+iqg45LaJz4HT5MHNzPSjveHm+C8aSs2JYWjhWi1sNRtOPDYLrj1X8QP3h1AJ1en8QGhHEfn0V9fDF1ABh5R6C8DGPrnD9HT0Yzu8hpEZtyn0F+c+y7gB+8IEmsQZYZA7F2kv3j3/iuGB6jwPxuPgFtYR1e6XQr9JSTFSn+R5PL3+prglbAbG5MjUNuRhqc2bUNagjeqDHX4YGyCqDUNdGamb0SlIIx+27NddFozB9QwPQCVbwBnUInMb+zAaUoIzs5bhajMNDP11pdgLnwTHlboLx2kvxxBZ0cLZq7UIXHjHjxGaUZzmw/+6WAH6lw6UWmKwAsUssgOVUHb/RPm+A2S18iB+ZZmjpUuFassor1bJOkv9yEjk3xYT3foBypQVjOEh7/zl9ie5Iu8N3+AqtZ+5BzYhb8i1cNEdmvp4dfxRhX9t7yllsJCxFJM4rmHs6FrPIF/eacKnQ9uQIhXMLtGmXPp0V2bh9xGNeIjIgg4oGmNE5RNT3wPLA2uFq7gZ//3ipKL6sSJ2jN/+Oc0+8+htfAj/P3bzegfmUZSECcma9vXVAImRSDExTcO25L7MUwOqUJ/kXZPtbTEXVswmNfPehazv7gK6LrxTcKTTz+MKIIavDy9QAgeknd8B//jQSeF/vLOj15GU0cPYijvlpvfi61Pfof0lzD4+PhyUjmCCyW56PYixeWpjQgL9IWXDykugipUNgk6o8wcgQmbdzyIQMpNnjxSpuR2z4ySK3z1IkaZ1B/HPG/R0hUXhR1dU9mP7MVDGZEI9PWia8VpbTW6WJqr/4Nmd4s9/MOy8MI3HyHMgnKrWu0S0y7z1WkV9fULwUP3GdEiiRCkDZkME5hkjrln0oN4fJsLJ2OXMTI0iFZzF6VYXfCtv/hbhf7y0YcfI6+wBbuTiU5g/ct2mwMpbcpCf3nme0jZEId/fGvwGv3FM9wZFScKMTXdSoWeVuyMtYeW6kJOxKy1caXUYIjGgc3bEU0yQ8OCF83BpLhwFeWkpsyf4xgc2Shlk2AZC23XQn8JZEN3o3h9MmcUDjzPqMWb5k6STsg8tdJf+hX6i18C6S+BAXCj+UfoL1WEAM+vKv2FmsFMFh+oouIS6S/Pbt6K+Eg/RP1BFH0wagy1FKGkYYZUEl8EBrFMZj6iApQeHqYWzLrdD3cvd3jMhmOw/AwuGGl6bCqHNwcMm09VcYjTzJSUGMEgIR+C0c1I8Rf6C3058EFcZBD8vLUUv4gh/WUA9mbSXxJJf2FAkVZrQUxmEFab/iK+KydKGVZXXMbYUD/vIRuRrBdHDuR9rXVotaNSDCNlx6lCpCIyy41tZWqyFRVlI9j04B6E8fm69PaIjQrlij6AkX4JCFyoUKJrZXpBeXvCeDtw4WINku8nVWihEZM0qanpT3PUGFBVVErJwZNsd1EUg3Dn5MQJztRghXGEgOgpTvqiKS7ustbpyUv3tW1Cf4nCrvsZW6An/YW1YaW/EHhP+ouZ7059OeWLFIeXmn5t1vscXUiFruhsC0f8OnFv+PEcAXwCMybmdHQFCf3FlSpH1L4e5OS+i+zSwUa4BsUim0phXfX19MW6oq6SKlpVLohZtxMbk4IV2TfpFr1FWjBQjY9ffRXuDvMwR+1HtK8dehrKUN0BPHngAegoGyg+NA21W8F+qZ0c1CvT7QgKj8VGrl69GaewZtr9OhqVBs72jLeYYZ3nu8DPN4LjVA6SwjwXfexqeHKxtuX+nbRozKKbcoASse9ISHxwQCT63z2El2rYyCxaPPdoGNA+qNBfooK9EaQOR0BQJMqoKjfLiZ1t6iUT+tvYGAhC+ktAkD9NqhIApAQVUxViAu19U5wJkOySmoisWB8MUkh9eIK5ZRSazj/fjKgUcgPp/9BQ1UgiTGSAEZIJRyDFh7YYP2C9J84SHBZJJxJYIPQXecGuHSetVI5dbK12JEVIgAr7Vuv3ygfrqVbn3wUYJkl/udCKRJoRE/lyq3k/VKyiakohPnz3AzTbpWJXThqc9FwZeoQiIiEJqSnJ8JkZwMDgEDqahzhgxBOVlo7U9DjoSJKYYLDV0k0pEz6z9Cs2+otkGDuyLJUJ0mJZSrnYM6hLIoaVPkhqfbGslp7v7n2mOL93OB597llkxAejr+wCfkxzS7vOFRu3RqP5ymF8fOhXOFPUAB2bg1pWyeYZdFTTB6wJRQblAb1daR6z18KTucWSRL1AhSuVWtJT2G544wvGKdRePI2j1Q7wI7XByPLv7+rB2JSesOhBVJFdW9M5LjwsjFENSolRXzCguSwfp64MYMvu9YjwX1uN3r028GXOzP6EnYADnaLyvlzb2Jg1JHI48G+2by0c5HxDk/DUC88ijhOi2ovH8JM3T6BnalY5bLKrEmfPnMIMzftCf9GQO2tkh+fkShKQ/STyT7xBfindTP0MyuPEzcvbDWNthXjt3QsYmrYqHSkdCIMbHTwCKEM4gqFRPS0jRk786nH65AX0eqZSXYzCMX396O3uw7yDN5I2P4b9XFQsjNTh47d/ilOUmDNaDUnXHmftw2qVgAPCE7Lx5HNPIpSa71eOfYJfHLqAUVr3bJsECdkxIE3GHeswQUsHxxtXJ45rdnOk90xBz0DWOR3HLvY39i4+UBgk7HPVDFZTU+LVlk0i57zNFantduSnNazYRn+p67LHtj0P4MlHSH8JnsP/Pj2Fzt5x+HpwZmgJwAMJMQggj8ZCZ67dglBcBijVxRSOqSFqsTIqig/IM7Lj5+yU6LHmtn5kZgr9RUgneoQwU9pRRYV+HpdJ+ovTJI/jKjbKwQndzR2kv6TzntwU0slcKEknq2LCXCwPTg4mKYpegVA8SfqLr5Q8I2z76q7gvbeO8AXMxB9871GkB9qhikFR/hlbsf9xItPsejFY+a/obmmBubQTm+9/Go89ug3o9sPf/agQPQOjSOBsSjaVxoLGln5sUegvkwxKMiCW7cMeXWjvF/pLPOkvAxiW6Ed2Qh0sk1FdlpX+Qqj3qtNfNB5Iy6EgtIr0lwANLr+Yh2HDXjzw/F8h6wEyaEc7cOydn2Pah42VJj39WB/yL5Uict0ziAnxhT0D1UzGfrT1jyCHJmn92ABGyQy0Z5DJ3KyBwSjTFA/vh6vJSFB8If1ZZRhkFHd0bBRyopPxe/8lFY93V+GX/+unqKjtQE6CL+bbr+CjDy5Ak7UH+3alW9FKi1W49uPrK4Evpr9Y5/+Orj5Yv/0BaMwT0NKS8U+HajHEKHbv+V6c/PRDVDA+4JsK/YWQb9JfwkIysWv3gwr9xdzXgtYJA7wCGfS3bSceeCASjQwmqX5/mFH1lLIkTc88N42GKvJq20PwNz/8G3iNVuDQK+/iU9dt6O/kgE3ofOHIMEX2qzASydiQiL0Ijs1GRLIdhiMY29DzcwYRDmNfZiScOOFb21a3BCy02rl5hWDTrmgK1vfTQjeDd4trMTLzADy4hrRwkuQigUKM9mY8LydxGi44NBiurUFRQTWy/9Pf4Zk0FY4cPIwjh0vwUBb95CM9HIjN8CL9RT85Di//u0p/cUGw9yxaaktw1sGA8Zo2ePlm0nRGgfauJqhomvMnZFqalmjkRpLucLW4AqcYWGQ3WIYFjzj6O5zQQ9LJjMwASIMxXi4hs/MoOh370Uj6S1Igl9Z2YfigqByn6EdV8zh4EWTN4JOgxtO4mn8as512KGzQMQpWy8AjDvWyQl2FTegvo0J/oaamjf6iH2/He798CYfq7PHYs44Yba1C+VQQVC7eMDLKtCSflET7YSLkPHBfEFewEd6oaKrC5fNzsAxVw0Q6iRtXmp2MWjV5+MM1PBX6q1U0V87Dz9KFVnsv5ASFIjQlBB+R/nKSPERjdykcA1MRRmxQYOUp5F06jRFvI8Hqc9i8wYOmKpaJLXfgLpaL+LmHGUld1zNJM/08OipriKGKQyBvYKilAYPDo+jpJB1mxg+PJkdTXNyE7goGigy74RtPx3KFydQp1nlgpC+OFuTi+Gy/ElzmELmeg58KDYQg6INo+fjmd5FMXd3ZmXHUWAbZSabC32WW2poXYeKs0zTZhRl7V8UMbhxpxNu/eBEfNnjj2xmkddSwXONTEEpI/Nr2dZWARHez7oqKUMxI7gYnPxRHa7GBFgkTwdwFjGKvZXspuloMF1MyHKc70EZxehWtEdVlHYiIToWXWodCBvwI/SXnwB+QS8oIXjOJSn7BCv3l3IUzCFGNobnfHdlbEuDg1oUjuRdwxhRGa1EztBHr4Ep/alvlVXQxdsFFUqYoG9jZyEFaR9eVqxZhMel4fOsm+tctGGqvg2nAgPR0dtb8e+HVNli4QBhtrUXvTCA2RPgy6nhVzT9fV+X91l3XRP92e0M12iepkawbIGJxBLGxW+E2P8LJTz3GPbKwMdKJC5xClJVXoq2jD0VlIfDlQKlh6uRkfxtqmSkwyZRNdy8PUn/U0KIIn336KcLMpMV0TCH76fCVpb+YZ2fQp7Mj/YUYrKBADppcbZEWfymvEIOWEOx9hELrMb7QDZNE7hyElLhI+NAEp6JYuSf9ENMNNcjNzSfFxVdZnWVGuqDgtV+hUfyg2esRbG5DYV4uB5dhbN0r9Jc0hAYFYaqumrSPfLTq/HDg8X3YSPpLhIsOZexwC8qaEbrpAPZs3wC/Vaa/TAx2Y55ItNTYcEbtqbmC6kRuBdNPGDk2M9yN5uZW6N3CKT6fDJfhOuRezEM5zbmJO/bjoe3rEc2BtLuiGBdJMmkdVePBx/YgK8INRa+/hnbnUCRmMBx7tomrtlzif6aw67EnsW1dEm37gRirKMXFy1zBmoLxpNBfkoX+MknsE6N/a7oQs+0JPLQlU0l/WR36CwfStjJ88ukJXLpSiTETqTRCfyHyqv7sx/j42Dk09utw36PfwYMbUuGqniNpox4GP64gSLQRSo2GwvKeXp7EyBWT4lJBfd1oPPvcQwjh5OOT145iISKZyLlkhLHt+Xl5w83ODJ+E9Ygid7A17wROnL2ESq4OwjbuxYHdmXAydONicRuj7eiX7WtHR2c3PCNTEO7vRXeDWUkUF9GONXHx2+sfby9ql/SXgWYc+/AYanpmYJwiB9JgT6tCEIZqL+HYxXL6owikoPC8nUcQnKYacOTYKVwh5cnskYDnv/0YotzNCmi7b4JQgolRNLV2YxwByMlOgI+jHhfOXkRjzzRi73sM+3ZkccLP1LCay7hUVAODUySe/cYe+Ft6cfqTg+h2z8DWtHDYT9TQ957PtJYJ+K9/GE88vBmJkSF0vQSSG+xOk7MjUtjHuDDCM/+jT3Ay7zLaxsxIf+hp7NuapBCFpBTXonZvbkt3M2rXMjuJxrJL+ODIKZRUdcEtbB2+8c2H4TJai+MkP036Z9GCOYbLJw7T1zkMlWkUOvp8ojM3IFBrRhlpWVdKq6HSBuHRJx5lymUo+4YpnDtzDg0Dc4jZvBeP7SAxiqvYFaW/zApdgwnIdjRjSD6lnlqGesrn2y/SPqwUEqGiqBURBo3YpZVtAXOGGRJQaC6xE9IJ7Sr0s+Yd/BiapM3Mt0yBCwXbp6Z0DCGwIzXG7Rr9RcDg04xyVTGqz41EEWH/WbginCF5RmARzq4kzDBxVi61mvQXieCT5xT6i1zbwnxISQSXgcumnqGhGVoiBCUKd4ZRYuRI8BlcFaIACxCz1PoU2gm4nwfpEhbDCNNFjsBl/f3ISif9halH0/QBWtQ8TqgxikCBlYgzbSA1xkGoFEJ/ocGdvpxplskcZ9guSplYc9vk5e7u7lZUfETZ6K5sNAWYeX29niYz5nPZ875cFukvc3oSNIiwohMcriSwKM/A/RUBBZpfRCjB5oMQPN0s99cbyA6iD8PVwYz20tMMU9fj6e88gqwon0XXLwPUmJBvoR9Vyn7OQMoLtV+lnJycXUgc4gqX9WFgfSgCEeJvpyvBkecUkYd5Khv1ME0niBM1UTZa2756CYiy0W3RX2ixMPB9lrhcMSGplDojApEmeyPpUtaN7iBOrDR8YwyGWUZmLih15+JCqg/fmzm+TyKSIO2FmdzKJEwoUws8h47v2fwCSUNs605sB5JhMMsgNz0JL/bSphjH0F6Zi/eE/vK97zEgyQsW0mR07JuEUOTENqoQihbvRGI1TCTQaCTySdonUyBEdEZl56jII16PACZOl39bo78sFtzij7tJfxHS1jzdPnq2EfF7KnXnMIfy3NM4fqWf9JfvIdHXSXENmawNTlnYOToxxob9g9ChFFEP0n0EwSaxGSLcMcN6VOhl7EelDckoJnhOqds7xqgtL547/23BQif+4CRciAdzl9a9AptEZN3b9JdZDLNM3LgycyUuaiU2WTkIWi6Qs2vRRb2nNiZTD3Y0YcyO6UP0oy5JJ72jx7BNLtboL7dfjPcs/YX9zmh/F3pmXJBIDqqj9J4rtK3RX24uyK+F/tLbhxHSX5KiA5TB8ea7+urfXKO/yIeV30TeTSaWX845KRJLMgu17e3kQg4hO8vpqTnlO+Xv/HTj6W48TnkOnktmCkuvLQOpdJLSoGVb+jfli6/xny/7bE6MWpXV3TT/l3L6sscpj3aLMpFVus0MJ5/vdpnI/d54jVvW32+oi6XncPIJQTCl20SOcv7GhrH0HMqz39x2lu4in+Xc0kbkfxEUkJ833u+Nx6z9vrwEpAzFZCdlJx2lmMfvWhkq9foV3uXPaQdL25Sdqx8iSH8x0vphi9+Vv/MqfI7lz3qr35R9l/Rjso98J6t0W5ncHTm8W93Nb+93UiaySpcykfFHImjvyqbUua2NkP6i9UcYo8INtNBdH21udeXPGb+Wnc9atzKmSD+q+u2jv1gfQh5vpV5COY88sCP1NO/qy32rOlmR71a+TGRyIS/4PVkmiw1aKdobOq7bLW55uWVCIe1EzLqivblS7e927+leO07KUDoVaVdi5ZBB494pw+vv2JcdOL9M/UiZyIAhEwxpV3dt0PgyN/Nbso+UiQAiVpX+wmvKlEi2lWqTtueQulWx4X+JuZb1Bj7vXznhspvj7+LfElu1bSYn+yzdlu3PP8ifbfsu3U8+245dfgwLRhxh0pEuOVD2lSvJd7YHkw5SfF82+suN51+535e+jNaz2u5NuS+5Xfp1rPf1+ftKYSjPvKT8lt/j4t+VmbLtKbnHteO+uEzk5V5Kf1l+/rvx2+LzLqmXr3oVa5nYyu/mo7/o7zcfsfybG+kvy/+69tuXKQEZRJfSX77MMV+4j/JOS5v+wj25g7Wd2d476xFKj/BlDr5pn1u3qVtd46ZDr30hq6+RkRGFiLMWxGYtFlmJTkxMICQkRJlwXSusFf5w6/r7oot8Tv0uGVtsZ5DnUOgvd25qoAiBBLgwr89G5ZijqPP4jBGObp5wo8qMZZ7Sd4wAEiqKJF5L7o4dg0xkf2nioiFroNC7PQMJblLop/N3anJaob9otSSd8GFkE/rL5LgOakehv1COS/mapJPpSTr9KSHmqVXUc5Sd+Y8UqMwG7/x5bWe8+adJUjAUyg2vx2tJcrkdCQIK/WVmAjMMr3b28IQLA41kXwlOkM7h+r5CiuHfGAk9MT0LO1cPaF2Yq3LjJvQXlolZ7QRPlonNMGKmossExeHtnKmQdI0aY8YMoyBnTWp4eIoS1GL5cUUqZSLlcTfLxHbrYqqflWAhimZIzpb1LmRiYWZAgKjDCHllcW9OIBibxPtaFJiQrxkcYphlO+PxtnZmO7fyk8EmRuMcaTE8vw0RtLiDQg5hwIhiQeL1TBL4pfyNF+R/al5bAuCkHFazTJbd/+/IL3dchlI/zAOUwEVbIxHqhgR5ODrcmPYuwWmk/UgbXmzX5nmudBhAYs+0JyXrlO1mnhNp68mkg+T/0g8s7i/BfYwZYrsioeWmOmAAnxBjpG8TCaPFTVwsRsoUKoQZW5td/Ju0Z1ma2POebNsdl4ntRL9DP+92mcgiThZQrA7lXVfTwmRnq/Nl5Ui3HzsgO3n3ZU/2iyYGzFrD29gOeZwt6FEhBTGQVOgvtrYiY4q0qRtb5rJLfPEvJoxRxDeveAJJmemIi6D4fH8jRX1r0Ef6i51nADasXwePuT5U1LWS/kKRBVJRZjmoxufsRDpTZlwoMNxDCkrVuAcymMIS4n09wMgyr0MzSSfX6S8bkJUQAce5MRRfYXpI3xhUjqTGbMpBbJg3JtoqUFDZhAk9dRQjMrFxXQLFyG8xEH3xg33lPWz0l5K6TswY+ZyMNhY4RULOdtJfxijA0IBxyvc4U5x9fWYKHCZaUV5/fV+JNE7atJM+GpMia9c3boCDewDS12dTRpHCBIs1Z2buXEPpZYbkD2FO5YLY7I3IpAi3xjCEwnzSX4ZIf3Em/WXLRkQFeWCkqRQFi/QX/+hsbMqKpVoQIw1Xa2NHNd7Xhsoq5m9ReMPZLwY5WfHwpmiC2TiJ6qIy9Bq8sGlzEnzc7die2lHX0I55bSzTd8LhwRSi2alBVJSWoptkGwvpLxnZTIKnXJdC6uALY5joQ1lZBXqHJqBxC0DGuvUk32ixwDD41toGdI3okZiTg1BvVxhGmE7FlIcp9rYatfS4rohKy0F8qO9qlcjadW5RAkJ/Ge5qQR01lx0CkrE+MQh2lhm01VSgqqGfgvOOiEjMQHZ6GBWKaDLm/oMdVdRonkBEKiEFfsxV76wlUajOSn+JIEGGKWaqiQ4UlTaSsMQBVIZjV6bVpecgJtBD0DJoLS+i0hYpLps3IICTT+u4yDZFoZfq8nK0kMK04OSDNKbiJVJJabSrHuXVHaSKzMIzSAgz6Upblk7YMDmI8sIa6JzDsHVjrDJhvsWjrn1110tA6C/t7BNqoRcZP9JfohPXUSJwOf1lWPqluh7Ya6OwaUM0pUz16GmrQVltNxd9jPD28EMy+5IAF/KsWxtR19SFWTYQr6AoZGWkwJOa3rbttgdSQZx1kP5y5J2f4eP6GPyP/xXHwcwZlZ8ewvEWNZJSI1F/7l3mDqqxPdoN48MjxIiZMFh6FOUjGjwflo6EUHc0lJ3H2z95A/1xT+K/x1Mr13Zn0gGT0PDOwc/gFBYPfxUT6T9gmPHzB+A/kIf3jlUjMikOlqYzaGRy9lO741H06w/Q7hDAlwQ49vohhq1/m7mkCXc6W7Dd0W/+yaWlpPKMEPI6SRZrX9Fh1Og88FxACFrqz6Fyxgfx4Z648Ou3mOj7PWwMnMMYy2ScjK+ewk9Qq/fCt8MSYZwoxvHLY0ggsqk/9x00TJrx+09RlcVTQvaZl9lSgDffOwlfSgt6zNbhzWbqE7+wBw5tZ3DwZAsS0mKgrzuJpskFPLUtBBdf/wAD3pGI9JzF0X9rht2ffQ+71kUudhi/+ZHu/K/0TY/34vj77+Bymx6RROA5GVwofxjFwVyDwaZcvPrye2g0rkdYfBjcmQN69cwneOOTs3Da/ieIiQ6Bh72BVI+jePmDYvhQzGNisBOFHXr8+bcfJoqOKSpccUwONODs+UuY0k9jgLJtJX1/jD99egvQWYL3/+9ryOtcwH/7aRyCfVzZdw6hqqSISfZMlSDt4Ur9JL77ty8iitG/qzPluvNS/d07A0Xi2U7OH/k13r1Qg+hHv8988wDYT4+i/Pw5XG5lqttIP6acqvD//OBPsY6pKTqi0T597SWcajbgib+IRzBTYDoqL+BCfgPmpoYxNGmPqb/4e6Q79XFwK4CO7WSCWrvVQ874sx8kIjrAFcMdFfi3n/wM3ZpIfD8plQIgjtZVrOj1Dnfg/OlL6GK+cV/vMM43TuF/fmM9+qoKkF8yBOgbMDBMv/rf/Bj3pwRT13oGrWXH8OK/noFdzH6kpEesDaRfW0MVIY0KvPvmcQQk+MHdxw9O/kkcbziQLt6TfmKQcpGH8M8HqxCb821kZkXByTxOTWeOR8fakRAUAN+gOIQQtmLPfP+jr7+J6oUwRDh1oJdCD7Nu/4CHk/2u9aO3PZCajSR0XLzC0XsUY6qka/QX/aiBgs+JiElIhVNPOekvdvCJ24CnFPpLGw4ONsGFYu7bM6guMt2AM5euoo5yW+4pgse9vklOaF9DCZVrMrFv/z6kaAcw+M8foFehv9QxedZGfwnED97uRB3pLxXGIOzbtx87U9zh3fOvGBkU+kvCKtJftuCFhBwqYzTh3V7SX5IewJbUaPSTTpIQyRkqX7b+cwU061rgn7IFz3ElNNHXiHd7GuGbsgebkmNgaBrFo9/YhQCneZQPnEPxLHMhFfqLHaXLSH+haLYpcpH+oulA/48Oo6uzlfSXBq5oH7lOf3mvE/WuFIs3R+IbNvpL14sK/WWSUnteqyGdKKlM7WU4XmvEw08+yueLgqenN3y0Luw4O3Hh9GU4UMovctqDkx3mf9IUY+cZi+1p9ZRto5QXm8McTeINV0sRv+1JvLBnHekvx/HDt0h/GchBGAdSEfbwDl+PP/nLjVCTU3v5g3/GGw2dGBrLgq/KHon3b0PD4VrmJ1tzC72icvCH38/kikaPxpKP0fR2I8KoaiSTSzEDrW1fRwlYzftuRF3dlzqIIZpfzIwl8PAMwgPf+hPsddOgrfwkfviTw4rFJdVfhWZCEEp7tciI81NcOCoHT6Tu/A4SH3bGRGsZ3vmXX6Cpawa7D2zHf/nbLfQejaPg3CH0nJxEhC81VCe6UXGFFBe/NMRTSlPWFtf7Hw28Q1IoL5kIF80kLh9/F784V4cx8w6k7nqecAQVOiuu4MNfH6a7hqYkHjnS1UyBmAr4UWPbRXLIrRmxX0dhrl2TZT9vsYN/aAaef4HyjdSEv4n+QqlZv4BwPLxjHs0M0Vb871yoqCgpGb0+Hd/cm6ao03lpndHTPc4oYzOSHtqNHMcmIvUKqdttsILlF037tzmQWukvO579PaRSqPn/fWuAaQhm+qc08Ax3QvnxQkwq9JcW0l80Cv3FUUVcTUUeGknhOLBpKyJ9XWl6i8V3/+gvsC3RD0eIqVkW98RWzUUpEuLCEODvA1f2dMn+1FjllGJ0wZvmTpJOfKguoiI1xqEXDqYZ+MUnUOGHMnqcWcZkBJH+Yr+69Be+kGqaj/rKc9FiTsJzmzYjOsQfkf5BmBvrwKmjZ5HfOYqnnZzoO3aAI/2Gsm+rJQUvbNpIXp43Fnx2I4Yv/ZXjR3A2vxMBjzsxWdxqQlACKOg/SkwMJ9GAKj6cZScL/YWVOa+y0l98KWllColGqKafs2QjApKSEOjnSxaihZMPK/1F/I/MHLnr2wLFEcZ62zChM6C3uR65lER0DUqjaH8UxirzUKWLodZpDKqvjFE5hEnvNMtu37EVztPl6NJR3INtwMzRzWh0RBwVZfz8SbHhxChwoZx+eRGyoGWWfmgHV0+wOcEwTNTe6CgiqKTl46NFaPRmbKNGZPGlTkmYVp5XrbFnUj7F7adG6TYoJaN0L6HQzCvjXykPsbZ9LSUg9JdI7NxFGYUZukA4z6fFnlrbjvCmbKYAB2bGh2Bg+kKItz0HrUbkF3Vg82PsJI2EN9N3qmZ8hYuLP++eZj2mROksXsgI81MiZeX1meprJae0DqnbvodorQrdNSWo7FThqf27MV1VzvgMMf3aNvrBGGcQQIPH/OQYpkd18A9Ohp8Ho7k106i6eoUqN2dRMGSP3YR2WChDV0srR7fzRhzYEUipQz0H5utns5117edqlQDpLxRtMenrkZ/rAl/fcELhNyIlnOplSrWQ/uIfgs277sPouB5dDRIYy3tjfIWafeNoSzku5g7BhyvSrTT5+4SFEZTgiLdf/ykK2AbglYmn49ln8FyL3Qr7/dva2OioROQf4Atfri6sGg+iYjOJNtJfgiJCkUYb8nouqwcppD48Lma0IVJRWhCdmoCU+CDlwhonLVFoxIEJtFol648bNj6cEpDEG+YElb0qI/foDJaDHdjwFd8xP9P9ocwm7TgTvE5/kQPk/9XcxE/ST9m/NiQp9BfRFZYgFjq6qVIUGB6FDfFEOHX2Y2zCqPj+ci+2I5kKTvFRi2YCBlnI/l5B4cii9N0Q6RIj49PLHsJWJlKJKkpzXCPiOLBepKGwYChkxD9KEIWUyWJDIeppNd9vSbGZnRrn5EcNN09faO2mcOndX+PsxbP44P3j8CBgXEOfeU9/Nzr7RsB4MysBRAI1lKqT+2U9O1Az+Rr9hX9bpL/Io9o2y+wISi6dwJU+LQfqVAR40t/FQVPIITxi+bYggg6VyKtXU/w/Sdl3+Q5rv61uCUi75YSIKkHK+7v04pwsdtCceuZCA9J3PYloZyPKzpzBmVYtgui7mhjqQV8X5QANXGKw0Ux0VuLMmZPQRW/ChoQAxdIAsw6dzVUo63LH7o0JsFB7+czJ8+jWEuZM9aKBnn50d/VRJm55H2SZm0AF5TWvNhrxwCM5CPBwgnFmBI3VNdSIHoI7UV3Cq2yvLcLZU5cQTJ+SkQNvV38PugfGGHSkNOKlT7P2eVVKwIFUrXV4+vmnEO5uQuGJI7emvzDgdUE6TKWaWPfULU/K2ornD2TDcbYHRz4+iEO5jTDQz+pC5TlHyxSDPMcZ9MYA2Bm9cpitD7rNFenS0rCGpdvoL/Wd9tj6yP14cu8GDAbO4u9PT7OTJP3FvQWVC4F4MJ70F9cl4zcbm0TzLm1zNjk9tQOp922L9BcPUmTIFQylqLCjphcdA4PIIP3FmQ7+IZqBbfSXoawM3pwb+kk6mQv7Gugvg82oUoXhKdJffJyYQ2YYZaBRM3wi47F+1164GFvxk/JRDJOF6WRqQZU6DM8kUH+Y+5rnDDRhVcLoH8XgmF3wdzPj8s/qMTAyhVT6V6XmVHYWNDT3Y3OWEVrVBDpIf4njLMoB3ZzEDCOT9BfzRD9GJHqQK+TOpnaMzgj9xYL+3tWlv8gA7qz1RgB939t37kakpgetZ/8Rne1NaJ13hC8DN6526VBS2QPPuDhkZsTAnwL9Gk6qBKmmYcSchtGRZhIcWvlsVvoLJyEqrSI1KRKAJg3NciQy1OadxhFOSrYc+APsyKCMotLEJFKbLZOdtFoiOxeb7Rx9bzX5lzEbug1JUSFk4S5tz2ufv64SWEZ/oUSkmKT6mhmHcfg06UD34Y/3b4HzbC+GmwfhYXZhMMk4V5blhD+EIJPQA+eZMfotDoIAAEAASURBVIX+UjXpjm/9EalKfq7KvFE/0o2qwnKo4x9AUogb5qnr2zZIicC5ZhSPjaCW79woQQjxMeGiygmzypGEKZr+r57CkZPVCNr9LPZsJFNYtUAdbery/tl/x97Hy/DeKy/ibG45/FJnMLBAOcMGCYjrRnGdEcm1WZwcB8KNEepr2+qWgNBfXLXByNkRBbu5NOpqT+PdojrSXx6EO3sBob+42ugv7BuE/iJRxAucdHuHJGJ3dBoMKQFcBHyAqsY29uejqKgexV//fy8h3lzNuJ3DOJzXjIyQHGWiL093xwOpmBuZ2KJ4BDQkuoT6GdFUXYST6hmMM1LX2zebq1ZSTzpJf6ED1z+AjMCbypWrWVt+Jf1qnWzYsySdOIexEPKLcOHkJ2hzHECTigQPrtQC7MPx/tUynGBajXqoAmrveIQnRiGk6SSu5nI22qZBSaMB2UlfB/2FAT2hIVxpWx3bs7NjuHLqEMa0yUiPC0ZPRZ9iu3enD3C4tgn2JMX4+3gpK/R5pvS0XT6H8xNaZGZHwdJbxZVcMKPD1OgoLcGcNoD0lzTMFlTg3IlZ+JL+0ubojU1BYQhLC8WHpQU4ruPKrqcMTkEZCI8LRFDlSeReOIFB0l+KWk3YmrN69Bc1IdoB0UlwOH4WF0+rEWhPMXH3aDy1iQLye9xImDNglIFC7QNujOAOgxNmUFNcjIKSKjRQfrkgwg870gN4Dn98WnAJn+p7MdVUCEf6Ob0ZzVt/8TzmwyhuP1uPl378Ctq81yPZSF95cSUSU2Jhx/0vX76CKpqVtVcK6Jt1RWqoFuPsSC8WjmHTC6mkvrjd1BrXvljtElgguWcM1VcLUFhWjTpGyRZGubGuNAzy+AXeuTCOR7+TjdbKUkyHhGPXH/0hcuhP14/0oNw4jmmye/0dDVx5vI4X3zqFnP2/j9G2WhQbY8jB9WZcRSOKamax80/S4OVK01xQEv7oT/+UFhAzRjobYB7QIyUzEe6MEm5hFG+PXSJiHVvx8ksvoVCfiv+wnQFq5SXUpfbDUGMNJlROMI13oHvGGcHp4XQjheE/h21huoWegYUVGGJkfXKsQMLXZmir3ZLkeib9BNrqq9A6ztQoMbs3jiAufhvc54m+K6kj/YXZC6S/9NYWoKS0HK0dPbhaEoTYYC8Gc9ZhmsSpqf5atI3MImlbAHV1mWqoMqCjqZoZJ32MAPeEllKtsphdXM5C83fc5Nfb2zgAGnUYMFjpL+EkcPhq7VFTUYK8y8UY5spsH+kvWTE+0A8TdOtC+ktMhEJ/uX49vkTT49DbByIhOgyeGh0uvfYami1W+kvoQgeKL+ejsnUU2/Y9hp2EYocECTWmFvnsHDsM/jjw2D7kpCcwdUSPisLLKKpsQ/jmA3iYNBXfVaa/TPI5Te5hSOXs1tOFKyqmV3hRyrasqABXiqth8MzEE1yxJwW7YVr29ZB9+dwUjFUzX03r64zuapJOCvhC692x78k9SA93JBHnVXS4hCExMwMBxhZcIfmmsVuH+x9/AttongwKCMIEfT25BcXot4TiiSf3cSJBvJzzNIou56Ksvhex24X+ksFr2fOltyjyXG5ubjSp3aV4VTpiHYmfcjG0EYF1FY2909i6/yk8uCkLESEB1Pn1gzcjaRcsnti4Pg7u6nGc+eQYiQw0izGxf4rRcXHpqYhj9O5QFdN4iqoxbh+LZ557EMGaIXz8+mcwB0XDkzPGxp5hRk1PYaCjAz1D84iIi4C+sxQnzhdRVpF5yrop0hyimOaiZaBJH5pnArB7ZxZCvRiwtNgYJe9MEqzX6C/X386v+skmO+lBrWyZ5X+5jfSXwVac+Pgk6kkDMulmmDOsYvtwRXt1CwwMApngqrKzdwiOxOat47sufYCP1l1Z8fknMbWJZt5O8miHpinJJ/7vtj5MqvyQFusN3dgg+iwR2EP2rA/zsu0IT5AI8AASk7TM1bYn4Dtl2wY4THXg9MeH0O0UjRB2nmUNA4rrQdJy+kYmuVqRNlWMU2cvkkY1As+EnXhu/w5GfPNcPF9gIN1UBG842PsTVxgHNydrmpmIn4gow1crky9XcvfqXneX/jKFpop8fPTpGZTX9MAjYgO+8Y2H4DRSQ/rLcUwHZCPKaYILnE9R0TFKLjbRk7Tsevp4oKfsBI6fv8p2aEJ0zl48t2cTQhiMaNYP4dL5XFRyUPZl7MVT+zbCn6mVK0d/kWAQ+hbsGGhko78YSGYxMDneTugvlAoTkQWhopjozJQEV5uogq0RSISuiU49Dc05FkYD5x86TPrLJmQTpeVspuo+7dFCfxE7tRNzyGQWIGa9Gar7q+0YjENCiuQUSnCLjuQZSZZ2dnFTaCoya5AOUgTafXx8lMZsu+7K/xQ/8TxX6FbKjeLD5UWExWktEwuTeUkUoJaw5Jvfat8FmrOMfDadgWZpijlI+ZkNw8g7dBSupL9kMr3F0WQg0cVA+749iTh8cVn2vAo1QuU4I32KQn8RagHNmixbHbVjrfQX0nMWqQXyct91+otSwLRYMNpYT7KHmVQWkY6z0moWS59RJXPSflivKn4W0XOxTigbB2InEhk0NKkZJXpZob+w/OxMaCs5jTfPz+Lpbz2EtDAPkltoF6FP1ir6YackTatY7kbJtucmrgMRc1CS+ul3oxYGhSHYFm2VxH1E2WiN/qIU123/I8pGt0d/oamVghtSf7KJuU0IQPKOmNkuFEUwmuHs+U5IHVoXA3zfmDy/wMhtifmfZzszMY7CIvtzDzVdG87cV0QSJPBdfLBLqlu5jsQXmBl9p2GbamUw5MFPa7DnO99GGidcZrYd+btNSMaB1+bKQblPE4PjHJ2d2MdYCVPKyfiPci0qMthLe7YuWZRBdI3+Yish60+ZsI6NjSGMgTxffsK1/Byf+xvrf560Fj37QsrOsJ5c4GxvRHneGZwoGMATv/8dJJD+IvsI/UVpSzTriuqUVTyInQP7VmemVCltTc4nAjoKTUYC0VzYj1rr/bea/jI8NAUXdw8OBitDOpEO9l6nv4ywTFw9hf6yMqtHWTncy/SXoc5mjJP+Eh5MAPwdOyisr6RtcrFGf/ncLuoL/3BP018GutGrc0FClNBfbHaKL3zkL9xB9JtFNlEGDVlIrG1QoAajjLAPDw9XFOfuepmYaDnt68Mo6S+JUf7KQmYlrikTAqlbFUfUxen/Spx2Jc6xGHjE2dxKNWVZkYoeophW7jlkmFKkiwFdizPclShlGUilTLy8vLjqu64mtRLnvuvn4KpFVh1ctiiz/pVqJzKQiiaqaDKLmP/a9tVLQEx2stLw9aUaF2f4987Gd4wTbmqSKQFqK3nfMrkQXVkpk7WB1FqyMrkQQpCfn9+qDKQSy6OkD7IPtUn+rUQdi8le6lbV3t7+WzaQrsTjLT+HrEiX0l+W//Xf528yuRAznAyiay+3tQ1ImSylv/z7bBl39tQyQVtKf7mzs/1uHC1lIoOpTOLX6C/WOpVJq0y6XAlat5nA78XalueQul0x+ov4LRfdG9ZVAkf+pbOAm1cN11dZ1oKUGSH/v6k0rSkMyvmX/F2OUY5TVifXj5I8NFm4LSXPSAe5OvQX3vy1Gc/y5/m8+5XHtT6/3LP1OX7TvkrxfKVrsJzoGJLyX1om0gBWh/4i9aTc9bV/lj6n8iWf+3oNWnez1a3te1sZKe3A9uW1My5+4IVsl7JdQ/5iO/aLvrPRXwR2fs+t0m8si6/pd+lUBgYGFNLJbQWxKXXI9nBjHd/q+1t9Z6vvG9rUrdqAFJHte7ng8kt+frv9TcfJ+Za2M9nXRn8R0snapFVKxGraHR8fv6v0l2t1Kxe8qX7ly+t1vLTOrh+3vB1e+37JuVaU/qLXGxkswuR3JRCIQRt6Jq6SXuLo7gV3F6sDVz9LCTjewHXSiZXiYSGxZGxCx0gARrcqdBd5wCWbkE4oSGBi7o9W63ZNwd8yp8f4JOkvFCSWZH22X26kv1AAQC/0Fy8tnCSiZ3GTQrjb9BflUlQrmpiYpOCdI82m7lS/sHbtJurwTkzrqZHOlJwbfL8SJLTAiF1HSUjnSeZJf5mc0sPOxROejDq+aWPAwwTLxKwh/cXT7Vo6kURQj0/KcaTGUBTeupH+MjkBAzWPtSTiODAoSzZZpUuZiKN/xZ391gtbryMkBUVKSa4rDZdBZQyOkt8kCGmeQWZCU7gxvtMkQUIMAFAoINzXwkAyJSiJwUefl1WwwMAqoQxpqHKjBBUpdwAleMu8wAhihqwvPj7m2OEzxATOi5GVsquUw6q1k8V7+137YaNh3Ha7UgKDVMwT5rtrbapKEVkYIGZmfqC9Eli3WGqyr3wn7/mSfQUgYWag2nXahwSrzSrBeRJst3SbnyVcgudwEFaxrXFwhwVezyRiJ0qb5en5rogcpa1L+bzjZPWppqDK0qCmtXa1tMStn++4ndx8ymXfyKJBKC7iAZLGIXVyE1mMgZ2zDIpViFRLIswXTEb2I2bmFIuS1fWGNU/xDumvnKQfWbya7TmWt6plt/JlfqEcV081cosmkJydgfhIL0z2Msle6C+U1bKjpNeGDeuhJf2lvFboL0y0txh482bEb9yJWB81pbWYxDxM5R4qJUWlrUdmYiTcFyXxLPMzaCTppLKF9IcFR0Smb8A65os6zY+SdHKZSfoUE2MeZdamTYiP8MF4S5lCfxnXL8AnIgOb1yfC38M2oHyZ57mzfUyGMdQyl7O+fUC537CUHKynQLt6pgcF+aXoHZ+CydkLOZu2IDmSotx8/w0TPci7WAiHmHUkXoRjYawDV5j6MjBG6DYlz9I2bkBcuN+1AVCIKXXF+aS/DFMW0Akx2ZuRHRcKjX4QBXmX0Tk0A42zL9Zv3YRo0l+GGktIf6EW77waflFZ2LwujulHtxic7+zRb3n0AqNmR9qref1OcHyjDOAc7GneSmWbYAIfalu6GXnNugpNxbrsWOt9sQMb76tDcXU3/OM3IImyXjrmmhaXVGJ4ygj3wCis37ARQSS52KZJEik51tuM8sp6UDAKLv6x2JwdD3eNAfWVFajrHGG0pzOSNmxGlK8jBihoXVbPdKx5ewSlZGFdCgXzGd25tn19JWAhTWWIerU1FBtxDErBhqQgRuhLDTM9jqCBsoJKTDqEYtvmeLjZm6lM1YQa9guOwdyXpBjZd560n86mKuYl6xGcuonC40IAGkcrRRuqKOKwoHJHeGY2shMCYce+pbWaZJmmQaiZAhaevB4ZUfTrSsfJXPax7gbmFi5SY1QmTk59kMB3LYJSm521ctwQ1O48LmkDMtjvmaZH2NZqMTJvR5IMUyM4kb3eBX995frv88oclygJWVxaQ0oP6S/ObohlPSWH+3BuLrXCNDemHlaQGNU2Mg0HjyBkrRe6jwtGe1tQUlZF8QYL3P0jOH5lI9BlAT2tVMaqaoB+gbnx4SQLZaUqKYu28r3tPFILo6BaKd118Jc/xOtnxpCWQ7RVkAOKDv4Sx6rG4RPsi6b8wxjW+DF3S4Pezi7qGhKLlPchzhJrFJiYBXPjeXxCOT1tCCUDB4rxackUdVJjEOLDpGnOFkZbC/DKG59BR4k459kWnCjpR0BwIOZ43OvHyuBIecGZlkLSPlTw01pw/p2DqJ1U83p65B4toShBOKIifNmBL1DaadKaenG3gkgWSJfoKMSLPz8K4gbgNteCD/LHERHqhb6CwzhRNgb/EB+01xVgVBOA2MhQuKqmUfzpG/jHn78GczhfyAhvNJx8DR/kdcGXuWm6hqso6NYgOjbcij4jt3WwIRcvv3kKFl5DM1mPk+UjCA8lFYXCC2+cqoWHvzdG6y6jfNQR/i4GHH/jENrmneFpN4GLR8qhjY5BBBOPVVyRTjLi7K7mkXIgHWqvxOlLVejo6EHjleM4fLYS0WyE4zVFKG3uRB8H2hOnmxGVmYpwf6axTA/gxJsv4e2PjkEVtRGJwVoMVp3AsUuF6GKSff6p0zAFUTUmkp2nMlskYWa0E5+88ybOVnRxYqXBjMmZ5a7FaP15vPL2aXTOcHZJYIBHYBgcdB146+dvoaJnlNi9Wlwo6kZkCjWafdzY5NbySG0dw+3+lBXZDNOtvlrOJC1JY904+f5b+LcPPkW/WzK2pYQyZYF6yyY9WkuO4Ic/OoTSTmfs3JkAzewgTrz/On71wWcYcE/Bdu7rxPSVUeYNH3r1RXx09Aq8MnYhOcQVAw1X8MufHkTL6DT66ypwuXaMyMco6LpK8PorH6F1yMgVBhP33UMQRfSesrqlFWy4ow5nz+SjZ6ALdaXncPhiNaI3bIL9UBXe/NUnynHOTlwYaEMR7qlBV8lZvPLiK2yDvci6fwcC3clIXixEcaOs5ZEub1F3M4+UyxMKL+Tj9VeZM2qaxBTTCbWBidQ891gcSJn611SBo59eQHNPO/UIitAx64mMYBUBBe+RQNSAeeaNFhSVYtIxHCF2fTj+zivIbRrGNAlChSUceyLXIZ4ovnn6eaVub3sabqaiSEVeIWdmJJ2rueqTySNXEwbq6nqR/hIVR0WbrlLoabL05criqbRszAy34r2BJrhu2YZtGfGYLunAw0/v4OieCHOTChdf7cfw2AxPRPF2mun6KbllDMnCozb6yw/fR19HC3rK6xGTtQOPHtgB574A/J+3OlBfMojKuRDsP0D6SzLpL70/wugQ5eRmExG4KotS5hrNMn/W4scwd4pDUIP4dMcsptlB1Bf2IDp9HQJJIfBz3w638CBK+hnRX5uPcxcrOXhMQxChczTN9jR2I/v+F/DoQzkwt3rjr1/uYiL6OGL8XagENEsiThnMUVvw8AGi1TQdGPiXT9Dd2Ybpq41I2bz3Gv3lH9/tQoNrF2oXovDNRfqLR/eP6b8aIsYtalXoLyqKQEdkP4Tvp+5mLuwYSj5+FboqX4QERCMxLQFb2WH1NBfgJz/NV0wsJpqse2pyic2yR1IMhRYoaGGh+Tpy3ZP4y03PU/i/Gwf/4QcclAcVSLpWVpFcPQy0leJUgxkHnn2U+s4U/GC+sMPcII6dOQ2X+B3Yt2MDIpgs76t1QHtZEWqMifiv33+auq0DePX//AxtnYNIi6ZfdPm7vvbbqpWA0F9U1F5Ows60IQw6kJ3C32U1OsYVwoWLlOFbl8rEeSdatJhXyiFKG5jMfUcUUoyyL8140FBZLflhpPVcUATFLXMz6O3i++O4Ef/wN0/Avr8cb730AWpq0qFqz0Of73r84RM5CAkk5IARtU42OT+NM0LTduHPE7aTGsO89jPvoe+cAQFqHVnAVzDgv8F6HLXGffw4USfXVEOXTdpD2zF1dYQTMjEJr21fXwnQrMs8X//QNDzz/D6EBJP+4qGl68Y2tbFDSHwO/tP3N8DVMoDDB9/BZ3WNaE40o5uWvge+8ed4Jl2N0598gguXc1FudkPXqAe+/d/+JxIWmvHhBx8iv7AVD6YGi81feczbHEiv01/ScuLxgzf6rtFftGFCf7lKH2kLmstasCtOAw+OEg4q6maWX0LjbBQepWkzhlSHhT3PIINJ08bRdpwqqlfMdgF+BO5y4yKS9m0V6S+U0OMqy4WNPIkzCnHDjnKgjQ2jipI3VXkQRfpLD0UKZuCfkIAgEkJc6IOMSg/CDK+rEGVWYyClT8bBjakkumqSAwzwNLdC77adSb1qZeXXWnIFdTUMrXcIwP1PJtG00IbcS2UI3nYAz2qZ2C36w/TBOFIDcqi3h8L2QTA2dRJQ20GzrCQWS3lIoZD1mkAaAWUFXdmpJAn9hYea1L6IjQiEj6c7TMEsE00f7C2zCv0lQMAA7halTBpoHVgt+os0Mg3r18WOFoHxYcpxDSHngYcQQy6gAzVTC68W49ypE6jVRePb9HPrRtopaF+PlPtJwLEQWyV6mPRtOHv5UrRrAUN9ExgzuiEqlFKJ18z/NMP1tpMBq0c35dt0PXVwo7kvO1KFpvoh6KM5mSiZQwMR8hnbN8KFOqp2FB6vKSf0VzvKAXmCYuX03yu+FGl5a9vql4DQXyIU+otquh5jspbjfxbDIOqLC9DpsAH7dwahsmCavkd7eHjLvkSjEcOokGLEEUbXkF9EFnZsBiYri7mfxADQL872tzDWRtnIGrjrWjFrmmaK0ygMdXWYt3NhO8hHDd1G0et3Y2uqrGylY7S2W2ceO0nEYVttHdK3/yGCHS2EjNdyCszjyvJ4nBNXqbuxJSUEMRvug5F9XEPVVb6jq1+Ca1dcWgIajhcU2iAz9vIlZ6azhSFl42akRXovxkgwNsLVnf9z7Uq4t25iDsHhsexjRMddi7ioALZHe4TSUupRWYJRfTrN+LGICCBkRUMKVUAESgiUN3LCdM1XuvTyX/4zHe/ScIk386YJwxo6QpUR0hLaeqcQHBWBjKx0bEzyV+gvQ/T3zeuozkMzboxCfwlUFrASFDJD/9fJj9/HuSbgwQe2IjmC4uy2jQ3SjkEH0rQVxRMqhljoD5ODBXukmLv5meOtMtBoSPtQ6BHSkKmGc2Mcnu20d+OnBDiM9XVC5xOJ2NR0ZGVlwX+GWC/mJY7xhQ7bsAf/+c//DHsTzDhLButnb/8an1UbGYhDLNTAFAfZdszMaRC3eTNmOmpx5vgJnD1XRv/Q3KJy0fW7lkAMmQjJZEOkOcQkaSsT+R6kvJj5/FJwSpnwg7LvKtNfbHe8QH9UR2UBau0ikE7guxdpP7P0KTU2ULx+kMFRpCoM9hHIffI4Pq0ido8uXN1YL3raOzA6RQUnnsgw1IKzxz7CgB/9qelR8CDpRtk4As5OTzAIhfKKPgE0YU+TMHMQV6ua0Wdgy3T2hD9NdhMN+XjnaCXlGyOwO2kBhWdO4eiRw6js6GdZSSCJ7W7Xfq5+CViD0OypZGSlv/B3+tP7ibM6eew8AiJ9MTs5ho5eri77x5TVq6hSqVlp0jasm9Shhu2A7ULqUhq8PX2YsUnYFDaDs0c+w7ETx9DQP8qJ5DyxbPNUOnJjp+iNmZ4KvPXeBcoLUuB56UZ3QDt9ruU9nrg/hx0tJ4TTo6bF47ww1V3G4y5ieIZqYux77KUdLbmjpada+7yaJeBA7XXSX154BpEcTkrOfIaX37+AESqjLd1M1OG9fO4i6ocdsWdPFjwYXOTgSC1ySqgqnScnbSo1rSDUDNcwtkWBW3DOpmZgrZpxJkvzUW9zRbr0dqwhwgr9hc7++i47bNm7C0+R/jIQoMffnyL9pX+CJk3SXxCIh+Kj6btTWjrtzfU49v7buNxOTdlv/h7uXxfJmxXZLw4OXH0R7IGmVqG/GBDiPknSidBfGDWl6UV7/wAyGLzkNDGAYZqBoxlM0kXSyRAH8IVQN1Lt+zEXTpnCa8v5pfe88p9Fim+irwV+KZvwyIF9iHHoQ2/JP2GgdwRzwgqlqTIqKg4OyWH46IwBg71GeDEKrKHsEqqrieoZpxbonh2ID4vHw/sDME+ahDlSjT6uvDwZ1SovqCxIVXyZ65v7GEwzCw4PaCf9JV6iDlWkv/QPIcsYR0FtmsjpYzbTtNpBesGILhOx3gvoIy7K5MEyYfFf74BWviyWn3GBg2If8rj6jlr3DGLoO1fRBaD2TsK3/yQVjz9Vi1/+7Y9QXFaPsN5B+Kjn0VRXgonmSgxQoisxOYWzwAX6Lt5HbrsJj39vvzKzVHMAlXxFM31yTh4+JMwEYOuO+xFqjkbzhRcxMmmGu9aPQVdb2QlGINTUhh9fGoXFbRu+9dd/hb2jExhoLcSvXz4BPzcnZVWvzMaW3/zab6tYAtfoLxwMNYwH0M+MY4jIPEtLBV0+fSitmUFCdTZSEsKViHw14ZHKMUsieTVqWn0YWSm0H/nfNyYT/3975x0c15Wd+a/RjdzIOYOIBAGQBAEmMIAiJVEiKUqkRlQYj8eu/WN2y6Xdqa1ylV1eT/mPKa9dM+vanSpZlmyF0UgakaIkSsxiBAEiBzYCkXOOjdgAOu53XncTUHJJBAiJGrwqgmSju99759133733fOf7/eIfkujbPIG2qus481ElsXm0jGN6xZezyP2PxqPJwwTdyVEK3yiEZN7TzHtPy2qD2ZFu1JTdgev6R5FKXYE7BSqhXAlzVT4Xi3DitGpPj2GW9pR8aisDexsHzYpieRXjtrarL0ZAVpe8KSDalhcPtTGDdrKTeK+0AWOzj4EO0EwXUaFt1aPq6llcKulH2tE/R15mFAa5ajE53Y2O4WlsDnSBnsYiMx7h8LQZFHHSKCHu/mTjzk6MIzA0U6ko4BRG2Zb9IJX6m0X6izcFIyY0cfZxjgeqb2hlDiEHoUJ/IYFBHU76C5deZTpsMU6h9NO38dofSki1P4a5obsoKJriTRKFeeZBDb5hCv3FUliC6xc+QqvbIFrUWmRGxiHMLR4niypxfsEAFZP/LkHrEbdhHWJbLqI4/wKmWtWoap5HTro/RLSrzMYcJ/yg/hJ5tTaYPMLquyi65oIOt1F0qYOwNyEZvqP9qC8ugIlLTCN3dYiKPohnnj9CQQIfMuSXnjcOQL9xO2JCPDFQdB0FHVrEUFQ11dEF/8RclrKo0VFWivmASIX+YiyuwpVzBgRZe9BJUkZuZCxiSaH4sLwIZ6lwXOi7A6/ILMSmhCOaQp38a+fRH2BEZbsVe3b40eibMZGn8mpsFuZ1W0pxe9wXL21MpopawxlkL8pvlGOG3rvW2X7oXbRISYrH3kPZOED15tzMBHO7wwgIy0IUhRzNt97Hb/7tA0TtPIHp3maUcClvPf11OwkCsMSlIzAuDe6Xr5KIY0WwagRtfgk4kbkR62aaUXjzBowDXBlp7EYalXn+HkbcvdNCAPgIOhoaMB+/E6kiXOLgQoiWa9v3EQGhv4xBd/s2yT861HkEoDDSFZuTd+HlX27iiouQN3Tonx1DJhW6VsMIKm8XKu+t9wxEUYw3dmUnAYZB3OLrpbpm+MUXIdxThQ0RarTe7YRez2Ximm4gNQ9bmJ/vndLhjzcv45whGoOtTQhI2A4tO8zW6gJ0qjfi0awwrog0oaLehEf+O6kx9KFUB4aQqBSN2zcu4+xsJAZamhFAQZyvmiridgIjCotR23gXhQVF8NjNSgIK5ZZ6OX8fkf1T3KfZMIk2KqtbxkykvwxxOX4UqWl7SX8hMq+iHnoK1CINpfjdq2+gx3c3MmfaUFZuQpC7FlpfNQrOnoFFp4Kuph3hm49iffQkmlQVHIR9zIH6AKsAZpBzQgZzKsw6piT3rdq1XyDOHvkwG1lww/rUeMQSkyaCjoaaKtwuqcK4Og6HDx3ApgTSX8b6YPSORHpSnKJAtRpZxlHZTFamgc4QenS1dxBLxJxDTACaz5xEiyUAydk5iLZ1U/pejLrOSew5fBR5hDZHRURihmisohIqOY3hOPrMIWzlzRHnM48a5lTKazsRn0v6y27SX5gnlWXhB63alRozrX8gMNiAUlJpajrHkZ73FB7fTeVpQiBadGUo4uh2wnsjjh1+FJtTI+HHBLiW5SDWmVlEpmdzbZ4UGB5vY2U5yiprMKnNwHPHH0MSWaK33ngTXU76i6kdJbeL0dw/hwNPH8euLcwNk/4yWa9DYVkVhhGL48cOIystETFeM6goZglR0wBS9h7HY7kbV4/+wkZiZU3WSE8rjKGbsY+UmgCO9M3ETDUUXMO1fOaNu/WI2f4Ennp8B/PeEUwXsA0Fsv7Y1YqglGzEB3tjqIHlQJxhmliL293ViyGjFmFeU7hy6iqwLgOZ6SzpMXYqOde24Xnsfeo4DmzdwJhomacvR1VtC2wRO/HckT2cRUzixsef4GpBFfTqeBx/6WlsSgzhjJRghTXV7rL73ftV7Qr95fKnn6OZKlrb3CxrwUlkyaEyMj4K4RT1+FK856oJQe62RFinunH5kytoHrG/d57vTUmLhaG/Fp9cKoaeQiXDLOtD3YIQ6zePW2c+ozq7CdbADJx46TDTR6EcnHphsrEMt6uauNyfSFHK4wg29+Iy00z9PhuZY/fF1OgQhlzW4eC+DGL7OOPkqpeWn5toLEVRdTNXd0gievEJxLNioKX8Oq6Uch9quqjR3NyTJVjJUQHKitiaaverzepBqnatFH+11BbhzIXr0DUMcLCzDS+++Cjch2tx4fwlTAeyv2AmvpaUKQ8VDUQ6W7hqZ0JSlpTHeaGtuBDlQnlJycWJY3lIipbJ3wxu3byFtjESqXIP4ejeDSzDclEABqLaVXFGuaypidTwGZmnE/qLLKvI/+d5I8wvmBRckTjoS2G0hQX1osyT9ylry5xVGIngsPD9cgjKUfBh5GKdRunHZ6FZvwObs9PgRVXmDEknVi6ZeJLy4q4s45D0wELqWTZYFdevvR37kDylwTBjp7+IQ78YHDBlIh3katFfFOoASSesPFOOy50F4Cop/GVM5uYtcOU6vKcQTZQELxsYT9xMsQvXg5QlIQ6/aSDgeK+7nRRjmh1GAcsCvLMfwabMBAqr5kl0YYE54yUxcZO1Wo6MjCSk2KkxHvAimcJJfzGQxiOFxJ5epLwztyTZRbm5V4v+Yr/2diKOXA8plhbZuEL74Dm406bQQ8mPLd5wQsyxccYqYbJQbHWP6iFqTraJbt01vJdvIv3loFI2JIpm6cCsLqQ2eDLXwZhIW1xgO5k3svyFsfRQlsj5GjvZeS7Hubpzv1Jc7bgWa/SXxfjf77+WQ38xsjieq3LKPWEX3tGoQxqM8hKXXKmR0LDzUvqceed7me4QUow781pMGSwoy6xyNzCXxVUiMSBZMPChys+68Z4QmpBcbvt3cPWD79ewHXhRSNRO+svJc3fxxM9/hs2xHBSzPzFL2mRJ2/zy55R7mUdtNhmVflA5WP6QIn8xqJGjl452jf7ijIz97wdOfzFK/2LkMwd2WotmXqG/XCoZxrG//Ck9DNx5zbgwy/5XMaeRtiJEF+mrne2FkxwPtivJxJvZBwn9RcheXlQpOQlWP1j6C2ycwXCN2tOHrEGvlZHb/ijoLzSt0NKZyIsPg5XYZObw0NJfqFYeEfoLGbax4YFUWq5ERBYHF2v0l/uPp1gE9vf3K6STh8q0noOz8aFeLh97cxYcxoGY/QF+/5FY/OQa/WUxFs5/iWH9atNfhgYGMGb2YyqHM8wVurz36C90rl/WjNQZmJX8W2anjoHoinytPEiX0l+WOQlfkWP6rl+y0jGRB6mQTvyJZpMZ3MMWEyF1iNR2pdqJWMDJLH0p/eVhi8l3bVMr/X6J4ZfpLw9PDLkqRt2AjW3KuVi0EvGRmMgsXWYuQn8RIdKf+iYxkVn6atJfZI1ipa+v8zyE/qIR4+Af+yY3syzbyYWTEfPaxlUwPojkwSExkRv94dtkSLmyY0CJibQT6fTWzMXvr0XIAM0ZQ/Ehfbg2e/nKyrYqVqg5YiId7sMXkwdzBaXvkXaiIMj4YF2dbeWvr5yHXN8VoL/w4BxxsI8+5f/O1yT3Kc3S/trSYCmvK9Ms56zC/l7l7UveaP8u+w7kM/bvkxmr8zuXfI6vOWt7lr53Kf1FrMuc37FkNyv2z8XjkuV3x/E6jtXZXJT9y++4V2es5LyXHpe8riRYvvS6HOjX7kN5/Wti+Q0xkYsvRJzQ0FAFZbR037KPldqU8/jKl0lc7Ofh/JVz/194/5IYOd9372/H75z//7rPye/ssZLYfk18lQ8vHovc2ELEEfrLwzhLd8bi+/pbYi2DMqG/COlE6C/O6/rtjsnZbyxeqy9cV/mSr2kTX9iH3Gt8m/zhJbfv3/Ea/yuvKG1B3sGX721f+I57rzraqLzR3oD46W/4nHO/994qe7d/XmZfsiK2Rn+xB1auqSyJyiQuOjr6gQwuvtJuuOuvXuPFa7n4O/tr8nl57d7rzusr3+P4LnmPDLrl2mqWv9RAr1OKazQsiHejkEW2hdlJhf7iIfQXFq5ajRQG3aO/2MVGkuCXhK2FpJMJEktcPH3h7yvmDl/aaCM3MU6aioo0Fbr2OGkgFlJj9KTGuHj4wM/Hk4IR+ZyVpJNx1nXRQizAn7mzxW+Tk5bR4IMeEdooBNJPTJHMQkcNHq8cl405vQUScoy0FLLxONzcKZqiCMl+dPQZnV1QOh1XsW26t5GNyUJvtbv7vcT24q8WWOMk+/BQYuIkUkgsx530Fxpl2OcDFkyz7mnOTKMDxsTd8WaZfa1GTITaYpZlWOnapDHyj1pjP3dR9M4vUKgmhdDOU6dgxECRgIg1nAl9EWNZlSJae8fkJMh8cb4jgi461rP42MPRDiVeVopADHTMd6X47N4+lEBSsMbroabIwBk/aRurERNl9z/SH8uOIa+/yUJhmuNesLcfZRTEi08aEAWE0lQEiDCnCBo9vmJYYhSiC3UkImJTvHPZ9YnozEIBoCc1Bs52YyOpaZ61gWr2RW5L+oovXBruR+hWKg4KPPlHNjP7MyOPUeDvS1dqhWY0b7JRwCbUkMVvWRqT5fe3i9/7MP/rQcfERtGQWfpbdj3y4JM+hw+7rwkZqwFExMbfO8dVVhOFi0Yr249cR2drESoXrzvFZx4UrTlfdp7HMmUazLP16JBfOkGpehaTuAGY6K2n2W8dBsYNUPsGY+u2bfBX6C+tmDDQUon0F+nY1u/Yj2QWvdaUVqBXSDGsB8vI2Yr1ifQ8dST6hf7SUF6AGlIe5oT+smkbqRAJ8DCOknRSiLYBGoS5BmJL7k6F/jLeUqnQXyZIFAmM3YzcrWkIYw3r6my8WaeGWGpSiJb+SUVNHEsCRXZqNEknjbhaWAPCS3gjeyGBFImMxEh4qunewvqzwso5ZO/KQgLrzpSNjWC8V4ebxR1I3rYbaRQ/OC+UmZZ29WW0J+uk0QNRbUJ/yU6JJf2FLh35hegeJf3Fg/SX3bRhjCT9haUjRbWkvxhJf4nPYkxSEfx1aLYHECTp7EZoWl+k66I1GxszBR2uVLxt2rULXvPjqLnTxJjMkb4RjOycLfT/nUZlWTn69QaAtYRZW7ZhHT2GO2m7Vt9DRxsqt01s4N5+67BndzqCOfCS28Q4S8u/ygo00UJwQe1Ntfd2bIhhydVIK0pKK1liZYZ7YDS2baevKqkxBromVevuYmwhGLv5PSHK9zyAAKx95beOgNBfhlhrXkuzEfeITGyn7Z6bbZaklXJUNo+Czy24+8Vgx45Mggf6WfdXxZIFDpB8wrCV/UZ8uB/F3KwfrKlmqZcQXVjTnr4V6RGu6G+uRV3XiGL0EsR+IZs11yaCDqrv1EA/y1mHZxCyt27FujCamjt7U0IoxgfaUFWtIwnEBt+gOFKHUrFAr/AqXQepIrQBjUhV6FaBbIZDnXVKydq0WUMf4ARsJ8QjxGeNAPOtG8CKvpHPJXo0l5XXYJbmPhpPHyRv2IZ0Akzs9BcZiC1giGV5FbXd7GqSSBVKYi25GcM9zahgLbNccy2tALdvy0G4t5We4DpU6howa/NEWFwKtmZvZG2xOCDZt/uuIxX6S+ud23hf6C9XJ7DRQX8pf/81XKibQjBrb1pvf4phdSg7bg0Ge3qV2ZLQX65VtRBflQ11D63ybncSVxOCad1ZVIx50OyeNT70g5UykLHWIrz+zgXMsVPVGttxsbwfoZFhpL9cw9sXdPAMCcVceynK+sBOlfSX906hcUbD/S2g4NMy3pBxq0d/4ei25855/O5UPtwCI+Azz7q4Ei4TRkdivvUyPrzZRPMTb/rOkgsaEcObzAXtFTfwzqu/wftFJmzP3UIzC5o/cpuf7MXpN36Df/1jIZK276XbUbD9QcqZ7SDpL6+RaAK/YLhNN5EqM0L6SwgmdBfxztVG+NOcXd9QhMoRN4SQ/nLx96fQRdPlALcp3PysCr7rElaP/sLjHe6sxbXCOnR196Gl5BLOXKtD0qYN6L52Gh+XDvI6j0N382PotYl0NRrFddaWDo4Mo6b4GjpUMUhjXfEwz6e4vhU9fb10OKJhdLsX9uzKoNGHB+EGc+iqvoj/9/vPMUS/3TbpeHtVSIr0Qu2FN3GmsJ6lLpOUvl/EhDYZSUTLtRZfxP99nUbV1Srs35tJpxt5INvLpGTJyYeK8YdKcaoc/Q/jh6QM7of+MjPWjUun38EbJLoM+KRjb2YsNHMjKL/0Ds6UD/A7pzFj9URKIj20Ocg8f6MMY6MDqCy+hWFtGjaz7nOo+Tbe/PdP0DFqZukZZxn+NHWZbcX7r76CVjPtSLtqaVPaiqjUCHSWfIDXztXC3WUBFTQyaTdFY0uKHccmkTTou3Ht9O/xCc3JVZyxWo00QefDur8mHwW6Xox0l6IkvxiBG/ciwDKIz999BVfq+rAw3Yey27dgidiMVNKepARN8miyvPvdiDg/jOv5oI7iQdaRCv2loeI23nr7EiEiM5imLkYGPYv0F7Eg7cf1z97DP79xiXSocNYKp0LF+uQrn7yLDwraOCgbRUkZHdbcYhBN+sv59/4dt1l3ahhpRpnQX+JyVo7+oiuqQEO7AdNcYlTWS7gsMz9phD/9ZuMSUqHpqMAcl3yDk7cig/SXafqlvt/fhL2792DP5jRY2+dw9IVchNCwvXbkCjrpgjNvUirJWBLG2VpTFYzRW3BAob8MYfCfT2Kgqw29LKJO2uKgv9C15tdvd9DEYBi15mg89ZiD/tJL+svI6tFfLDze0Z4mBGbsx5FnnsQ6j070/ep19LS3wa29G56+/giLiUEEXYjW0SZPbRxGZUEFmnvp++nGWbNjBcFqmkINO/38ulHmm7z5AHUOkdnRc+looKkatoRdDvpLFwZ++xF6uunTW9KCjNzDePpgDiztQfjHd7vR5EMElCoRf6bQX1zg2/0vGBoaIf0lYfXoL1l2+ouwWss/+g8Y6sM4UPJC16geYetpFL4jAnWcYczS7tGHbkz/9eVszOtpKXjyVZQyZ2l19cf2w3+BrCfoAjXahpPWNvTFkgzj66Xc4xYSd3oYE/C7/urFR+AxUoy//90t1DS4oKF6DAdefBn7N4Wj9izRbNWtyMsgSs07lHSdLLxb4ck6Znt7e1Adxtr3fpsIWJiUcUFAVAav1SgGFPqLlIUKpN0Dmfsfx2NbkhEdQvgCfZrNhEH8j+S9mOslRvD0uzRlMWNWP6ygswaCs0lm2U6iSwjz/3zwcQCmX7Bh2848RM71wnBOx/zcBB/MU/CM34UD++IQW3wF1ZOstebsxb6ZiPdrxK2SIex+/ufYzlWl4KBg+GtdEeX3AjIPuHDwdhun3v2UgzTaffa2obHdip/88lfIDqKv7+k3kc/78YksspN9VqZc7dtEce09zgg46C9RmXj2xBFEC/3Fj+aA95YbKCo0uyAyOgGH91vROCdpJ8JQuttJFxsl/eV/4sQmob98jOscqFVbtegZ98PP/vpXSEUzTp8S+ksrHs9cNv2FZvLuQdh34i85E03Br9/qo+sMS1WZGPCNcUP1+VLmSNvQypmnnf6ioaGzGX2VN9FM+svTdOJPCPWHLSgP8RY9yq5cwJX8NmgfOwhv3ijKJmkRrkenJkUjJCSA+QkXglcd9Bd61yaSABIc4A1XWzyi3XrhYZkmCJrItJAQGg+4IT4zAtOrSH+RvJ2sqc+NdNJ9p53rmA0swm6CdpyJ6GEzBmlxNtjbRUqLD/Y9+zP8ZE8Knvz5L7B553r8+g96uv1IcbAJA81FKLgzjoPPvYjIizrSSpw3N3+tCB6E/hKNoEB/mlUYsZ70F4YGZjXpLzFhCPTTktcZjxjXPi6NzZP7moZQYsW0WgvB6BFYVfoLW6eaAynJT0yODXEkN4ytB5/AetoBuuck4M23PkRLhTsGaLf2d8eSEeJPF6N6LkWXXMf5z2uQ/hfHFANpNYvwPTVGDA41omgogG4jPCdxtuemYo7TncvFE4OtKL8TCP+pLloxDhFJxwJqN1Jw2KEKKScqaT1mStlZkvGwZfduft8EzlYalByK8kVrP77HCJDoEhyPvDx6bE8Sjs12I01doBYuzIl23y1D4UQbQqISsGvXDg4C1cyWFCOfFn/Xi9rxSC5Zsgsz6G1opBmLJ2qrCL63uipklgyuDqVFafDh66/SGpMAttRnaUOaguGZjZj+6BO80uIF24wKz/5dInw9HQ89Duj1tPXUsZ49rL0G+T21cAtehycJ1dBytlNXWIirn19D0bAGB5kmsY2yZJ8rKrHh/uyTPBFOkog+f4qTAvHgXXuQrn7D0tBgg0v2c00ovEkdSUA00nfkEtxOhrMyL+GgLSwK2/P2ECoyjfZGvshk6gL1Gq4af6WO2CeQqLWICPhVV2B8biPTlLy+oT4IUUcimEu+5a2TK0F/YQdG+ktwSCACtHQKccyahP7S1juNmMR4bMlhjjIjAoMDIxhS6C9058nvQFJmCtKTw+yf4PG70IkmgEuw23ZmYYKm64MjS8pxeDNpmC+Vc1foL3SisNNfxOHEobiiyb2VpBN53IiTiYhFlOwyH0AM5apdQxeKYyI27EKEhUs7+SS3XL2JPj3FUF4+SHvsWfzyV/+Ef/mnX+EXB3y5ZFCLLlpNBYUFwY9iLJmMylkaSSP47NT7aKAxv4kPyT7aKta192PaQS2QOMimcPXkNGWwwRm8nf7yxZgwza68VyVEHP5L3itiLOU/ym9W74eNXMgOXREa3OKxKSMWfgQpm1UeCOby9oJAdz1ZcsKlu4WFeRbFd6GztxtWPw0mRgnSpdBDNuPMCGoLb9M28BGkx0ZxYGY/frW7loOmfUjSjODGtc/x2bnb6BjnoISiIxVTAp7iSsOTF6SW2psiAT7UXfhwt4kL1+o1D/vBrv38xgjIfatRHIQcSzPs2NRegdRSPI1juzdBu9BL8PcruEB4uziajfa1o2d4APBRQ0+V8DSX9af1zIVTfBgWQVrMYB3+8P4V9LHvgTaClvJkJbMpeVN3IeJGG52+4gK9aGhOMZzWj5i/JTNS5kdNtJmz0lnLLyiclqbzqLj+R9xq6sMMSUNdzW0YHZlCoBsHd4NjimuYiweNQaSXZptS8UGvJspR6Yu+8YzXfvHgIuCKuNRsPPfSCSQG0nf9+gW8dvL6F+gvcm3UbG8i/rR3lXLhxN2IRkD0VVY6StJfqIK101/Yl9yjv7APUUu/LZ91bE4Ni/P/9/E3D0COgZ20mfSXxh4NdhzaR/rLNgyFzOAfSH/pFvqLthU1qggcTElU6C9WWi61V1Zh2j8aKZm7aeBuRv5vqzEwOAFzcog8ORX6S1NrPzZvIv3FV0gnBsQI/UUj9JcB0l8s8NQTVSb0Fyphexo7MOSgv/T1kv4St3r0F4mAu08Edh44rogaPGzJzKP4IybIj9ZkbggODUYovV9nYqJhrpObWkarstnjx4VbLjPS1ckWCiON7KuLmqBrrsJMzW4ceXQbAt29JSQK/aWxmfSXLK77q/ToGJxDKjsFd5detPUP2ekvXP8fYUdk4cO9q7ENI7mkv9CvV2Ji9lt9+ssMxT0F+dVkPj6PJOaNDKM6XPysEI+/8DIO74hE8R9/izM3K5AR+Sg27HsO6XuexJ1Lr+N/f1iDrsf3ID7IDWPkjBZUz2HnzzYgMojLurT/MzDvZHP1RGhyLv7X324k93UcDWXnMXpxBFqqos3jzeifmEa6yQP6gT74BMYqqk0+ZZXBiyju1lSU9lb4Q/h5j/7CB5IILFVc4QqMo7d2iismyJKc6GhAowgPUylOO/xfkHPgMK6e/De8dbMSe1J3ICQ2HJmb9mDf/kS0crBW/XYtdFV6fN4egb/5x79F8IQO773yHs5cdIFb3U3EPvcy/n5PJErOfYB3z5QiNysGsYQqmDmT1LgHIjYyA3vyhCjUjjkKoQZGZgBCKH7y8t/gCSLU3vvX/4Mb5W2IzARmRpsxNGNEtOsspoaH4B8aq1gE/hDi+qd2DJKt8SL0JGdPHFNoG2j5pyf9pYn0FwN8eOdbVe58WMpDkhaOXElUST/AwbWG5KmpqR4+Z+z0l/ExB/0FpL8M92CEExo/8yTTCGOc0T4w+gsbv5s34admNFYX4gyVtZNNLXyAbKWYg/SXjmbSX0K5TBuidGIWmXExz3C+y4rUTcSLjTcwjximKN26Kkph8A6DVyzJD4VFuHruFJrcBtGq9sGmqHiEeazDB4WVOEtfXdVILTTB6Ygn/SWu5QKKbpyjqbQL7jQvYFtGAKhHWZ2ZB/PDk/31uHqrgzdgKMVR3ZjkIEHL/G/V1ZO47JGIDQkRGKitpaDqCUSFaNm+pU6Jy7IchIgPsYdPLP7bX/01jCzZmBjQURwxjZxtVNl6q9BWWoyFoGh4x22Cqbgcl8/OKPSXLi8qT6Ni4WWKw6kyGjUTK2fsq4F3VA5HZeGIqTmPm8QF9fov4E6HDXtz/VeV/iLlQAOkvxTp/fHTzGQFIjAzSW4kiTaDXXUosg6hV8/SJs4Wx9p4TWtMfMxZ0Fs7xJWKFJLtvewDtKJCjAdkYUNSDESLZpxlLvn6DVjWZSEx2IpmQtDHxwbQoGtB+s5DyEiOxEREPm4xbTDWpEVrRQcy8nZTaWdDV00R8guK0do4j4JbzLtp9yE22O9Prb/5AZ0vkXjTYxSEFTD3VI1aDz8URrkSduFD7UMzjK42BVHYOR2FrCgthppKUDBshAdpLY3tXH6NyURERBQsKXEoIJnlU75vpKONRJdUhAfMw4fgi/qKInjOkYzk448gH957HICNdTFVoOli1QBdh8LXsdOdRDMH992aDUgOi0Gq9y2cO38GEcSHt4z5IYtN5G7pVUzaXGGiGKljxhsxcVEcIJsR4zOFC2c+RpOWaETqG3KeieNgjp312rbqERD6S2tdFZpGjFQVDdG4fhRp6XnwMY6grr4Wer+tyF3nTr/uW1T1l6O5qwP5heFIDvClDasGtz79mBQvFerq2hG55RmkRU+gWV2BTz78EFHWQTR2G7D1+biVpb9IjeiYUegvccxHhTPP5Y4mUkhKhHTiGo8jT5L+wrXpuXEClLWR2JAo9Bcur3EZ1p9rzqPt9SilOqqXjfLg0weRk+KHit+/BQLYkEL6S4yqF3dKS0kJmUbekaexN4czEt40BqKP5HN95igcffpJ5GSkIN7HiPqqUlTe7cG6XUdJXsmmsGV16C8yuvFgjtTQUYvyykp0TXngsWNPY9dmHq/Whto7lSgnMcIUvJUUkkeQGsGxEWeNZqMBeuZ1MtOIhwvQwlurpbpPfIZdWfumxabtBIR7mVHwH2+i2zsWaZs3ItzcifLiUrQNGvHoM8eQuzmFJgLMCTfWEi11B6MucTh+/BAJMwmI9Z5FdRnxTq1DJEiR/sLyAT8uXQjzVRSqWu5PCucf1GYTERaX4SxhWcjLWg9/npfGzYv5KBt0FZUsJWimKjsVx4/mkcwyjut8OJZX3cWsJhZPPXeEpT0RUDH/1dfcg+jtu7B5PWeVGivGeupx8s1LUMczt+o+jvPnzhOf1QG3+Dy8cPQRpDCHHujvxhlJOWqaeqBN3Yfjh3ZyELaAmpsccFFh6e5uYu3pFILWpRNh508GpkWJyZpq9/5bw/2qdqeH2zlgvka6BvOY7FMWWEYSGemDlvzruMX7vHsS2PT4szi0I4GQ9xZc+vw67tR3QEWu7fHnn0TGOuazfLWYZglcCQdTZt8EPPfTI9iYGAbXqSYUFbHDHCBlaetBHD+4AzFhpFSVl6BCV48F1wAcffYwaclU335yGgN+TEttTEKAxzyKiEbrpAp4A1ea9meGY6SlGFduFKG+YwLBaQdw4tBulq3R1MTLQiJTEYlMM4jKPohj+zez77Ev+K2pdr/anh6katc6P422umKcvZyPuuYhBCZux4svPApXob+co5I3MhvrPCdRdv0y7g5Ms5JiBkZOaBI370BCiDeRlUWoahlHaOouPPfMXiRG0ZvXxYDbzI13jJPEt+swjuxZv9L0F1rNsVOWJTJRRdkeq6fiAAAFuUlEQVTpCESjUUknijtPpXiVC78kKcisS953TzzFB4lJqB0swFdp3GjIznIG0wSKTgv9ZRs2KfQXFtST1qEUU5Py4iSdmJlPM8yzUJrwatmHyMyF/jJHcorRIkWzND1Qci72sobVo78sKMflwvMRsoir5OJEzUzHlwXmM90opXcSKKR5SbxMRBRIsbCzxklpdhIbUnUkXibyFwtPnyf9JQ8bM1hHyxqo2VnSX7iG78mZnLPo3MSaTImlEN29HIQZqeUU02yTjYId1rC60ZVAVvbl5l4t+ouoLy1ch5BztKcVmNPmtVqgLF3OUeNGcw6SF4SSs8D2YGKcNIyfu5OSIwMOfocQQaRA2mYyoKXsEt69ZcKzLz2GDLJJ55lrtVhYyM8ieXcKVKSNOeNuZHyF/qK8zlUAE8kQZq6TO9MjGi6By/eaqBIWt6cIigzE2Wht++4RuF/6i9JviEqbfQTvZK7rsr2IYQtfM7IvYWeitAehuVjYpqUjFiKQKxXvMhBU+h62EyPb1IIszbJNeUibkpQTr/cC7wvJebqzrxAqi7QNMWmQYnw1v8PT1YJ2XSFOXWjEk3/+Z9gYEwiVYthA4oeNg2S2B/GIEBLRAik1FhvzaWyfzj5GqgzEftSkFOyzn5K+xxG+NfrLV9uRDOLHCc6OYSXDiqdXuMwnNB5pI/LMERGou3oOVQVXcLlsVKG/JAd5KG1L6DD2Pol9B9uR0geJ8QKbnBuvuZi7SHu08PvmeH1p4aL0DU4Djx8u/YVLvqNUUnlypuS9Rn9RWqCVKsIxGi1oKeEWZ5aV2OTB9LDSX5RZLksOJl3DEC30F4fQe7lxcQ4u1ugv9x/Jh5v+0ocBlpwlx4au0V/uvwl8q09+L/QXitL0pL8k05hBdGErscmAYGhoCCr6HfKZ/OPexA5PqB6yZOfFGsZ7/ok/7tP+T89O/Ied9BcPWQl4yCSswjQVoQDHk/yz/CYsKj55kAraKSAgQLF/e9hi8p9e8FX4pcRQZgHioRrEkisxtXioYsh74MteussNm8REBhdizr5Gf7FHU2Iiq2TyMJWYiOjvwW92PYpMP2Vvy+8xZCZrp9gokAv58WPfnKQTcVwRY/K1zb7cLQ+ONSLOYmuQwYUzJg8nEWfxXL6vf8lKh8RQRuorvmT3fZ3UMvcr8ZA/0teuxcQeTOmH5Y/EZHUepMu8iN/wcTkHafMqdh4r8XD+ht38MF6WDlLygTJKllnp2mbPkS6lv6zFhEpg3hRL6S9rMfnuEfgy/eW7f8OP7xNL6S9r1pP26ysDLSf95WEeXMhAQKG/LH80YKHAh8l9JtddFeQBE/4U1swxW6txY7JWcnpM2otgQEwVZDosy3EaJnbl/falaiF3kPhBgYmT+uC8nYTQMEu6DGU3dCyShL99GUAIDHP8jFNspLxMscoclwwUsRHf664cD6fxjv3Kvpd/vs4js/8tlAEhSLhS4OIkBShUCUlWc2lr0ZZKqBEUI/A8lNcda/Rf/ryIIIykWijEFB6viCkW42TfpwgbDIyxlWIjL56nCK3EbEHMDeZp6i6kE08lSS4vk6ZioKiCIghPLmvbxVrU7XA5xRmPlY6J/Sgdx0pRkZkDGfsSrLxGwRmdiqxCZlgyhBOHIkWYxXgajSYFsCzJ/6W5DAtJMjYaKzjjbN+D/acIUIw07HChIEkoRI7wMuZGirkoEWDbEpGJbHZhi2MfFKQ4U6yrFRP7Ef84fy4vhhR1cHQv97orPakXN/vrFgrmnNdQLDmNFKVJ/Z/zNbkHTGw71K+xP3Jd7CsoFJEabGVj+7G/X0RnnE186b2L+3S0EwqLbNKmWMpy74jYcOU4ufN797fcx7JvsTp0pSOTo5tSvm55MVl6RD+efz/omCjXg/2Bc3OhWE2ztDNx/ELeJ5QYlVPIKCJPtgsmjtiG3Bx9qzxD7K+LeEmur/O7nOfx/wG2+sg/TzhRRQAAAABJRU5ErkJggg==", i0.ɵɵsanitizeUrl);
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_pdf_viewer_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "pdf-viewer", 25);
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext(2).$implicit;
    i0.ɵɵproperty("src", file_r4 == null ? null : file_r4.preview)("page", 1)("show-all", false)("autoresize", true)("original-size", false);
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_img_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "img", 24);
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext(2).$implicit;
    i0.ɵɵproperty("src", file_r4 == null ? null : file_r4.preview, i0.ɵɵsanitizeUrl);
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_a_6_Template(rf, ctx) { if (rf & 1) {
    const _r41 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "a", 26);
    i0.ɵɵelementStart(1, "i", 27);
    i0.ɵɵlistener("click", function LoadfilesComponent_ng_container_1_div_1_div_4_a_6_Template_i_click_1_listener() { i0.ɵɵrestoreView(_r41); const i_r5 = i0.ɵɵnextContext(2).index; const ctx_r39 = i0.ɵɵnextContext(2); return ctx_r39.deleteFile(i_r5); });
    i0.ɵɵtext(2, "cancel");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_strong_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "strong");
    i0.ɵɵtext(1, "Subiendo:\u00A0");
    i0.ɵɵelementEnd();
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_13_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 30);
    i0.ɵɵelementStart(1, "div", 31);
    i0.ɵɵelementStart(2, "span", 32);
    i0.ɵɵtext(3, "Loading...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_13_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 33);
    i0.ɵɵelementStart(1, "i", 34);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_13_div_1_Template, 4, 0, "div", 28);
    i0.ɵɵtemplate(2, LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_13_span_2_Template, 3, 0, "span", 29);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext(2).$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !file_r4.completed);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", file_r4.completed);
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_14_Template(rf, ctx) { if (rf & 1) {
    const _r46 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "id-progress-bar", 35);
    i0.ɵɵlistener("status", function LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_14_Template_id_progress_bar_status_1_listener($event) { i0.ɵɵrestoreView(_r46); const ctx_r45 = i0.ɵɵnextContext(4); return ctx_r45.getStatusFile($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext(2).$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("progress", file_r4 == null ? null : file_r4.progress);
} }
function LoadfilesComponent_ng_container_1_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 6);
    i0.ɵɵelementStart(1, "div", 15);
    i0.ɵɵelementStart(2, "div", 16);
    i0.ɵɵtemplate(3, LoadfilesComponent_ng_container_1_div_1_div_4_img_3_Template, 1, 1, "img", 17);
    i0.ɵɵtemplate(4, LoadfilesComponent_ng_container_1_div_1_div_4_pdf_viewer_4_Template, 1, 5, "pdf-viewer", 18);
    i0.ɵɵtemplate(5, LoadfilesComponent_ng_container_1_div_1_div_4_img_5_Template, 1, 1, "img", 17);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(6, LoadfilesComponent_ng_container_1_div_1_div_4_a_6_Template, 3, 0, "a", 19);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "div", 20);
    i0.ɵɵelementStart(8, "small", 21);
    i0.ɵɵtemplate(9, LoadfilesComponent_ng_container_1_div_1_div_4_strong_9_Template, 2, 0, "strong", 1);
    i0.ɵɵelementStart(10, "div", 22);
    i0.ɵɵtext(11);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(12, "div", 23);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(13, LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_13_Template, 3, 2, "ng-container", 1);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(14, LoadfilesComponent_ng_container_1_div_1_div_4_ng_container_14_Template, 2, 1, "ng-container", 1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const file_r4 = i0.ɵɵnextContext().$implicit;
    const ctx_r9 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", file_r4.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file_r4.type == "application/vnd.ms-excel");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", file_r4.type == "application/pdf");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", (file_r4 == null ? null : file_r4.progress) >= 10 && file_r4.type == "image/jpeg" || (file_r4 == null ? null : file_r4.progress) >= 10 && file_r4.type == "image/jpg" || (file_r4 == null ? null : file_r4.progress) >= 10 && file_r4.type == "image/png");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r9.ui.controlProcess ? file_r4.completed : (file_r4 == null ? null : file_r4.progress) == 100);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r9.ui.controlProcess ? !file_r4.completed : (file_r4 == null ? null : file_r4.progress) == 100);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(file_r4 == null ? null : file_r4.name);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r9.ui.controlProcess);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r9.ui.controlProcess);
} }
const _c1$3 = function (a0) { return { "file-error": a0 }; };
function LoadfilesComponent_ng_container_1_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵtemplate(1, LoadfilesComponent_ng_container_1_div_1_div_1_Template, 14, 1, "div", 5);
    i0.ɵɵtemplate(2, LoadfilesComponent_ng_container_1_div_1_div_2_Template, 12, 2, "div", 5);
    i0.ɵɵtemplate(3, LoadfilesComponent_ng_container_1_div_1_div_3_Template, 14, 1, "div", 5);
    i0.ɵɵtemplate(4, LoadfilesComponent_ng_container_1_div_1_div_4_Template, 15, 8, "div", 5);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const file_r4 = ctx.$implicit;
    const ctx_r3 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(5, _c1$3, !file_r4.valid || file_r4.alert || ctx_r3.ui.controlProcess && file_r4.loaded === false));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r3.ui.controlProcess && file_r4.loaded === false);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !file_r4.valid && !file_r4.alert);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !file_r4.valid && file_r4.alert);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r3.ui.controlProcess ? file_r4.loaded : file_r4.valid);
} }
function LoadfilesComponent_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, LoadfilesComponent_ng_container_1_div_1_Template, 5, 7, "div", 3);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r0.files);
} }
function LoadfilesComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 36);
    i0.ɵɵelementStart(1, "div", 31);
    i0.ɵɵelementStart(2, "span", 32);
    i0.ɵɵtext(3, "Loading...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function LoadfilesComponent_ng_container_3_div_1_small_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small", 45);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r51 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("JPG/PNG/PDF/XLSX/XLS. M\u00E1ximo ", ctx_r51.ui.maxSize, " mb.");
} }
function LoadfilesComponent_ng_container_3_div_1_ng_template_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small", 45);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r53 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("JPG/PNG/PDF. M\u00E1ximo ", ctx_r53.ui.maxSize, " mb.");
} }
function LoadfilesComponent_ng_container_3_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r55 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 38);
    i0.ɵɵlistener("fileDropped", function LoadfilesComponent_ng_container_3_div_1_Template_div_fileDropped_0_listener($event) { i0.ɵɵrestoreView(_r55); const ctx_r54 = i0.ɵɵnextContext(2); return ctx_r54.onFileDropped($event); });
    i0.ɵɵelementStart(1, "input", 39, 40);
    i0.ɵɵlistener("change", function LoadfilesComponent_ng_container_3_div_1_Template_input_change_1_listener($event) { i0.ɵɵrestoreView(_r55); const ctx_r56 = i0.ɵɵnextContext(2); return ctx_r56.fileBrowseHandler($event.target.files); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "div", 41);
    i0.ɵɵelementStart(4, "i", 42);
    i0.ɵɵtext(5, "flip_to_front");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "p");
    i0.ɵɵtext(7, "Arrastra y suelta el archivo o ");
    i0.ɵɵelementStart(8, "label");
    i0.ɵɵtext(9, "haz click aqu\u00ED para buscarlo en tu computador");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 43);
    i0.ɵɵelementStart(11, "i", 42);
    i0.ɵɵtext(12, "attach_file");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "p");
    i0.ɵɵelementStart(14, "label");
    i0.ɵɵtext(15, "haz click aqu\u00ED para subir el archivo desde tu celular");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(16, LoadfilesComponent_ng_container_3_div_1_small_16_Template, 2, 1, "small", 44);
    i0.ɵɵtemplate(17, LoadfilesComponent_ng_container_3_div_1_ng_template_17_Template, 2, 1, "ng-template", null, 14, i0.ɵɵtemplateRefExtractor);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const _r52 = i0.ɵɵreference(18);
    const ctx_r49 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("multiple", ctx_r49.ui.multiple)("accept", ctx_r49.ui.filesExtend ? "image/*,.pdf,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" : "image/*,.pdf");
    i0.ɵɵattribute("id", ctx_r49.dynamicId);
    i0.ɵɵadvance(7);
    i0.ɵɵattribute("for", ctx_r49.dynamicId);
    i0.ɵɵadvance(6);
    i0.ɵɵattribute("for", ctx_r49.dynamicId);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r49.ui.filesExtend)("ngIfElse", _r52);
} }
function LoadfilesComponent_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, LoadfilesComponent_ng_container_3_div_1_Template, 19, 7, "div", 37);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r2.disabledLoad);
} }
const _c2$3 = function (a0, a1, a2) { return { "clear": a0, "pair": a1, "center": a2 }; };
class LoadfilesComponent {
    constructor(iterableDiffers) {
        this.iterableDiffers = iterableDiffers;
        this.files = [];
        this.setFiles = [];
        this.collectionFiles = [];
        this.disabledLoad = true;
        this.receivedFiles = 0;
        this.dynamicId = '';
        this.ui = {
            multiple: false,
            filesUpload: 0,
            controlProcess: false,
            disabled: false,
            filesExtend: false,
            maxSize: 5
        };
        this.errorLoad = {
            title: null,
            message: null
        };
        this.dataOutput = new EventEmitter();
        this.iterableDiffer = this.iterableDiffers.find([]).create(null);
    }
    set controlProgressFile(value) {
        // console.log(value);
        this._controlProgressFile = value;
        this.updateProcessFile(this._controlProgressFile);
    }
    get controlProgressFile() {
        return this._controlProgressFile;
    }
    ngDoCheck() {
        let changes = this.iterableDiffer.diff(this.dataEntry);
        if (changes) {
            this.collectionFiles = changes.collection;
            for (const file of this.collectionFiles) {
                let unique = this.uniqueFile(file, this.setFiles);
                if (!unique) {
                    this.receivedFiles = this.receivedFiles <= this.ui.filesUpload ? ++this.receivedFiles : this.ui.filesUpload;
                    let item = new File([''], file.name, Object.assign({}, file));
                    item['preview'] = file.preview;
                    item['valid'] = 'valid' in file ? file.valid : true;
                    item['progress'] = 'progress' in item ? file.progress : 100;
                    item['alert'] = 'alert' in item ? file.progress : false;
                    this.setFiles.push(item);
                    this.files.push(item);
                }
                if (!this.ui.multiple) {
                    this.disabledLoad = false;
                    break;
                }
            }
        }
    }
    ngOnInit() {
        this.dynamicId = `fileDropRef-${this.makeid(3)}`;
        if (this.ui.maxSize === undefined) {
            this.ui.maxSize = 5;
        }
        this.formatValid = this.ui.filesExtend ? 'JPG/PNG/PDF/XLSX/XLS' : 'JPG/PNG/PDF';
        this.errorLoad = {
            title: 'El archivo no es válido.',
            message: `${this.formatValid}. Máximo ${this.ui.maxSize}mb.`
        };
    }
    fileBrowseHandler(files) {
        this.prepareFilesList(files);
        this.fileDropRef.nativeElement.value = "";
    }
    onFileDropped($event) {
        if (!this.ui.multiple) {
            Array.from($event).forEach((file, index) => {
                if (index === 0) {
                    this.prepareFilesList([file]);
                }
            });
        }
        else {
            this.prepareFilesList($event);
        }
    }
    prepareFilesList(files) {
        if (!this.ui.multiple && files.length >= 0) {
            this.disabledLoad = false;
        }
        for (const item of files) {
            item.progress = 'progress' in item ? item.progress : 0;
            item.valid = 'valid' in item ? item.valid : false;
            item.error = null;
            item.message = null;
            const reader = new FileReader();
            const type = item.type;
            const size = item.size != 0 ? item.size / 1024 / 1024 : item.size;
            reader.readAsDataURL(item);
            reader.onload = event => {
                item.preview = 'preview' in item ? item.preview : reader.result;
            };
            if (item.name.toLowerCase().split('.').pop() == 'pdf'
                || item.name.toLowerCase().split('.').pop() == 'jpg'
                || item.name.toLowerCase().split('.').pop() == 'jpeg'
                || item.name.toLowerCase().split('.').pop() == 'png'
                || this.ui.filesExtend && item.name.toLowerCase().split('.').pop() == 'xlsx'
                || this.ui.filesExtend && item.name.toLowerCase().split('.').pop() == 'xls') {
                if (size != 0 && Math.round(size) <= this.ui.maxSize) {
                    item.valid = true;
                    item.alert = false;
                    this.uploadFilesSimulator(0);
                }
                else {
                    item.error = 'Límite de peso superado';
                    item.message = `Máximo ${this.ui.maxSize}mb`;
                }
            }
            else {
                item.error = 'Formato incorrecto';
                item.message = `Debe ser ${this.formatValid}`;
            }
            this.files.forEach(function (file, index) {
                if (item.name === file.name) {
                    if (file.valid || file.alert) {
                        item.valid = false;
                        item.alert = true;
                    }
                    else if (!file.valid) {
                        item.valid = false;
                        item.alert = false;
                    }
                }
            });
            if (this.ui.controlProcess && item['valid'] && !item['alert']) {
                item.loaded = true;
                item.completed = false;
                this.dataOutput.emit(item);
            }
            console.log(item);
            this.files.push(item);
        }
    }
    uploadFilesSimulator(index) {
        setTimeout(() => {
            if (index === this.files.length) {
                return;
            }
            else {
                const progressInterval = setInterval(() => {
                    if (this.files[index]) {
                        if (this.files[index].progress === 100) {
                            clearInterval(progressInterval);
                            this.uploadFilesSimulator(index + 1);
                        }
                        else {
                            this.files[index].progress += 5;
                        }
                    }
                    else {
                        return;
                    }
                }, 200);
            }
        }, 100);
    }
    deleteFile(index) {
        this.setFiles.splice(index, 1);
        this.files.splice(index, 1);
        this.collectionFiles.splice(index, 1);
        this.disabledLoad = true;
        this.getStatusFile();
    }
    getStatusFile(status) {
        // console.log('status:', status);
        let output = this.files.filter(function (obj) {
            return obj.valid === true;
        });
        this.dataOutput.emit(output);
    }
    triggerUploader(index) {
        this.deleteFile(index);
        setTimeout(() => {
            let element = document.getElementById(this.dynamicId);
            element.click();
        }, 300);
    }
    uniqueFile(obj, list) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i].name === obj.name) {
                return true;
            }
        }
        return false;
    }
    makeid(length) {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    updateProcessFile(progress) {
        // console.log('progress:', progress);
        this.files.forEach((file) => {
            if (file.name === progress.name) {
                file.loaded = progress.loaded;
                file.completed = progress.completed;
            }
        });
    }
}
/** @nocollapse */ LoadfilesComponent.ɵfac = function LoadfilesComponent_Factory(t) { return new (t || LoadfilesComponent)(i0.ɵɵdirectiveInject(i0.IterableDiffers)); };
/** @nocollapse */ LoadfilesComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: LoadfilesComponent, selectors: [["id-loadfiles"]], viewQuery: function LoadfilesComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0$5, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.fileDropRef = _t.first);
    } }, inputs: { ui: "ui", dataEntry: "dataEntry", controlProgressFile: "controlProgressFile" }, outputs: { dataOutput: "dataOutput" }, decls: 4, vars: 8, consts: [[1, "loadfile", 3, "ngClass"], [4, "ngIf"], ["class", "drag-area p-3 rounded-sm text-center", 4, "ngIf"], ["class", "drag-area drag-area--loading rounded-sm text-center", 3, "ngClass", 4, "ngFor", "ngForOf"], [1, "drag-area", "drag-area--loading", "rounded-sm", "text-center", 3, "ngClass"], ["class", "file-data", 4, "ngIf"], [1, "file-data"], [1, "message", "message-danger", "message-action"], [1, "message-side"], [1, "message-icon"], ["src", "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4IiB2aWV3Qm94PSIwIDAgMzIgMzIiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDYyICg5MTM5MCkgLSBodHRwczovL3NrZXRjaC5jb20gLS0+CiAgICA8dGl0bGU+ZXJyb3I8L3RpdGxlPgogICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+CiAgICA8ZyBpZD0iUGFnZS0xIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgICAgICA8ZyBpZD0iZXJyb3IiIGZpbGwtcnVsZT0ibm9uemVybyI+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNiwwIEM3LjE1MiwwIDAsNy4xNTIgMCwxNiBDMCwyNC44NDggNy4xNTIsMzIgMTYsMzIgQzI0Ljg0OCwzMiAzMiwyNC44NDggMzIsMTYgQzMyLDcuMTUyIDI0Ljg0OCwwIDE2LDAgWiIgaWQ9IlBhdGgiIGZpbGw9IiNFRDJDMkQiPjwvcGF0aD4KICAgICAgICAgICAgPHBvbHlnb24gaWQ9IlBhdGgiIGZpbGw9IiNGRkZGRkYiIHBvaW50cz0iMjQgMjEuNzQ0IDIxLjc0NCAyNCAxNiAxOC4yNTYgMTAuMjU2IDI0IDggMjEuNzQ0IDEzLjc0NCAxNiA4IDEwLjI1NiAxMC4yNTYgOCAxNiAxMy43NDQgMjEuNzQ0IDggMjQgMTAuMjU2IDE4LjI1NiAxNiI+PC9wb2x5Z29uPgogICAgICAgIDwvZz4KICAgIDwvZz4KPC9zdmc+", "alt", "Error"], ["type", "button", 1, "btn", "btn-sm", "btn-primary", "btn-text", 3, "click"], [1, "btn-label"], [4, "ngIf", "ngIfElse"], ["img", ""], [1, "image-preview", "rounded-sm"], [1, "image-frame"], [3, "src", 4, "ngIf"], [3, "src", "page", "show-all", "autoresize", "original-size", 4, "ngIf"], ["href", "javascript:void(0);", 4, "ngIf"], [1, "d-flex", "justify-content-between", "mb-1"], [1, "text-truncate"], [1, "truncate"], [2, "white-space", "nowrap"], [3, "src"], [3, "src", "page", "show-all", "autoresize", "original-size"], ["href", "javascript:void(0);"], [1, "material-icons", "md-18", 3, "click"], ["class", "d-flex justify-content-center align-items-center", 4, "ngIf"], ["class", "progress-status d-flex text-decoration-none", 4, "ngIf"], [1, "d-flex", "justify-content-center", "align-items-center"], ["role", "status", 1, "spinner-border", "text-primary"], [1, "sr-only"], [1, "progress-status", "d-flex", "text-decoration-none"], [1, "material-icons", "md-18"], [3, "progress", "status"], [1, "drag-area", "p-3", "rounded-sm", "text-center"], ["class", "drag-area p-3 rounded-sm text-center", "id-dnd", "", 3, "fileDropped", 4, "ngIf"], ["id-dnd", "", 1, "drag-area", "p-3", "rounded-sm", "text-center", 3, "fileDropped"], ["type", "file", 3, "multiple", "accept", "change"], ["fileDropRef", ""], [1, "d-none", "d-lg-block"], [1, "material-icons", "md-36", "text-primary"], [1, "d-block", "d-lg-none"], ["class", "text-info", 4, "ngIf", "ngIfElse"], [1, "text-info"]], template: function LoadfilesComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, LoadfilesComponent_ng_container_1_Template, 2, 1, "ng-container", 1);
        i0.ɵɵtemplate(2, LoadfilesComponent_div_2_Template, 4, 0, "div", 2);
        i0.ɵɵtemplate(3, LoadfilesComponent_ng_container_3_Template, 2, 1, "ng-container", 1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction3(4, _c2$3, ctx.files.length === 0, ctx.files.length === 1, ctx.files.length === 1 && !ctx.ui.multiple));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.ui.filesUpload == undefined || ctx.ui.filesUpload <= ctx.receivedFiles);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.ui.filesUpload != undefined && ctx.ui.filesUpload > ctx.receivedFiles);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.ui.filesUpload == undefined || ctx.ui.filesUpload <= ctx.receivedFiles);
    } }, directives: [i1$1.NgClass, i1$1.NgIf, i1$1.NgForOf, i2$1.PdfViewerComponent, ProgressBarComponent, DndDirective], styles: ["pdf-viewer[_ngcontent-%COMP%]{-o-object-fit:cover;max-width:160px;object-fit:cover}.image-preview[_ngcontent-%COMP%]   .image-frame[_ngcontent-%COMP%]{align-items:center;display:flex;height:auto;justify-content:center;max-height:100%;overflow:hidden;width:100%}.image-preview[_ngcontent-%COMP%]   .image-frame[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{-o-object-fit:cover;object-fit:cover}.drag-area.file-error[_ngcontent-%COMP%]{background-color:#fdeaea}.drag-area.file-error[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%]{padding:0}.drag-area.file-error[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%], .drag-area.file-error[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%]   .message-side[_ngcontent-%COMP%]{align-items:center;flex-direction:column;justify-content:center}.drag-area.file-error[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%]   .message-side[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]{margin:10px 0 0}.drag-area.file-error[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%]   .message-side[_ngcontent-%COMP%]   .message-icon[_ngcontent-%COMP%]{margin:0 0 10px}.flex-1[_ngcontent-%COMP%]{flex:1}@media screen and (max-width:992px){.loadfile[_ngcontent-%COMP%]   .drag-area[_ngcontent-%COMP%]   .image-preview[_ngcontent-%COMP%]{width:100%}}.spinner-border[_ngcontent-%COMP%]{border-width:.2rem;height:1rem;width:1rem}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(LoadfilesComponent, [{
        type: Component,
        args: [{
                selector: 'id-loadfiles',
                templateUrl: './loadfiles.component.html',
                styleUrls: ['./loadfiles.component.css']
            }]
    }], function () { return [{ type: i0.IterableDiffers }]; }, { ui: [{
            type: Input
        }], dataEntry: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }], fileDropRef: [{
            type: ViewChild,
            args: ['fileDropRef', { static: false }]
        }], controlProgressFile: [{
            type: Input
        }] }); })();

class AnalyticsService {
    constructor() { }
    getUserId(rut) {
        if (!this.userId) {
            this.userId = btoa(rut);
        }
        return this.userId;
    }
    /**
     * @param categoryEvent Hace referencia a la sección del sitio web en donde el usuario está interactuando.
     * @param etiqueta Texto del elemento que se esta registrando.
     * @param tipo Tipo de evento a registrar. Valor pro defecto: 'click'.
     */
    eventRegister(categoryEvent, labelEvent, typeEvent) {
        if (!typeEvent) {
            typeEvent = 'click';
        }
        const data = {
            event: 'evento-interactivo',
            'evento-interactivo-categoria': categoryEvent,
            'evento-interactivo-accion': typeEvent,
            'evento-interactivo-etiqueta': labelEvent
        };
        window.dataLayer.push(data);
    }
    /**
     * @param categoryEvent Hace referencia a la sección del sitio web en donde el usuario está interactuando.
     * @param pageEvent Direccion o identificador de la pagina actual.
     * @param userRut Rut del usuario logeado.
     */
    pageLoadRegister(categoryEvent, pageEvent, userRut) {
        const userId = this.getUserId(userRut);
        const data = {
            event: 'pagina-virtual',
            pageEvent,
            pagename: categoryEvent,
            userID: userId,
        };
        window.dataLayer.push(data);
    }
}
/** @nocollapse */ AnalyticsService.ɵfac = function AnalyticsService_Factory(t) { return new (t || AnalyticsService)(); };
/** @nocollapse */ AnalyticsService.ɵprov = /** @pureOrBreakMyCode */ i0.ɵɵdefineInjectable({ token: AnalyticsService, factory: AnalyticsService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AnalyticsService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();

class AnaltyticsDirective {
    constructor(analyticsService) {
        this.analyticsService = analyticsService;
        this.typeEvent = 'click';
    }
    onClick() {
        this.analyticsService.eventRegister(this.categoryEvent, this.typeEvent, this.labelEvent);
    }
}
/** @nocollapse */ AnaltyticsDirective.ɵfac = function AnaltyticsDirective_Factory(t) { return new (t || AnaltyticsDirective)(i0.ɵɵdirectiveInject(AnalyticsService)); };
/** @nocollapse */ AnaltyticsDirective.ɵdir = /** @pureOrBreakMyCode */ i0.ɵɵdefineDirective({ type: AnaltyticsDirective, selectors: [["", "id-analtytics", ""]], hostBindings: function AnaltyticsDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("click", function AnaltyticsDirective_click_HostBindingHandler() { return ctx.onClick(); });
    } }, inputs: { categoryEvent: "categoryEvent", labelEvent: "labelEvent", typeEvent: "typeEvent" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AnaltyticsDirective, [{
        type: Directive,
        args: [{
                selector: '[id-analtytics]'
            }]
    }], function () { return [{ type: AnalyticsService }]; }, { categoryEvent: [{
            type: Input
        }], labelEvent: [{
            type: Input
        }], typeEvent: [{
            type: Input
        }], onClick: [{
            type: HostListener,
            args: ['click']
        }] }); })();

class ToastDirective {
    constructor(
    // private _el: ElementRef,
    _renderer) {
        this._renderer = _renderer;
        this.maxCharacters = 100;
        this.timeoutToast = undefined;
        this.idToast = new EventEmitter();
    }
    onClick(event, targetElement) {
        this.show();
        this.timer();
    }
    create() {
        this.toast = this._renderer.createElement('div');
        this._renderer.addClass(this.toast, 'alert');
        this._renderer.addClass(this.toast, 'alert-toast');
        this._renderer.addClass(this.toast, 'alert-dismissible');
        this._renderer.addClass(this.toast, `alert-${this.type}`);
        this._renderer.addClass(this.toast, 'fade');
        this._renderer.addClass(this.toast, 'show');
        const span = this._renderer.createElement('h6');
        this._renderer.appendChild(span, this._renderer.createText((this.toastText.length > this.maxCharacters) ? (this.toastText.slice(0, this.maxCharacters)) : this.toastText));
        this._renderer.appendChild(this.toast, span);
        this._renderer.setAttribute(this.toast, 'role', 'alert');
        const btn = this._renderer.createElement('button');
        this._renderer.addClass(btn, 'close');
        this._renderer.setAttribute(btn, 'type', 'button');
        this._renderer.listen(btn, 'click', () => {
            this._renderer.removeClass(this.toast, 'show');
            this._renderer.removeChild(document.body, this.toast);
            this.toast = null;
        });
        const icon = this._renderer.createElement('i');
        this._renderer.addClass(icon, 'material-icons');
        this._renderer.appendChild(icon, this._renderer.createText('close'));
        this._renderer.appendChild(btn, icon);
        this._renderer.appendChild(this.toast, btn);
        this._renderer.appendChild(document.body, this.toast);
    }
    show() {
        // console.log(document.querySelectorAll('.alert-toast'));
        // if (document.querySelectorAll('.alert-toast')) {
        //   document.querySelectorAll('.alert-toast').forEach(e => e.remove());
        // }
        if (this.toast != undefined) {
            this.hide();
        }
        this.create();
    }
    hide() {
        if (this.toast != undefined) {
            this._renderer.removeClass(this.toast, 'show');
            this._renderer.removeChild(document.body, this.toast);
            this.toast = undefined;
        }
    }
    timer() {
        window.clearTimeout(this.timeoutToast);
        this.timeoutToast = window.setTimeout(() => {
            this.hide();
        }, 5000);
    }
}
/** @nocollapse */ ToastDirective.ɵfac = function ToastDirective_Factory(t) { return new (t || ToastDirective)(i0.ɵɵdirectiveInject(i0.Renderer2)); };
/** @nocollapse */ ToastDirective.ɵdir = /** @pureOrBreakMyCode */ i0.ɵɵdefineDirective({ type: ToastDirective, selectors: [["", "id-toast", ""]], hostBindings: function ToastDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("click", function ToastDirective_click_HostBindingHandler($event) { return ctx.onClick($event, $event.target); });
    } }, inputs: { toastText: ["text", "toastText"], type: "type" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ToastDirective, [{
        type: Directive,
        args: [{
                selector: '[id-toast]'
            }]
    }], function () { return [{ type: i0.Renderer2 }]; }, { toastText: [{
            type: Input,
            args: ['text']
        }], type: [{
            type: Input
        }], onClick: [{
            type: HostListener,
            args: ['click', ['$event', '$event.target']]
        }] }); })();

function PagerComponent_nav_0_li_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 8);
    i0.ɵɵelementStart(1, "a", 4);
    i0.ɵɵtext(2, "...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
const _c0$4 = function (a0, a1) { return { "active": a0, "hide": a1 }; };
function PagerComponent_nav_0_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    const _r6 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_ng_container_10_Template_li_click_1_listener() { const restoredCtx = i0.ɵɵrestoreView(_r6); const page_r4 = restoredCtx.$implicit; const ctx_r5 = i0.ɵɵnextContext(2); return ctx_r5.goToPage(page_r4); });
    i0.ɵɵelementStart(2, "a", 4);
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const page_r4 = ctx.$implicit;
    const ctx_r2 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction2(2, _c0$4, page_r4 === (ctx_r2.pagerOption == null ? null : ctx_r2.pagerOption.currentPage), page_r4 == 1 || page_r4 == (ctx_r2.pagerOption == null ? null : ctx_r2.pagerOption.totalPages)));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(page_r4);
} }
function PagerComponent_nav_0_li_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 8);
    i0.ɵɵelementStart(1, "a", 4);
    i0.ɵɵtext(2, "...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
const _c1$2 = function (a0) { return { "disabled": a0 }; };
const _c2$2 = function (a0) { return { "active": a0 }; };
function PagerComponent_nav_0_Template(rf, ctx) { if (rf & 1) {
    const _r8 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "nav", 1);
    i0.ɵɵelementStart(1, "ul", 2);
    i0.ɵɵelementStart(2, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_Template_li_click_2_listener() { i0.ɵɵrestoreView(_r8); const ctx_r7 = i0.ɵɵnextContext(); return ctx_r7.goToPage((ctx_r7.pagerOption == null ? null : ctx_r7.pagerOption.currentPage) - 1); });
    i0.ɵɵelementStart(3, "a", 4);
    i0.ɵɵelementStart(4, "span", 5);
    i0.ɵɵtext(5, "keyboard_arrow_left");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_Template_li_click_6_listener() { i0.ɵɵrestoreView(_r8); const ctx_r9 = i0.ɵɵnextContext(); return ctx_r9.goToPage(1); });
    i0.ɵɵelementStart(7, "a", 4);
    i0.ɵɵtext(8, "1");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(9, PagerComponent_nav_0_li_9_Template, 3, 0, "li", 6);
    i0.ɵɵtemplate(10, PagerComponent_nav_0_ng_container_10_Template, 4, 5, "ng-container", 7);
    i0.ɵɵtemplate(11, PagerComponent_nav_0_li_11_Template, 3, 0, "li", 6);
    i0.ɵɵelementStart(12, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_Template_li_click_12_listener() { i0.ɵɵrestoreView(_r8); const ctx_r10 = i0.ɵɵnextContext(); return ctx_r10.goToPage(ctx_r10.pagerOption.totalPages); });
    i0.ɵɵelementStart(13, "a", 4);
    i0.ɵɵtext(14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(15, "li", 3);
    i0.ɵɵlistener("click", function PagerComponent_nav_0_Template_li_click_15_listener() { i0.ɵɵrestoreView(_r8); const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.goToPage((ctx_r11.pagerOption == null ? null : ctx_r11.pagerOption.currentPage) + 1); });
    i0.ɵɵelementStart(16, "a", 4);
    i0.ɵɵelementStart(17, "span", 5);
    i0.ɵɵtext(18, " keyboard_arrow_right ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(8, _c1$2, (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) === 1));
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(10, _c2$2, (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) === 1));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.totalPages) > ctx_r0.maxPages && (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) > ctx_r0.maxPages - ctx_r0.siblingPage);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.pages);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.totalPages) > ctx_r0.maxPages && (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) < ctx_r0.pagerOption.totalPages - ctx_r0.siblingPage);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(12, _c2$2, (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) === ctx_r0.pagerOption.totalPages));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r0.pagerOption.totalPages);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(14, _c1$2, (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.currentPage) === (ctx_r0.pagerOption == null ? null : ctx_r0.pagerOption.totalPages)));
} }
class PagerComponent {
    constructor() {
        this.currentPage = 1;
        this.perPage = 10;
        this.pageChange = new EventEmitter();
        this.maxPages = 5;
        this.siblingPage = 2;
        this.pagerOption = {
            currentPage: this.currentPage,
            perPage: this.perPage
        };
    }
    ngOnInit() {
    }
    ngOnChanges(changes) {
        const newValue = changes.totalItems;
        if (newValue) {
            this.pagerOption.totalPages = Math.ceil(this.totalItems / this.perPage);
            if (this.pagerOption.totalPages <= this.maxPages) {
                this.pagerOption.pages = Array.apply(null, Array(this.pagerOption.totalPages)).map((x, i) => i + 1);
            }
            else {
                this.calculePages();
            }
            this.goToPage(1);
        }
    }
    goToPage(page) {
        this.pagerOption.currentPage = page;
        const since = (this.pagerOption.currentPage - 1) * this.pagerOption.perPage;
        const until = this.pagerOption.currentPage * this.pagerOption.perPage;
        const ouput = { since, until };
        this.pageChange.emit(ouput);
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        if (this.pagerOption.totalPages > this.maxPages) {
            this.calculePages();
        }
    }
    calculePages() {
        let since = this.pagerOption.currentPage - this.siblingPage;
        let until = this.pagerOption.currentPage + this.siblingPage;
        if (since <= 0) {
            since = 1;
            until = since + (this.maxPages - 1);
        }
        else if (until > this.pagerOption.totalPages) {
            until = this.pagerOption.totalPages;
            since = this.pagerOption.totalPages - (this.maxPages - 1);
        }
        const pages = [];
        for (let i = since; i <= until; i++) {
            pages.push(i);
        }
        this.pagerOption.pages = pages;
    }
}
/** @nocollapse */ PagerComponent.ɵfac = function PagerComponent_Factory(t) { return new (t || PagerComponent)(); };
/** @nocollapse */ PagerComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: PagerComponent, selectors: [["id-pager"]], inputs: { totalItems: "totalItems", currentPage: "currentPage", perPage: "perPage" }, outputs: { pageChange: "pageChange" }, features: [i0.ɵɵNgOnChangesFeature], decls: 1, vars: 1, consts: [["aria-label", "pager", 4, "ngIf"], ["aria-label", "pager"], [1, "pagination", "justify-content-center", "m-0"], [1, "page-item", 3, "ngClass", "click"], ["href", "javascript:", 1, "page-link"], [1, "material-icons"], ["class", "page-item disabled xs-hide", 4, "ngIf"], [4, "ngFor", "ngForOf"], [1, "page-item", "disabled", "xs-hide"]], template: function PagerComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, PagerComponent_nav_0_Template, 19, 16, "nav", 0);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", (ctx.pagerOption == null ? null : ctx.pagerOption.totalPages) > 1);
    } }, directives: [i1$1.NgIf, i1$1.NgClass, i1$1.NgForOf], styles: ["@media (min-width:480px){.hide[_ngcontent-%COMP%]{display:none}}@media (max-width:480px){.pagination[_ngcontent-%COMP%]   .page-item[_ngcontent-%COMP%]:nth-last-child(-n+2), .xs-hide[_ngcontent-%COMP%]{display:none}.pagination[_ngcontent-%COMP%]   .page-item[_ngcontent-%COMP%]:last-child{display:flex}.pagination[_ngcontent-%COMP%]   .page-item[_ngcontent-%COMP%]:nth-child(2){display:none}}.page-link[_ngcontent-%COMP%]:focus{box-shadow:none}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(PagerComponent, [{
        type: Component,
        args: [{
                selector: 'id-pager',
                templateUrl: './pager.component.html',
                styleUrls: ['./pager.component.css'],
            }]
    }], function () { return []; }, { totalItems: [{
            type: Input
        }], currentPage: [{
            type: Input
        }], perPage: [{
            type: Input
        }], pageChange: [{
            type: Output
        }] }); })();

const _c0$3 = ["calendario"];
const _c1$1 = ["calendar"];
const _c2$1 = function (a0) { return { mask: a0, guide: false }; };
function CalendarComponent_ng_container_0_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "mat-form-field", 1);
    i0.ɵɵelementStart(2, "mat-label");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "input", 2, 3);
    i0.ɵɵlistener("dateChange", function CalendarComponent_ng_container_0_Template_input_dateChange_4_listener($event) { i0.ɵɵrestoreView(_r5); const ctx_r4 = i0.ɵɵnextContext(); return ctx_r4.dateChange($event); })("keypress", function CalendarComponent_ng_container_0_Template_input_keypress_4_listener($event) { i0.ɵɵrestoreView(_r5); const ctx_r6 = i0.ɵɵnextContext(); return ctx_r6.validKey($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "sat-datepicker", 4, 5);
    i0.ɵɵlistener("opened", function CalendarComponent_ng_container_0_Template_sat_datepicker_opened_6_listener() { i0.ɵɵrestoreView(_r5); const ctx_r7 = i0.ɵɵnextContext(); return ctx_r7.setInitialDate(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelement(8, "sat-datepicker-toggle", 6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const _r3 = i0.ɵɵreference(7);
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("mat-focused", ctx_r0.rangestate)("mat-error", !ctx_r0.validDate);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r0.ui.label ? ctx_r0.ui.label : "Selecciona");
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("placeholder", ctx_r0.ui.placeholder);
    i0.ɵɵproperty("satDatepicker", _r3)("textMask", i0.ɵɵpureFunction1(15, _c2$1, ctx_r0.ui.range ? ctx_r0.maskRange : ctx_r0.maskSingle))("min", ctx_r0.ui.min)("max", ctx_r0.ui.max)("value", ctx_r0.initialDate ? ctx_r0.initialDate : "")("disabled", ctx_r0.ui.disabled);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("rangeMode", ctx_r0.ui == null ? null : ctx_r0.ui.range)("selectFirstDateOnClose", true);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("for", _r3);
} }
function CalendarComponent_section_1_Template(rf, ctx) { if (rf & 1) {
    const _r9 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section");
    i0.ɵɵelementStart(1, "div", 7);
    i0.ɵɵelementStart(2, "sat-calendar", 8);
    i0.ɵɵlistener("dateRangesChange", function CalendarComponent_section_1_Template_sat_calendar_dateRangesChange_2_listener($event) { i0.ɵɵrestoreView(_r9); const ctx_r8 = i0.ɵɵnextContext(); return ctx_r8.inlineRangeChange($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("rangeMode", true)("selected", ctx_r1.selectedDate);
} }
const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'DD MMMM YYYY'
    },
};
class CalendarComponent {
    constructor() {
        this.maskSingle = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        this.maskRange = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        // dateRange;
        this.ui = {
            range: false,
            inlineCalendar: false,
            disabled: false,
            min: null,
            max: null,
            label: 'Fecha',
            placeholder: 'Fecha'
        };
        this.dataOutput = new EventEmitter();
        this.rangestate = false;
        this.validDate = true;
    }
    set initialDate(value) {
        this._InitialDate = value;
        this.updateInitialDate(this._InitialDate);
    }
    get initialDate() {
        return this._InitialDate;
    }
    ngOnInit() { }
    validKey($event) {
        const value = ($event.which) ? $event.which : $event.keyCode;
        if (value == 8) {
            return true;
        }
        else if (value >= 48 && value <= 57 || value == 45 || value == 47) {
            return true;
        }
        else {
            return false;
        }
    }
    dateChange($event) {
        // console.log($event);
        const inputDate = $event.target.value;
        const range = { begin: null, end: null };
        if (inputDate != null) {
            this.validDate = true;
            if (moment.isMoment(inputDate)) {
                range.begin = inputDate.format('DD/MM/YYYY');
            }
            else {
                range.begin = inputDate.begin.format('DD/MM/YYYY');
                range.end = inputDate.end.format('DD/MM/YYYY');
            }
            // this.dataOutput.emit(range);
        }
        else {
            this.validDate = false;
        }
        this.dataOutput.emit(range);
    }
    inlineRangeChange($event) {
        const range = { begin: null, end: null };
        range.begin = $event.begin.format('DD/MM/YYYY');
        range.end = $event.end ? $event.end.format('DD/MM/YYYY') : null;
        this.dataOutput.emit(range);
    }
    updateInitialDate(date) {
        // console.log('updateInitialDate');
        // console.log(date);
        if (this.ui.limitedRange) {
            this.ui.min = date.begin;
        }
    }
    selectedBeginDate(event) {
        // console.log('selectedBeginDate');
        // console.log(event);
    }
    setInitialDate() {
        // console.log('setInitialDate');
        // setTimeout(() => {
        //   console.log(this.calendario.nativeElement);
        // }, 1000);
    }
}
/** @nocollapse */ CalendarComponent.ɵfac = function CalendarComponent_Factory(t) { return new (t || CalendarComponent)(); };
/** @nocollapse */ CalendarComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: CalendarComponent, selectors: [["id-calendar"]], viewQuery: function CalendarComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0$3, 5);
        i0.ɵɵviewQuery(_c1$1, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.calendario = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.calendar = _t.first);
    } }, inputs: { ui: "ui", initialDate: "initialDate" }, outputs: { dataOutput: "dataOutput" }, features: [i0.ɵɵProvidersFeature([
            { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
            { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
            { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
        ])], decls: 2, vars: 2, consts: [[4, "ngIf"], ["appendTo", "body"], ["matInput", "", 3, "placeholder", "satDatepicker", "textMask", "min", "max", "value", "disabled", "dateChange", "keypress"], ["calendario", ""], [3, "rangeMode", "selectFirstDateOnClose", "opened"], ["calendar", ""], ["matSuffix", "", 3, "for"], [1, "inlineCalendarContainer"], [3, "rangeMode", "selected", "dateRangesChange"]], template: function CalendarComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, CalendarComponent_ng_container_0_Template, 9, 17, "ng-container", 0);
        i0.ɵɵtemplate(1, CalendarComponent_section_1_Template, 3, 2, "section", 0);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", !ctx.ui.inlineCalendar);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.ui.inlineCalendar);
    } }, directives: [i1$1.NgIf, i2$2.MatFormField, i2$2.MatLabel, i3.MatInput, i4.SatDatepickerInput, i5.MaskedInputDirective, i4.SatDatepicker, i4.SatDatepickerToggle, i2$2.MatSuffix, i4.SatCalendar], styles: [""] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(CalendarComponent, [{
        type: Component,
        args: [{
                selector: 'id-calendar',
                templateUrl: './calendar.component.html',
                styleUrls: ['./calendar.component.css'],
                providers: [
                    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
                    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
                    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
                ]
            }]
    }], function () { return []; }, { calendario: [{
            type: ViewChild,
            args: ['calendario', { static: false }]
        }], calendar: [{
            type: ViewChild,
            args: ['calendar', { static: false }]
        }], ui: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }], initialDate: [{
            type: Input
        }] }); })();

function SelectComponent_ng_option_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "ng-option", 6);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const opt_r3 = ctx.$implicit;
    i0.ɵɵproperty("value", opt_r3.value);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(opt_r3.label);
} }
function SelectComponent_small_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "small");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.ui.message);
} }
const _c0$2 = function (a0) { return { "fg-invalid": a0 }; };
class SelectComponent {
    constructor() {
        this.ui = {
            label: 'Seleccionar',
            placeholder: 'Seleccionar',
            state: true,
            message: 'Este campo es requerido',
            disabled: false,
            notfound: 'Sin resultados'
        };
        this.options = [{
                label: 'Seleccionar',
                value: null
            }];
        this.dataOutput = new EventEmitter();
    }
    set clear(value) {
        this._Clear = value;
        this.clearSelect(this._Clear);
    }
    get currentTrader() {
        return this._Clear;
    }
    ngOnInit() {
        if (this.ui.notfound === undefined) {
            this.ui.notfound = 'Sin resultados';
        }
    }
    emitChanges(event) {
        const selected = this.options.filter((option) => option.value === event);
        const output = {};
        output['label'] = selected.length != 0 ? selected[0].label : undefined;
        output['state'] = selected.length != 0 ? this.ui.state : undefined;
        output['value'] = selected.length != 0 ? selected[0].value : undefined;
        this.dataOutput.emit(output);
    }
    clearSelect(clear) {
        if (clear) {
            this.ngSelectComponent.handleClearClick();
        }
    }
}
/** @nocollapse */ SelectComponent.ɵfac = function SelectComponent_Factory(t) { return new (t || SelectComponent)(); };
/** @nocollapse */ SelectComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: SelectComponent, selectors: [["id-select"]], viewQuery: function SelectComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(NgSelectComponent, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.ngSelectComponent = _t.first);
    } }, inputs: { selected: "selected", ui: "ui", options: "options", clear: "clear" }, outputs: { dataOutput: "dataOutput" }, decls: 7, vars: 10, consts: [[1, "form-group", "mb-0", "w-100", 3, "ngClass"], ["id", "select", 1, "custom", 3, "placeholder", "disabled", "notFoundText", "ngModel", "ngModelChange", "change"], ["selector", ""], [3, "value", 4, "ngFor", "ngForOf"], ["for", "buscar"], [4, "ngIf"], [3, "value"]], template: function SelectComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "ng-select", 1, 2);
        i0.ɵɵlistener("ngModelChange", function SelectComponent_Template_ng_select_ngModelChange_1_listener($event) { return ctx.selected = $event; })("change", function SelectComponent_Template_ng_select_change_1_listener($event) { return ctx.emitChanges($event); });
        i0.ɵɵtemplate(3, SelectComponent_ng_option_3_Template, 2, 2, "ng-option", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "label", 4);
        i0.ɵɵtext(5);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(6, SelectComponent_small_6_Template, 2, 1, "small", 5);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(8, _c0$2, !(ctx.ui == null ? null : ctx.ui.state)));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("placeholder", ctx.ui.placeholder)("disabled", ctx.ui.disabled)("notFoundText", ctx.ui.notfound)("ngModel", ctx.selected);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngForOf", ctx.options);
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.ui.label);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !(ctx.ui == null ? null : ctx.ui.state));
    } }, directives: [i1$1.NgClass, i2$3.NgSelectComponent, i1.NgControlStatus, i1.NgModel, i1$1.NgForOf, i1$1.NgIf, i2$3.ɵr], styles: [""] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(SelectComponent, [{
        type: Component,
        args: [{
                selector: 'id-select',
                templateUrl: './select.component.html',
                styleUrls: ['./select.component.css']
            }]
    }], function () { return []; }, { selected: [{
            type: Input
        }], ui: [{
            type: Input
        }], options: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }], ngSelectComponent: [{
            type: ViewChild,
            args: [NgSelectComponent, { static: false }]
        }], clear: [{
            type: Input
        }] }); })();

const _c0$1 = ["pdfViewer"];
const _c1 = ["scene"];
function DocumentsComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 10);
    i0.ɵɵelementStart(1, "div", 11);
    i0.ɵɵelementStart(2, "span", 12);
    i0.ɵɵtext(3, "Loading...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
const _c2 = function (a0) { return { "disabled": a0 }; };
function DocumentsComponent_ng_container_3_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r8 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 15);
    i0.ɵɵelementStart(1, "button", 16);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_3_div_1_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r8); const ctx_r7 = i0.ɵɵnextContext(2); return ctx_r7.zoomOut(); });
    i0.ɵɵelementStart(2, "span", 7);
    i0.ɵɵtext(3, "remove");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "button", 17);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_3_div_1_Template_button_click_4_listener() { i0.ɵɵrestoreView(_r8); const ctx_r9 = i0.ɵɵnextContext(2); return ctx_r9.resetView(); });
    i0.ɵɵelementStart(5, "span", 7);
    i0.ɵɵtext(6, " filter_center_focus ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "button", 16);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_3_div_1_Template_button_click_7_listener() { i0.ɵɵrestoreView(_r8); const ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.zoomIn(); });
    i0.ɵɵelementStart(8, "span", 7);
    i0.ɵɵtext(9, "add");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(2, _c2, ctx_r5.disabledLess));
    i0.ɵɵadvance(6);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(4, _c2, ctx_r5.disabledMore));
} }
function DocumentsComponent_ng_container_3_pan_zoom_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "pan-zoom", 18);
    i0.ɵɵelementStart(1, "div", 19);
    i0.ɵɵelement(2, "img", 20, 21);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("config", ctx_r6.panzoomConfig);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("src", ctx_r6.doc, i0.ɵɵsanitizeUrl);
} }
function DocumentsComponent_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, DocumentsComponent_ng_container_3_div_1_Template, 10, 6, "div", 13);
    i0.ɵɵtemplate(2, DocumentsComponent_ng_container_3_pan_zoom_2_Template, 4, 2, "pan-zoom", 14);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.doc);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.doc);
} }
function DocumentsComponent_ng_container_4_ng2_pdfjs_viewer_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "ng2-pdfjs-viewer", 23, 24);
} if (rf & 2) {
    const ctx_r12 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("pdfSrc", ctx_r12.doc)("find", false)("print", false)("download", false)("startPrint", false)("fullScreen", false)("openFile", false)("viewBookmark", false);
} }
function DocumentsComponent_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, DocumentsComponent_ng_container_4_ng2_pdfjs_viewer_1_Template, 2, 8, "ng2-pdfjs-viewer", 22);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r2.doc);
} }
const _c3 = function (a0) { return { "active": a0 }; };
function DocumentsComponent_ng_container_6_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r18 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 26);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_6_button_1_Template_button_click_0_listener() { const restoredCtx = i0.ɵɵrestoreView(_r18); const item_r15 = restoredCtx.$implicit; const index_r16 = restoredCtx.index; const ctx_r17 = i0.ɵɵnextContext(2); return ctx_r17.requestNextFile(item_r15, index_r16); });
    i0.ɵɵelementStart(1, "i", 7);
    i0.ɵɵtext(2, "lens");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const index_r16 = ctx.index;
    const ctx_r14 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(1, _c3, ctx_r14.visible == index_r16));
} }
function DocumentsComponent_ng_container_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, DocumentsComponent_ng_container_6_button_1_Template, 3, 3, "button", 25);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r3.documents);
} }
function DocumentsComponent_ng_container_19_Template(rf, ctx) { if (rf & 1) {
    const _r20 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 27);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_19_Template_div_click_1_listener() { i0.ɵɵrestoreView(_r20); const ctx_r19 = i0.ɵɵnextContext(); return ctx_r19.requestNextFile(ctx_r19.documents[ctx_r19.visible - 1], ctx_r19.visible - 1); });
    i0.ɵɵelementStart(2, "i", 7);
    i0.ɵɵtext(3, " navigate_before ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 28);
    i0.ɵɵlistener("click", function DocumentsComponent_ng_container_19_Template_div_click_4_listener() { i0.ɵɵrestoreView(_r20); const ctx_r21 = i0.ɵɵnextContext(); return ctx_r21.requestNextFile(ctx_r21.documents[ctx_r21.visible + 1], ctx_r21.visible + 1); });
    i0.ɵɵelementStart(5, "i", 7);
    i0.ɵɵtext(6, " navigate_next ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(2, _c2, ctx_r4.visible == 0));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(4, _c2, ctx_r4.visible == ctx_r4.documents.length - 1));
} }
class DocumentsComponent {
    constructor(_domSanitizer) {
        this._domSanitizer = _domSanitizer;
        this.panzoomConfig = new PanZoomConfig({
            zoomLevels: 8,
            scalePerZoomLevel: 2.0,
            zoomStepDuration: 0.2,
            zoomToFitZoomLevelFactor: 2,
            zoomOnMouseWheel: false,
            zoomOnDoubleClick: false,
            // initialZoomToFit: {
            //   x: 0,
            //   y: 0,
            //   width: 600,
            //   height: 600
            // }
        });
        this.fileRequest = new EventEmitter();
        this.fileDownload = new EventEmitter();
        this.loaded = false;
        this.disabledLess = false;
        this.disabledMore = false;
        this.visible = 0;
        this.persistenceDocuments = [];
    }
    ngOnInit() {
        if (this.doc) {
            this.typefile = this.getFileExtension(this.doc);
            this.loaded = true;
        }
    }
    getFileExtension(doc) {
        let extension = null;
        extension = doc.type;
        return extension;
    }
    requestNextFile(name, index) {
        this.doc = undefined;
        this.loaded = false;
        this.visible = index != this.visible ? index : this.visible;
        let existe = this.persistenceDocuments.some(item => item.name === name);
        if (existe) {
            this.prepareDocs(this.persistenceDocuments[index].file);
        }
        else {
            this.fileRequest.emit(name);
        }
    }
    ngOnChanges(changes) {
        if (!changes.doc.firstChange) {
            if (changes.doc.previousValue != changes.doc.currentValue) {
                const document = changes.doc.currentValue;
                this.persistenceData(document);
                if (document) {
                    this.prepareDocs(document);
                }
            }
        }
    }
    downloadFile(name) {
        this.fileDownload.emit(name);
    }
    downloadFiles(name) {
        this.fileDownload.emit({ category: true, filename: name });
    }
    zoomIn() {
        this.disabledLess = false;
        if (this.panZoomAPI.model.zoomLevel >= 5) {
            this.disabledMore = true;
        }
        else {
            this.disabledMore = false;
        }
        this.panZoomAPI.zoomIn();
        //
    }
    zoomOut() {
        this.disabledMore = false;
        if (this.panZoomAPI.model.zoomLevel <= 2) {
            this.disabledLess = true;
        }
        else {
            this.disabledLess = false;
        }
        this.panZoomAPI.zoomOut();
    }
    resetView() {
        this.disabledLess = false;
        this.disabledMore = false;
        this.panZoomAPI.resetView();
    }
    ngAfterViewInit() {
        if (this.typefile === 'image/jpeg' || this.typefile === 'image/png') {
            this.apiSubscription = this.panzoomConfig.api.subscribe((api) => this.panZoomAPI = api);
            this.panZoomAPI.resetView();
        }
    }
    ngOnDestroy() {
        if (this.apiSubscription) {
            this.apiSubscription.unsubscribe();
        }
    }
    persistenceData(doc) {
        const item = {};
        item.name = this.documents[this.visible];
        item.file = doc;
        this.persistenceDocuments.splice(this.visible, 0, item);
    }
    prepareDocs(blobFile) {
        this.typefile = this.getFileExtension(blobFile);
        if (this.typefile === 'image/jpeg' || this.typefile === 'image/png') {
            this.apiSubscription = this.panzoomConfig.api.subscribe((api) => this.panZoomAPI = api);
            const blob = blobFile;
            const url = URL.createObjectURL(blob);
            const image = this._domSanitizer.bypassSecurityTrustUrl(url);
            this.loaded = true;
            this.doc = image;
        }
        else {
            if (this.apiSubscription !== undefined) {
                this.apiSubscription.unsubscribe();
            }
            this.loaded = true;
            this.doc = blobFile;
            if (this.pdfViewer !== undefined) {
                this.pdfViewer.pdfSrc = this.doc;
                this.pdfViewer.refresh();
            }
        }
    }
}
/** @nocollapse */ DocumentsComponent.ɵfac = function DocumentsComponent_Factory(t) { return new (t || DocumentsComponent)(i0.ɵɵdirectiveInject(i1$2.DomSanitizer)); };
/** @nocollapse */ DocumentsComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: DocumentsComponent, selectors: [["id-documents"]], viewQuery: function DocumentsComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0$1, 5);
        i0.ɵɵviewQuery(_c1, 5);
    } if (rf & 2) {
        let _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.pdfViewer = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.scene = _t.first);
    } }, inputs: { doc: "doc", documents: "documents" }, outputs: { fileRequest: "fileRequest", fileDownload: "fileDownload" }, features: [i0.ɵɵNgOnChangesFeature], decls: 20, vars: 5, consts: [[1, "id-documents"], [1, "doc-content"], ["class", "status", 4, "ngIf"], [4, "ngIf"], [1, "doc-pagination"], [1, "d-flex", "flex-column", "justify-content-center", "align-items-center", "mt-4"], ["type", "button", 1, "btn", "btn-sm", "btn-outline-secondary", "d-flex", "justify-content-center", "align-items-center", "px-4", "mb-4", 3, "click"], [1, "material-icons"], ["href", "javascript:void(0);", 1, "link", "link-icon", 3, "click"], [1, "material-icons", "link"], [1, "status"], ["role", "status", 1, "spinner-border", "text-primary"], [1, "sr-only"], ["class", "control-zoom", 4, "ngIf"], [3, "config", 4, "ngIf"], [1, "control-zoom"], [1, "btn", "btn-primary", 3, "ngClass", "click"], [1, "btn", "btn-primary", 3, "click"], [3, "config"], [2, "position", "relative", "text-align", "center"], ["id", "scene", "alt", "", 3, "src"], ["scene", ""], [3, "pdfSrc", "find", "print", "download", "startPrint", "fullScreen", "openFile", "viewBookmark", 4, "ngIf"], [3, "pdfSrc", "find", "print", "download", "startPrint", "fullScreen", "openFile", "viewBookmark"], ["pdfViewer", ""], [3, "ngClass", "click", 4, "ngFor", "ngForOf"], [3, "ngClass", "click"], [1, "doc-prev", 3, "ngClass", "click"], [1, "doc-next", 3, "ngClass", "click"]], template: function DocumentsComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵtemplate(2, DocumentsComponent_div_2_Template, 4, 0, "div", 2);
        i0.ɵɵtemplate(3, DocumentsComponent_ng_container_3_Template, 3, 2, "ng-container", 3);
        i0.ɵɵtemplate(4, DocumentsComponent_ng_container_4_Template, 2, 1, "ng-container", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "div", 4);
        i0.ɵɵtemplate(6, DocumentsComponent_ng_container_6_Template, 2, 1, "ng-container", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(7, "div", 5);
        i0.ɵɵelementStart(8, "button", 6);
        i0.ɵɵlistener("click", function DocumentsComponent_Template_button_click_8_listener() { return ctx.downloadFiles(ctx.documents[ctx.visible]); });
        i0.ɵɵelementStart(9, "i", 7);
        i0.ɵɵtext(10, " get_app ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(11, "span");
        i0.ɵɵtext(12, "Descargar documentos categor\u00EDa");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(13, "p");
        i0.ɵɵelementStart(14, "a", 8);
        i0.ɵɵlistener("click", function DocumentsComponent_Template_a_click_14_listener() { return ctx.downloadFile(ctx.documents[ctx.visible]); });
        i0.ɵɵelementStart(15, "i", 9);
        i0.ɵɵtext(16, "insert_drive_file");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(17, "span");
        i0.ɵɵtext(18, "Descargar solo este documento");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(19, DocumentsComponent_ng_container_19_Template, 7, 6, "ng-container", 3);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", !ctx.loaded);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.typefile === "image/jpeg" || ctx.typefile === "image/png");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.typefile === "application/pdf");
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.documents.length > 1);
        i0.ɵɵadvance(13);
        i0.ɵɵproperty("ngIf", ctx.documents.length > 1);
    } }, directives: [i1$1.NgIf, i1$1.NgClass, i3$1.PanZoomComponent, i4$1.PdfJsViewerComponent, i1$1.NgForOf], styles: ["[_nghost-%COMP%]    {display:block;width:100%}[_nghost-%COMP%]     iframe{min-height:600px!important}ng2-pdfjs-viewer[_ngcontent-%COMP%]{max-height:600px;min-height:300px;width:100%}.id-documents[_ngcontent-%COMP%]{height:100%;position:relative;width:100%}.doc-content[_ngcontent-%COMP%]{height:600px;min-height:300px;overflow:hidden;padding:0 36px;position:relative;width:auto}.control-zoom[_ngcontent-%COMP%], .doc-content[_ngcontent-%COMP%]{align-items:center;display:flex;justify-content:center}.control-zoom[_ngcontent-%COMP%]{bottom:0;position:absolute;z-index:999}.control-zoom[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]{display:flex;margin:5px;padding:3px}.control-zoom[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%]{font-size:22px}.doc-content[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{-o-object-fit:cover;height:100%;object-fit:cover;width:auto;z-index:2}.doc-prev[_ngcontent-%COMP%]{left:0}.doc-next[_ngcontent-%COMP%], .doc-prev[_ngcontent-%COMP%]{position:absolute;top:50%;transform:translateY(-50%)}.doc-next[_ngcontent-%COMP%]{right:0}.doc-next[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], .doc-prev[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{font-size:36px}.doc-next[_ngcontent-%COMP%]:hover, .doc-prev[_ngcontent-%COMP%]:hover{cursor:pointer}.img-full[_ngcontent-%COMP%]{height:auto;width:100%}.doc-pagination[_ngcontent-%COMP%]{align-items:center;display:flex;justify-content:center;margin-top:16px}.doc-pagination[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{background-color:transparent;border:0;display:flex;margin:0 3px;opacity:.5;padding:0}.doc-pagination[_ngcontent-%COMP%]   button.active[_ngcontent-%COMP%]{opacity:1}.doc-pagination[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:var(--primary);display:flex;font-size:12px}.disabled[_ngcontent-%COMP%]{opacity:.35}.disabled[_ngcontent-%COMP%], .disabled[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%]{color:var(--primary)}.status[_ngcontent-%COMP%]{left:0;margin:0 auto;position:absolute;right:0;top:50%;transform:translateY(-50%);width:50px;z-index:0}.disabled[_ngcontent-%COMP%]{opacity:.5;pointer-events:none}pan-zoom[_ngcontent-%COMP%]{align-items:center;display:flex;height:100%;justify-content:center;width:100%}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DocumentsComponent, [{
        type: Component,
        args: [{
                selector: 'id-documents',
                templateUrl: './documents.component.html',
                styleUrls: ['./documents.component.css']
            }]
    }], function () { return [{ type: i1$2.DomSanitizer }]; }, { doc: [{
            type: Input
        }], documents: [{
            type: Input
        }], fileRequest: [{
            type: Output
        }], fileDownload: [{
            type: Output
        }], pdfViewer: [{
            type: ViewChild,
            args: ['pdfViewer', { static: false }]
        }], scene: [{
            type: ViewChild,
            args: ['scene', { static: false }]
        }] }); })();

function TablelistComponent_ng_container_4_th_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const head_r3 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(head_r3);
} }
function TablelistComponent_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, TablelistComponent_ng_container_4_th_1_Template, 2, 1, "th", 3);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const index_r4 = ctx.index;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", index_r4 <= 2);
} }
function TablelistComponent_tr_6_td_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td");
    i0.ɵɵelementStart(1, "span", 4);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "span", 5);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext();
    const index_r8 = ctx_r10.index;
    const row_r7 = ctx_r10.$implicit;
    const ctx_r9 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r9.colhead[index_r8]);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(row_r7.qty);
} }
function TablelistComponent_tr_6_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "tr");
    i0.ɵɵelementStart(1, "td");
    i0.ɵɵelementStart(2, "span", 4);
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "span", 5);
    i0.ɵɵtext(5);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(6, TablelistComponent_tr_6_td_6_Template, 5, 2, "td", 3);
    i0.ɵɵelementStart(7, "td");
    i0.ɵɵelementStart(8, "span", 4);
    i0.ɵɵtext(9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "p", 6);
    i0.ɵɵelementStart(11, "a", 7);
    i0.ɵɵlistener("click", function TablelistComponent_tr_6_Template_a_click_11_listener() { const restoredCtx = i0.ɵɵrestoreView(_r12); const row_r7 = restoredCtx.$implicit; const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.linkDoc(row_r7.item); });
    i0.ɵɵelementStart(12, "i", 8);
    i0.ɵɵtext(13, "insert_drive_file");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(14, "span");
    i0.ɵɵtext(15);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r7 = ctx.$implicit;
    const index_r8 = ctx.index;
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r1.colhead[index_r8]);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(row_r7.item);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.quantity);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r1.colhead[index_r8]);
    i0.ɵɵadvance(6);
    i0.ɵɵtextInterpolate(row_r7.namelink);
} }
function TablelistComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 9);
    i0.ɵɵelementStart(1, "div", 10);
    i0.ɵɵelementStart(2, "span", 11);
    i0.ɵɵtext(3, "Loading...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
class TablelistComponent {
    constructor() {
        this.download = new EventEmitter();
        this.quantity = true;
        this.value = [];
        this.colhead = [];
    }
    set wait(value) {
        this._Wait = value;
    }
    get wait() {
        return this._Wait;
    }
    ngOnInit() {
        /* en caso de que no se agregue la col de cantidad */
        this.rows.forEach(row => {
            if (row.qty == undefined) {
                this.value.push(row.qty);
                if (this.value.length == this.rows.length) {
                    this.quantity = false;
                    this.colhead.forEach((name, index) => {
                        if (name == 'Cantidad de documentos' || name == 'Cantidad') {
                            this.colhead.splice(index, 1);
                        }
                    });
                }
            }
        });
    }
    linkDoc(event) {
        this.download.emit(event);
    }
}
/** @nocollapse */ TablelistComponent.ɵfac = function TablelistComponent_Factory(t) { return new (t || TablelistComponent)(); };
/** @nocollapse */ TablelistComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: TablelistComponent, selectors: [["id-tablelist"]], inputs: { rows: "rows", wait: "wait", colhead: "colhead" }, outputs: { download: "download" }, decls: 8, vars: 3, consts: [[1, "id-table", "id-table--mobile", "table", "table-striped"], [4, "ngFor", "ngForOf"], ["class", "loaderblock", 4, "ngIf"], [4, "ngIf"], [1, "term"], [1, "val"], [1, "val", "mb-0"], ["href", "javascript:void(0);", 1, "link", "link-icon", 3, "click"], [1, "material-icons", "link"], [1, "loaderblock"], ["role", "status", 1, "spinner-border", "text-primary"], [1, "sr-only"]], template: function TablelistComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementContainerStart(0);
        i0.ɵɵelementStart(1, "table", 0);
        i0.ɵɵelementStart(2, "thead");
        i0.ɵɵelementStart(3, "tr");
        i0.ɵɵtemplate(4, TablelistComponent_ng_container_4_Template, 2, 1, "ng-container", 1);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "tbody");
        i0.ɵɵtemplate(6, TablelistComponent_tr_6_Template, 16, 5, "tr", 1);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementContainerEnd();
        i0.ɵɵtemplate(7, TablelistComponent_div_7_Template, 4, 0, "div", 2);
    } if (rf & 2) {
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngForOf", ctx.colhead);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngForOf", ctx.rows);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.wait);
    } }, directives: [i1$1.NgForOf, i1$1.NgIf], styles: [".loaderblock[_ngcontent-%COMP%]{align-items:center;background-color:hsla(0,0%,100%,.9);display:flex;height:100vh;justify-content:center;left:0;position:fixed;top:0;width:100%;z-index:100000}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(TablelistComponent, [{
        type: Component,
        args: [{
                selector: 'id-tablelist',
                templateUrl: './tablelist.component.html',
                styleUrls: ['./tablelist.component.css']
            }]
    }], function () { return []; }, { download: [{
            type: Output
        }], rows: [{
            type: Input
        }], wait: [{
            type: Input
        }], colhead: [{
            type: Input
        }] }); })();

const _c0 = function (a0, a1) { return { "step-active": a0, "step-past": a1 }; };
function StepperComponent_div_0_li_4_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "li", 6);
    i0.ɵɵelementStart(1, "a", 7);
    i0.ɵɵlistener("click", function StepperComponent_div_0_li_4_Template_a_click_1_listener() { const restoredCtx = i0.ɵɵrestoreView(_r5); const i_r3 = restoredCtx.index; const ctx_r4 = i0.ɵɵnextContext(2); return ctx_r4.linkPrev(i_r3); });
    i0.ɵɵelementStart(2, "span", 8);
    i0.ɵɵelementStart(3, "small");
    i0.ɵɵelementStart(4, "strong");
    i0.ɵɵtext(5);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "small");
    i0.ɵɵelementStart(7, "strong");
    i0.ɵɵtext(8);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const step_r2 = ctx.$implicit;
    const i_r3 = ctx.index;
    const ctx_r1 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction2(3, _c0, i_r3 + 1 === ctx_r1.ui.activeStep, i_r3 + 1 < ctx_r1.ui.activeStep));
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate(i_r3 + 1);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(step_r2.name);
} }
function StepperComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 1);
    i0.ɵɵelement(1, "div", 2);
    i0.ɵɵelementStart(2, "div", 3);
    i0.ɵɵelementStart(3, "ul", 4);
    i0.ɵɵtemplate(4, StepperComponent_div_0_li_4_Template, 9, 6, "li", 5);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("ngForOf", ctx_r0.steps);
} }
class StepperComponent {
    constructor() {
        this.steps = [];
        this.dataOutput = new EventEmitter();
    }
    ngDoCheck() {
        if (this.ui.activeStep > 1 && this.ui.activeStep < this.steps.length) {
            this.dataOutput.emit(this.ui.activeStep);
        }
        else if (this.ui.activeStep <= 0) {
            this.dataOutput.emit(1);
        }
        else if (this.ui.activeStep >= this.steps.length) {
            this.dataOutput.emit(this.steps.length);
        }
    }
    ngOnInit() {
        /*steps min-max*/
        if (this.steps !== undefined) {
            if (this.steps.length < 3 || this.steps.length > 6) {
                console.warn('La cantidad de pasos del flujo no debe ser menor a 3 ni mayor a 6');
            }
        }
        /*steps undefined*/
        if (this.steps === undefined) {
            this.steps = [
                {
                    name: 'Paso'
                },
                {
                    name: 'Paso'
                },
                {
                    name: 'Paso'
                }
            ];
        }
        /* stepActive*/
        if (this.ui === undefined || this.ui.activeStep === undefined) {
            this.ui.activeStep = 0;
        }
    }
    updateUi(state) {
        this.dataOutput.emit(state.activeStep);
    }
    linkPrev(i) {
        if (i + 1 < this.ui.activeStep) {
            this.ui.activeStep = i + 1;
            this.dataOutput.emit(this.ui.activeStep);
        }
    }
}
/** @nocollapse */ StepperComponent.ɵfac = function StepperComponent_Factory(t) { return new (t || StepperComponent)(); };
/** @nocollapse */ StepperComponent.ɵcmp = /** @pureOrBreakMyCode */ i0.ɵɵdefineComponent({ type: StepperComponent, selectors: [["id-stepper"]], inputs: { ui: "ui", steps: "steps" }, outputs: { dataOutput: "dataOutput" }, decls: 1, vars: 1, consts: [["class", "row w-100 no-gutters border-bottom-1 pb-4 stepbystep", 4, "ngIf"], [1, "row", "w-100", "no-gutters", "border-bottom-1", "pb-4", "stepbystep"], [1, "d-none"], [1, "col-12"], [1, "list-group", "list-group-horizontal", "justify-content-between"], ["class", "list-group-item p-0 border-0", 3, "ngClass", 4, "ngFor", "ngForOf"], [1, "list-group-item", "p-0", "border-0", 3, "ngClass"], [3, "click"], [1, "rounded-circle"]], template: function StepperComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, StepperComponent_div_0_Template, 5, 1, "div", 0);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", ctx.steps.length > 2 && ctx.steps.length < 7);
    } }, directives: [i1$1.NgIf, i1$1.NgForOf, i1$1.NgClass], styles: ["[_nghost-%COMP%]   .step-past[_ngcontent-%COMP%]:hover{cursor:pointer}[_nghost-%COMP%]   .list-group-item[_ngcontent-%COMP%]   small[_ngcontent-%COMP%]{line-height:normal}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(StepperComponent, [{
        type: Component,
        args: [{
                selector: 'id-stepper',
                templateUrl: './stepper.component.html',
                styleUrls: ['./stepper.component.css']
            }]
    }], function () { return []; }, { ui: [{
            type: Input
        }], steps: [{
            type: Input
        }], dataOutput: [{
            type: Output
        }] }); })();

const externalModules = [
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    SatDatepickerModule,
    SatNativeDateModule,
    NgSelectModule,
    NgxPanZoomModule
];
class IsapreDigitalModule {
}
/** @nocollapse */ IsapreDigitalModule.ɵfac = function IsapreDigitalModule_Factory(t) { return new (t || IsapreDigitalModule)(); };
/** @nocollapse */ IsapreDigitalModule.ɵmod = /** @pureOrBreakMyCode */ i0.ɵɵdefineNgModule({ type: IsapreDigitalModule });
/** @nocollapse */ IsapreDigitalModule.ɵinj = /** @pureOrBreakMyCode */ i0.ɵɵdefineInjector({ providers: [
        {
            provide: LOCALE_ID,
            useValue: 'es-CL'
        }
    ], imports: [[
            FormsModule,
            ReactiveFormsModule,
            SwiperModule,
            PdfViewerModule,
            PdfJsViewerModule,
            externalModules,
            TextMaskModule
        ], MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        NgSelectModule,
        NgxPanZoomModule] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(IsapreDigitalModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    IsapreDigitalComponent,
                    ValidateContactInfoComponent,
                    CarouselComponent,
                    LoadfilesComponent,
                    DndDirective,
                    ProgressBarComponent,
                    TooltipDirective,
                    AnaltyticsDirective,
                    ToastDirective,
                    PagerComponent,
                    CalendarComponent,
                    SelectComponent,
                    DocumentsComponent,
                    TablelistComponent,
                    StepperComponent
                ],
                imports: [
                    FormsModule,
                    ReactiveFormsModule,
                    SwiperModule,
                    PdfViewerModule,
                    PdfJsViewerModule,
                    externalModules,
                    TextMaskModule
                ],
                exports: [
                    IsapreDigitalComponent,
                    ValidateContactInfoComponent,
                    CarouselComponent,
                    LoadfilesComponent,
                    PagerComponent,
                    TooltipDirective,
                    ToastDirective,
                    AnaltyticsDirective,
                    externalModules,
                    CalendarComponent,
                    SelectComponent,
                    DocumentsComponent,
                    TablelistComponent,
                    StepperComponent
                ],
                providers: [
                    {
                        provide: LOCALE_ID,
                        useValue: 'es-CL'
                    }
                ]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(IsapreDigitalModule, { declarations: [IsapreDigitalComponent,
        ValidateContactInfoComponent,
        CarouselComponent,
        LoadfilesComponent,
        DndDirective,
        ProgressBarComponent,
        TooltipDirective,
        AnaltyticsDirective,
        ToastDirective,
        PagerComponent,
        CalendarComponent,
        SelectComponent,
        DocumentsComponent,
        TablelistComponent,
        StepperComponent], imports: [FormsModule,
        ReactiveFormsModule,
        SwiperModule,
        PdfViewerModule,
        PdfJsViewerModule, MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        NgSelectModule,
        NgxPanZoomModule, TextMaskModule], exports: [IsapreDigitalComponent,
        ValidateContactInfoComponent,
        CarouselComponent,
        LoadfilesComponent,
        PagerComponent,
        TooltipDirective,
        ToastDirective,
        AnaltyticsDirective, MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        NgSelectModule,
        NgxPanZoomModule, CalendarComponent,
        SelectComponent,
        DocumentsComponent,
        TablelistComponent,
        StepperComponent] }); })();

/*
 * Public API Surface of isapre-digital
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AnaltyticsDirective, AnalyticsService, CalendarComponent, CarouselComponent, DocumentsComponent, IsapreDigitalComponent, IsapreDigitalModule, IsapreDigitalService, LoadfilesComponent, MY_FORMATS, PagerComponent, SelectComponent, StepperComponent, TablelistComponent, ToastDirective, TooltipDirective, ValidateContactInfoComponent };
//# sourceMappingURL=isapre-digital.js.map
