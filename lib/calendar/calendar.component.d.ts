import { OnInit, EventEmitter, ElementRef } from '@angular/core';
import { calendarUI } from '../../models/calendar.model';
import * as i0 from "@angular/core";
export interface Date {
    begin: any;
    end: any;
}
export declare const MY_FORMATS: {
    parse: {
        dateInput: string;
    };
    display: {
        dateInput: string;
        monthYearLabel: string;
        dateA11yLabel: string;
        monthYearA11yLabel: string;
    };
};
export declare class CalendarComponent implements OnInit {
    maskSingle: (string | RegExp)[];
    maskRange: (string | RegExp)[];
    selectedDate: Date;
    calendario: ElementRef;
    calendar: ElementRef;
    ui?: calendarUI;
    dataOutput: EventEmitter<object>;
    set initialDate(value: Date);
    get initialDate(): Date;
    private _InitialDate;
    rangestate: boolean;
    validDate: boolean;
    constructor();
    ngOnInit(): void;
    validKey($event: any): boolean;
    dateChange($event: {
        target: {
            value: any;
        };
    }): void;
    inlineRangeChange($event: any): void;
    updateInitialDate(date: any): void;
    selectedBeginDate(event: any): void;
    setInitialDate(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CalendarComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CalendarComponent, "id-calendar", never, { "ui": "ui"; "initialDate": "initialDate"; }, { "dataOutput": "dataOutput"; }, never, never>;
}
