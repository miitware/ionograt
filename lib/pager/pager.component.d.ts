import { OnInit, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { IPaginateOption, IPaginateOuput } from '../../models/pager.model';
import * as i0 from "@angular/core";
export declare class PagerComponent implements OnInit, OnChanges {
    totalItems?: number;
    currentPage?: number;
    perPage?: number;
    pageChange: EventEmitter<IPaginateOuput>;
    maxPages: number;
    pagerOption: IPaginateOption;
    siblingPage: number;
    constructor();
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    goToPage(page: number): void;
    private calculePages;
    static ɵfac: i0.ɵɵFactoryDeclaration<PagerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<PagerComponent, "id-pager", never, { "totalItems": "totalItems"; "currentPage": "currentPage"; "perPage": "perPage"; }, { "pageChange": "pageChange"; }, never, never>;
}
