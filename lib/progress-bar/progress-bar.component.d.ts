import { OnInit, EventEmitter, SimpleChanges } from '@angular/core';
import * as i0 from "@angular/core";
export declare class ProgressBarComponent implements OnInit {
    progress: number;
    status: EventEmitter<boolean>;
    constructor();
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ProgressBarComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ProgressBarComponent, "id-progress-bar", never, { "progress": "progress"; }, { "status": "status"; }, never, never>;
}
