import { OnInit, EventEmitter, DoCheck } from '@angular/core';
import { StepperUi, Steps } from '../../models/stepper.model';
import * as i0 from "@angular/core";
export declare class StepperComponent implements OnInit, DoCheck {
    ui: StepperUi;
    steps: Array<Steps>;
    dataOutput: EventEmitter<any>;
    constructor();
    ngDoCheck(): void;
    ngOnInit(): void;
    updateUi(state: StepperUi): void;
    linkPrev(i: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<StepperComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<StepperComponent, "id-stepper", never, { "ui": "ui"; "steps": "steps"; }, { "dataOutput": "dataOutput"; }, never, never>;
}
