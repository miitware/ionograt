import { EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DndDirective {
    fileOver: boolean;
    fileDropped: EventEmitter<any>;
    onDragOver(evt: any): void;
    onDragLeave(evt: any): void;
    ondrop(evt: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DndDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<DndDirective, "[id-dnd]", never, {}, { "fileDropped": "fileDropped"; }, never>;
}
