import { OnInit, EventEmitter } from '@angular/core';
import { Rows } from '../../models/tablelist.model';
import * as i0 from "@angular/core";
export declare class TablelistComponent implements OnInit {
    download: EventEmitter<string>;
    rows: Array<Rows>;
    set wait(value: boolean);
    quantity: boolean;
    value: any[];
    get wait(): boolean;
    private _Wait;
    colhead: Array<string>;
    constructor();
    ngOnInit(): void;
    linkDoc(event: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TablelistComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TablelistComponent, "id-tablelist", never, { "rows": "rows"; "wait": "wait"; "colhead": "colhead"; }, { "download": "download"; }, never, never>;
}
