import { OnInit, EventEmitter, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ValidateContactInfoUi, ValidateContactInfoData, ValidateContactInfoOutputData } from '../../models/validate-contact-info.model';
import * as i0 from "@angular/core";
export declare class ValidateContactInfoComponent implements OnInit {
    private _fb;
    private _el;
    ui?: ValidateContactInfoUi;
    dataEntry?: ValidateContactInfoData;
    dataOutput: EventEmitter<ValidateContactInfoOutputData>;
    validationOfIssuedEmail: boolean;
    formValidateContactInfo: FormGroup;
    subscription: any;
    constructor(_fb: FormBuilder, _el: ElementRef);
    ngOnInit(): void;
    editInput(event: Event, input: string): void;
    numberOnly(event: any): boolean;
    emitDynamicallyData(stateForm?: boolean): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ValidateContactInfoComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ValidateContactInfoComponent, "id-validate-contact-info", never, { "ui": "ui"; "dataEntry": "dataEntry"; }, { "dataOutput": "dataOutput"; }, never, never>;
}
