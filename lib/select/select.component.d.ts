import { EventEmitter, OnInit } from '@angular/core';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Option, SelectUi } from '../../models/select.mode';
import * as i0 from "@angular/core";
export declare class SelectComponent implements OnInit {
    selected: any;
    ui: SelectUi;
    options: Array<Option>;
    dataOutput: EventEmitter<object>;
    ngSelectComponent: NgSelectComponent;
    set clear(value: boolean);
    get currentTrader(): boolean;
    private _Clear;
    constructor();
    ngOnInit(): void;
    emitChanges(event: any): void;
    clearSelect(clear: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<SelectComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<SelectComponent, "id-select", never, { "selected": "selected"; "ui": "ui"; "options": "options"; "clear": "clear"; }, { "dataOutput": "dataOutput"; }, never, never>;
}
