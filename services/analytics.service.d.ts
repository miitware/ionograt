import * as i0 from "@angular/core";
export declare class AnalyticsService {
    private userId;
    constructor();
    private getUserId;
    /**
     * @param categoryEvent Hace referencia a la sección del sitio web en donde el usuario está interactuando.
     * @param etiqueta Texto del elemento que se esta registrando.
     * @param tipo Tipo de evento a registrar. Valor pro defecto: 'click'.
     */
    eventRegister(categoryEvent: string, labelEvent: string, typeEvent?: string): void;
    /**
     * @param categoryEvent Hace referencia a la sección del sitio web en donde el usuario está interactuando.
     * @param pageEvent Direccion o identificador de la pagina actual.
     * @param userRut Rut del usuario logeado.
     */
    pageLoadRegister(categoryEvent: string, pageEvent: string, userRut: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<AnalyticsService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<AnalyticsService>;
}
